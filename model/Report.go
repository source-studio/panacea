package model

import (
	gm "gxs3/godfather/model"
	"time"
)

/*
 Generic container for report data. Holds metdata around report creating and actual report data. Used to help report
 data fit the model framework used in the rest of the application
*/
type Report struct {
	*gm.AbstractModel
	idUser     uint                   // User that generated the report
	timestamp  time.Time              // Time when the report was generated
	reportType string                 // Type of report generated
	arguments  map[string]string      // Arguments provided by the user to create the report
	data       map[string]interface{} // Report data
}

/*
 Creates a new report with predefined metadata and report data
*/
func NewReport(id, idUser uint, timestamp time.Time, reportType string, arguments map[string]string, data map[string]interface{}) (*Report, error) {
	if idUser < 1 {
		return nil, gm.OUT_OF_RANGE
	}

	if len(reportType) < 1 {
		return nil, gm.EMPTY_STRING
	}

	return &Report{gm.NewModel(id), idUser, timestamp, reportType, arguments, data}, nil
}

/*
 Creates a new empty report
*/
func NewEmptyReport() *Report {
	var report = new(Report)
	report.AbstractModel = gm.NewModel(0)
	report.timestamp = time.Now()
	report.arguments = make(map[string]string)
	report.data = make(map[string]interface{})

	return report
}

func (r *Report) IdUser() uint {
	return r.idUser
}

func (r *Report) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return gm.OUT_OF_RANGE
	}

	r.idUser = idUser
	return nil
}

func (r *Report) Timestamp() time.Time {
	return r.timestamp
}

func (r *Report) SetTimestamp(timestamp time.Time) {
	r.timestamp = timestamp
}

func (r *Report) ReportType() string {
	return r.reportType
}

func (r *Report) SetReportType(reportType string) error {
	if len(reportType) < 1 {
		return gm.EMPTY_STRING
	}

	r.reportType = reportType
	return nil
}

func (r *Report) Arguments() map[string]string {
	return r.arguments
}

func (r *Report) SetArguments(arguments map[string]string) error {
	if arguments == nil {
		return gm.NIL_ARGUMENT
	}

	r.arguments = arguments
	return nil
}

func (r *Report) Data() map[string]interface{} {
	return r.data
}

func (r *Report) SetData(data map[string]interface{}) error {
	if data == nil {
		return gm.NIL_ARGUMENT
	}

	r.data = data
	return nil
}
