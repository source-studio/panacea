package model

import (
	gm "gxs3/godfather/model"
	"time"
)

/*
 Model representing a company or person that sells products to the business for resale to customers.
*/
type Supplier struct {
	*gm.AbstractModel
	*Contactable
	organization string // Organization or company name
	firstName    string // First name of primary contact
	lastName     string // Last name of primary contact
}

/*
 Order for goods to resell made from a supplier company
*/
type Order struct {
	*gm.AbstractModel
	idSupplier uint       // Id of the supplier from which this order was made
	timestamp  time.Time  // Timestamp on which the order was made
	reference  string     // Optional reference for order. Could be an order ID maintained by the supplier company.
	amount     float32    // Total amount the owed for the order
	paid       bool       // Wether the order was paid or not
	paidDate   *time.Time // Date on which the order was paid, if such has happened
}

/**
 * A note of credit provided by a supplier to the business when a return of product is done, or in any other situation
 * where the supplier makes a future promise to provide product free of charge or subsidized.
 */
type CreditNote struct {
	*gm.AbstractModel
	idSupplier   uint       // ID of the supplier that provided the credit note
	timestamp    time.Time  // Timestamp when the credit note was given
	reference    string     // Optional reference for credit note. Could be an ID maintained by the supplier company.
	amount       float32    // Total amount credited
	reason       string     // Reason why the credit note was given
	redeemed     bool       // Has this credit note been redeemed yet?
	redeemedDate *time.Time // Date on which the note was redeemed, if such has happened
}

/*
 Creates a new supplier
*/
func NewSupplier(id uint, organization, firstName, lastName string) (*Supplier, error) {
	if len(organization) < 1 || len(firstName) < 1 || len(lastName) < 1 {
		return nil, gm.EMPTY_STRING
	}

	var s = Supplier{gm.NewModel(id), NewContactable(), organization, firstName, lastName}
	return &s, nil
}

/*
 Creates a new order
*/
func NewOrder(id, idSupplier uint, timestamp time.Time, reference string, amount float32, paid bool, paidDate *time.Time) (*Order, error) {
	if amount < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	var order = Order{gm.NewModel(id), idSupplier, timestamp, reference, amount, paid, paidDate}
	return &order, nil
}

/*
 Creates a new credit note
*/
func NewCreditNote(id, idSupplier uint, timestamp time.Time, reference string, amount float32, reason string, redeemed bool, redeemedDate *time.Time) (*CreditNote, error) {
	if amount < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	var cn = CreditNote{gm.NewModel(id), idSupplier, timestamp, reference, amount, reason, redeemed, redeemedDate}
	return &cn, nil
}

func (s *Supplier) Organization() string {
	return s.organization
}

func (s *Supplier) SetOrganization(organization string) error {
	if len(organization) < 1 {
		return gm.EMPTY_STRING
	}

	s.organization = organization
	return nil
}

func (s *Supplier) FirstName() string {
	return s.firstName
}

func (s *Supplier) SetFirstName(firstName string) error {
	if len(firstName) < 1 {
		return gm.EMPTY_STRING
	}

	s.firstName = firstName
	return nil
}

func (s *Supplier) LastName() string {
	return s.lastName
}

func (s *Supplier) SetLastName(lastName string) error {
	if len(lastName) < 1 {
		return gm.EMPTY_STRING
	}

	s.lastName = lastName
	return nil
}

func (o *Order) IdSupplier() uint {
	return o.idSupplier
}

func (o *Order) SetIdSupplier(idSupplier uint) {
	o.idSupplier = idSupplier
}

func (o *Order) Timestamp() time.Time {
	return o.timestamp
}

func (o *Order) SetTimestamp(timestamp time.Time) {
	o.timestamp = timestamp
}

func (o *Order) Reference() string {
	return o.reference
}

func (o *Order) SetReference(reference string) {
	o.reference = reference
}

func (o *Order) Amount() float32 {
	return o.amount
}

func (o *Order) SetAmount(amount float32) error {
	if amount < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	o.amount = amount
	return nil
}

func (o *Order) Paid() bool {
	return o.paid
}

func (o *Order) SetPaid(paid bool) {
	o.paid = paid
}

func (o *Order) PaidDate() *time.Time {
	return o.paidDate
}

func (o *Order) SetPaidDate(paidDate *time.Time) {
	o.paidDate = paidDate
}

func (cn *CreditNote) IdSupplier() uint {
	return cn.idSupplier
}

func (cn *CreditNote) SetIdSupplier(idSupplier uint) {
	cn.idSupplier = idSupplier
}

func (cn *CreditNote) Timestamp() time.Time {
	return cn.timestamp
}

func (cn *CreditNote) SetTimestamp(timestamp time.Time) {
	cn.timestamp = timestamp
}

func (cn *CreditNote) Reference() string {
	return cn.reference
}

func (cn *CreditNote) SetReference(reference string) {
	cn.reference = reference
}

func (cn *CreditNote) Amount() float32 {
	return cn.amount
}

func (cn *CreditNote) SetAmount(amount float32) error {
	if amount < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	cn.amount = amount
	return nil
}

func (cn *CreditNote) Reason() string {
	return cn.reason
}

func (cn *CreditNote) SetReason(reason string) {
	cn.reason = reason
}

func (cn *CreditNote) Redeemed() bool {
	return cn.redeemed
}

func (cn *CreditNote) SetRedeemed(redeemed bool) {
	cn.redeemed = redeemed
}

func (cn *CreditNote) RedeemedDate() *time.Time {
	return cn.redeemedDate
}

func (cn *CreditNote) SetRedeemedDate(redeemedDate *time.Time) {
	cn.redeemedDate = redeemedDate
}
