package model

import (
	gm "gxs3/godfather/model"
	"time"
)

/*
 Model representing a credit line. Credit lines are used to approve a series of invoices or orders to be purchased in credit
 to be repaid later. Credit lines can be used by the business to sell to customers, as well as by the business to keep
 track of credit lines they may have with suppliers.
*/
type CreditLine struct {
	*gm.AbstractModel
	limit    float32    // Maximum amount that can be spent on this credit line account. -1 indicates unlimited.
	balance  float32    // Current amount owed in te credit line
	active   bool       // Indicates if a account is active. If inactive, new credit sales should not be allowed. Payments should be permissible.
	invoices []*Invoice // Collection of invoices for the account.
	payments []*Payment // Collection of payments for the account.
}

/*
 Model representing a payment made to a credit line account. Payments can both be received by the business from customers
 who have credit accounts with it, as well as payments made to suppliers which sell products on credit to the business
 itself
*/
type Payment struct {
	*gm.AbstractModel
	idCreditLine uint      // The credit line this payment goes towards
	idUser       uint      // ID of the user that received or made the payment for the account
	timestamp    time.Time // Moment in time when the payment was generated
	amount       float32   // Amount of money that was contributed to the account
	note         string    // Optional note that describes the payment or provides further information
}

/*
 Creates a new credit line object
*/
func NewCreditLine(id uint, limit, balance float32, active bool) (*CreditLine, error) {
	if limit < -1 || balance < 0 {
		return nil, gm.OUT_OF_RANGE
	}

	var cl = CreditLine{gm.NewModel(id), limit, balance, active, make([]*Invoice, 0), make([]*Payment, 0)}
	return &cl, nil
}

/*
 Creates a new payment object
*/
func NewPayment(id, idCreditLine, idUser uint, timestamp time.Time, amount float32, note string) (*Payment, error) {
	if amount < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	var p = Payment{gm.NewModel(id), idCreditLine, idUser, timestamp, amount, note}
	return &p, nil
}

func (cl *CreditLine) Limit() float32 {
	return cl.limit
}

func (cl *CreditLine) SetLimit(limit float32) error {
	if limit < -1 {
		return gm.OUT_OF_RANGE
	}

	cl.limit = limit
	return nil
}

func (cl *CreditLine) Balance() float32 {
	return cl.balance
}

func (cl *CreditLine) SetBalance(balance float32) error {
	if balance < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	cl.balance = balance
	return nil
}

func (cl *CreditLine) Active() bool {
	return cl.active
}

func (cl *CreditLine) SetActive(active bool) {
	cl.active = active
}

func (cl *CreditLine) Invoices() []*Invoice {
	return cl.invoices
}

func (cl *CreditLine) AddInvoice(invoice *Invoice) error {
	if invoice == nil {
		return gm.NIL_ARGUMENT
	}

	cl.invoices = append(cl.invoices, invoice)
	return nil
}

func (cl *CreditLine) SetInvoices(invoices []*Invoice) {
	if invoices == nil {
		invoices = make([]*Invoice, 0)
	}

	cl.invoices = invoices
}

func (cl *CreditLine) Payments() []*Payment {
	return cl.payments
}

func (cl *CreditLine) AddPayment(payment *Payment) error {
	if payment == nil {
		return gm.NIL_ARGUMENT
	}

	cl.payments = append(cl.payments, payment)
	return nil
}

func (cl *CreditLine) SetPayments(payments []*Payment) {
	if payments == nil {
		payments = make([]*Payment, 0)
	}

	cl.payments = payments
}

func (p *Payment) IdCreditLine() uint {
	return p.idCreditLine
}

func (p *Payment) SetIdCreditLine(idCreditLine uint) {
	p.idCreditLine = idCreditLine
}

func (p *Payment) IdUser() uint {
	return p.idUser
}

func (p *Payment) SetIdUser(idUser uint) {
	p.idUser = idUser
}

func (p *Payment) Timestamp() time.Time {
	return p.timestamp
}

func (p *Payment) SetTimestamp(timestamp time.Time) {
	p.timestamp = timestamp
}

func (p *Payment) Amount() float32 {
	return p.amount
}

func (p *Payment) SetAmount(amount float32) error {
	if amount < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	p.amount = amount
	return nil
}

func (p *Payment) Note() string {
	return p.note
}

func (p *Payment) SetNote(note string) {
	p.note = note
}
