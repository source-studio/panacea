package model

import (
	"fmt"
	gm "gxs3/godfather/model"
)

/*
 Represents a customer who makes purchases from the system. Not all people need to have customer profiles. The most common
 transaction will probably be with an annonymous customer. However for people that will require a history of transactions,
 that will have credit lines with the business and other needs, a customer profile will be necessary.
*/
type Customer struct {
	*gm.AbstractModel
	*Contactable
	firstName string   // Customer's first name
	lastName  string   // Customer's last name
	nickName  string   // Customer's nick name, if applicable
	address   *Address // Customer's address if applicable
}

/*
 Creates a new customer
*/
func NewCustomer(id uint, firstName, lastName, nickName string) (*Customer, error) {
	if len(firstName) < 1 || len(lastName) < 1 {
		return nil, gm.EMPTY_STRING
	}

	var customer = Customer{gm.NewModel(id), NewContactable(), firstName, lastName, nickName, nil}
	return &customer, nil
}

func (c *Customer) String() string {
	var rep string

	if len(c.nickName) < 1 {
		rep = fmt.Sprintf("%s %s (%s)", c.firstName, c.lastName, c.nickName)
	} else {
		rep = fmt.Sprintf("%s %s", c.firstName, c.lastName)
	}

	return rep
}

func (c *Customer) FirstName() string {
	return c.firstName
}

func (c *Customer) SetFirstName(firstName string) error {
	if len(firstName) < 1 {
		return gm.EMPTY_STRING
	}

	c.firstName = firstName
	return nil
}

func (c *Customer) LastName() string {
	return c.lastName
}

func (c *Customer) SetLastName(lastName string) error {
	if len(lastName) < 1 {
		return gm.EMPTY_STRING
	}

	c.lastName = lastName
	return nil
}

func (c *Customer) NickName() string {
	return c.nickName
}

func (c *Customer) SetNickName(nickName string) {
	c.nickName = nickName
}

func (c *Customer) Address() *Address {
	return c.address
}

func (c *Customer) SetAddress(address *Address) {
	c.address = address
}
