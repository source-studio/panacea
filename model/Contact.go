package model

import (
	"errors"
	gm "gxs3/godfather/model"
	"strconv"
	"strings"
)

const (
	HOME_PHONE   ContactType = 1 // Home phone contact type
	MOBILE_PHONE ContactType = 2 // Mobile or cellular phone contact type
	WORK_PHONE   ContactType = 3 // Work phone contact type
	EMAIL        ContactType = 4 // Email contact type
	FAX          ContactType = 5 // Fax contact type
)

/**
 * Type used to define different types of contacts
 */
type ContactType uint

/*
Model that represents a contact point for a person. Contacts are used for everal models
that represent people: customers, institutions, sales people. Any person or insttitution
that has a contact information made available.
*/
type Contact struct {
	*gm.ChildModel
	contactType ContactType // Contact type
	value       string      // Contact value
}

/*
 Utility abstract type. Provides a collection of contacts and methods for accessing and mutating these contacts. Main
 purpose is for embedding in other types that need to handle  contact info.
*/
type Contactable struct {
	contacts []*Contact // Available forms of communication with an entity
}

/*
Creates a new contact
*/
func NewContact(id uint, uuid string, contactType ContactType, value string) (*Contact, error) {
	if len(value) < 1 {
		return nil, gm.EMPTY_STRING
	}

	var contact = Contact{gm.NewChildModel(id, uuid), contactType, value}
	return &contact, nil
}

/*
 Creates a new contactable object
*/
func NewContactable() *Contactable {
	var c = Contactable{make([]*Contact, 0)}
	return &c
}

/*
Utility function for converting between a uint value and it's corresponding Contact Type. If the value is not within the
expected range of values an error will be returned
*/
func ToContactType(value uint) (ContactType, error) {
	var ct ContactType
	var err error

	switch value {
	case 1:
		ct, err = HOME_PHONE, nil
	case 2:
		ct, err = MOBILE_PHONE, nil
	case 3:
		ct, err = WORK_PHONE, nil
	case 4:
		ct, err = EMAIL, nil
	case 5:
		ct, err = FAX, nil
	default:
		ct, err = ContactType(0), errors.New("Unexpected value for Contact Type: "+strconv.FormatUint(uint64(value), 10))
	}

	return ct, err
}

func FormatContactType(value ContactType) (string, error) {
	switch value {
	case HOME_PHONE:
		return "HOME_PHONE", nil

	case MOBILE_PHONE:
		return "MOBILE_PHONE", nil

	case WORK_PHONE:
		return "WORK_PHONE", nil

	case EMAIL:
		return "EMAIL", nil

	case FAX:
		return "FAX", nil

	}

	return "", errors.New("Unexpected contact type value")
}

func ParseContactType(value string) (ContactType, error) {
	switch strings.ToUpper(value) {
	case "HOME_PHONE":
		return HOME_PHONE, nil

	case "MOBILE_PHONE":
		return MOBILE_PHONE, nil

	case "WORK_PHONE":
		return WORK_PHONE, nil

	case "EMAIL":
		return EMAIL, nil

	case "FAX":
		return FAX, nil
	}

	return ContactType(0), errors.New("Unexpected contact type value")
}

func (c *Contact) ContactType() ContactType {
	return c.contactType
}

func (c *Contact) SetContactType(contactType ContactType) {
	c.contactType = contactType
}

func (c *Contact) Value() string {
	return c.value
}

func (c *Contact) SetValue(value string) error {
	if len(value) < 1 {
		return gm.EMPTY_STRING
	}

	c.value = value
	return nil
}

/*
 Returns the curent collection of contacts. This is not a copy, but a reference to the existing collection.
*/
func (c *Contactable) Contacts() []*Contact {
	return c.contacts
}

/*
 Determines if the entity has a contact in its collection
*/
func (c *Contactable) HasContact(contact *Contact) bool {
	if contact == nil {
		return false
	}

	for _, existing := range c.contacts {
		if existing.Uuid() == contact.Uuid() {
			return true
		}
	}

	return false
}

/*
 Adds a contact to the existing collection. Returns an error if the argument is nil
*/
func (c *Contactable) Add(contact *Contact) error {
	if contact == nil {
		return gm.NIL_ARGUMENT
	}

	c.contacts = append(c.contacts, contact)
	return nil
}

/*
 Establishes the collection of contacts as the parameter. Takes a reference. If nil is passed as an argument,
 it will establish an empty collection.
*/
func (c *Contactable) SetContacts(contacts []*Contact) {
	if contacts == nil {
		c.contacts = make([]*Contact, 0)
	} else {
		c.contacts = contacts
	}
}
