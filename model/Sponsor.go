package model

import (
	"errors"
	"fmt"
	gm "gxs3/godfather/model"
	"time"
)

/*
 An institution that has a sponsor relationship with the business. Institutions will pay a portion of invoices which they
 choose to sponsor. These will be approved by them before or after the transaction takes place. They will have a credit
 line associated to them, to keep track of all sponsored transactions, for follow by the business.
*/
type Sponsor struct {
	*gm.AbstractModel
	*Contactable
	organization string   // Name of the organization
	firstName    string   // First name of the primary contact within the organization
	lastName     string   // Last name of the primary contact within the organization
	address      *Address // Address for the organization
}

/*
 An instance when an invoice was sponsored. When this happens a sponsor agrees to pay a portion of the invoice total.
 The customer only needs to pay the remaining portion. The customer pays his portion upon sale. The sponsor pays a
 collection of sponsorships at a later date.
*/
type Sponsorship struct {
	*gm.AbstractModel
	idSponsor uint       // ID of the sponsor who is doing the sponsorship
	idInvoice uint       // ID of the invoice being sponsored
	timestamp time.Time  // Date/Time when the sponsored invoice was sold
	total     float32    // Total amount the original invoice adds up to
	sponsored float32    // Amount sponsored
	paid      bool       // Has this been paid by the sponsor yet?
	paidDate  *time.Time // Date the sponsor paid, if such has happened
}

/*
 Creates a new sponsor
*/
func NewSponsor(id uint, organization, firstName, lastName string) (*Sponsor, error) {
	if len(organization) < 1 || len(firstName) < 1 || len(lastName) < 1 {
		return nil, gm.EMPTY_STRING
	}

	var sponsor = Sponsor{gm.NewModel(id), NewContactable(), organization, firstName, lastName, nil}
	return &sponsor, nil
}

/*
 Creates a new sponsorship
*/
func NewSponsorship(id, idSponsor, idInvoice uint, timestamp time.Time, total, sponsored float32, paid bool, paidDate *time.Time) (*Sponsorship, error) {
	if idSponsor < 1 || idInvoice < 1 {
		return nil, gm.INVALID_ARGUMENT
	}

	if total < 0 || sponsored < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	if sponsored > total {
		return nil, errors.New(fmt.Sprintf("The amount sponsored (%f) cannot be higher than the invoice total (%f)", sponsored, total))
	}

	var sponsorship = Sponsorship{gm.NewModel(id), idSponsor, idInvoice, timestamp, total, sponsored, paid, paidDate}
	return &sponsorship, nil
}

func (s *Sponsor) String() string {
	return s.organization
}

func (s *Sponsor) Organization() string {
	return s.organization
}

func (s *Sponsor) SetOrganization(organization string) error {
	if len(organization) < 1 {
		return gm.EMPTY_STRING
	}

	s.organization = organization
	return nil
}

func (s *Sponsor) FirstName() string {
	return s.firstName
}

func (s *Sponsor) SetFirstName(firstName string) error {
	if len(firstName) < 1 {
		return gm.EMPTY_STRING
	}

	s.firstName = firstName
	return nil
}

func (s *Sponsor) LastName() string {
	return s.lastName
}

func (s *Sponsor) SetLastName(lastName string) error {
	if len(lastName) < 1 {
		return gm.EMPTY_STRING
	}

	s.lastName = lastName
	return nil
}

func (s *Sponsor) Address() *Address {
	return s.address
}

func (s *Sponsor) SetAddress(address *Address) {
	s.address = address
}

func (s *Sponsorship) IdSponsor() uint {
	return s.idSponsor
}

func (s *Sponsorship) SetIdSponsor(idSponsor uint) error {
	if idSponsor < 1 {
		return gm.INVALID_ARGUMENT
	}

	s.idSponsor = idSponsor
	return nil
}

func (s *Sponsorship) IdInvoice() uint {
	return s.idInvoice
}

func (s *Sponsorship) SetIdInvoice(idInvoice uint) error {
	if idInvoice < 1 {
		return gm.INVALID_ARGUMENT
	}

	s.idInvoice = idInvoice
	return nil
}

func (s *Sponsorship) Timestamp() time.Time {
	return s.timestamp
}

func (s *Sponsorship) SetTimestamp(timestamp time.Time) {
	s.timestamp = timestamp
}

func (s *Sponsorship) Total() float32 {
	return s.total
}

func (s *Sponsorship) SetTotal(total float32) error {
	if total < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	s.total = total
	return nil
}

func (s *Sponsorship) Sponsored() float32 {
	return s.sponsored
}

func (s *Sponsorship) SetSponsored(sponsored float32) error {
	if sponsored < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	s.sponsored = sponsored
	return nil
}

func (s *Sponsorship) Paid() bool {
	return s.paid
}

func (s *Sponsorship) SetPaid(paid bool) {
	s.paid = paid
}

func (s *Sponsorship) PaidDate() *time.Time {
	return s.paidDate
}

func (s *Sponsorship) SetPaidDate(paidDate *time.Time) {
	s.paidDate = paidDate
}
