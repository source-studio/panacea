package model

import (
	"errors"
	gm "gxs3/godfather/model"
	"strconv"
	"strings"
	"time"
)

const (
	INVENTORY_THRESHOLD NotificationType = 1 // Notification raised when a product's inventory goes below the threshold
	EXPIRED_INVENTORY   NotificationType = 2 // Notification raised when a product has passed its expiration date
)

/**
 * Determines the type of notification.
 */
type NotificationType uint

/*
 Model representing a notificaiton. Notifications are presented to the user upon login, and the user interferce is expected
 to poll for them at an interval. They present timely information about activity of the business. The user can dismiss them
 or take action on them directly.
*/
type Notification struct {
	*gm.AbstractModel
	idSubscription   uint             // ID of the subscription that generated this notification
	idUser           uint             // ID of the user who is meant to see this notification
	idSource         uint             // Optional ID that points to the source object for which the notification is being raised
	notificationType NotificationType // Type of notification
	timestamp        time.Time        // Date/Time when the notification was created
}

/*
 Creates a new Notification
*/
func NewNotification(id, idSubscription, idUser, idSource uint, notificationType NotificationType, timestamp time.Time) (*Notification, error) {
	if idSubscription < 1 || idUser < 1 {
		return nil, gm.OUT_OF_RANGE
	}

	return &Notification{gm.NewModel(id), idSubscription, idUser, idSource, notificationType, timestamp}, nil
}

/*
 Formats a notification type value into a string representation
*/
func FormatNotificationType(ntype NotificationType) (string, error) {
	switch ntype {
	case INVENTORY_THRESHOLD:
		return "INVENTORY_THRESHOLD", nil

	case EXPIRED_INVENTORY:
		return "EXPIRED_INVENTORY", nil
	}

	return "", errors.New("Unexpected notification type")
}

/*
 Converts a uint value into the corresponding notification type
*/
func ToNotificationType(value uint) (NotificationType, error) {
	var ntype NotificationType
	var err error

	switch value {
	case 1:
		ntype, err = INVENTORY_THRESHOLD, nil
	case 2:
		ntype, err = EXPIRED_INVENTORY, nil
	default:
		ntype, err = NotificationType(0), errors.New("Unexpected value for NotificationType: "+
			strconv.FormatUint(uint64(value), 10))
	}

	return ntype, err
}

/*
 Converts a string representation of a notification type to its native value
*/
func ParseNotificationType(value string) (NotificationType, error) {
	switch strings.ToUpper(value) {
	case "INVENTORY_THRESHOLD":
		return INVENTORY_THRESHOLD, nil
	case "EXPIRED_INVENTORY":
		return EXPIRED_INVENTORY, nil
	}

	return NotificationType(0), errors.New("Unexpected notification type value: " + value)
}

func (n *Notification) IdSubscription() uint {
	return n.idSubscription
}

func (n *Notification) SetIdSubscription(idSubscription uint) error {
	if idSubscription < 1 {
		return gm.OUT_OF_RANGE
	}

	n.idSubscription = idSubscription
	return nil
}

func (n *Notification) IdUser() uint {
	return n.idUser
}

func (n *Notification) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return gm.OUT_OF_RANGE
	}

	n.idUser = idUser
	return nil
}

func (n *Notification) IdSource() uint {
	return n.idSource
}

func (n *Notification) SetIdSource(idSource uint) {
	n.idSource = idSource
}

func (n *Notification) NotificationType() NotificationType {
	return n.notificationType
}

func (n *Notification) SetNotificationType(notificationType NotificationType) {
	n.notificationType = notificationType
}

func (n *Notification) Timestamp() time.Time {
	return n.timestamp
}

func (n *Notification) SetTimestamp(timestamp time.Time) {
	n.timestamp = timestamp
}
