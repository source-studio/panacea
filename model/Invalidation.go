package model

import (
	"gxs3/godfather/model"
	"time"
)

/*
 Represents metadata around an invoice invalidation. Invoices may be decided to be "removed" from the system in the case
 of errors, or handling customer requests. However, invoices aren't meant to be deleted. They can be invalidated. Once
 invalidated, they will not be summed up to total sales, but a record of their existence will be kept.
*/
type Invalidation struct {
	*model.AbstractModel
	idUser    uint      // User that processed the invalidation
	idInvoice uint      // ID of the invoice that was invalidated
	timestamp time.Time // Timestamp when the invalidation occurred
	reason    string    // Reason why the invalidation was made
}

/*
 Creates a new refund object
*/
func NewInvalidation(id, idUser, idInvoice uint, timestamp time.Time, reason string) (*Invalidation, error) {
	if idUser < 1 || idUser < 1 {
		return nil, model.OUT_OF_RANGE
	}

	if idInvoice < 1 || idInvoice < 1 {
		return nil, model.OUT_OF_RANGE
	}

	var invalidation = Invalidation{model.NewModel(id), idUser, idInvoice, timestamp, reason}
	return &invalidation, nil
}

func (i *Invalidation) IdUser() uint {
	return i.idUser
}

func (i *Invalidation) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return model.OUT_OF_RANGE
	}

	i.idUser = idUser
	return nil
}

func (i *Invalidation) IdInvoice() uint {
	return i.idInvoice
}

func (i *Invalidation) SetIdInvoice(idInvoice uint) error {
	if idInvoice < 1 {
		return model.OUT_OF_RANGE
	}

	i.idInvoice = idInvoice
	return nil
}

func (i *Invalidation) Timestamp() time.Time {
	return i.timestamp
}

func (i *Invalidation) SetTimestamp(timestamp time.Time) {
	i.timestamp = timestamp
}

func (i *Invalidation) Reason() string {
	return i.reason
}

func (i *Invalidation) SetReason(reason string) {
	i.reason = reason
}
