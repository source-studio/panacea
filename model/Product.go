package model

import (
	"fmt"
	gm "gxs3/godfather/model"
)

/*
 Model that represents a product that is saved in inventory and is sold
*/
type Product struct {
	*gm.AbstractModel
	brand     string  // Product brand. Company that makes/supplies product
	name      string  // Product name. This includes any specifications about size or type.
	barCode   uint64  // Product bar code. If none is registered, then 0 value is assumed.
	taxRate   float32 // Tax Rate to charge for the product
	price     float32 // Sale price for product
	threshold uint    // Threshold after which alerts will be raised about low inventory. If zero no alerts will be raised
	minimum   uint    // Minimum quantity that should be sold for this product. If zero, no minimum will be enforced
}

/*
 Creates a new product
*/
func NewProduct(id uint, brand string, name string, barCode uint64, taxRate float32, price float32, threshold uint, minimum uint) (*Product, error) {
	if len(brand) < 1 || len(name) < 1 {
		return nil, gm.EMPTY_STRING
	}

	if taxRate < 0 || price < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	var product = Product{gm.NewModel(id), brand, name, barCode, taxRate, price, threshold, minimum}
	return &product, nil
}

/*
 Returns a string representation of the product
*/
func (p *Product) String() string {
	return fmt.Sprintf("%s %s", p.brand, p.name)
}

func (p *Product) Brand() string {
	return p.brand
}

func (p *Product) SetBrand(brand string) error {
	if len(brand) < 1 {
		return gm.EMPTY_STRING
	}

	p.brand = brand
	return nil
}

func (p *Product) Name() string {
	return p.name
}

func (p *Product) SetName(name string) error {
	if len(name) < 1 {
		return gm.EMPTY_STRING
	}

	p.name = name
	return nil
}

func (p *Product) BarCode() uint64 {
	return p.barCode
}

func (p *Product) SetBarCode(barCode uint64) {
	p.barCode = barCode
}

func (p *Product) TaxRate() float32 {
	return p.taxRate
}

func (p *Product) SetTaxRate(taxRate float32) error {
	if taxRate < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	p.taxRate = taxRate
	return nil
}

func (p *Product) Price() float32 {
	return p.price
}

func (p *Product) SetPrice(price float32) error {
	if price < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	p.price = price
	return nil
}

func (p *Product) Threshold() uint {
	return p.threshold
}

func (p *Product) SetThreshold(threshold uint) {
	p.threshold = threshold
}

func (p *Product) Minimum() uint {
	return p.minimum
}

func (p *Product) SetMinimum(minimum uint) {
	p.minimum = minimum
}
