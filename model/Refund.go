package model

import (
	"gxs3/godfather/model"
	"time"
)

/*
 Represents a refund to an invoice. When a refund is performed the invoice service will attempt to perform a transaction
 to reflect any restock of individual items that may occur. This model does not reflect that activity, it is simply an
 invoice level representation that a refund took place, and the details around that
*/
type Refund struct {
	*model.AbstractModel
	idUser     uint       // User that processed the refund
	idInvoice  uint       // Invoice for which the refund is being made
	timestamp  time.Time  // Timestamp when the refund occurred
	calculated float32    // The calculated amount that should be refunded
	actual     float32    // The actual amount that was refunded. This may be different from calculated for several reasons.
	restock    []*Restock // Data on the invoice items that were refunded
}

/*
 Represents an invoice item involved in a refund. Holds data related to the how much of the item is refunded, the value
 refunded, and wether the products were restocked or not
*/
type Restock struct {
	*model.AbstractModel
	idRefund      uint    // ID of the refund this restock is tied to
	idInvoiceItem uint    // ID of the invoice item this restock is tied to
	quantity      float32 // Quantity that was refunded
	value         float32 // Value associated with the refund for this item
	restocked     bool    // If the items returned were restocked or not
}

/*
 Creates a new refund object
*/
func NewRefund(id, idUser, idInvoice uint, timestamp time.Time, calculated, actual float32) (*Refund, error) {
	if idUser < 1 || idInvoice < 1 {
		return nil, model.OUT_OF_RANGE
	}

	if calculated < 0 || actual < 0 {
		return nil, model.NEGATIVE_ARGUMENT
	}

	var refund = Refund{model.NewModel(id), idUser, idInvoice, timestamp, calculated, actual, make([]*Restock, 0)}
	return &refund, nil
}

/*
 Creates a new restock object
*/
func NewRestock(id, idRefund, idInvoiceItem uint, quantity, value float32, restocked bool) (*Restock, error) {
	if idInvoiceItem < 1 {
		return nil, model.OUT_OF_RANGE
	}

	if quantity < 0 || value < 0 {
		return nil, model.NEGATIVE_ARGUMENT
	}

	var model = Restock{model.NewModel(id), idRefund, idInvoiceItem, quantity, value, restocked}
	return &model, nil
}

func (r *Refund) IdUser() uint {
	return r.idUser
}

func (r *Refund) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return model.OUT_OF_RANGE
	}

	r.idUser = idUser
	return nil
}

func (r *Refund) IdInvoice() uint {
	return r.idInvoice
}

func (r *Refund) SetIdInvoice(idInvoice uint) error {
	if idInvoice < 1 {
		return model.OUT_OF_RANGE
	}

	r.idInvoice = idInvoice
	return nil
}

func (r *Refund) Timestamp() time.Time {
	return r.timestamp
}

func (r *Refund) SetTimestamp(timestamp time.Time) {
	r.timestamp = timestamp
}

func (r *Refund) Calculated() float32 {
	return r.calculated
}

func (r *Refund) SetCalculated(calculated float32) error {
	if calculated < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	r.calculated = calculated
	return nil
}

func (r *Refund) Actual() float32 {
	return r.actual
}

func (r *Refund) SetActual(actual float32) error {
	if actual < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	r.actual = actual
	return nil
}

func (r *Refund) Restock() []*Restock {
	return r.restock
}

func (r *Refund) Add(restock *Restock) error {
	if restock == nil {
		return model.NIL_ARGUMENT
	}

	r.restock = append(r.restock, restock)
	return nil
}

func (r *Restock) IdRefund() uint {
	return r.idRefund
}

func (r *Restock) SetIdRefund(idRefund uint) {
	r.idRefund = idRefund
}

func (r *Restock) IdInvoiceItem() uint {
	return r.idInvoiceItem
}

func (r *Restock) SetIdInvoiceItem(idInvoiceItem uint) error {
	if idInvoiceItem < 1 {
		return model.OUT_OF_RANGE
	}

	r.idInvoiceItem = idInvoiceItem
	return nil
}

func (r *Restock) Quantity() float32 {
	return r.quantity
}

func (r *Restock) SetQuantity(quantity float32) error {
	if quantity < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	r.quantity = quantity
	return nil
}

func (r *Restock) Value() float32 {
	return r.value
}

func (r *Restock) SetValue(value float32) error {
	if value < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	r.value = value
	return nil
}

func (r *Restock) Restocked() bool {
	return r.restocked
}

func (r *Restock) SetRestocked(restocked bool) {
	r.restocked = restocked
}
