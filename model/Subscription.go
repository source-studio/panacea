package model

import (
	gm "gxs3/godfather/model"
)

/*
 A subscription represents a user's willingness to receice a type of notification from the application.
 once a user is has subscribed to a notification type, all notifications generated of that type will be put into his/her
 queue. The user can consume those notification at his leasure.
*/
type Subscription struct {
	*gm.AbstractModel
	idUser           uint
	notificationType NotificationType
}

/*
 Creates a new subscription
*/
func NewSubscription(id, idUser uint, notificationType NotificationType) (*Subscription, error) {
	if idUser < 1 {
		return nil, gm.OUT_OF_RANGE
	}

	return &Subscription{gm.NewModel(id), idUser, notificationType}, nil
}

func (s *Subscription) IdUser() uint {
	return s.idUser
}

func (s *Subscription) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return gm.OUT_OF_RANGE
	}

	s.idUser = idUser
	return nil
}

func (s *Subscription) NotificationType() NotificationType {
	return s.notificationType
}

func (s *Subscription) SetNotificationType(notificationType NotificationType) {
	s.notificationType = notificationType
}
