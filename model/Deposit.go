package model

import (
	gm "gxs3/godfather/model"
	"time"
)

/*
 Model representing a deposit. Represents a transaction where money enters the business, but is not the result of a sale
 or payment of a debt by a customer. There are several situations where money is deposited in a business. This is used to
 represent those miscelaneous sitautions where money comes in and needs to be accounted for.
*/
type Deposit struct {
	*gm.AbstractModel
	idUser           uint      // ID of the user that registerd the deposit
	timestamp        time.Time // Date and time when the deposit was made
	reason           string    // Reason or detail explaining why the deposit was made
	amount           float32   // Total amount deposited
	includeInBalance bool      // Does this deposit go into the next balance?
}

/*
 Creates a new deposit
*/
func NewDeposit(id, idUser uint, timestamp time.Time, reason string, amount float32, includeInBalance bool) (*Deposit, error) {
	if idUser < 1 {
		return nil, gm.OUT_OF_RANGE
	}

	if amount < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	return &Deposit{gm.NewModel(id), idUser, timestamp, reason, amount, includeInBalance}, nil
}

func (d *Deposit) IdUser() uint {
	return d.idUser
}

func (d *Deposit) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return gm.OUT_OF_RANGE
	}

	d.idUser = idUser
	return nil
}

func (d *Deposit) Timestamp() time.Time {
	return d.timestamp
}

func (d *Deposit) SetTimestamp(timestamp time.Time) {
	d.timestamp = timestamp
}

func (d *Deposit) Reason() string {
	return d.reason
}

func (d *Deposit) SetReason(reason string) {
	d.reason = reason
}

func (d *Deposit) Amount() float32 {
	return d.amount
}

func (d *Deposit) SetAmount(amount float32) error {
	if amount < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	d.amount = amount
	return nil
}

func (d *Deposit) IncludeInBalance() bool {
	return d.includeInBalance
}

func (d *Deposit) SetIncludeInBalance(includeInBalance bool) {
	d.includeInBalance = includeInBalance
}
