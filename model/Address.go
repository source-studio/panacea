package model

import (
	gm "gxs3/godfather/model"
)

/*
Represents an address for a person or place. This is used in many context. Anywhere
where an address needs to be attached to a customer, institution, sales perso, etc.
*/
type Address struct {
	*gm.AbstractModel
	street1  string // First line of address
	street2  string // Second line of address
	city     string // City
	province string // Province of State
	country  string // Country
}

/*
Creates a new address
*/
func NewAddress(id uint, street1, street2, city, province, country string) (*Address, error) {
	if len(street1) < 1 || len(city) < 1 || len(province) < 1 || len(country) < 1 {
		return nil, gm.EMPTY_STRING
	}

	var address = Address{gm.NewModel(id), street1, street2, city, province, country}
	return &address, nil
}

func (a *Address) Street1() string {
	return a.street1
}

func (a *Address) SetStreet1(street1 string) error {
	if len(street1) < 1 {
		return gm.EMPTY_STRING
	}

	a.street1 = street1
	return nil
}

func (a *Address) Street2() string {
	return a.street2
}

func (a *Address) SetStreet2(street2 string) {
	a.street2 = street2
}

func (a *Address) City() string {
	return a.city
}

func (a *Address) SetCity(city string) error {
	if len(city) < 1 {
		return gm.EMPTY_STRING
	}

	a.city = city
	return nil
}

func (a *Address) Province() string {
	return a.province
}

func (a *Address) SetProvince(province string) error {
	if len(province) < 1 {
		return gm.EMPTY_STRING
	}

	a.province = province
	return nil
}

func (a *Address) Country() string {
	return a.country
}

func (a *Address) SetCountry(country string) error {
	if len(country) < 1 {
		return gm.EMPTY_STRING
	}

	a.country = country
	return nil
}
