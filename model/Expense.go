package model

import (
	gm "gxs3/godfather/model"
	"time"
)

/*
 Model that represents a company expense. Usually happens when money has to be taken out of the cashier to pay for something
 for the business. This relates to buying from suppliers, or purchasing goods to keep the business running.
*/
type Expense struct {
	*gm.AbstractModel
	idUser           uint      // ID of the user that registerd the expense
	timestamp        time.Time // Date and time when the expense was incurred
	reason           string    // Reason or detail explaining why the expense was incurred
	amount           float32   // Total amount the expense cost
	includeInBalance bool      // Has this expense been taken directly from the cashier, and included in the balance.
}

/*
 Creates a new expense
*/
func NewExpense(id, idUser uint, timestamp time.Time, reason string, amount float32, includeInBalance bool) (*Expense, error) {
	if idUser < 1 {
		return nil, gm.OUT_OF_RANGE
	}

	if amount < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	var e = Expense{gm.NewModel(id), idUser, timestamp, reason, amount, includeInBalance}
	return &e, nil
}

func (e *Expense) IdUser() uint {
	return e.idUser
}

func (e *Expense) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return gm.OUT_OF_RANGE
	}

	e.idUser = idUser
	return nil
}

func (e *Expense) Timestamp() time.Time {
	return e.timestamp
}

func (e *Expense) SetTimestamp(timestamp time.Time) {
	e.timestamp = timestamp
}

func (e *Expense) Reason() string {
	return e.reason
}

func (e *Expense) SetReason(reason string) {
	e.reason = reason
}

func (e *Expense) Amount() float32 {
	return e.amount
}

func (e *Expense) SetAmount(amount float32) error {
	if amount < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	e.amount = amount
	return nil
}

func (e *Expense) IncludeInBalance() bool {
	return e.includeInBalance
}

func (e *Expense) SetIncludeBalance(includeInBalance bool) {
	e.includeInBalance = includeInBalance
}
