package model

import (
	"gxs3/godfather/model"
	"time"
)

/*
 Inventory or stock of a product in existence. Usually entries are created when a batch of stock is acquired and made
 available for sale or distribution. There can and should be multiple inventory entries for a single product. Product
 batches are bought at different times, and as such can have different details like date entered, expiration date, and
 cost. The system will assume a FIFO (first in, first out) order of products sold. That is, the oldest products should
 always be sold first. So when having multiple inventory entries for a product, it will always be assumed that the
 oldest will be sold first.
*/
type Inventory struct {
	*model.AbstractModel
	idProduct  uint       // ID of the product this inventory entry is keeping track of
	quantity   float32    // Number of items of the product that were originally entered into inventory
	remaining  float32    // Number of items of the product in this inventory batch that are still in existence
	entered    time.Time  // Date the inventory entry was entered
	expiration *time.Time // Expiration date for the product
	cost       float32    // Cost, per unit, of the product
}

/*
 Creates a new inventory entry for a product.
*/
func NewInventory(id, idProduct uint, quantity, remaining float32, entered time.Time, expiration *time.Time, cost float32) (*Inventory, error) {
	if idProduct < 1 {
		return nil, model.OUT_OF_RANGE
	}

	if cost < 0 || quantity < 0 || remaining < 0 {
		return nil, model.NEGATIVE_ARGUMENT
	}

	var inventory = Inventory{model.NewModel(id), idProduct, quantity, remaining, entered, expiration, cost}
	return &inventory, nil
}

func (i *Inventory) IdProduct() uint {
	return i.idProduct
}

func (i *Inventory) SetIdProduct(idProduct uint) error {
	if idProduct < 1 {
		return model.OUT_OF_RANGE
	}

	i.idProduct = idProduct
	return nil
}

func (i *Inventory) Quantity() float32 {
	return i.quantity
}

func (i *Inventory) SetQuantity(quantity float32) error {
	if quantity < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	i.quantity = quantity
	return nil
}

func (i *Inventory) Remaining() float32 {
	return i.remaining
}

func (i *Inventory) SetRemaining(remaining float32) error {
	if remaining < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	i.remaining = remaining
	return nil
}

func (i *Inventory) Entered() time.Time {
	return i.entered
}

func (i *Inventory) SetEntered(entered time.Time) {
	i.entered = entered
}

func (i *Inventory) Expiration() *time.Time {
	return i.expiration
}

func (i *Inventory) SetExpiration(expiration *time.Time) {
	i.expiration = expiration
}

func (i *Inventory) Cost() float32 {
	return i.cost
}

func (i *Inventory) SetCost(cost float32) error {
	if cost < 0 {
		return model.NEGATIVE_ARGUMENT
	}

	i.cost = cost
	return nil
}
