package model

import (
	gm "gxs3/godfather/model"
	"time"
)

/*
 Model representing a cash balance. Cash balances are usually done at the end of the day or cashier shift. It is used to verify
 that the current transactions and cash at hand matches up. If it does not the balance, will reflect that and will provide
 information about the difference.
*/
type Balance struct {
	*gm.AbstractModel
	idUser      uint      // User that generated this balance report
	timestamp   time.Time // Moment in time when this report was generated
	start       float32   // Amount of money that was left available to make transactions at the begining of the time period
	cashSales   float32   // Total amount of sales made in cash
	creditSales float32   // Total amount of sales made in credit
	refunds     float32   // Total value of refunds made
	payments    float32   // Total amount that was received in payments
	expenses    float32   // Total amount that was spent as expenses
	deposits    float32   // Total amount of money that was deposited. This is outside of regular sale transactions
	nextStart   float32   // Amount of money that will be left for the next period
	balance     float32   // Cash balance after all calculations
	difference  float32   // Amount of money over or under which
	notes       string    // Optional user inputted notes that provide explanation or detail about the balance
}

/*
 Creates a new cash balance
*/
func NewBalance(id, idUser uint, timestamp time.Time, start, cashSales, creditSales, refunds, payments, expenses, deposits, nextStart, balance, difference float32, notes string) (*Balance, error) {
	if idUser < 1 {
		return nil, gm.OUT_OF_RANGE
	}

	if start < 0 || cashSales < 0 || creditSales < 0 || refunds < 0 || payments < 0 || expenses < 0 || deposits < 0 || nextStart < 0 {
		return nil, gm.NEGATIVE_ARGUMENT
	}

	var b = Balance{gm.NewModel(id), idUser, timestamp, start, cashSales, creditSales, refunds, payments, expenses,
		deposits, nextStart, balance, difference, notes}
	return &b, nil
}

func (b *Balance) IdUser() uint {
	return b.idUser
}

func (b *Balance) SetIdUser(idUser uint) error {
	if idUser < 1 {
		return gm.OUT_OF_RANGE
	}

	b.idUser = idUser
	return nil
}

func (b *Balance) Timestamp() time.Time {
	return b.timestamp
}

func (b *Balance) SetTimestamp(timestamp time.Time) {
	b.timestamp = timestamp
}

func (b *Balance) Start() float32 {
	return b.start
}

func (b *Balance) SetStart(start float32) error {
	if start < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.start = start
	return nil
}

func (b *Balance) CashSales() float32 {
	return b.cashSales
}

func (b *Balance) SetCashSales(cashSales float32) error {
	if cashSales < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.cashSales = cashSales
	return nil
}

func (b *Balance) CreditSales() float32 {
	return b.creditSales
}

func (b *Balance) SetCreditSales(creditSales float32) error {
	if creditSales < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.creditSales = creditSales
	return nil
}

func (b *Balance) Refunds() float32 {
	return b.refunds
}

func (b *Balance) SetRefunds(refunds float32) error {
	if refunds < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.refunds = refunds
	return nil
}

func (b *Balance) Payments() float32 {
	return b.payments
}

func (b *Balance) SetPayments(payments float32) error {
	if payments < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.payments = payments
	return nil
}

func (b *Balance) Expenses() float32 {
	return b.expenses
}

func (b *Balance) SetExpenses(expenses float32) error {
	if expenses < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.expenses = expenses
	return nil
}

func (b *Balance) Deposits() float32 {
	return b.deposits
}

func (b *Balance) SetDeposits(deposits float32) error {
	if deposits < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.deposits = deposits
	return nil
}

func (b *Balance) NextStart() float32 {
	return b.nextStart
}

func (b *Balance) SetNextStart(nextStart float32) error {
	if nextStart < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.nextStart = nextStart
	return nil
}

func (b *Balance) Balance() float32 {
	return b.balance
}

func (b *Balance) SetBalance(balance float32) error {
	if balance < 0 {
		return gm.NEGATIVE_ARGUMENT
	}

	b.balance = balance
	return nil
}

func (b *Balance) Difference() float32 {
	return b.difference
}

func (b *Balance) SetDifference(difference float32) {
	b.difference = difference
}

func (b *Balance) Notes() string {
	return b.notes
}

func (b *Balance) SetNotes(notes string) {
	b.notes = notes
}
