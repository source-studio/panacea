#!/usr/bin/tclsh

## TODO: Need to reimplement this script in nodejs

#########################################################################################################
# Google Closure provides a script called closurebuilder.py which is supposed to automate building
# by giving the compiler all the necessary info. Sadly, either it doesn't work, or I'm not smart
# enough to work it. The purpose of this script is to dynamically generate information about files
# and namespaces in the project and give those to closurebuilder.py so the project can be built.
# Again, one would expect closurebuilder.py to do this, but hasn't been possible.
########################################################################################################

set project_home_key 	"--project_home"
set library_home_key 	"--library_home"
set compiler_home_key 	"--compiler_home"
set output_file_key 	"--output_file"
set level_key			"--compilation_level"
set debug_key			"--debug"
set pretty_key			"--pretty_print"
set compress_key		"--compress"
array set argmap		[]

proc showHelp {} {
	puts "Syntax: build $::project_home_key={path/to/project} $::library_home_key={path/to/closure/library}\
		  $::compiler_home_key={path/to/compiler} $::level_key={level} $::pretty_key={state}\
		  $::output_file_key={path/to/output_file}"
	puts ""
	puts "$::project_home_key:  Directory where your project's javascript sources are"
	puts "$::library_home_key:  Directory where the google closure library is located at"
	puts "$::compiler_home_key: Directory where the google closure compiler is located at"
	puts "$::level_key:			Compilation level: \[WHITESPACE_ONLY | SIMPLE_OPTIMIZATIONS | ADVANCED_OPTIMIZATIONS\]"
	puts "							Default value is WHITESPACE_ONLY"
	puts "$::pretty_key:		Pretty printing: \[ON | OFF\]. Default value is ON."
	puts "$::debug_key:			Debug: \[ON | OFF\]. Default value is ON"
	puts "$::output_file_key:   File where compiled output will be written to"
	puts "$::compress_key:		Command to use to compress the file after it's been generated"
	puts ""
	puts "Notes: * Braquets are for notation. They are to be left out when submitting actual values."
	puts "       * There should be NO whitespace between the tags, equals sign and their values"
	puts "       * On Windows, you need to invoke tclsh manually, i.e. tclsh build {args}"
}

proc getValue { arg } {
	set result [split $arg =]
	return [lindex $result 1]
}

proc verify { key required {default ""} } {
	upvar argmap map
	set names [array names map]
	if { [lsearch -exact $names $key] < 0 && $required && [string length $default] == 0} {
		puts "Error: $key not provided."
		showHelp
		exit
	} elseif { [lsearch -exact $names $key] < 0 } {
		set map($key) $default
	}
}

proc execute { command arguments } {
	set commandstr "exec \$command "
	set size [llength $arguments]

	for {set cont 0} { $cont <  $size} {incr cont} {
		set commandstr [concat $commandstr " \[lindex \$arguments $cont\]"]
	}

	if { [catch { eval $commandstr } e ] } {
		puts $e
	}
}

foreach arg $argv {
	set parts [split $arg =]
	set argmap([lindex $parts 0]) [lindex $parts 1]
}

verify $project_home_key 1
verify $library_home_key 1
verify $compiler_home_key 1
verify $level_key 0 "WHITESPACE_ONLY"
verify $pretty_key 0 "ON"
verify $debug_key 0 "ON"
verify $output_file_key 1
verify $compress_key 0

set files ""

if { [info exists env(OS)] } {
	set WINDOWS [expr [string first "Windows" $env(OS)] != -1]
} else {
	set WINDOWS 0
}

if { $WINDOWS } {
	set win_project_home [string map {/ \\} $argmap($project_home_key)]
	set files [exec $env(windir)\\System32\\cmd.exe /c dir /b /s $win_project_home | findstr \\.js$]
	set files [split $files \n]
} else {
	set files [exec find $argmap($project_home_key) | grep \\.js$]
}

set namespaces [list]

set args [list "--root=$argmap($library_home_key)" \
			   "--root=$argmap($project_home_key)" \
			   "--output_mode=compiled" \
			   "--compiler_jar=$argmap($compiler_home_key)/compiler.jar" \
			   "--compiler_flags=--compilation_level=$argmap($level_key)" \
			   "--output_file=$argmap($output_file_key)"]

if { $argmap($pretty_key) == "ON" } {
	lappend args "--compiler_flags=--formatting=PRETTY_PRINT"
}

if { $argmap($debug_key) == "ON" } {
	lappend args "--compiler_flags=--debug"
}

set command "" 
set grepcmd ""

if { $WINDOWS } {
	set grepcmd "findstr" 
	set command "python"
	set args [linsert $args 0 "$argmap($library_home_key)/closure/bin/build/closurebuilder.py"]
} else {
	set grepcmd "grep" 
	set command "$argmap($library_home_key)/closure/bin/build/closurebuilder.py"
}

foreach file $files {
	lappend args "--input=$file"
	set provides ""
	
	if { [catch { set provides [exec $grepcmd "goog\\.provide" $file] } e] } {
		puts "Error: No goog.provide() declaration in file '$file'"
		exit
	}
	
	set provides [split $provides "\n"]

	if { [llength $provides] < 0} {
		continue
	}

	foreach provide $provides {
		set start [string first "\"" $provide]
		set end [string last "\"" $provide]

		if { $start == -1 } {
			set start [string first "'" $provide]
			set end [string last "'" $provide]
		}

		set start [expr $start + 1]
		set end [expr $end - 1]

		if { $start == -1 } {
			puts "Error: Unrecognized provide '$provide' in file '$file'"
			exit
		} 

		set namespace [string range $provide $start $end]
		lappend args "--namespace=$namespace"
	}
}

execute $command $args

if { $argmap($compress_key) != "" } {
	set compressArgs [split $argmap($compress_key) { }]
	execute [lindex $compressArgs 0] [lrange $compressArgs 1 end]
}
