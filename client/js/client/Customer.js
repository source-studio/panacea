goog.provide('pnc.client.Customer');

goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.string');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Address');
goog.require('pnc.model.Contact');
goog.require('pnc.model.Customer');


/**
 * Service API client to handle customer data requests
 * @constructor
 */
pnc.client.Customer = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Customer.Rel);
};

/**
 * List of Rel descriptors to use in the customer API client
 * @enum {string}
 */
pnc.client.Customer.Rel = {
    GET            : '/rels/pnc.customer',
    GET_COLLECTION : '/rels/pnc.customer-collection',
    SEARCH         : '/rels/pnc.customer-search',
    ADD            : '/rels/pnc.customer-add',
    UPDATE         : '/rels/pnc.customer-update',
    REMOVE         : '/rels/pnc.customer-remove'
};


pnc.client.Customer.prototype.get = function(id, callback) {
    var link    = this.links[pnc.client.Customer.Rel.GET];
    var uri     = link.uri().replace('{id}', id.toString());
    var decoder = goog.partial(pnc.client.decode, pnc.client.Customer.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieve all existing customers
 * @param  {ResponseCallback} callback  Callback to handle response
 */
pnc.client.Customer.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Customer.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Customer.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Searches through the existing customer catalog based on a given search criteria
 * @param  {string}           criteria  Search criteria
 * @param  {ResponseCallback} callback  Callback to handle response
 */
pnc.client.Customer.prototype.search = function(criteria, callback) {
    var link    = this.links[pnc.client.Customer.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Customer.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{name}', goog.string.urlEncode(criteria));
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Add a new customer to the collection
 * @param {pnc.model.Customer} customer  Customer to add
 * @param {ResponseCallback}   callback  Callback to handle response
 */
pnc.client.Customer.prototype.add = function(customer, callback) {
    var link    = this.links[pnc.client.Customer.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Customer.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Customer.encode(customer));
};

/**
 * Update details on an existing customer
 * @param  {pnc.model.Customer} customer  Customer to add
 * @param  {ResponseCallback}   callback  Callback to handle response
 */
pnc.client.Customer.prototype.update = function(customer, callback) {
    var link    = customer.meta.links()[pnc.client.Customer.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Customer.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Customer.encode(customer));
};

/**
 * Remove a customer from the collection
 * @param  {pnc.model.Customer} customer  Customer to remove
 * @param  {ResponseCallback}   callback  Callback to handle response
 */
pnc.client.Customer.prototype.remove = function(customer, callback) {
    var link  = customer.meta.links()[pnc.client.Customer.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes a customer to a serialized representation for transport
 * @param  {pnc.model.Customer} customer  Customer to encode
 * @return {string}                       serialized representation
 */
pnc.client.Customer.encode = function(customer) {
    var contacts = customer.getContacts();
    var address  = customer.getAddress();
    var job      = {
        Id        : customer.getId(),
        FirstName : customer.getFirstName(),
        LastName  : customer.getLastName(),
        NickName  : customer.getNickName(),
        Contacts  : [],
        Address   : null
    };

    for(var cont = 0, size = customer.getContacts().length; cont < size; cont++) {
        var contact    = customer.getContacts()[cont];
        var contactJob = {
            Id          : contact.getId(),
            Uuid        : contact.getUuid(),
            ContactType : contact.getContactType(),
            Value       : contact.getValue()
        };
        job.Contacts.push(contactJob);
    }

    if(goog.isDefAndNotNull(address)) {
        job.Address = {
            Id       : address.getId(),
            Street1  : address.getStreet1(),
            Street2  : address.getStreet2(),
            City     : address.getCity(),
            Province : address.getProvince(),
            Country  : address.getCountry()
        };
    }

    return goog.json.serialize(job);
};

/**
 * Customer decoding logic implemenation
 * @param  {string} rel          Rel descriptor for object
 * @param  {Array}  flatLinks    Json array representing links
 * @param  {Object} model        Json object representing model
 * @return {pnc.model.Customer}  Customer object derived from data
 * @private
 */
pnc.client.Customer.decode = function(rel, flatLinks, model) {
    var links    = pnc.client.Link.fromJsonArray(flatLinks);
    var customer = new pnc.model.Customer(model.Id,
                                          model.FirstName,
                                          model.LastName,
                                          model.NickName,
                                          null, // address
                                          rel, links);

    if(goog.isDefAndNotNull(model.Address)) {
        var address = new pnc.model.Address(model.Address.Id,
                                            model.Address.Street1,
                                            model.Address.Street2,
                                            model.Address.City,
                                            model.Address.Province,
                                            model.Address.Country);
        customer.setAddress(address);
    }

    if(goog.isDefAndNotNull(model.Contacts) && model.Contacts.length > 0) {
        for(var cont = 0; cont < model.Contacts.length; cont++) {
            var contactJob = model.Contacts[cont];
            var contact    = new pnc.model.Contact(contactJob.Id,
                                                   contactJob.Uuid,
                                                   contactJob.ContactType,
                                                   contactJob.Value);
            customer.getContacts().push(contact);
        }
    }

    return customer;
};
