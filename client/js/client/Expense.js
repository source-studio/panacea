goog.provide('pnc.client.Expense');

goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.string');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');


/**
 * Expense API client to perform expense related transactions
 * @constructor
 */
pnc.client.Expense = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Expense.Rel);
};

/**
 * List of rel descriptors to be used in the expense client
 * @enum {string}
 */
pnc.client.Expense.Rel = {
    GET_COLLECTION : '/rels/pnc.expense-collection',
    SEARCH         : '/rels/pnc-expense-search',
    ADD            : '/rels/pnc.expense-add',
    UPDATE         : '/rels/pnc.expense-update',
    REMOVE         : '/rels/pnc.expense-remove'
};


/**
 * Retrieve all available expenses
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Expense.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Expense.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Expense.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves expenses that exists within a given date/time range
 * @param {Date}            from      Initial date/time value in range
 * @param {Date}            to        End date/time value in range
 * @param {number}          user      Optional user ID used to filter the search.
 * @param {ResponseCalback} callback  Response handler
 */
pnc.client.Expense.prototype.search = function(from, to, user, callback) {
    var link    = this.links[pnc.client.Expense.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Expense.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var userStr = goog.isDefAndNotNull(user) ? user.toString() : '';
    from.setHours(0); from.setMinutes(0); from.setSeconds(0);
    to.setHours(23);  to.setMinutes(59);  to.setSeconds(59);
    from    = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    to      = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri = link.uri().replace('{from}', from).replace('{to}', to).replace('{user}', userStr);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Adds a new expense
 * @param {pnc.model.Expense} expense   Expense object to add
 * @param {ResponseCallback}  callback  Response handler
 */
pnc.client.Expense.prototype.add = function(expense, callback) {
    var link    = this.links[pnc.client.Expense.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Expense.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Expense.encode(expense));
};

/**
 * Updates details on an existing expense object
 * @param  {pnc.model.Expense} expense   Expense object to update
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Expense.prototype.update = function(expense, callback) {
    var link    = expense.meta.links()[pnc.client.Expense.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Expense.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Expense.encode(expense));
};

/**
 * Removes an existing expense object
 * @param  {pnc.model.Expense} expense   Expense object to remove
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Expense.prototype.remove = function(expense, callback) {
    var link  = expense.meta.links()[pnc.client.Expense.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes an expense object to a serialized format for transport
 * @param  {pnc.model.Expense} expense  Expense object to serialize
 * @return {string}                     Serialized representation
 */
pnc.client.Expense.encode = function(expense) {
    var job = {
        Id               : expense.getId(),
        IdUser           : expense.getIdUser(),
        Timestamp        : pnc.model.DATE_TIME_FORMATTER.format(expense.getTimestamp()),
        Reason           : expense.getReason(),
        Amount           : expense.getAmount(),
        IncludeInBalance : expense.isIncludedInBalance()
    };

    return goog.json.serialize(job);
};

/**
 * Expense decoding logic implementation
 * @param  {string} rel         Rel descriptor
 * @param  {Array}  flatLinks   Link representation
 * @param  {Object} model       Model representation
 * @return {pnc.model.Expense}  Expense model derived from representaiton
 * @private
 */
pnc.client.Expense.decode = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var expense = new pnc.model.Expense(model.Id,
                                        model.IdUser,
                                        timestamp,
                                        model.Reason,
                                        model.Amount,
                                        model.IncludeInBalance,
                                        rel, links);
    return expense;
};
