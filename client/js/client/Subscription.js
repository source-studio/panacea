goog.provide('pnc.client.Subscription');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Subscription');


/**
 * Subscription service API client
 * @constructor
 */
pnc.client.Subscription = function() {
    this.links = pnc.client.Profile.getLinks(pnc.client.Subscription.Rel);
};

/**
 * Rel descriptors to be used in the subscription client
 * @enum {string}
 * @private
 */
pnc.client.Subscription.Rel = {
    GET         : '/rels/pnc.subscription-get',
    GET_BY_USER : '/rels/pnc.subscription-by-user',
    SYNC        : '/rels/pnc.subscription-sync',
    REMOVE      : '/rels/pnc.subscription-remove'
};


/**
 * Retrieve a single subscription
 * @param  {number}           id        ID of subscription to retrieve
 * @param  {ResponseCallback} callback  Handler to manage server response
 */
pnc.client.Subscription.prototype.get = function(id, callback) {
    var link    = this.links[pnc.client.Subscription.Rel.GET];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Subscription.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    var uri     = link.uri().replace('{id}', id.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieves all existing subscriptions for a user
 * @param  {number}           user      ID of the user for which subscriptions wil be retrieved
 * @param  {ResponseCallback} callback  Handler to manage server response
 */
pnc.client.Subscription.prototype.getByUser = function(user, callback) {
    user        = goog.isNumber(user) ? user : user.getId();
    var link    = this.links[pnc.client.Subscription.Rel.GET_BY_USER];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Subscription.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{user}', user.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Synchronizes the subscriptions held by a user. Removes all existing subscriptions and replaces them with new 
 * ones based on the notification types provided as an argument.
 * @param  {number}           user      ID of theuser for which subscriptions will be retrieved
 * @param  {Array.<string>}   ntypes    Array of notification types. All values in the array must come from the
 *                                        pnc.model.Notification.Type enumeration.
 * @param  {ResponseCallback} callback  Handler to manage server response
 */
pnc.client.Subscription.prototype.sync = function(user, ntypes, callback) {
    user      = goog.isNumber(user) ? user : user.getId();
    var link  = this.links[pnc.client.Subscription.Rel.SYNC];
    var local = pnc.client.createExecCallback(callback);
    var data  = goog.json.serialize(ntypes);
    var uri   = link.uri().replace('{user}', user.toString());
    goog.net.XhrIo.send(uri, local, link.method(), data);
};

/**
 * Removes a subscription
 * @param  {pnc.model.Subscription} subscription  Subscription to remove
 * @param  {ResponseCallback}       callback      Handler to manage server response
 */
pnc.client.Subscription.prototype.remove = function(subscription, callback) {
    var link  = subscription.meta.links()[pnc.client.Subscription.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), lcoal, link.method());
};


/**
 * Subscription decoding implementation logic
 * @param  {string} rel              Rel descriptor
 * @param  {Array}  flatLinks        Link representation
 * @param  {Object} model            Model representation
 * @return {pnc.model.Subscription}  Subscription derived from data
 */
pnc.client.Subscription.decode = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var subscription = new pnc.model.Subscription(model.Id,
                                                  model.IdUser,
                                                  model.NotificationType,
                                                  rel, links);
    return subscription;
};
