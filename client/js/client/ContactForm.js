goog.provide('pnc.ui.ContactForm');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.fx.dom.FadeOutAndHide');
goog.require('goog.fx.Animation');

goog.require('pnc.model.Contact');


/**
 * A dynanamic form used to enter multiple contact items. Generic in nature, can be used to enter contacts for multiple
 * model types. 
 * @param  {Array.<pnc.model.Contact>} contacts  Initial contacts used to populate the form. If none are provided, the form
 *                                               starts up empty.
 * @constructor
 */
pnc.ui.ContactForm = function(contacts) {
    /**
     * Collection of contacts currently populated in the form.
     * @type {Array.<pnc.model.Contact>}
     * @private
     */
    this.contacts = goog.isDefAndNotNull(contacts) ? contacts : [];
    
    /**
     * Main container that holds the form
     * @type {Element}
     * @private
     */
    this.container = null;
    this.containerLabel = null;
};

/**
 * Human readable list of contact types
 * @enum {string}
 * @private
 */
pnc.ui.ContactForm.ContactType = {
    HOME_PHONE   : 'Hogar',
    MOBILE_PHONE : 'Movil',
    WORK_PHONE   : 'Trabajo',
    EMAIL        : 'E-Mail',
    FAX          : 'Fax'
};


/**
 * Renders the contact form
 * @param  {Element} parent  Parent element where the form will be rendered
 */
pnc.ui.ContactForm.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', 'contact-form');
    goog.dom.appendChild(parent, this.container);

    var topRow = goog.dom.createDom('div');
    goog.dom.appendChild(this.container, topRow);

    var addButton = goog.dom.createDom('button', 'btn pull-right');
    goog.dom.appendChild(topRow, addButton);
    var addImg = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS_GRAY, 'title' : 'Agregar Contacto'});
    goog.dom.appendChild(addButton, addImg);

    var contacts = this.contacts.length == 1 ? ' Contacto' : ' Contactos';
    this.counterLabel = goog.dom.createDom('span', {'class' : 'inline pull-right', 'style' : 'margin-top: 4px; min-width: 80px'}, 
        this.contacts.length + contacts);
    goog.dom.appendChild(topRow, this.counterLabel);
    var self = this;

    var addCallback = function() {
        var contact = new pnc.model.Contact();
        self.contacts.push(contact);
        self.renderContact(contact);

        var contacts = self.contacts.length == 1 ? ' Contacto' : ' Contactos';
        goog.dom.setTextContent(self.counterLabel, self.contacts.length + contacts);
    };
    goog.events.listen(addButton, goog.events.EventType.CLICK, addCallback);

    for(var cont = 0, size = this.contacts.length; cont < size; cont++)
        this.renderContact(this.contacts[cont]);
};

/**
 * Disposes the form and releases and resources that may be in use
 */
pnc.ui.ContactForm.prototype.dispose = function() {
    goog.dom.removeNode(this.container);
};

/**
 * Retrieves the form element
 * @return {Element}  form element
 */
pnc.ui.ContactForm.prototype.getElement = function() {
    return this.container;
};

/**
 * Read the form contents and produce a list of contacts 
 * @return {Array.<pnc.model.Contact>}  List of contacts
 */
pnc.ui.ContactForm.prototype.getContacts = function() {
    var rows = goog.dom.getElementsByClass('row', this.container);
    var self = this;

    var find = function(uuid) {
        for(var cont = 0, size = rows.length; cont < size; cont++)
            if(self.contacts[cont].getUuid() === uuid)
                return self.contacts[cont];

        return null;
    };

    for(var cont = 0, size = rows.length; cont < size; cont++) {
        var contact  = find(rows[cont].dataset.contact);
        var typeMenu = goog.dom.getElementsByTagNameAndClass('select', null, rows[cont])[0];
        var input    = goog.dom.getElementsByTagNameAndClass('input', null, rows[cont])[0];

        if(!goog.isDefAndNotNull(input.value) || input.value.length < 1)
            continue;

        contact.setContactType(typeMenu.value);
        contact.setValue(input.value);
    }

    return this.contacts;
};

/**
 * Renders a contact on the form
 * @param  {pnc.model.Contact} contact  Contact to render
 * @private
 */
pnc.ui.ContactForm.prototype.renderContact = function(contact) {
    var row = goog.dom.createDom('div', {'class' : 'row', 'style' : 'margin: 0 0 10px 0', 'data-contact' : contact.getUuid()});
    goog.dom.appendChild(this.container, row);

    var typeMenu = goog.dom.createDom('select', {'data-contact' : contact.getUuid()});
    goog.dom.appendChild(row, typeMenu);

    for(var key in pnc.ui.ContactForm.ContactType) {
        var option = goog.dom.createDom('option', {'value' : key}, pnc.ui.ContactForm.ContactType[key]);
        goog.dom.appendChild(typeMenu, option);

        if(contact.getContactType() === key)
            option.selected = true;
    }

    var input = goog.dom.createDom('input', {'data-contact' : contact.getUuid(), 'type' : 'text', 'style' : 'margin: 0; width: 180px'});
    input.value = contact.getValue();
    goog.dom.appendChild(row, input);

    var removeButton = goog.dom.createDom('button', 'btn');
    goog.dom.appendChild(row, removeButton);
    var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.REMOVE, 'title' : 'Remover contacto'});
    goog.dom.appendChild(removeButton, img);
    var self = this;

    var fadeCallback = function() { 
        goog.dom.removeNode(row);

        for(var cont = 0, size = self.contacts.length; cont < size; cont++) {
            if(self.contacts[cont].getUuid() === contact.getUuid()) {
                goog.array.remove(self.contacts, contact);
                break;
            }
        }

        var contacts = self.contacts.length == 1 ? ' Contacto' : ' Contactos';
        goog.dom.setTextContent(self.counterLabel, self.contacts.length + contacts);
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK) 
            return;

        var fade = new goog.fx.dom.FadeOutAndHide(row, 300);
        goog.events.listen(fade, goog.fx.Animation.EventType.END, fadeCallback);
        fade.play();
    };

    var clickCallback = function(e) {
        var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover ese contacto?');
        dialog.setCallback(selectCallback);
        dialog.open();
    };
    goog.events.listen(removeButton, goog.events.EventType.CLICK, clickCallback);
};
