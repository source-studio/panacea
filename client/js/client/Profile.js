goog.provide('pnc.client.Profile');

goog.require('goog.array');
goog.require('goog.storage.Storage');
goog.require('goog.storage.mechanism.HTML5LocalStorage');

goog.require('pnc.client.Link');

/**
 * A place to store client side data that lives while the user session is active. Data is usually wiped in between page 
 * loads. The profile helps this data be persisted, without having to rely on more expensive operations like passing
 * data in cookies back and forth between the client and server in each transaction. The client keeps it's state.
 * @class
 * @static
 */
pnc.client.Profile = {};

/**
 * Enumeration of keys that the application looks for in the user profile.
 * @enum {string}
 */
pnc.client.Profile.Keys = {
    /**
     * Privileges which the user has access to. This is used in the application to determine which controls are available
     * for the user to use.
     * @type {String}
     */
    PRIVILEGES  : 'pnc.privileges',

    /**
     * Information about the user that is currently logged in to the session.
     * @type {String}
     */
    USER        : 'pnc.user',

    /**
     * Global links that the application uses to start off the interaction with the server.
     * @type {String}
     */
    LINKS       : 'pnc.links'
};

/**
 * Initializes in browser local storage. The objective is to have data cached locally in the browser will the session is
 * active. This avoids more expensive techniques like sending data back and forth through cookies. The application will
 * then read this cached data to make decisions like what elements to display based on privileges.
 */
pnc.client.Profile.init = function() {
    /**
     * Profile storage mechanism
     * @type {goog.storage.mechanism.HTML5LocalStorage}
     * @private
     */
    pnc.client.Profile.mechanism = new goog.storage.mechanism.HTML5LocalStorage();

    /**
     * Profile storage
     * @type {goog.storage.Storage}
     * @private
     */
    pnc.client.Profile.storage   = new goog.storage.Storage(pnc.client.Profile.mechanism);
};

/**
 * Clears out all locally cached data
 */
pnc.client.Profile.clear = function() {
    pnc.client.Profile.mechanism.clear();
};

/**
 * Returns all user privileges that have been saved in profile
 * @return {Array.<pnc.model.Privilege>} List of all saved privileges
 */
pnc.client.Profile.getPrivileges = function() {
    return pnc.client.Profile.storage.get(pnc.client.Profile.Keys.PRIVILEGES);
};

/**
 * Sets the provided user privileges in the profile for later use.
 * @param {Array.<pnc.model.Privilege>} privs Privileges to set
 */
pnc.client.Profile.setPrivileges = function(privs) {
    var keys = [];

    for(var cont = 0, size = privs.length; cont < size; cont++)
        keys.push(privs[cont].getKey());

    goog.array.sort(keys);
    pnc.client.Profile.storage.set(pnc.client.Profile.Keys.PRIVILEGES, keys);
};

/**
 * Retrieves information about the user that is logged on to the current session.
 * @return {pnc.model.User} User that is currently logged on
 */
pnc.client.Profile.getUser = function() {
    var job  = pnc.client.Profile.storage.get(pnc.client.Profile.Keys.USER);
    var user = new pnc.model.User(job.id, job.username, job.password, job.firstName, job.lastName, job.enabled);
    return user;
};

/**
 * Establishes the user that is logged into the current profile for later use.
 * @param {pnc.model.User} user User to set
 */
pnc.client.Profile.setUser = function(user) {
    pnc.client.Profile.storage.set(pnc.client.Profile.Keys.USER, user);
};

/**
 * Searches for a specific link in the collection, and retrieves it if found. Returns null if no link is found with the
 * given key.
 * @param  {string} key      Identifier key for the link search criteria
 * @return {pnc.model.Link}  Link object, if found
 */
pnc.client.Profile.getLink = function(key) {
    var links = pnc.client.Profile.storage.get(pnc.client.Profile.Keys.LINKS);
    var model = null;

    for(var existing in links) {
        if(key === existing) {
            var link = links[key];
            model = new pnc.client.Link(link.contentType_, link.method_, link.uri_);
            break;
        }
    }

    return model;
};

/**
 * [getLinks description]
 * @param  {[type]} keys [description]
 * @return {[type]}      [description]
 */
pnc.client.Profile.getLinks = function(keys) {
    var links  = pnc.client.Profile.storage.get(pnc.client.Profile.Keys.LINKS);
    var values = null;
    var models = {};
    var cont   = 0;

    if(goog.isArray(keys))
        values = keys;
    else if(goog.isObject(keys))
        values = goog.object.getValues(keys);
    else
        return null;

    var size = values.length;

    for(var key in links) {
        if(goog.array.contains(values, key)) {
            var link = links[key];
            models[key] = new pnc.client.Link(link.contentType_, link.method_, link.uri_);

            if(++cont >= size)
                break;
        }
    }

    return models;
};

/**
 * Gets the top level links that have been provided by the server on log on and have been set to the user profile.
 * Links here are treated in the context of REST and HATEOAS. They describe what are the next eligible steps that
 * the client can take in the current state of the application.
 * @return {Array.<pnc.client.Link>} Available links
 */
pnc.client.Profile.getAllLinks = function() {
    var links     = pnc.client.Profile.storage.get(pnc.client.Profile.Keys.LINKS);
    var relModels = {};

    for(var key in links) {
        var link = links[key];
        relModels[key] = new pnc.client.Link(link.contentType_, link.method_, link.uri_);
    }

    return relModels;
};

/**
 * Establishes top level links in the user profile that as provided by the server on logon.
 * Links here are treated in the context of REST and HATEOAS. They describe what are the next eligible steps that
 * the client can take in the current state of the application.
 * @param {Array.<pnc.client.Link>} links Top level links to set
 */
pnc.client.Profile.setLinks = function(links) {
    pnc.client.Profile.storage.set(pnc.client.Profile.Keys.LINKS, links);
};
