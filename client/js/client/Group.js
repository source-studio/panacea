goog.provide('pnc.client.Group');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Group');


/**
 * API client that interfaces with the Group endpoint
 * @constructor
 */
pnc.client.Group = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Group.Rel);
};

/**
 * Rel descriptors to be used by the group client
 * @enum {string}
 */
pnc.client.Group.Rel = {
    GET_COLLECTION : '/rels/gf.group-collection',
    USER_GROUPS    : '/rels/gf.user-groups',
    ADD            : '/rels/gf.group-add',
    UPDATE         : '/rels/gf.group-update',
    REMOVE         : '/rels/gf.group-remove',
    ADD_USER       : '/rels/gf.group-add-user',
    REMOVE_USER    : '/rels/gf.group-remove-user'
};


/**
 * Retrieve all available groups
 * @param  {ResponseCallback} callback Callback function to handle processed response.
 */
pnc.client.Group.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Group.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Group.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve all groups to which a user is subscribed
 * @param  {pnc.model.User} user  User for which groups will be retrieved
 * @param  {ResponseCallback}     callback Callback to handle processed response
 */
pnc.client.Group.prototype.getByUser = function(user, callback) {
    var link    = user.meta.links()[pnc.client.Group.Rel.USER_GROUPS];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Group.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Add a new group
 * @param {pnc.model.Group} group  Group to add
 * @param {ResponseCallback}       callback Callback to handle processed response
 */
pnc.client.Group.prototype.add = function(group, callback) {
    var link    = this.links[pnc.client.Group.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Group.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Group.encode(group));
};

/**
 * Updates an existing group
 * @param  {pnc.model.Group}  group     Group to update
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Group.prototype.update = function(group, callback) {
    var link    = group.meta.links()[pnc.client.Group.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Group.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Group.encode(group));
};

/**
 * Removes an existing group
 * @param  {pnc.model.Group}  group     Group to remove
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Group.prototype.remove = function(group, callback) {
    var link  = group.meta.links()[pnc.client.Group.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Subscribe a user to the group
 * @param {pnc.model.Group}  group     Group to add the user to
 * @param {pnc.model.User}   user      User that will be subscried
 * @param {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Group.prototype.addUser = function(group, user, callback) {
    var link  = group.meta.links()[pnc.client.Group.Rel.ADD_USER];
    var local = pnc.client.createExecCallback(callback);
    var uri   = link.uri().replace('{user}', user.getId().toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Unsubscribe a user from the group
 * @param {pnc.model.Group}  group     Group from which the user will be removed
 * @param {pnc.model.User}   user      User to unsubscribe
 * @param {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Group.prototype.removeUser = function(group, user, callback) {
    var link  = group.meta.links()[pnc.client.Group.Rel.REMOVE_USER];
    var local = pnc.client.createExecCallback(callback);
    var uri   = link.uri().replace('{user}', user.getId().toString());
    goog.net.XhrIo.send(uri, local, link.method());
};


/**
 * Encodes a group to a serialized representation for transport
 * @param  {pnc.model.Group} group  Group to encode
 * @return {string}                 Encoded representation
 */
pnc.client.Group.encode = function(group) {
    var job = {
        Id          : group.getId(),
        Name        : group.getName(),
        Description : group.getDescription()
    };
    
    return goog.json.serialize(job);
};

/**
 * Decoding logic implementation
 * @param  {string} rel        Rel descriptor for object
 * @param  {Array}  flatLinks  Json array representing links
 * @param  {Object} model      Json object representing model
 * @return {pnc.model.Group}   Group object derived from serialized data
 * @private
 */
pnc.client.Group.decode = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var group = new pnc.model.Group(model.Id,
                                    model.Name,
                                    model.Description,
                                    rel, links);
    return group;
};
