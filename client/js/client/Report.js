goog.provide('pnc.client.Report');

goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.string');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Report');


/**
 * Client used to make report requests to the server
 * @constructor
 */
pnc.client.Report = function() {
    /**
     * Global REST links applicable to this client
     * @type {array.<pnc.model.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Report.Rel);
};

/**
 * Collection of Rel identifiers applicable to this client
 * @type {Object}
 */
pnc.client.Report.Rel = {
    STATE                : '/rels/pnc.report-state',
    ACTIVITY             : '/rels/pnc.report-activity',
    ACTIVITY_MONTH       : '/rels/pnc.report-activity-month',
    ACTIVITY_YEAR        : '/rels/pnc.report-activity-year',
    ACTIVITY_INTERVAL    : '/rels/pnc.report-activity-interval',
    INVENTORY            : '/rels/pnc.report-inventory',
    INVENTORY_SUPPLIER   : '/rels/pnc.report-inventory-supplier',
    INVENTORY_EXISTENCE  : '/rels/pnc.report-inventory-existence',
    INVENTORY_EXPIRATION : '/rels/pnc.report-inventory-expiration'
};


/**
 * Generates the data for the state report
 * @param  {ResponseCallback} callback  Handles server response
 */
pnc.client.Report.prototype.generateState = function(callback) {
    var link    = this.links[pnc.client.Report.Rel.STATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Generates the data for the unbounded activity report
 * @param  {date}             from      Initial date in range
 * @param  {date}             to        End date in range
 * @param  {ResponseCallback} callback  Handles server response
 */
pnc.client.Report.prototype.generateActivity = function(from, to, callback) {
    var link    = this.links[pnc.client.Report.Rel.ACTIVITY];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    from.setHours(0); from.setMinutes(0); from.setSeconds(0);
    to.setHours(23);  to.setMinutes(59);  to.setSeconds(59);
    from    = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    to      = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri = link.uri().replace('{from}', from).replace('{to}', to);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Generates the monthly activity report data
 * @param  {number}           month     Month for which the report will be run
 * @param  {number}           year      Year for which the report will be run
 * @param  {ResponseCallback} callback  Handles the server response
 */
pnc.client.Report.prototype.generateActivityMonth = function(from, to, callback) {
    var link    = this.links[pnc.client.Report.Rel.ACTIVITY_MONTH];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    from        = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    to          = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri     = link.uri().replace('{from}', from).replace('{to}', to);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Generates the yearly activity report data
 * @param  {number}           year      Year for which the report will be run
 * @param  {ResponseCallback} callback  Handles the server response
 */
pnc.client.Report.prototype.generateActivityYear = function(from, to, callback) {
    var link    = this.links[pnc.client.Report.Rel.ACTIVITY_YEAR];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    from        = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    to          = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri     = link.uri().replace('{from}', from).replace('{to}', to);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieves the interval of date for which invoice activity is recorded on the server. Provides a bounds in time where
 * data can be expected to be reported on.
 * @param  {ResponseCallback} callback  Handles server response
 */
pnc.client.Report.prototype.getInterval = function(callback) {
    var link    = this.links[pnc.client.Report.Rel.ACTIVITY_INTERVAL];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Generates the supplier report data
 * @param  {ResponseCallback}   callback  Handles the server response
 */
pnc.client.Report.prototype.generateInventory = function(callback) {
    var link    = this.links[pnc.client.Report.Rel.INVENTORY];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Generates the inventory supplier report data
 * @param  {pnc.model.Supplier} supplier  Supplier for which the report will be created
 * @param  {ResponseCallback}   callback  Handles the server response
 */
pnc.client.Report.prototype.generateInventorySupplier = function(supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.Report.Rel.INVENTORY_SUPPLIER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Generates the inventory existence report data
 * @param  {ResponseCallback} callback  Handles the server response
 */
pnc.client.Report.prototype.generateInventoryExistence = function(callback) {
    var link    = this.links[pnc.client.Report.Rel.INVENTORY_EXISTENCE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Generates the inventory expiration report data.
 * @param  {boolean}          expired   Looking for expired products?
 * @param  {number}           days      Number of days until expiration. Only used if the 'expired' argument is false.
 * @param  {ResponseCallback} callback  Handles the server response
 */
pnc.client.Report.prototype.generateInventoryExpiration = function(expired, days, callback) {
    var link    = this.links[pnc.client.Report.Rel.INVENTORY_EXPIRATION];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Report.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    days        = goog.isDefAndNotNull(days) ? days.toString() : '0';
    var uri     = link.uri().replace('{expired}', expired.toString()).replace('{remaining}', days);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Decodes a report 
 * @param  {string} rel        Rel descriptor
 * @param  {Array}  flatLinks  Link array representation
 * @param  {Object} model      Model representation
 * @return {pnc.model.Report}  Report object derived from data
 * @private
 */
pnc.client.Report.decode = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var report = new pnc.model.Report(model.Id,
                                      model.IdUser,
                                      timestamp,
                                      model.ReportType,
                                      model.Arguments,
                                      model.Data,
                                      rel, links);
    return report;
};
