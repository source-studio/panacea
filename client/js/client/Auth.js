goog.provide('pnc.client.Auth');

goog.require('goog.net.XhrIo');
goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');

/**
 * Authentication and authorization client. Used to manage the user's own session and perform login/logout transactions,
 * but also for administrative purposes like working with other accounts, and managing privileges.
 * @constructor
 */
pnc.client.Auth = function() {};

pnc.client.Auth.Rel = {
    SESSION_INFO        : '/rels/gf.session-info',
    RESET_SELF_PASSWORD : '/rels/gf.session-reset-pass',
    LOGOUT              : '/rels/gf.session-logout'
};

/**
 * Logs user in and creates a session
 * @param  {string}   username          User username
 * @param  {string}   password          User password
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Auth.prototype.login = function(username, password, callback) {
    var postdata = ['username=', username, '&password=', password].join();
    var localCallback = function(e) {
        var xhr = e.target;
        var content = {};
        var response = new pnc.client.Response(xhr.getStatus(), content);
        
        if(xhr.getStatus() !== pnc.client.Status.OK)
            return;

        var job   = goog.json.unsafeParse(xhr.getResponseText());
        var user  = pnc.client.User.decode('', job.user);
        var privs = [];

        for(var rel in job.privileges) {
            var priv = pnc.client.Privilege.decode(rel, job.privileges[rel]);
            privs.push(priv);
        }

        var links = pnc.client.Link.fromJsonArray(job.links);

        pnc.client.Profile.setUser(user);
        pnc.client.Profile.setPrivileges(privs);
        pnc.client.Profile.setLinks(links);

        var response = new pnc.client.Response(xhr.getStatus());
        callback(response);
    };
    goog.net.XhrIo.send('/auth/session', localCallback, pnc.client.Method.POST, postdata);
};

/**
 * Logs user out and destroys session
 * @param  {ResponseCallback} callback Callback to handle processed response
 */
pnc.client.Auth.prototype.logout = function(callback) {
    var localCallback = function(e) { 
        var response = new pnc.client.Response(e.target.getStatus());
        callback(response); 
    };
    goog.net.XhrIo.send('/auth/session', localCallback, pnc.client.Method.DELETE);
};

/**
 * Provides session info. Usually used for debugging and diagnostic purposes.
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Auth.prototype.info = function(callback) {
    var localCallback = function(e) {
        var xhr = e.target;
        var job = goog.json.unsafeParse(xhr.getResponseText());
        var response = new pnc.client.Response(xhr.getStatus(), job);
        callback(response);
    };
    goog.net.XhrIo.send('/auth/session', localCallback, pnc.client.Method.GET);
};

/**
 * Resets the password of the current user logged into the active session.
 * @param  {string}   newPassword       New password to establish
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Auth.prototype.resetPassword = function(current, newPassword, callback) {
    var localCallback = function(e) {
        var response = new pnc.client.Response(e.target.getStatus());
        callback(response);
    };
    var postdata = ['current=', current, '&password=', newPassword].join('');
    goog.net.XhrIo.send('/auth/password', localCallback, pnc.client.Method.POST, postdata);
};

/**
 * Grants a privilege to a group.
 * @param  {number}   priv              ID of privilege to be granted
 * @param  {number}   group             ID of group that will receive the privilege
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Auth.prototype.grant = function(priv, group, callback) {
    var localCallback = function(e) {
        var response = new pnc.client.Response(e.target.getStatus());
        callback(response);
    };
    var uri = ['/auth/priv/', priv, '?group=', group].join('');
    goog.net.XhrIo.send(uri, localCallback, pnc.client.Method.POST);
};

/**
 * Revokes a privilege from a group.
 * @param  {number}   priv              ID of the privilege to be granted
 * @param  {number}   group             ID of the group that will be stripped of the privilege
 * @param  {ResponseCallback} callback  Callback to handle processed response
 */
pnc.client.Auth.prototype.revoke = function(priv, group, callback) {
    var localCallback = function(e) {
        var response = new pnc.client.Response(e.target.getStatus());
        callback(response);
    };
    var uri = ['/auth/priv/', priv, '?group=', group].join('');
    goog.net.XhrIo.send(uri, localCallback, pnc.client.Method.DELETE);
};
