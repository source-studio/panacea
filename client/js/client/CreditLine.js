goog.provide('pnc.client.CreditLine');

goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.string');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.CreditLine');
goog.require('pnc.model.Invoice');
goog.require('pnc.model.Payment');


/**
 * API client to access and modify credit line and related data
 * @constructor
 */
pnc.client.CreditLine = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.CreditLine.Rel);
};

/**
 * List of Rel descriptors to be used in the credit line API end point
 * @enum {string}
 */
pnc.client.CreditLine.Rel = {
    GET               : '/rels/pnc.credit-line',
    GET_FROM_CUSTOMER : '/rels/pnc.customer-credit-line',
    GET_FROM_SUPPLIER : '/rels/pnc.supplier-credit-line',
    GET_FROM_SPONSOR  : '/rels/pnc.sponsor-credit-line',
    ADD_TO_CUSTOMER   : '/rels/pnc.customer-credit-line-add',
    ADD_TO_SUPPLIER   : '/rels/pnc.supplier-credit-line-add',
    UPDATE            : '/rels/pnc.credit-line-update',
    REMOVE            : '/rels/pnc.credit-line-remove',
    LINK_INVOICE      : '/rels/pnc.credit-line-link-invoice',
    UNLINK_INVOICE    : '/rels/pnc.credit-line-unlink-invoice',
    ADD_PAYMENT       : '/rels/pnc.credit-line-add-payment',
    UPDATE_PAYMENT    : '/rels/pnc.payment-update',
    REMOVE_PAYMENT    : '/rels/pnc.payment-remove'
};


/**
 * Retrieves a single credit line identified by the given ID
 * @param  {number}           id        ID of the credit line to retrieve
 * @param  {ResponseCallback} callback  Callback to handle response
 */
pnc.client.CreditLine.prototype.get = function(id, callback) {
    var link    = this.links[pnc.client.CreditLine.Rel.GET];
    var uri     = link.uri().replace('{id}', id.toString());
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieve the credit line for a customer if it exists
 * @param  {pnc.model.Customer} customer  Customer for whom the credit line will be retrieved
 * @param  {ResponeCallback}    callback  Callback to handle response
 */
pnc.client.CreditLine.prototype.getByCustomer = function(customer, callback) {
    var link    = customer.meta.links()[pnc.client.CreditLine.Rel.GET_FROM_CUSTOMER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve the credit line for a supplier if it exists
 * @param  {pnc.model.Supplier} supplier  Supplier for whom the credit line will be retrieved
 * @param  {ResponseCallback}   callback  Callback to handle response
 */
pnc.client.CreditLine.prototype.getBySupplier = function(supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.CreditLine.Rel.GET_FROM_SUPPLIER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve the credit line for a sponsor
 * @param  {pnc.model.Sponsor} sponsor   Spnsor for whom the credit line will be retrieved
 * @param  {ResponseCallback}  callback  Callback to handle response
 */
pnc.client.CreditLine.prototype.getBySponsor = function(sponsor, callback) {
    var link    = sponsor.meta.links()[pnc.client.CreditLine.Rel.GET_FROM_SPONSOR];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Add a credit line to a customer
 * @param {pnc.model.CreditLine} creditLine  Credit line to add
 * @param {pnc.model.Customer}   customer    Customer that will have the credit line
 * @param {ResponseCallback}     callback    Callback to handle response
 */
pnc.client.CreditLine.prototype.addToCustomer = function(creditLine, customer, callback) {
    var link    = customer.meta.links()[pnc.client.CreditLine.Rel.ADD_TO_CUSTOMER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.CreditLine.encode(creditLine));
};

/**
 * Add credit line to supplier
 * @param {pnc.model.CreditLine} creditLine  Credit line to add
 * @param {pnc.model.Supplier}   supplier    Supplier that will have credit line
 * @param {Responsecallback}     callback    Callback to handle response
 */
pnc.client.CreditLine.prototype.addToSupplier = function(creditLine, supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.CreditLine.Rel.ADD_TO_SUPPLIER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.CreditLine.encode(creditLine));
};

/**
 * Update details on existing credit line
 * @param  {pnc.model.CreditLine}  creditLine Credit line object to update
 * @param  {ResponseCallback}      callback   Callback to handle response
 */
pnc.client.CreditLine.prototype.update = function(creditLine, callback) {
    var link    = creditLine.meta.links()[pnc.client.CreditLine.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.CreditLine.encode(creditLine));
};

/**
 * Removes a credit line
 * @param  {pnc.model.CreditLine} creditLine  Credit line to remove
 * @param  {ResponseCallback}     callback    Callback to handle response
 */
pnc.client.CreditLine.prototype.remove = function(creditLine, callback) {
    var link  = creditLine.meta.links()[pnc.client.CreditLine.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Links an invoice to a credit line 
 * @param  {pnc.model.CreditLine} creditLine  Credit line that will be linked ot the invoice
 * @param  {number}               idInvoice   ID of the invoice to link
 * @param  {ResponseCallback}     callback    Callback to handle response
 */
pnc.client.CreditLine.prototype.linkInvoice = function(creditLine, idInvoice, callback) {
    var link  = creditLine.meta.links()[pnc.client.CreditLine.Rel.LINK_INVOICE];
    var local = pnc.client.createExecCallback(callback);
    var uri   = link.uri().replace('{invoice}', idInvoice.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Unlink invoice from a credit line
 * @param  {pnc.model.CreditLine} creditLine  Credit line from which the invoice will be unlinked
 * @param  {number}               idInvoice   ID of invoice to unlink
 * @param  {ResponseCallback}     callback    Callback to handle response
 */
pnc.client.CreditLine.prototype.unlinkInvoice = function(creditLine, idInvoice, callback) {
    var link  = creditLine.meta.links()[pnc.client.CreditLine.Rel.UNLINK_INVOICE];
    var local = pnc.client.createExecCallback(callback);
    var uri   = link.uri().replace('{invoice}', idInvoice.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieves all payments performed between the givne date/time ranges
 * @param  {Date}     from      Initial value in date range
 * @param  {Date}     to        End value in date range
 * @param  {Function} callback  Callback to be invoked when server responds
 */
pnc.client.CreditLine.prototype.getPayments = function(from, to, callback) {
    var link    = this.links[pnc.client.Expense.Rel.PAYMENT_BY_RANGE];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.decodePayment);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var from    = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    var to      = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri     = link.uri().replace('{from}', from).replace('{to}', to);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Add a payment to a credit line
 * @param {pnc.model.CreditLine} creditLine  Credit line that will receive payment
 * @param {pnc.model.Payment}    payment     Payment to add
 * @param {ResponseCallback}     callback    Callback to handle response
 */
pnc.client.CreditLine.prototype.addPayment = function(creditLine, payment, callback) {
    var link    = creditLine.meta.links()[pnc.client.CreditLine.Rel.ADD_PAYMENT];
    var decoder = goog.partial(pnc.client.decode, pnc.client.CreditLine.decodePayment);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.CreditLine.encodePayment(payment));
};

/**
 * Update details on an existing payment object
 * @param  {pnc.model.Payment} payment   Payment object to update
 * @param  {ResponseCallback}  callback  Callback to handle response
 */
pnc.client.CreditLine.prototype.updatePayment = function(payment, callback) {
    var link    = payment.meta.links()[pnc.client.CreditLine.Rel.UPDATE_PAYMENT];
    var decoder = goog.partial(pnc.client.decode, pnc.client.decodePayment);
    var local   = pnc.client.createModelCallback(decoder, calback);
    goog.net.XhrIo.send(link.uri(), local, linkMethod(), pnc.client.CreditLine.encodePayment(payment));
};

/**
 * Removes a payment object
 * @param  {pnc.model.payment} payment  Payment object to remove
 * @param  {ResponseCallback}  callback Callback to handle response
 */
pnc.client.CreditLine.prototype.removePayment = function(payment, callback) {
    var link  = payment.meta.links()[pnc.client.CreditLine.Rel.REMOVE_PAYMENT];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes a credit line object to a serialized format for transport
 * @param  {pnc.model.CreditLine} creditLine  Credit line object to encode
 * @return {string}                           Serialized representation
 */
pnc.client.CreditLine.encode = function(creditLine) {
    var job = {
        Id       : creditLine.getId(),
        Limit    : creditLine.getLimit(),
        Balance  : creditLine.getBalance(),
        Active   : creditLine.isActive(),
        Invoices : [],
        Payments : []
    };

    return goog.json.serialize(job);
};

/**
 * Decodes a credit line object representation into a live native object
 * @param  {string} message        Serialized mesasage to decode
 * @return {pnc.model.CreditLine}  Credit line object derived from message
 */
pnc.client.CreditLine.decode = function(rel, flatLinks, model) {
    var links      = pnc.client.Link.fromJsonArray(flatLinks);
    var creditLine = new pnc.model.CreditLine(model.Id,
                                              model.Limit,
                                              model.Balance,
                                              model.Active,
                                              rel, links);

    for(var cont = 0, size = model.Invoices.length; cont < size; cont++) {
        var invoiceJob = model.Invoices[cont];
        var timestamp  = new Date();
        pnc.model.DATE_TIME_PARSER.strictParse(invoiceJob.Timestamp, timestamp);
        var invoice = new pnc.model.Invoice(invoiceJob.Id,
                                            invoiceJob.IdUser,
                                            invoiceJob.Transaction,
                                            invoiceJob.Payment,
                                            invoiceJob.State,
                                            timestamp,
                                            invoiceJob.Total);
        creditLine.getInvoices().push(invoice);
    }

    for(var cont = 0, size = model.Payments.length; cont < size; cont++) {
        var paymentJob   = model.Payments[cont];
        var rel          = goog.object.getKeys(paymentJob)[0];
        var paymentModel = paymentJob[rel].model;
        var links        = pnc.client.Link.fromJsonArray(paymentJob[rel].links);
        var timestamp    = new Date();
        pnc.model.DATE_TIME_PARSER.strictParse(paymentModel.Timestamp, timestamp);
        var payment = new pnc.model.Payment(paymentModel.Id,
                                            paymentModel.IdCreditLine,
                                            paymentModel.IdUser,
                                            timestamp,
                                            paymentModel.Amount,
                                            paymentModel.Note,
                                            rel, links);
        creditLine.getPayments().push(payment);
    }

    return creditLine;
};

/**
 * Encodes a payment object to a serialized format for transport
 * @param  {pnc.model.Payment} payment  Payment object to encode
 * @return {string}                     Serialized representation
 */
pnc.client.CreditLine.encodePayment = function(payment) {
    var job = {
        Id           : payment.getId(),
        IdCreditLine : payment.getIdCreditLine(),
        IdUser       : payment.getIdUser(),
        Timestamp    : pnc.model.DATE_TIME_FORMATTER.format(payment.getTimestamp()),
        Amount       : payment.getAmount(),
        Note         : payment.getNote()
    };

    return goog.json.serialize(job);
};

/**
 * Decodes a payment object representation into a live native object
 * @param  {string} message     Serialized message to decode
 * @return {pnc.model.Payment}  Payment object derived from message
 */
pnc.client.CreditLine.decodePayment = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var payment = new pnc.model.Payment(model.Id,
                                        model.IdCreditLine,
                                        model.IdUser,
                                        timestamp,
                                        model.Amount,
                                        model.Note,
                                        rel, links);
    return payment;
};
