goog.provide('pnc.client');
goog.provide('pnc.client.Link');
goog.provide('pnc.client.Response');

/**
 * Provides functionality to make requests to the server API and process responses. Makes life easier for the UI package,
 * since abstracts most of the networking logic away.
 * @namespace
 * @name pnc.client
 */

/**
 * Response callback function. This function is used so that the UI package can be abstracted of client and networking logic.
 * Any users of the client package will make server requests through it, and will pass this function which will be called
 * when the server responds. The client package may do some pre-processing of results, like parsing results, and checking statuses.
 * @name ResponseCallback
 * @function
 * @param {pnc.client.Response} [callback] Response contains key information that was retried from the server, prmarily
 *                                         the status, and any parsed content.
 */

/**
 * Response decoder function. Used to parse server responses and turn them into native model objects which can be easily
 * processed by the UI and other upstream layers of the application.
 * @name Decoder
 * @function
 * @param  {string} rel        Rel descriptor
 * @param  {Array}  flatLinks  Flat representation of links
 * @param  {Object} model      Json object with raw refund data
 * @return {Object}            Model derived from the message
 */

/**
 * Enumeration of the HTTP status codes that the application will use. The application only uses a subset of all the
 * valid http status codes. For more infomration on status codes please see
 * <a href="http    ://en.wikipedia.org/wiki/HTTP_status" target="_blank">Status Code Reference</a>
 * @enum {number}
 */
pnc.client.Status = {
    /**
     * The server is not responding or is not reachable
     * @type {Number}
     */
    UNREACHABLE                 : 0,

    /**
     * The request was successful
     * @type {Number}
     */
    OK                          : 200,

    /**
     * The request wanted to create a resource, and it was created successfuly.
     * @type {Number}
     */
    CREATED                     : 201,
     
    /**
     * The request made was not understood by the server. Could have been constructed incorrectly, or have bad data in it.
     * @type {Number}
     */
    BAD_REQUEST                 : 400,

    /**
     * The transaction is not authorized. The user does not have the required privilege.
     * @type {Number}
     */
    UNAUTHORIZED                : 401,

    /**
     * The transaction is not allowed to be performed, this is not dependant on privileges.
     * @type {Number}
     */
    FORBIDDEN                   : 403,

    /**
     * The requested resource(s) could not be found
     * @type {Number}
     */
    NOT_FOUND                   : 404,

    /**
     * The method used in the transaction is not allowed to be applied on the resource
     * @type {Number}
     */
    METHOD_NOT_ALLOWED          : 405,

    /**
     * Primarily used to indicate that the user's session has timed out.
     * @type {Number}
     */
    REQUEST_TIMEOUT             : 408,

    /**
     * Indicates that the request could not be processed because of conflict in the request, such as an edit conflict.
     * @type {Number}
     */
    CONFLICT                    : 409,

    /**
     * Error that is raised when a transaction is trying te execute a transaction, but a precondition was not met
     * @type {Number}
     */
    PRECONDITION_FAILED         : 412,
     
    /**
     * Something has gone wrong on the server while attempting to perform the transaction.
     * @type {Number}
     */
    INTERNAL_SERVER_ERROR       : 500,

    /**
     * The server has not implemented the functionality required to serve the request.
     * @type {Number}
     */
    NOT_IMPLEMENTED             : 501,

    /**
     * The server is over burdened with request. It's advised to wait some time and try again.
     * @type {Number}
     */
    SERVICE_UNAVAILABLE         : 503
};

/**
 * Enumeration of methods that will be used to make requests to the server. The application only uses a subset of the
 * valid HTTP methods. For more information on HTTP methods please see
 * <a href="http    ://en.wikipedia.org/wiki/HTTP_method#Request_methods" target="_blank">HTTP Method Reference</a>
 * @enum {string}
 */
pnc.client.Method = {
    /**
     * Retrieve resources
     * @type {String}
     */
    GET         : 'GET',

    /**
     * Create resources
     * @type {String}
     */
    POST        : 'POST',

    /**
     * Update resources. It is expected that the client will send a FULL representation of the object to be updated.
     * @type {String}
     */
    PUT         : 'PUT',

    /**
     * Remove resources
     * @type {String}
     */
    DELETE      : 'DELETE'
};

/**
 * Enumeration with list of privilege keys used in the system
 * @enum {string}
 */
pnc.client.Privileges = {
    USER_VIEW               : 'gf.user-view',
    USER_ADD                : 'gf.user-add',
    USER_UPDATE             : 'gf.user-update',
    USER_REMOVE             : 'gf.user-remove',
    USER_PASSWORD_RESET     : 'gf.user-password-reset',
    USER_NOTIFICATION       : 'pnc.user-notification',
    
    GROUP_VIEW              : 'gf.group-view',
    GROUP_ADD               : 'gf.group-add',
    GROUP_UPDATE            : 'gf.group-update',
    GROUP_REMOVE            : 'gf.group-remove',
    GROUP_SUBSCRIPTION      : 'gf.group-subscription',
     
    PRIVILEGE_VIEW          : 'gf.priv-view',
    PRIVILEGE_GRANT         : 'gf.priv-grant',
    
    NOTIFICATION_VIEW       : 'pnc.notification-view',
    NOTIFICATION_REMOVE     : 'pnc.notification-remove',
    SUBSCRIPTION_VIEW       : 'pnc.subscription-view',
    SUBSCRIPTION_ADD        : 'pnc.subscription-add',
    SUBSCRIPTION_REMOVE     : 'pnc.subscription-remove',
    
    PRODUCT_VIEW            : 'pnc.product-view',
    PRODUCT_ADD             : 'pnc.product-add',
    PRODUCT_UPDATE          : 'pnc.product-update',
    PRODUCT_REMOVE          : 'pnc.product-remove',
    
    INVENTORY_VIEW          : 'pnc.inventory-view',
    INVENTORY_ADD           : 'pnc.inventory-add',
    INVENTORY_UPDATE        : 'pnc.inventory-update',
    INVENTORY_REMOVE        : 'pnc.inventory-remove',
    
    INVOICE_VIEW            : 'pnc.invoice-view',
    INVOICE_SELL            : 'pnc.invoice-sell',
    INVOICE_INVALIDATE      : 'pnc.invoice-invalidate',
    INVOICE_INVALIDATE_UNDO : 'pnc.invoice-invalidate-undo',
    INVOICE_REFUND          : 'pnc.invoice-refund',
    INVOICE_REFUND_UNDO     : 'pnc.invoice-refund-undo',

    BALANCE_VIEW            : 'pnc.balance-view',
    BALANCE_ADD             : 'pnc.balance-add',
    BALANCE_UPDATE          : 'pnc.balance-update',
    BALANCE_REMOVE          : 'pnc.balance-remove',
    
    CREDIT_LINE_VIEW        : 'pnc.credit-line-view',
    CREDIT_LINE_ADD         : 'pnc.credit-line-add',
    CREDIT_LINE_UPDATE      : 'pnc.credit-line-update',
    CREDIT_LINE_REMOVE      : 'pnc.credit-line-remove',
    CREDIT_LINE_INVOICE     : 'pnc.credit-line-invoice',
    CREDIT_LINE_PAYMENT     : 'pnc.credit-line-payment',
    
    CUSTOMER_VIEW           : 'pnc.customer-view',
    CUSTOMER_ADD            : 'pnc.customer-add',
    CUSTOMER_UPDATE         : 'pnc.customer-update',
    CUSTOMER_REMOVE         : 'pnc.customer-remove',
    
    SUPPLIER_VIEW           : 'pnc.supplier-view',
    SUPPLIER_ADD            : 'pnc.supplier-add',
    SUPPLIER_UPDATE         : 'pnc.supplier-update',
    SUPPLIER_REMOVE         : 'pnc.supplier-remove',
    SUPPLIER_PRODUCT        : 'pnc.supplier-product',
    SUPPLIER_ORDER          : 'pnc.supplier-order',
    SUPPLIER_CREDIT_NOTE    : 'pnc.supplier-credit-note',

    ORDER_VIEW              : 'pnc.order-view',
    ORDER_ADD               : 'pnc.order-add',
    ORDER_UPDATE            : 'pnc.order-update',
    ORDER_REMOVE            : 'pnc.order-remove',

    CREDIT_NOTE_VIEW        : 'pnc.credit-note-view',
    CREDIT_NOTE_ADD         : 'pnc.credit-note-add',
    CREDIT_NOTE_UPDATE      : 'pnc.credit-note-update',
    CREDIT_NOTE_REMOVE      : 'pnc.credit-note-remove',

    SPONSOR_VIEW            : 'pnc.sponsor-view',
    SPONSOR_ADD             : 'pnc.sponsor-add',
    SPONSOR_UPDATE          : 'pnc.sponsor-update',
    SPONSOR_REMOVE          : 'pnc.sponsor-remove',
    SPONSOR_SPONSORSHIP     : 'pnc.sponsor-sponsorship',
    
    EXPENSE_VIEW            : 'pnc.expense-view',
    EXPENSE_ADD             : 'pnc.expense-add',
    EXPENSE_UPDATE          : 'pnc.expense-update',
    EXPENSE_REMOVE          : 'pnc.expense-remove',
    
    DEPOSIT_VIEW            : 'pnc.deposit-view',
    DEPOSIT_ADD             : 'pnc.deposit-add',
    DEPOSIT_UPDATE          : 'pnc.deposit-update',
    DEPOSIT_REMOVE          : 'pnc.deposit-remove',

    REPORT_GENERATE         : 'pnc.report-generate'
};

/**
 * Standard date formatter to be used in the client namespace
 */
pnc.client.DATE_FORMATTER = new goog.i18n.DateTimeFormat("yyyy'-'MM'-'dd");

/**
 * Stardard date parser to be used in the client namespace
 */
pnc.client.DATE_PARSER    = new goog.i18n.DateTimeParse("yyyy'-'MM'-'dd");


/**
 * Generic decoder
 * @param  {Decoder} decoder  Decoder with model specific decoding logic
 * @param  {string}  message  Message to decode
 * @return {*}                Decoded model
 */
pnc.client.decode = function(decoder, message) {
    var job = goog.json.unsafeParse(message);
    var rel = goog.object.getKeys(job)[0];
    return decoder(rel, job[rel].links, job[rel].model);
};

/**
 * Generic collection decoder
 * @param  {Decoder} decoder Decoder with model specific decoding logic
 * @param  {string}  message Message to decode
 * @return {Array}           Array with decoded model
 */
pnc.client.decodeCollection = function(decoder, message) {
    var job    = goog.json.unsafeParse(message);
    var models = [];

    for(var rel in job)
        models.push(decoder(rel, job[rel].links, job[rel].model));

    return models;
}

/**
 * Creates a callback that handles an XHR response, decodes the incoming data, and invokes a response handler callback
 * with processed results. Used to handle requests that expect collections as responses.
 * @param  {Decoder}          decoder   Decodes message and provides native models
 * @param  {ResponseCallback} callback  Response callback so that upper layers can process results
 * @return {Function}                   Generated callback
 */
pnc.client.createCollectionCallback = function(decoder, callback) {
    return function(e) {
        var xhr      = e.target;
        var response = new pnc.client.Response(xhr.getStatus());

        if(xhr.getStatus() === pnc.client.Status.OK)
            response.content_ = decoder(xhr.getResponseText());

        callback(response);
    };
};

/**
 * Creates a callback that handles an XHR response, decodes the incoming data, and invokes a response handler callback
 * with processed results. Used to handle requests that expect single model responses.
 * @param  {Function(string)}           decoder   Decodes message and provides a native model
 * @param  {ResponseCallback} callback  Response callback that helps upper layers easily process results
 * @return {Function}                   Generated callback
 */
pnc.client.createModelCallback = function(decoder, callback) {
    return function(e) {
        var xhr = e.target;
        var response = new pnc.client.Response(xhr.getStatus());

        if(xhr.getStatus() === pnc.client.Status.CREATED || xhr.getStatus() === pnc.client.Status.OK)
            response.content_ = decoder(xhr.getResponseText());

        callback(response);
    };
};

/**
 * Creates a callback that handles an XHR response. This type of transaction is not expected to decode any result data,
 * simply to obtain the response code and pass it along to the upper layer.
 * @param  {ResponseCallback} callback  response callback that helps upper layers easily process results.
 * @return {Function}                   Generated callback
 */
pnc.client.createExecCallback = function(callback) {
    return function(e) {
        var response = new pnc.client.Response(e.target.getStatus());
        callback(response);
    };
};

/**
 * Handles errors during XHR evaluation state. This will use an HTTP status code and attmept to match a predefined error
 * category based on it. For example    : If encounted with a 404 error, this method maps that to "The element(s) you have 
 * requested could not be found on the server".
 * @param  {!number} status HTTP status code
 * @return {string} Friendly error message that describes the error condition.
 */
pnc.client.decodeStatus = function(status) {
    if(goog.isDef(status.status) && goog.isFunction(status.status))
        status = status.status();
    else if(goog.isDef(status.getStatus) && goog.isFunction(status.status))
        status = status.getStatus();

    switch(status) {
        case pnc.client.Status.UNREACHABLE:
            return 'El servidor no esta respondiendo o no se puede alcanzar.';

        case pnc.client.Status.BAD_REQUEST:
            return 'La solicitud hecha no fue interpretada correctamente. Esto probablemente es un error en la aplicacion.';

        case pnc.client.Status.UNAUTHORIZED:
            return 'Su sesion ha expirado. Por favor salga y vuelta a entrar a la aplicacion.';

        case pnc.client.Status.FORBIDDEN:
            return 'A usted le esta prohibido a ejecutar esta transaccion.';

        case pnc.client.Status.NOT_FOUND:
            return 'El/los elementos que solicito no fueron encontrados.';

        case pnc.client.Status.METHOD_NOT_ALLOWED:
            return 'El metodo utilizado para hacer la solicitud no es esperado. Esto probablemente es un error en la aplicacion.';

        case pnc.client.Status.REQUEST_TIMEOUT:
            return 'Su sesion se ha expirado. Por favor vuelva a entrar a la aplicacion.';

        case pnc.client.Status.INTERNAL_SERVER_ERROR:
            return 'Error interna de la aplicacion.';

        case pnc.client.Status.NOT_IMPLEMENTED:
            return 'La aplicacion aun no ha implementado la solicitud que usted pide.';

        case pnc.client.Status.SERVICE_UNAVAILABLE:
            return 'La aplicacion esta demasiada ocupada. Por favor espere y vuelva a pedir la solicitud.';

        case pnc.client.Status.CONFLICT:
            return 'Esta transaction no pudo ser procesada por un conflicto de data.';
    }

    return 'Error no categorizado';
};


/**
 * Generic container for response form the server. Used as a bridge between client and ui logic.
 * @constructor
 * @param {number} status  Status code of response
 * @param {Object} content Response content. This is expected to be free form. The UI logic will need to have an expectation
 *                          of what data will be included here.
 */
pnc.client.Response = function(status, content) {
    /**
     * Status code of response
     * @type {number}
     * @private
     */
    this.status_  = status;

    /**
     * Response content. This is expected to be free form. The UI logic will need to have an expectation of what data 
     * will be included here.
     * @type {Object}
     * @private
     */
    this.content_ = content;
};

/**
 * In this context, a link relates to how links are used as hypermedia controls in REST design and architecture. The basic
 * idea is that in a traditional application when logic changes on the server side, the client side needs to adjust to stay
 * in sync. However, if the server, along with the reqeusted information, also tells you what you can do with it, you
 * greatly reduce the need to manually keep client and server in sync. These links are precisely that. Instructions on the
 * next possible actions that can be taken with resources that have been brought down from the server.
 * For more information please see <a href="http    ://martinfowler.com/articles/richardsonMaturityModel.html" target="_blank">
 * Richardson Maturity Model</a>
 * @constructor
 * @param {string} contentType  The type of content that is expected by be recieved when performing this action. Should be
 *                              an HTTP compliant content type like "text/hmtl" or "application/xml".
 * @param {string} method       The HTTP method that needs to be used in order to performed the transaction. Should be an
 *                              HTTP standard method like "GET" or "POST". An enumeration with standard methods is provided
 *                              in {@link pnc.client.Methods}.
 * @param {string} uri          URI that you need to request in order to perform the transaction.
 * @see  pnc.client.Methods
 */
pnc.client.Link = function(contentType, method, uri) {
    /**
      * The type of content that is expected by be recieved when performing this action. Should bean HTTP compliant
      * content type like "text/hmtl" or "application/xml".
      * @type {string}
      * @private
      */
     this.contentType_  = contentType;

    /**
      * The HTTP method that needs to be used in order to performed the transaction. Should be an HTTP standard method
      * like "GET" or "POST". An enumeration with standard methods is provided in {@link pnc.client.Methods}.
      * @type {string}
      * @private
      */
     this.method_       = method;

    /**
      * URI that you need to request in order to perform the transaction.
      * @type {string}
      * @private
      */
    this.uri_           = uri;
};


/**
 * Commonly used keys in response objects
 * @enum {string}
 */
pnc.client.Response.Keys = {
    MODEL          : 'model',
    LINKS          : 'links',
    COLLECTION     : 'colleciton',
    MODEL          : 'model'
};

/**
 * Retrieves the response status
 * @return {string}
 */
pnc.client.Response.prototype.status = function() {
    return this.status_;
};

/**
 * Retrieves the content
 * @return {string}
 */
pnc.client.Response.prototype.content = function() {
    return this.content_;
};


/**
 * Creates a collection of Link objects based on a flat json array representation. Used as a utility when parent objects
 * are deserializing.
 * @param  {!Array.<Object>} array   Flat array of objects
 * @return {Array.<pnc.client.Link>} Array of links derived from the flat array
 */
pnc.client.Link.fromJsonArray = function(array) {
    var links = {};

    for(var cont = 0, size = array.length; cont < size; cont++) {
        var job = array[cont];
        links[job.Rel] = new pnc.client.Link(job.ContentType,
                                             job.Method,
                                             job.Uri);
    }

    return links;
};

/**
 * Get the content type for the link
 * @return {string}
 */
pnc.client.Link.prototype.contentType = function() {
    return this.contentType_;
};

/**
 * Get the HTTP method which the link requires to be used to perform the transaction
 * @return {string}
 * @see  pnc.client.Methods
 */
pnc.client.Link.prototype.method = function() {
    return this.method_;
};

/**
 * Get the URI which needs to be requested to perform the transaction
 * @return {string}
 */
pnc.client.Link.prototype.uri = function() {
    return this.uri_;
};
