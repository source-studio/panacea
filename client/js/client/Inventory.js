goog.provide('pnc.client.Inventory');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Inventory');


/**
 * API client to perform requests on Inventory data
 * @constructor
 */
pnc.client.Inventory = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Inventory.Rel);
};

/**
 * List of rel descriptors to be used in the Inventory client
 * @enum {string}
 */
pnc.client.Inventory.Rel = {
    GET_COLLECTION : '/rels/pnc.inventory-collection',
    GET_BY_PRODUCT : '/rels/pnc.product-inventory',
    ADD            : '/rels/pnc.inventory-add',
    UPDATE         : '/rels/pnc.inventory-update',
    REMOVE         : '/rels/pnc.inventory-remove'
};


/**
 * Retrieves all available inventory items
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Inventory.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Inventory.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Inventory.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all inventory items available for a given product
 * @param  {pnc.model.Product} product   Product for which the inventory will be retrieved
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Inventory.prototype.getByProduct = function(product, callback) {
    var link    = product.meta.links()[pnc.client.Inventory.Rel.GET_BY_PRODUCT];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Inventory.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Adds a new inventory item
 * @param {pnc.model.Inventory} inventory  Inventory item to add
 * @param {ResponseCallback}    callback   Response handler
 */
pnc.client.Inventory.prototype.add = function(inventory, callback) {
    var link    = this.links[pnc.client.Inventory.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Inventory.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Inventory.encode(inventory));
};

/**
 * Updates detail on an existing inventory item
 * @param  {pnc.model.Inventory} inventory  Inventory item to update
 * @param  {ResponseCallback}    callback   Response handler
 */
pnc.client.Inventory.prototype.update = function(inventory, callback) {
    var link    = inventory.meta.links()[pnc.client.Inventory.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Inventory.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Inventory.encode(inventory));
};

/**
 * Removes an existing iventory item
 * @param  {pnc.model.Inventory} inventory  Inventory item to remove
 * @param  {ResponseCallback}    callback   Response handler
 */
pnc.client.Inventory.prototype.remove = function(inventory, callback) {
    var link  = inventory.meta.links()[pnc.client.Inventory.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes an inventory item to a serialized representation for transport
 * @param  {pnc.model.Inventory} inventory  Inventory to encode
 * @return {string}                         Serialized representation
 */
pnc.client.Inventory.encode = function(inventory) {
    var expirationStr = goog.isDefAndNotNull(inventory.getExpiration()) 
                      ? pnc.model.DATE_FORMATTER.format(inventory.getExpiration()) 
                      : null;
    var job = {
        Id         : inventory.getId(),
        IdProduct  : inventory.getIdProduct(),
        Quantity   : inventory.getQuantity(),
        Remaining  : inventory.getRemaining(),
        Entered    : pnc.model.DATE_TIME_FORMATTER.format(inventory.getEntered()),
        Expiration : expirationStr,
        Cost       : inventory.getCost()
    };

    return goog.json.serialize(job);
};

/**
 * Decoding logic implementation.
 * @param  {string} rel           Rel descriptor
 * @param  {Array} flatLinks      Link representation
 * @param  {Object} model         Model representation
 * @return {pnc.model.Inventory}  Inventory item derived from data
 * @private
 */
pnc.client.Inventory.decode = function(rel, flatLinks, model) {
    var links   = pnc.client.Link.fromJsonArray(flatLinks);
    var entered = new Date();
    pnc.model.DATE_PARSER.strictParse(model.Entered, entered);

    var expiration = null;

    if(goog.isDefAndNotNull(model.Expiration) && model.Expiration.length > 0) {
        expiration = new Date();
        pnc.model.DATE_PARSER.strictParse(model.Expiration, expiration);
    }

    var inventory = new pnc.model.Inventory(model.Id,
                                            model.IdProduct,
                                            model.Quantity,
                                            model.Remaining,
                                            entered,
                                            expiration,
                                            model.Cost,
                                            rel, links);
    return inventory;
};
