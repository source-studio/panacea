goog.provide('pnc.client.Notification');

goog.require('goog.json');
goog.require('goog.net.WebSocket');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Notification');

/**
 * Notification service API client
 * @constructor
 */
pnc.client.Notification = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links  = pnc.client.Profile.getLinks(pnc.client.Notification.Rel);

    /**
     * Web socket used to receive server initiated messages with notification data
     * @type {goog.net.WebSocket}
     * @private
     */
    this.socket = new goog.net.WebSocket();
};

/**
 * Rel descriptors to be used in the notification client
 * @enum {string}
 */
pnc.client.Notification.Rel = {
    GET         : '/rels/pnc.notification-get',
    GET_BY_USER : '/rels/pnc.notification-by-user',
    REMOVE      : '/rels/pnc.notification-remove',
    CONNECT     : '/rels/pnc.notification-connect',
    DISCONNECT  : '/rels/pnc.notification-disconnect'
};


/**
 * Used to retrieve a single notification
 * @param  {number}           id        ID of the notification to retrieve
 * @param  {ResponseCallback} callback  Handler to manage server response
 */
pnc.client.Notification.prototype.get = function(id, callback) {
    var link    = this.links[pnc.client.Notification.Rel.GET];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Notification.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    var uri     = link.uri().replace('{id}', id.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieves all existing notifications for the current user that is logged in.
 * @param  {ResponseCallback} callback  Handler to manage server response
 */
pnc.client.Notification.prototype.getByUser = function(callback) {
    var link    = this.links[pnc.client.Notification.Rel.GET_BY_USER];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Notification.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Removes a notification. 
 * @param  {pnc.model.Notification} notification  Notification to remove
 * @param  {ResponseCallback}       callback      Handler to manage server response
 */
pnc.client.Notification.prototype.remove = function(notification, callback) {
    var link  = notification.meta.links()[pnc.client.Notification.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Used to connect to the websocket that will allow the client to receive server initiate messages containing notification
 * data
 */
pnc.client.Notification.prototype.connect = function() {
    if(this.socket.isOpen())
        this.disconnect();

    var link = this.links[pnc.client.Notification.Rel.CONNECT];
    var uri = 'ws://' + window.location.host + link.uri();
    this.socket.open(uri);
};

/**
 * Used to disconnect from the notification websocket
 */
pnc.client.Notification.prototype.disconnect = function() {
    if(goog.isDefAndNotNull(this.socket))
        this.socket.close();
};


/**
 * Notification decoding implementation logic
 * @param  {string} rel              Rel descriptor
 * @param  {Array}  flatLinks        Link representation
 * @param  {Object} model            Model representation
 * @return {pnc.model.Notification}  Notification derived from data
 */
pnc.client.Notification.decode = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.parse(model.Timestamp, timestamp);
    var notification = new pnc.model.Notification(model.Id,
                                                  model.IdSubscription,
                                                  model.IdUser,
                                                  model.IdSource,
                                                  model.NotificationType,
                                                  timestamp,
                                                  rel, links);
    return notification;
};
