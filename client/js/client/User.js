goog.provide('pnc.client.User');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.User');

/**
 * API client that interfaces with the User end point
 * @constructor
 */
pnc.client.User = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.User.Rel);
};


/**
 * Rel descriptors to be used by the user client
 * @enum {string}
 */
pnc.client.User.Rel = {
    GET_COLLECTION   : '/rels/gf.user-collection',
    GET_BY_STATE     : '/rels/gf.user-by-state',
    SEARCH           : '/rels/gf.user-search',
    ADD              : '/rels/gf.user-add',
    UPDATE           : '/rels/gf.user-update',
    UPDATE_PASSWORD  : '/rels/gf.user-password-reset',
    REMOVE           : '/rels/gf.user-remove',
    GET_GROUP_USERS  : '/rels/gf.group-users',
    GET_GROUP_OTHERS : '/rels/gf.group-others'
};


/**
 * Retrieve all available users
 * @param  {ResponseCallback} callback  Calback function to handle processed response
 */
pnc.client.User.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.User.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.User.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves users by their enabled state
 * @param  {boolean}          state     If users retrieves should be enabled/disabled
 * @param  {ResponseCallback} callback  Callback function to handle processed response 
 */
pnc.client.User.prototype.getByState = function(state, callback) {
    var link    = this.links[pnc.client.User.Rel.GET_BY_STATE];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.User.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{state}', state.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieve all users which are subscribed to a given group
 * @param  {pnc.model.Group}  group     Group used as basis of search
 * @param  {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.User.prototype.getInGroup = function(group, callback) {
    var link    = group.meta.links()[pnc.client.User.Rel.GET_GROUP_USERS];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.User.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve all users which have NOT been subscribed to a group
 * @param  {pnc.model.Group}  group     Group used as basis of search
 * @param  {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.User.prototype.getNotInGroup = function(group, callback) {
    var link    = group.meta.links()[pnc.client.User.Rel.GET_GROUP_OTHERS];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.User.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Searches for users which match the given search criteria
 * @param  {string}           criteria  Search criteria
 * @param  {ResponseCallback} callback  Callback function to handle response
 */
pnc.client.User.prototype.search = function(criteria, callback) {
    var link    = this.links[pnc.client.User.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.user.decde);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{name}', criteria);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Adds a new user
 * @param {pnc.model.User}   user      User to add
 * @param {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.User.prototype.add = function(user, callback) {
    var link    = this.links[pnc.client.User.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.User.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.User.encode(user));
};

/**
 * Updates an existing user
 * @param  {pnc.model.User}  user      User to update
 * @param  {ReponseCallback} callback  Callback function to handle processed response
 */
pnc.client.User.prototype.update = function(user, callback) {
    var link    = user.meta.links()[pnc.client.User.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.User.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.User.encode(user));
};

/**
 * Updates the password on a user account
 * @param  {pnc.model.User}   user      User who's password will be updated
 * @param  {string}           password  New password to set on account
 * @param  {ResponseCallback} callback  Callback to handle response
 */
pnc.client.User.prototype.updatePassword = function(user, password, callback) {
    var link     = user.meta.links()[pnc.client.User.Rel.UPDATE_PASSWORD];
    var local    = pnc.client.createExecCallback(callback);
    var postdata = ["password=", password].join('');
    goog.net.XhrIo.send(link.uri(), local, link.method(), postdata)
};

/**
 * Removes a user
 * @param  {pnc.model.User}   user      User to remove
 * @param  {ResponseCallback} callback  Callback function t handle processed response
 */
pnc.client.User.prototype.remove = function(user, callback) {
    var link  = user.meta.links()[pnc.client.User.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes a user to a serialized representation for transport
 * @param  {pnc.model.User} user  User to encode
 * @return {string}               Encoded representation
 */
pnc.client.User.encode = function(user) {
    var job = {
        Id        : user.getId(),
        Username  : user.getUsername(),
        Password  : user.getPassword(),
        FirstName : user.getFirstName(),
        LastName  : user.getLastName(),
        Enabled   : user.isEnabled()
    };

    return goog.json.serialize(job);
};

/**
 * Decoding logic implementation
 * @param  {string} rel        Rel descriptor for object
 * @param  {Array}  flatLinks  Json array representing links
 * @param  {Object} model      Json object representing model
 * @return {pnc.model.User}    User object derived from serialized class
 * @private
 */
pnc.client.User.decode = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var user  = new pnc.model.User(model.Id,
                                   model.Username,
                                   model.Password,
                                   model.FirstName,
                                   model.LastName,
                                   model.Enabled,
                                   rel, links);
    return user;
};
