goog.provide('pnc.client.Privilege');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Privilege');

/**
 * API client that interfaces with the Privilege end point
 * @constructor
 */
pnc.client.Privilege = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Privilege.Rel);
};

/**
 * Rel descriptors to be used by the privilege client
 * @enum {string}
 */
pnc.client.Privilege.Rel = {
    GET_COLLECTION      : '/rels/gf.priv-collection',
    GET_GROUP_GRANTED   : '/rels/gf.group-granted-privs',
    GET_GROUP_UNGRANTED : '/rels/gf.group-ungrated-privs',
    GET_USER_GRANTED    : '/rels/gf.user-granted-privs',
    SYNC_PRIVILEGES     : '/rels/gf.group-sync-privs'
};


/**
 * Retrieve all available privileges
 * @param  {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.Privilege.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Privilege.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Privilege.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve privileges which have been granted to a group
 * @param  {pnc.model.Group}  group     Group for which the request will be made
 * @param  {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.Privilege.prototype.getGroupGranted = function(group, callback) {
    var link    = group.meta.links()[pnc.client.Privilege.Rel.GET_GROUP_GRANTED];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Privilege.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve privilege which have NOT been granted to a group yet
 * @param  {pnc.model.Group}  group     Group for which the request will be made
 * @param  {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.Privilege.prototype.getGroupUngranted = function(group, callback) {
    var link    = group.meta.links()[pnc.client.Privilege.Rel.GET_GROUP_UNGRANTED];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Privilege.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve privilege which have been granted to a user
 * @param  {pnc.model.User}   user      User for which the request will be made
 * @param  {ResponseCallback} callback  Callback function to handle processed response
 */
pnc.client.Privilege.prototype.getUserGranted = function(user, callback) {
    var link    = user.meta.links()[pnc.client.Privilege.Rel.GET_USER_GRANTED];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Privilege.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Synchronizes a groups granted privileges. Only the privileges listed in the request will be available to the group
 * after the request is complete.
 * @param  {pnc.moel.Group}   group     Group for which privileges will be synchronized
 * @param  {Array.<number>}   grants    IDs of the privileges the group will have moving forward
 * @param  {ResponseCallback} callback  Handles server response
 */
pnc.client.Privilege.prototype.sync = function(group, grants, callback) {
    var link  = group.meta.links()[pnc.client.Privilege.Rel.SYNC_PRIVILEGES];
    var local = pnc.client.createExecCallback(callback);
    var data  = goog.json.serialize(grants);
    goog.net.XhrIo.send(link.uri(), local, link.method(), data);
};

/**
 * Decoding logic implementation
 * @param  {string} rel          Rel descriptor for object
 * @param  {Array}  flatLinks    Json array representing links
 * @param  {Object} model        Json object representing model
 * @return {pnc.model.Privilege} Privilege object derived from serialized data
 * @private
 */
pnc.client.Privilege.decode = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var priv  = new pnc.model.Privilege(model.Id,
                                        model.Module,
                                        model.Submodule,
                                        model.Key,
                                        model.Name,
                                        model.Dependencies,
                                        rel, links);
    return priv;
};
