goog.provide('pnc.client.Balance');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Balance');

/**
 * API client that interfaces with the Balance endpoint
 * @constructor
 */
pnc.client.Balance = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Balance.Rel);
};

/**
 * Rel descriptors to be used in the balance client
 * @enum {string}
 */
pnc.client.Balance.Rel = {
    GET_LAST       : '/rels/pnc.balance-last',
    GET_COLLECTION : '/rels/pnc.balance-collection',
    RANGE          : '/rels/pnc.balance-range',
    SEARCH         : '/rels/pnc.balance-search',
    GENERATE       : '/rels/pnc.balance-generate',
    ADD            : '/rels/pnc.balance-add',
    UPDATE         : '/rels/pnc.balance-update',
    REMOVE         : '/rels/pnc.balance-remove'
};


/**
 * Retrieves the last balance that was generated
 * @param  {ResponseCallback} callback  callback to invoke to handle server response
 */
pnc.client.Balance.prototype.getLast = function(callback) {
    var link    = this.links[pnc.client.Balance.Rel.GET_LAST];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Balance.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve all available balances
 * @param  {ResponseCallback} callback Callback to handle processed result
 */
pnc.client.Balance.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Balance.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Balance.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all balances between a date range
 * @param  {Date}             from      From date/time
 * @param  {Date}             to        To date/time
 * @param  {ResponseCallback} callback  Callback to handle processed result
 */
pnc.client.Balance.prototype.getRange = function(from, to, callback) {
    var link    = this.links[pnc.client.Balance.Rel.RANGE];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Balance.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{from}', pnc.model.DATE_TIME_FORMATTER.format(from));
    uri         = uri.replace('{to}', pnc.model.DATE_TIME_FORMATTER.format(to));
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Search from the available balance entries
 * @param  {date}              from     From date/time
 * @param  {date}              to       To date/time
 * @param  {number}            user     Optional user ID to filter the search
 * @param  {ResponseCallback}  callback Callback to handle result
 */
pnc.client.Balance.prototype.search = function(from, to, user, callback) {
    var link    = this.links[pnc.client.Balance.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Balance.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var userStr = goog.isDefAndNotNull(user) ? user.toString() : '';
    from.setHours(0); from.setMinutes(0); from.setSeconds(0);
    to.setHours(23);  to.setMinutes(59);  to.setSeconds(59);
    var uri = link.uri().replace('{from}', pnc.model.DATE_TIME_FORMATTER.format(from));
    uri     = uri.replace('{to}', pnc.model.DATE_TIME_FORMATTER.format(to));
    uri     = uri.replace('{user}', userStr);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Generates a balance. Uses the last available balance and sales and other data to calculate most values for the 
 * next balance. The user will still need to input data in order to complete the balance.
 * @param  {ResponseCallback} callback  Callback to handle processed result
 */
pnc.client.Balance.prototype.generate = function(callback) {
    var link    = this.links[pnc.client.Balance.Rel.GENERATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Balance.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Adds a balance.
 * @param {pnc.model.Balance} balance   Balance object to add
 * @param {ResponseCallback}  callback  Callback to handle processed result
 */
pnc.client.Balance.prototype.add = function(balance, callback) {
    var link    = this.links[pnc.client.Balance.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Balance.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Balance.encode(balance));
};

/**
 * Updates the details on an existing balance.
 * @param  {pnc.model.Balance} balance   Balance object to update
 * @param  {ResponseCallback}  callback  Callback to handle processed result
 */
pnc.client.Balance.prototype.update = function(balance, callback) {
    var link    = balance.meta.links()[pnc.client.Balance.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Balance.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Balance.encode(balance));
};

/**
 * Removes a balance object
 * @param  {pnc.model.Balance} balance   Balance object to remove
 * @param  {ResponseCallback}  callback  Callback to handle processed result
 */
pnc.client.Balance.prototype.remove = function(balance, callback) {
    var link  = balance.meta.links()[pnc.client.Balance.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes a balance object into a serialized representation for transport
 * @param  {pnc.model.Balance} balance  Balance to encode
 * @return {string}                     Encode representation
 */
pnc.client.Balance.encode = function(balance) {
    var job = {
        Id          : balance.getId(),
        IdUser      : balance.getIdUser(),
        Timestamp   : pnc.model.DATE_TIME_FORMATTER.format(balance.getTimestamp()),
        Start       : balance.getStart(),
        CashSales   : balance.getCashSales(),
        CreditSales : balance.getCreditSales(),
        Refunds     : balance.getRefunds(),
        Payments    : balance.getPayments(),
        Expenses    : balance.getExpenses(),
        Deposits    : balance.getDeposits(),
        NextStart   : balance.getNextStart(),
        Balance     : balance.getBalance(),
        Difference  : balance.getDifference(),
        Notes       : balance.getNotes()
    };

    return goog.json.serialize(job);
};

/**
 * Decoding logic implementation
 * @param  {string} rel         Rel descriptor for object
 * @param  {Array}  flatLinks   Json array representing links
 * @param  {Object} model       Json object representing model
 * @return {pnc.model.Balance}  Balance object derived from serialized data
 * @private
 */
pnc.client.Balance.decode = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var balance = new pnc.model.Balance(model.Id,
                                        model.IdUser,
                                        timestamp,
                                        model.Start,
                                        model.CashSales,
                                        model.CreditSales,
                                        model.Refunds,
                                        model.Payments,
                                        model.Expenses,
                                        model.Deposits,
                                        model.NextStart,
                                        model.Balance,
                                        model.Difference,
                                        model.Notes,
                                        rel, links);
    return balance;
};
