goog.provide('pnc.client.Deposit');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Deposit');


/**
 * Deposit service API client
 * @constructor
 */
pnc.client.Deposit = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Deposit.Rel);
};

/**
 * Rel descriptors to use in the deposit client
 * @enum {string}
 */
pnc.client.Deposit.Rel = {
    GET_COLLECTION : '/rels/pnc.deposit-collection',
    SEARCH         : '/rels/pnc.deposit-search',
    ADD            : '/rels/pnc.deposit-add',
    UPDATE         : '/rels/pnc.deposit-update',
    REMOVE         : '/rels/pnc.deposit-remove'
};


/**
 * Retrieve all existing deposits
 * @param  {ResponseCallback} callback  Callback to handle response
 */
pnc.client.Deposit.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Deposit.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Deposit.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrive all deposits within the specified date/time range
 * @param {Date}             from      Date/time range initial value
 * @param {Date}             to        Date/time range end value
 * @param {number}           user      Optional user ID used to filter the search.
 * @param {ResponceCallback} callback  Response handler
 */
pnc.client.Deposit.prototype.search = function(from, to, user, callback) {
    var link    = this.links[pnc.client.Deposit.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Deposit.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var userStr = goog.isDefAndNotNull(user) ? user.toString() : '';
    from.setHours(0); from.setMinutes(0); from.setSeconds(0);
    to.setHours(23);  to.setMinutes(59);  to.setSeconds(59);
    from    = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    to      = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri = link.uri().replace('{from}', from).replace('{to}', to).replace('{user}', userStr);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Add a deposit to the collection
 * @param {pnc.model.Deposit} deposit   Deposit object to add
 * @param {ResponseCallback}  callback  Response handler
 */
pnc.client.Deposit.prototype.add = function(deposit, callback) {
    var link    = this.links[pnc.client.Deposit.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Deposit.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Deposit.encode(deposit));
};

/**
 * Update the details on an existing deposit object
 * @param  {pnc.model.Deposit} deposit   Deposit to update
 * @param  {ResponseCallback}  callback  Response callback
 */
pnc.client.Deposit.prototype.update = function(deposit, callback) {
    var link    = deposit.meta.links()[pnc.client.Deposit.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Deposit.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Deposit.encode(deposit));
};

/**
 * Remove an existing deposit object
 * @param  {pnc.model.Deposit} deposit   Deposit to remove
 * @param  {ResponseCallback}  callback  Response callback
 */
pnc.client.Deposit.prototype.remove = function(deposit, callback) {
    var link  = deposit.meta.links()[pnc.client.Deposit.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encode a deposit object to a serialized representation for transport
 * @param  {pnc.model.Deposit} deposit  Deposit to encode
 * @return {string}                     Serialized representation
 */
pnc.client.Deposit.encode = function(deposit) {
    var job = {
        Id               : deposit.getId(),
        IdUser           : deposit.getIdUser(),
        Timestamp        : pnc.model.DATE_TIME_FORMATTER.format(deposit.getTimestamp()),
        Reason           : deposit.getReason(),
        Amount           : deposit.getAmount(),
        IncludeInBalance : deposit.isIncludedInBalance()
    };

    return goog.json.serialize(job);
};

/**
 * Deposit decoding logic implementation
 * @param  {string} rel         Rel descriptor
 * @param  {Array}  flatLinks   Link array representation
 * @param  {Object} model       Model representation
 * @return {pnc.model.Deposit}  Deposit object derived from data
 * @private
 */
pnc.client.Deposit.decode = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var deposit = new pnc.model.Deposit(model.Id,
                                        model.IdUser,
                                        timestamp,
                                        model.Reason,
                                        model.Amount,
                                        model.IncludeInBalance,
                                        rel, links);
    return deposit;
};
