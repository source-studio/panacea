goog.provide('pnc.client.Supplier');

goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.string');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model');
goog.require('pnc.model.Supplier');


/**
 * Supplier service API client
 * @constructor
 */
pnc.client.Supplier = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Supplier.Rel);
};

/**
 * Rel descriptors to use in the supplier client
 * @enum {string}
 */
pnc.client.Supplier.Rel = {
    GET_COLLECTION        : '/rels/pnc.supplier-collection',
    SEARCH                : '/rels/pnc.supplier-search',
    ADD                   : '/rels/pnc.supplier-add',
    UPDATE                : '/rels/pnc.supplier-update',
    REMOVE                : '/rels/pnc.supplier-remove',
    GET_ORDERS            : '/rels/pnc.supplier-orders',
    GET_ORDERS_BY_PAID    : '/rels/pnc.supplier-orders-by-paid',
    ADD_ORDER             : '/rels/pnc.supplier-order-add',
    UPDATE_ORDER          : '/rels/pnc.order-update',
    REMOVE_ORDER          : '/rels/pnc.order-remove',
    GET_NOTES             : '/rels/pnc.supplier-credit-notes',
    GET_NOTES_BY_REDEEMED : '/rels/pnc.supplier-credit-notes-by-redeemed',
    ADD_NOTE              : '/rels/pnc.credit-note-add',
    UPDATE_NOTE           : '/rels/pnc.credit-note-update',
    REMOVE_NOTE           : '/rels/pnc.credit-note-remove',
    ADD_PRODUCT           : '/rels/pnc.supplier-product-add',
    REMOVE_PRODUCT        : '/rels/pnc.supplier-product-remove'
};


/**
 * Retrieves all suppliers
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Supplier.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Supplier.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Searches through available suppliers
 * @param  {string}           org       Organization search criteria
 * @param  {ResponseCallback} callback  Callback to invoke with response
 */
pnc.client.Supplier.prototype.search = function(org, callback) {
    var link    = this.links[pnc.client.Supplier.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{org}', goog.string.urlEncode(org));
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Adds a supplier to the collection
 * @param {pnc.model.Supplier} supplier  Supplier to add
 * @param {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.add = function(supplier, callback) {
    var link    = this.links[pnc.client.Supplier.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Supplier.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Supplier.encode(supplier));
};

/**
 * Updates the details of an existing supplier
 * @param  {pnc.model.Supplier} supplier  Supplier to update
 * @param  {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.update = function(supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Supplier.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Supplier.encode(supplier));
};

/**
 * Removes a supplier
 * @param  {pnc.model.Supplier} supplier  Supplier to remove
 * @param  {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.remove = function(supplier, callback) {
    var link  = supplier.meta.links()[pnc.client.Supplier.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all existing orders for a supplier
 * @param  {pnc.model.Supplier} supplier  Supplier for whom orders will be retrieved
 * @param  {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.getOrders = function(supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.GET_ORDERS];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decodeOrder);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all existing orders for a supplier by paid state
 * @param  {pnc.model.Supplier} supplier  Supplier for whome orders will be retrieved
 * @param  {boolean}            paid      Paid state to filter orders by
 * @param  {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.getOrdersByPaid = function(supplier, paid, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.GET_ORDERS_BY_PAID];
    var uri     = link.uri().replace('{paid}', paid.toString());
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decodeOrder);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Adds an order to a supplier
 * @param {pnc.model.Supplier} supplier  Supplier to whom the order will be added
 * @param {pnc.model.Order}    order     Order to add
 * @param {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.addOrder = function(supplier, order, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.ADD_ORDER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Supplier.decodeOrder);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Supplier.encodeOrder(order));
};

/**
 * Update the details of an existing order
 * @param  {pnc.model.Order}  order     Order to update
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Supplier.prototype.updateOrder = function(order, callback) {
    var link    = order.meta.links()[pnc.client.Supplier.Rel.UPDATE_ORDER];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Supplier.decodeOrder);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Supplier.encodeOrder(order));
};

/**
 * Remove an order
 * @param  {pnc.model.Order}  order    Order to remove
 * @param  {ResponseCallback} callback Response handler
 */
pnc.client.Supplier.prototype.removeOrder = function(order, callback) {
    var link  = order.meta.links()[pnc.client.Supplier.Rel.REMOVE_ORDER];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all credit notes for a supplier
 * @param  {pnc.model.Supplier} supplier  Supplier for whom credit notes will be retrieved
 * @param  {ResponseCallback}   callback  Response handler
 */
pnc.client.Supplier.prototype.getNotes = function(supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.GET_NOTES];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decodeNote);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves credit notes for a supplier filtered by a redeemed state
 * @param  {pnc.model.Supplier} supplier  Supplier for whom credit notes will be retrieved
 * @param  {boolean}            redeemed  Redeemed state to filter by
 * @param  {ResponseCallback}   callback  Reponse handler
 */
pnc.client.Supplier.prototype.getNotesByRedeemed = function(supplier, redeemed, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.GET_NOTES_BY_REDEEMED];
    var uri     = link.uri().replace('{redeemed}', redeemed.toString());
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decodeNote);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Adds a credit note to a supplier
 * @param {pnc.model.Supplier}   supplier  Supplier to whom the note will added
 * @param {pnc.model.CreditNote} note      Note to add
 * @param {RequestCallback}      callback  Response handler
 */
pnc.client.Supplier.prototype.addNote = function(supplier, note, callback) {
    var link    = supplier.meta.links()[pnc.client.Supplier.Rel.ADD_NOTE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Supplier.decodeNote);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Supplier.encodeNote(note));
};

/**
 * Update the details of a credit note
 * @param  {pnc.model.CreditNote} note      Credit note to update
 * @param  {RequestCallback}      callback  Response handler
 */
pnc.client.Supplier.prototype.updateNote = function(note, callback) {
    var link    = note.meta.links()[pnc.client.Supplier.Rel.UPDATE_NOTE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Supplier.decodeNote);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Supplier.encodeNote(note));
};

/**
 * Remove a credit note
 * @param  {pnc.model.CreditNote}  note      Credit note to remove
 * @param  {RequestCallback}       callback  Response handler
 */
pnc.client.Supplier.prototype.removeNote = function(note, callback) {
    var link  = note.meta.links()[pnc.client.Supplier.Rel.REMOVE_NOTE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Associates a product with a supplier. This indicates that the product can be purchased from this supplier.
 * @param  {pnc.model.Supplier}       supplier  Supplier to which the product will be associated
 * @param  {pnc.model.Product|number} product   Product (or product ID) to associate
 * @param  {ResponseCallback}         callback  Callback to handle transaction results
 */
pnc.client.Supplier.prototype.addProduct = function(supplier, product, callback) {
    var link  = supplier.meta.links()[pnc.client.Supplier.Rel.ADD_PRODUCT];
    var id    = goog.isNumber(product) ? product : product.getId();
    var local = pnc.client.createExecCallback(callback);
    var uri   = link.uri().replace('{product}', id.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Disassociates a product from the supplier. Indicates that the product will no longer be bought from that supplier.
 * @param  {pnc.model.Supplier}       supplier  Supplier to which the product will be disassociated
 * @param  {pnc.model.Product|number} product   Product (or product ID) to remove
 * @param  {ResponseCallback}         callback  Callback to handle transaction results
 */
pnc.client.Supplier.prototype.removeProduct = function(supplier, product, callback) {
    var link  = supplier.meta.links()[pnc.client.Supplier.Rel.REMOVE_PRODUCT];
    var id    = goog.isNumber(product) ? product : product.getId();
    var local = pnc.client.createExecCallback(callback);
    var uri   = link.uri().replace('{product}', id.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};


/**
 * Encodes a supplier to a serialized representation for transport
 * @param  {pnc.model.Supplier} supplier  Supplier to encode
 * @return {string}                       Supplier representation
 */
pnc.client.Supplier.encode = function(supplier) {
    var job = {
        Id           : supplier.getId(),
        Organization : supplier.getOrganization(),
        FirstName    : supplier.getFirstName(),
        LastName     : supplier.getLastName(),
        Contacts     : []
    };

    for(var cont = 0, size = supplier.getContacts().length; cont < size; cont++) {
        var contact = supplier.getContacts()[cont];
        var contactJob = {
            Id          : contact.getId(),
            Uuid        : contact.getUuid(),
            ContactType : contact.getContactType(),
            Value       : contact.getValue()
        };
        job.Contacts.push(contactJob);
    }

    return goog.json.serialize(job);
};

/**
 * Supplier decoding logic implementation
 * @param  {string} rel          Rel descrptor
 * @param  {Array}  flatLinks    Link representations
 * @param  {Object} model        Model reporesentation
 * @return {pnc.model.Supplier}  Supplier model derived from data
 * @private
 */
pnc.client.Supplier.decode = function(rel, flatLinks, model) {
    var links    = pnc.client.Link.fromJsonArray(flatLinks);
    var supplier = new pnc.model.Supplier(model.Id,
                                          model.Organization,
                                          model.FirstName,
                                          model.LastName,
                                          rel, links);

    for(var cont = 0; cont < model.Contacts.length; cont++) {
        var contactJob = model.Contacts[cont];
        var contact    = new pnc.model.Contact(contactJob.Id,
                                               contactJob.Uuid,
                                               contactJob.ContactType,
                                               contactJob.Value);
        supplier.getContacts().push(contact);
    }

    return supplier;
};

/**
 * Encodes an order object to a serialized represnetation for transport
 * @param  {pnc.model.Order} order  Order object to encode
 * @return {string}                 Order representation
 */
pnc.client.Supplier.encodeOrder = function(order) {
    var paidDate = goog.isDefAndNotNull(order.getPaidDate())
                 ? pnc.model.DATE_TIME_FORMATTER.format(order.getPaidDate())
                 : null;
    var job = {
        Id         : order.getId(),
        IdSupplier : order.getIdSupplier(),
        Timestamp  : pnc.model.DATE_TIME_FORMATTER.format(order.getTimestamp()),
        Reference  : order.getReference(),
        Amount     : order.getAmount(),
        Paid       : order.isPaid(),
        PaidDate   : paidDate
    };

    return goog.json.serialize(job);
};

/**
 * Order decoding logic implementation
 * @param  {string} rel        Rel descriptor
 * @param  {Array}  flatLinks  Link representations
 * @param  {Object} model      Model representation
 * @return {pnc.model.Order}   Order model derived from data
 * @private
 */
pnc.client.Supplier.decodeOrder = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var paidDate  = null;

    if(goog.isDefAndNotNull(model.PaidDate) && model.PaidDate.length > 0) {
        paidDate = new Date();
        pnc.model.DATE_TIME_PARSER.strictParse(model.PaidDate, paidDate);
    }

    var order = new pnc.model.Order(model.Id,
                                    model.IdSupplier,
                                    timestamp,
                                    model.Reference,
                                    model.Amount,
                                    model.Paid,
                                    paidDate,
                                    rel, links);
    return order;
};

/**
 * Encodes a credit note model to a serialized representation for transport
 * @param  {pnc.model.CreditNote} note  Credit note object to encode
 * @return {string}                     Encoded representation
 */
pnc.client.Supplier.encodeNote = function(note) {
    var redeemedDate = goog.isDefAndNotNull(note.getRedeemedDate())
                     ? pnc.model.DATE_TIME_FORMATTER.format(note.getRedeemedDate())
                     : null;
    var job = {
        Id           : note.getId(),
        IdSupplier   : note.getIdSupplier(),
        Timestamp    : pnc.model.DATE_TIME_FORMATTER.format(note.getTimestamp()),
        Reference    : note.getReference(),
        Amount       : note.getAmount(),
        Reason       : note.getReason(),
        Redeemed     : note.isRedeemed(),
        RedeemedDate : redeemedDate
    };

    return goog.json.serialize(job);
};

/**
 * Credit note decoding logic implementation.
 * @param  {string} rel            Rel descriptor
 * @param  {Array}  flatLinks      Link representations
 * @param  {Object} model          Model representation
 * @return {pnc.model.CreditNote}  Credit note object derived from data
 */
pnc.client.Supplier.decodeNote = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    var redeemedDate = null;

    if(goog.isDefAndNotNull(model.RedeemedDate) && model.RedeemedDate.length > 0) {
        redeemedDate = new Date();
        pnc.model.DATE_TIME_PARSER.strictParse(model.RedeemedDate, redeemedDate);
    }

    var note = new pnc.model.CreditNote(model.Id,
                                        model.IdSupplier,
                                        timestamp,
                                        model.Reference,
                                        model.Amount,
                                        model.Reason,
                                        model.Redeemed,
                                        redeemedDate,
                                        rel, links);
    return note;
};
