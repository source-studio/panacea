goog.provide('pnc.client.ClientSync');

goog.require('goog.object');

pnc.client.ClientSync = function(callback) {
    this.callback  = callback;
    this.requests  = {};
    this.responses = {};
};

pnc.client.ClientSync.prototype.add = function(key, request) {
    this.requests[key] = request;
};

pnc.client.ClientSync.prototype.invoke = function() {
    var size = goog.object.getCount(this.requests);
    var hits = 0;
    var self = this;

    var local = function(key, response) {
        self.responses[key] = response;

        if(++hits < size)
            return;

        self.callback(self.responses);
    };

    for(key in this.requests)
        this.requests[key](goog.partial(local, key));
};
