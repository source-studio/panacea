goog.provide('pnc.client.Product');

goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.string');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Product');


/**
 * Product service API client
 * @constructor
 */
pnc.client.Product = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Product.Rel);
};

/**
 * Rel descriiptors to be used in the product client
 * @enum {string}
 */
pnc.client.Product.Rel = {
    GET                : '/rels/pnc.product',
    GET_COLLECTION     : '/rels/pnc.product-collection',
    GET_BY_SUPPLIER    : '/rels/pnc.supplier-product',
    GET_BY_INVOICE     : '/rels/pnc.invoice-product',
    SEARCH             : '/rels/pnc.product-search',
    SEARCH_BY_SUPPLIER : '/rels/pnc.supplier-product-search',
    SEARCH_BRANDS      : '/rels/pnc.product-search-brands',
    ADD                : '/rels/pnc-product-add',
    UPDATE             : '/rels/pnc.product-update',
    REMOVE             : '/rels/pnc.product-remove'
};


/**
 * Used to retrieve a single product from the server.
 * @param  {number}           id        ID of the product to retrieve
 * @param  {ResponseCallback} callback  Handler to manage server response
 */
pnc.client.Product.prototype.get = function(id, callback) {
    var link    = this.links[pnc.client.Product.Rel.GET];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Product.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    var uri     = link.uri().replace('{id}', id.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieve all available products
 * @param {ResponseCallback}  callback  Response handler
 */
pnc.client.Product.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Product.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Product.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieve all products which were bought by a provided supplier
 * @param  {pnc.model.Supplier} supplier  Supplier for whom products will be retrieved
 * @param  {ResponseCallback}   callback  Response handler
 */
pnc.client.Product.prototype.getBySupplier = function(supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.Product.Rel.GET_BY_SUPPLIER];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Product.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all products that were sold on a given invoice
 * @param  {pnc.model.Invoice} invoice   Invoice to retrieve products for
 * @param  {ResponseCallback}  callback  Callback to handle response
 */
pnc.client.Product.prototype.getByInvoice = function(invoice, callback) {
    var link    = invoice.meta.links()[pnc.client.Product.Rel.GET_BY_INVOICE];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Product.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Searches for products by name and brand. One of the two parameters can be passed as empty, but not both.
 * @param  {string}           name      Name search criteria
 * @param  {string}           brand     Brand search criteria
 * @param  {ResponseCallback} callback  Callback to handle server response
 */
pnc.client.Product.prototype.search = function(name, brand, callback) {
    var link    = this.links[pnc.client.Product.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Product.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    name        = goog.isDefAndNotNull(name) ? goog.string.urlEncode(name) : '';
    brand       = goog.isDefAndNotNull(brand) ? goog.string.urlEncode(brand) : '';
    var uri     = link.uri().replace('{brand}', brand).replace('{name}', name);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Searches for supplier specific products by name and brand. One of the two parameters can be passed as empty, but not both.
 * @param  {string}             name      Name search criteria
 * @param  {string}             brand     Brand search criteria
 * @param  {pnc.model.Supplier} supplier  Supplier for whom products will be searched
 * @param  {ResponseCallback}   callback  Callback to handle server response
 */
pnc.client.Product.prototype.searchBySupplier = function(name, brand, supplier, callback) {
    var link    = supplier.meta.links()[pnc.client.Product.Rel.SEARCH_BY_SUPPLIER];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Product.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    name        = goog.isDefAndNotNull(name) ? goog.string.urlEncode(name) : '';
    brand       = goog.isDefAndNotNull(brand) ? goog.string.urlEncode(brand) : '';
    var uri     = link.uri().replace('{name}', name).replace('{brand}', brand);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Add a product to the collection
 * @param {pnc.model.Product} product   Product to add
 * @param {ResponseCallback}  callback  Response handler
 */
pnc.client.Product.prototype.add = function(product, callback) {
    var link    = this.links[pnc.client.Product.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Product.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Product.encode(product));
};

/**
 * Update the details of an existing product
 * @param  {pnc.model.Product} product   Product to update
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Product.prototype.update = function(product, callback) {
    var link    = product.meta.links()[pnc.client.Product.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Product.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Product.encode(product));
};

/**
 * Remove the product from the collection
 * @param  {pnc.model.Product} product   Product to remove
 * @param  {ResponseCallback}  callback  Respone handler
 */
pnc.client.Product.prototype.remove = function(product, callback) {
    var link  = product.meta.links()[pnc.client.Product.Rel.REMOVE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Encodes a product to a serialized representation for transport
 * @param  {pnc.model.Product} product  Product to serialize
 * @return {string}                     String representation
 */
pnc.client.Product.encode = function(product) {
    var job = {
        Id        : product.getId(),
        Brand     : product.getBrand(),
        Name      : product.getName(),
        BarCode   : product.getBarCode(),
        TaxRate   : product.getTaxRate(),
        Price     : product.getPrice(),
        Threshold : product.getThreshold(),
        Minimum   : product.getMinimum()
    };

    return goog.json.serialize(job);
};

/**
 * Product decoding implementation logic
 * @param  {string} rel         Rel descriptor
 * @param  {Array}  flatLinks   Link representation
 * @param  {Object} model       Model representation
 * @return {pnc.model.Product}  Product derived from data
 */
pnc.client.Product.decode = function(rel, flatLinks, model) {
    var links   = pnc.client.Link.fromJsonArray(flatLinks);
    var product = new pnc.model.Product(model.Id,
                                        model.Brand,
                                        model.Name,
                                        model.BarCode,
                                        model.TaxRate,
                                        model.Price,
                                        model.Threshold,
                                        model.Minimum,
                                        rel, links);
    return product;
};
