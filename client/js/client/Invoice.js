goog.provide('pnc.client.Invoice');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model.Invoice');
goog.require('pnc.model.InvoiceItem');


/**
 * Invoice service API client.
 * @constructor
 */
pnc.client.Invoice = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Invoice.Rel);
};

/**
 * List of rel descriptors to use in the invoice client
 * @enum {string}
 */
pnc.client.Invoice.Rel = {
    GET              : '/rels/pnc.invoice-get',
    GET_COLLECTION   : '/rels/pnc.invoice-collection',
    GET_RANGE        : '/rels/pnc.invoice-range',
    GET_REFUND       : '/rels/pnc.invoice-get-refund',
    GET_INVALIDATION : '/rels/pnc.invoice-get-invalidation',
    SEARCH           : '/rels/pnc.invoice-search',
    SELL             : '/rels/pnc.invoice-sell',
    SELL_CREDIT      : '/rels/pnc.invoice-sell-credit',
    SELL_SPONSORED   : '/rels/pnc.invoice-sell-sponsored',
    INVALIDATE       : '/rels/pnc.invoice-invalidate',
    INVALIDATE_UNDO  : '/rels/pnc.invoice-invalidate-undo',
    REFUND           : '/rels/pnc.invoice-refund',
    REFUND_UNDO      : '/rels/pnc.invoice-refund-undo'
};


/**
 * Retrieves a single invoice
 * @param  {number}           id        ID of the invoice to retrieve
 * @param  {ResponseCallback} callback  Callback to handle processing response
 */
pnc.client.Invoice.prototype.get = function(id, callback) {
    var link    = this.links[pnc.client.Invoice.Rel.GET];
    var uri     = link.uri().replace('{invoice}', id.toString());
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieves all available invoices
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Invoice.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Invoice.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Invoice.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves invoices in provided date range
 * @param  {Date}             from      Initial value in date/time range
 * @param  {Date}             to        End value in date/time range
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Invoice.prototype.getByRange = function(from, to, callback) {
    var link    = this.links[pnc.client.Invoice.Rel.GET_RANGE];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Invoice.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{from}', pnc.model.DATE_TIME_FORMATTER.format(from));
    uri         = uri.replace('{to}', pnc.model.DATE_TIME_FORMATTER.format(to));
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Retrieves a refund for an invoice if such exists
 * @param  {pnc.model.Invoice} invoice   Invoice model for which refund will be retrieved
 * @param  {ResponseCallback}  callback  Callback to handle results
 */
pnc.client.Invoice.prototype.getRefund = function(invoice, callback) {
    var link    = invoice.meta.links()[pnc.client.Invoice.Rel.GET_REFUND];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decodeRefund);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves an invalidation for an invoice if such exists
 * @param  {pnc.model.Invoice} invoice   Invoice model from which the invalidation will be retrieved
 * @param  {ResponseCallback}  callback  Callback to handle results
 */
pnc.client.Invoice.prototype.getInvalidation = function(invoice, callback) {
    var link    = invoice.meta.links()[pnc.client.Invoice.Rel.GET_INVALIDATION];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decodeInvalidation);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Searches invoices. The from and to arguments are required. The user and payment arguments are optional. They will only
 * be used as search criteria if included.
 * @param  {Date}             from         Initial date in range
 * @param  {Date}             to           End date in range
 * @param  {number}           user         User ID to use as search criteria. Optional.
 * @param  {string}           payment      Payment type to use as search criteria. Optional.
 * @param  {string}           transaction  Transaction type search criteria. Optionaal.
 * @param  {ResponseCallback} callback     Callback to process response
 */
pnc.client.Invoice.prototype.search = function(from, to, user, payment, transaction, callback) {
    var link           = this.links[pnc.client.Invoice.Rel.SEARCH];
    var decoder        = goog.partial(pnc.client.decodeCollection, pnc.client.Invoice.decode);
    var local          = pnc.client.createCollectionCallback(decoder, callback);
    var userStr        = goog.isDefAndNotNull(user) ? user.toString() : '';
    var paymentStr     = goog.isDefAndNotNull(payment) && payment !== 'ALL' ? payment : '';
    var transactionStr = goog.isDefAndNotNull(transaction) ? transaction : '';
    from.setHours(0); from.setMinutes(0); from.setSeconds(0);
    to.setHours(23);  to.setMinutes(59);  to.setSeconds(59);
    from    = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(from));
    to      = goog.string.urlEncode(pnc.model.DATE_TIME_FORMATTER.format(to));
    var uri = link.uri().replace('{from}', from);
    uri     = uri.replace('{to}', to);
    uri     = uri.replace('{user}', userStr).replace('{payment}', paymentStr).replace('{transaction}', transactionStr);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Sells an invoice on credit
 * @param  {pnc.model.Invoice}    invoice     Invoice that will be sold
 * @param  {pnc.model.CreditLine} creditLine  Credit line the invoice will be attached to
 * @param  {ResponseCallback}     callback    Response handler
 */
pnc.client.Invoice.prototype.sellCredit = function(invoice, creditLine, callback) {
    var link    = this.links[pnc.client.Invoice.Rel.SELL_CREDIT];
    var id      = goog.isNumber(creditLine) ? creditLine.toString() : creditLine.getId().toString();
    var uri     = link.uri().replace('{credit-line}', id);
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method(), pnc.client.Invoice.encode(invoice));
};

/**
 * Sells a sponsored invoice
 * @param  {pnc.model.Invoice} invoice   Invoice that will be sold
 * @param  {pnc.model.Sponsor} sponsor   Sponsor to whom the invoice will be attached to
 * @param  {number}            amount    Amount being sponsored
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Invoice.prototype.sellSponsored = function(invoice, sponsor, amount, callback) {
    var link    = this.links[pnc.client.Invoice.Rel.SELL_SPONSORED];
    var id      = goog.isNumber(sponsor) ? sponsor.toString() : sponsor.getId().toString();
    var uri     = link.uri().replace('{sponsor}', id).replace('{amount}', amount.toString());
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method(), pnc.client.Invoice.encode(invoice));
};

/**
 * Performs a sale transaction on an invoice.
 * @param  {pnc.model.Invoice} invoice   Invoice to sell
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Invoice.prototype.sell = function(invoice, callback) {
    var link    = this.links[pnc.client.Invoice.Rel.SELL];
    var uri     = link.uri();
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(uri, local, link.method(), pnc.client.Invoice.encode(invoice));
};

/**
 * Invalidates an invoice. This reverse its effect on inventory and other related items. The invoice
 * will not be added sale totals. Essentially "deletes" the invoice, while leaving a record of it's existence.
 * @param  {pnc.model.Invoice} invoice   Invoice to invalidate
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Invoice.prototype.invalidate = function(invoice, invalidation, callback) {
    var link  = invoice.meta.links()[pnc.client.Invoice.Rel.INVALIDATE];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Invoice.encodeInvalidation(invalidation));
};

/**
 * Reverses the actions taken by a previous invalidate action
 * @param  {pnc.model.Invalidation} invalidation  Invalidation to undo
 * @param  {ResponseCallback}       callback      Callback to be invoked to process results
 */
pnc.client.Invoice.prototype.undoInvalidate = function(invalidation, callback) {
    var link  = invalidation.meta.links()[pnc.client.Invoice.Rel.INVALIDATE_UNDO];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Refunds an invoice. Reverse its effect on total sales. The user will select if any of the products sold in the inventory
 * can be restocked. Only those indicated will be used to reverse their respective inventory totals.
 * @param  {pnc.model.Invoice}        invoice   Invoice to refund
 * @param  {pnc.model.Refund}         refund    Refund model
 * @param  {ResponseCallback}         callback  Response handler
 */
pnc.client.Invoice.prototype.refund = function(invoice, refund, callback) {
    var link    = invoice.meta.links()[pnc.client.Invoice.Rel.REFUND];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Invoice.decodeRefund);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Invoice.encodeRefund(refund));
};

/**
 * Reverses the actions performed by a previous refund action
 * @param  {pnc.model.Refund} refund    Refund to undo
 * @param  {ResponseCallback} callback  Callback to process results
 */
pnc.client.Invoice.prototype.undoRefund = function(refund, callback) {
    var link  = refund.meta.links()[pnc.client.Invoice.Rel.REFUND_UNDO];
    var local = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Encodes an invoice to a serialized representation for transport
 * @param  {pnc.model.Invoice} invoice  Invoice to encode
 * @return {string}                     Serialized representation
 */
pnc.client.Invoice.encode = function(invoice) {
    var job = {
        Id          : invoice.getId(),
        IdUser      : invoice.getIdUser(),
        Transaction : invoice.getTransaction(),
        Payment     : invoice.getPayment(),
        State       : invoice.getState(),
        Timestamp   : pnc.model.DATE_TIME_FORMATTER.format(invoice.getTimestamp()),
        Total       : invoice.getTotal(),
        Discount    : invoice.getDiscount(),
        Items       : []
    };

    for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
        var item = invoice.getItems()[cont];
        var itemJob = {
            Id        : item.getId(),
            Uuid      : goog.isDefAndNotNull(item.getUuid()) ? item.getUuid() : '',
            IdInvoice : item.getIdInvoice(),
            IdProduct : item.getIdProduct(),
            Quantity  : item.getQuantity()
        };
        job.Items.push(itemJob);
    }

    return goog.json.serialize(job);
};

/**
 * Decoding logic implementation
 * @param  {string} rel         Rel descriptor
 * @param  {Array}  flatLinks   Link representation
 * @param  {Object} model       Model representation
 * @return {pnc.model.Invoice}  Invoice derived from data
 * @private
 */
pnc.client.Invoice.decode = function(rel, flatLinks, model) {
    var links     = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.parse(model.Timestamp, timestamp);
    var invoice = new pnc.model.Invoice(model.Id,
                                        model.IdUser,
                                        model.Transaction,
                                        model.Payment,
                                        model.State,
                                        timestamp,
                                        model.Total,
                                        model.Discount,
                                        rel, links);
    
    for(var cont = 0; cont < model.Items.length; cont++) {
        var itemJob = model.Items[cont];
        var item    = new pnc.model.InvoiceItem(itemJob.Id,
                                                itemJob.Uuid,
                                                itemJob.IdInvoice,
                                                itemJob.IdProduct,
                                                itemJob.Quantity);
        invoice.getItems().push(item);
    }

    return invoice;
};

/**
 * Encodes a refund object into a serialized representation for transport to the server.
 * @param  {pnc.model.Refund} refund  Refund object to serialize
 * @return {string}                   Serialized representation
 */
pnc.client.Invoice.encodeRefund = function(refund) {
    var job = {
            Id         : refund.getId(),
            IdUser     : refund.getIdUser(),
            IdInvoice  : refund.getIdInvoice(),
            Timestamp  : pnc.model.DATE_TIME_FORMATTER.format(refund.getTimestamp()),
            Calculated : refund.getCalculated(),
            Actual     : refund.getActual(),
            Restock    : []
    };

    for(var cont = 0, size = refund.getRestock().length; cont < size; cont++) {
        var restock = refund.getRestock()[cont];
        var rjob = {
            Id            : restock.getId(),
            IdRefund      : restock.getIdRefund(),
            IdInvoiceItem : restock.getIdInvoiceItem(),
            Quantity      : restock.getQuantity(),
            Value         : restock.getValue(),
            Restocked     : restock.isRestocked()
        };
        job.Restock.push(rjob);
    }

    return goog.json.serialize(job);
};

/**
 * Internal decoding logic for the refund object
 * @param  {string} rel        Rel descriptor
 * @param  {Array}  flatLinks  Flat representation of links
 * @param  {Object} model      Json object with raw refund data
 * @return {pnc.model.Refund}  Refund object derived from data
 * @private
 */
pnc.client.Invoice.decodeRefund = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.parse(model.Timestamp, timestamp);
    var refund = new pnc.model.Refund(model.Id,
                                      model.IdUser,
                                      model.IdInvoice,
                                      timestamp,
                                      model.Calculated,
                                      model.Actual,
                                      rel, links);

    for(var cont = 0, size = model.Restock.length; cont < size; cont++) {
        var rsJob   = model.Restock[cont];
        var restock = new pnc.model.Restock(rsJob.Id,
                                            rsJob.IdRefund,
                                            rsJob.IdInvoiceItem,
                                            rsJob.Quantity,
                                            rsJob.Value,
                                            rsJob.Restocked);
        refund.getRestock().push(restock);
    }

    return refund;
};

/**
 * Encodes an invalidation object into a serialized representation for transport to the server.
 * @param  {pnc.model.Invalidate} invalidation  Invalidation object to serialize
 * @return {string}                             Serialized representation
 */
pnc.client.Invoice.encodeInvalidation = function(invalidation) {
    var job = {
        Id        : invalidation.getId(),
        IdUser    : invalidation.getIdUser(),
        IdInvoice : invalidation.getIdInvoice(),
        Timestamp : pnc.model.DATE_TIME_FORMATTER.format(invalidation.getTimestamp()),
        Reason    : invalidation.getReason()
    };
    return goog.json.serialize(job);
};

/**
 * Internal decoding logic for the invalidation object
 * @param  {string} rel              Rel descriptor
 * @param  {Array}  flatLinks        Flat representation of links
 * @param  {Object} model            Json object with raw refund data
 * @return {pnc.model.Invalidation}  Invalidation object derived from data
 * @private
 */
pnc.client.Invoice.decodeInvalidation = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var timestamp = new Date();
    pnc.model.DATE_TIME_PARSER.parse(model.Timestamp, timestamp);
    var invalidation = new pnc.model.Invalidation(model.Id,
                                                  model.IdUser,
                                                  model.IdInvoice,
                                                  timestamp,
                                                  model.Reason,
                                                  rel, links);
    return invalidation;
};
