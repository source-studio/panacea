goog.provide('pnc.client.Sponsor');

goog.require('goog.json');
goog.require('goog.net.XhrIo');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Response');
goog.require('pnc.model');
goog.require('pnc.model.Sponsor');
goog.require('pnc.model.Sponsorship');


/**
 * Sponsor service API client
 * @constructor
 */
pnc.client.Sponsor = function() {
    /**
     * Array of global links which were made available upon login
     * @type {Array.<pnc.client.Link>}
     * @private
     */
    this.links = pnc.client.Profile.getLinks(pnc.client.Sponsor.Rel);
};

/**
 * Rel descriptors to use in the sponsor client
 * @enum {string}
 */
pnc.client.Sponsor.Rel = {
    GET_COLLECTION           : '/rels/pnc.sponsor-collection',
    SEARCH                   : '/rels/pnc.sponsor-search',
    ADD                      : '/rels/pnc.sponsor-add',
    UPDATE                   : '/rels/pnc.sponsor-update',
    REMOVE                   : '/rels/pnc.sponsor-remove',
    GET_SPONSORSHIPS         : '/rels/pnc.sponsor-sponsorships',
    GET_SPONSORSHIPS_BY_PAID : '/rels/pnc.sponsor-sponsorships-by-paid',
    UPDATE_SPONSORSHIP       : '/rels/pnc.sponsorship-update'
};


/**
 * Retrieve all sponsors
 * @param  {ResponseCallback} callback  Response handler
 */
pnc.client.Sponsor.prototype.getAll = function(callback) {
    var link    = this.links[pnc.client.Sponsor.Rel.GET_COLLECTION];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Sponsor.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Searches for sponsors with the provided criteria
 * @param  {string}           criteria  Search criteria
 * @param  {ResponseCallback} callback  Response hanlder
 */
pnc.client.Sponsor.prototype.search = function(criteria, callback) {
    var link    = this.links[pnc.client.Sponsor.Rel.SEARCH];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Sponsor.decode);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{org}', criteria);
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Add a new sponsor to the collection
 * @param {pnc.model.Sponsor} sponsor   Sponsor to add
 * @param {ResponseCallback}  callback  Response handler
 */
pnc.client.Sponsor.prototype.add = function(sponsor, callback) {
    var link    = this.links[pnc.client.Sponsor.Rel.ADD];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Sponsor.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Sponsor.encode(sponsor));
};

/**
 * Updates the details of an existing sponsor
 * @param  {pnc.model.Sponsor} sponsor   Sponsor to update
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Sponsor.prototype.update = function(sponsor, callback) {
    var link    = sponsor.meta.links()[pnc.client.Sponsor.Rel.UPDATE];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Sponsor.decode);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Sponsor.encode(sponsor));
};

/**
 * Removes a sponsor from the collection
 * @param  {pnc.model.Sponsor} sponsor   Sponsor to remove
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Sponsor.prototype.remove = function(sponsor, callback) {
    var link    = sponsor.meta.links()[pnc.client.Sponsor.Rel.REMOVE];
    var local   = pnc.client.createExecCallback(callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};


/**
 * Retrieves all sponsorships made by the given sponsor
 * @param  {pnc.model.Sponsor} sponsor   Sponsor for which sponsorships will be returned
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Sponsor.prototype.getSponsorships = function(sponsor, callback) {
    var link    = sponsor.meta.links()[pnc.client.Sponsor.Rel.GET_SPONSORSHIPS];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Sponsor.decodeSponsorship);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method());
};

/**
 * Retrieves all sponsorships made by the given sponsor filtered by paid state
 * @param  {pnc.model.Sponsor} sponsor   Sponsor for which sponsorships will be returned
 * @param  {boolean}           paid      Paid filter criteria
 * @param  {ResponseCallback}  callback  Response handler
 */
pnc.client.Sponsor.prototype.getSponsorshipsByPaid = function(sponsor, paid, callback) {
    var link    = sponsor.meta.links()[pnc.client.Sponsor.Rel.GET_SPONSORSHIPS_BY_PAID];
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Sponsor.decodeSponsorship);
    var local   = pnc.client.createCollectionCallback(decoder, callback);
    var uri     = link.uri().replace('{paid}', paid.toString());
    goog.net.XhrIo.send(uri, local, link.method());
};

/**
 * Updates the details on an existing sponsorships. Only paid and paidDate properties will be updated, all other changes
 * will be ignored.
 * @param  {pnc.model.Sponsorship} sponsorship  Sponsorship to update
 * @param  {ResponseCallback}      callback     Response handler
 */
pnc.client.Sponsor.prototype.updateSponsorship = function(sponsorship, callback) {
    var link    = sponsorship.meta.links()[pnc.client.Sponsor.Rel.UPDATE_SPONSORSHIP];
    var decoder = goog.partial(pnc.client.decode, pnc.client.Sponsor.decodeSponsorship);
    var local   = pnc.client.createModelCallback(decoder, callback);
    goog.net.XhrIo.send(link.uri(), local, link.method(), pnc.client.Sponsor.encodeSponsorship(sponsorship));
};


/**
 * Encodes a sponsor into a serialized representation for transport
 * @param  {pnc.model.Sponsor} sponsor  Sponsor to encode
 * @return {string}                     Serialized representation
 */
pnc.client.Sponsor.encode = function(sponsor) {
    var job = {
        Id           : sponsor.getId(),
        Organization : sponsor.getOrganization(),
        FirstName    : sponsor.getFirstName(),
        LastName     : sponsor.getLastName(),
        Contacts     : [],
        Address      : null
    };

    if(goog.isDefAndNotNull(sponsor.getAddress())) {
        var address = sponsor.getAddress();
        job.Address = {
            Id       : address.getId(),
            Street1  : address.getStreet1(),
            Street2  : address.getStreet2(),
            City     : address.getCity(),
            Province : address.getProvince(),
            Country  : address.getCountry()
        };
    }

    for(var cont = 0, size = sponsor.getContacts().length; cont < size; cont++) {
        var contact    = sponsor.getContacts()[cont];
        var contactJob = {
            Id          : contact.getId(),
            Uuid        : contact.getUuid(),
            ContactType : contact.getContactType(),
            Value       : contact.getValue()
        };
        job.Contacts.push(contactJob);
    }

    return goog.json.serialize(job);
};

/**
 * Decoding logic implementation
 * @param  {string} rel         Rel descriptor
 * @param  {Array}  flatLinks   Link representations
 * @param  {Object} model       Model representation
 * @return {pnc.model.Sponsor}  Sponsor model derived from data
 * @private
 */
pnc.client.Sponsor.decode = function(rel, flatLinks, model) {
    var links   = pnc.client.Link.fromJsonArray(flatLinks);
    var sponsor = new pnc.model.Sponsor(model.Id,
                                        model.Organization,
                                        model.FirstName,
                                        model.LastName,
                                        null, // address
                                        rel, links);

    if(goog.isDefAndNotNull(model.Address)) {
        var address = new pnc.model.Address(model.Address.Id,
                                            model.Address.Street1,
                                            model.Address.Street2,
                                            model.Address.City,
                                            model.Address.Province,
                                            model.Address.Country);
        sponsor.setAddress(address);
    }

    if(goog.isDefAndNotNull(model.Contacts) && model.Contacts.length > 0) {
        for(var cont = 0; cont < model.Contacts.length; cont++) {
            var contactJob = model.Contacts[cont];
            var contact = new pnc.model.Contact(contactJob.Id,
                                                contactJob.Uuid,
                                                contactJob.ContactType,
                                                contactJob.Value);
            sponsor.getContacts().push(contact);
        }
    }

    return sponsor;
};

/**
 * Encodes a sponsorship to a serialized representation for transport
 * @param  {pnc.model.Sponsorship} sponsorship  Sponsorship to encode
 * @return {string}                             Serialized representation
 */
pnc.client.Sponsor.encodeSponsorship = function(sponsorship) {
    var paidDate = goog.isDefAndNotNull(sponsorship.getPaidDate()) 
                 ? pnc.model.DATE_TIME_FORMATTER.format(sponsorship.getPaidDate()) 
                 : null;
    var job = {
        Id : sponsorship.getId(),
        IdSponsor : sponsorship.getIdSponsor(),
        IdInvoice : sponsorship.getIdInvoice(),
        Timestamp : pnc.model.DATE_TIME_FORMATTER.format(sponsorship.getTimestamp()),
        Total     : sponsorship.getTotal(),
        Sponsored : sponsorship.getSponsored(),
        Paid      : sponsorship.isPaid(),
        PaidDate  : paidDate
    }

    return goog.json.serialize(job);
};

/**
 * Decodes a sponsorship from a serialized representation into a model
 * @param  {string} rel             Rel descriptor
 * @param  {Array}  flatLinks       Link representations
 * @param  {Object} model           Model representation
 * @return {pnc.model.Sponsorship}  Sponsorship derived from data
 */
pnc.client.Sponsor.decodeSponsorship = function(rel, flatLinks, model) {
    var links = pnc.client.Link.fromJsonArray(flatLinks);
    var paidDate, timestamp = null;

    if(goog.isDefAndNotNull(model.PaidDate) && model.PaidDate.length > 0) {
        paidDate = new Date();
        pnc.model.DATE_TIME_PARSER.strictParse(model.PaidDate, paidDate)
    }

    if(goog.isDefAndNotNull(model.Timestamp) && model.Timestamp.length > 0) {
        timestamp = new Date();
        pnc.model.DATE_TIME_PARSER.strictParse(model.Timestamp, timestamp);
    }

    var sponsorship = new pnc.model.Sponsorship(model.Id,
                                                model.IdSponsor,
                                                model.IdInvoice,
                                                timestamp,
                                                model.Total,
                                                model.Sponsored,
                                                model.Paid,
                                                paidDate,
                                                rel, links);
    return sponsorship;
};
