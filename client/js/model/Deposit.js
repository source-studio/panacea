goog.provide('pnc.model.Deposit');

goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Model representing a deposit. Represents a transaction where money enters the business, but is not the result of a
 * sale or payment of a debt by a customer. There are several situations where money is deposited in a business. This is
 * used to represent those miscelaneous sitautions where money comes in and needs to be accounted for.
 * @constructor
 * @param {number}  id                Unique identifier for the deposit
 * @param {number}  idUser            ID of the user that registerd the deposit
 * @param {Date}    timestamp         Date and time when the deposit was made
 * @param {string}  reason            Reason or detail explaining why the deposit was made
 * @param {number}  amount            Total amount deposited
 * @param {boolean} includeInBalance  Will this deposit be included in the next balance?
 * @param {string}  rel               Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 */
pnc.model.Deposit = function(id, idUser, timestamp, reason, amount, includeInBalance, rel, links) {
    /**
     * Unique identifier for the deposit
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that registerd the deposit
     * @type {number}
     * @private
     */
    this.idUser    = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;

    /**
     * Date and time when the deposit was made
     * @type {Date}
     * @private
     */
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Reason or detail explaining why the deposit was made
     * @type {string}
     * @private
     */
    this.reason    = goog.isDef(reason) ? reason : null;

    /**
     * Total amount deposited
     * @type {number}
     * @private
     */
    this.amount    = goog.isDefAndNotNull(amount) ? amount : 0;

    /**
     * Will this deposit be included in the next balance?
     * @type {boolean}
     * @private
     */
    this.includeInBalance = goog.isDefAndNotNull(includeInBalance) ? includeInBalance : false;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta             = new pnc.model.Meta(rel, links);
};


/**
 * Compares to deposit objects to determine their prescedence. Does reverse-chronological-order sorting.
 * @param  {pnc.model.Deposit} deposit1 First deposit to compare
 * @param  {pnc.model.Deposit} deposit2 Second deposit to compare
 * @return {number}
 */
pnc.model.Deposit.compare = function(deposit1, deposit2) {
    var time1 = deposit1.getTimestamp().getTime();
    var time2 = deposit2.getTimestamp().getTime();

    if(time1 > time2)
        return -1;

    if (time2 > time1)
        return 1;

    return 0;
};

pnc.model.Deposit.prototype.getId = function() {
    return this.id;
};

pnc.model.Deposit.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Deposit.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Deposit.prototype.setIdUser = function(idUser) {
    this.idUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;
};

pnc.model.Deposit.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Deposit.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.Deposit.prototype.getReason = function() {
    return this.reason;
};

pnc.model.Deposit.prototype.setReason = function(reason) {
    this.reason = goog.isDef(reason) ? reason : null;
};

pnc.model.Deposit.prototype.getAmount = function() {
    return this.amount;
};

pnc.model.Deposit.prototype.setAmount = function(amount) {
    this.amount = goog.isDefAndNotNull(amount) ? amount : 0;
};

pnc.model.Deposit.prototype.isIncludedInBalance = function() {
    return this.includeInBalance;
};

pnc.model.Deposit.prototype.setIncludedInBalance = function(includeInBalance) {
    this.includeInBalance = goog.isDefAndNotNull(includeInBalance) ? includeInBalance : false;
};
