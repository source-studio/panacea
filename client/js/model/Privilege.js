goog.provide('pnc.model.Privilege');

goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * A privilege that is granted to a group of users. This is used to dynamically adjust the UI to present or remove controls
 * based on what the user is allowed to access. The server will also check the privileges for the user server side.
 * @constructor
 * @param {number} id                    Unique identifier for the privilege object
 * @param {string} module                Module this privilege affects
 * @param {string} submodule             Further subdivision of module grouping
 * @param {string} key                   Unique key identifier for privilege. This will be the reference used in the UI
 * @param {string} name                  Human readable privilege name.
 * @param {Array.<number>} dependencies  IDs of the other privileges that this privilege depends on. The user must be 
 *                                          granted all dependant privileges in order to have this one as well.
 * @param {string} rel                   Rel descriptor for object
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 */
pnc.model.Privilege = function(id, module, submodule, key, name, dependencies, rel, links) {
    if(!goog.isDefAndNotNull(module) || module.length < 1 || !goog.isDefAndNotNull(key) || key.length < 1 ||
    !goog.isDefAndNotNull(name) || name.length < 1)
        throw pnc.model.Error.NULL_OR_EMPTY_ARGUMENT;

    /**
     * Unique identifier for the privilege object
     * @type {number}
     * @private
     */
    this.id           = goog.isDefAndNotNull(id) ? id : pnc.model.Keys.DEFAUT_ID;

    /**
     * Module this privilege affects
     * @type {string}
     * @private
     */
    this.module       = goog.isDefAndNotNull(module) ? module : '';

    /**
     * Further subdivision of module grouping
     * @type {string}
     * @private
     */
    this.submodule    = goog.isDefAndNotNull(submodule) ? submodule : '';

    /**
     * Unique key identifier for privilege. This will be the reference used in the UI
     * @type {string}
     * @private
     */
    this.key          = key;

    /**
     * Human readable privilege name.
     * @type {string}
     * @private
     */
    this.name         = name;

    /**
     * IDs of the other privileges that this privilege depends on. The user must be granted all dependant privileges in
     * order to have this one as well.
     * @type {Array.<number>}
     * @private
     */
    this.dependencies = goog.isDefAndNotNull(dependencies) ? dependencies : [];

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta         = new pnc.model.Meta(rel, links);
};


pnc.model.Privilege.prototype.getId = function() {
    return this.id;
};

pnc.model.Privilege.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Privilege.prototype.getModule = function() {
    return this.module;
};

pnc.model.Privilege.prototype.setModule = function(module) {
    if(!goog.isDefAndNotNull(module) || module.length < 1)
        throw pnc.model.Error.NULL_OR_EMPTY_ARGUMENT;

    this.module = module;
};

pnc.model.Privilege.prototype.getSubmodule = function() {
    return this.submodule;
};

pnc.model.Privilege.prototype.setSubmodule = function(submodule) {
    this.submodule = goog.isDefAndNotNull(submodule) ? submodule : '';
};

pnc.model.Privilege.prototype.getKey = function() {
    return this.key;
};

pnc.model.Privilege.prototype.setKey = function(key) {
    if(!goog.isDefAndNotNull(key) || key.length < 1)
        throw pnc.model.Error.NULL_OR_EMPTY_ARGUMENT;

    this.key = key;
};

pnc.model.Privilege.prototype.getName = function() {
    return this.name;
};

pnc.model.Privilege.prototype.setName = function(name) {
    if(!goog.isDefAndNotNull(name) || name.length < 1)
        throw pnc.model.Error.NULL_OR_EMPTY_ARGUMENT;

    this.name = name;
};

pnc.model.Privilege.prototype.getDependencies = function() {
    return this.dependencies;
};

pnc.model.Privilege.prototype.setDependencies = function(dependencies) {
    this.dependencies = goog.isDefAndNotNull(dependencies) ? dependencies : [];
};
