goog.provide('pnc.model.Notification');

goog.require('pnc.model');
goog.require('pnc.model.Meta');

/**
 * Model representing a notificaiton. Notifications are presented to the user upon login, and the user interferce is 
 * expected to poll for them at an interval. They present timely information about activity of the business. The user 
 * can dismiss them or take action on them directly.
 * @param {number} id               Unique identifier for subscription
 * @param {number} idSubscription   ID of the subscription that generated this notification
 * @param {number} idUser           ID of the user who is meant to see this notification
 * @param {number} idSource         Optional ID that points to the source object for which the notification is being raised
 * @param {string} notificationType Type of notification. Should be a value from the pnc.model.Notification.Type enumeration.
 * @param {Date}   timestamp        Date/Time when the notification was created
 * @param {string} rel            Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links  Links with next possible states
 * @constructor
 */
pnc.model.Notification = function(id, idSubscription, idUser, idSource, notificationType, timestamp, rel, links) {
    /**
     * Unique identifier for subscription
     * @type {number}
     * @private
     */
    this.id               = goog.isDefAndNotNull(id)             ? id               : pnc.model.DEFAULT_ID;

    /**
     * ID of the subscription that generated this notification
     * @type {number}
     * @private
     */
    this.idSubscription   = goog.isDefAndNotNull(idSubscription) ? idSubscription   : pnc.model.DEFAULT_ID;

    /**
     * ID of the user who is meant to see this notification
     * @type {number}
     * @private
     */
    this.idUser           = goog.isDefAndNotNull(idUser)         ? idUser           : pnc.model.DEFAULT_ID;

    /**
     * Optional ID that points to the source object for which the notification is being raised
     * @type {number}
     * @private
     */
    this.idSource         = goog.isDefAndNotNull(idSource)       ? idSource         : pnc.model.DEFAULT_ID;

    /**
     * Type of notification. Should be a value from the pnc.model.Notification.Type enumeration.
     * @type {string}
     * @private
     */
    this.notificationType = goog.isDef(notificationType)         ? notificationType : null;

    /**
     * Date/Time when the notification was created
     * @type {Date}
     * @private
     */
    this.timestamp        = goog.isDef(timestamp)                ? timestamp        : null;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta             = new pnc.model.Meta(rel, links);
};

/**
 * List of possible valid values for the notificationType property in the notification model
 * @enum {string}
 */
pnc.model.Notification.Type = {
    /**
     * Notification raised when a product's inventory goes below the threshold
     * @type {String}
     */
    INVENTORY_THRESHOLD : 'INVENTORY_THRESHOLD',

    /**
     * Notification raised when a product has passed its expiration date
     * @type {String}
     */
    EXPIRED_INVENTORY   : 'EXPIRED_INVENTORY'
};

/**
 * Compares two notifications for sorting. Sorts notifications in reverse chronological order.
 * @param  {pnc.model.Notification} notification1  First notification to compare
 * @param  {pnc.model.Notification} notification2  Second notification to compare
 * @return {number}                                Comparison result.
 */
pnc.model.Notification.compare = function(notification1, notification2) {
    if(!goog.isDefAndNotNull(notification1.getTimestamp()) || !goog.isDefAndNotNull(notification.getTimestamp()))
        return 0;

    var time1 = notification1.getTimestamp().getTime();
    var time2 = notification2.getTimestamp().getTime();

    if(time1 > time2)
        return -1;
    else if(time1 < time2)
        return 1;

    return 0;
};


pnc.model.Notification.prototype.getId = function() {
    return this.id;
};

pnc.model.Notification.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Notification.prototype.getIdSubscription = function() {
    return this.idSubscription;
};

pnc.model.Notification.prototype.setIdSubscription = function(idSubscription) {
    this.idSubscription = goog.isDefAndNotNull(idSubscription) ? idSubscription : pnc.model.DEFAULT_ID;
};

pnc.model.Notification.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Notification.prototype.setIdUser = function(idUser) {
    this.iUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;
};

pnc.model.Notification.prototype.getIdSource = function() {
    return this.idSource;
};

pnc.model.Notification.prototype.setIdSource = function(idSource) {
    this.idSource = goog.isDefAndNotNull(idSource) ? idSource : pnc.model.DEFAULT_ID;
};

pnc.model.Notification.prototype.getNotificationType = function() {
    return this.notificationType;
};

pnc.model.Notification.prototype.setNotificationType = function(notificationType) {
    this.notificationType = goog.isDef(notificationType) ? notificationType : null;
};

pnc.model.Notification.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Notification.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDef(timestamp) ? timestamp : null;
};
