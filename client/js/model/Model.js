goog.provide('pnc.model');

goog.require('goog.array');
goog.require('goog.i18n.DateTimeFormat');
goog.require('goog.i18n.DateTimeParse');


/**
 * Models are representations of objects (real, or abstract conepts) to be used in the application. They
 * Simplify how the application serializes / deserializes objects when sending and receiving data from itself
 * and the server. It helps in giving the controls a common interface for the data they will present.
 * @namespace
 * @name pnc.model
 */

/**
 * Default identifier to use when no IDs are available or provided. If the primary ID of a model has the default value
 * it is assumed it is a new model that has not been persisted. If the its a relation ID, it is considered that the
 * model has no relationships set up yet, and may not be persisted.
 */
pnc.model.DEFAULT_ID          = 0,

/**
 * Generic date formatter. Used to format all dates that internal use and transport to server.
 */
pnc.model.DATE_FORMATTER      = new goog.i18n.DateTimeFormat('yyyy-MM-dd'),

/**
 * Generic date parser. Used to format all dates for internal use and transport to server.
 */
pnc.model.DATE_PARSER         = new goog.i18n.DateTimeParse('yyyy-MM-dd'),

/**
 * Generic time formatter
 */
pnc.model.TIME_FORMATTER      = new goog.i18n.DateTimeFormat('HH:mm:ss'),

/**
 * Generic time parser
 */
pnc.model.TIME_PARSER         = new goog.i18n.DateTimeParse('HH:mm:ss'),

/**
 * Generic Date/Time formatter.
 */
pnc.model.DATE_TIME_FORMATTER = new goog.i18n.DateTimeFormat('yyyy-MM-dd HH:mm:ss'),

/**
 * Generic Date/Time parser
 */
pnc.model.DATE_TIME_PARSER    = new goog.i18n.DateTimeParse('yyyy-MM-dd HH:mm:ss');

/**
 * List of common error messages
 * @enum {string}
 */
pnc.model.Error = {
    EMPTY_STRING           : 'The string provided was empty',
    NULL_ARGUMENT          : 'The argument provided was null',
    NULL_OR_EMPTY_ARGUMENT : 'The argument provides was null or empty',
    OUT_OF_RANGE           : 'The argument provided was outside the expected range',
    NEGATIVE_ARGUMENT      : 'The argument provided was a negative value. This condition was not expected.'
};

pnc.model.findModel = function(list, id) {
    for(var cont = 0, size = list.length; cont < size; cont++)
        if(list[cont].getId() == id)
            return list[cont];

    return null;
};

pnc.model.replaceModel = function(list, model) {
    for(var cont = 0, size = list.length; cont < size; cont++) {
        if(list[cont].getId() == model.getId()) {
            list[cont] = model;
            return;
        }
    }
};

pnc.model.removeModel = function(list, model) {
    var id = goog.isNumber(model) || goog.isString(model) ? model : model.getId();
    for(var cont = 0, size = list.length; cont < size; cont++) {
        if(list[cont].getId() == id) {
            goog.array.removeAt(list, cont);
            return;
        }
    }
};
