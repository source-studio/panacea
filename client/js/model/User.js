goog.provide('pnc.model.User');

goog.require('goog.string');
goog.require('pnc.model');
goog.require('pnc.model.Meta');

/**
 * A user that has the ability to log in to the application and perform transactions. Users belong to one or more groups.
 * Whatever privileges have been granted to the subscribed groups are inherited to the user. The application will alter
 * the UI to show controls for the transaction which a user has access to.
 * @constructor
 * @param {number} id        Unique identifier for the user
 * @param {string} username  Username the user uses to log into the application
 * @param {string} password  Password the user users to log into the application
 * @param {string} firstName User first name
 * @param {string} lastName  User last name
 * @param {boolean} enabled  Is the user account enabled? If not the user will not be able to log in. Disabling the user
 *                             is prefered to deleting the user account to maintain referential integrity with transactions
 *                             performed by old users.
 * @param {string} rel       Rel describor for object
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 */
pnc.model.User = function(id, username, password, firstName, lastName, enabled, rel, links) {
    /**
     * Unique identifier for the user
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Username the user uses to log into the application
     * @type {string}
     * @private
     */
    this.username  = username;

    /**
     * Password the user users to log into the application
     * @type {string}
     * @private
     */
    this.password  = password;

    /**
     * User first name
     * @type {string}
     * @private
     */
    this.firstName = firstName;

    /**
     * User last name
     * @type {string}
     * @private
     */
    this.lastName  = lastName;

    /**
     * Is the user account enabled? If not the user will not be able to log in. Disabling the user is prefered to
     * deleting the user account to maintain referential integrity with transactions performed by old users.
     * @type {boolean}
     * @private
     */
    this.enabled   = goog.isDefAndNotNull(enabled) ? enabled : false;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};

/**
 * Compares two users to determine which should have prescedence. User first name and last name used as basis for comparison.
 * @param  {!pnc.model.User} user1 First user to compare
 * @param  {!pnc.model.User} user2 Second user t compare
 * @return {number}
 */
pnc.model.User.compare = function(user1, user2) {
    var result = goog.string.caseInsensitiveCompare(user1.getFirstName(), user2.getFirstName());

    if(result !== 0)
        return result;

    return goog.string.caseInsensitiveCompare(user1.getLastName(), user2.getLastName());
};


pnc.model.User.prototype.toString = function() {
    return this.firstName + ' ' + this.lastName;
};

pnc.model.User.prototype.getId = function() {
    return this.id;
};

pnc.model.User.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.User.prototype.getUsername = function() {
    return this.username;
};

pnc.model.User.prototype.setUsername = function(username) {
    if(!goog.isDefAndNotNull(username) || username.length < 1)
        throw pnc.model.Error.NULL_OR_EMPTY_ARGUMENT;

    this.username = username;
};

pnc.model.User.prototype.getPassword = function() {
    return this.password;
};

pnc.model.User.prototype.setPassword = function(password) {
    this.password = password;
};

pnc.model.User.prototype.getFirstName = function() {
    return this.firstName;
};

pnc.model.User.prototype.setFirstName = function(firstName) {
    this.firstName = firstName;
};

pnc.model.User.prototype.getLastName = function() {
    return this.lastName;
};

pnc.model.User.prototype.setLastName = function(lastName) {
    this.lastName = lastName;
};

pnc.model.User.prototype.isEnabled = function() {
    return this.enabled;
};

pnc.model.User.prototype.setEnabled = function(enabled) {
    this.enabled = goog.isDefAndNotNull(enabled) ? enabled : false;
};
