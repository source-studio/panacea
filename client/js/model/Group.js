goog.provide('pnc.model.Group');

goog.require('goog.string');
goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Represents a group of users that have access to the application. Privileges are granted to groups of users as a whole
 * and not individual users to facilitate administration.
 * @constructor
 * @param {!number} id          Unique identifier for the group.
 * @param {!string} name        Name of the group. Unique among the collection
 * @param {string} description  Optional description for the group
 * @param {string} rel          Object rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Next possible states for object
 */
pnc.model.Group = function(id, name, description, rel, links) {
    /**
     * Unique identifier for the group.
     * @type {number}
     * @private
     */
    this.id          = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Name of the group. Unique among the collection
     * @type {string}
     * @private
     */
    this.name        = name;

    /**
     * Optional description for the group
     * @type {string}
     * @private
     */
    this.description = description;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta        = new pnc.model.Meta(rel, links);
};

/**
 * Compares two groups and determines which should have prescedence. Uses group name as factor.
 * @param  {!pnc.model.Group} group1 First group to compare
 * @param  {!pnc.model.Group} group2 Second group to compare
 * @return {number}                   Result of comparison
 */
pnc.model.Group.compare = function(group1, group2) {
    return goog.string.caseInsensitiveCompare(group1.getName(), group2.getName());
};


pnc.model.Group.prototype.getId = function() {
    return this.id;
};

pnc.model.Group.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Group.prototype.getName = function() {
    return this.name;
};

pnc.model.Group.prototype.setName = function(name) {
    this.name = name;
};

pnc.model.Group.prototype.getDescription = function() {
    return this.description;
};

pnc.model.Group.prototype.setDescription = function(description) {
    this.description = description;
};
