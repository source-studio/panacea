goog.provide('pnc.model.Inventory');

goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Inventory or stock of a product in existence. Usually entries are created when a batch of stock is acquired and made
 * available for sale or distribution. There can and should be multiple inventory entries for a single product. Product
 * batches are bought at different times, and as such can have different details like date entered, expiration date, and
 * cost. The system will assume a FIFO (first in, first out) order of products sold. That is, the oldest products should
 * always be sold first. So when having multiple inventory entries for a product, it will always be assumed that the
 * oldest will be sold first.
 * @constructor
 * @param {number}  id         Unique identifier for the inventory entry
 * @param {!bynber} idProduct  ID of the product this inventory entry is keeping track of
 * @param {number}  quantity   Number of items of the product that were originally entered into inventory
 * @param {number}  remaining  Number of items of the product in this inventory batch that are still in existence
 * @param {Date}    entered    Date the inventory entry was entered
 * @param {Date}    expiration Expiration date for the product
 * @param {number}  cost       Cost, per unit, of the product
 * @param {string}  rel        Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 */
pnc.model.Inventory = function(id, idProduct, quantity, remaining, entered, expiration, cost, rel, links) {
    /**
     * Unique identifier for the inventory entry
     * @type {number}
     * @private
     */
    this.id         = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the product this inventory entry is keeping track of
     * @type {number}
     * @private
     */
    this.idProduct  = idProduct;

    /**
     * Number of items of the product that were originally entered into inventory
     * @type {number}
     * @private
     */
    this.quantity   = goog.isDefAndNotNull(quantity) ? quantity : 0;

    /**
     * Number of items of the product in this inventory batch that are still in existence
     * @type {number}
     * @private
     */
    this.remaining  = goog.isDefAndNotNull(remaining) ? remaining : 0;

    /**
     * Date the inventory entry was entered
     * @type {Date}
     * @private
     */
    this.entered    = goog.isDefAndNotNull(entered) ? entered : new Date();

    /**
     * Expiration date for the product
     * @type {Date}
     * @private
     */
    this.expiration = goog.isDef(expiration) ? expiration : null;

    /**
     * Cost, per unit, of the product
     * @type {number}
     * @private
     */
    this.cost       = goog.isDefAndNotNull(cost) ? cost : 0;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta       = new pnc.model.Meta(rel, links);
};

/**
 * Compares inventory objects to determine prescedence. Orders in chronological order.
 * @param  {pnc.model.Inventory} inventory1 First inventory entry to compare
 * @param  {pnc.model.Inventory} inventory2 Second inventory entry to compare
 * @return {number}
 */
pnc.model.Inventory.compare = function(inventory1, inventory2) {
    var time1 = inventory1.getEntered().getTime();
    var time2 = inventory2.getEntered().getTime();

    if(time1 > time2)
        return -1;

    if(time1 < time2)
        return 1;

    return 0;
};


pnc.model.Inventory.prototype.getId = function() {
    return this.id;
};

pnc.model.Inventory.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Inventory.prototype.getIdProduct = function() {
    return this.idProduct;
};

pnc.model.Inventory.prototype.setIdProduct = function(idProduct) {
    if(!goog.isDefAndNotNull(idProduct) || idProduct < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    this.idProduct = idProduct;
};

pnc.model.Inventory.prototype.getQuantity = function() {
    return this.quantity;
};

pnc.model.Inventory.prototype.setQuantity = function(quantity) {
    this.quantity = goog.isDefAndNotNull(quantity) ? quantity : 0;
};

pnc.model.Inventory.prototype.getRemaining = function() {
    return this.remaining;
};

pnc.model.Inventory.prototype.setRemaining = function(remaining) {
    this.remaining = goog.isDefAndNotNull(remaining) ? remaining : 0;
};

pnc.model.Inventory.prototype.getEntered = function() {
    return this.entered;
};

pnc.model.Inventory.prototype.setEntered = function(entered) {
    this.entered = goog.isDefAndNotNull(entered) ? entered : new Date();
};

pnc.model.Inventory.prototype.getExpiration = function() {
    return this.expiration;
};

pnc.model.Inventory.prototype.setExpiration = function(expiration) {
    this.expiration = goog.isDef(expiration) ? expiration : null;
};

pnc.model.Inventory.prototype.getCost = function() {
    return this.cost;
};

pnc.model.Inventory.prototype.setCost = function(cost) {
    this.cost = goog.isDefAndNotNull(cost) ? cost : 0;
};
