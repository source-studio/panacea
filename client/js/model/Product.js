goog.provide('pnc.model.Product');

goog.require('goog.string');
goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Model that represents a product that is saved in inventory and is sold
 * @constructor
 * @param {number} id        Unique identifier for the product
 * @param {string} brand     Product brand. Company that makes/supplies product
 * @param {string} name      Product name. This includes any specifications about size or type.
 * @param {number} barCode   Product bar code. If none is registered, then 0 value is assumed.
 * @param {number} taxRate   Percent of tax that is charged for this product.
 * @param {number} price     Sale price for product
 * @param {number} threshold Threshold after which alerts will be raised about low inventory
 * @param {number} minimum   Minimum quantity that should be sold for this product. If zero, no minimum will be enforced
 * @param {string} rel       Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links  Links with next possible states
 */
pnc.model.Product = function(id, brand, name, barCode, taxRate, price, threshold, minimum, rel, links) {
    /**
     * Unique identifier for the product
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Product brand. Company that makes/supplies product
     * @type {string}
     * @private
     */
    this.brand     = goog.isDef(brand) ? brand : null;

    /**
     * Product name. This includes any specifications about size or type.
     * @type {string}
     * @private
     */
    this.name      = goog.isDef(name) ? name : null;

    /**
     * Product bar code. If none is registered, then 0 value is assumed.
     * @type {string}
     * @private
     */
    this.barCode   = barCode;

    /**
     * Percentage of tax that is charged for the product
     * @type {number}
     * @private
     */
    this.taxRate   = goog.isDefAndNotNull(taxRate) ? taxRate : 0;

    /**
     * Sale price for product
     * @type {number}
     * @private
     */
    this.price     = goog.isDefAndNotNull(price) ? price : 0;

    /**
     * Threshold after which alerts will be raised about low inventory
     * @type {number}
     * @private
     */
    this.threshold = goog.isDefAndNotNull(threshold) ? threshold : 0;

    /**
     * Minimum quantity that should be sold for this product. If zero, no minimum will be enforced
     * @type {number}
     * @private
     */
    this.minimum   = goog.isDefAndNotNull(minimum) ? minimum : 0;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};

/**
 * Compares to products to determine their prescedense. Orders by brand and name in alpha order.
 * @param  {pnc.model.Product} product1 First product to compare
 * @param  {pnc.model.Product} product2 Second product to compare
 * @return {number}
 */
pnc.model.Product.compare = function(product1, product2) {
    var result = goog.string.caseInsensitiveCompare(product1.getBrand(), product2.getBrand());

    if(result !== 0)
        return result;

    return goog.string.caseInsensitiveCompare(product1.getName(), product2.getName());
};


pnc.model.Product.prototype.toString = function() {
    return this.brand + ' ' + this.name;
};

pnc.model.Product.prototype.getId = function() {
    return this.id;
};

pnc.model.Product.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Product.prototype.getBrand = function() {
    return this.brand;
};

pnc.model.Product.prototype.setBrand = function(brand) {
    this.brand = brand;
};

pnc.model.Product.prototype.getName = function() {
    return this.name;
};

pnc.model.Product.prototype.setName = function(name) {
    this.name = name;
};

pnc.model.Product.prototype.getBarCode = function() {
    return this.barCode;
};

pnc.model.Product.prototype.setBarCode = function(barCode) {
    this.bardCode = barCode;
};

pnc.model.Product.prototype.getTaxRate = function() {
    return this.taxRate;
};

pnc.model.Product.prototype.setTaxRate = function(taxRate) {
    this.taxRate = goog.isDefAndNotNull(taxRate) ? taxRate : 0;
};

pnc.model.Product.prototype.getPrice = function() {
    return this.price;
};

pnc.model.Product.prototype.setPrice = function(price) {
    this.price = goog.isDefAndNotNull(price) ? price : 0;
};

pnc.model.Product.prototype.getThreshold = function() {
    return this.threshold;
};

pnc.model.Product.prototype.setThreshold = function(threshold) {
    this.threshold = goog.isDefAndNotNull(threshold) ? threshold : 0;
};

pnc.model.Product.prototype.getMinimum = function() {
    return this.minimum;
};

pnc.model.Product.prototype.setMinimum = function(minimum) {
    this.minimum = goog.isDefAndNotNull(minimum) ? minimum : 0;
};
