goog.provide('pnc.model.Report');

goog.require('pnc.model');


/**
 * Generic container for report data. Holds metdata around report generation, and provides an interface so that report
 * data can be handled by model framework.
 * @param {number}                  id          Unique identifier for the report
 * @param {number}                  idUser      ID of the user that generated the report
 * @param {date}                    timestamp   Timestamp indicaitng when the report was generated
 * @param {string}                  reportType  Report type identifier
 * @param {object.<string, string>} arguments   Arguments that were provided to the report generation service.
 * @param {object.<string, ?>}      data        Data yielded in report
 * @param {string}                  rel         Rel identifier for object
 * @param {array.<pnc.model.Link>}  links       REST links
 * @constructor
 */
pnc.model.Report = function(id, idUser, timestamp, reportType, args, data, rel, links) {
    /**
     * Unique identifier for the report
     * @type {number}
     * @private
     */
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that generated the report
     * @type {number}
     * @private
     */
    this.idUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;

    /**
     * Timestamp indicaitng when the report was generated
     * @type {date}
     * @private
     */
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Report type identifier
     * @type {string}
     * @private
     */
    this.reportType = goog.isDef(reportType) ? reportType : null;

    /**
     * Arguments that were provided to the report generation service.
     * @type {object.<string, string>}
     * @private
     */
    this.args = goog.isDefAndNotNull(args) ? args : {};

    /**
     * Data yielded in report
     * @type {object.<string, ?>}
     * @private
     */
    this.data = goog.isDefAndNotNull(data) ? data : {};

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta = new pnc.model.Meta(rel, links);
};

pnc.model.Report.prototype.getId = function() {
    return this.id;
};

pnc.model.Report.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Report.prototype.getIdUser = function() {
    return this.idUser;
}

pnc.model.Report.prototype.setIdUser = function(idUser) {
    this.idUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;
};

pnc.model.Report.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Report.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDef(timestamp) ? timestamp : null;
};

pnc.model.Report.prototype.getReportType = function() {
    return this.reportType;
};

pnc.model.Report.prototype.setReportType = function(reportType) {
    this.reportType = goog.isDef(reportType) ? reportType : null;
};

pnc.model.Report.prototype.getArguments = function() {
    return this.arguments;
};

pnc.model.Report.prototype.setArguments = function(args) {
    this.args = goog.isDefAndNotNull(args) ? args : {};
};

pnc.model.Report.prototype.getData = function() {
    return this.data;
};

pnc.model.Report.prototype.setData = function(data) {
    this.data = goog.isDefAndNotNull(data) ? data : {};
};
