goog.provide('pnc.model.Address');

goog.require('pnc.model');


/**
 * An address that can be assigned to people, businesses or institutions
 * @constructor
 * @param {number} id       Unique identifier for the address
 * @param {string} street1  First line of the address
 * @param {string} street2  Second line of the address or sector
 * @param {string} city     Address city
 * @param {string} province Address province or state
 * @param {string} country  Address country
 */
pnc.model.Address = function(id, street1, street2, city, province, country) {
    /**
     * Unique identifier for the address
     * @type {number}
     * @private
     */
    this.id       = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * First line of the address
     * @type {string}
     * @private
     */
    this.street1  = goog.isDef(street1) ? street1 : null;

    /**
     * Second line of the address or sector
     * @type {string}
     * @private
     */
    this.street2  = goog.isDef(street2) ? street2 : null;

    /**
     * Address city
     * @type {string}
     * @private
     */
    this.city     = goog.isDef(city) ? city : null;

    /**
     * Address province or state
     * @type {string}
     * @private
     */
    this.province = goog.isDef(province) ? province : null;

    /**
     * Address country
     * @type {string}
     * @private
     */
    this.country  = goog.isDef(country) ? country : null;
};


pnc.model.Address.prototype.getId = function() {
    return this.id;
};

pnc.model.Address.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Address.prototype.getStreet1 = function() {
    return this.street1;
};

pnc.model.Address.prototype.setStreet1 = function(street1) {
    this.street1 = goog.isDef(street1) ? street1 : null;
};

pnc.model.Address.prototype.getStreet2 = function() {
    return this.street2;
};

pnc.model.Address.prototype.setStreet2 = function(street2) {
    this.street2 = goog.isDef(street2) ? street2 : null;
};

pnc.model.Address.prototype.getCity = function() {
    return this.city;
};

pnc.model.Address.prototype.setCity = function(city) {
    this.city = goog.isDef(city) ? city : null;
};

pnc.model.Address.prototype.getProvince = function() {
    return this.province;
};

pnc.model.Address.prototype.setProvince = function(province) {
    this.province = goog.isDef(province) ? province : null;
};

pnc.model.Address.prototype.getCountry = function() {
    return this.country;
};

pnc.model.Address.prototype.setCountry = function(country) {
    this.country = goog.isDef(country) ? country : null;
};
