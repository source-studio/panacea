goog.provide('pnc.model.Refund');

goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Model representing a refund. 
 * @param {number} id          Unique identifier of the refund
 * @param {number} idUser      ID of the user that made the refund
 * @param {number} idInvoice   ID of the invoice being refunded
 * @param {Date}   timestamp   Date/Time when the refund was made
 * @param {number} calculated  Amount calculated that should be refunded
 * @param {number} actual      Amount actually refunded
 * @param {string} rel         Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 * @constructor
 */
pnc.model.Refund = function(id, idUser, idInvoice, timestamp, calculated, actual, rel, links) {
    /**
     * Unique identifier of the refund
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id)          ? id         : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that made the refund
     * @type {number}
     * @private
     */
    this.idUser     = goog.isDefAndNotNull(idUser)     ? idUser     : pnc.model.DEFAULT_ID;

    /**
     * ID of the invoice being refunded
     * @type {number}
     * @private
     */
    this.idInvoice  = goog.isDefAndNotNull(idInvoice)  ? idInvoice  : pnc.model.DEFAULT_ID;

    /**
     * Date/Time when the refund was made
     * @type {Date}
     * @private
     */
    this.timestamp  = goog.isDefAndNotNull(timestamp)  ? timestamp  : new Date();

    /**
     * Amount calculated that should be refunded
     * @type {number}
     * @private
     */
    this.calculated = goog.isDefAndNotNull(calculated) ? calculated : 0;

    /**
     * Amount actually refunded
     * @type {number}
     * @private
     */
    this.actual     = goog.isDefAndNotNull(actual)     ? actual     : 0;

    /**
     * Collection of restock items tied to this refund
     * @type {Array.<pnc.model.Restock>}
     * @private
     */
    this.restock    = [];

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};

/**
 * Model representing a restock. A restock is directly related to an invoice item from an invoice which participates
 * in a refund transaction. It holds data about how much of the item is being refunded, and if it's bieng restocked or not.
 * @param  {number} id             Unique identifier for the restock
 * @param  {number} idRefund       ID of the parent refund object
 * @param  {number} idInvoiceItem  ID of the invoice item this restock is tied to
 * @param  {number} quantity       Amount being returned. This value should not be greater than the invoice item quanitty.
 * @param  {number} value          Total value of the product being returned.
 * @param  {bool} restocked        Is the returned product being restocked?
 * @constructor
 */
pnc.model.Restock = function(id, idRefund, idInvoiceItem, quantity, value, restocked) {
    /**
     * Unique identifier for the restock
     * @type {number}
     * @private
     */
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the parent refund object
     * @type {number}
     * @private
     */
    this.idRefund = goog.isDefAndNotNull(idRefund) ? idRefund : pnc.model.DEFAULT_ID;

    /**
     * ID of the invoice item this restock is tied to
     * @type {number}
     * @private
     */
    this.idInvoiceItem = goog.isDefAndNotNull(idInvoiceItem) ? idInvoiceItem : pnc.model.DEFAULT_ID;

    /**
     * Amount being returned. This value should not be greater than the invoice item quanitty.
     * @type {number}
     * @private
     */
    this.quantity = goog.isDefAndNotNull(quantity) ? quantity : 0;

    /**
     * Total value of the product being returned.
     * @type {number}
     * @private
     */
    this.value = goog.isDefAndNotNull(value) ? value : 0;

    /**
     * Is the returned product being restocked?
     * @type {boolean}
     * @private
     */
    this.restocked = goog.isDefAndNotNull(restocked) ? restocked : false;
};

/**
 * Compares two refunds to determine which should be placed first. Uses reverse chronological order based on the timestamp
 * property.
 * @param  {pnc.model.Refund} refund1 [description]
 * @param  {pnc.model.Refund} refund2 [description]
 * @return {number}                   -1, 0 or 1 which determines if the first argument should be placed before, equally
 *                                        or after the second argument respectively.
 */
pnc.model.Refund.compare = function(refund1, refund2) {
    var time1 = refund1.getTimestamp().getTime();
    var time2 = refund2.getTimestamp().getTime();

    if(time1 > time2)
        return 1;

    if(time1 < time2)
        return -1;

    return 0;
};


pnc.model.Refund.prototype.getId = function() {
    return this.id;
};

pnc.model.Refund.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Refund.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Refund.prototype.setIdUser = function(idUser) {
    this.idUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;
};

pnc.model.Refund.prototype.getIdInvoice = function() {
    return this.idInvoice;
};

pnc.model.Refund.prototype.setIdInvoice = function(idInvoice) {
    this.idInvoice = goog.isDefAndNotNull(idInvoice) ? idInvoice : pnc.model.DEFAULT_ID;
};

pnc.model.Refund.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Refund.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.Refund.prototype.getCalculated = function() {
    return this.calculated;
};

pnc.model.Refund.prototype.setCalculated = function(calculated) {
    this.calculated = goog.isDefAndNotNull(calculated) ? calculated : 0;
};

pnc.model.Refund.prototype.getActual = function() {
    return this.actual;
};

pnc.model.Refund.prototype.setActual = function(actual) {
    this.actual = goog.isDefAndNotNull(actual) ? actual : 0;
};

pnc.model.Refund.prototype.getRestock = function() {
    return this.restock;
};


pnc.model.Restock.prototype.getId = function() {
    return this.id;
};

pnc.model.Restock.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Restock.prototype.getIdRefund = function() {
    return this.idRefund;
};

pnc.model.Restock.prototype.setIdRefund = function(idRefund) {
    this.idRefund = goog.isDefAndNotNull(idRefund) ? idRefund : pnc.model.DEFAULT_ID;
};

pnc.model.Restock.prototype.getIdInvoiceItem = function() {
    return this.idInvoiceItem;
};

pnc.model.Restock.prototype.setIdInvoiceItem = function(idInvoiceItem) {
    this.idInvoiceItem = gog.isDefAndNotNull(idInvoiceItem) ? idInvoiceItem : pnc.model.DEFAULT_ID;
};

pnc.model.Restock.prototype.getQuantity = function() {
    return this.quantity;
};

pnc.model.Restock.prototype.setQuantity = function(quantity) {
    this.quantity = goog.isDefAndNotNull(quantity) ? quantity : 0;
};

pnc.model.Restock.prototype.getValue = function() {
    return this.value;
};

pnc.model.Restock.prototype.setValue = function(value) {
    this.value = goog.isDefAndNotNull(value) ? value : 0;
};

pnc.model.Restock.prototype.isRestocked = function() {
    return this.restocked;
};

pnc.model.Restock.prototype.setRestocked = function(restocked) {
    this.restocked = goog.isDefAndNotNull(restocked) ? restocked : false;
};
