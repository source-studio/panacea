goog.provide('pnc.model.Expense');

goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Model that represents a company expense. Usually happens when money has to be taken out of the cashier to pay for
 * something for the business. This relates to buying from suppliers, or purchasing goods to keep the business running.
 * @constructor
 * @param {number}  id                Unique identifier for the expense
 * @param {!number} idUser            ID of the user that registerd the expense
 * @param {Date}    timestamp         Date and time when the expense was incurred
 * @param {string}  reason            Reason or detail explaining why the expense was incurred
 * @param {number}  amount            Total amount the expense cost
 * @param {boolean} includeInBalance  Will this expense be included in the next balance?
 * @param {string}  rel               Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 */
pnc.model.Expense = function(id, idUser, timestamp, reason, amount, includeInBalance, rel, links) {
    /**
     * Unique identifier for the expense
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that registerd the expense
     * @type {number}
     * @private
     */
    this.idUser    = idUser;

    /**
     * Date and time when the expense was incurred
     * @type {Date}
     * @private
     */
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Reason or detail explaining why the expense was incurred
     * @type {string}
     * @private
     */
    this.reason    = goog.isDef(reason) ? reason : null;

    /**
     * Total amount the expense cost
     * @type {number}
     * @private
     */
    this.amount    = goog.isDefAndNotNull(amount) ? amount : 0;

    /**
     * Will this expense be included in the next balance?
     * @type {boolean}
     * @prviate
     */
    this.includeInBalance = goog.isDefAndNotNull(includeInBalance) ? includeInBalance : false;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};


/**
 * Compares to expense objects to determine their prescedence. Does reverse-chronological-order sorting.
 * @param  {pnc.model.Expense} expense1 First expense object to compare
 * @param  {pnc.model.Expense} expense2 Second expense object to compare
 * @return {number}
 */
pnc.model.Expense.compare = function(expense1, expense2) {
    var time1 = expense1.getTimestamp().getTime();
    var time2 = expense2.getTimestamp().getTime();

    if(time1 > time2)
        return -1;

    if (time2 > time1)
        return 1;

    return 0;
};


pnc.model.Expense.prototype.getId = function() {
    return this.id;
};

pnc.model.Expense.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Expense.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Expense.prototype.setIdUser = function(idUser) {
    this.idUser = idUser;
};

pnc.model.Expense.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Expense.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.Expense.prototype.getReason = function() {
    return this.reason;
};

pnc.model.Expense.prototype.setReason = function(reason) {
    this.reason = goog.isDef(reason) ? reason : null;
};

pnc.model.Expense.prototype.getAmount = function() {
    return this.amount;
};

pnc.model.Expense.prototype.setAmount = function(amount) {
    this.amount = goog.isDefAndNotNull(amount) ? amount : 0;
};

pnc.model.Expense.prototype.isIncludedInBalance = function() {
    return this.includeInBalance;
};

pnc.model.Expense.prototype.setIncludedInBalance = function(includeInBalance) {
    this.includeInBalance = goog.isDefAndNotNull(includeInBalance) ? includeInBalance : false;
};
