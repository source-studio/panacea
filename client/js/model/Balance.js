goog.provide('pnc.model.Balance');

goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Model representing a cash balance. Cash balances are usually done at the end of the day or cashier shift. It is used
 * to verifythat the current transactions and cash at hand matches up. If it does not the balance, will reflect that and
 * will provideinformation about the difference.
 * @constructor
 * @param {number}  id          Unique identifier for the balance ojbect
 * @param {!number} idUser      User that generated this balance report
 * @param {Date}    timestamp   Moment in time when this report was generated
 * @param {number}  start       Amount of money that was left available to make transactions at the begining of the time period
 * @param {number}  cashSales   Total amount of sales made in cash
 * @param {number}  creditSales Total amount of sales made in credit
 * @param {number}  refunds     Total value of all refunds made
 * @param {number}  payments    Total amount that was received in payments
 * @param {number}  expenses    Total amount that was spent as expenses
 * @param {number}  deposits    Total amount of money that was deposited. This is outside of regular sale transactions
 * @param {number}  nextStart   Amount of money that will be left for the next period
 * @param {number}  balance     Cash balance after all calculations
 * @param {number}  difference  Amount of money over or under which
 * @param {string}  notes       Optional user inputted notes that provide explanation or detail about the balance
 * @param {string}  rel         Rel descriptor for the object
 * @param {Object.<string, pnc.client.Link>} links Links that provided next possible states
 */
pnc.model.Balance = function(id, idUser, timestamp, start, cashSales, creditSales, refunds, payments, expenses, deposits, nextStart, balance, difference, notes, rel, links) {
    if(!goog.isDefAndNotNull(idUser) || idUser < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    if(!goog.isDefAndNotNull(start))
        throw pnc.model.Error.NULL_ARGUMENT;

    /**
     * Unique identifier for the balance ojbect
     * @type {number}
     * @private
     */
    this.id          = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * User that generated this balance report
     * @type {number}
     * @private
     */
    this.idUser      = idUser;

    /**
     * Moment in time when this report was generated
     * @type {Date}
     * @private
     */
    this.timestamp   = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Amount of money that was left available to make transactions at the begining of the time period
     * @type {number}
     * @private
     */
    this.start       = start;

    /**
     * Total amount of sales made in cash
     * @type {number}
     * @private
     */
    this.cashSales   = goog.isDefAndNotNull(cashSales)   ? cashSales   : 0;

    /**
     * Total amount of sales made in credit
     * @type {number}
     * @private
     */
    this.creditSales = goog.isDefAndNotNull(creditSales) ? creditSales : 0;

    /**
     * Total value of all refunds made
     * @type {number}
     * @private
     */
    this.refunds     = goog.isDefAndNotNull(refunds)     ? refunds     : 0;

    /**
     * Total amount that was received in payments
     * @type {number}
     * @private
     */
    this.payments    = goog.isDefAndNotNull(payments)    ? payments    : 0;

    /**
     * Total amount that was spent as expenses
     * @type {number}
     * @private
     */
    this.expenses    = goog.isDefAndNotNull(expenses)    ? expenses    : 0;

    /**
     * Total amount of money that was deposited. This is outside of regular sale transactions
     * @type {number}
     * @private
     */
    this.deposits    = goog.isDefAndNotNull(deposits)    ? deposits    : 0;

    /**
     * Amount of money that will be left for the next period
     * @type {number}
     * @private
     */
    this.nextStart   = goog.isDefAndNotNull(nextStart)   ? nextStart   : 0;

    /**
     * Cash balance after all calculations
     * @type {number}
     * @private
     */
    this.balance     = goog.isDefAndNotNull(balance)     ? balance     : 0;

    /**
     * Difference between balance and actual cash, after all calculations
     * @type {number}
     * @private
     */
    this.difference  = goog.isDefAndNotNull(difference)  ? difference  : 0;

    /**
     * Optional user inputted notes that provide explanation or detail about the balance
     * @type {string}
     * @private
     */
    this.notes       = notes;

    /**
     * REST metadat
     * @type {pnc.model.Meta}
     */
    this.meta        = new pnc.model.Meta(rel, links);
};

/**
 * Compares two balance objects to determine which should have prescedence. Uses the timestamp property. Provides
 * descending order sorting.
 * @param  {pnc.model.Balance} balance1
 * @param  {pnc.model.Balance} balance2
 * @return {number}
 */
pnc.model.Balance.compare = function(balance1, balance2) {
    var time1 = balance1.getTimestamp().getTime();
    var time2 = balance2.getTimestamp().getTime();

    if(time1 > time2)
        return 1;

    if(time1 < time2)
        return -1;

    return 0;
};

pnc.model.Balance.prototype.getId = function() {
    return this.id;
};

pnc.model.Balance.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Balance.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Balance.prototype.setIdUser = function(idUser) {
    if(!goog.isDefAndNotNull(idUser) || idUser < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    this.idUser = idUser;
};

pnc.model.Balance.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Balance.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.Balance.prototype.getStart = function() {
    return this.start;
};

pnc.model.Balance.prototype.setStart = function(start) {
    if(!goog.isDefAndNotNull(start))
        throw pnc.model.Error.NULL_ARGUMENT;
 
    this.start = start;
};

pnc.model.Balance.prototype.getCashSales = function() {
    return this.cashSales;
};

pnc.model.Balance.prototype.setCashSales = function(cashSales) {
    this.cashSales = goog.isDefAndNotNull(cashSales) ? cashSales : 0;
};

pnc.model.Balance.prototype.getCreditSales = function() {
    return this.creditSales;
};

pnc.model.Balance.prototype.setCreditSales = function(creditSales) {
    this.creditSales = goog.isDefAndNotNull(creditSales) ? creditSales : 0;
};

pnc.model.Balance.prototype.getRefunds = function() {
    return this.refunds;
};

pnc.model.Balance.prototype.setRefunds = function(refunds) {
    this.refunds = goog.isDefAndNotNull(refunds) ? refunds : 0;
};

pnc.model.Balance.prototype.getPayments = function() {
    return this.payments;
};

pnc.model.Balance.prototype.setPayments = function(payments) {
    this.payments = goog.isDefAndNotNull(payments) ? payments : 0;
};

pnc.model.Balance.prototype.getExpenses = function() {
    return this.expenses;
};

pnc.model.Balance.prototype.setExpenses = function(expenses) {
    this.expenses = goog.isDefAndNotNull(expenses) ? expenses : 0;
};

pnc.model.Balance.prototype.getDeposits = function() {
    return this.deposits;
};

pnc.model.Balance.prototype.setDeposits = function(deposits) {
    this.deposits = goog.isDefAndNotNull(deposits) ? deposits : 0;
};

pnc.model.Balance.prototype.getNextStart = function() {
    return this.nextStart;
};

pnc.model.Balance.prototype.setNextStart = function(nextStart) {
    this.nextStart = goog.isDefAndNotNull(nextStart) ? nextStart : 0;
};

pnc.model.Balance.prototype.getBalance = function() {
    return this.balance;
};

pnc.model.Balance.prototype.setBalance = function(balance) {
    this.balance = goog.isDefAndNotNull(balance) ? balance : 0;
};

pnc.model.Balance.prototype.getDifference = function() {
    return this.difference;
};

pnc.model.Balance.prototype.setDifference = function(difference) {
    this.difference = goog.isDefAndNotNull(difference) ? difference : 0;
};

pnc.model.Balance.prototype.getNotes = function() {
    return this.notes;
};

pnc.model.Balance.prototype.setNotes = function(notes) {
    this.notes = notes;
};
