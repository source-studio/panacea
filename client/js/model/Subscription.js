goog.provide('pnc.model.Subscription');

goog.require('pnc.model');
goog.require('pnc.model.Meta');

/**
 * A subscription represents a user's willingness to receice a type of notification from the application. once a user 
 * is has subscribed to a notification type, all notifications generated of that type will be put into his/her queue. 
 * The user can consume those notification at his leasure.
 * @param {number} id                Unique identifier for subscription
 * @param {number} idUser            ID of the user that is subscribed
 * @param {string} notificationType  Type of notification the user is subscried to. Has to be a value from the
 *                                       pnc.model.Notification.Type enumeration.
 * @param {string} rel               Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links  Links with next possible states
 * @constructor
 */
pnc.model.Subscription = function(id, idUser, notificationType, rel, links) {
    /**
     * Unique identifier for subscription
     * @type {number}
     * @private
     */
    this.id               = goog.isDefAndNotNull(id)     ? id               : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that is subscribed
     * @type {number}
     * @private
     */
    this.idUser           = goog.isDefAndNotNull(idUser) ? idUser           : pnc.model.DEFAULT_ID;

    /**
     * Type of notification the user is subscried to. Has to be a value from the pnc.model.Notification.Type 
     * enumeration.
     * @type {string}
     * @private
     */
    this.notificationType = goog.isDef(notificationType) ? notificationType : null;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta             = new pnc.model.Meta(rel, links);
};


pnc.model.Subscription.prototype.getId = function() {
    return this.id;
};

pnc.model.Subscription.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Subscription.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Subscription.prototype.setIdUser = function(idUser) {
    this.idUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;
};

pnc.model.Subscription.prototype.getNotificationType = function() {
    return this.notificationType;
};

pnc.model.Subscription.prototype.setNotificationType = function(notificationType) {
    this.notificationType = goog.isDef(notificationType) ? notificationType : null;
};
