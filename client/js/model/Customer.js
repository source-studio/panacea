goog.provide('pnc.model.Customer');

goog.require('goog.string');
goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Represents a customer who makes purchases from the system. Not all people need to have customer profiles. The most
 * common transaction will probably be with an annonymous customer. However for people that will require a history of
 * transactions, that will have credit lines with the business and other needs, a customer profile will be necessary.
 * @constructor
 * @param {number} id                 Unique identifier for the user
 * @param {string} firstName          Customer's first name
 * @param {string} lastName           Customer's last name
 * @param {string} nickName           Customer's nick name, if applicable
 * @param {pnc.model.Address} address Customer's address if applicable
 * @param {string} rel                Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 */
pnc.model.Customer = function(id, firstName, lastName, nickName, address, rel, links) {
    /**
     * Unique identifier for the user
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Customer's first name
     * @type {string}
     * @private
     */
    this.firstName = goog.isDef(firstName) ? firstName : null;

    /**
     * Customer's last name
     * @type {string}
     * @private
     */
    this.lastName  = goog.isDef(lastName) ? lastName : null;

    /**
     * Customer's nick name, if applicable
     * @type {string}
     * @private
     */
    this.nickName  = goog.isDef(nickName) ? nickName : null;

    /**
     * Customer's address if applicable
     * @type {pnc.model.Address}
     * @private
     */
    this.address   = goog.isDef(address) ? address : null;

    /**
     * Customer's contacts
     * @type {Array.<pnc.model.Contact>}
     * @private
     */
    this.contacts  = [];

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};


/**
 * Compares two customers to determine prescedence. Uses customer's first and last name for alpha sorting.
 * @param  {pnc.model.Customer} customer1 First customer to compare
 * @param  {pnc.model.Customer} customer2 Second customer to compare
 * @return {number}
 */
pnc.model.Customer.compare = function(customer1, customer2) {
    var result = goog.string.caseInsensitiveCompare(customer1.getFirstName(), customer2.getFirstName());

    if(result !== 0)
        return result;

    return goog.string.caseInsensitiveCompare(customer1.getLastName(), customer2.getLastName());
};

/**
 * Returns a string representation of the customer. Made using the first and last name.
 * @return {string}
 */
pnc.model.Customer.prototype.toString = function() {
    return this.firstName + ' ' + this.lastName;
};

pnc.model.Customer.prototype.getId = function() {
    return this.id;
};

pnc.model.Customer.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Customer.prototype.getFirstName = function() {
    return this.firstName;
};

pnc.model.Customer.prototype.setFirstName = function(firstName) {
    this.firstName = goog.isDef(firstName) ? firstName : null;
};

pnc.model.Customer.prototype.getLastName = function() {
    return this.lastName;
};

pnc.model.Customer.prototype.setLastName = function(lastName) {
    this.lastName = goog.isDef(lastName) ? lastName : null;
};

pnc.model.Customer.prototype.getNickName = function() {
    return this.nickName;
};

pnc.model.Customer.prototype.setNickName = function(nickName) {
    this.nickName = goog.isDef(nickName) ? nickName : null;
};

pnc.model.Customer.prototype.getAddress = function() {
    return this.address;
};

pnc.model.Customer.prototype.setAddress = function(address) {
    this.address = goog.isDef(address) ? address : null;
};

pnc.model.Customer.prototype.getContacts = function() {
    return this.contacts;
};

pnc.model.Customer.prototype.setContacts = function(contacts) {
    this.contacts = goog.isDefAndNotNull(contacts) ? contacts : [];
};
