goog.provide('pnc.model.CreditLine');
goog.provide('pnc.model.Payment');


/**
 * Model representing a credit line. Credit lines are used to approve a series of invoices or orders to be purchased in
 * credit to be repaid later. Credit lines can be used by the business to sell to customers, as well as by the business
 * to keep track of credit lines they may have with suppliers.
 * @param {number}  id      Unique identifier for the credit line object
 * @param {number}  limit   Maximum amount that can be spent on this credit line account. -1 indicates unlimited.
 * @param {number}  balance Current amount owed in te credit line
 * @param {boolean} active  Indicates if a account is active. If inactive, new credit sales should not be allowed.
 *                            Payments should be permissible.
 * @param {string}  rel     Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states for object
 * @constructor
 */
pnc.model.CreditLine = function(id, limit, balance, active, rel, links) {
    /**
     * Unique identifier for the credit line object
     * @type {number}
     * @private
     */
    this.id       = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Maximum amount that can be spent on this credit line account. -1 indicates unlimited.
     * @type {number}
     * @private
     */
    this.limit    = goog.isDefAndNotNull(limit) ? limit : 0;

    /**
     * Current amount owed in te credit line
     * @type {number}
     * @private
     */
    this.balance  = goog.isDefAndNotNull(balance) ? balance : 0;

    /**
     * Indicates if a account is active. If inactive, new credit sales should not be allowed. Payments should be permissible.
     * @type {number}
     * @private
     */
    this.active   = goog.isDefAndNotNull(active) ? active : false;

    /**
     * Collection of invoices for the account.
     * @type {Array.<pnc.model.Invoice>}
     * @private
     */
    this.invoices = [];

    /**
     * Collection of payments for the account.
     * @type {Array.<pnc.model.Payment>}
     * @private
     */
    this.payments = [];

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta     = new pnc.model.Meta(rel, links);
};

/**
 * Model representing a payment made to a credit line account. Payments can both be received by the business from
 * customers who have credit accounts with it, as well as payments made to suppliers which sell products on credit to
 * the business itself
 * @param {number} id            Unique identifier for the payment object
 * @param {number} idCreditLine  The credit line this payment goes towards
 * @param {number} idUser        ID of the user that received or made the payment for the account
 * @param {Date}   timestamp     Moment in time when the payment was generated
 * @param {number} amount        Amount of money that was contributed to the account
 * @param {string} note          Optional note that describes the payment or provides further information
 * @param {string} rel           Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links  Links with next possible states for object
 * @constructor
 */
pnc.model.Payment = function(id, idCreditLine, idUser, timestamp, amount, note, rel, links) {
    if(!goog.isDefAndNotNull(idUser) || idUser < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    /**
     * Unique identifier for the payment object
     * @type {number}
     * @private
     */
    this.id           = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * The credit line this payment goes towards
     * @type {number}
     * @private
     */
    this.idCreditLine = goog.isDefAndNotNull(idCreditLine) ? idCreditLine : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that received or made the payment for the account
     * @type {number}
     * @private
     */
    this.idUser       = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;

    /**
     * Moment in time when the payment was generated
     * @type {Date}
     * @private
     */
    this.timestamp    = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Amount of money that was contributed to the account
     * @type {number}
     * @private
     */
    this.amount       = goog.isDefAndNotNull(amount) ? amount : 0;

    /**
     * Optional note that describes the payment or provides further information
     * @type {string}
     * @private
     */
    this.note         = goog.isDef(note) ? note : null;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta         = new pnc.model.Meta(rel, links);
};


pnc.model.CreditLine.prototype.getId = function() {
    return this.id;
};

pnc.model.CreditLine.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.CreditLine.prototype.getLimit = function() {
    return this.limit;
};

pnc.model.CreditLine.prototype.setLimit = function(limit) {
    this.limit = goog.isDefAndNotNull(limit) ? limit : 0;
};

pnc.model.CreditLine.prototype.getBalance = function() {
    return this.balance;
};

pnc.model.CreditLine.prototype.setBalance = function(balance) {
    this.balance = goog.isDefAndNotNull(balance) ? balance : 0;
};

pnc.model.CreditLine.prototype.isActive = function() {
    return this.active;
};

pnc.model.CreditLine.prototype.setActive = function(active) {
    this.active = goog.isDefAndNotNull(active) ? active : false;
};

pnc.model.CreditLine.prototype.getInvoices = function() {
    return this.invoices;
};

pnc.model.CreditLine.prototype.getPayments = function() {
    return this.payments;
};


pnc.model.Payment.prototype.getId = function() {
    return this.id;
};

pnc.model.Payment.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Payment.prototype.getIdCreditLine = function() {
    return this.idCreditLine;
};

pnc.model.Payment.prototype.setIdCreditLine = function(idCreditLine) {
    this.idCreditLine = goog.isDefAndNotNull(idCreditLine) ? idCreditLine : 0;
};

pnc.model.Payment.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Payment.prototype.setIdUser = function(idUser) {
    if(!goog.isDefAndNotNull(idUser) || idUser < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    this.idUser = idUser;
};

pnc.model.Payment.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Payment.prototype.setTimestamp = function(timestamp) {
    this.timestmap = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.Payment.prototype.getAmount = function() {
    return this.amount;
};

pnc.model.Payment.prototype.setAmount = function(amount) {
    this.amount = goog.isDefAndNotNull(amount) ? amount : 0;
};

pnc.model.Payment.prototype.getNote = function() {
    return this.note;
};

pnc.model.Payment.prototype.setNote = function(note) {
    this.note = goog.isDef(note) ? note : null;
};
