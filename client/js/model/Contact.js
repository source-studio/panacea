goog.provide('pnc.model.Contact');

goog.require('pnc.model');


/**
 * Model that represents a contact point for a person. Contacts are used for everal models that represent people:
 * customers, institutions, sales people. Any person or insttitution that has a contact information made available.
 * @constructor
 * @param {number} id          Unique identifer for the contact
 * @param {string} uuid        UUID for the contact
 * @param {string} contactType Type of contact. Should be a value from the pnc.model.Contact.ContactType enum
 * @param {string} value       Contact value
 */
pnc.model.Contact = function(id, uuid, contactType, value) {
    /**
     * Unique identifer for the contact
     * @type {number}
     * @private
     */
    this.id          = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * UUID for the contact
     * @type {string}
     * @private
     */
    this.uuid        = goog.isDefAndNotNull(uuid) ? uuid : goog.math.randomInt(1e12).toString();

    /**
     * Type of contact. Should be a value from the pnc.model.Contact.ContactType enum
     * @type {string}
     * @private
     */
    this.contactType = goog.isDefAndNotNull(contactType) && contactType.length > 0 ? contactType : pnc.model.Contact.ContactType.MOBILE_PHONE;

    /**
     * Contact value
     * @type {string}
     * @private
     */
    this.value       = goog.isDef(value) ? value : null;
};

/**
 * Lists possible values for contact type
 * @enum {string}
 */
pnc.model.Contact.ContactType = {
    HOME_PHONE   : 'HOME_PHONE',
    MOBILE_PHONE : 'MOBILE_PHONE',
    WORK_PHONE   : 'WORK_PHONE',
    EMAIL        : 'EMAIL',
    FAX          : 'FAX'
};


/**
 * Returns a string representation of the contact. Uses the first letter of the contact type as a type indicator.
 * @return {string}  String representation
 */
pnc.model.Contact.prototype.toString = function() {
    return ['[', this.contactType.substring(0, 1), '] ', this.value].join('');
};

pnc.model.Contact.prototype.getId = function() {
    return this.id;
};

pnc.model.Contact.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Contact.prototype.getUuid = function() {
    return this.uuid;
};

pnc.model.Contact.prototype.setUuid = function(uuid) {
    this.uuid = uuid;
};

pnc.model.Contact.prototype.getContactType = function() {
    return this.contactType;
};

pnc.model.Contact.prototype.setContactType = function(contactType) {
    this.contactType = contactType;
};

pnc.model.Contact.prototype.getValue = function() {
    return this.value;
};

pnc.model.Contact.prototype.setValue = function(value) {
    this.value = goog.isDef(value) ? value : null;
};
