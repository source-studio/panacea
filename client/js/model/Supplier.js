goog.provide('pnc.model.CreditNote');
goog.provide('pnc.model.Order');
goog.provide('pnc.model.Supplier');

goog.require('goog.string');
goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * Model representing a company or person that sells products to the business for resale to customers.
 * @constructor
 * @param {number} id           Unique identifier for the supplier
 * @param {string} organization Organization or company name
 * @param {string} firstName    First name of primary contact
 * @param {string} lastName     Last name of primary contact
 * @param {string} rel          Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links that describe next possible states
 */
pnc.model.Supplier = function(id, organization, firstName, lastName, rel, links) {
    /**
     * Unique identifier for the supplier
     * @type {number}
     * @private
     */
    this.id           = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Organization or company name
     * @type {string}
     * @private
     */
    this.organization = organization;

    /**
     * First name of primary contact
     * @type {string}
     * @private
     */
    this.firstName    = firstName;

    /**
     * Last name of primary contact
     * @type {string}
     * @private
     */
    this.lastName     = lastName;

    /**
     * Collection of contacts for the supplier
     * @type {Array.<pnc.model.Contact>}
     * @private
     */
    this.contacts     = [];

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta         = new pnc.model.Meta(rel, links);
};

/**
 * Order for goods to resell made from a supplier company
 * @constructor
 * @param {number}  id         Unique identifier for the order
 * @param {number}  idSupplier Id of the supplier from which this order was made
 * @param {Date}    timetamp   Timestamp on which the order was made
 * @param {string}  reference  Optional reference for order. Could be an order ID maintained by the supplier company.
 * @param {number}  amount     Total amount the owed for the order
 * @param {boolean} paid       Wether the order was paid or not
 * @param {date}    paidDate   Date when the order was paid, if such has happened
 * @param {string}  rel        Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links that determine possible changes in state for object
 */
pnc.model.Order = function(id, idSupplier, timestamp, reference, amount, paid, paidDate, rel, links) {
    /**
     * Unique identifier for the order
     * @type {number}
     * @private
     */
    this.id         = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Id of the supplier from which this order was made
     * @type {number}
     * @private
     */
    this.idSupplier = goog.isDefAndNotNull(idSupplier) ? idSupplier : pnc.model.DEFAULT_ID;

    /**
     * Timestamp on which the order was made
     * @type {Date}
     * @private
     */
    this.timestamp  = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Optional reference for order. Could be an order ID maintained by the supplier company.
     * @type {string}
     * @private
     */
    this.reference  = goog.isDef(reference) ? reference : null;

    /**
     * Total amount the owed for the order
     * @type {number}
     * @private
     */
    this.amount     = goog.isDefAndNotNull(amount) ? amount : 0;

    /**
     * Wether the order was paid or not
     * @type {boolean}
     * @private
     */
    this.paid       = goog.isDefAndNotNull(paid) ? paid : false;

    /**
     * Date the order was paid, if such has happened
     * @type {date}
     * @private
     */
    this.paidDate   = goog.isDef(paidDate) ? paidDate : null;

    /**
     * REST metdata
     * @type {pnc.model.Meta}
     */
    this.meta       = new pnc.model.Meta(rel, links);
};

/**
 * A note of credit provided by a supplier to the business when a return of product is done, or in any other situation
 * where the supplier makes a future promise to provide product free of charge or subsidized.
 * @constructor
 * @param {number}  id            Unique identifier for the credit note
 * @param {number}  idSupplier    ID of the supplier that provided the credit note
 * @param {Date}    timestamp     Timestamp when the credit note was given
 * @param {string}  reference     Optional reference for credit note. Could be an ID maintained by the supplier company.
 * @param {number}  amount        Total amount credited
 * @param {string}  reason        Reason why the credit note was given
 * @param {boolean} redeemed      Has this credit note been redeemed yet?
 * @param {date}    redeemedDate  Date on which the credit note was redeemed, if such has happened
 * @param {string}  rel           Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links that provided next possible states for object
 */
pnc.model.CreditNote = function(id, idSupplier, timestamp, reference, amount, reason, redeemed, redeemedDate, rel, links) {
    /**
     * Unique identifier for the credit note
     * @type {number}
     * @private
     */
    this.id         = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the supplier that provided the credit note
     * @type {number}
     * @private
     */
    this.idSupplier = idSupplier;

    /**
     * Timestamp when the credit note was given
     * @type {Date}
     * @private
     */
    this.timestamp  = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Optional reference for credit note. Could be an ID maintained by the supplier company.
     * @type {string}
     * @private
     */
    this.reference  = reference;

    /**
     * Total amount credited
     * @type {number}
     * @private
     */
    this.amount     = goog.isDefAndNotNull(amount) ? amount : 0;

    /**
     * Reason why the credit note was given
     * @type {string}
     * @private
     */
    this.reason     = reason;

    /**
     * Has this credit note been redeemed yet?
     * @type {boolean}
     * @private
     */
    this.redeemed   = goog.isDefAndNotNull(redeemed) ? redeemed : false;

    /**
     * Date on which the credit note was redeemed if such has happened
     * @type {date}
     * @private
     */
    this.redeemedDate = goog.isDef(redeemedDate) ? redeemedDate : null;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta       = new pnc.model.Meta(rel, links);
};

/**
 * Compares two suppliers to determine their prescedence. Sorts by alpha order by organization, first name and last name.
 * @param  {pnc.model.Supplier} supplier1 First supplier to compare
 * @param  {pnc.model.Supplier} supplier2 Second supplier to compare
 * @return {number}
 */
pnc.model.Supplier.compare = function(supplier1, supplier2) {
    var result = goog.string.caseInsensitiveCompare(supplier1.getOrganization(), supplier2.getOrganization());

    if(result !== 0)
        return result;

    result = goog.string.caseInsensitiveCompare(supplier1.getFirstName(), supplier2.getFirstName());

    if(result !== 0)
        return result;

    return goog.string.caseInsensitiveCompare(supplier1.getLastName(), supplier2.getLastName());
};

pnc.model.Order.compare = function(order1, order2) {
    if(order1.getTimestamp() < order2.getTimestamp())
        return -1;
    else if(order1.getTimestamp() > order2.getTimestamp())
        return 1;

    return 0;
};

pnc.model.CreditNote.compare = function(note1, note2) {
    if(note1.getTimestamp() < note2.getTimestamp())
        return -1;
    else if(note1.getTimestamp() > note2.getTimestamp())
        return 1;

    return 0;
};


pnc.model.Supplier.prototype.toString = function() {
    return this.organization;
};

pnc.model.Supplier.prototype.getId = function() {
    return this.id;
};

pnc.model.Supplier.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.Supplier.Keys.DEFAULT_ID;
};

pnc.model.Supplier.prototype.getOrganization = function() {
    return this.organization;
};

pnc.model.Supplier.prototype.setOrganization = function(organization) {
    this.organization = organization;
};

pnc.model.Supplier.prototype.getFirstName = function() {
    return this.firstName;
};

pnc.model.Supplier.prototype.setFirstName = function(firstName) {
    this.firstName = firstName;
};

pnc.model.Supplier.prototype.getLastName = function() {
    return this.lastName;
};

pnc.model.Supplier.prototype.setLastName = function(lastName) {
    this.lastName = lastName;
};

pnc.model.Supplier.prototype.getContacts = function() {
    return this.contacts;
};

pnc.model.Supplier.prototype.setContacts = function(contacts) {
    this.contacts = goog.isDefAndNotNull(contacts) ? contacts : [];
};


pnc.model.Order.prototype.getId = function() {
    return this.id;
};

pnc.model.Order.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Order.prototype.getIdSupplier = function() {
    return this.idSupplier;
};

pnc.model.Order.prototype.setIdSupplier = function(idSupplier) {
    if(!goog.isDefAndNotNull(idSupplier) || idSupplier < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    this.idSupplier = idSupplier;
};

pnc.model.Order.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Order.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.Order.prototype.getReference = function() {
    return this.reference;
};

pnc.model.Order.prototype.setReference = function(reference) {
    this.reference = reference;
};

pnc.model.Order.prototype.getAmount = function() {
    return this.amount;
};

pnc.model.Order.prototype.setAmount = function(amount) {
    this.amount = goog.isDefAndNotNull(amount) ? amount : 0;
};

pnc.model.Order.prototype.isPaid = function() {
    return this.paid;
};

pnc.model.Order.prototype.setPaid = function(paid) {
    this.paid = goog.isDefAndNotNull(paid) ? paid : false;
};

pnc.model.Order.prototype.getPaidDate = function() {
    return this.paidDate;
};

pnc.model.Order.prototype.setPaidDate = function(paidDate) {
    this.paidDate = goog.isDef(paidDate) ? paidDate : null;
};


pnc.model.CreditNote.prototype.getId = function() {
    return this.id;
};

pnc.model.CreditNote.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.CreditNote.prototype.getIdSupplier = function() {
    return this.idSupplier;
};

pnc.model.CreditNote.prototype.setIdSupplier = function(idSupplier) {
    if(!goog.isDefAndNotNull(idSupplier) || idSupplier < 1)
        throw pnc.model.Error.OUT_OF_RANGE;

    this.idSupplier = idSupplier;
};

pnc.model.CreditNote.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.CreditNote.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();
};

pnc.model.CreditNote.prototype.getReference = function() {
    return this.reference;
};

pnc.model.CreditNote.prototype.setReference = function(reference) {
    this.reference = reference;
};

pnc.model.CreditNote.prototype.getAmount = function() {
    return this.amount;
};

pnc.model.CreditNote.prototype.setAmount = function(amount) {
    this.amount = goog.isDefAndNotNull(amount) ? amount : 0;
};

pnc.model.CreditNote.prototype.getReason = function() {
    return this.reason;
};

pnc.model.CreditNote.prototype.setReason = function(reason) {
    this.reason = reason;
};

pnc.model.CreditNote.prototype.isRedeemed = function() {
    return this.redeemed;
};

pnc.model.CreditNote.prototype.setRedeemed = function(redeemed) {
    this.redeemed = goog.isDefAndNotNull(redeemed) ? redeemed : false;
};

pnc.model.CreditNote.prototype.getRedeemedDate = function() {
    return this.redeemedDate;
};

pnc.model.CreditNote.prototype.setRedeemedDate = function(redeemedDate) {
    this.redeemedDate = goog.isDef(redeemedDate) ? redeemedDate : null;
};
