goog.provide('pnc.model.Sponsor');
goog.provide('pnc.model.Sponsorship');

goog.require('goog.string');
goog.require('pnc.model');
goog.require('pnc.model.Meta');


/**
 * An institution that has a sponsor relationship with the business. Institutions will pay a portion of invoices which
 * they choose to sponsor. These will be approved by them before or after the transaction takes place. They will have a
 * credit line associated to them, to keep track of all sponsored transactions, for follow by the business.
 * @param {number}            id           Unique identifier for the sponsor
 * @param {string}            organization Name of the organization
 * @param {string}            firstName    First name of the primary contact within the organization
 * @param {string}            lastName     Last name of the primary contact within the organization
 * @param {pnc.model.Address} address      Address for the organization
 * @param {string}            rel          Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links that provide next possible changes in state
 * @constructor
 */
pnc.model.Sponsor = function(id, organization, firstName, lastName, address, rel, links) {
    /**
     * Unique identifier for the sponsor
     * @type {number}
     * @private
     */
    this.id           = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * Name of the organization
     * @type {string}
     * @private
     */
    this.organization = goog.isDef(organization) ? organization : null;

    /**
     * First name of the primary contact within the organization
     * @type {string}
     * @private
     */
    this.firstName    = goog.isDef(firstName) ? firstName : null;

    /**
     * Last name of the primary contact within the organization
     * @type {string}
     * @private
     */
    this.lastName     = goog.isDef(lastName) ? lastName : null;

    /**
     * Address for the organization
     * @type {string}
     * @private
     */
    this.address      = goog.isDef(address) ? address : null;

    /**
     * Ways to contact the sponsor
     * @type {Array.<pnc.model.Contact>}
     * @private
     */
    this.contacts     = [];

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta         = new pnc.model.Meta(rel, links);
};

/**
 * An instance when an invoice was sponsored. When this happens a sponsor agrees to pay a portion of the invoice total.
 * The customer only needs to pay the remaining portion. The customer pays his portion upon sale. The sponsor pays a
 * collection of sponsorships at a later date.
 * @param {number}  id         Unique identifier for the sponsorship
 * @param {number}  idSponsor  ID of the sponsor that made the sponsorship
 * @param {number}  idInvoice  ID of the invoice that was sponsored
 * @param {date}    timestamp  Date/Time when the sponsorship happened
 * @param {number}  total      Total amount the invoice was sold for
 * @param {number}  sponsored  Amount that was sponsored from the total
 * @param {boolean} paid       Has the sponsorship been paid yet?
 * @param {date}    paidDate   Date the sponsorship was paid, if such has happened
 * @param {string}  rel        Rel descriptor
 * @param {Object.<string, pnc.model.Link>} links  REST links
 * @constructor
 */
pnc.model.Sponsorship = function(id, idSponsor, idInvoice, timestamp, total, sponsored, paid, paidDate, rel, links) {
    /**
     * Unique identifier for the sponsorship
     * @type {number}
     * @private
     */
    this.id        = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;

    /**
     * ID of the sponsor that made the sponsorship
     * @type {number}
     * @private
     */
    this.idSponsor = goog.isDefAndNotNull(idSponsor) ? idSponsor : pnc.model.DEFAULT_ID;

    /**
     * ID of the invoice that was sponsored
     * @type {number}
     * @private
     */
    this.idInvoice = goog.isDefAndNotNull(idInvoice) ? idInvoice : pnc.model.DEFAULT_ID;

    /**
     * Date/Time when the sponsorship happened
     * @type {date}
     * @private
     */
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Total amount the invoice was sold for
     * @type {date}
     * @private
     */
    this.total     = goog.isDefAndNotNull(total) ? total : 0;

    /**
     * Amount that was sponsored from the total
     * @type {number}
     * @private
     */
    this.sponsored = goog.isDefAndNotNull(sponsored) ? sponsored : 0;

    /**
     * Has the sponsorship been paid yet?
     * @type {boolean}
     * @private
     */
    this.paid      = goog.isDef(paid) ? paid : false;

    /**
     * Date the sponsorship was paid, if such has happened
     * @type {date}
     * @private
     */
    this.paidDate  = goog.isDef(paidDate) ? paidDate : null;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};

/**
 * Compares two sponsors to determine prescedence. Orders in alpha order by organization, first name and last name.
 * @param  {pnc.model.Sponsor} sponsor1 First sponsor to compare
 * @param  {pnc.model.Sponsor} sponsor2 Second sponsor to compare
 * @return {number}
 */
pnc.model.Sponsor.compare = function(sponsor1, sponsor2) {
    var result = goog.string.caseInsensitiveCompare(sponsor1.getOrganization(), sponsor2.getOrganization());

    if(result !== 0)
        return result;

    result = goog.string.caseInsensitiveCompare(sponsor1.getFirstName(), sponsor2.getFirstName());

    if(result !== 0)
        return result;

    return goog.string.caseInsensitiveCompare(sponsor1.getLastName(), sponsor2.getLastName());
};


pnc.model.Sponsor.prototype.toString = function() {
    return this.organization;
};

pnc.model.Sponsor.prototype.getId = function() {
    return this.id;
};

pnc.model.Sponsor.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Sponsor.prototype.getOrganization = function() {
    return this.organization;
};

pnc.model.Sponsor.prototype.setOrganization = function(organization) {
    this.organization = goog.isDef(organization) ? organization : null;
};

pnc.model.Sponsor.prototype.getFirstName = function() {
    return this.firstName;
};

pnc.model.Sponsor.prototype.setFirstName = function(firstName) {
    this.firstName = goog.isDef(firstName) ? firstName : null;
};

pnc.model.Sponsor.prototype.getLastName = function() {
    return this.lastName;
};

pnc.model.Sponsor.prototype.setLastName = function(lastName) {
    this.lastName = goog.isDef(lastName) ? lastName : null;
};

pnc.model.Sponsor.prototype.getAddress = function() {
    return this.address;
};

pnc.model.Sponsor.prototype.setAddress = function(address) {
    this.address = goog.isDef(address) ? address : null;
};

pnc.model.Sponsor.prototype.getContacts = function() {
    return this.contacts;
};

pnc.model.Sponsor.prototype.setContacts = function(contacts) {
    this.contacts = goog.isDefAndNotNull(contacts) ? contacts : [];
};


pnc.model.Sponsorship.prototype.getId = function() {
    return this.id;
};

pnc.model.Sponsorship.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Sponsorship.prototype.getIdSponsor = function() {
    return this.idSponsor;
};

pnc.model.Sponsorship.prototype.setIdSponsor = function(idSponsor) {
    this.idSponsor = goog.isDefAndNotNull(idSponsor) ? idSponsor : pnc.model.DEFAULT_ID;
};

pnc.model.Sponsorship.prototype.getIdInvoice = function() {
    return this.idInvoice;
};

pnc.model.Sponsorship.prototype.setIdInvoice = function(idInvoice) {
    this.idInvoice = goog.isDefAndNotNull(idInvoice) ? idInvoice : pnc.model.DEFAULT_ID;
};

pnc.model.Sponsorship.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Sponsorship.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDef(timestamp) ? timestamp : null;
};

pnc.model.Sponsorship.prototype.getTotal = function() {
    return this.total;
};

pnc.model.Sponsorship.prototype.setTotal = function(total) {
    this.total = goog.isDefAndNotNull(total) ? total : 0;
};

pnc.model.Sponsorship.prototype.getSponsored = function() {
    return this.sponsored;
};

pnc.model.Sponsorship.prototype.setSponsored = function(sponsored) {
    this.sponsored = goog.isDefAndNotNull(sponsored) ? sponsored : 0;
};

pnc.model.Sponsorship.prototype.isPaid = function() {
    return this.paid;
};

pnc.model.Sponsorship.prototype.setPaid = function(paid) {
    this.paid = goog.isDefAndNotNull(paid) ? paid : false;
};

pnc.model.Sponsorship.prototype.getPaidDate = function() {
    return this.paidDate;
};

pnc.model.Sponsorship.prototype.setPaidDate = function(paidDate) {
    this.paidDate = goog.isDef(paidDate) ? paidDate : null;
};
