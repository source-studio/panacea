goog.provide('pnc.model.Meta');


/**
 * Model metadata used for RESTful interaction with the server. Using a composition approach rather than "inheritance
 * simulation hacks" available in javascript. Top level models will include a meta property which will encapsulate rel
 * descriptions, state links and other REST metadata.
 *
 * The application takes a RESTful approach to server interaction. Any objects requested from the server may have one or
 * more Links attached to it. These links describe next possible states that can be taken with an object. For example when
 * retriving an invoice, that invoice can be invalidated or refunded. Links will provide details that the client will need
 * in order to perform those transactions.
 * @constructor
 * @param {!string} rel                            Rel descriptor for object
 * @param {Object.<string, pnc.client.Link>} links Link dictionary
 */
pnc.model.Meta = function(rel, links) {
    /**
     * Object rel descriptor. URI unique identifier for each object. Can be used to identify the object in the context of
     * invocations to the server.
     * @type {string}
     * @private
     */
    this.rel_   = rel;

    /**
     * Dictionary of links. Links provide next possible states that can be taken with this object. Keys provide standard
     * rels that identify the link. Values provide link information that can be used to make further requests.
     * @type {Object.<string, pnc.client.Link}
     * @private
     */
    this.links_ = goog.isDefAndNotNull(links) ? links : {};
};

pnc.model.Meta.prototype.rel = function() {
    return this.rel_;
};

pnc.model.Meta.prototype.links = function() {
    return this.links_;
};
