goog.provide('pnc.model.Invalidation');

goog.require('pnc.model');
goog.require('pnc.model.Meta');

/**
 * Represents metadata around an invoice invalidation. Invoices may be decided to be "removed" from the system in the case
 * of errors, or handling customer requests. However, invoices aren't meant to be deleted. They can be invalidated. Once
 * invalidated, they will not be summed up to total sales, but a record of their existence will be kept.
 * @param {number} id         Unique identifer for the invalidation
 * @param {number} idUser     ID of the user that performed the invalidation
 * @param {number} idInvoice  ID of the invoice that was invalidated
 * @param {date}   timestamp  Timestamp when the invoice was invalidated
 * @param {string} reason     Optional explanation why the invoice was invalidated
 * @param {string} rel        Rel descriptor
 * @param {Object.<string, pnc.client.Link>} links Links with next possible states
 * @constructor
 */
pnc.model.Invalidation = function(id, idUser, idInvoice, timestamp, reason, rel, links) {
    /**
     * Unique identifer for the invalidation
     * @type {number} 
     * @private
     */
    this.id        = goog.isDefAndNotNull(id)        ? id        : pnc.model.DEFAULT_ID;

    /**
     * ID of the user that performed the invalidation
     * @type {number}
     * @private
     */
    this.idUser    = goog.isDefAndNotNull(idUser)    ? idUser    : pnc.model.DEFAULT_ID;

    /**
     * ID of the invoice that was invalidated
     * @type {number}
     * @private
     */
    this.idInvoice = goog.isDefAndNotNull(idInvoice) ? idInvoice : pnc.model.DEFAULT_ID;

    /**
     * Timestamp when the invoice was invalidated
     * @type {date}
     * @private
     */
    this.timestamp = goog.isDefAndNotNull(timestamp) ? timestamp : new Date();

    /**
     * Optional explanation why the invoice was invalidated
     * @type {string}
     * @private
     */
    this.reason    = goog.isDef(reason)              ? reason    : null;

    /**
     * REST metadata
     * @type {pnc.model.Meta}
     */
    this.meta      = new pnc.model.Meta(rel, links);
};

pnc.model.Invalidation.prototype.getId = function() {
    return this.id;
};

pnc.model.Invalidation.prototype.setId = function(id) {
    this.id = goog.isDefAndNotNull(id) ? id : pnc.model.DEFAULT_ID;
};

pnc.model.Invalidation.prototype.getIdUser = function() {
    return this.idUser;
};

pnc.model.Invalidation.prototype.setIdUser = function(idUser) {
    this.idUser = goog.isDefAndNotNull(idUser) ? idUser : pnc.model.DEFAULT_ID;
};

pnc.model.Invalidation.prototype.getIdInvoice = function() {
    return this.idInvoice;
};

pnc.model.Invalidation.prototype.setIdInvoice = function(idInvoice) {
    this.idInvoice = goog.isDefAndNotNull(idInvoice) ? idInvoice : pnc.model.DEFAULT_ID;
};

pnc.model.Invalidation.prototype.getTimestamp = function() {
    return this.timestamp;
};

pnc.model.Invalidation.prototype.setTimestamp = function(timestamp) {
    this.timestamp = goog.isDef(timestamp) ? timestamp : null;
};

pnc.model.Invalidation.prototype.getReason = function() {
    return this.reason;
};

pnc.model.Invalidation.prototype.setReason = function(reason) {
    this.reason = goog.isDef(reason) ? reason : null;
};
