goog.provide('pnc.model.Link');


/**
 Represents a hypermedia link. Links are used so that server programs can dictate next possible states in an application
 flow instead of leaving the responsability to the client. Google REST/HATEOAS for more info. While there is no perfect
 standard for these links.

 When retrieving elements from the server, each element will come with it's set of links. The client should always follow
 the provided links to know which possible next steps are available. This will avoid double logic on client/server, easier
 upgrades to state logic, and avoid URL construction on the client side.

 There are some minor exceptions to this approach. Some URIs will be used for search criteria, looking up ranges, etc.
 In these cases, it is not easy to know before hand what the URI will be. In these cases, a partial URI is returned, and
 the client must complete it to perform the transaction. These are exceptions, in the rest of cases, URIs should be used
 as returned from the server.
 * @param {string} contentType  Will dictate the type of content that will be received when making this request.
 * @param {string} rel          Relation identifier key. Should be unique in the app.
 * @param {string} method       HTTP method which should be invoked to perform this transaction
 * @param {string} uri          URI which should be invoked.
 * @constructor
 */
pnc.model.Link = function(contentType, rel, method, uri) {
    /**
     * Will dictate the type of content that will be received when making this request.
     * @type {string}
     * @private
     */
    this.contentType_ = contentType;

    /**
     * Relation identifier key. Should be unique in the app.
     * @type {string}
     * @private
     */
    this.rel_         = rel;

    /**
     * HTTP method which should be invoked to perform this transaction
     * @type {string}
     * @private
     */
    this.method_      = method;

    /**
     * URI which should be invoked.
     * @type {string}
     * @private
     */
    this.uri_         = uri;
};


pnc.model.Link.prototype.contentType = function() {
    return this.contentType_;
};

pnc.model.Link.prototype.rel = function() {
    return this.rel_;
};

pnc.model.Link.prototype.method = function() {
    return this.method_;
};

pnc.model.Link.prototype.uri = function() {
    return this.uri_;
};
