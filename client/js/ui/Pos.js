goog.provide('pnc.ui.Pos');

goog.require('goog.array');
goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');
goog.require('goog.fx.Animation');
goog.require('goog.fx.dom.FadeOutAndHide');
goog.require('goog.fx.dom.ResizeHeight');
goog.require('goog.json');
goog.require('goog.net.XhrIo');
goog.require('goog.object');
goog.require('goog.string');
goog.require('goog.ui.Component');
goog.require('goog.ui.ac');
goog.require('goog.ui.ac.AutoComplete');

goog.require('pnc.client.CreditLine');
goog.require('pnc.client.Customer');
goog.require('pnc.client.Inventory');
goog.require('pnc.client.Product');
goog.require('pnc.client.Profile');
goog.require('pnc.model.Invoice');
goog.require('pnc.model.InvoiceItem');
goog.require('pnc.model.Product');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');

/**
 * Point of sale module
 * @constructor
 */
pnc.ui.Pos = function() {
    /**
     * A map of products. Keys are the string representaiton of the product, values are product objects. Used for the
     * search function
     * @type {Object.<string, pnc.model.Product>}
     * @private
     */
    this.productMap    = {};

    /**
     * A map of customers. Keys are string representations of the customer, values are customer objects. Used for the 
     * customer search function
     * @type {Object.<stirng, pnc.model.Customer>}
     * @private
     */
    this.customerMap   = {};

    /**
     * A map of sponsors. Keys are string representations of the sponsor, values are sponsor objects. Used for the 
     * auto complete in the sponsor search function.
     * @type {Object.<string, pnc.model.Sponsor>}
     * @private
     */
    this.sponsorMap    = {};

    /**
     * A map of items and products. Used to calculate invoice totals.
     * @type {Object.<number, pnc.model.Product>}
     */
    this.items         = {};

    /**
     * The current invoice that is being processed
     * @type {pnc.model.Invoice}
     * @private
     */
    this.invoice       = new pnc.model.Invoice(null, pnc.client.Profile.getUser().getId());

    /**
     * The credit line for the currently selected customer, if applicable
     * @type {pnc.model.CreditLine}
     * @private
     */
    this.creditLine    = null;

    /**
     * Invoice API client used to make backend calls.
     * @type {pnc.client.Invoice}
     * @private
     */
    this.client        = new pnc.client.Invoice();

    /**
     * Collection of components that can be disposed
     * @type {Array}
     * @private
     */
    this.disposables   = [];

    //------------- elements -------------//
    /** @private */ this.container        = null;
    /** @private */ this.paymentSelect    = null;
    /** @private */ this.customerSearch   = null;
    /** @private */ this.customerAc       = null;
    /** @private */ this.sponsorSearch    = null;
    /** @private */ this.sponsorAc        = null;
    /** @private */ this.brandSearch      = null;
    /** @private */ this.productSearch    = null;
    /** @private */ this.productAdd       = null;
    /** @private */ this.productTable     = null;
    /** @private */ this.discountInput    = null;
    /** @private */ this.sponsoredInput   = null;
    /** @private */ this.totalValue       = null;
    /** @private */ this.purchaseButton   = null;
    /** @private */ this.customerContent  = null;
    /** @private */ this.inventoryContent = null;
};
pnc.ui.Main.registerModule('pos', pnc.ui.Pos);

/**
 * Human readable values for the pnc.model.Invoice.Payment enumeration
 * @enum {string}
 * @private
 */
pnc.ui.Pos.PaymentType = {
    CASH      : 'Efectivo',
    CREDIT    : 'Credito',
    QUOTE     : 'Cotizacion',
    SPONSORED : 'Patrocinio'
};

/**
 * Human readable representations of the pnc.model.Contact.ContactType enumeration.
 * Represents contact types as abbreviations.
 * @enum {string}
 * @private
 */
pnc.ui.Pos.ContactType = {
    HOME_PHONE   : '(H)',
    MOBILE_PHONE : '(M)',
    WORK_PHONE   : '(T)',
    EMAIL        : '(E)',
    FAX          : '(F)'
};


/**
 * Renders the point of sale module
 * @param {Element} parent  Parent element where the module will be rendered
 */
pnc.ui.Pos.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderPaymentPane();
    this.renderCustomerPane();
    this.renderSearchPane();
    this.renderProductPane();
    this.renderInventoryPane();
    this.populatePendingInvoice();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Punto de Venta');
};

/**
 * Disposes the point of sale
 */
pnc.ui.Pos.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.array.clear(this.disposables);
    goog.dom.removeNode(this.container);
};

/**
 * Renders the pane that defines the way the invoice will be paid, and allows to optionally assign the invoice to a customer.
 * @private
 */
pnc.ui.Pos.prototype.renderPaymentPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.COG_GRAY, 'Factura');
    var paymentLabel = goog.dom.createDom('label', 'span1', 'Pago');
    goog.dom.appendChild(content, paymentLabel);

    this.paymentSelect = new goog.ui.Select();
    this.paymentSelect.render(content);
    var caption = goog.dom.getElementByClass('goog-menu-button-caption', this.paymentSelect.getElement());
    caption.style.minWidth = '70px';
    this.disposables.push(this.paymentSelect);

    for(var key in pnc.model.Invoice.Payment) {
        var item = new goog.ui.MenuItem(pnc.ui.Pos.PaymentType[key]);
        this.paymentSelect.addItem(item);
        item.setValue(key);
    }

    this.paymentSelect.setValue(pnc.model.Invoice.Payment.CASH);
    var self     = this;
    var callback = function(e) { self.invoice.setPayment(self.paymentSelect.getValue()); };
    goog.events.listen(this.paymentSelect, goog.ui.Component.EventType.ACTION, callback);
    var expanded = false;
    var callback = function() {
        self.sponsorSearch.value = '';
        pnc.ui.closePill(goog.dom.getElement('sponsor-search-pill'));
        var display = '';
        var resize1, resize2 = null;
        var sponsoredPercent = goog.dom.getElement('sponsored-percent');

        if(self.paymentSelect.getValue() === pnc.model.Invoice.Payment.SPONSORED) {
            resize1       = new goog.fx.dom.ResizeHeight(content, 40, 80, 300);
            resize2       = new goog.fx.dom.ResizeHeight(sponsoredPercent.parentElement, 40, 70, 300);
            var callback1 = function() { sponsorRow.style.display = 'initial'; };
            var callback2 = function() { sponsoredPercent.style.display = 'initial'; };
            goog.events.listen(resize1, goog.fx.Animation.EventType.END, callback1);
            goog.events.listen(resize2, goog.fx.Animation.EventType.END, callback2);

            if(!expanded) {
                resize1.play();
                resize2.play();
                expanded = true;
            }
        } else {
            sponsorRow.style.display       = 'none';
            sponsoredPercent.style.display = 'none';
            self.sponsoredInput.value      = '0.00';
            resize1  = new goog.fx.dom.ResizeHeight(content, 80, 40, 300);
            resize2  = new goog.fx.dom.ResizeHeight(sponsoredPercent.parentElement, 70, 40, 300);

            if(expanded) {
                resize1.play();
                resize2.play();
                expanded = false;
            }
        }
        self.calculateTotal();
    };
    goog.events.listen(this.paymentSelect, goog.ui.Component.EventType.ACTION, callback);

    var customerLabel = goog.dom.createDom('label', 'span1', 'Cliente');
    goog.dom.appendChild(content, customerLabel);
    this.customerSearch = goog.dom.createDom('input', {'class' : 'span3', 'type' : 'text'});
    goog.dom.appendChild(content, this.customerSearch);
    this.customerAc = goog.ui.ac.createSimpleAutoComplete([], this.customerSearch, false, true);
    this.disposables.push(this.customerAc);
    var pill = pnc.ui.createPill(this.customerSearch, this.customerAc);
    pill.id  = 'customer-search-pill';
    var callback = function() { 
        goog.dom.removeChildren(self.customerContent);
        self.creditLine = null;
    };
    pnc.ui.onPillClose(pill, callback);

    var customerLink     = pnc.client.Profile.getLink(pnc.client.Customer.Rel.SEARCH);
    var decoder          = goog.partial(pnc.client.decodeCollection, pnc.client.Customer.decode);
    var customerResolver = function() { return self.customerSearch.value; };
    pnc.ui.updateAcData(this.customerSearch, this.customerAc, customerLink, {'{name}' : customerResolver}, decoder, this.customerMap);

    var sponsorRow = goog.dom.createDom('div', {'class' : 'span6 row', 'style' : 'display: none'});
    goog.dom.appendChild(content, sponsorRow);

    var sponsorLabel = goog.dom.createDom('label', {'style' : 'margin-left: 180px'}, 'Patrocinador');
    goog.dom.appendChild(sponsorRow, sponsorLabel);
    this.sponsorSearch = goog.dom.createDom('input', {'class' : 'span3', 'type' : 'text'});
    goog.dom.appendChild(sponsorRow, this.sponsorSearch);
    this.sponsorAc = goog.ui.ac.createSimpleAutoComplete([], this.sponsorSearch, false, true);
    this.disposables.push(this.sponsorAc);
    pill = pnc.ui.createPill(this.sponsorSearch, this.sponsorAc);
    pill.id = 'sponsor-search-pill';
    
    var sponsorLink = pnc.client.Profile.getLink(pnc.client.Sponsor.Rel.SEARCH);
    decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Sponsor.decode);
    var sponsorResolver = function() { return self.sponsorSearch.value; };
    pnc.ui.updateAcData(this.sponsorSearch, this.sponsorAc, sponsorLink, {'{org}' : sponsorResolver}, decoder, this.sponsorMap);
};

/**
 * Renders the search pane of the point of sale. Provides the user the ability to search products by name
 * and filter by brand.
 * @private
 */
pnc.ui.Pos.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    var row = goog.dom.createDom('div', 'row');
    goog.dom.appendChild(content, row);
    var brandTitle = goog.dom.createDom('label', 'span1', 'Marca');
    goog.dom.appendChild(row, brandTitle);
    this.brandSearch = goog.dom.createDom('input', {'class' : 'span4', 'type' : 'text'});
    goog.dom.appendChild(row, this.brandSearch);
    var brandAc = goog.ui.ac.createSimpleAutoComplete([], this.brandSearch, false, true);
    this.disposables.push(brandAc);
    var brandPill = pnc.ui.createPill(this.brandSearch, brandAc);
    brandPill.id  = 'brand-search-pill';

    row = goog.dom.createDom('div', 'row');
    goog.dom.appendChild(content, row);
    var productTitle = goog.dom.createDom('label', 'span1', 'Producto');
    goog.dom.appendChild(row, productTitle);
    var searchGroup = goog.dom.createDom('div', 'input-append');
    goog.dom.appendChild(row, searchGroup);
    this.productSearch = goog.dom.createDom('input', {'class' : 'span4', 'type' : 'text', 'style' : 'width: 210px'});
    goog.dom.appendChild(searchGroup, this.productSearch);
    var productAc = goog.ui.ac.createSimpleAutoComplete([], this.productSearch, false, true);
    this.disposables.push(productAc);
    var productPill  = pnc.ui.createPill(this.productSearch, productAc);
    productPill.id   = 'product-search-pill';
    productPill.style.width = '202px';
    this.productAdd  = goog.dom.createDom('button', {'id' : 'product-add', 'class' : 'btn btn-primary', 'disabled' : true}, 'Agregar');
    goog.dom.appendChild(searchGroup, this.productAdd);
    var callback = function() {
        self.addCallback();
        pnc.ui.closePill(productPill);
    };
    goog.events.listen(this.productAdd, goog.events.EventType.CLICK, callback);

    callback = function() { productAc.getMatcher().setRows([]); };
    goog.events.listen(brandAc, goog.ui.ac.AutoComplete.EventType.UPDATE, callback);

    var brandLink     = pnc.client.Profile.getLink(pnc.client.Product.Rel.SEARCH_BRANDS);
    var nameLink      = pnc.client.Profile.getLink(pnc.client.Product.Rel.SEARCH);
    var self          = this;
    var valueResolver = function() { return self.brandSearch.value; };
    pnc.ui.updateAcData(this.brandSearch, brandAc, brandLink, {'{brand}' : valueResolver}, goog.json.unsafeParse);

    var brandCallback = function(e) {
        if(goog.events.KeyCodes.isCharacterKey(e.keyCode) && goog.events.KeyCodes.isTextModifyingKeyEvent(e)) {
            pnc.ui.closePill(productPill);
            self.productSearch.value = '';
        }
    };
    goog.events.listen(this.brandSearch, goog.events.EventType.KEYUP, brandCallback);

    var xhrCallback = function(e) {
        goog.object.clear(self.productMap);

        switch(e.target.getStatus()) {
            case pnc.client.Status.OK:
                var products = pnc.client.decodeCollection(pnc.client.Product.decode, e.target.getResponseText());
                var data     = [];

                for(var cont = 0, size = products.length; cont < size; cont++) {
                    data[cont] = products[cont].toString();
                    self.productMap[data[cont]] = products[cont];
                }

                productAc.matcher_.setRows(data);
                break;

            case pnc.client.Status.NOT_FOUND:
                productAc.matcher_.setRows([]);
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(e.target)).open();
                break;
        }
    };

    var inputCallback = function(e) {
        var productValue = goog.string.trim(self.productSearch.value);
        var brandValue   = goog.string.trim(self.brandSearch.value);
        self.productAdd.disabled = productValue.length < 1;

        if(productValue.length < 1 || !goog.events.KeyCodes.isCharacterKey(e.keyCode))
            return;

        var method, uri = null;

        if(brandValue.length > 0) {
            method = nameLink.method();
            uri    = nameLink.uri().replace('{brand}', goog.string.urlEncode(brandValue));
            uri    = uri.replace('{name}', goog.string.urlEncode(productValue));
        } else {
            method = nameLink.method();
            uri    = nameLink.uri().replace('{name}', goog.string.urlEncode(productValue));
            uri    = uri.replace('{brand}', '');
        }

        goog.net.XhrIo.send(uri, xhrCallback, method);
    };
    goog.events.listen(this.productSearch, goog.events.EventType.KEYUP, inputCallback);
};

/**
 * Renders the product pane. Provides a list of all products which have been included in the invoice.
 * @private
 */
pnc.ui.Pos.prototype.renderProductPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SHOPPING_CART, 'Productos');
    this.productTable = goog.dom.createDom('table', {'class' : 'table table-striped table-bordered', 'style' : 'width: 520px'});
    goog.dom.appendChild(content, this.productTable);
    var thead = goog.dom.createDom('thead');
    goog.dom.appendChild(this.productTable, thead);
    var tbody = goog.dom.createDom('tbody', {'id' : 'product-table'});
    goog.dom.appendChild(this.productTable, tbody);
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(thead, row);

    var cols = {
        '#'         : '20px',
        'Nombre'    : '150px',
        'Precio'    : '50px',
        'Sub-Total' : '50px',
        ''          : '20px'
    };

    for(var key in cols) {
        var col = goog.dom.createDom('th', {'style' : 'width: ' + cols[key]}, key);
        goog.dom.appendChild(row, col);
    }

    var totals = goog.dom.createDom('div', 'pos-totals');
    goog.dom.appendChild(content, totals);
    var row = goog.dom.createDom('div', 'row');
    goog.dom.appendChild(totals, row);
    var discountLabel = goog.dom.createDom('label', 'span1', 'Descuento');
    goog.dom.appendChild(row, discountLabel);
    var discountValue = goog.dom.createDom('div', {'class' : 'span2 input-append', 'style': 'display: inline-block; margin-left: 10px'});
    goog.dom.appendChild(row, discountValue);
    this.discountInput = goog.dom.createDom('input', {'type' : 'text', 'style' : 'width: 50px', 'value' : '0.00'});
    goog.dom.appendChild(discountValue, this.discountInput);
    pnc.ui.assureNumbers(this.discountInput);
    goog.events.listen(this.discountInput, goog.events.EventType.KEYUP, goog.bind(this.calculateTotal, this));
    var percentLabel = goog.dom.createDom('span', 'add-on bold', '%');
    goog.dom.appendChild(discountValue, percentLabel);

    var totalLabel = goog.dom.createDom('label', 'span1 offset1', 'Total');
    goog.dom.appendChild(row, totalLabel);
    this.totalValue = goog.dom.createDom('label', {'class' : 'span2 invoice-total', 'style' : 'font-size: 16px'}, '0.00');
    goog.dom.appendChild(row, this.totalValue);

    var sponsorRow = goog.dom.createDom('div', {'id' : 'sponsored-percent', 'class' : 'row span6 input-append', 'style' : 'display : none'});
    goog.dom.appendChild(totals, sponsorRow);
    var sponsoredLabel = goog.dom.createDom('label', 'span1', 'Patrocinio');
    goog.dom.appendChild(sponsorRow, sponsoredLabel);
    this.sponsoredInput = goog.dom.createDom('input', {'type' : 'text', 'style' : 'width: 50px; margin-left: 10px', 'value' : '0.00'});
    goog.dom.appendChild(sponsorRow, this.sponsoredInput);
    pnc.ui.assureNumbers(this.sponsoredInput);
    goog.events.listen(this.sponsoredInput, goog.events.EventType.KEYUP, goog.bind(this.calculateTotal, this));
    percentLabel = goog.dom.createDom('span', 'add-on bold', '%');
    goog.dom.appendChild(sponsorRow, percentLabel);

    var controlPanel = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPanel);
    var resetButton = goog.dom.createDom('button', 'btn', 'Limpiar');
    goog.dom.appendChild(controlPanel, resetButton);
    goog.events.listen(resetButton, goog.events.EventType.CLICK, goog.bind(this.clear, this));

    this.purchaseButton = goog.dom.createDom('button', {'class' : 'btn btn-primary', 'disabled' : 'true'}, 'Vender');
    goog.dom.appendChild(controlPanel, this.purchaseButton);
    goog.events.listen(this.purchaseButton, goog.events.EventType.CLICK, goog.bind(this.finalizeSale, this));
};

/**
 * Invoked to handle the case when a product needs to be added to the product pane.
 * @private
 */
pnc.ui.Pos.prototype.addCallback = function() {
    this.productSearch.focus();
    var product = this.productMap[this.productSearch.value];

    if(!goog.isDefAndNotNull(product)) {
        pnc.ui.Alert.error('Este product no existe');
        return;
    }

    if(this.invoice.hasProduct(product.getId())) {
        pnc.ui.Alert.info('Ya este product existe en la factura');
        return;
    }

    this.brandSearch.value       = '';
    this.productSearch.value     = '';
    this.purchaseButton.disabled = false;
    var item                     = new pnc.model.InvoiceItem();
    item.setIdProduct(product.getId());

    if(product.getMinimum() > 1)
        item.setQuantity(product.getMinimum());

    this.invoice.getItems().push(item);
    this.items[item.getUuid()] = product;
    var body = goog.dom.getElement('product-table');
    this.renderItem(body, product, item);
    this.calculateTotal();
};

/**
 * Renders an item in the product pane
 * @param  {Element}               parent   Parent element where the item will be rendered
 * @param  {pnc.model.Product}     product  Product for which the rendered the item for
 * @param  {pnc.model.InvoiceItem} item     Invoice item which is being represented
 * @private
 */
pnc.ui.Pos.prototype.renderItem = function(parent, product, item) {
    var product = this.items[item.getUuid()];
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);

    var inventoryClient = new pnc.client.Inventory();
    var self = this;
    var clickCallback = function(e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(row, 'info');
        self.populateInventoryPane(inventoryClient, product);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, clickCallback);

    var cell = goog.dom.createDom('td', {'style' : 'padding: 4px 8px'});
    goog.dom.appendChild(row, cell);
    var input = goog.dom.createDom('input', {'type' : 'text', 'style' : 'width: 35px; height: 15px; margin: 0 -15px 0 0', 'value' : item.getQuantity().toString()});
    goog.dom.appendChild(cell, input);

    cell = goog.dom.createDom('td', {}, product.getName());
    goog.dom.appendChild(row, cell);

    cell = goog.dom.createDom('td', {'style' : 'text-align: right'}, pnc.ui.DECIMAL_FORMATTER.format(product.getPrice()));
    goog.dom.appendChild(row, cell);

    var subtotalCell = goog.dom.createDom('td', {'style' : 'text-align: right'}, pnc.ui.DECIMAL_FORMATTER.format(product.getPrice() * item.getQuantity()));
    goog.dom.appendChild(row, subtotalCell);

    cell = goog.dom.createDom('td', {'style' : 'padding: 4px'});
    goog.dom.appendChild(row, cell);
    var button = goog.dom.createDom('button', {'class' : 'btn btn-mini', 'style' : 'margin: 2px'});
    goog.dom.appendChild(cell, button);
    var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.REMOVE, 'title' : 'Remover', 'style' : 'width: 16px'});
    goog.dom.appendChild(button, img);

    var downCallback = function(e) {
        if(goog.events.KeyCodes.isCharacterKey(e.keyCode))
            if(e.shiftKey || e.keyCode < goog.events.KeyCodes.ZERO || e.keyCode > goog.events.KeyCodes.NINE)
                e.preventDefault();
    };
    goog.events.listen(input, goog.events.EventType.KEYDOWN, downCallback);

    var upCallback = function(e) {
        var quantity = input.value.length > 0 ? pnc.ui.round(input.value) : 1; 
        item.setQuantity(quantity);
        var total = pnc.ui.DECIMAL_FORMATTER.format(product.getPrice() * item.getQuantity());
        goog.dom.setTextContent(subtotalCell, total);
        self.calculateTotal();

        if(input.value.length > 0 && product.getMinimum() > 1 && quantity < product.getMinimum()) {
            pnc.ui.Alert.warning('La cantidad minima de ' + product.toString() + ' que se puede vender es ' + product.getMinimum());
        } else {
            goog.dom.classes.remove(input, 'input-error');
        }
    };
    goog.events.listen(input, goog.events.EventType.KEYUP, upCallback);

    var blurCallback = function(e) {
        if(input.value.length > 0)
            return;

        var minimum = product.getMinimum() > 1 ? product.getMinimum() : 1;

        input.value = minimum.toString();
        item.setQuantity(minimum);
        goog.dom.setTextContent(subtotalCell, pnc.ui.DECIMAL_FORMATTER.format(product.getPrice()));
    };
    goog.events.listen(input, goog.events.EventType.BLUR, blurCallback);

    var animCallback   = function(e) { goog.dom.removeNode(row); };
    var removeCallback = function(e) {
        e.stopPropagation();
        goog.array.remove(self.invoice.getItems(), item);
        goog.object.remove(self.items, item.getUuid());
        self.purchaseButton.disabled = self.invoice.getItems().length < 1;
        self.calculateTotal();

        var anim = new goog.fx.dom.FadeOutAndHide(row, 300);
        goog.events.listen(anim, goog.fx.Animation.EventType.END, animCallback);
        anim.play();
    };
    goog.events.listen(button, goog.events.EventType.CLICK, removeCallback);
};

/**
 * Renders the pane that displays full context information about the selected customer.
 * @private
 */
pnc.ui.Pos.prototype.renderCustomerPane = function() {
    this.customerContent = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.USER_GRAY, 'Cliente');
    this.customerContent.style.minHeight = '203px';
    var customerClient   = new pnc.client.Customer();
    var creditLineClient = new pnc.client.CreditLine();
    var self = this;

    var creditLineCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.NOT_FOUND:
                //nothing
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.creditLine = response.content();

        if(!goog.isDefAndNotNull(self.creditLine))
            return;

        var data = [
            ['Balance',    pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getBalance())],
            ['Limite',     self.creditLine.getLimit() > -1 ? pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getLimit()) : 'Ilimitado'],
            ['Disponible', self.creditLine.getLimit() > -1 ? pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getLimit() - self.creditLine.getBalance()) : 'Ilimitado'],
            ['Activo',     self.creditLine.isActive() ? 'Si' : 'No']
        ];
        pnc.ui.createSummaryTable(self.customerContent, 'pos-table summary-table table-condensed', 2, 'Linea de Credito', data);
    };

    var customerCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }
        
        var customer  = response.content();
        var nameValue = goog.dom.createDom('label', 'span3', customer.toString());
        goog.dom.appendChild(self.customerContent, nameValue);

        if(customer.contacts.length > 0) {
            var value = pnc.ui.Pos.ContactType[customer.contacts[0].getContactType()] + ' ' + customer.contacts[0].getValue();
            var contactValue = goog.dom.createDom('label', 'span3', value);
            goog.dom.appendChild(self.customerContent, contactValue);
        }

        creditLineClient.getByCustomer(customer, creditLineCallback);
    };

    var updateCallback = function(e) {
        goog.dom.removeChildren(self.customerContent);
        var customer = self.customerMap[e.row];
        customerClient.get(customer.getId(), customerCallback);
    };
    goog.events.listen(this.customerAc, goog.ui.ac.AutoComplete.EventType.UPDATE, updateCallback);
};

/**
 * Renders the pane that displays full context informationa about the available inventory for a product.
 * @private
 */
pnc.ui.Pos.prototype.renderInventoryPane = function() {
    this.inventoryContent = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Producto');
    this.inventoryContent.style.minHeight = '163px';
    var icon = goog.dom.getElementsByTagNameAndClass('img', null, this.inventoryContent.parentElement)[0];
    icon.style.width = '12px';
};

/**
 * Populate the contents of the inventory pane. This will be invoked when the user clicks on a product in the product
 * table. It will show further info on the product, including the available existence.
 * @param  {pnc.client.Inventory} client   Inventory client. Used to request inventory data for the product.
 * @param  {pnc.model.Product}    product  Product for which further info will be displayed.
 * @private
 */
pnc.ui.Pos.prototype.populateInventoryPane = function(client, product) {
    goog.dom.removeChildren(this.inventoryContent);
    var self = this;

    var callback = function(response) {
        var total     = 0;

        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                break;

            case pnc.client.Status.OK:
                var inventory = response.content();
                var today     = new Date();

                for(var cont = 0, size = inventory.length; cont < size; cont++) {
                    var i = inventory[cont];

                    if(!goog.isDefAndNotNull(i.getExpiration()) || i.getExpiration() > today)
                        total += i.getRemaining();
                }
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var data = [
            ['Marca',      product.getBrand()],
            ['Nombre',     product.getName()],
            ['Existencia', total.toString()],
            ['Precio',     pnc.ui.DECIMAL_FORMATTER.format(product.getPrice())]
        ];

        var table = pnc.ui.createSummaryTable(self.inventoryContent, 'pos-table summary-table table-condensed', 2, 'Inventario', data);
        table.style.marginBottom = '0px';

        if(product.getMinimum() > 1)
            pnc.ui.createSummaryContent(table, 'Venta Minima', product.getMinimum().toString());
    };
    client.getByProduct(product, callback);
};

/**
 * Populates an invoice if left in the dropbox. Does nothing if the dropbox is empty.
 * Other modules may leave an invoice in the dropbox to be populated by the point of sale for various reasons.
 * @private
 */
pnc.ui.Pos.prototype.populatePendingInvoice = function() {
    if(!goog.isDefAndNotNull(pnc.ui.Main.dropbox) || pnc.ui.Main.dropbox[0] !== 'copy')
        return;

    var invoice         = pnc.ui.Main.dropbox[1];
    var products        = pnc.ui.Main.dropbox[2];
    pnc.ui.Main.dropbox = null;
    var body            = goog.dom.getElement('product-table');

    for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
        var item    = invoice.getItems()[cont];
        var product = pnc.model.findModel(products, item.getIdProduct());
        this.items[item.getUuid()] = product;
        this.invoice.getItems().push(item);
        this.renderItem(body, product, item);
    }

    this.calculateTotal();
    this.purchaseButton.disabled = false;
};

/**
 * Generates a popup which gives the user a summary of the invoice, a total for the transaction, and if the user
 * is going to collect cash, helps determine the amount that needs to be returned to the customer, if any.
 * @private
 */
pnc.ui.Pos.prototype.finalizeSale = function() {
    if(!this.validateCredit() || !this.validateSponsored())
        return;

    var panel     = goog.dom.createDom('div', 'pos-confirm');
    var sponsored = 0;
    
    if(this.invoice.getDiscount() > 0 || this.sponsoredInput.value > 0) {
        var row = goog.dom.createDom('div', 'row');
        goog.dom.appendChild(panel, row);
        var subTotalLabel = goog.dom.createDom('label', 'span2', 'Sub-Total');
        goog.dom.appendChild(row, subTotalLabel);
        var total         = pnc.ui.DECIMAL_FORMATTER.format(this.invoice.getTotal());
        var subTotalValue = goog.dom.createDom('label', 'invoice-summary-value span1', total);
        goog.dom.appendChild(row, subTotalValue);
    }

    if(this.invoice.getDiscount() > 0) {
        row = goog.dom.createDom('div', 'row');
        goog.dom.appendChild(panel, row);
        var discountLabel = goog.dom.createDom('label', 'span2', 'Descuento');
        goog.dom.appendChild(row, discountLabel);
        var discount      = pnc.ui.DECIMAL_FORMATTER.format(this.invoice.getDiscount());
        var discountValue = goog.dom.createDom('label', 'invoice-summary-value span1', discount);
        goog.dom.appendChild(row, discountValue);
    }

    if(this.invoice.getPayment() === pnc.model.Invoice.Payment.SPONSORED) {
        row = goog.dom.createDom('div', 'row');
        goog.dom.appendChild(panel, row);
        var sponsoredLabel = goog.dom.createDom('label', 'span2', 'Patrocinado');
        goog.dom.appendChild(row, sponsoredLabel);
        sponsored = this.invoice.getTotal() * pnc.ui.round(this.sponsoredInput.value) / 100;
        var sponsoredValue = goog.dom.createDom('label', 'invoice-summary-value span1', pnc.ui.DECIMAL_FORMATTER.format(sponsored));
        goog.dom.appendChild(row, sponsoredValue);
    }

    row = goog.dom.createDom('div', 'row');
    goog.dom.appendChild(panel, row);
    var totalLabel = goog.dom.createDom('label', 'span2', 'Total');
    goog.dom.appendChild(row, totalLabel);

    total          = pnc.ui.DECIMAL_FORMATTER.format(this.invoice.getTotal() - this.invoice.getDiscount() - sponsored);
    var totalValue = goog.dom.createDom('label', 'invoice-summary-value span1 bold', total);
    goog.dom.appendChild(row, totalValue);
    var self = this;

    if(this.invoice.getPayment() === pnc.model.Invoice.Payment.CASH || this.invoice.getPayment() === pnc.model.Invoice.Payment.SPONSORED) {
        row = goog.dom.createDom('div', {'class' : 'row input-prepend'});
        goog.dom.appendChild(panel, row);
        var cashLabel = goog.dom.createDom('label', 'span2', 'Efectivo');
        goog.dom.appendChild(row, cashLabel);
        var moneyLabel = goog.dom.createDom('span', {'class' : 'add-on', 'style' : 'margin-left: -30px'}, '$');
        goog.dom.appendChild(row, moneyLabel);
        var cashInput = goog.dom.createDom('input', {'type' : 'text', 'style' : 'width: 80px; text-align: right; font-size: 18px'});
        goog.dom.appendChild(row, cashInput);
        pnc.ui.assureNumbers(cashInput);

        row = goog.dom.createDom('div', {'class' : 'row'});
        goog.dom.appendChild(panel, row);
        var remainderLabel = goog.dom.createDom('label', 'span2', 'Devuelta');
        goog.dom.appendChild(row, remainderLabel);
        var remainderValue = goog.dom.createDom('label', 'invoice-summary-value span1 bold', '0.00');
        goog.dom.appendChild(row, remainderValue);
        
        var upCallback = function(e) {
            var cash  = cashInput.value.length > 0 ? pnc.ui.round(cashInput.value) : 0;
            var value = cashInput.value.length > 0 ? cash - self.invoice.getTotal() + self.invoice.getDiscount() + sponsored : 0;
            
            if(value < 0)
                goog.dom.classes.add(cashInput, 'validation-error');
            else
                goog.dom.classes.remove(cashInput, 'validation-error');

            var remainder = pnc.ui.DECIMAL_FORMATTER.format(value);
            goog.dom.setTextContent(remainderValue, remainder);

            sellButton.disabled = cashInput.value.length < 1;
        };
        goog.events.listen(cashInput, goog.events.EventType.KEYUP, upCallback);
    }
   
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(panel, controlPane);

    var disposeCallback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, disposeCallback);

    var clientCallback = function(response) {
        switch(response.status()) { 
            case pnc.client.Status.CREATED:
                pnc.ui.Main.breadcrumbPop();
                slider.dispose();
                self.clear();
                pnc.ui.Alert.success('La venta de la factura se vendio exitosamente');
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('No hay suficiente inventario para vender por lo menos un producto en la factura').open();
                break;

            case pnc.client.Status.PRECONDITION_FAILED:
                pnc.ui.SlideDialog.error('El cliente no tiene suficiente credito para completar esta transaccion').open();
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                break;
        }
    };
    var sellCallback = function() { 
        if(self.invoice.getPayment() === pnc.model.Invoice.Payment.CREDIT && goog.isDefAndNotNull(self.creditLine)) {
            self.client.sellCredit(self.invoice, self.creditLine, clientCallback);
        } else if(self.invoice.getPayment() === pnc.model.Invoice.Payment.SPONSORED) {
            var sponsor = self.sponsorMap[self.sponsorSearch.value];
            var amount  = self.sponsoredInput.value.length > 0 ? pnc.ui.round(self.sponsoredInput.value) : 0;
            amount = self.invoice.getTotal() * amount / 100;
            self.client.sellSponsored(self.invoice, sponsor, amount, clientCallback)
        } else {
            self.client.sell(self.invoice, clientCallback);
        }
    };
    var sellButton      = goog.dom.createDom('button', 'btn btn-primary', 'Vender');
    sellButton.disabled = this.invoice.getPayment() === pnc.model.Invoice.Payment.CASH;
    goog.dom.appendChild(controlPane, sellButton);
    goog.events.listen(sellButton, goog.events.EventType.CLICK, sellCallback);

    var slider = new pnc.ui.SlidePanel('Vender Factura', pnc.ui.Resources.INVOICE, panel);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Vender Factura');
};

/**
 * Validates if the invoice is being sold to a credit line account, and if so checks to see if the invoice meets with the
 * necessary criteria: Is there a client selected? Does he have a credit line open? Does that credit line have enough
 * balance to process the invoice?
 * @return {boolean}  Does the invoice meet the criteria?
 * @private
 */
pnc.ui.Pos.prototype.validateCredit = function() {
    if(this.invoice.getPayment() !== pnc.model.Invoice.Payment.CREDIT)
        return true;

    if(!goog.isDefAndNotNull(this.creditLine)) {
        pnc.ui.SlideDialog.info('Este cliente no tiene linea de credito.').open();
        return false;
    }

    if(!this.creditLine.isActive()) {
        pnc.ui.SlideDialog.info('La linea de credito de este cliente ha sido desactivada.').open();
        return false;
    }

    if(this.creditLine.getLimit() > -1 && this.creditLine.getBalance() + this.invoice.getTotal() > this.creditLine.getLimit()) {
        pnc.ui.SlideDialog.info('La venta de esta factura sobrepasara el limite de credito del cliente.').open();
        return false;
    }

    return true;
};

/**
 * Calidates the criteria for a sponsored invoice
 * @return {boolean} is the criteria met?
 * @private
 */
pnc.ui.Pos.prototype.validateSponsored = function() {
    if(this.invoice.getPayment() !== pnc.model.Invoice.Payment.SPONSORED)
        return true;

    if(!goog.isDefAndNotNull(this.sponsorMap[this.sponsorSearch.value])) {
        pnc.ui.SlideDialog.error('No puede vender una factura patrocinada sin seleccionar el patrocinador').open();
        return false;
    }

    return true;
};

/**
 * Checks all the items and discounts applied to the invoice to calculate the total value for the invoice.
 * The total displayed is not the necessarily the same as the total saved in the model. The model saves the total value
 * of all invoices, without subtracting the discount. That is calulcated at runtime. In the UI for simplicity, the 
 * discount is subtracted from the total.
 * @private
 */
pnc.ui.Pos.prototype.calculateTotal = function() {
    var total        = 0;

    for(var cont = 0, size = this.invoice.getItems().length ; cont < size; cont++) {
        var item = this.invoice.getItems()[cont];
        total   += item.getQuantity() * this.items[item.getUuid()].getPrice();
    }

    this.invoice.setTotal(total);
    var discount    = this.discountInput.value.length > 0 ? pnc.ui.round(this.discountInput.value) : 0;
    var discountVal = total * discount / 100;
    this.invoice.setDiscount(discountVal);
    var sponsored    = this.sponsoredInput.value.length > 0 ? pnc.ui.round(this.sponsoredInput.value) : 0;
    var sponsoredVal = total * sponsored / 100;
    
    goog.dom.setTextContent(this.totalValue, pnc.ui.DECIMAL_FORMATTER.format(total - discountVal - sponsoredVal));
};

/**
 * Clears the point of sale and underlying model.
 * @private
 */
pnc.ui.Pos.prototype.clear = function () {
    goog.array.clear(this.invoice.getItems());
    this.invoice = new pnc.model.Invoice(null, pnc.client.Profile.getUser().getId());
    this.creditLine = null;
    goog.object.clear(this.items);
    this.paymentSelect.setValue(pnc.model.Invoice.Payment.CASH);
    this.customerSearch.value = '';
    this.brandSearch.value    = '';
    this.productSearch.value  = '';
    this.productAdd.disabled  = true;

    var buttons = goog.dom.getElementsByClass('btn', this.productTable);

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        goog.events.removeAll(buttons[cont]);

    var body = goog.dom.getElementsByTagNameAndClass('tbody', null, this.productTable)[0];
    goog.dom.removeChildren(body);

    this.discountInput.value = '0.00';
    goog.dom.setTextContent(this.sponsoredInput, '0.00');
    goog.dom.setTextContent(this.totalValue, '0.00');
    this.purchaseButton.disabled = true;

    goog.dom.removeChildren(this.customerContent);
    goog.dom.removeChildren(this.inventoryContent);

    var pills = [
        goog.dom.getElement('customer-search-pill'),
        goog.dom.getElement('brand-search-pill'),
        goog.dom.getElement('product-search-pill')
    ];

    for(var cont = 0, size = pills.length; cont < size; cont++)
        pnc.ui.closePill(pills[cont]);
};
