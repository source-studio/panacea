goog.provide('pnc.ui.CustomerView');

goog.require('goog.dom');
goog.require('goog.events');

goog.require('pnc.client');
goog.require('pnc.client.Customer');
goog.require('pnc.model.Address');
goog.require('pnc.model.Contact');
goog.require('pnc.model.CreditLine');
goog.require('pnc.model.Customer');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');


/**
 * View that allows the user to search customers, edit thier details and add new ones to the collection.
 * @constructor
 */
pnc.ui.CustomerView = function() {
    /**
     * Client used to make customer transactions to the server
     * @type {pnc.client.Customer}
     * @private
     */
    this.client           = new pnc.client.Customer();

    /**
     * Client used to mek credit line trasnactions to the server
     * @type {pnc.client.CreditLine}
     * @private
     */
    this.creditLineClient = new pnc.client.CreditLine();

    /**
     * Auxiliary credit line view, used to show credit line info for the customer
     * @type {pnc.ui.CreditLineView}
     * @private
     */
    this.creditLineView   = null;

    //------------- elements -------------//
    /** @private */ this.container   = null;
    /** @private */ this.paginator   = null;
};
pnc.ui.Main.registerModule('customers', pnc.ui.CustomerView);


/**
 * Renders the view
 * @param  {Element} parent  Parent element that will hold the view
 */
pnc.ui.CustomerView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderSearchPane();
    this.renderActionPane();
    this.renderCustomerPane();
    this.renderDetailPane();
    this.renderCreditLinePane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Clientes');
};

/**
 * Disposes the view and cleans up any resources that may be used by it
 */
pnc.ui.CustomerView.prototype.dispose = function() {
    goog.dom.removeNode(this.container);
};


/**
 * Renders the pane used to searching customres. The user will be able to enter search criteria and perform search
 * transactions here.
 * @private
 */
pnc.ui.CustomerView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var nameLabel = goog.dom.createDom('label', 'span1', 'Nombre');
    goog.dom.appendChild(content, nameLabel);
    var nameSearch = goog.dom.createDom('input', {'id' : 'name-search', 'class' : 'span3', 'type' : 'text'});
    goog.dom.appendChild(content, nameSearch);

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Buscar');
    goog.dom.appendChild(controlPane, searchButton);
    var self = this;

    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateCustomerPane(response.content());
    };

    var clickCallback = function() {
        if(nameSearch.value.length < 1) {
            goog.dom.classes.add(nameSearch, 'validation-error');
            return;
        }

        goog.dom.classes.remove(nameSearch, 'validation-error');
        self.client.search(nameSearch.value, searchCallback);
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Renders the action pane. This holds any collection wide actions that may be available. Holds the action to add a new
 * user.
 * @private
 */
pnc.ui.CustomerView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.CUSTOMER_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));
    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Cliente');
    goog.dom.appendChild(addButton, span);
};

/**
 * Renders the customer pane. This holds customers that match search criteria after the search transaction has been executed.
 * The user can click on any of these users to view extended details on them.
 * @private
 */
pnc.ui.CustomerView.prototype.renderCustomerPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Clientes');
    content.id  = 'customer-pane';
    content.style.minHeight = '390px';
    var head = goog.dom.getElementsByClass('box-head', content.parentElement)[0];

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var editButton, removeButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.CUSTOMER_UPDATE))
        editButton = pnc.ui.createToolbarButton(actionGroup, 'edit-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.edit, this));

    if(goog.array.contains(privs, pnc.client.Privileges.CUSTOMER_REMOVE))
        editButton = pnc.ui.createToolbarButton(actionGroup, 'remove-button', pnc.ui.Resources.REMOVE, 'Remover', false, goog.bind(this.remove, this));

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var navCallback = function(e) {
        if(!goog.isNull(editButton))
            editButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
        goog.dom.removeChildren(goog.dom.getElement('credit-line-table'));
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallback);

    var titles = ['Nombre', 'Apodo'];
    this.paginator = new pnc.ui.Paginator(titles, [], null, pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderCustomer, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
    this.creditLineView = new pnc.ui.CreditLineView(this.paginator);
    var self = this;
    
    var callback = function(creditLine) {
        var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', self.paginator.getElement())[0];
        var customer = pnc.model.findModel(self.paginator.getData(), row.dataset.customer);
        self.populateCreditLinePane(customer);
    };
    this.creditLineView.setRefreshCallback(callback);
};

/**
 * Renders the detail pane. This pane will hold additional information about the customer not displayed in the customer pane.
 * This will be activated when the user clicks on a customer.
 * @private
 */
pnc.ui.CustomerView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '157px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 1, 'Cliente');
    body.id = 'detail-table';
};

/**
 * Renders teh credit lien pane. This pane will hold credit line details for the customre. This pane will only show
 * information when the user clicks on a customer, and if said customer has a credit line.
 * @private
 */
pnc.ui.CustomerView.prototype.renderCreditLinePane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.PIN, 'Linea de Credito');
    content.style.minHeight = '150px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 2, 'Cuenta');
    body.id = 'credit-line-table';
};

/**
 * Utility to render a customer row within the paginator in the customer pane
 * @param  {Element}            parent    Parent element where the customer representation will be placed
 * @param  {pnc.model.Customer} customer  Customer to represent
 * @private
 */
pnc.ui.CustomerView.prototype.renderCustomer = function(parent, customer) {
    var row = goog.dom.createDom('tr', {'data-customer' : customer.getId()});
    goog.dom.appendChild(parent, row);
    var editButton   = goog.dom.getElement('edit-button');
    var removeButton = goog.dom.getElement('remove-button');
    var self = this;

    var callback = function(customer, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = false;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = false;

        self.populateDetailPane(customer, row);
        self.populateCreditLinePane(customer, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, customer));

    var cells = [
        goog.dom.createDom('td', {'style' : 'width: 150px'}, customer.toString()),
        goog.dom.createDom('td', {'style' : 'width: 100px'}, customer.getNickName())
    ];

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Populates the customer pane with the search results yielded from a corresponding search transaction.
 * @param  {Array.<pnc.model.Customer>} customers  Customers to render in paginator.
 * @private
 */
pnc.ui.CustomerView.prototype.populateCustomerPane = function(customers) {
    var content          = goog.dom.getElement('customer-pane');
    var editButton       = goog.dom.getElement('edit-button');
    var removeButton     = goog.dom.getElement('remove-button');
    var backButton       = goog.dom.getElement('back-button');
    var forwardButton    = goog.dom.getElement('forward-button');
    var detailTable      = goog.dom.getElement('detail-table');
    var creditLineTable  = goog.dom.getElement('credit-line-table');

    goog.dom.removeChildren(detailTable);
    goog.dom.removeChildren(creditLineTable);
    backButton.disabled = true;
    forwardButton.disabled = goog.isDefAndNotNull(customers) && customers.length <= pnc.ui.PAGE_SIZE;

    if(goog.isDefAndNotNull(editButton))
        editButton.disabled = true;

    if(goog.isDefAndNotNull(removeButton))
        removeButton.disabled = true;

    this.paginator.setData(customers);
    this.paginator.refresh();
};

/**
 * Populates the detail pane. The detail pane is populated when the user selects a customer from the paginator.
 * @param  {pnc.model.Customer} customer  Customer for which extend details will be shown.
 * @private
 */
pnc.ui.CustomerView.prototype.populateDetailPane = function(customer) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);

    if(!goog.isDefAndNotNull(customer))
        return;

    pnc.ui.createSummaryContent(body, customer.toString());
    var address  = customer.getAddress();
    var contacts = customer.getContacts();

    if(goog.isDefAndNotNull(address)) {
        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td');
        goog.dom.appendChild(row, cell);
        var data = [address.getStreet1(), address.getStreet2(), address.getCity(), address.getProvince()];

        for(var cont = 0, size = data.length; cont < size; cont++) {
            var entry = goog.dom.createDom('div', null, data[cont]);
            goog.dom.appendChild(cell, entry);
        }
    } else {
        pnc.ui.createSummaryContent(body, 'Ninguna direccion registrada');
        goog.dom.getElementsByTagNameAndClass('td', null, body)[0].style.textAlign = 'center';
    }

    if(goog.isDefAndNotNull(contacts) && contacts.length > 0) {
        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td');
        goog.dom.appendChild(row, cell);

        for(var cont = 0, size = contacts.length; cont < size; cont++) {
            var entry = goog.dom.createDom('div', null, contacts[cont].toString());
            goog.dom.appendChild(cell, entry);
        }
    } else {
        pnc.ui.createSummaryContent(body, 'Ningun contacto registrado');
        goog.dom.getElementsByTagNameAndClass('td', null, body)[1].style.textAlign = 'center';
    }
};

/**
 * Populates credit line pane. The credit line pane is populated when the user selects a customer from the paginator.
 * Data will only be displayed here if the selected customer has a credit line associated with him/her. This method takes
 * care of executing the transaction to retrieve the credit line.
 * @param  {pnc.model.Customer} customer  Customer for which credit line info will be displayed, if exists.
 * @private
 */
pnc.ui.CustomerView.prototype.populateCreditLinePane = function(customer) {
    var body = goog.dom.getElement('credit-line-table');
    goog.dom.removeChildren(body);
    var existing = goog.dom.getElementsByTagNameAndClass('button', null, body.parentElement.parentElement)[0];

    if(goog.isDefAndNotNull(existing))
        goog.dom.removeNode(existing);

    if(!goog.isDefAndNotNull(customer))
        return;

    var self = this;
    var callback = function(response) {
        var privs = pnc.client.Profile.getPrivileges();

        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                if(!goog.array.contains(privs, pnc.client.Privileges.CREDIT_LINE_ADD))
                    return;

                pnc.ui.createSummaryContent(body, 'No tiene linea de credito');
                goog.dom.getElementsByTagNameAndClass('td', null, body)[0].style.textAlign = 'center';
                var add = goog.dom.createDom('button', {'id' : 'credit-line-add-button', 'class' : 'btn btn-block btn-success'});
                goog.dom.appendChild(body.parentElement.parentElement, add);
                goog.events.listen(add, goog.events.EventType.CLICK, goog.bind(self.creditLineView.addCreditLine, self.creditLineView));
                var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
                goog.dom.appendChild(add, plus);
                var span = goog.dom.createDom('span', null, 'Linea de Credito');
                goog.dom.appendChild(add, span);
                add.disabled = goog.dom.getElementsByClass('slide-panel').length > 0;
                return;

            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.Alert.error('Error al tratar de buscar informacion de linea de credito del cliente');
                return;
        }

        var creditLine = response.content();
        var limit = creditLine.getLimit() > -1 ? pnc.ui.DECIMAL_FORMATTER.format(creditLine.getLimit()) : 'Ilimitado';
        pnc.ui.createSummaryContent(body, 'Limite',  limit);
        var balanceRow = pnc.ui.createSummaryContent(body, 'Balance', pnc.ui.DECIMAL_FORMATTER.format(creditLine.getBalance()));
        pnc.ui.createSummaryContent(body, 'Activo',  creditLine.isActive() ? 'Si' : 'No');

        balanceRow.children[1].id = 'credit-line-summary-balance';

        if(!goog.array.contains(privs, pnc.client.Privileges.CREDIT_LINE_VIEW))
            return;

        var detailButton = goog.dom.createDom('button', {'id' : 'credit-line-detail-button', 'class' : 'btn btn-info btn-block'}, 'Detalles');
        goog.dom.appendChild(body.parentElement.parentElement, detailButton);
        goog.events.listen(detailButton, goog.events.EventType.CLICK, goog.bind(self.creditLineView.creditLineDetail, self.creditLineView, creditLine));
    };
    this.creditLineClient.getByCustomer(customer, callback);
};

/**
 * Creates a form that will be used to add a new customer to the collection. Performs the add transaction when the user
 * submits.
 * @private
 */
pnc.ui.CustomerView.prototype.add = function() {
    var buttons = [
        goog.dom.getElement('add-button'),
        goog.dom.getElement('credit-line-add-button'),
        goog.dom.getElement('credit-line-detail-button')
    ];

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        if(goog.isDefAndNotNull(buttons[cont]))
            buttons[cont].disabled = true;

    var content     = goog.dom.createDom('div');
    var inputs      = this.createCustomerForm(content, null);
    var contactForm = new pnc.ui.ContactForm();
    contactForm.render(content);
    contactForm.getElement().style.marginTop = '-10px';
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        contactForm.dispose();
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        var customer = response.content();
        self.paginator.getData().push(customer);
        goog.array.sort(self.paginator.getData(), pnc.model.Customer.compare);
        self.paginator.refresh();
        self.populateDetailPane(null);
        self.populateCreditLinePane(null);

        var editButton   = goog.dom.getElement('edit-button');
        var removeButton = goog.dom.getElement('remove-button');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = true;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El cliente fue agregado');
    };

    var saveCallback = function() {
        if(!self.validateCustomerForm(inputs))
            return;

        var customer = self.parseCustomerForm(inputs);
        customer.setContacts(contactForm.getContacts());
        self.client.add(customer, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar Cliente', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Creates a form that will be used to edit the details of an existing customer in the collection. Performs the edit
 * transaction when the user submits.
 * @private
 */
pnc.ui.CustomerView.prototype.edit = function() {
    var row         = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var customer    = pnc.model.findModel(this.paginator.getData(), row.dataset.customer);
    var buttons     = [
        goog.dom.getElement('add-button'),
        goog.dom.getElement('credit-line-add-button'),
        goog.dom.getElement('credit-line-detail-button')
    ];

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        if(goog.isDefAndNotNull(buttons[cont]))
            buttons[cont].disabled = true;

    var content     = goog.dom.createDom('div');
    var inputs      = this.createCustomerForm(content, customer);
    var contactForm = new pnc.ui.ContactForm(customer.getContacts());
    contactForm.render(content);
    contactForm.getElement().style.marginTop = '-10px';
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() {
        contactForm.dispose();
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newCustomer = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newCustomer);
        self.paginator.refresh();
        self.populateDetailPane(newCustomer);
        self.populateCreditLinePane(newCustomer);
        cancelCallback();
        pnc.ui.Alert.success('El cliente fue actualizado');

        var editButton   = goog.dom.getElement('edit-button');
        var removeButton = goog.dom.getElement('remove-button');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = true;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = true;

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;
    };

    var saveCallback = function() {
        if(!self.validateCustomerForm(inputs))
            return;

        var newCustomer = self.parseCustomerForm(inputs, customer);
        newCustomer.setId(customer.getId());
        newCustomer.setContacts(contactForm.getContacts());
        self.client.update(newCustomer, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog box. If the user confirms, performs the transaction to attempt to delete
 * a customer. If there is an integrity constraint, the transaction will fail.
 * @private
 */
pnc.ui.CustomerView.prototype.remove = function() {
    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var customer = pnc.model.findModel(this.paginator.getData(), row.dataset.customer);
    var dialog   = pnc.ui.SlideDialog.warning('Esta seguro que desea remover este cliente?');
    var self     = this;

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Este cliente tiene referencias a otra data. Probablemente facturas o linea de credito. No puede ser removido.').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), customer);
        self.paginator.refresh();
        self.populateDetailPane(null);
        self.populateCreditLinePane(null);

        var editButton   = goog.dom.getElement('edit-button');
        var removeButton = goog.dom.getElement('remove-button');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = true;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = true;

        pnc.ui.Alert.success('El cliente fue removido');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.remove(customer, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a form that is used to create and edit customers.
 * @param  {Element}            parent    Parent element where the form will be placed
 * @param  {pnc.model.Customer} customer  Optional customer. If povided the form will be prepopulated with this customer's
 *                                        details.
 * @return {Object.<string, Element>}     Map with input elemets in form.
 * @private
 */
pnc.ui.CustomerView.prototype.createCustomerForm = function(parent, customer) {
    var inputs  = {};
    var customerHeader = goog.dom.createDom('h4', 'form-header', 'Cliente');
    goog.dom.appendChild(parent, customerHeader);

    var create = function(key, name, help) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'class' : 'span1'}, name);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span3', 'type' : 'text'});
        goog.dom.appendChild(group, inputs[key]);

        if(!goog.isNull(help)) {
            var help = goog.dom.createDom('span', 'help-inline', help);
            goog.dom.appendChild(group, help);
        }
    }

    var fields = {
        'customer-first' : ['Nombre',   'No puede estar vacio'],
        'customer-last'  : ['Apellido', 'No puede estar vacio'],
        'customer-nick'  : ['Apodo',    null]
    };

    for(var key in fields)
        create(key, fields[key][0], fields[key][1]);
    
    var addressHeader = goog.dom.createDom('h4', 'form-header', 'Direccion');
    goog.dom.appendChild(parent, addressHeader);

    fields = {
        'address-street1'  : ['Calle 1',   'Si va a incluir una direccion, este campo no puede estar vacio'],
        'address-street2'  : ['Calle 2',   null],
        'address-city'     : ['Ciudad',    'Si va a incluir una direccion, este campo no puede estar vacio'],
        'address-province' : ['Provincia', 'Si va a incluir una direccion, este campo no puede estar vacio'],
        'address-country'  : ['Pais',      null]
    };

    for(var key in fields)
        create(key, fields[key][0], fields[key][1]);

    if(goog.isDefAndNotNull(customer)) {
        inputs['customer-first'].value  = customer.getFirstName();
        inputs['customer-last'].value   = customer.getLastName();
        inputs['customer-nick'].value   = goog.isNull(customer.getNickName()) ? '' : customer.getNickName();
        var address = customer.getAddress();

        if(goog.isDefAndNotNull(address)) {
            inputs['address-street1'].value  = goog.isNull(address.getStreet1())  ? '' : address.getStreet1();
            inputs['address-street2'].value  = goog.isNull(address.getStreet2())  ? '' : address.getStreet2();
            inputs['address-city'].value     = goog.isNull(address.getCity())     ? '' : address.getCity();
            inputs['address-province'].value = goog.isNull(address.getProvince()) ? '' : address.getProvince();
            inputs['address-country'].value  = goog.isNull(address.getCountry())  ? '' : address.getCountry();
        }
    }

    var contactHeader = goog.dom.createDom('h4', 'form-header', 'Contacto');
    goog.dom.appendChild(parent, contactHeader);

    return inputs;
};

/**
 * Validates the customer form. If issues are encountered, embedded error messages will be highlighted.
 * @param  {Object.<string, Element>} inputs  Map with input elements from the form
 * @return {boolean}                          true if the form data was found to be valid, false otherwise
 * @private
 */
pnc.ui.CustomerView.prototype.validateCustomerForm = function(inputs) {
    var errors = goog.dom.getElementsByClass('error', inputs['customer-first'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    var test = function(key) {
        if(inputs[key].value.length < 1)
            goog.dom.classes.add(inputs[key].parentElement, 'error');
    };

    test('customer-first');
    test('customer-last');

    var addressFields   = ['address-street1', 'address-street2', 'address-city', 'address-province', 'address-country'];
    var addressIncluded = false;

    for(var cont = 0, size = addressFields.length; cont < size; cont++) {
        var key = addressFields[cont];

        if(inputs[key].value.length > 0) {
            addressIncluded = true;
            break;
        }
    }

    if(addressIncluded) {
        for(var cont = 0, size = addressFields.length; cont < size; cont++)
            test(addressFields[cont]);
    }

    errors = goog.dom.getElementsByClass('error', inputs['customer-first'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the form data, and creates or updates a customer object using its content. The invoker has the option to include
 * a customer parameter. If included, it is assumed that this is an update transaction, and its details will be updated.
 * Otherwise it is assumed this is a create transaction, and a new customer object will be created.
 * @param  {Object.<string, Element>} inputs    Map with input elements in from
 * @param  {pnc.model.Customer}       customer  Optional customer element to use as base
 * @return {pnc.model.Customer}                 Customer element derived from form.
 */
pnc.ui.CustomerView.prototype.parseCustomerForm = function(inputs, customer) {
    customer = goog.isDefAndNotNull(customer) ? customer : new pnc.model.Customer();
    customer.setFirstName(inputs['customer-first'].value);
    customer.setLastName( inputs['customer-last'].value);
    customer.setNickName( inputs['customer-nick'].value);

    if(inputs['address-street1'].value.length > 0) {
        var address = goog.isDefAndNotNull(customer.getAddress()) ? customer.getAddress() : new pnc.model.Address();
        address.setStreet1( inputs['address-street1'].value);
        address.setStreet2( inputs['address-street2'].value);
        address.setCity(    inputs['address-city'].value);
        address.setProvince(inputs['address-province'].value);
        address.setCountry( inputs['address-country'].value);
        customer.setAddress(address);
    }

    return customer;
};
