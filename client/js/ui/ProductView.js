goog.provide('pnc.ui.ProductView');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.json');
goog.require('goog.string');

goog.require('pnc.client.Inventory');
goog.require('pnc.client.Product');
goog.require('pnc.client.Profile');
goog.require('pnc.client.Supplier');
goog.require('pnc.model.Inventory');
goog.require('pnc.model.Product');
goog.require('pnc.model.Supplier');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');

/**
 * View that allows the user to search products, edit their details and add new ones to the catalog.
 * @constructor
 */
pnc.ui.ProductView = function() {
    /**
     * Map of suppliers. Keys are string representation of suppliers, values are supplier objects. Used in the supplier
     * search function
     * @type {Object.<string, pnc.model.Supplier>}
     * @private
     */
    this.supplierMap = {};

    /**
     * Service client for the Product service
     * @type {pnc.client.Product}
     * @private
     */
    this.productClient   = new pnc.client.Product();

    /**
     * Service client for the Inventory client
     * @type {pnc.client.Inventory}
     * @private
     */
    this.inventoryClient = new pnc.client.Inventory();

    //------------- elements -------------//
    /** @private */ this.container   = null;
    /** @private */ this.disposables = [];
    /** @private */ this.paginator   = null;
};
pnc.ui.Main.registerModule('products', pnc.ui.ProductView);

/**
 * Renders the view
 * @param  {Element} parent  Parent element where the view will be placed in
 */
pnc.ui.ProductView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderSearchPane();
    this.renderActionPane();
    this.renderProductPane();
    this.renderDetailPane();
    this.renderInventoryPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Productos');
};

/**
 * Cleans up resources and removes the element from the DOM
 */
pnc.ui.ProductView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};


/**
 * Renders the pane for searching products. The user will be able to search by brand, product name and/or supplier.
 * @private
 */
pnc.ui.ProductView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var brandLabel = goog.dom.createDom('label', 'span1', 'Marca');
    goog.dom.appendChild(content, brandLabel);
    var brandSearch = goog.dom.createDom('input', {'id' : 'brand-search', 'class' : 'span2', 'type' : 'text'});
    goog.dom.appendChild(content, brandSearch);
    var brandAc = goog.ui.ac.createSimpleAutoComplete([], brandSearch, false, true);
    this.disposables.push(brandAc);
    var pill = pnc.ui.createPill(brandSearch, brandAc);
    var self = this;
    var callback = function() { 
        var productContent = goog.dom.getElementsByTagNameAndClass('tbody', null, self.paginator.getElement());
        goog.dom.removeChildren(productContent); 
        self.populateDetailPane(null);
        self.populateInventoryPane(null);
    };
    pnc.ui.onPillClose(pill, callback);

    var link     = pnc.client.Profile.getLink(pnc.client.Product.Rel.SEARCH_BRANDS);
    var resolver = function() { return brandSearch.value; };
    pnc.ui.updateAcData(brandSearch, brandAc, link, {'{brand}' : resolver}, goog.json.unsafeParse);

    var supplierLabel = goog.dom.createDom('label', 'span1', 'Suplidor');
    goog.dom.appendChild(content, supplierLabel);
    var supplierSearch = goog.dom.createDom('input', {'id' : 'supplier-search', 'class' : 'span2', 'type' : 'text'});
    goog.dom.appendChild(content, supplierSearch);
    var supplierAc = goog.ui.ac.createSimpleAutoComplete([], supplierSearch, false, true);
    this.disposables.push(supplierAc);
    pill = pnc.ui.createPill(supplierSearch, supplierAc);
    pnc.ui.onPillClose(pill, callback);

    link        = pnc.client.Profile.getLink(pnc.client.Supplier.Rel.SEARCH);
    var decoder = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decode);
    resolver    = function() { return supplierSearch.value; };
    pnc.ui.updateAcData(supplierSearch, supplierAc, link, {'{org}' : resolver}, decoder, this.supplierMap);

    var productLabel = goog.dom.createDom('label', 'span1', 'Producto');
    goog.dom.appendChild(content, productLabel);
    var productSearch = goog.dom.createDom('input', {'id' : 'product-search', 'class' : 'span2', 'type' : 'text'});
    goog.dom.appendChild(content, productSearch);

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Buscar');
    goog.dom.appendChild(controlPane, searchButton);

    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateProductPane(response.content());
    };

    var clickCallback = function() { 
        if(brandSearch.value.length < 1 && supplierSearch.value.length < 1 && productSearch.value.length < 1) {
            goog.dom.classes.add(productSearch, 'validation-error');
            return;
        }

        goog.dom.classes.remove(productSearch, 'validation-error');
        
        if(supplierSearch.value.length > 0) {
            var supplier = self.supplierMap[supplierSearch.value];
            self.productClient.searchBySupplier(productSearch.value, brandSearch.value, supplier, searchCallback);
        } else {
            self.productClient.search(productSearch.value, brandSearch.value, searchCallback);
        }
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Renders the action pane. It allows the user to add new products.
 * @private
 */
pnc.ui.ProductView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.PRODUCT_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));
    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Producto');
    goog.dom.appendChild(addButton, span);
};

/**
 * Renders the product pane. This will contain the product listing for elements that match the search criteria.
 * @private
 */
pnc.ui.ProductView.prototype.renderProductPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Productos');
    content.id = 'product-pane';
    content.style.minHeight = '390px';
    var head = goog.dom.getElementByClass('box-head', content.parentElement);

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var inventoryButton, editButton, removeButton = null;  

    if(goog.array.contains(privs, pnc.client.Privileges.INVENTORY_VIEW))
        inventoryButton = pnc.ui.createToolbarButton(actionGroup, 'inventory-button', pnc.ui.Resources.BARS, 'Inventario', false, goog.bind(this.inventory, this));

    if(goog.array.contains(privs, pnc.client.Privileges.PRODUCT_UPDATE))
        editButton = pnc.ui.createToolbarButton(actionGroup, 'edit-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.edit, this));

    if(goog.array.contains(privs, pnc.client.Privileges.PRODUCT_REMOVE))
        removeButton = pnc.ui.createToolbarButton(actionGroup, 'remove-button', pnc.ui.Resources.REMOVE, 'Remover', false, goog.bind(this.remove, this));

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    var self = this;

    var navCallback = function(e) {
        if(!goog.isNull(editButton))
            editButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
        goog.dom.removeChildren(goog.dom.getElement('inventory-table'));
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallback);

    var titles     = ['Marca', 'Nombre', 'Precio'];
    this.paginator = new pnc.ui.Paginator(titles, [], null, pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderProduct, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Renders the detail pane. This is a side pane that provides more information about a selected product
 * @private
 */
pnc.ui.ProductView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '157px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 2, 'Producto');
    body.id  = 'detail-table';
};

/**
 * Renders the inventory pane. This is a side pane that provides the total existence of the product.
 * @private
 */
pnc.ui.ProductView.prototype.renderInventoryPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.BARS, 'Inventario');
    content.style.minHeight = '150px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 2, 'Existencia');
    body.id  = 'inventory-table';
};

/**
 * Renders a single product within the product pane
 * @param  {Element} parent             Parent element where the product representation will be placed in
 * @param  {pnc.model.Product} product  Product to render
 * @private
 */
pnc.ui.ProductView.prototype.renderProduct = function(parent, product) {
    var row = goog.dom.createDom('tr', {'data-product' : product.getId()});
    goog.dom.appendChild(parent, row);
    var inventoryButton = goog.dom.getElement('inventory-button');
    var editButton      = goog.dom.getElement('edit-button');
    var removeButton    = goog.dom.getElement('remove-button');

    var self = this;
    var callback = function(product, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        if(goog.isDefAndNotNull(inventoryButton))
            inventoryButton.disabled = false;

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = false;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = false;

        self.populateDetailPane(product, row);
        self.populateInventoryPane(product, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, product));

    var cells = [
        goog.dom.createDom('td', {'style' : 'width: 150px'}, product.getBrand()),
        goog.dom.createDom('td', {'style' : 'width: 220px'}, product.getName()),
        goog.dom.createDom('td', {'style' : 'text-align: right'}, pnc.ui.DECIMAL_FORMATTER.format(product.getPrice()))
    ];

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Populates the product pane with content coming from a search transaction
 * @param  {Array.<pnc.model.Product>} products  Products to populate into the pane
 * @private
 */
pnc.ui.ProductView.prototype.populateProductPane = function(products) {
    var content         = goog.dom.getElement('product-pane');
    var inventoryButton = goog.dom.getElement('inventory-button');
    var editButton      = goog.dom.getElement('edit-button');
    var removeButton    = goog.dom.getElement('remove-button');
    var detailTable     = goog.dom.getElement('detail-table');
    var inventoryTable  = goog.dom.getElement('inventory-table');

    goog.dom.removeChildren(detailTable);
    goog.dom.removeChildren(inventoryTable);

    if(goog.isDefAndNotNull(inventoryButton))
        inventoryButton.disabled = true;

    if(goog.isDefAndNotNull(editButton))
        editButton.disabled = true;

    if(goog.isDefAndNotNull(removeButton))
        removeButton.disabled = true;

    this.paginator.setData(products);
    this.paginator.refresh();
};

/**
 * Populates the detail pane. This will be invoked when the user clicks on a product in the product pane.
 * @param  {pnc.model.Product} product  Product for which detailed will be displayed
 * @private
 */
pnc.ui.ProductView.prototype.populateDetailPane = function(product) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);
    
    if(!goog.isDefAndNotNull(product))
        return;

    pnc.ui.createSummaryContent(body, 'Impuesto',          product.getTaxRate().toString() + '%');
    pnc.ui.createSummaryContent(body, 'Codigo de Barra',   product.getBarCode().toString());

    if(product.getThreshold() > 0)
        pnc.ui.createSummaryContent(body, 'Existencia Minima', product.getThreshold().toString());

    if(product.getMinimum() > 1)
        pnc.ui.createSummaryContent(body, 'Venta Minima', product.getMinimum().toString());
};

/**
 * Populates the inventory pane. This will be invoked when a user clicks on a product in the product pane.
 * @param  {pnc.model.Product} product  Product for which inventory data will be displayed.
 * @private
 */
pnc.ui.ProductView.prototype.populateInventoryPane = function(product) {
    var body = goog.dom.getElement('inventory-table');
    goog.dom.removeChildren(body);

    if(!goog.isDefAndNotNull(product))
        return;

    var callback = function(response) {
        var inventory = null;

        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                inventory = [];
                break;

            case pnc.client.Status.OK:
                inventory = response.content();
                break;

            default:
                pnc.ui.Alert.error('Error al tratar de buscar informacion de inventario del producto seleccionado');
                return;
        }

        var total = 0;

        for(var cont = 0, size = inventory.length; cont < size; cont++)
            total += inventory[cont].getRemaining();

        pnc.ui.createSummaryContent(body, 'Cantidad', pnc.ui.DECIMAL_FORMATTER.format(total));
    };
    this.inventoryClient.getByProduct(product, callback);
};

/**
 * Creates a view that allows the user to see the existing details for product inventory and if privileges
 * suffice, to create, edit and remove as well.
 * @private
 */
pnc.ui.ProductView.prototype.inventory = function() {
    var row        = goog.dom.getElementsByTagNameAndClass ('tr', 'info', this.paginator.getElement())[0];
    var product    = pnc.model.findModel(this.paginator.getData(), row.dataset.product);
    var productAdd = goog.dom.getElement('add-button');

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                pnc.ui.Alert.warning("No existe registro de inventario para este producto");
                return;

            case pnc.client.Status.OK:
                paginator.setData(response.content());
                paginator.refresh();
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }
    };
    this.inventoryClient.getByProduct(product, callback); 

    if(goog.isDefAndNotNull(productAdd))
        productAdd.disabled = true;

    var content = goog.dom.createDom('div', {'id' : 'inventory-content', 'style' : 'min-height : 150px'});
    var slider  = new pnc.ui.SlidePanel('Inventario - ' + product.toString(), pnc.ui.Resources.BARS, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Inventario');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);
    var addButton, editButton, removeButton  = null;
    var privs = pnc.client.Profile.getPrivileges();
    var self  = this;

    var callback = function() { 
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(productAdd))
            productAdd.disabled = false;
    };
    pnc.ui.createToolbarButton(actionGroup, 'inventory-back-button', pnc.ui.Resources.BACK, 'Regresar a producto', true, callback);

    if(goog.array.contains(privs, pnc.client.Privileges.INVENTORY_ADD)) {
        var callback = function() { self.addInventory(product, paginator); };
        addButton = pnc.ui.createToolbarButton(actionGroup, 'inventory-add-button', pnc.ui.Resources.PLUS_GRAY, 'Agregar', true, callback);
    }

    if(goog.array.contains(privs, pnc.client.Privileges.INVENTORY_UPDATE)) {
        var callback = function() { self.updateInventory(product, paginator); };
        editButton = pnc.ui.createToolbarButton(actionGroup, 'inventory-update-button', pnc.ui.Resources.EDIT, 'Editar', false, callback);
    }

    if(goog.array.contains(privs, pnc.client.Privileges.INVENTORY_REMOVE)) {
        var callback = function() { self.removeInventory(paginator); };
        removeButton = pnc.ui.createToolbarButton(actionGroup, 'inventory-remove-button', pnc.ui.Resources.REMOVE, 'Remove', false, callback);
    }

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var forwardCallback = function(e) {
        if(!goog.isNull(editButton))
            editButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, forwardCallback);

    var backCallback = function(e) {
        if(!goog.isNull(editButton))
            editButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;
    };
    goog.events.listen(backButton, goog.events.EventType.CLICK, backCallback);

    var titles    = ['Cantidad Original', 'Cantidad Restante', 'Registro', 'Expiracion', 'Costo'];
    var paginator = new pnc.ui.Paginator(titles, [], 'inventory-detail-table', pnc.ui.PAGE_SIZE, backButton, forwardButton);
    paginator.setRowRenderer(goog.bind(this.renderInventory, this));
    paginator.render(content);
    paginator.getElement().style.width = '515px';
};

/**
 * Creates a form that will be used to add a new product
 * @private
 */
pnc.ui.ProductView.prototype.add = function() {
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createProductForm(content, null);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var cancelCallback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);
    
    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.CREATED:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('El nombre y marca del producto no pueden repetirse').open();
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var product = response.content();
        self.paginator.getData().push(product);
        goog.array.sort(self.paginator.getData(), pnc.model.Product.compare);
        self.paginator.refresh();
        self.populateDetailPane(null);
        self.populateInventoryPane(null);
        cancelCallback();
        pnc.ui.Alert.success('El producto fue agregado');
    };

    var saveCallback = function() {
        if(!self.validateProductForm(inputs))
            return;

        var product = self.parseProductForm(inputs);
        self.productClient.add(product, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Creates a form that will be used to edit a product.
 * @private
 */
pnc.ui.ProductView.prototype.edit = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var product   = pnc.model.findModel(this.paginator.getData(), row.dataset.product);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createProductForm(content, product);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var cancelCallback = function() { 
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);
    
    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('El nombre y marca de un producto no puede repetirse.').open();
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var newProduct = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newProduct);
        self.paginator.refresh();
        self.populateDetailPane(newProduct);
        self.populateInventoryPane(newProduct);

        var buttons = [
            goog.dom.getElement('inventory-button'),
            goog.dom.getElement('edit-button'),
            goog.dom.getElement('remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El producto fue actualizado');
    };

    var saveCallback = function() {
        if(!self.validateProductForm(inputs))
            return;

        var newProduct = self.parseProductForm(inputs, product);
        newProduct.setId(product.getId());
        self.productClient.update(newProduct, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Produces a dialog confirm that a user wants to remove a product. If the user confirms, executes the transaction to 
 * remove the product. 
 * @private
 */
pnc.ui.ProductView.prototype.remove = function() {
    var row     = goog.dom.getElementsByTagNameAndClass ('tr', 'info', this.paginator.getElement())[0];
    var product = pnc.model.findModel(this.paginator.getData(), row.dataset.product);
    var dialog  = pnc.ui.SlideDialog.warning('Esta seguro que desea remover este producto?');

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Este producto ha sido utilizado en transaccioes de venta, y no puede ser eliminido.').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), product);
        self.paginator.refresh();
        self.populateDetailPane(null);
        self.populateInventoryPane(null);
        pnc.ui.Alert.success('El producto fue removido');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.productClient.remove(product, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a form that is used to update or create a new product.
 * @param  {Element}           parent   Parent element where the form will be placed in
 * @param  {pnc.model.Product} product  Product element that will be used to be populated in the form. If null is passed
 *                                      the form will be created with no data inserted.
 * @return {Object.<string, Element>}   A map with the input controls created in the form.
 * @private
 */
pnc.ui.ProductView.prototype.createProductForm = function(parent, product) {
    var fields = {
        'product-brand'     : ['Marca',             'No puede estar vacio', false],
        'product-name'      : ['Nombre',            'No puede estar vacio', false],
        'product-barcode'   : ['Codigo Barra',      'Debe ser un numero',   true],
        'product-tax'       : ['Impuesto',          'Debe ser un numero',   true],
        'product-price'     : ['Precio',            'Debe ser un numero',   true],
        'product-threshold' : ['Existencia Minima', 'Debe ser un numero',   true],
        'product-minimum'   : ['Venta Minima',      'Debe ser un numero',   true]
    };
    var inputs = {};

    for(var key in fields) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'class' : 'span2'}, fields[key][0]);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span2', 'type' : 'text'});
        goog.dom.appendChild(group, inputs[key]);

        if(fields[key][2])
            pnc.ui.assureNumbers(inputs[key]);

        var help = goog.dom.createDom('span', 'help-inline', fields[key][1]);
        goog.dom.appendChild(group, help);
    }

    goog.dom.classes.add(inputs['product-tax'].parentElement, 'input-append');
    var percentLabel = goog.dom.createDom('span', 'add-on', '%');
    goog.dom.insertSiblingAfter(percentLabel, inputs['product-tax']);

    goog.dom.classes.add(inputs['product-price'].parentElement, 'input-prepend');
    var moneyLabel = goog.dom.createDom('span', 'add-on', '$');
    goog.dom.insertSiblingBefore(moneyLabel, inputs['product-price']);

    if(goog.isDefAndNotNull(product)) {
        inputs['product-brand'].value     = product.getBrand();
        inputs['product-name'].value      = product.getName();
        inputs['product-barcode'].value   = product.getBarCode() > 0 ? product.getBarCode().toString() : '';
        inputs['product-tax'].value       = product.getTaxRate() > 0 ? product.getTaxRate().toString() : '';
        inputs['product-price'].value     = pnc.ui.DECIMAL_FORMATTER.format(product.getPrice());
        inputs['product-threshold'].value = product.getThreshold() > 0 ? product.getThreshold().toString() : '';
        inputs['product-minimum'].value   = product.getMinimum() > 0 ? product.getMinimum().toString() : '';
    }

    return inputs;
};

/**
 * Validates the contents of the form. Will validate the contents of data in the form. Any fields that have invalid data
 * will be marked as such. 
 * @param  {Object.<string, Element>} inputs  Map of input controls in the form
 * @return {boolean}                          Is the form content valid?
 */
pnc.ui.ProductView.prototype.validateProductForm = function(inputs) {
    var intrgx   = /^\d+$/;
    var errors   = goog.dom.getElementsByClass('error', inputs['product-brand'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(inputs['product-brand'].value.length < 1)
        goog.dom.classes.add(inputs['product-brand'].parentElement, 'error');

    if(inputs['product-name'].value.length < 1)
        goog.dom.classes.add(inputs['product-name'].parentElement, 'error');
    
    if(inputs['product-barcode'].value.length > 0 && !intrgx.test(inputs['product-barcode'].value))
        goog.dom.classes.add(inputs['product-barcode'].parentElement, 'error');

    if(inputs['product-tax'].value.length > 0 && !pnc.ui.FLOAT_RGX.test(inputs['product-tax'].value))
        goog.dom.classes.add(inputs['product-tax'].parentElement, 'error');

    if(!pnc.ui.FLOAT_RGX.test(inputs['product-price'].value))
        goog.dom.classes.add(inputs['product-price'].parentElement, 'error');

    if(inputs['product-threshold'].value.length > 0 && !intrgx.test(inputs['product-threshold'].value))
        goog.dom.classes.add(inputs['product-threshold'].parentElement, 'error');

    if(inputs['product-minimum'].value.length > 0 && !intrgx.test(inputs['product-minimum'].value))
        goog.dom.classes.add(inputs['product-minimum'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['product-brand'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the data in product form. Assumes the data is valid. Populates data into a product model.
 * @param  {Object.<string, Element>} inputs  Map of input controls in the form
 * @return {pnc.model.Product}                Product derived
 */
pnc.ui.ProductView.prototype.parseProductForm = function(inputs, product) {
    product       = goog.isDefAndNotNull(product) ? product : new pnc.model.Product();
    var barcode   = inputs['product-barcode'].value.length > 0   ? goog.string.toNumber(inputs['product-barcode'].value)   : 0;
    var threshold = inputs['product-threshold'].value.length > 0 ? goog.string.toNumber(inputs['product-threshold'].value) : 0;
    var minimum   = inputs['product-minimum'].value.length > 0   ? goog.string.toNumber(inputs['product-minimum'].value)   : 0;
    var taxRate   = inputs['product-tax'].value.length > 0       ? goog.string.toNumber(inputs['product-tax'].value)       : 0;

    product.setName(inputs['product-name'].value);
    product.setBrand(inputs['product-brand'].value);
    product.setBarCode(barcode);
    product.setTaxRate(taxRate);
    product.setPrice(pnc.ui.round(inputs['product-price'].value));
    product.setThreshold(threshold);
    product.setMinimum(minimum);

    return product;
};

/**
 * Creates a slide pane with a form that will allow the user to add a new inventory entry.
 * @param  {pnc.model.Product} product   Product for which a new inventory entry will be added.
 * @param  {pnc.ui.Paginator}  paginator Paginator that holds current inventory entry data
 * @private
 */
pnc.ui.ProductView.prototype.addInventory = function(product, paginator) {
    var content = goog.dom.createDom('div');
    var slider  = new pnc.ui.SlidePanel('Nuevo Inventario - ' + product.toString(), pnc.ui.Resources.BARS, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');

    var inputs    = this.createInventoryForm(content);
    var actionBox = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actionBox);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actionBox, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Agregar');
    goog.dom.appendChild(actionBox, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var inventory = response.content();
        var old       = paginator.getData()[paginator.getData().length - 1];
        var change    = paginator.getData().length > 0 && inventory.getCost() != old.getCost();
        paginator.getData().push(inventory);
        goog.array.sort(paginator.getData(), pnc.model.Inventory.compare);
        paginator.refresh();

        pnc.ui.Alert.success('Registro de inventario agregado');
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(change) {
            pnc.ui.Main.breadcrumbPop();
            self.updateProductPrice(product, inventory, old);
        }
    };

    var clickCallback = function() {
        if(!self.validateInventoryForm(inputs))
            return;

        var inventory = self.parseInventoryForm(inputs);
        inventory.setIdProduct(product.getId());
        self.inventoryClient.add(inventory, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Creates a slide panel with a form that updates an existing inventory entry
 * @param  {pnc.model.Product} product    Product for which the inventory entry will be updated
 * @param  {pnc.ui.Paginator}  paginator  Paginator that holds the current inventory entries
 * @private
 */
pnc.ui.ProductView.prototype.updateInventory = function(product, paginator) {
    var row       = goog.dom.getElementsByTagNameAndClass ('tr', 'info', paginator.getElement())[0];
    var inventory = pnc.model.findModel(paginator.getData(), row.dataset.inventory);
    var content   = goog.dom.createDom('div');
    var slider    = new pnc.ui.SlidePanel('Actualizar Inventario - ' + product.toString(), pnc.ui.Resources.BARS, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Actualizar');

    var inputs    = this.createInventoryForm(content, inventory);
    var actionBox = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actionBox);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actionBox, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var updateButton = goog.dom.createDom('button', 'btn btn-primary', 'Actualizar');
    goog.dom.appendChild(actionBox, updateButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newInventory = response.content();
        pnc.model.replaceModel(paginator.getData(), newInventory);
        goog.array.sort(paginator.getData(), pnc.model.Inventory.compare);
        paginator.refresh();
        pnc.ui.Alert.success('Registro de inventario actualizado');
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        var update = goog.dom.getElement('inventory-update-button');
        var remove = goog.dom.getElement('inventory-remove-button');

        if(goog.isDefAndNotNull(update))
            update.disabled = true;

        if(goog.isDefAndNotNull(remove))
            remove.disabled = true;
    };

    var clickCallback = function() {
        if(!self.validateInventoryForm(inputs))
            return;

        var parsedInventory = self.parseInventoryForm(inputs);
        parsedInventory.setId(inventory.getId());
        parsedInventory.setIdProduct(product.getId());
        parsedInventory.meta = inventory.meta;
        self.inventoryClient.update(parsedInventory, responseCallback);
    };
    goog.events.listen(updateButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Creates a dialog that will ask the user to confirm to remove an inventory entry. If the user accepts a transaction
 * will be executed to remove the inventory.
 * @param  {pnc.ui.Paginator} paginator  Paginator that holds the current inventory entries
 * @private
 */
pnc.ui.ProductView.prototype.removeInventory = function(paginator) {
    var row       = goog.dom.getElementsByTagNameAndClass ('tr', 'info', paginator.getElement())[0];
    var inventory = pnc.model.findModel(paginator.getData(), row.dataset.inventory);
    var dialog    = pnc.ui.SlideDialog.warning('Esta seguro que desea eliminar este registro de inventario?');
    var self      = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            dialog.dispose();
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        pnc.model.removeModel(paginator.getData(), inventory);
        paginator.refresh();
        pnc.ui.Alert.success('Registro de inventario eliminado');
        var update = goog.dom.getElement('inventory-update-button');
        var remove = goog.dom.getElement('inventory-remove-button');

        if(goog.isDefAndNotNull(update))
            update.disabled = true;

        if(goog.isDefAndNotNull(remove))
            remove.disabled = true;
    };

    var selectCallback = function(e) {
        if(e.key === pnc.ui.SlideDialog.Keys.OK)
            self.inventoryClient.remove(inventory, responseCallback);
    };
    dialog.setCallback(selectCallback);
    dialog.open();
};

/**
 * Renders an inventory row in the paginator
 * @param  {Element}             parent     Parent element where the inventory representation will be placed
 * @param  {pnc.model.Inventory} inventory  Inventory object to represent
 * @private
 */
pnc.ui.ProductView.prototype.renderInventory = function(parent, inventory) {
    var row = goog.dom.createDom('tr', {'data-inventory' : inventory.getId()});
    goog.dom.appendChild(parent, row);

    var self = this;
    var callback = function(e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');
        var addButton    = goog.dom.getElement('inventory-add-button');
        var updateButton = goog.dom.getElement('inventory-update-button');
        var removeButton = goog.dom.getElement('inventory-remove-button');

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;

        if(goog.isDefAndNotNull(updateButton))
            updateButton.disabled = false;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = false;
    };
    goog.events.listen(row, goog.events.EventType.CLICK, callback);

    var expiration = goog.isDefAndNotNull(inventory.getExpiration()) ? pnc.ui.DATE_FORMATTER.format(inventory.getExpiration()) : '';
    var cells = [];
    cells[0]  = goog.dom.createDom('td', null, inventory.getQuantity().toString());
    cells[1]  = goog.dom.createDom('td', null, inventory.getRemaining().toString());
    cells[2]  = goog.dom.createDom('td', null, pnc.ui.DATE_FORMATTER.format(inventory.getEntered()));
    cells[3]  = goog.dom.createDom('td', null, expiration);
    cells[4]  = goog.dom.createDom('td', null, pnc.ui.DECIMAL_FORMATTER.format(inventory.getCost()));

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Creates a form that will hold inventory details. Used for both creating new inventory objects as well as updating
 * existing ones.
 * @param  {Element}             parent     Parent element where the form will be created
 * @param  {pnc.model.Inventory} inventory  Optional inventory object. If provided, the form will be populated with its details.
 * @return {Object.<string, Element>}       A map with all the created input elements
 * @private
 */
pnc.ui.ProductView.prototype.createInventoryForm = function(parent, inventory) {
    var fields = {'inventory-quantity'   : ['Cantidad',   'Debe ser un numero', true,  false],
                  'inventory-remaining'  : ['Restante',   'Debe ser un numero', true,  true],
                  'inventory-expiration' : ['Expiracion', '',                   false, false],
                  'inventory-cost'       : ['Costo',      'Debe ser un numero', true,  false]};
    var inputs = {};

    var container = goog.dom.createDom('div', {'id' : 'inventory-form', 'style' : 'margin: 20px 0'});
    goog.dom.appendChild(parent, container);

    for(var key in fields) {
        if(!goog.isDefAndNotNull(inventory) && fields[key][3])
            continue;

        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(container, group);

        var label = goog.dom.createDom('label', {'for' : key, 'class' : 'span2'}, fields[key][0]);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span2'});
        inputs[key].type = fields[key][1].length > 0 ? 'text' : 'date';
        goog.dom.appendChild(group, inputs[key]);

        if(fields[key][2])
            pnc.ui.assureNumbers(inputs[key]);

        if(fields[key][1].length > 0) {
            var help = goog.dom.createDom('span', 'help-inline', fields[key][1]);
            goog.dom.appendChild(group, help);
        }
    }

    goog.dom.classes.add(inputs['inventory-cost'].parentElement, 'input-prepend');
    var moneyLabel = goog.dom.createDom('span', 'add-on', '$');
    goog.dom.insertSiblingBefore(moneyLabel, inputs['inventory-cost']);

    if(goog.isDefAndNotNull(inventory)) {
        inputs['inventory-quantity'].value   = inventory.getQuantity().toString();
        inputs['inventory-expiration'].value = goog.isDefAndNotNull(inventory.getExpiration()) ? pnc.client.DATE_FORMATTER.format(inventory.getExpiration()) : '';
        inputs['inventory-cost'].value       = inventory.getCost().toString();

        if(inventory.getId() != pnc.model.DEFAULT_ID)
            inputs['inventory-remaining'].value = inventory.getRemaining().toString();
    }

    return inputs;
};

/**
 * Validates the contents of the inventory form. If an error is found in any of the form items, it will be displayed to
 * the user.
 * @param  {Object.<string, Element>} inputs  Map of inputs to verify
 * @return {boolean}                          true if all fields were found to be valid, false if at least one had an issue
 * @private
 */
pnc.ui.ProductView.prototype.validateInventoryForm = function(inputs) {
    var intrgx   = /^\d+$/;
    var errors   = goog.dom.getElementsByClass('error', inputs['inventory-quantity'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(!intrgx.test(inputs['inventory-quantity'].value))
        goog.dom.classes.add(inputs['inventory-quantity'].parentElement, 'error');

    if(goog.isDefAndNotNull(inputs['inventory-remaining']) && !intrgx.test(inputs['inventory-remaining'].value))
        goog.dom.classes.add(inputs['inventory-remaining'].parentElement, 'error');

    if(!pnc.ui.FLOAT_RGX.test(inputs['inventory-cost'].value))
        goog.dom.classes.add(inputs['inventory-cost'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['inventory-quantity'].parentElement.parentElement);

    return errors.length < 1;
};

/**
 * Parses an inventory from the data in the map of inputs.
 * @param  {Object.<string, Element>} inputs     Inputs to read data from
 * @param  {pnc.model.Inventory}      inventory  Optional inventory object. If provided its details will be updated. If
 *                                               none is provided, a new inventory object will be created.
 * @return {pnc.model.Inventory}                 Inventory object with data populated from the form contents.
 */
pnc.ui.ProductView.prototype.parseInventoryForm = function(inputs, inventory) {
    var quantity   = goog.string.toNumber(inputs['inventory-quantity'].value);
    var cost       = pnc.ui.round(inputs['inventory-cost'].value);
    var remaining  = 0;
    var expiration = null;

    if(goog.isDefAndNotNull(inventory))
        remaining = inventory.getRemaining();
    else if(goog.isDefAndNotNull(inputs['inventory-remaining']))
        remaining = goog.string.toNumber(inputs['inventory-remaining'].value);
    else
        remaining = quantity;

    if(goog.isDefAndNotNull(inputs['inventory-expiration'].value) && inputs['inventory-expiration'].value.length > 0) {
        expiration = new Date();        
        pnc.client.DATE_PARSER.parse(inputs['inventory-expiration'].value, expiration);
    }

    inventory = goog.isDefAndNotNull(inventory) ? inventory : new pnc.model.Inventory();
    inventory.setQuantity(quantity);
    inventory.setRemaining(remaining);
    inventory.setCost(cost);
    inventory.setExpiration(expiration);

    return inventory;
};

/**
 * Creates a dialog alerting the user that there is a difference in the cost of the product. If the user agrees to
 * continue a form will be presented that will help the user calculate the selling price for the product moving forward.
 * @param  {pnc.model.Product} product         Product that is being processed
 * @param  {pnc.model.Inventory} newInventory  New inventory object that was just generated. This has the new cost.
 * @param  {pnc.model.Inventory} oldInventory  Most recent inventory object before the introduction of 'newInventory'. This
 *                                             has the previous cost.
 * @private
 */
pnc.ui.ProductView.prototype.updateProductPrice = function(product, newInventory, oldInventory) {
    var message = 'El costo de ' + product.toString() + ' ha cambiado. Desea re-calcular el precio de venta?';
    var dialog  = pnc.ui.SlideDialog.warning(message);
    var self    = this;
    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        var panel  = goog.dom.createDom('div');
        var inputs = self.createPriceUpdateForm(panel, product, newInventory, oldInventory);

        var controlPane = goog.dom.createDom('div', 'box-actions');
        goog.dom.appendChild(panel, controlPane);

        var closeCallback = function() { 
            slider.dispose();
            breadcrumbPop();
        };
        var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
        goog.dom.appendChild(controlPane, cancelButton);
        goog.events.listen(cancelButton, goog.events.EventType.CLICK, closeCallback);
        var oldPrice = 0;

        var responseCallback = function(response) {
            if(response.status() !== pnc.client.Status.OK) {
                product.setPrice(oldPrice);
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
            }

            self.paginator.refresh();
            slider.dispose();
            pnc.ui.Main.breadcrumbPop();
            pnc.ui.Alert.success('El precio del producto ha sido actualizado');
        };

        var updateCallback = function() {
            if(!self.validatePriceUpdateForm(inputs))
                return;

            var newPrice = pnc.ui.round(inputs['product-price'].value);
            oldPrice = product.getPrice();
            product.setPrice(pnc.ui.round(newPrice));
            self.productClient.update(product, responseCallback);
        };

        var updateButton = goog.dom.createDom('button', 'btn btn-primary', 'Actualizar');
        goog.dom.appendChild(controlPane, updateButton);
        goog.events.listen(updateButton, goog.events.EventType.CLICK, updateCallback);

        var slider = new pnc.ui.SlidePanel('Actualizar precio de ' + product.toString(), pnc.ui.Resources.PLAY, panel);
        slider.render();
        pnc.ui.Main.breadcrumbPush('Actualizar Precio');
    };
    dialog.setCallback(selectCallback);
    dialog.open();
};

/**
 * Creates a form used to make calulations to help update the price of a product. It relies of information from the product
 * and the most recent two inventory objects. It will calculate the profit margin based on the old inventory, and use it
 * plus the tax percentage to calculate what the new price should be based, on the new inventory cost. The user has the
 * ability to override this value if he/she desires. 
 * @param  {Element}             parent        Parent elemnt where the form will be placed
 * @param  {pnc.model.Product}   product       Product that is being processed
 * @param  {pnc.model.Inventory} newInventory  New inventory object that was just generated. This has the new cost.
 * @param  {pnc.model.Inventory} oldInventory  Most recent inventory object before the introduction of 'newInventory'. This
 *                                             has the previous cost.
 * @return {Object.<string, Element>}          Map with inputs within the form
 */
pnc.ui.ProductView.prototype.createPriceUpdateForm = function(parent, product, newInventory, oldInventory) {
    var profit = (product.getPrice() / (1 + product.getTaxRate() / 100)) - oldInventory.getCost();
    var price  = newInventory.getCost() + (newInventory.getCost() * profit / 100);
    price      = price + (price * product.getTaxRate() / 100);
    var fields = {
        'product-cost'   : ['Costo Nuevo', '$' + pnc.ui.DECIMAL_FORMATTER.format(newInventory.getCost()), false],
        'product-profit' : ['Beneficio',    pnc.ui.DECIMAL_FORMATTER.format(profit),                      true],
        'product-tax'    : ['Impuesto',     pnc.ui.DECIMAL_FORMATTER.format(product.getTaxRate()) + '%',  false],
        'product-price'  : ['Precio Nuevo', pnc.ui.DECIMAL_FORMATTER.format(price),                       true]
    };
    var inputs = {};

    for(var key in fields) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'class' : 'span2'}, fields[key][0]);
        goog.dom.appendChild(group, label);

        if(fields[key][2]) {
            inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span2', 'type' : 'text', 'value' : fields[key][1]});
            goog.dom.appendChild(group, inputs[key]);
            pnc.ui.assureNumbers(inputs[key]);

            var help = goog.dom.createDom('span', 'help-inline', 'Debe ser un numero');
            goog.dom.appendChild(group, help);
        } else {
            var sticker = goog.dom.createDom('label', {'id' : key, 'class' : 'span2', 'style' : 'margin: 0; font-size: 18px'}, fields[key][1]);
            goog.dom.appendChild(group, sticker);
        }
    }

    goog.dom.classes.add(inputs['product-profit'].parentElement, 'input-append');
    var percentLabel = goog.dom.createDom('span', 'add-on', '%');
    goog.dom.insertSiblingAfter(percentLabel, inputs['product-profit']);

    goog.dom.classes.add(inputs['product-price'].parentElement, 'input-prepend');
    var moneyLabel = goog.dom.createDom('span', 'add-on', '$');
    goog.dom.insertSiblingBefore(moneyLabel, inputs['product-price']);

    var profitCallback = function(e) {
        var newProfit = inputs['product-profit'].value.length > 0 ? pnc.ui.round(inputs['product-profit'].value) : 0;
        var newPrice  = newInventory.getCost() + (newInventory.getCost() * newProfit / 100);
        newPrice      = newPrice + (newPrice * product.getTaxRate() / 100);
        inputs['product-price'].value = pnc.ui.round(newPrice).toString();
    };
    goog.events.listen(inputs['product-profit'], goog.events.EventType.KEYUP, profitCallback);

    var priceCallback = function(e) {
        var newPrice  = inputs['product-price'].value.length > 0 ? pnc.ui.round(inputs['product-price'].value) : 0;
        var beforeTax = pnc.ui.round(newPrice / (1 + product.getTaxRate() / 100));
        var newProfit = pnc.ui.round(newPrice - newInventory.getCost());
        inputs['product-profit'].value = newProfit.toString();
    };
    goog.events.listen(inputs['product-price'], goog.events.EventType.KEYUP, priceCallback);

    return inputs;
};

/**
 * Validates the price update form. If any errors are found, they will be highlighted on the form itself.
 * @param  {Object.<string, Element>} inputs  Inputs created with the 'createPriceUpdateForm' method.
 * @return {boolean}                          true if data was validated OK, false otherwise.
 */
pnc.ui.ProductView.prototype.validatePriceUpdateForm = function(inputs) {
    var errors   = goog.dom.getElementsByClass('error', inputs['product-profit'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(!pnc.ui.FLOAT_RGX.test(inputs['product-profit'].value))
        goog.dom.classes.add(inputs['product-profit'].parentElement, 'error');

    if(!pnc.ui.FLOAT_RGX.test(inputs['product-price'].value))
        goog.dom.classes.add(inputs['product-price'].parentElement, 'error');  

    errors = goog.dom.getElementsByClass('error', inputs['product-profit'].parentElement.parentElement);

    return errors.length < 1;  
};
