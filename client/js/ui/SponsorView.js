goog.provide('pnc.ui.SponsorView');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');

goog.require('pnc.client');
goog.require('pnc.client.Sponsor');
goog.require('pnc.client.Invoice');
goog.require('pnc.client.Product');
goog.require('pnc.model.Sponsor');
goog.require('pnc.ui');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.ContactForm');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.Paginator');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');

/**
 * View that is used to view and alter data on suppliers
 * @constructor
 */
pnc.ui.SponsorView = function() {
    /**
     * Client used to perform sponsor related transactions
     * @type {pnc.client.Sponsor}
     * @private
     */
    this.client        = new pnc.client.Sponsor();

    /**
     * Client used to perform invoice related transactions
     * @type {pnc.client.Invoice}
     * @private
     */
    this.invoiceClient = new pnc.client.Invoice();

    /**
     * Client used to perform product related transactions
     * @type {pnc.client.Product}
     * @private
     */
    this.productClient = new pnc.client.Product();

    //------------- elements -------------//
    /** @private */ this.container            = null;
    /** @private */ this.paginator            = null;
    /** @private */ this.sponsorshipPaginator = null;
    /** @private */ this.disposables          = [];
};
pnc.ui.Main.registerModule('sponsors', pnc.ui.SponsorView);


/**
 * Renders the sponsor view
 * @param  {Element} parent  Parent element where the sponsor view will be placed
 */
pnc.ui.SponsorView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderSearchPane();
    this.renderActionPane();
    this.renderSponsorPane();
    this.renderDetailPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Patrocinadores');
};

/**
 * Disposes fo the view and releases any resources that may be in use
 */
pnc.ui.SponsorView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        if(goog.isDefAndNotNull(this.disposables[cont]) && goog.isDef(this.disposables[cont].dispose))
            this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};

/**
 * Renders the search pane portion of the view. Allows the user to search for sponsors by organization name.
 * @private
 */
pnc.ui.SponsorView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var nameLabel = goog.dom.createDom('label', 'span1', 'Nombre');
    goog.dom.appendChild(content, nameLabel);
    var nameSearch = goog.dom.createDom('input', {'id' : 'name-search', 'class' : 'span3', 'type' : 'text'});
    goog.dom.appendChild(content, nameSearch);

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Buscar');
    goog.dom.appendChild(controlPane, searchButton);
    var self = this;

    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateSponsorPane(response.content());
    };

    var clickCallback = function() {
        if(nameSearch.value.length < 1) {
            goog.dom.classes.add(nameSearch, 'validation-error');
            return;
        }

        goog.dom.classes.remove(nameSearch, 'validation-error');
        self.client.search(nameSearch.value, searchCallback);
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Renders the pane that provides the action to add an sponsor
 * @private
 */
pnc.ui.SponsorView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.SPONSOR_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));
    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Patrocinador');
    goog.dom.appendChild(addButton, span);
};

/**
 * Renders the pane that shows the list of sponsors that match the provided search criteria
 * @private
 */
pnc.ui.SponsorView.prototype.renderSponsorPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Patrocinadores');
    content.id  = 'sponsor-pane';
    content.style.minHeight = '390px';
    var head = goog.dom.getElementsByClass('box-head', content.parentElement)[0];

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var editButton, removeButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.SPONSOR_SPONSORSHIP))
        pnc.ui.createToolbarButton(actionGroup, 'invoice-button', pnc.ui.Resources.INVOICE, 'Patrocinios', false, goog.bind(this.sponsorshipDetail, this));

    if(goog.array.contains(privs, pnc.client.Privileges.SPONSOR_UPDATE))    
        editButton = pnc.ui.createToolbarButton(actionGroup, 'edit-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.edit, this));

    if(goog.array.contains(privs, pnc.client.Privileges.SPONSOR_REMOVE))
        editButton = pnc.ui.createToolbarButton(actionGroup, 'remove-button', pnc.ui.Resources.REMOVE, 'Remover', false, goog.bind(this.remove, this));

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var navCallback = function(e) {
        if(!goog.isNull(editButton))
            editButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallback);

    var titles = ['Organizacion', 'Nombre'];
    this.paginator = new pnc.ui.Paginator(titles, [], null, pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderSponsor, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Renders the pane that provides additional details for the selected sponsor in the sponsor pane
 * @private
 */
pnc.ui.SponsorView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '157px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 1, 'Patrocinador');
    body.id  = 'detail-table';
};

/**
 * Paginator callback. Used to render a row that represents a sponsor.
 * @param  {Element}           parent   Parent element where the row will be placed
 * @param  {pnc.model.Sponsor} sponsor  Sponsor model to represent
 * @private
 */
pnc.ui.SponsorView.prototype.renderSponsor = function(parent, sponsor) {
    var row = goog.dom.createDom('tr', {'data-sponsor' : sponsor.getId()});
    goog.dom.appendChild(parent, row);
    var buttons = [
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button'),
        goog.dom.getElement('invoice-button')
    ];
    var self = this;

    var callback = function(sponsor, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;

        self.populateDetailPane(sponsor, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, sponsor));

    var cells = [
        goog.dom.createDom('td', null, sponsor.getOrganization()),
        goog.dom.createDom('td', null, sponsor.getFirstName() + ' ' + sponsor.getLastName())
    ];

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Populates the paginator with data once the search results are retrieved
 * @param  {Array.<pnc.model.Sponsor>} sponsors  Sponsors to place in paginator
 * @private
 */
pnc.ui.SponsorView.prototype.populateSponsorPane = function(sponsors) {
    var content       = goog.dom.getElement('sponsor-pane');
    var editButton    = goog.dom.getElement('edit-button');
    var removeButton  = goog.dom.getElement('remove-button');
    var backButton    = goog.dom.getElement('back-button');
    var forwardButton = goog.dom.getElement('forward-button');
    var detailTable   = goog.dom.getElement('detail-table');

    goog.dom.removeChildren(detailTable);
    backButton.disabled    = true;
    forwardButton.disabled = goog.isDefAndNotNull(sponsors) && sponsors.length <= pnc.ui.PAGE_SIZE;

    if(goog.isDefAndNotNull(editButton))
        editButton.disabled = true;

    if(goog.isDefAndNotNull(removeButton))
        removeButton.disabled = true;

    this.paginator.setData(sponsors);
    this.paginator.refresh();
};

/**
 * Populates the detail pane with additional info for the selected sponsor
 * @param  {pnc.model.Sponsor} sponsor  Selected sponsor
 * @private
 */
pnc.ui.SponsorView.prototype.populateDetailPane = function(sponsor) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);

    if(!goog.isDefAndNotNull(sponsor))
        return;

    pnc.ui.createSummaryContent(body, sponsor.getOrganization());
    var address  = sponsor.getAddress();
    var contacts = sponsor.getContacts();

    if(goog.isDefAndNotNull(address)) {
        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td');
        goog.dom.appendChild(row, cell);
        var data = [address.getStreet1(), address.getStreet2(), address.getCity(), address.getProvince()];

        for(var cont = 0, size = data.length; cont < size; cont++) {
            var entry = goog.dom.createDom('div', null, data[cont]);
            goog.dom.appendChild(cell, entry);
        }
    } else {
        pnc.ui.createSummaryContent(body, 'Ninguna direccion registrada');
        goog.dom.getElementsByTagNameAndClass('td', null, body)[0].style.textAlign = 'center';
    }

    if(goog.isDefAndNotNull(contacts) && contacts.length > 0) {
        var row  = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td', {'colspan' : '2'});
        goog.dom.appendChild(row, cell);

        for(var cont = 0, size = contacts.length; cont < size; cont++) {
            var entry = goog.dom.createDom('div', null, contacts[cont].toString());
            goog.dom.appendChild(cell, entry);
        }
    } else {
        var row = pnc.ui.createSummaryContent(body, 'Ningun contacto registrado');
        row.children[0].style.textAlign = 'center';
    }

    var row = pnc.ui.createSummaryContent(body, 'Patrocinios Pendientes');
    row.children[0].style.fontWeight = 'bold';
    row.children[0].style.textAlign  = 'center';
    row = pnc.ui.createSummaryContent(body, '');
    var sponsorshipCell = row.children[0];
    sponsorshipCell.id = 'unpaid-sponsorships-cell';

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                goog.dom.setTextContent(sponsorshipCell, pnc.ui.DECIMAL_FORMATTER.format(0));
                break;

            case pnc.client.Status.OK:
                var total = 0;
                var sponsorships = response.content();

                for(var cont = 0, size = sponsorships.length; cont < size; cont++)
                    total += sponsorships[cont].getSponsored();

                goog.dom.setTextContent(sponsorshipCell, pnc.ui.DECIMAL_FORMATTER.format(total));
                break;

            default:
                pnc.ui.Alert.error('Error al tratar de calcular el total de patricinios pendientes.');
                break;
        }
    };
    this.client.getSponsorshipsByPaid(sponsor, false, callback);
};

/**
 * Provides a form that will allow the user to add a new sponsor
 * @private
 */
pnc.ui.SponsorView.prototype.add = function() {
    var button  = goog.dom.getElement('add-button');
    var buttons = [
        goog.dom.getElement('invoice-button'),
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button')
    ];

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var content     = goog.dom.createDom('div');
    var inputs      = this.createSponsorForm(content, null);
    var contactForm = new pnc.ui.ContactForm();
    contactForm.render(content);
    contactForm.getElement().style.marginTop = '-10px';
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        contactForm.dispose();
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(button))
            button.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        var sponsor = response.content();
        self.paginator.getData().push(sponsor);
        goog.array.sort(self.paginator.getData(), pnc.model.Sponsor.compare);
        self.paginator.refresh();
        self.populateDetailPane(null);

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El patrocinador fue agregado');
    };

    var saveCallback = function() {
        if(!self.validateSponsorForm(inputs))
            return;

        var sponsor = self.parseSponsorForm(inputs);
        sponsor.setContacts(contactForm.getContacts());
        self.client.add(sponsor, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar Patrocinador', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Provides a form that will allow the user to update the details on an existing sponsor
 * @private
 */
pnc.ui.SponsorView.prototype.edit = function() {
    var row     = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var sponsor = pnc.model.findModel(this.paginator.getData(), row.dataset.sponsor);
    var button  = goog.dom.getElement('add-button');
    var buttons = [
        goog.dom.getElement('invoice-button'),
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button')
    ];

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var content     = goog.dom.createDom('div');
    var inputs      = this.createSponsorForm(content, sponsor);
    var contactForm = new pnc.ui.ContactForm(sponsor.getContacts());
    contactForm.render(content);
    contactForm.getElement().style.marginTop = '-10px';
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() {
        contactForm.dispose();
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(button))
            button.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newSponsor = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newSponsor);
        self.paginator.refresh();
        self.populateDetailPane(newSponsor);

        if(goog.isDefAndNotNull(button))
            button.disabled = false;

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El patrocinador fue actualizado');
    };
    var saveCallback = function() {
        if(!self.validateSponsorForm(inputs))
            return;

        var newSponsor = self.parseSponsorForm(inputs, sponsor);
        newSponsor.setId(sponsor.getId());
        newSponsor.setContacts(contactForm.getContacts());
        self.client.update(newSponsor, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog. If the user accepts, the transaction to remove a sponsor is initiated.
 * @private
 */
pnc.ui.SponsorView.prototype.remove = function() {
    var row     = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var sponsor = pnc.model.findModel(this.paginator.getData(), row.dataset.sponsor);
    var dialog  = pnc.ui.SlideDialog.warning('Esta seguro que desea remover este patrocinador? Tambien se eliminaran los ' +
                    'vinculos a facturas patrocinadas.');
    var self    = this;
    var buttons = [
        goog.dom.getElement('invoice-button'),
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button')
    ];

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Este patrocinador tiene referencias a otra data. No puede ser removido.').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), sponsor);
        self.paginator.refresh();
        self.populateDetailPane(null);

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        pnc.ui.Alert.success('El patrocinador fue removido');
    };
    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.remove(sponsor, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates the form used to add and update sponsor details
 * @param  {Element}           parent   Parent element where the form will be placed
 * @param  {pnc.model.Sponsor} sponsor  Optional sponsor. If provided the form will be pre-poulated with its details.
 * @return {Object.<string, Element>}   Map with the form input elements
 * @private
 */
pnc.ui.SponsorView.prototype.createSponsorForm = function(parent, sponsor) {
    var inputs = {};
    var header = goog.dom.createDom('h4', 'form-header', 'Patrocinador');
    goog.dom.appendChild(parent, header);

    var create = function(key, name, help) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'style' : 'width: 100px'}, name);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span3', 'type' : 'text'});
        goog.dom.appendChild(group, inputs[key]);

        if(!goog.isNull(help)) {
            var help = goog.dom.createDom('span', 'help-inline', help);
            goog.dom.appendChild(group, help);
        }
    };

    var fields = {
        'sponsor-organization' : ['Organizacion', 'No puede estar vacio'],
        'sponsor-first'        : ['Nombre',       'No puede estar vacio'],
        'sponsor-last'         : ['Apellido',     'No puede estar vacio']
    };

    for(var key in fields)
        create(key, fields[key][0], fields[key][1]);

    if(goog.isDefAndNotNull(sponsor)) {
        inputs['sponsor-organization'].value = sponsor.getOrganization();
        inputs['sponsor-first'].value        = sponsor.getFirstName();
        inputs['sponsor-last'].value         = sponsor.getLastName();
    }

    header = goog.dom.createDom('h4', 'form-header', 'Direccion');
    goog.dom.appendChild(parent, header);

    fields = {
        'address-street1'  : ['Calle 1',   'Si va a incluir una direccion, este campo no puede estar vacio'],
        'address-street2'  : ['Calle 2',   null],
        'address-city'     : ['Ciudad',    'Si va a incluir una direccion, este campo no puede estar vacio'],
        'address-province' : ['Provincia', 'Si va a incluir una direccion, este campo no puede estar vacio'],
        'address-country'  : ['Pais',      null]
    };

    for(var key in fields)
        create(key, fields[key][0], fields[key][1]);

    if(goog.isDefAndNotNull(sponsor)) {
        inputs['sponsor-organization'].value = sponsor.getOrganization();
        inputs['sponsor-first'].value        = sponsor.getFirstName();
        inputs['sponsor-last'].value         = sponsor.getLastName();
        var address = sponsor.getAddress();

        if(goog.isDefAndNotNull(address)) {
            inputs['address-street1'].value  = goog.isNull(address.getStreet1())  ? '' : address.getStreet1();
            inputs['address-street2'].value  = goog.isNull(address.getStreet2())  ? '' : address.getStreet2();
            inputs['address-city'].value     = goog.isNull(address.getCity())     ? '' : address.getCity();
            inputs['address-province'].value = goog.isNull(address.getProvince()) ? '' : address.getProvince();
            inputs['address-country'].value  = goog.isNull(address.getCountry())  ? '' : address.getCountry();
        }
    }

    header = goog.dom.createDom('h4', 'form-header', 'Contacto');
    goog.dom.appendChild(parent, header);

    return inputs;
};

/**
 * Validates the form to make sure all data entry is avlid.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @return {boolean}                          Is the from data valid?
 * @private
 */
pnc.ui.SponsorView.prototype.validateSponsorForm = function(inputs) {
    var errors = goog.dom.getElementsByClass('error', inputs['sponsor-organization'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    var test = function(key) {
        if(inputs[key].value.length < 1)
            goog.dom.classes.add(inputs[key].parentElement, 'error');
    };

    test('sponsor-organization');
    test('sponsor-first');
    test('sponsor-last');

    var addressFields   = ['address-street1', 'address-street2', 'address-city', 'address-province', 'address-country'];
    var addressIncluded = false;

    for(var cont = 0, size = addressFields.length; cont < size; cont++) {
        var key = addressFields[cont];

        if(inputs[key].value.length > 0) {
            addressIncluded = true;
            break;
        }
    }

    if(addressIncluded) {
        for(var cont = 0, size = addressFields.length; cont < size; cont++)
            test(addressFields[cont]);
    }

    errors = goog.dom.getElementsByClass('error', inputs['sponsor-organization'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the content in the sponsor form and creates a sponsor model object from it.
 * @param  {Object.<string, Element} inputs   Form input elements
 * @param  {pnc.model.Sponsor}       sponsor  Optional sponsor. If provided its details will be updated with for data.
 *                                              If not a new sponsor will be created.
 * @private
 */
pnc.ui.SponsorView.prototype.parseSponsorForm = function(inputs, sponsor) {
    sponsor = goog.isDefAndNotNull(sponsor) ? sponsor : new pnc.model.Sponsor();
    sponsor.setOrganization(inputs['sponsor-organization'].value);
    sponsor.setFirstName(inputs['sponsor-first'].value);
    sponsor.setLastName(inputs['sponsor-last'].value);

    if(inputs['address-street1'].value.length > 0) {
        var address = goog.isDefAndNotNull(sponsor.getAddress()) ? sponsor.getAddress() : new pnc.model.Address();
        address.setStreet1( inputs['address-street1'].value);
        address.setStreet2( inputs['address-street2'].value);
        address.setCity(    inputs['address-city'].value);
        address.setProvince(inputs['address-province'].value);
        address.setCountry( inputs['address-country'].value);
        sponsor.setAddress(address);
    }

    return sponsor;
};

/**
 * Provides a slide panel with information related to the sponsorships made by a sponsor.
 * @private
 */
pnc.ui.SponsorView.prototype.sponsorshipDetail = function() {
    var button = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var row     = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var sponsor = pnc.model.findModel(this.paginator.getData(), row.dataset.sponsor);
    var content = goog.dom.createDom('div');
    var slider  = new pnc.ui.SlidePanel('Patrocinios hechos por ' + sponsor.getOrganization(), pnc.ui.Resources.SHAKING_HANDS, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Patrocinios');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 5px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);

    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);
    var self = this;

    var callback = function() {
        if(goog.isDefAndNotNull(button))
            button.disabled = false;

        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'sponsor-back-button', pnc.ui.Resources.BACK, 'Regresar al Patrocinador', true, callback);
    var privs = pnc.client.Profile.getPrivileges();

    if(goog.array.contains(privs, pnc.client.Privileges.SPONSOR_SPONSORSHIP))
        pnc.ui.createToolbarButton(actionGroup, 'sponsorship-update-button', pnc.ui.Resources.EDIT, 'Editar Patrocinio', false, goog.bind(this.updateSponsorship, this));

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    this.sponsorshipPaginator = new pnc.ui.Paginator(['Factura', 'Fecha', 'Total', 'Patrocinio', 'Pagado'], [], 
                            'table-condensed table-bordered summary-table table-striped', 20, backButton, forwardButton);
    this.sponsorshipPaginator.setRowRenderer(goog.bind(this.renderSponsorship, this, sponsor));
    this.sponsorshipPaginator.render(content);
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.NOT_FOUND:
                self.sponsorshipPaginator.setData(response.content());
                self.sponsorshipPaginator.refresh();
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                break;
        }
    };
    this.client.getSponsorships(sponsor, callback);
};

/**
 * Paginator callback. Used to create a row that represents a sponsorship within the paginator.
 * @param  {pnc.model.Sponsor}     sponsor      Sponsor that made the sponsorship
 * @param  {Element}               parent       Parent element where the representation will be placed
 * @param  {pnc.model.Sponsorship} sponsorship  Sponsorship element to represent
 * @private
 */
pnc.ui.SponsorView.prototype.renderSponsorship = function(sponsor, parent, sponsorship) {
    var row = goog.dom.createDom('tr');
    row.dataset.sponsorship = sponsorship.getId().toString();
    goog.dom.appendChild(parent, row);
    var cell = null;

    var data = [
        '',
        pnc.ui.DATE_FORMATTER.format(sponsorship.getTimestamp()),
        pnc.ui.DECIMAL_FORMATTER.format(sponsorship.getTotal()),
        pnc.ui.DECIMAL_FORMATTER.format(sponsorship.getSponsored()),
        ''
    ];

    for(var cont = 0, size = data.length; cont < size; cont++) {
        cell = goog.dom.createDom('td', {'style' : 'text-align: right'}, data[cont]);
        goog.dom.appendChild(row, cell);
        cell.style.width = 'initial';
    }

    cell.style.textAlign = 'center'
    var link = goog.dom.createDom('a', null, sponsorship.getIdInvoice().toString());
    goog.dom.appendChild(row.children[0], link);
    var self = this;

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                pnc.ui.Alert.error('Los datos de esta factura no fueron encontrados');
                break;

            case pnc.client.Status.OK:
                self.invoiceDetail(response.content());
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                break;
        }
    };
    var clickCallback = function() {self.invoiceClient.get(sponsorship.getIdInvoice(), responseCallback); };
    goog.events.listen(link, goog.events.EventType.CLICK, clickCallback);

    if(sponsorship.isPaid()) {
        var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.RESULT, 'title' : 'Pago'});
        goog.dom.appendChild(row.children[4], img);
    }

    var editButton = goog.dom.getElement('sponsorship-update-button');
    var callback = function(sponsorship, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = false;
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, sponsorship));
};

/**
 * Provides a form that the user will use to update the details on a sponsorship.
 * @param  {pnc.model.Sponsorship} sponsorship  Sponsorship to update
 * @private
 */
pnc.ui.SponsorView.prototype.updateSponsorship = function(sponsorship) {
    var row         = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.sponsorshipPaginator.getElement())[0];
    var sponsorship = pnc.model.findModel(this.sponsorshipPaginator.getData(), row.dataset.sponsorship);
    var paid        = sponsorship.isPaid();
    var content     = goog.dom.createDom('div');
    var inputs      = this.createSponsorshipForm(content, sponsorship);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newSponsorship = response.content();
        pnc.model.replaceModel(self.sponsorshipPaginator.getData(), newSponsorship);
        self.sponsorshipPaginator.refresh();

        if(newSponsorship.isPaid() && !paid) {
            var cell  = goog.dom.getElement('unpaid-sponsorships-cell');
            var value = pnc.ui.DECIMAL_FORMATTER.parse(goog.dom.getTextContent(cell));
            value -= newSponsorship.getSponsored();
            goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(value));
        } else if(!newSponsorship.isPaid() && paid) {
            var cell  = goog.dom.getElement('unpaid-sponsorships-cell');
            var value = pnc.ui.DECIMAL_FORMATTER.parse(goog.dom.getTextContent(cell));
            value += newSponsorship.getSponsored();
            goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(value));
        }

        cancelCallback();
        pnc.ui.Alert.success('El patrocinio fue actualizado');

        var button = goog.dom.getElement('sponsorship-update-button');

        if(goog.isDefAndNotNull(button))
            button.disabled = true;
    };

    var saveCallback = function() { 
        self.parseSponsorshipForm(inputs, sponsorship);
        self.client.updateSponsorship(sponsorship, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Creates a form that will be used to update the details on a sponsorhip
 * @param  {Element}               parent       Parent element where the form will be placed
 * @param  {pnc.model.Sponsorship} sponsorship  Sponsorship model that will be updated
 * @return {Object.<string, Element>}           Form input elements
 */
pnc.ui.SponsorView.prototype.createSponsorshipForm = function(parent, sponsorship) {
    var inputs = {};
    var group, label = null;
    var labels = {
        'Fecha'      : pnc.ui.DATE_TIME_FORMATTER.format(sponsorship.getTimestamp()),
        'Total'      : pnc.ui.DECIMAL_FORMATTER.format(sponsorship.getTotal()),
        'Patrocinio' : pnc.ui.DECIMAL_FORMATTER.format(sponsorship.getSponsored())
    };

    for(var key in labels) {
        group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);
        label = goog.dom.createDom('label', 'span1', key);
        goog.dom.appendChild(group, label);
        var value = goog.dom.createDom('label', 'span2', labels[key]);
        goog.dom.appendChild(group, value);
    }

    group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    label = goog.dom.createDom('label', {'for' : 'paid-value', 'class' : 'span1'}, 'Pago');
    goog.dom.appendChild(group, label);
    inputs['paid-value'] = goog.dom.createDom('input', {'type' : 'checkbox', 'id' : 'paid-value', 'style' : 'margin-left: 20px'});
    goog.dom.appendChild(group, inputs['paid-value']);
    inputs['paid-value'].checked = sponsorship.isPaid();

    return inputs;
};

/**
 * Parses the contents of the sponsorship form and updates the model.
 * @param  {Object.<string, Element>} inputs       Form input elements
 * @param  {pnc.model.Sponsorship}    sponsorship  Sponsorship to update
 * @private
 */
pnc.ui.SponsorView.prototype.parseSponsorshipForm = function(inputs, sponsorship) {
    sponsorship.setPaid(inputs['paid-value'].checked);
};

/**
 * Provides a read-only view of an invoice and its items. Used to present the contents of the invoice that was
 * sponsored.
 * @param  {pnc.model.Invoice} invoice  Invoice to show
 * @private
 */
pnc.ui.SponsorView.prototype.invoiceDetail = function(invoice) {
    var content  = goog.dom.createDom('div', {'id' : 'invoice-detail-content'});
    var slider   = new pnc.ui.SlidePanel('Factura #' + invoice.getId().toString(), pnc.ui.Resources.INVOICE, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Factura');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var self = this;

    var callback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'credit-line-back-button', pnc.ui.Resources.BACK, 'Regresar a la Linea de Credito', true, callback);

    var data = {
        'Fecha' : pnc.ui.DATE_FORMATTER.format(invoice.getTimestamp()),
        'Hora'  : pnc.ui.TIME_FORMATTER.format(invoice.getTimestamp()),
        'Pago'  : pnc.ui.InvoiceView.Payment[invoice.getPayment()],
        'Total' : pnc.ui.DECIMAL_FORMATTER.format(invoice.getTotal())
    };

    for(var key in data) {
        var label = goog.dom.createDom('label', {'style' : 'width: 50px'}, key);
        goog.dom.appendChild(content, label);

        var value = goog.dom.createDom('label', 'span2 bold', data[key]);
        goog.dom.appendChild(content, value);
    }

    var itemHeader = goog.dom.createDom('h4', {'class' : 'form-header', 'style' : 'margin: 10px 5px'}, 'Itemes');
    goog.dom.appendChild(content, itemHeader);

    var productCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var products = response.content();
        var data     = [];

        for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
            var item    = invoice.getItems()[cont];
            var product = pnc.model.findModel(products, item.getIdProduct());
            var row     = [
                item.getQuantity().toString(),
                product.getBrand(),
                product.getName(),
                pnc.ui.DECIMAL_FORMATTER.format(item.getQuantity() * product.getPrice())
            ];
            data.push(row);
        }

        var body = pnc.ui.createTable(content, 'table-bordered table-striped', ['#', 'Marca', 'Producto', 'Sub-Total'], data);

        for(var cont = 0, size = body.children.length; cont < size; cont++) {
            body.children[cont].firstChild.style.textAlign = 'right';
            body.children[cont].lastChild.style.textAlign  = 'right';
        }
    };
    var self = this;
    var invoiceCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        invoice = response.content();
        self.productClient.getByInvoice(invoice, productCallback);
    };
    this.invoiceClient.get(invoice.getId(), invoiceCallback);
};
