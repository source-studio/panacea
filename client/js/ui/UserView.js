goog.provide('pnc.ui.UserView');

goog.require('goog.dom');
goog.require('goog.events');

goog.require('pnc.client.User');
goog.require('pnc.client.Group');
goog.require('pnc.ui');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.Paginator');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');

/**
 * View that allows an administrative user to administer other user accounts.
 * @constructor
 */
pnc.ui.UserView = function() {
    /**
     * Collection of available users
     * @type {Array.<pnc.model.User>}
     * @private
     */
    this.users       = [];

    /**
     * Client used to make user transactions
     * @type {pnc.client.User}
     * @private
     */
    this.client      = new pnc.client.User();

    /**
     * Client used to make group transactions
     * @type {pnc.client.Group}
     * @private
     */
    this.groupClient = new pnc.client.Group();

    /**
     * Client used to make subscription tranactions
     * @type {pnc.client.Subscription}
     * @private
     */
    this.subClient   = new pnc.client.Subscription();

    /**
     * Main container that will show the view
     * @type {Element}
     * @private
     */
    this.container   = null;

    /**
     * Array of items to be manually disposed when the view is being closed
     * @type {Array}
     * @private
     */
    this.disposables = [];

    /**
     * Principal paginato which will display users
     * @type {pnc.ui.Paginator}
     * @private
     */
    this.paginator   = null;
};
pnc.ui.Main.registerModule('users', pnc.ui.UserView);

/**
 * User readable versions of the pnc.model.Notification.Type enumeration
 * @enum {string}
 * @private
 */
pnc.ui.UserView.NotificationType = {
    /**
     * Notification raised when a product's inventory goes below the threshold
     * @type {String}
     */
    INVENTORY_THRESHOLD : 'Existencia Minima',

    /**
     * Notification raised when a product has passed its expiration date
     * @type {String}
     */
    EXPIRED_INVENTORY   : 'Inventario Expirado'
};

/**
 * Renders the view on screen
 * @param  {Element} parent  Parent element where the view will be rendered in
 */
pnc.ui.UserView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left : 0'});
    goog.dom.appendChild(parent, this.container);

    this.renderUserPane();
    this.renderActionPane();
    this.renderDetailPane();
    this.getUsers();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Usuarios');
};

/**
 * Disposes of the view
 */
pnc.ui.UserView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};

/**
 * Retrieves the list of users from the server. Populates the user pane with the available users once the server replies.
 * @private
 */
pnc.ui.UserView.prototype.getUsers = function() {
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.populateUserPane(response.content());
    };
    this.client.getAll(callback);
};

/**
 * Renders the pane where the users will be displayed
 * @private
 */
pnc.ui.UserView.prototype.renderUserPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Usuarios');
    content.id = 'user-pane';
    content.style.minHeight = '590px';

    var head = goog.dom.getElementByClass('box-head', content.parentElement);
    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var backButton, forwardButton, updateButton = null, removeButton = null, resetButton = null, notifyButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.USER_NOTIFICATION))
        notifyButton = pnc.ui.createToolbarButton(actionGroup, 'notify-button', pnc.ui.Resources.LIGHT_BULB_GRAY, 'Notificaciones', false, goog.bind(this.notifications, this));

    if(goog.array.contains(privs, pnc.client.Privileges.USER_PASSWORD_RESET))
        resetButton = pnc.ui.createToolbarButton(actionGroup, 'reset-button', pnc.ui.Resources.KEY, 'Cambiar Contraseña', false, goog.bind(this.reset, this));

    if(goog.array.contains(privs, pnc.client.Privileges.USER_UPDATE))
        updateButton = pnc.ui.createToolbarButton(actionGroup, 'update-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.update, this));

    if(goog.array.contains(privs, pnc.client.Privileges.USER_REMOVE))
        removeButton = pnc.ui.createToolbarButton(actionGroup, 'remove-button', pnc.ui.Resources.REMOVE, 'Remover', false, goog.bind(this.remove, this));

    backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button', pnc.ui.Resources.ARROW_LEFT, 'Atras', false);
    forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var self = this;
    var navCallback = function(e) {
        if(!goog.isNull(notifyButton))
            notifyButton.disabled = true;

        if(!goog.isNull(resetButton))
            resetButton.disabled = true;

        if(!goog.isNull(updateButton))
            updateButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
    };
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallback);
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);

    var titles = ['Nombre', 'Usuario', 'Habilitado'];
    this.paginator = new pnc.ui.Paginator(titles, [], null, 20, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderUser, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Renders the action pane that will provide the action to add a user.
 * @private
 */
pnc.ui.UserView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.USER_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));
    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Usuario');
    goog.dom.appendChild(addButton, span);
};

/**
 * Renders the detail pane. This will show the gorups that a user belongs to.
 * @private
 */
pnc.ui.UserView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '157px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 1, 'Grupos');
    body.id = 'detail-table';
};

/**
 * Paginator callback. Used to render a row representing a user inside the paginator.
 * @param  {Element} parent       Parent element where the row should be placed
 * @param  {pnc.model.User} user  User to represent
 * @private
 */
pnc.ui.UserView.prototype.renderUser = function(parent, user) {
    var row = goog.dom.createDom('tr', {'data-user' : user.getId()});
    goog.dom.appendChild(parent, row);
    var buttons = [
        goog.dom.getElement('update-button'),
        goog.dom.getElement('remove-button'),
        goog.dom.getElement('reset-button'),
        goog.dom.getElement('notify-button')
    ];

    var self = this;
    var callback = function(user, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;

        self.populateDetailPane(user, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, user));

    var cells = [
        goog.dom.createDom('td', null, user.toString()),
        goog.dom.createDom('td', null, user.getUsername()),
        goog.dom.createDom('td', {'style' : 'text-align: center'})
    ];

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);

    if(user.isEnabled()) {
        var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.RESULT});
        goog.dom.appendChild(cells[2], img);
    }
};

/**
 * Populates the user pane with the provided users
 * @param  {Array.<pnc.model.User>} users  List of usersto place in the user pane
 * @private
 */
pnc.ui.UserView.prototype.populateUserPane = function(users) {
    var content      = goog.dom.getElement('user-pane');
    var detailTable  = goog.dom.getElement('detail-table');
    var buttons = [
        goog.dom.getElement('notify-button'),
        goog.dom.getElement('update-button'),
        goog.dom.getElement('remove-button'),
        goog.dom.getElement('reset-button')
    ];

    goog.dom.removeChildren(detailTable);

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        if(goog.isDefAndNotNull(buttons[cont]))
            buttons[cont].disabled = true;

    this.paginator.setData(users);
    this.paginator.refresh();
};

/**
 * Populates the detail pane with the groups for the selected user
 * @param  {pnc.model.User} user  User who's groups will be retrieved and shown
 * @private
 */
pnc.ui.UserView.prototype.populateDetailPane = function(user) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);

    if(!goog.isDefAndNotNull(user))
        return;

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                return;

            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.Alert.error(pnc.client.decodeStatus(response));
                return;
        }

        var groups = response.content();
        var groupStr = '';

        for(var cont = 0, size = groups.length; cont < size; cont++)
            groupStr += groups[cont].getName() + ' ';

        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td', null, groupStr);
        goog.dom.appendChild(row, cell);
    };
    this.groupClient.getByUser(user, callback);
};

/**
 * Creates a form that will provide the ability to add a new user
 * @private
 */
pnc.ui.UserView.prototype.add = function() {
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createUserForm(content, null);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.CREATED:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('El nombre de usuario no se puede repetir');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var user = response.content();
        self.paginator.getData().push(user);
        goog.array.sort(self.paginator.getData(), pnc.model.User.compare);
        self.paginator.refresh();
        self.populateDetailPane(null);
        cancelCallback();
        pnc.ui.Alert.success('El usuario fue agregado');
    };

    var saveCallback = function() {
        if(!self.validateUserForm(inputs))
            return;

        var user = self.parseUserForm(inputs);
        self.client.add(user, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Creates a form that will provide the ability to update an existing user. The ability to change the password will not
 * be provided. The user will need to use the password reset option for that.
 * @private
 */
pnc.ui.UserView.prototype.update = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var user      = pnc.model.findModel(this.paginator.getData(), row.dataset.user);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createUserForm(content, user);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var self = this;
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('El nombre de usuario no se puede repetir');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var newUser = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newUser);
        self.paginator.refresh();
        self.populateDetailPane(newUser);

        var buttons = [
            goog.dom.getElement('reset-button'),
            goog.dom.getElement('update-button'),
            goog.dom.getElement('remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El usuario fue actualizado');
    };

    var saveCallback = function() {
        if(!self.validateUserForm(inputs))
            return;

        var newUser = self.parseUserForm(inputs, user);
        newUser.setId(user.getId());
        self.client.update(newUser, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog. If the user confirms, the transaction to remove a user will be initiated.
 * @private
 */
pnc.ui.UserView.prototype.remove = function() {
    var row    = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var user   = pnc.model.findModel(this.paginator.getData(), row.dataset.user);
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover a este usuario?');

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Este usuario no puede ser removido, ya que tiene actividad registrada. ' +
                    'Recuerde que puede desactivar el usuario editandolo').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), user);
        self.paginator.refresh();
        self.populateDetailPane(null);
        pnc.ui.Alert.success('El usuario fue removido');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.remove(user, responseCallback);
    };
    dialog.setCallback(selectCallback);
    dialog.open();
};

/**
 * Provides the user with a form used to reset the user's password. All other details will not be available for update.
 * @private
 */
pnc.ui.UserView.prototype.reset = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var user      = pnc.model.findModel(this.paginator.getData(), row.dataset.user);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createUserForm(content, user, true);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var self = this;
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var responseCallback = function(response) {
        user.setPassword(null);
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var buttons = [
            goog.dom.getElement('notify-button'),
            goog.dom.getElement('reset-button'),
            goog.dom.getElement('update-button'),
            goog.dom.getElement('remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('La contraseña fue actualizada');
    };

    var saveCallback = function() {
        if(!self.validateUserForm(inputs, true))
            return;

        var newUser = self.parseUserForm(inputs, user, true);
        self.client.updatePassword(newUser, newUser.getPassword(), responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Cambiar Contraseña', pnc.ui.Resources.KEY, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Cambiar Contraseña');
};

/**
 * Provides the user with a form to select the subscriptions the user is subscribed to receive notifications
 * @private
 */
pnc.ui.UserView.prototype.notifications = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var user      = pnc.model.findModel(this.paginator.getData(), row.dataset.user);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div', 'notify-form');
    var inputs  = this.createNotificationForm(content, user);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var self = this;
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var buttons = [
            goog.dom.getElement('notify-button'),
            goog.dom.getElement('reset-button'),
            goog.dom.getElement('update-button'),
            goog.dom.getElement('remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('Las notificaciones del usuario fueron actualizadas');
    };

    var saveCallback = function() {
        var ntypes = [];

        for(var key in inputs)
            if(inputs[key].checked)
                ntypes.push(key)

        self.subClient.sync(user, ntypes, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Notificaciones', pnc.ui.Resources.LIGHT_BULB_GRAY, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Notificaciones');
};

/**
 * Creates the form that will be used to create users, update users and reset user passwords.
 * @param  {Element}        parent  Parent element where the form will be placed
 * @param  {pnc.model.User} user    Optional user model. If provided its details will be used to populate the form.
 * @param  {boolean}        reset   Optional reset flag. If povided and true, the form will only be used to reset
 *                                      the user password.
 * @private
 */
pnc.ui.UserView.prototype.createUserForm = function(parent, user, reset) {
    reset = goog.isDefAndNotNull(reset) ? reset : false;
    var fields = {
        'user-first' : ['Nombre',   'No puede estar vacio'],
        'user-last'  : ['Apellido', 'No puede estar vacio'],
        'user-name'  : ['Usuario',  'No puede estar vacio']
    };

    var inputs = {};

    var makeField = function(key, type, name, message) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'class' : 'span2'}, name);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span2', 'type' : type});
        goog.dom.appendChild(group, inputs[key]);

        var help = goog.dom.createDom('span', 'help-inline', message);
        goog.dom.appendChild(group, help);
    };

    if(!reset) {
        for(var key in fields)
            makeField(key, 'text', fields[key][0], fields[key][1]);
        
        makeField('user-enabled', 'checkbox', 'Habilitado');
        inputs['user-enabled'].style.verticalAlign = 'super';
    }

    if(goog.isDefAndNotNull(user) && !reset) {
        inputs['user-first'].value     = user.getFirstName();
        inputs['user-last'].value      = user.getLastName();
        inputs['user-name'].value      = user.getUsername();
        inputs['user-enabled'].checked = user.isEnabled();
    } else {
        makeField('user-pass',    'password', 'Contraseña', 'No puede estar vacia');
        makeField('user-confirm', 'password', 'Confirmar',  'Debe ser igual a contraseña');

        if(!reset)
            inputs['user-enabled'].checked = true;
    }

    return inputs;
};

/**
 * Validates the form with user data. 
 * @param  {Object.<string, element>} inputs  Input elements to validate
 * @param  {boolean}                  reset   Optional reset flag. If provided only the password fields will be validated.
 * @private
 */
pnc.ui.UserView.prototype.validateUserForm = function(inputs, reset) {
    reset       = goog.isDefAndNotNull(reset) ? reset : false;
    var element = reset ? inputs['user-pass'] : inputs['user-first'];
    var errors  = goog.dom.getElementsByClass('error', element.parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(!reset) {
        if(inputs['user-first'].value.length < 1)
            goog.dom.classes.add(inputs['user-first'].parentElement, 'error');

        if(inputs['user-last'].value.length < 1)
            goog.dom.classes.add(inputs['user-last'].parentElement, 'error');

        if(inputs['user-name'].value.length < 1)
            goog.dom.classes.add(inputs['user-name'].parentElement, 'error');
    }

    if(goog.isDefAndNotNull(inputs['user-pass']) && inputs['user-pass'].value.length < 1)
        goog.dom.classes.add(inputs['user-pass'].parentElement, 'error');

    if(goog.isDefAndNotNull(inputs['user-confirm']) && inputs['user-confirm'].value !== inputs['user-pass'].value)
        goog.dom.classes.add(inputs['user-confirm'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', element.parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the user form. Will create a user model from the form elements.
 * @param  {object.<string, element>} inputs  Form input elements.
 * @param  {pnc.model.User}           user    Optional user model. If porivded its details will be updated from the form
 *                                                values. If not a new user will be created.
 * @param  {boolean}                  reset   Optional reset flag. If provided and true, only the password fields will
 *                                                be read from the form.
 * @private
 */
pnc.ui.UserView.prototype.parseUserForm = function(inputs, user, reset) {
    reset = goog.isDefAndNotNull(reset) ? reset : false;
    user  = goog.isDefAndNotNull(user) ? user : new pnc.model.User();

    if(!reset) {
        user.setFirstName(inputs['user-first'].value);
        user.setLastName(inputs['user-last'].value);
        user.setUsername(inputs['user-name'].value);
        user.setEnabled(inputs['user-enabled'].checked);
    }

    if(goog.isDefAndNotNull(inputs['user-pass']))
        user.setPassword(inputs['user-pass'].value);

    return user;
};

/**
 * Creates the form with subscription options for notifications
 * @param  {element}        parent     Parent element where the form will be placed
 * @param  {pnc.model.User} user       User for which the form will be created
 * @return {Object.<string, element>}  Form input elements
 * @private
 */
pnc.ui.UserView.prototype.createNotificationForm = function(parent, user) {
    var inputs = {};

    for(var key in pnc.ui.UserView.NotificationType) {
        var id = 'notification-type-' + key;
        inputs[key] = goog.dom.createDom('input', {'type' : 'checkbox', 'data-type' : key, 'id' : id, 'disabled' : 'true'});
        goog.dom.appendChild(parent, inputs[key]);
        var label = goog.dom.createDom('label', {'for' : id}, pnc.ui.UserView.NotificationType[key]);
        goog.dom.appendChild(parent, label);
    }

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.NOT_FOUND:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var subs = goog.isDefAndNotNull(response.content()) ? response.content() : [];

        for(var cont = 0, size = subs.length; cont < size; cont++) {
            var sub = subs[cont];
            inputs[sub.getNotificationType()].checked = true;
        }

        for(var key in inputs)
            inputs[key].disabled = false;
    };
    this.subClient.getByUser(user, callback);

    return inputs;
};
