goog.provide('pnc.ui.SlideDialog');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');
goog.require('goog.fx.Transition.EventType');
goog.require('goog.fx.dom.ResizeHeight');
goog.require('goog.math.Coordinate');
goog.require('goog.positioning');
goog.require('goog.ui.CustomButton');
goog.require('goog.ui.Dialog');
goog.require('goog.ui.PopupBase');


/**
 * An implementation of the goog.ui.Dialog. It places the dialog at the top of the page, and slides it down on entry,
 * slides it up out of view on exit. It's also color coded, based on the severity of the message.
 * @constructor
 * @param {!string} content             Text to place in the dialog
 * @param {!Array.<string>} buttons     The slide dialog provides generic "CANCEL", and "OK" buttons that can be added to the
 *                                      dialog by specifing them as arguments. Alternatively the client object can pass their
 *                                      own buttons in the content.
 * @param {!string} type                Defines the sevirity of the dialog. Should be a value from the pnc.ui.SlideDialog.Type
 *                                      enumeration.
 */
pnc.ui.SlideDialog = function(content, buttons, type) {
    /** @private */ this.type = goog.isDef(type) ? type : pnc.ui.SlideDialog.Types.INFO;
    /** @private */ this.dialog = new goog.ui.Dialog();
    
    this.dialog.setContent(content);
    this.dialog.setDisposeOnHide(true);
    this.dialog.setEscapeToCancel(true);
    this.dialog.setModal(true);
    this.dialog.setHasTitleCloseButton(false);
    this.dialog.setDraggable(false);
    var buttonSet = new goog.ui.Dialog.ButtonSet();

    if(goog.array.contains(buttons, pnc.ui.SlideDialog.Keys.CANCEL))
        buttonSet.addButton({'caption': 'Cancel', 'key': pnc.ui.SlideDialog.Keys.CANCEL});
    
    if(goog.array.contains(buttons, pnc.ui.SlideDialog.Keys.OK))
        buttonSet.addButton({'caption': 'OK', 'key': pnc.ui.SlideDialog.Keys.OK}, true);
    
    this.dialog.setButtonSet(buttonSet);
    this.callback = null;
    this.customContent = null;
};


/**
 * Enumerates constants used in the pnc.ui.SlideDialog class
 * @enum {string|number}
 */
pnc.ui.SlideDialog.Keys = {
    /**
     * Cancel button identifier
     * @type {String}
     */
    CANCEL  : 'CANCEL',

    /**
     * OK button identifier
     * @type {String}
     */
    OK      : 'OK',

    /**
     * Amount of time the dialog slide animation will last. Measured in miliseconds.
     * @type {Number}
     */
    DURATION: 200
};

/**
 * Enumeration of the basic predefined types of slide dialog that can be created.
 * @enum {String}
 */
pnc.ui.SlideDialog.Types = {
    INFO    : 'INFO',
    WARNING : 'WARNING',
    ERROR   : 'ERROR'
};

/**
 * Utility method. Creates a slide dialog with sevirty of info, and provies a default OK buton.
 * @param  {!Element} content       Content to display in the dialog
 * @return {pnc.ui.SlideDialog}    Generated dialog
 */
pnc.ui.SlideDialog.info = function(content) {
    var dialog = new pnc.ui.SlideDialog(content,
                                        [pnc.ui.SlideDialog.Keys.OK],
                                        pnc.ui.SlideDialog.Types.INFO);
    return dialog;
};

/**
 * Utility method. Creates a slide dialog with severity of warning, and provides the default CANCEL and OK buttons.
 * @param  {!Element} content       Content to display in the dialog
 * @return {pnc.ui.SlideDialog}    Generated dialog
 */
pnc.ui.SlideDialog.warning = function(content) {
    var dialog = new pnc.ui.SlideDialog(content,
                                         [pnc.ui.SlideDialog.Keys.CANCEL, pnc.ui.SlideDialog.Keys.OK],
                                         pnc.ui.SlideDialog.Types.WARNING);
    return dialog;
};

/**
 * Utility method. Creates a slide dialog with severity of error; provides the default OK button.
 * @param  {!Element} content       Content to display in the dialog
 * @return {pnc.ui.SlideDialog}    Generated dialog
 */
pnc.ui.SlideDialog.error = function(content) {
    var dialog = new pnc.ui.SlideDialog(content,
                                         [pnc.ui.SlideDialog.Keys.OK],
                                         pnc.ui.SlideDialog.Types.ERROR);
    return dialog;
};


/**
 * Renders the dialog and slides it into place
 */
pnc.ui.SlideDialog.prototype.open = function() {
    var existing = goog.dom.getElementByClass(pnc.ui.Styles.MODAL_DIALOG);
    
    try {
        this.dialog.render();
    } catch(e) {}
    var dialogDiv = goog.dom.getElementByClass(pnc.ui.Styles.MODAL_DIALOG);
    var titleBar  = goog.dom.getElementByClass(pnc.ui.Styles.MODAL_DIALOG_TITLE);
    var kids      = goog.dom.getChildren(dialogDiv);

    for(var cont = 0, size = kids.length; cont < size; cont++)
        goog.dom.classes.add(kids[cont], pnc.ui.Styles.INVISIBLE);

    switch(this.type) {
        case pnc.ui.SlideDialog.Types.WARNING:
            goog.dom.classes.add(dialogDiv, pnc.ui.Styles.DIALOG_WARNING);
            goog.dom.classes.add(titleBar, pnc.ui.Styles.DIALOG_WARNING);
            break;

        case pnc.ui.SlideDialog.Types.ERROR:
            goog.dom.classes.add(dialogDiv, pnc.ui.Styles.DIALOG_ERROR);
            goog.dom.classes.add(titleBar, pnc.ui.Styles.DIALOG_ERROR);
            break;
    }
    
    var buttonset = this.dialog.getButtonSet();
    var cancel    = buttonset.getButton(pnc.ui.SlideDialog.Keys.CANCEL);
    var ok        = buttonset.getButton(pnc.ui.SlideDialog.Keys.OK);

    if(goog.isDefAndNotNull(cancel))
        goog.dom.classes.add(cancel, 'btn');

    if(goog.isDefAndNotNull(ok))
        goog.dom.classes.add(ok, 'btn');

    if(goog.isDefAndNotNull(cancel) && goog.isDefAndNotNull(ok))
        goog.dom.classes.add(ok, 'btn-info');

    if(goog.isDefAndNotNull(this.customContent))
        goog.dom.appendChild(this.dialog.getContentElement(), this.customContent);

    this.dialog.setVisible(true);
    
    if(goog.isDefAndNotNull(this.callback)) {
        var self = this;
        var selectCallback = function(e) {
            if(e.key != pnc.ui.SlideDialog.Keys.OK)
                return;

            self.callback(e);
        };
        goog.events.listen(this.dialog, goog.ui.Dialog.EventType.SELECT, selectCallback);
    }

    var coord = new goog.math.Coordinate(dialogDiv.offsetLeft, 0);
    goog.positioning.positionAtCoordinate(coord, dialogDiv, goog.positioning.Corner.TOP_LEFT);

    var resize = new goog.fx.dom.ResizeHeight(dialogDiv, 0, dialogDiv.offsetHeight, pnc.ui.SlideDialog.Keys.DURATION);
    var self   = this;

    var endCallback = function(e) {
        for(var cont = 0, size = kids.length; cont < size; cont++)
            goog.dom.classes.remove(kids[cont], pnc.ui.Styles.INVISIBLE);

        self.dialog.focus();
    };
    goog.events.listen(resize, goog.fx.Transition.EventType.END, endCallback);

    var closeCallback = function(e) {
        e.preventDefault();

        var dialogDiv = goog.dom.getElementByClass(pnc.ui.Styles.MODAL_DIALOG);
        var kids = goog.dom.getChildren(dialogDiv);

        for(var cont = 0, size = kids.length; cont < size; cont++)
            goog.dom.classes.add(kids[cont], pnc.ui.Styles.INVISIBLE);

        var resize = new goog.fx.dom.ResizeHeight(dialogDiv, dialogDiv.offsetHeight, 0, pnc.ui.SlideDialog.Keys.DURATION);
        goog.events.listen(resize, goog.fx.Transition.EventType.END, goog.bind(self.dispose, self));
        goog.dom.classes.add(dialogDiv, pnc.ui.Styles.BORDERLESS);
        resize.play();
    };
    goog.events.listen(this.dialog, goog.ui.PopupBase.EventType.BEFORE_HIDE, closeCallback, true);
    goog.events.listen(this.dialog, goog.ui.PopupBase.EventType.HIDE, goog.bind(this.dispose, this));

    resize.play();
};

/**
 * Closes the dialog
 */
pnc.ui.SlideDialog.prototype.dispose = function() {
    if(goog.isDefAndNotNull(this.customContent))
        goog.dom.removeNode(this.customContent);

    this.dialog.disposeInternal();
};

/**
 * Establishes custom content to place in the dialog
 * @param  {Element} element  parent node of custom content
 */
pnc.ui.SlideDialog.prototype.setCustomContent = function(element) {
    this.customContent = element;
};

/**
 * Sets a callback to be invoked if the user clicks on the OK button of the dialog.
 * @param  {Function} callback  Function that will be invoked
 */
pnc.ui.SlideDialog.prototype.setCallback = function(callback) {
    this.callback = callback;
};

/**
 * Returns the underlying goog.ui.Dialog object. Useful to attach event handlers and listen to the user selection.
 * @return {goog.ui.Dialog}
 */
pnc.ui.SlideDialog.prototype.getElement = function() {
    return this.dialog;
};
