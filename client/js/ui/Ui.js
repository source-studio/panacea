goog.provide('pnc.ui');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.i18n.NumberFormat');
goog.require('goog.net.XhrIo');
goog.require('goog.string');
goog.require('goog.ui.Tooltip');

goog.require('pnc.client');
goog.require('pnc.ui.SlideDialog');

/**
 * User Interface namespace. Contains reusable constructs that create generic elements that are used in different contexts.
 * @namespace
 * @name pnc.ui
 */

/**
 * Collection of constant css class or style names to easily and safely reference in code
 * @enum {string}
 */
pnc.ui.Styles = {
    BORDERLESS         : 'borderless',
    INVISIBLE          : 'invisible',

    MODAL_DIALOG       : 'modal-dialog',
    MODAL_DIALOG_TITLE : 'modal-dialog-title',
    DIALOG_WARNING     : 'dialog-warning',
    DIALOG_ERROR       : 'dialog-error'
};

/**
 * Collection of resource URIs to use to embed images and other media.
 * @enum {string}
 */
pnc.ui.Resources = {
    ALERT_ERROR      : 'resources/alert-error.png',
    ALERT_INFO       : 'resources/alert-info.png',
    ALERT_OK         : 'resources/alert-ok.png',
    ALERT_WARNING    : 'resources/alert-warning.png',
    ARROW_LEFT       : 'resources/arrow-left.png',
    ARROW_RIGHT      : 'resources/arrow-right.png',
    ARROW_UP         : 'resources/arrow-up.png',
    BACK             : 'resources/back.png',
    BAR_CODE         : 'resources/bar-code.png',
    BARS             : 'resources/bars.png',
    BILL             : 'resources/bill.png',
    BOLT             : 'resources/bolt.png',
    BOOK             : 'resources/book.png',
    CHART            : 'resources/chart.png',
    CHART_INCREASING : 'resources/chart-increasing.png',
    COG_WHITE        : 'resources/cog-white.png',
    COG_GRAY         : 'resources/cog-dark-gray.png',
    COPY             : 'resources/copy.png',
    CREDIT_NOTE      : 'resources/credit-note.png',
    EDIT             : 'resources/edit.png',
    EQUALIZER        : 'resources/equalizer.png',
    FAILURE          : 'resources/failure.png',
    INFO             : 'resources/info.png',
    INVOICE          : 'resources/invoice.png',
    KEY              : 'resources/key.png',
    LAYERS           : 'resources/layers.png',
    LIGHT_BULB       : 'resources/light-bulb.png',
    LIGHT_BULB_GRAY  : 'resources/light-bulb-gray.png',
    LIST             : 'resources/list.png',
    LOADING          : 'resources/loading.gif',
    MONEY            : 'resources/money.png',
    ORDER            : 'resources/order.png',
    PAYMENT          : 'resources/payment.png',
    PAWN             : 'resources/pawn.png',
    PIN              : 'resources/pin.png',
    PLAY             : 'resources/play.png',
    PLAY_LIGHT       : 'resources/play-light.png',
    PLUS             : 'resources/plus.png',
    PLUS_GRAY        : 'resources/plus-dark-gray.png',
    REMOVE           : 'resources/remove.png',
    RESULT           : 'resources/result.png',
    RETURN           : 'resources/return.png',
    SEARCH           : 'resources/search.png',
    SHAKING_HANDS    : 'resources/shaking-hands.png',
    SHARE            : 'resources/share.png',
    SHOPPING_CART    : 'resources/shopping-cart.png',
    SUCCESS          : 'resources/success.png',
    USER_WHITE       : 'resources/user-white.png',
    USER_GRAY        : 'resources/user-dark-gray.png'
};

/**
 * Default decimal formatter for the UI namespace. Will use two decimal places.
 * @type {goog.i18n.NumberFormat}
 * @constant
 */
pnc.ui.DECIMAL_FORMATTER = new goog.i18n.NumberFormat(goog.i18n.NumberFormat.Format.DECIMAL);
pnc.ui.DECIMAL_FORMATTER.setMaximumFractionDigits(2);
pnc.ui.DECIMAL_FORMATTER.setMinimumFractionDigits(2);

/**
 * Default date formatter for the UI namespace.
 * @type {goog.i18n.DateTimeFormat}
 * @constant
 */
pnc.ui.DATE_FORMATTER = new goog.i18n.DateTimeFormat("dd'/'MM'/'yyyy");

/**
 * Default date parser for the UI namespace
 * @type {goog.i18n.DateTimeParse}
 * @constant
 */
pnc.ui.DATE_PARSER    = new goog.i18n.DateTimeParse("dd'/'MM'/'yyyy");

/**
 * Default time formatter for the UI namespace.
 * @type {goog.i18n.DateTimeFormat}
 * @constant
 */
pnc.ui.TIME_FORMATTER = new goog.i18n.DateTimeFormat("hh':'mm' 'a");

/**
 * Default time parser for the UI namespace
 * @type {goog.i18n.DateTimeParse}
 * @constant
 */
pnc.ui.TIME_PARSER    = new goog.i18n.DateTimeParse("hh':'mm' 'a");

/**
 * Default date/time formatter for the UI namespace
 * @type {goog.i18n.DateTimeFormat}
 * @constant
 */
pnc.ui.DATE_TIME_FORMATTER = new goog.i18n.DateTimeFormat("dd'/'MM'/'yyyy' 'hh':'mm' 'a");

/**
 * Default date/time parser for the UI namespace
 * @type {goog.i18n.DateTimeFormat}
 * @constant
 */
pnc.ui.DATE_TIME_PARSER    = new goog.i18n.DateTimeParse("dd'/'MM'/'yyyy' 'hh':'mm' 'a");

/**
 * Default page size for paginator controls in the UI namespace
 * @type {number}
 * @constant
 */
pnc.ui.PAGE_SIZE = 10;

/**
 * Standard regular expression used to test for floating point or money values.
 * @type {RegExp}
 * @constant
 */
pnc.ui.FLOAT_RGX = /^[\d,]+\.?\d*$/;


/**
 * Creates a structure to display a label and an element to dispaly data.
 * @param {Element}                 parent      Parent element where the structure will be rendered in.
 * @param {string}                  dom         Dom element that you want to generate
 * @param {string}                  label       Label text that you want to place in front of the element
 * @param {string}                  value       Value the element should hold
 * @param {Object.<string, string>} properties  Map of properties that the object should hold
 * @return {Element}                            Created element
 */
pnc.ui.createLabeledDom = function(parent, dom, label, value, properties) {
    var sticker = goog.dom.createDom('td', 'control-label', label);
    goog.dom.appendChild(parent, sticker);

    var inputCell = goog.dom.createDom('td', 'control-cell');
    goog.dom.appendChild(parent, inputCell);

    var element = goog.dom.createDom(dom, properties);
    goog.dom.appendChild(inputCell, element);

    if(goog.isDefAndNotNull(value))
        element.value = value;

    return element;
};

/**
 * Creates a box. The box is the basic UI container used across most if not all screens. It has a header element with
 * an icon and title. It has a content element which represents the body where content goes. When invoking this method
 * the content will be returned such to allow the invoker to start populating the body.
 * @param  {Element} parent  Parent element where the box will be placed
 * @param  {string}  style   Style or styles to be placed on the box
 * @param  {string}  img     Image resource URI
 * @param  {string}  title   Title text to place on the box
 * @return {Element}         Reference to the body of the box.
 */
pnc.ui.createBox = function(parent, style, img, title) {
    var box = goog.dom.createDom('div', ['box', style]);
    goog.dom.appendChild(parent, box);

    var head = goog.dom.createDom('div', 'box-head');
    goog.dom.appendChild(box, head);
    var icon = goog.dom.createDom('img', {'src' : img, 'alt' : title});
    goog.dom.appendChild(head, icon);
    var title = goog.dom.createDom('h3', {}, title);
    goog.dom.appendChild(head, title);

    var content = goog.dom.createDom('div', 'box-content');
    goog.dom.appendChild(box, content);

    return content;
};

/**
 * Creates a summary table. A summary table is a particular type of table used in the UI. It has a title which spans all
 * the columns, and rows in the body that provide content. It usually only has a few rows and is used to present summary
 * data.
 * @param  {Element}                parent   Parent element where the table will be placed.
 * @param  {string}                 style    Style or styles to be placed on the table
 * @param  {number}                 colspan  Number of columns the table will span
 * @param  {string}                 title    Title text to place in the table header
 * @param  {Array.<Array.<string>>} data     Array of array of strings. Holds the data that will be placed in the table body.
 * @return {Elemnt}                          Reference to the table body
 */
pnc.ui.createSummaryTable = function(parent, style, colspan, title, data) {
    style = goog.isDefAndNotNull(style) ? style : '';
    var table = goog.dom.createDom('table', ['table table-bordered', style]);
    goog.dom.appendChild(parent, table);
    var head = goog.dom.createDom('thead');
    goog.dom.appendChild(table, head);

    if(goog.isDefAndNotNull(title) && title.length > 0) {
        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(head, row);
        var th = goog.dom.createDom('th', {'colspan' : colspan.toString()}, title);
        goog.dom.appendChild(row, th);
    }
    
    var body = goog.dom.createDom('tbody');
    goog.dom.appendChild(table, body);

    if(!goog.isDefAndNotNull(data))
        return body;

    for(var y = 0, ys = data.length; y < ys; y++) {
        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        
        for(x = 0, xs = data[y].length; x < xs; x++)     {
            var cell = goog.dom.createDom('td', {}, data[y][x]);
            goog.dom.appendChild(row, cell);
        }
    }

    return body;
};

/**
 * Creates a summary row. Meant to be placed within a summary table.
 * @param  {Element} parent  Parent table to place the generated row
 * @param  {string}  title   Row title
 * @param  {string}  value   Value value
 * @return {Element}         Created row
 */
pnc.ui.createSummaryContent = function(parent, title, value) {
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);
    var cell = goog.dom.createDom('td', {'style' : 'width: 70px; word-wrap: break-word; vertical-align: middle'}, title);
    goog.dom.appendChild(row, cell);

    if(!goog.isDefAndNotNull(value))
        return row;

    if(goog.isNumber(value)) {
        value = pnc.ui.DECIMAL_FORMATTER.format(value);
    } else if(value.length > 30) {
        var tooltip = new goog.ui.AdvancedTooltip(cell, value);
        self.disposables.push(tooltip);
        value = goog.string.truncate(value, 30) + '...';
    }

    cell = goog.dom.createDom('td', {'style' : 'text-align: right; vertical-align: middle'}, value);
    goog.dom.appendChild(row, cell);

    return row;
};

/**
 * Creates a generic table
 * @param  {Element}                 parent   Parent element where the table will be placed in
 * @param  {string}                  style    One or more CSS classes to apply to the top level table element
 * @param  {Array.<string>}          headers  List of titles to place in the table header cells
 * @param  {Array.<Array.<string>>}  data     Optional data. A matrix of strings that will be populated into the table
 *                                                if provided.
 * @return {Element}                          Returns the table body element. Makes it easier for the invoke to directly
 *                                                append content into the table if required. Accessing the top level table 
 *                                                element is as simple as calling .parentElement on the return value.
 */
pnc.ui.createTable = function(parent, style, headers, data) {
    style = goog.isDefAndNotNull(style) ? style : '';
    var table = goog.dom.createDom('table', ['table table-bordered table-striped', style]);
    goog.dom.appendChild(parent, table);
    var head = goog.dom.createDom('thead');
    goog.dom.appendChild(table, head);
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(head, row);

    for(var cont = 0, size = headers.length; cont < size; cont++) {
        var cell = goog.dom.createDom('th', {}, headers[cont]);
        goog.dom.appendChild(row, cell);
    }

    var body = goog.dom.createDom('tbody');
    goog.dom.appendChild(table, body);

    if(!goog.isDefAndNotNull(data))
        return body;

    for(var y = 0, ys = data.length; y < ys; y++) {
        row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        
        for(x = 0, xs = data[y].length; x < xs; x++)     {
            var cell = goog.dom.createDom('td', {}, data[y][x]);
            goog.dom.appendChild(row, cell);
        }
    }

    return body;
};

/**
 * Event handler designed to avoid any data entry into text inputs that isn't a number or a period
 * @param  {Element} input  Input that will be monitored
 */
pnc.ui.assureNumbers = function(input) {
    var callback = function(e) {
        if(goog.events.KeyCodes.isCharacterKey(e.keyCode) &&
            (e.shiftKey || e.keyCode < goog.events.KeyCodes.ZERO || e.keyCode > goog.events.KeyCodes.NINE) &&
            (e.shiftKey || e.keyCode < goog.events.KeyCodes.NUM_ZERO || e.keyCode > goog.events.KeyCodes.NUM_NINE) &&
            (e.keyCode !== goog.events.KeyCodes.PERIOD && e.keyCode !== goog.events.KeyCodes.NUM_PERIOD))
                e.preventDefault();

        if(e.keyCode === goog.events.KeyCodes.PERIOD || e.keyCode === goog.events.KeyCodes.NUM_PERIOD)
            if(goog.string.contains(input.value, '.'))
                e.preventDefault();
    };
    goog.events.listen(input, goog.events.EventType.KEYDOWN, callback);
};

/** 
 * Updates the content of an auto complete control based on a call to the backend server. Content is refreshed after
 * every keystroke. This is only recommended for environments where the server is hosted, or on the same LAN. Using this
 * on a WAN or the internet will probably not produce a good user experience.
 * @param {element}                 input    Input element that will be monitored.
 * @param {goog.ui.ac.AutoComplete} ac       Auto complete object that is attached to the input
 * @param {pnc.model.Link}          link     Link holding server request meta data
 * @param {Object}                  map      Map with substitutions to be performed on the link URI. The values in the map
 *                                             can be strings or no-arg functions to be invoked to get the values.
 * @param {function(string)}        decoder  Function used to decode the server response into usable data
 * @param {Object.<string, *>}      values   Values represented by the auto complete search function. Keys are strings thet
 *                                            represent the object. Values are the actual object. When the user makes a 
 *                                            selection the object will be referenced.
 */
pnc.ui.updateAcData = function(input, ac, link, map, decoder, values) {
    var xhrCallback = function(e) {
        switch(e.target.getStatus()) {
            case pnc.client.Status.OK:
                var data       = decoder(e.target.getResponseText());
                var stringData = [];
                
                if(goog.isDefAndNotNull(values))
                    goog.object.clear(values);
                else
                    values = {};

                for(var cont = 0, size = data.length; cont < size; cont++) {
                    var sdata = data[cont].toString();
                    stringData.push(sdata);
                    values[sdata] = data[cont];
                }

                ac.getMatcher().setRows(stringData);
                break;

            case pnc.client.Status.NOT_FOUND:
                ac.getMatcher().setRows([]);
                break;

            default:
                var message = pnc.client.decodeStatus(e.target.getStatus());
                pnc.ui.SlideDialog.error(message).open();
                break;
        }
    };

    var inputCallback = function(e) {
        if(goog.string.trim(input.value).length < 1 || !goog.events.KeyCodes.isCharacterKey(e.keyCode))
            return;

        var uri = link.uri();

        if(goog.isDefAndNotNull(map)) {
            for(var key in map) {
                if(goog.isFunction(map[key]))
                    uri = uri.replace(key, goog.string.urlEncode(map[key]()));
                else
                    uri = uri.replace(key, goog.string.urlEncode(map[key]));
            }
        }

        goog.net.XhrIo.send(uri, xhrCallback, link.method());
    };
    goog.events.listen(input, goog.events.EventType.KEYUP, inputCallback);
};

/**
 * Creates a pill representation of search selection. Useful to confirm to the user that a selection has been made.
 * The user can remove the pill to restore the original search input.
 * @param  {Element}                 input  Input element that will be monitored
 * @param  {goog.ui.ac.AutoComplete} ac     Auto complete object that will be checked
 * @return {Element}                        Pill element
 */
pnc.ui.createPill = function(input, ac) {
    var container = goog.dom.createDom('div', {'class' : 'pill', 'style' : 'display : none;'});
    var width = document.defaultView.getComputedStyle(input, '').width.replace('px', '');
    width = (Number(width) - 9) + 'px';
    container.style.margin = document.defaultView.getComputedStyle(input, '').margin;
    container.style.width  = width;
    goog.dom.appendChild(input.parentElement, container);

    var content = goog.dom.createDom('div');
    goog.dom.appendChild(container, content);

    var close = goog.dom.createDom('span', {}, 'X');
    goog.dom.appendChild(container, close);

    var closeCallback = function() {
        input.value             = '';
        input.style.display     = 'inline-block';
        container.style.display = 'none';
    };
    goog.events.listen(close, goog.events.EventType.CLICK, closeCallback);

    var updateCallback = function() {
        goog.dom.setTextContent(content, input.value);
        input.style.display     = 'none';
        container.style.display = 'inline-block';
    };
    goog.events.listen(ac, goog.ui.ac.AutoComplete.EventType.UPDATE, updateCallback);

    return container;
};

/**
 * Attaches a callback to the close button of a pill
 * @param  {Element}   pill     Pill element created by the pnc.ui.createPill function
 * @param  {Function} callback  Callback to invoke when the close button is pressed
 */
pnc.ui.onPillClose = function(pill, callback) {
    var close = goog.dom.getElementsByTagNameAndClass('span', null, pill)[0];
    goog.events.listen(close, goog.events.EventType.CLICK, callback);
};

/**
 * Simulates a click event on the pill close button
 * @param  {Element} pill  Pill element created via the createPill function
 */
pnc.ui.closePill = function(pill) {
    if(!goog.isDefAndNotNull(pill))
        return;

    var close = goog.dom.getElementsByTagNameAndClass('span', null, pill)[0];
    close.click();
};

/**
 * Utility for creating toolbar button with the same common style
 * @param  {Element}  parent    Parent element where the button should be placed
 * @param  {string}   id        ID to give the button
 * @param  {string}   src       Source for the icon to place inside the button
 * @param  {string}   title     Contents that the user should see when hovering over the button
 * @param  {boolean}  enabled   Should the button be initially enabled?
 * @param  {Function} callback  Callback to assign to the button's click event
 * @return {Element}            Generated button
 */
pnc.ui.createToolbarButton = function(parent, id, src, title, enabled, callback) {
    var button = goog.dom.createDom('button', {'id' : id, 'class' : 'btn'});
    goog.dom.appendChild(parent, button);
    button.disabled = !enabled;
    var img = goog.dom.createDom('img', {'src' : src});
    goog.dom.appendChild(button, img);

    var tooltip = new goog.ui.Tooltip(button, title);
    tooltip.setShowDelayMs(50);
    var container = goog.dom.getElement('tooltips');
    goog.dom.appendChild(container, tooltip.getElement());

    var tooltipCallback = function() { 
        button.click(); 
        tooltip.setVisible(false);
    };
    goog.events.listen(tooltip.getElement(), goog.events.EventType.CLICK, tooltipCallback);

    if(goog.isDefAndNotNull(callback))
        goog.events.listen(button, goog.events.EventType.CLICK, callback);

    return button;
};

/**
 * Utility function used to round to two decimal places.
 * @param  {number} value  Value to round
 * @return {number}        Rounded value
 */
pnc.ui.round = function(value) {
    value = goog.isString(value) ? pnc.ui.DECIMAL_FORMATTER.parse(value) : value;

    var temp = Math.round(value * 100);
    return temp / 100;
};
