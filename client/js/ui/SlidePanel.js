goog.provide('pnc.ui.SlidePanel');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.fx.dom.FadeInAndShow');
goog.require('goog.fx.dom.FadeOutAndHide');
goog.require('goog.fx.dom.Slide');
goog.require('goog.fx.Animation');
goog.require('goog.fx.AnimationParallelQueue');

/**
 * Generic panel that overlays the main content in order to focus the user on an immediate task, like
 * creating or editing content.
 * @constructor
 */
pnc.ui.SlidePanel = function(title, icon, content) {
    this.container = null;
    this.title     = goog.isDefAndNotNull(title) ? title : '';
    this.icon      = goog.isDefAndNotNull(icon) ? icon : '';
    this.header    = null;
    this.content   = goog.isDef(content) ? content : null;
};


/**
 * Renders the slide panel
 * @return {Element}  The body of the panel
 */
pnc.ui.SlidePanel.prototype.render = function() {
    var parent     = goog.dom.getElement('content');
    var wrapper    = goog.dom.createDom('div', {'style' : 'position: absolute; left: -20px; top: 10px; width: 800px; z-index: 2'});
    goog.dom.appendChild(parent, wrapper);
    this.container = goog.dom.createDom('div', 'slide-panel span7');
    this.container.style.minHeight = parent.offsetHeight + 'px'
    goog.dom.appendChild(wrapper, this.container);

    var glassPane = goog.dom.createDom('div', 'slide-panel-glass');
    goog.dom.appendChild(wrapper, glassPane);

    var existing = goog.dom.getElementsByClass('slide-panel', parent);

    if(goog.isDefAndNotNull(existing) && existing.length > 1)
        glassPane.style.visibility = 'hidden';

    this.header = goog.dom.createDom('div', 'box-head');
    goog.dom.appendChild(this.container, this.header);

    if(this.icon.length > 0) {
        var img = goog.dom.createDom('img', {'src' : this.icon, 'alt' : this.title});
        goog.dom.appendChild(this.header, img);
    }

    if(this.title.length > 0) {
        var title = goog.dom.createDom('h3', {}, this.title);
        goog.dom.appendChild(this.header, title);
    }

    var body = goog.dom.createDom('div', 'box-content');
    goog.dom.appendChild(this.container, body);

    if(goog.isDefAndNotNull(this.content))
        goog.dom.appendChild(body, this.content);

    var queue = new goog.fx.AnimationParallelQueue();
    var fade  = new goog.fx.dom.FadeInAndShow(this.container, 300);
    queue.add(fade);
    var x = parent.offsetLeft; var y = parent.offsetTop;
    var slide = new goog.fx.dom.Slide(this.container, [x - 320, 10], [-20, 10], 300);
    queue.add(slide);

    if(document.body.scrollTop > 0) {
        var scroll = new goog.gx.dom.Scroll(docment.body, [0, document.body.scrollTop], [0, 0], 300);
        queue.add(scroll);
    }

    queue.play();

    return body;
};

/**
 * Disposes the resources being used by the slide panel and removes it from the dom
 */
pnc.ui.SlidePanel.prototype.dispose = function() {
    var fade     = new goog.fx.dom.FadeOutAndHide(this.container.parentElement, 300);
    var callback = goog.partial(goog.dom.removeNode, this.container.parentElement);
    goog.events.listen(fade, goog.fx.Animation.EventType.END, callback);
    fade.play();
};

/**
 * Returns the slider element
 * @return {Element}  slider element
 */
pnc.ui.SlidePanel.prototype.getElement = function() {
    return this.container;
};

/**
 * Retrieves the element that contains the slider header
 * @return {Element}
 */
pnc.ui.SlidePanel.prototype.getHeaderElement = function() {
    return this.header;
};

/**
 * Returns the content element if provided.
 * @return {Elemenet}  content element
 */
pnc.ui.SlidePanel.prototype.getContentElement = function() {
    return this.content;
};
