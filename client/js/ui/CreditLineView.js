goog.provide('pnc.ui.CreditLineView');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.fx.Animation');
goog.require('goog.fx.dom.FadeOutAndHide');
goog.require('goog.ui.Dialog');

goog.require('pnc.client.Balance');
goog.require('pnc.client.CreditLine');
goog.require('pnc.client.Product');
goog.require('pnc.client.Profile');
goog.require('pnc.model.CreditLine');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Paginator');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');

/**
 * Control that lets the user see a credit line, inspect the invoices and payments that have been made, add new payments,
 * and if the payments have been made after the last balance, they can also be removed.
 * @param  {Element} paginator  Paginator element that holds the person with the credit line. Could be a customer, supplier
 *                                or any other type that can be associated with a credit line.
 * @constructor
 */
pnc.ui.CreditLineView = function(paginator) {
    /**
     * Paginator element that holds the person with the credit line. Could be a customer, supplier or any other type 
     * that can be associated with a credit line.
     * @type {pnc.ui.Paginator}
     * @private
     */
    this.paginator       = paginator;

    /**
     * Paginator element that holds payment data
     * @type {pnc.ui.Paginator}
     * @private
     */
    this.paymentPaginator = null;

    /**
     * Credit line that is being viewed. It will be brought in by the initial detail method sent by the invoker, but will
     * also be refreshed when transactions occur to the server.
     * @type {pnc.model.CreditLine}
     * @private
     */
    this.creditLine      = null;

    /**
     * Client used to make credit line transactions to the server
     * @type {pnc.client.CreditLine}
     * @private
     */
    this.client          = new pnc.client.CreditLine();

    /**
     * Client used to make balance transactions to the server
     * @type {pnc.client.CreditLine}
     * @private
     */
    this.balanceClient   = new pnc.client.Balance();

    /**
     * Client used to make product transactions to the server
     * @type {pnc.client.Product}
     * @private
     */
    this.productClient   = new pnc.client.Product();

    /**
     * Client used to make invoice transactions to the server
     * @type {pnc.client.Invoice}
     * @private
     */
    this.invoiceClient   = new pnc.client.Invoice();

    /**
     * Callback that can be set by the invoker. It is called when the credit line is refreshed from the server. This allows
     * the invoker to refresh its content as well to reflect the latest data.
     * @type {function(pnc.model.CreditLine)}
     * @private
     */
    this.refreshCallback = null;

    /**
     * Reference to the main slider of the view. Slider that presents credit line data
     * @type {pnc.ui.SlidePane}
     * @private
     */
    this.creditLineSlider = null;
};


/**
 * Callback that can be set by the invoker. It is called when the credit line is refreshed from the server. This allows
 * the invoker to refresh its content as well to reflect the latest data.
 * @param  {function(pnc.model.CreditLine)} refreshCallback  Callback
 */
pnc.ui.CreditLineView.prototype.setRefreshCallback = function(refreshCallback) {
    this.refreshCallback = refreshCallback;
};

/**
 * Creates a control that will let the user add a new credit line to an entity. An entity can only have one credit line.
 */
pnc.ui.CreditLineView.prototype.addCreditLine = function() {
    var buttons = [
        goog.dom.getElement('add-button'),
        goog.dom.getElement('credit-line-add-button'),
        goog.dom.getElement('credit-line-detail-button')
    ];

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        if(goog.isDefAndNotNull(buttons[cont]))
            buttons[cont].disabled = true;

    var row         = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var customer    = pnc.model.findModel(this.paginator.getData(), row.dataset.customer);
    var content     = goog.dom.createDom('div');
    var inputs      = this.createCreditLineForm(content);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        slider.dispose(); 

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        self.creditLine = response.content();
        self.refreshCallback(self.creditLine);
        cancelCallback();
        pnc.ui.Alert.success('La linea de credito fue creada');
    };

    var saveCallback = function() {
        if(!self.validateCreditLineForm(inputs))
            return;

        self.parseCreditLineForm(inputs);
        self.client.addToCustomer(self.creditLine, customer, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Crear Linea de Credito para ' + customer.toString(), pnc.ui.Resources.EDIT, content);
    slider.render();
};

/**
 * Creates a form that will be used to update the details of a credit line
 * @private
 */
pnc.ui.CreditLineView.prototype.update = function() {
    var content     = goog.dom.createDom('div');
    var inputs      = this.createCreditLineForm(content);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { slider.dispose(); };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        self.creditLine = response.content();
        var limit = self.creditLine.getLimit() > -1 ? pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getLimit()) : 'Ilimitado';
        goog.dom.setTextContent(goog.dom.getElement('credit-line-active'), self.creditLine.isActive() ? 'Si' : 'No');
        goog.dom.setTextContent(goog.dom.getElement('credit-line-limit'),  limit);
        cancelCallback();
        pnc.ui.Alert.success('La linea de credito fue actualizada');
    };

    var saveCallback = function() {
        if(!self.validateCreditLineForm(inputs))
            return;

        self.parseCreditLineForm(inputs);
        self.client.update(self.creditLine, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Actualizar Linea de Credito', pnc.ui.Resources.EDIT, content);
    slider.render();
};

/**
 * Prompts the user to confirm the removal transaction. If the user confirms, will process the server transaction to remove
 * the credit line.
 * @private
 */
pnc.ui.CreditLineView.prototype.remove = function() {
    var message = 'Esta seguro que desea remover esta linea de credito? Esto eliminara todo el historial de pagos y ' +
        'vinculos a facturas. Si desea suspenderle credito al cliente es preferible desactivar la cuenta.';
    var dialog = new pnc.ui.SlideDialog.warning(message);
    var self   = this;

    var removeCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            self.creditLine = null;
                pnc.ui.Alert.success('La linea de credito fue removida');
                break;

            case pnc.client.Status.NOT_FOUND:
                pnc.ui.Alert.error('No se encontraron los datos de la linea de credito para remover');
                break;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.creditLineSlider.dispose();
        self.refreshCallback(null);
        pnc.ui.Main.breadcrumbPop();
    };

    var selectCallback = function() { self.client.remove(self.creditLine, removeCallback); };
    dialog.setCallback(selectCallback);
    dialog.open();
};

/**
 * Creates the form that will be used to enter credit line info for add and update transactions
 * @param  {Element}          parent  Parent element where form will be placed
 * @return {Object.<string, Elemet>}  Input elements in form
 * @private
 */
pnc.ui.CreditLineView.prototype.createCreditLineForm = function(parent) {
    var inputs  = {};

    if(goog.isDefAndNotNull(this.creditLine)) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);
        var label = goog.dom.createDom('label', 'span1', 'Balance');
        goog.dom.appendChild(group, label);
        var value = goog.dom.createDom('span', {'class' : 'span2 bold', 'style' : 'margin-left: 0; vertical-align: text-bottom'}, 
                    pnc.ui.DECIMAL_FORMATTER.format(this.creditLine.getBalance()));
        goog.dom.appendChild(group, value);
    }

    var group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    var label = goog.dom.createDom('label', {'for' : 'cr-limit-input', 'class' : 'span1'}, 'Limite');
    goog.dom.appendChild(group, label);
    var limitInput = goog.dom.createDom('input', {'id' : 'cr-limit-input', 'class' : 'span2', 'type' : 'text'});
    goog.dom.appendChild(group, limitInput);
    pnc.ui.assureNumbers(limitInput);
    inputs['cr-limit'] = limitInput;
    var limitCb = goog.dom.createDom('input', {'id' : 'cr-limit-cb', 'type' : 'checkbox', 
        'style' : 'width: 16px; height: 16px; margin: -10px 4px 0 10px'});
    goog.dom.appendChild(group, limitCb);
    inputs['cr-unlimited'] = limitCb;
    var span = goog.dom.createDom('span', {'style' : 'cursor: pointer; vertical-align: text-bottom'}, 'Ilimitado');
    goog.dom.appendChild(group, span);
    var help = goog.dom.createDom('span', 'help-inline', 'Debe ser un numero o ilimitado');
    goog.dom.appendChild(group, help);

    var spanCallback = function() { limitCb.click(); };
    goog.events.listen(span, goog.events.EventType.CLICK, spanCallback);
    var cbCallback = function() { 
        limitInput.disabled = limitCb.checked; 
        limitInput.value = limitCb.checked ? '' : limitInput.value;
    };
    goog.events.listen(limitCb, goog.events.EventType.CLICK, cbCallback);

    group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    goog.dom.appendChild(parent, group);
    label = goog.dom.createDom('label', {'for' : 'cr-limit-active', 'class' : 'span1'}, 'Activo');
    goog.dom.appendChild(group, label);
    var activeCb = goog.dom.createDom('input', {'id' : 'cr-active-cb', 'type' : 'checkbox', 'checked' : true,
        'style' : 'width: 16px; height: 16px; margin: -10px 4px 0 0'});
    goog.dom.appendChild(group, activeCb);
    inputs['cr-active'] = activeCb;

    if(goog.isDefAndNotNull(this.creditLine)) {
        limitInput.value    = this.creditLine.getLimit() > 0 ? pnc.ui.DECIMAL_FORMATTER.format(this.creditLine.getLimit()) : '';
        limitCb.checked     = this.creditLine.getLimit() === -1;
        limitInput.disabled = limitCb.checked;
        activeCb.checked    = this.creditLine.isActive();
    }

    return inputs;
};

/**
 * Validates input elements in the credit line form. If an input is not satisfactory an error is raised inline within
 * te form.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @return {boolean}                          Was the form data OK?
 * @private
 */
pnc.ui.CreditLineView.prototype.validateCreditLineForm = function(inputs) {
    var errors = goog.dom.getElementsByClass('error', inputs['cr-limit'].parentElement.parentElement);
    
    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(!inputs['cr-unlimited'].checked && inputs['cr-limit'].value.length < 1) {
        goog.dom.classes.add(inputs['cr-limit'].parentElement, 'error');
        return false;
    }

    return true;
};

/**
 * Parses the content of the credit line form and creates a credit line model out of it. The data that is read is placed
 * on the credit line instance property of this object. If none existed, a new one is created.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @private
 */
pnc.ui.CreditLineView.prototype.parseCreditLineForm = function(inputs) {
    this.creditLine = goog.isDefAndNotNull(this.creditLine) ? this.creditLine : new pnc.model.CreditLine();
    var limit = 0;

    if(!inputs['cr-unlimited'].checked && inputs['cr-limit'].value.length > 0)
        limit = pnc.ui.DECIMAL_FORMATTER.parse(inputs['cr-limit'].value);
    else if(inputs['cr-unlimited'].checked)
        limit = -1;

    this.creditLine.setLimit(limit);
    this.creditLine.setActive(inputs['cr-active'].checked);
};

/**
 * Displays a control to the user that shows details for the credit line of a selected entity.
 * @param  {pnc.model.CreditLine} creditLine  Credit Line object to be displayed.
 */
pnc.ui.CreditLineView.prototype.creditLineDetail = function(creditLine) {
    this.creditLine = creditLine;
    var buttons = [
        goog.dom.getElement('add-button'),
        goog.dom.getElement('credit-line-add-button'),
        goog.dom.getElement('credit-line-detail-button')
    ];

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        if(goog.isDefAndNotNull(buttons[cont]))
            buttons[cont].disabled = true;

    this.createDetailPane();
    this.createInvoicePane();
    this.createPaymentPane();
};

/**
 * Creates the top detail pane for the credit line. This displays the top level atomic properties.
 * @private
 */
pnc.ui.CreditLineView.prototype.createDetailPane = function() {
    var content  = goog.dom.createDom('div', {'id' : 'credit-line-detail-content', 'style' : 'min-height : 60px; padding-left: 10px'});
    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var customer = pnc.model.findModel(this.paginator.getData(), row.dataset.customer);
    this.creditLineSlider = new pnc.ui.SlidePanel('Linea de Credito de ' + customer.toString(), pnc.ui.Resources.PIN, content);
    this.creditLineSlider.render();
    pnc.ui.Main.breadcrumbPush('Linea de Credito');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(this.creditLineSlider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var self = this;

    var callback = function() { 
        var buttons = [
            goog.dom.getElement('add-button'),
            goog.dom.getElement('credit-line-add-button'),
            goog.dom.getElement('credit-line-detail-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;

        self.creditLineSlider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'credit-line-back-button', pnc.ui.Resources.BACK, 'Regresar al Cliente', true, callback);

    var privs = pnc.client.Profile.getPrivileges();

    if(goog.array.contains(privs, pnc.client.Privileges.CREDIT_LINE_PAYMENT)) {
        var callback = function() { self.makePayment(); };
        var button   = pnc.ui.createToolbarButton(actionGroup, 'credit-line-payment-button', pnc.ui.Resources.BILL, 'Hacer Pago', true, callback);
        var img      = goog.dom.getElementsByTagNameAndClass('img', null, button)[0];
        img.style.width  = '28px';
        img.style.margin = '0 6px';
    }

    if(goog.array.contains(privs, pnc.client.Privileges.CREDIT_LINE_UPDATE)) {
        var callback = function() { self.update(); };
        pnc.ui.createToolbarButton(actionGroup, 'credit-line-update-button', pnc.ui.Resources.EDIT, 'Editar Linea de Credito', true, callback);
    }

    if(goog.array.contains(privs, pnc.client.Privileges.CREDIT_LINE_REMOVE)) {
        var callback = function() { self.remove(); };
        pnc.ui.createToolbarButton(actionGroup, 'credit-line-remove-button', pnc.ui.Resources.REMOVE, 'Remover Linea de Credito', true, callback);
    }

    var data = {
        'Cliente' : customer.toString(),
        'Activo'  : self.creditLine.isActive() ? 'Si' : 'No',
        'Limite'  : self.creditLine.getLimit() > -1 ? pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getLimit()) : 'Ilimitado',
        'Balance' : pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getBalance())
    };

    var even = false;
    var row  = null;    

    for(var key in data) {
        even = !even;
        if(even) {
            row = goog.dom.createDom('div', 'row');
            goog.dom.appendChild(content, row);         
        }

        var label = goog.dom.createDom('label', 'span1', key);
        goog.dom.appendChild(row, label);

        var value = goog.dom.createDom('label', 'span2 bold', data[key]);
        goog.dom.appendChild(row, value);
    }

    content.children[0].children[3].id = 'credit-line-active';
    content.children[1].children[1].id = 'credit-line-limit';
    content.children[1].children[3].id = 'credit-line-detail-balance';
};

/**
 * Creates the invoice pane. This shows a history of all invoices tied to this credit line in reverse chronological order.
 * @private
 */
pnc.ui.CreditLineView.prototype.createInvoicePane = function() {
    var container = goog.dom.getElement('credit-line-detail-content').parentElement.parentElement;
    var content   = pnc.ui.createBox(container, 'credit-line-invoices', pnc.ui.Resources.INVOICE, 'Facturas');
    var head      = goog.dom.getElementsByClass('box-head', content.parentElement)[0];

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    var paginator     = new pnc.ui.Paginator(['#', 'Fecha', 'Total'], this.creditLine.getInvoices(), 
                        'table-condensed table-bordered summary-table table-striped', 15, backButton, forwardButton);
    paginator.setRowRenderer(goog.bind(this.renderInvoice, this));
    paginator.render(content);
};

/**
 * Callback for the paginator control to render an invoice.
 * @param  {Element}           parent   Parent element where the invoice row will be placed.
 * @param  {pnc.model.Invoice} invoice  Invoice to represent
 * @private
 */
pnc.ui.CreditLineView.prototype.renderInvoice = function(parent, invoice) {
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);

    var data = [
        '',
        pnc.ui.DATE_FORMATTER.format(invoice.getTimestamp()),
        pnc.ui.DECIMAL_FORMATTER.format(invoice.getTotal())
    ];

    for(var cont = 0, size = data.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', {'style' : 'text-align: right'}, data[cont]);
        goog.dom.appendChild(row, cell);
    }

    var link = goog.dom.createDom('a', null, invoice.getId().toString())
    goog.dom.appendChild(row.children[0], link);
    goog.events.listen(link, goog.events.EventType.CLICK, goog.bind(this.invoiceDetail, this, invoice));
};

/**
 * Creates a slider that will show full invoice detail.
 * @param  {pnc.model.Invoice} invoice  Invoice to represent
 * @private
 */
pnc.ui.CreditLineView.prototype.invoiceDetail = function(invoice) {
    var content  = goog.dom.createDom('div', {'id' : 'invoice-detail-content'});
    var slider   = new pnc.ui.SlidePanel('Factura #' + invoice.getId().toString(), pnc.ui.Resources.INVOICE, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Factura');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var self = this;

    var callback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'credit-line-back-button', pnc.ui.Resources.BACK, 'Regresar a la Linea de Credito', true, callback);

    var data = {
        'Fecha' : pnc.ui.DATE_FORMATTER.format(invoice.getTimestamp()),
        'Hora'  : pnc.ui.TIME_FORMATTER.format(invoice.getTimestamp()),
        'Pago'  : pnc.ui.InvoiceView.Payment[invoice.getPayment()],
        'Total' : pnc.ui.DECIMAL_FORMATTER.format(invoice.getTotal())
    };

    for(var key in data) {
        var label = goog.dom.createDom('label', {'style' : 'width: 50px'}, key);
        goog.dom.appendChild(content, label);

        var value = goog.dom.createDom('label', 'span2 bold', data[key]);
        goog.dom.appendChild(content, value);
    }

    var itemHeader = goog.dom.createDom('h4', {'class' : 'form-header', 'style' : 'margin: 10px 5px'}, 'Itemes');
    goog.dom.appendChild(content, itemHeader);

    var productCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var products = response.content();
        var data     = [];

        for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
            var item    = invoice.getItems()[cont];
            var product = pnc.model.findModel(products, item.getIdProduct());
            var row     = [
                item.getQuantity().toString(),
                product.getBrand(),
                product.getName(),
                pnc.ui.DECIMAL_FORMATTER.format(item.getQuantity() * product.getPrice())
            ];
            data.push(row);
        }

        var body = pnc.ui.createTable(content, 'table-bordered table-striped', ['#', 'Marca', 'Producto', 'Sub-Total'], data);

        for(var cont = 0, size = body.children.length; cont < size; cont++) {
            body.children[cont].firstChild.style.textAlign = 'right';
            body.children[cont].lastChild.style.textAlign  = 'right';
        }
    };
    var self = this;
    var invoiceCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        invoice = response.content();
        self.productClient.getByInvoice(invoice, productCallback);
    };
    this.invoiceClient.get(invoice.getId(), invoiceCallback);
};

/**
 * Creates the payment pane. This shows a history of all payments made to this credit line in reverse chronological order.
 * @private
 */
pnc.ui.CreditLineView.prototype.createPaymentPane = function() {
    var container = goog.dom.getElement('credit-line-detail-content').parentElement.parentElement;
    var content   = pnc.ui.createBox(container, 'credit-line-payments', pnc.ui.Resources.PAYMENT, 'Pagos');
    var head      = goog.dom.getElementsByClass('box-head', content.parentElement)[0];

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    if(goog.isDefAndNotNull(this.paymentPaginator))
        this.paymentPaginator.dispose();

    var backButton        = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton     = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    this.paymentPaginator = new pnc.ui.Paginator(['Fecha', 'Total', ''], this.creditLine.getPayments(), 
                            'table-condensed table-bordered summary-table table-striped', 15, backButton, forwardButton);
    var self = this;
    var callback = function(response) {
        var lastBalance = null;

        switch(response.status()) {
            case pnc.client.Status.OK:
                lastBalance = response.content();
                break;

            case pnc.client.Status.NOT_FOUND:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var privs   = pnc.client.Profile.getPrivileges();
        var hasPriv = goog.array.contains(privs, pnc.client.Privileges.CREDIT_LINE_PAYMENT);
        self.paymentPaginator.setRowRenderer(goog.bind(self.renderPayment, self, lastBalance, hasPriv));
        self.paymentPaginator.render(content);
        self.paymentPaginator.getElement().id = 'payment-paginator';
    };
    this.balanceClient.getLast(callback);
};

/**
 * Paginator callback used to render a payment representation
 * @param  {pnc.model.Balance}  lastBalance  Last balance created, if such exists
 * @param  {Boolean}            hasPriv      Does the current user have the privilege to remove payments?
 * @param  {Element}            parent       Parent element where the payment representation will be placed
 * @param  {pnc.model.Payment}  payment      Payment object to represent
 * @private
 */
pnc.ui.CreditLineView.prototype.renderPayment = function(lastBalance, hasPriv, parent, payment) {
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);
    row.dataset.payment = payment.getId().toString();

    var cell = goog.dom.createDom('td', {'style' : 'text-align: right; width: initial'}, pnc.ui.DATE_FORMATTER.format(payment.getTimestamp()));
    goog.dom.appendChild(row, cell);

    cell = goog.dom.createDom('td', {'style' : 'text-align: right; width: initial'}, pnc.ui.DECIMAL_FORMATTER.format(payment.getAmount()));
    goog.dom.appendChild(row, cell);
    
    cell = goog.dom.createDom('td', {'style' : 'width: 28px; padding-left: 5px'});
    goog.dom.appendChild(row, cell);

    if(hasPriv && goog.isDefAndNotNull(lastBalance) && lastBalance.getTimestamp().getTime() < payment.getTimestamp().getTime()) {
        var removeButton = goog.dom.createDom('button', {'class' : 'btn', 'style' : 'padding: 0 6px'});
        goog.dom.appendChild(cell, removeButton);
        var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.REMOVE, 'title' : 'Remover Pago', 
                    'style' : 'width : 14px; height: 14px'});
        goog.dom.appendChild(removeButton, img);
        goog.events.listen(removeButton, goog.events.EventType.CLICK, goog.bind(this.removePayment, this));
    }
};

/**
 * Creates a form that allows the user to register a payment.
 * @private
 */
pnc.ui.CreditLineView.prototype.makePayment = function() {
    var content     = goog.dom.createDom('div');
    var inputs      = this.createPaymentForm(content);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var creditLineCallback = function(response) {
        self.creditLine = response.content();

        if(goog.isDefAndNotNull(self.refreshCallback))
            self.refreshCallback(self.creditLine);

        var balance = goog.dom.getElement('credit-line-detail-balance');
        var value   = pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getBalance());
        goog.dom.setTextContent(balance, value);
        self.paymentPaginator.setData(self.creditLine.getPayments());
        self.paymentPaginator.refresh();
        cancelCallback();
        pnc.ui.Alert.success('El pago fue hecho');
    };

    var paymentCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.CREATED:
                break;

            case pnc.client.Status.PRECONDITION_FAILED:
                pnc.ui.SlideDialog.error('El pago excede el balance del cliente').open();
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.creditLine.getPayments().push(response.content());
        self.client.get(self.creditLine.getId(), creditLineCallback);
    };

    var saveCallback = function() {
        if(!self.validatePaymentForm(inputs))
            return;

        var newPayment = self.parsePaymentForm(inputs, self.creditLine);

        if(newPayment.getAmount() > self.creditLine.getBalance()) {
            pnc.ui.SlideDialog.error('El monto del pago excede el balance de la linea de credito').open();
            return;
        }

        self.client.addPayment(self.creditLine, newPayment, paymentCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Hacer Pago', pnc.ui.Resources.PAYMENT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Pago');
};

/**
 * Creates the form that is used to enter payment data.
 * @param  {Element}           parent  Parent element where the form will be placed
 * @return {Object.<string, Element>}  Input elements within form
 * @private
 */
pnc.ui.CreditLineView.prototype.createPaymentForm = function(parent) {
    var inputs  = {};

    var group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    var label = goog.dom.createDom('label', {'for' : 'payment-amount', 'class' : 'span1'}, 'Cantidad');
    goog.dom.appendChild(group, label);
    var amountInput = goog.dom.createDom('input', {'id' : 'payment-amount', 'class' : 'span2', 'type' : 'text'});
    goog.dom.appendChild(group, amountInput);
    pnc.ui.assureNumbers(amountInput);
    inputs['payment-amount'] = amountInput;
    var help = goog.dom.createDom('span', 'help-inline', 'Debe ser un numero mayor a cero');
    goog.dom.appendChild(group, help);

    group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    label = goog.dom.createDom('label', {'for' : 'payment-notes', 'class' : 'span1'}, 'Notas');
    goog.dom.appendChild(group, label);
    var notesInput = goog.dom.createDom('textarea', {'id' : 'payment-notes', 'class' : 'span4'});
    goog.dom.appendChild(group, notesInput);
    inputs['payment-notes'] = notesInput;

    return inputs;
};

/**
 * Validates the content of the payment form. If an error is found in any of the fields it is highlighted inline within
 * the form.
 * @param  {Object.<string, Element>} inputs  Input elements within form
 * @return {boolean}                          Was the form data OK?
 * @private
 */
pnc.ui.CreditLineView.prototype.validatePaymentForm = function(inputs) {
    var errors = goog.dom.getElementsByClass('error', inputs['payment-amount'].parentElement.parentElement);
    
    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(inputs['payment-amount'].value.length < 1 || !pnc.ui.FLOAT_RGX.test(inputs['payment-amount'].value) || 
       inputs['payment-amount'].value <= 0) {
        goog.dom.classes.add(inputs['payment-amount'].parentElement, 'error');
        return false;
    }

    return true;
};

/**
 * Parses the data found in the payment form and creates a payment model out of it.
 * @param  {Object.<string, Element>} inputs  Input elements within form
 * @return {pnc.model.Payment}                Derived payment model
 */
pnc.ui.CreditLineView.prototype.parsePaymentForm = function(inputs) {
    var payment = new pnc.model.Payment(pnc.model.DEFAULT_ID,
                                        this.creditLine.getId(),
                                        pnc.client.Profile.getUser().getId(),
                                        new Date(),
                                        pnc.ui.round(inputs['payment-amount'].value),
                                        inputs['payment-notes'].value);
    return payment;
};

/**
 * Provides the user with a dialog box to confirm the action to remove the payment. If the user cofirms, processes the
 * server transaction to have the payment removed.
 * @param  {event} e  Raised event
 * @private
 */
pnc.ui.CreditLineView.prototype.removePayment = function(e) {
    var row     = e.target.parentElement.parentElement;
    row         = row.nodeName === 'TD' ? row.parentElement : row;
    var payment = pnc.model.findModel(this.creditLine.getPayments(), row.dataset.payment);
    var dialog  = pnc.ui.SlideDialog.warning('Esta seguro que desea remover este pago?');
    var self    = this;

    var creditLineCallack = function(response) {
        self.creditLine = response.content();

        if(goog.isDefAndNotNull(self.refreshCallback))
            self.refreshCallback(self.creditLine);

        var balance = goog.dom.getElement('credit-line-detail-balance');
        var value = pnc.ui.DECIMAL_FORMATTER.format(self.creditLine.getBalance());
        goog.dom.setTextContent(balance, value);
    };
    var fadeCallback = function() { 
        pnc.ui.Alert.success('El pago fue removido exitosamente');
        goog.dom.removeNode(row); 
    };
    var paymentCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                dialog.dispose();
                pnc.ui.SlideDialog.error('No se pudo encontrar este pago').open();
                return;

            default:
                dialog.dispose();
                var message = pnc.client.decodeStatus(response);
                pnc.ui.SlideDialog.error(message).open();
                return;
        }

        var fade = new goog.fx.dom.FadeOutAndHide(row, 500);
        goog.events.listen(fade, goog.fx.Animation.EventType.END, fadeCallback);
        fade.play();
        self.client.get(self.creditLine.getId(), creditLineCallack);
    };
    var selectCallback = function(e) {
        if(e.key === pnc.ui.SlideDialog.Keys.OK) 
            self.client.removePayment(payment, paymentCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};
