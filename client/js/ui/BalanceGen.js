goog.provide('pnc.ui.BalanceGen');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.string');

goog.require('pnc.client.Balance');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');

/**
 * A view used to view balances, and generate new ones. A balance is a report of all activity performed during a shift.
 * It totals activity, and allows the user to enter data about cash in hand, and reconcile any possible discrepancy.
 * @constructor
 */
pnc.ui.BalanceGen = function() {
    /**
     * The balance object that is being represented by the view
     * @type {pnc.model.Balance}
     * @private
     */
    this.balance = null;

    /**
     * Service client used to make balance related transactions
     * @type {pnc.client.Balance}
     * @private
     */
    this.client  = new pnc.client.Balance();

    //------------- elements -------------//
    /** @private */ this.container        = null;
};
pnc.ui.Main.registerModule('balance-gen', pnc.ui.BalanceGen);

/**
 * Renders the view
 * @param  {Element} parent  Parent element where the view will be rendered
 */
pnc.ui.BalanceGen.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderActivityPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Generar Balance');
};

/**
 * Disposes the view. Clenas up resources consumed
 */
pnc.ui.BalanceGen.prototype.dispose = function() {
    goog.dom.removeNode(this.container);
};

/**
 * Renders the activity panel
 * @private
 */
pnc.ui.BalanceGen.prototype.renderActivityPane = function() {
    var self = this;
    var callback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.balance = response.content();
        var content = pnc.ui.createBox(self.container, 'span10', pnc.ui.Resources.SHARE, 'Actividad');
        var formatter = pnc.ui.DECIMAL_FORMATTER;

        var data = [
            ['Inicio',           formatter.format(self.balance.getStart())],
            ['Ventas Contado',   formatter.format(self.balance.getCashSales())],
            ['Ventas a Credito', formatter.format(self.balance.getCreditSales())],
            ['Devoluciones',     formatter.format(self.balance.getRefunds())]
        ];
        var body = pnc.ui.createSummaryTable(content, 'balance-table summary-table', 2, 'Ventas', data);
        body.parentElement.style.margin = '0 20px 0 10px';

        data = [
            ['Pagos',     formatter.format(self.balance.getPayments())],
            ['Gastos',    formatter.format(self.balance.getExpenses())],
            ['Depositos', formatter.format(self.balance.getDeposits())]
        ];
        pnc.ui.createSummaryTable(content, 'balance-table summary-table', 2, 'Transacciones', data);

        self.renderBalancePane();
        self.renderResultPane();
    };

    this.client.generate(callback);
};

/**
 * Renders the balance panel
 * @private
 */
pnc.ui.BalanceGen.prototype.renderBalancePane = function() {
    var content = pnc.ui.createBox(this.container, 'span10', pnc.ui.Resources.EQUALIZER, 'Balance');

    var nextGroup = goog.dom.createDom('div', {'class' : 'inline input-prepend span4', 'style' : 'margin-right: 50px'});
    goog.dom.appendChild(content, nextGroup);
    var nextLabel = goog.dom.createDom('label', 'span2', 'Proximo Inicio');
    goog.dom.appendChild(nextGroup, nextLabel);
    var moneyLabel1 = goog.dom.createDom('span', 'add-on', '$');
    goog.dom.appendChild(nextGroup, moneyLabel1);
    var nextInput = goog.dom.createDom('input', {'id' : 'balance-next', 'type' : 'text', 'class' : 'span2', 'value' : '0.00'});
    goog.dom.appendChild(nextGroup, nextInput);
    pnc.ui.assureNumbers(nextInput);

    var balanceLabel = goog.dom.createDom('label', 'span2 offset1', 'Balance');
    goog.dom.appendChild(content, balanceLabel);
    var value = pnc.ui.DECIMAL_FORMATTER.format(this.balance.getBalance());
    var balanceValue = goog.dom.createDom('label', {'class' : 'span2 bold', 'style' : 'margin-left: -60px'}, value);
    goog.dom.appendChild(content, balanceValue);
    goog.events.listen(nextInput, goog.events.EventType.KEYUP, goog.bind(this.calculateBalance, this));

    var cashGroup = goog.dom.createDom('div', {'class' : 'inline input-prepend span4', 'style' : 'margin-right: 50px'});
    goog.dom.appendChild(content, cashGroup);
    var cashLabel = goog.dom.createDom('label', 'span2', 'Efectivo');
    goog.dom.appendChild(cashGroup, cashLabel);
    var moneyLabel2 = goog.dom.createDom('span', 'add-on', '$');
    goog.dom.appendChild(cashGroup, moneyLabel2);
    var cashInput = goog.dom.createDom('input', {'id' : 'balance-cash', 'type' : 'text', 'class' : 'span2', 'value' : '0.00'});
    goog.dom.appendChild(cashGroup, cashInput);
    goog.events.listen(cashInput, goog.events.EventType.KEYUP, goog.bind(this.calculateBalance, this));
    pnc.ui.assureNumbers(cashInput);

    var notesLabel = goog.dom.createDom('label', 'span2 offset1', 'Notas');
    goog.dom.appendChild(content, notesLabel);
    var notesInput = goog.dom.createDom('textarea', {'id' : 'balance-notes', 'style' : 'width: 240px; height: 80px; margin-left: -60px;'});
    goog.dom.appendChild(content, notesInput);
};

/**
 * Renders the result panel
 * @private
 */
pnc.ui.BalanceGen.prototype.renderResultPane = function() {
    var content = pnc.ui.createBox(this.container, 'span10', pnc.ui.Resources.RESULT, 'Resultado');

    var expectedLabel = goog.dom.createDom('label', 'span2', 'Balance Esperado');
    goog.dom.appendChild(content, expectedLabel);
    var expectedValue = goog.dom.createDom('label', {'id' : 'expected-value', 'class': 'span2 bold'});
    goog.dom.appendChild(content, expectedValue);

    var differenceLabel = goog.dom.createDom('label', 'span2 offset1', 'Diferencia');
    goog.dom.appendChild(content, differenceLabel);
    var differenceValue = goog.dom.createDom('label', {'id' : 'difference-value', 'class' : 'span2 bold'});
    goog.dom.appendChild(content, differenceValue);
    this.calculateBalance();

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var okButton = goog.dom.createDom('button', 'btn btn-primary', 'OK');
    goog.dom.appendChild(controlPane, okButton);

    var self = this;
    var clientCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        pnc.ui.Alert.success('Balance procesado');
        window.location.href = '#pos';
    };

    var clickCallback = function(e) { self.client.add(self.balance, clientCallback); };
    goog.events.listen(okButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Uses balance data, and user input to calculate the expected balance, as well as any applicable difference.
 * Updates the model with the current values for next start and difference.
 * @private
 */
pnc.ui.BalanceGen.prototype.calculateBalance = function() {
    var nextInput       = goog.dom.getElement('balance-next');
    var cashInput       = goog.dom.getElement('balance-cash');
    var expectedValue   = goog.dom.getElement('expected-value');
    var differenceValue = goog.dom.getElement('difference-value');

    var next = nextInput.value.length > 0 ? pnc.ui.round(nextInput.value) : 0;
    var cash = cashInput.value.length > 0 ? pnc.ui.round(cashInput.value) : 0;

    var expected = this.balance.getStart() + this.balance.getCashSales() - this.balance.getRefunds() + 
        this.balance.getPayments() + this.balance.getDeposits() - this.balance.getExpenses() - next;
    var actual = cash - expected;

    this.balance.setNextStart(next);
    this.balance.setDifference(actual);
    goog.dom.setTextContent(expectedValue, pnc.ui.DECIMAL_FORMATTER.format(expected));
    goog.dom.setTextContent(differenceValue, pnc.ui.DECIMAL_FORMATTER.format(actual));
};
