goog.provide('pnc.ui.Main');

goog.require('goog.History');
goog.require('goog.array');
goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.ui.AnimatedZippy');
goog.require('goog.ui.Component');
goog.require('goog.ui.MenuItem');
goog.require('pnc.ui.Notifier');
goog.require('goog.ui.Select');
goog.require('goog.ui.Zippy');

goog.require('pnc.client.Auth');

/**
 * Main applicatin page scaffolding and basic outer content functionality
 * @class
 * @static
 */
pnc.ui.Main = {};

/**
 * Used to keep track of changes in state and navigation.
 * @type {goog.History
 * @private
 */
pnc.ui.Main.history = null;

/**
 * Holds a list of registered modules. Navigation into these modules will be performed by getting history change events.
 * @type {Array}
 * @private
 */
pnc.ui.Main.modules = {};

pnc.ui.Main.currentView = null;
pnc.ui.Main.zippies     = [];
pnc.ui.Main.dropbox     = null;
pnc.ui.Main.breadcrumb  = [];

/**
 * Registers the main script to the page. Main point of entry for the script.
 */
pnc.ui.Main.register = function() {
    pnc.client.Profile.init();
    pnc.ui.Main.buildUserMenu();
    pnc.ui.Main.loadNotifications();
    pnc.ui.Main.buildMainMenu();
    pnc.ui.Main.buildContentPlaceHolder();
    pnc.ui.Main.setupHistory();
    document.location.href = '#';
};
goog.exportSymbol('pnc.ui.Main.register', pnc.ui.Main.register);

/**
 * Builds the user menu. This provides the abilty to reset password and log out.
 * @private
 */
pnc.ui.Main.buildUserMenu = function() {
    var userBox = goog.dom.getElement('user-box');
    var user    = pnc.client.Profile.getUser();
    var select  = new goog.ui.Select(user.toString());
    select.render(userBox);

    var options = {
        change : ['Cambiar Contrasena', pnc.ui.Main.resetPassword],
        logout : ['Salir', pnc.ui.Main.logout]
    };

    for(var key in options) {
        var item = new goog.ui.MenuItem(options[key][0]);
        item.setValue(key);
        select.addItem(item);
        var element = goog.dom.getElementByClass('goog-menuitem-content', item.getElement());

        if(goog.isDefAndNotNull(element))
            element.style.fontSize = '16px';
    }

    var callback = function(e) { 
        options[select.getValue()][1](); 
        select.setValue(null);
    };
    goog.events.listen(select, goog.ui.Component.EventType.ACTION, callback);
};

/**
 * Initializes the notification widget and loads any existing notifications for the user.
 */
pnc.ui.Main.loadNotifications = function() {
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.NOTIFICATION_VIEW))
        return;

    var parent   = goog.dom.getElement('notifications');
    var notifier = new pnc.ui.Notifier();
    notifier.render(parent);
    notifier.update();

    var content = goog.dom.getElement('content');
    var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.ARROW_UP, 'style' : 'float:right'});
    goog.dom.insertChildAt(content, img, 0);
    var message = goog.dom.createDom('h2', {'style' : 'width: 300px; display:inline-block; float:right; ' +
        'margin-right:-70px; margin-top: 20px'}, 'Notificaciones indican casos que requieren de atencion');
    goog.dom.appendChild(content, message);
};

/**
 * Builds the main navigation menu. Checks the user's privileges before deciding which elements to populate into the menu.
 */
pnc.ui.Main.buildMainMenu = function() {
    var sidebar = goog.dom.getElement('side-bar');
    var privs   = pnc.client.Profile.getPrivileges();
    var keys    = pnc.client.Privileges;
    var menu    = null;

    menu = pnc.ui.Main.createMenuItem(sidebar, pnc.ui.Resources.MONEY, 'Caja', privs, [keys.ORDER_ADD, keys.BALANCE_ADD]);
    pnc.ui.Main.createSubMenuItem(menu, 'Punto de Venta', '#pos',         privs, keys.ORDER_ADD);
    pnc.ui.Main.createSubMenuItem(menu, 'Balance',        '#balance-gen', privs, keys.BALANCE_ADD);
    
    menu = pnc.ui.Main.createMenuItem(sidebar, pnc.ui.Resources.LAYERS, 'Data', privs, [keys.INVOICE_VIEW, keys.BALANCE_VIEW, keys.PRODUCT_VIEW]);
    pnc.ui.Main.createSubMenuItem(menu, 'Facturas',  '#orders',   privs, keys.INVOICE_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Balances',  '#balances', privs, keys.BALANCE_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Productos', '#products', privs, keys.PRODUCT_VIEW);

    menu = pnc.ui.Main.createMenuItem(sidebar, pnc.ui.Resources.USER_WHITE, 'Gente', privs, [keys.CUSTOMER_VIEW, keys.SUPPLIER_VIEW, keys.SPONSOR_VIEW]);
    pnc.ui.Main.createSubMenuItem(menu, 'Clientes',       '#customers', privs, keys.CUSTOMER_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Suplidores',     '#suppliers', privs, keys.SUPPLIER_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Patrocinadores', '#sponsors',  privs, keys.SPONSOR_VIEW);

    menu = pnc.ui.Main.createMenuItem(sidebar, pnc.ui.Resources.CHART_INCREASING, 'Administracion', privs, [keys.EXPENSE_VIEW, keys.INCOME_VIEW, keys.REPORT_GENERATE]);
    pnc.ui.Main.createSubMenuItem(menu, 'Gastos',    '#expenses', privs, keys.EXPENSE_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Depositos', '#deposits', privs, keys.DEPOSIT_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Reportes',  '#reports',  privs, keys.REPORT_GENERATE);

    menu = pnc.ui.Main.createMenuItem(sidebar, pnc.ui.Resources.COG_WHITE, 'Configuracion', privs, [keys.USER_VIEW, keys.GROUP_VIEW]);
    pnc.ui.Main.createSubMenuItem(menu, 'Usuarios', '#users',  privs, keys.USER_VIEW);
    pnc.ui.Main.createSubMenuItem(menu, 'Grupos',   '#groups', privs, keys.GROUP_VIEW);
};

/**
 * Creates a main menu item in the main navigation pane.
 * @param {Element} parent           Parent element where this menu item will be placed
 * @param {string} icon              String indicating the resource file for the icon to be placed on this menu tiem.
 * @param {string} title             String with text that should be displayed by the menu item.
 * @param {Array.<string>} privs     List of privilege keys the user has been granted
 * @param {Array.<string>} required  List of privileges required to show the menu. These privileges correspond to the expected
 *                                       child menu items. If any one of these privileges are met, the menu will be generated.
 * @return {Element}                 Created item
 * @private
 */
pnc.ui.Main.createMenuItem = function(parent, icon, title, privs, required) {
    var found = false;

    for(var cont = 0, size = required.length; cont < size; cont++) {
        if(goog.array.contains(privs, required[cont])) {
            found = true;
            break;
        }
    }

    if(!found)
        return;

    var menuItem = goog.dom.createDom('li');
    goog.dom.appendChild(parent, menuItem);
    var link = goog.dom.createDom('a', {'href' : '#'});
    goog.dom.appendChild(menuItem, link);
    var icon = goog.dom.createDom('img', {'src' : icon});
    goog.dom.appendChild(link, icon);
    var label = goog.dom.createDom('span', {}, title);
    goog.dom.appendChild(link, label);

    var submenu = goog.dom.createDom('ul', 'sub-menu');
    goog.dom.appendChild(parent, submenu);
    var zippy = new goog.ui.AnimatedZippy(menuItem, submenu);
    pnc.ui.Main.zippies.push(zippy);

    var zippyCallback = function(zippy) {
        for(var cont = 0, size = pnc.ui.Main.zippies.length; cont < size; cont++) {
            var z = pnc.ui.Main.zippies[cont];

            if(z !== zippy && z.isExpanded())
                z.setExpanded(false);                
        }
    };
    goog.events.listen(zippy, goog.ui.Zippy.Events.ACTION, goog.partial(zippyCallback, zippy));

    var callback = function(e) { e.preventDefault(); };
    goog.events.listen(link, goog.events.EventType.TOUCHEND, callback);
    
    return submenu;
};

/** 
 * Creates a sub menu item in the main navigation pane.
 * @param {Element}        parent    Parent element where this menu item will be placed in. A ul element is expected.
 * @param {string}         title     String that should be the text content of the menu
 * @param {string}         anchor    Anchor link where the menu item should point to
 * @param {Array.<string>} privs     List of all privileges the user has access to
 * @param {string}         required  The privilege which is required of the user in order to presetn this menu item in the session.
 * @private
 */
pnc.ui.Main.createSubMenuItem = function(parent, title, anchor, privs, required) {
    if(!goog.array.contains(privs, required))
        return;

    var menuItem = goog.dom.createDom('li');
    goog.dom.appendChild(parent, menuItem);
    var link = goog.dom.createDom('a', {'href' : anchor}, title);
    goog.dom.appendChild(menuItem, link);
};


/** 
 * Generates the placeholder content that will be placed in the main view, before the user makes any selections from the
 * main menu.
 */
pnc.ui.Main.buildContentPlaceHolder = function() {};

/**
 * Provides the dialog window with the form that the user resets the password.
 */
pnc.ui.Main.resetPassword = function() {
    var dialog  = new pnc.ui.SlideDialog('', [pnc.ui.SlideDialog.Keys.CANCEL, pnc.ui.SlideDialog.Keys.OK], pnc.ui.SlideDialog.Types.INFO);
    var content = dialog.getElement().getContentElement();
    goog.dom.classes.add(content, 'password-reset-dialog');

    var table = goog.dom.createDom('table');
    goog.dom.appendChild(content, table);
    var rows = [];

    for(var cont = 0, size = 3; cont < size; cont++) {
        rows[cont] = goog.dom.createDom('tr');
        goog.dom.appendChild(table, rows[cont]);
    }

    var current = pnc.ui.createLabeledDom(rows[0], 'input', 'Contrasena Actual', '', {type : 'password'});
    var passwd  = pnc.ui.createLabeledDom(rows[1], 'input', 'Nueva Contrasena', '', {type : 'password'});
    var confirm = pnc.ui.createLabeledDom(rows[2], 'input', 'Confirmar', '', {type : 'password'});
    current.focus();

    var checkCell = goog.dom.createDom('td', {'rowspan' : 2});
    goog.dom.appendChild(rows[1], checkCell);
    var check = goog.dom.createDom('img', 'password-confirm invisible');
    goog.dom.appendChild(checkCell, check);

    var confirmCallback = function(e) {
        if(passwd.value.length === 0 && confirm.value.length === 0) {
            check.src = '';
            goog.dom.classes.add(check, 'invisible');
        } else if(passwd.value === confirm.value) {
            check.src = pnc.ui.Resources.SUCCESS;
            goog.dom.classes.remove(check, 'invisible');
         } else {
            check.src = pnc.ui.Resources.FAILURE;
            goog.dom.classes.remove(check, 'invisible');
        }
    };
    goog.events.listen(passwd, goog.events.EventType.INPUT, confirmCallback);
    goog.events.listen(confirm, goog.events.EventType.INPUT, confirmCallback);

    var xhrCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                dialog.dispose();
                break;

            case pnc.client.Status.UNAUTHORIZED:
                var message = goog.dom.createDom('div', 'input-error', 'La contraseña actual no fue reconocida');
                goog.dom.classes.add(check, 'invisible');
                goog.dom.appendChild(current.parentElement, message);
                break;

            default:
                dialog.dispose();
                dialog.getElement().dispose();
                var message = pnc.client.decodeStatus(response);
                pnc.ui.SlideDialog.error(message).open();
                break;
        }
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        e.preventDefault();
        var errors = goog.dom.getElementsByClass('input-error', table);

        for(var cont = 0, size = errors.length; cont < size; cont++)
            goog.dom.removeNode(errors[cont]);

        if(!goog.isDef(current.value) || goog.isNull(current.value) || current.value.length < 1) {
            var message = goog.dom.createDom('div', 'input-error', 'The current password field cannot be empty');
            goog.dom.appendChild(current.parentElement, message);
            return;
        }

        if(!goog.isDef(passwd.value) || goog.isNull(passwd.value) || passwd.value.length < 1) {
            var message = goog.dom.createDom('div', 'input-error', 'The password field cannot be empty');
            goog.dom.appendChild(passwd.parentElement, message);
            return;
        }

        if(!goog.isDef(confirm.value) || goog.isNull(confirm.value) || confirm.value.length < 1) {
            var message = goog.dom.createDom('div', 'input-error', 'The password confirm field cannot be empty');
            goog.dom.appendChild(confirm.parentElement, message);
            return;
        }

        if(passwd.value !== confirm.value) {
            var message = goog.dom.createDom('div', 'input-error', 'The password and confirm fields do not match');
            goog.dom.appendChild(passwd.parentElement, message);
            return;
        }

        var client = new pnc.client.Auth();
        client.resetPassword(current.value, passwd.value, xhrCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Handles the logout transaction
 */
pnc.ui.Main.logout = function() {
    var callback = function(r) {
        switch(r.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.UNAUTHORIZED: //session already expired
                pnc.client.Profile.clear();
                goog.net.XhrIo.cleanup();
                goog.events.removeAll();
                document.location.href = 'login.html';
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(r)).open();
                break;
        }
    };
    var client = new pnc.client.Auth();
    client.logout(callback);
};

/**
 * Sets up history management and loads all registered modules to respond to changes in URL fragment
 * @private
 */
pnc.ui.Main.setupHistory = function() {
    pnc.ui.Main.history = new goog.History();
    pnc.ui.Main.history.setEnabled(true);
    var content = goog.dom.getElement('content');

    var callback = function(e) {
        if(!goog.isDefAndNotNull(e.token) || e.token.length < 1)
            return;

        var notifier = goog.dom.getElement('notifier-pane');

        if(goog.isDefAndNotNull(notifier))
            goog.dom.removeNode(notifier);

        var sliders = goog.dom.getElementsByClass('slide-panel');

        for(var cont = 0, size = sliders.length; cont < size; cont++)
            goog.dom.removeNode(sliders[cont].parentElement);

        if(goog.isDefAndNotNull(pnc.ui.Main.currentView))
            pnc.ui.Main.currentView.dispose();

        goog.dom.removeChildren(content);
        var constructor = pnc.ui.Main.modules[e.token];
        
        if(goog.isDefAndNotNull(constructor)) {
            var tooltips = goog.dom.getElement('tooltips');
            goog.dom.removeChildren(tooltips);

            var view = new constructor();
            view.render(content);
            pnc.ui.Main.currentView = view;
        } else {
            pnc.ui.Alert.info('Este modulo aun no ha sido implementado');
        }
    };
    goog.events.listen(pnc.ui.Main.history, goog.history.EventType.NAVIGATE, callback);
};

/**
 * Used to register modules to changes in URL fragment are made. This function must be invoked during script evaluation.
 * By the time the 'pnc.ui.Main.register' function is invoked, as part of the main page bootstrapping, it will take all
 * registered modules, and handle changes with the accordingly. To register a module, the technique is similar to exporting
 * symbols with closure. After the module constructor declaration, invoke this function, passing the constructor as the
 * module argument. It's expected that the object passed can work with a no-arg constructor, and implements 'render' and 
 * 'dispose' methods (for rendering, and disposing respectively).
 * @param {string}   fragment  URL fragment this module should listen for. 
 * @param {function} module    Constructor of the class that will be used for this module. The constructor should work without
 *                             arguments. The class should also have 'render' and 'dispose' methods implemented
 *                             for initilization and tear-down respectively.
 */
pnc.ui.Main.registerModule = function(fragment, module) {
    pnc.ui.Main.modules[fragment] = module;
};

/**
 * Pushes a title unto the breadcrumb portion of the title bar
 * @param  {string} title  Title to push
 */
pnc.ui.Main.breadcrumbPush = function(title) {
    pnc.ui.Main.breadcrumb.push(title);
    pnc.ui.Main.renderBreadcrumb();
};

/**
 * Removes the last title from the breadcrumb in the title bar
 */
pnc.ui.Main.breadcrumbPop = function() {
    goog.array.removeAt(pnc.ui.Main.breadcrumb, pnc.ui.Main.breadcrumb.length -1);
    pnc.ui.Main.renderBreadcrumb();
};

/**
 * Removes all elements from the breadcrumb in the title bar
 */
pnc.ui.Main.breadcrumbClear = function() {
    pnc.ui.Main.breadcrumb = [];
    var breadcrumb = goog.dom.getElement('breadcrumb');
    goog.dom.removeChildren(breadcrumb);
};

/**
 * Renders the breadcrumb with the current title stack
 * @private
 */
pnc.ui.Main.renderBreadcrumb = function() {
    var breadcrumb = goog.dom.getElement('breadcrumb');
    goog.dom.removeChildren(breadcrumb);

    for(var cont = 0, size = pnc.ui.Main.breadcrumb.length; cont < size; cont++) {
        var title  = pnc.ui.Main.breadcrumb[cont];
        var header = goog.dom.createDom('h2', null, title);
        goog.dom.appendChild(breadcrumb, header);

        if((size - cont) > 1) {
            var img    = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLAY_LIGHT});
            goog.dom.appendChild(breadcrumb, img);
        }
    }
};
