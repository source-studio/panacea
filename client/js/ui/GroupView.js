goog.provide('pnc.ui.GroupView');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');

goog.require('pnc.client.Group');
goog.require('pnc.client.Privilege');
goog.require('pnc.client.User');
goog.require('pnc.ui');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.Paginator');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');


/**
 * View that allows the user to administer groups, the users subscribed to them, and the privileges granted to them.
 * @constructor
 */
pnc.ui.GroupView = function() {
    this.groups        = null;
    this.client        = new pnc.client.Group();
    this.userClient    = new pnc.client.User();
    this.privClient    = new pnc.client.Privilege();
    this.container     = null;
    this.disposables   = [];
    this.paginator     = null;
    this.userPaginator = null;
    this.userMap       = {};
};
pnc.ui.Main.registerModule('groups', pnc.ui.GroupView);


/**
 * Renders the view
 * @param  {Element} parent  Parent element wherethe view will be placed in
 */
pnc.ui.GroupView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0'});
    goog.dom.appendChild(parent, this.container);

    this.renderGroupPane();
    this.renderActionPane();
    this.renderDetailPane();
    this.getGroups();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Grupos');
};

/**
 * Cleans up resources and removes the element from the DOM
 * @return {[type]} [description]
 */
pnc.ui.GroupView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};


/**
 * Retrieves the collection of existing groups. Populates groups on collection element.
 * @private
 */
pnc.ui.GroupView.prototype.getGroups = function() {
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.populateGroupPane(response.content());
    };
    this.client.getAll(callback);
};

/**
 * Renders the pane where existing groups will be displayed
 * @private
 */
pnc.ui.GroupView.prototype.renderGroupPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Grupos');
    content.id = 'group-pane';
    content.style.minHeight = '590px';

    var head = goog.dom.getElementByClass('box-head', content.parentElement);
    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var backButton, forwardButton, updateButton = null, removeButton = null, userButton = null, privButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.GROUP_SUBSCRIPTION))
        userButton = pnc.ui.createToolbarButton(actionGroup, 'user-button', pnc.ui.Resources.USER_GRAY, 'Usuarios', false, goog.bind(this.users, this));

    if(goog.array.contains(privs, pnc.client.Privileges.GROUP_SUBSCRIPTION))
        privButton = pnc.ui.createToolbarButton(actionGroup, 'priv-button', pnc.ui.Resources.PAWN, 'Privilegios', false, goog.bind(this.privileges, this));

    if(goog.array.contains(privs, pnc.client.Privileges.GROUP_UPDATE))
        updateButton = pnc.ui.createToolbarButton(actionGroup, 'update-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.update, this));

    if(goog.array.contains(privs, pnc.client.Privileges.GROUP_REMOVE))
        removeButton = pnc.ui.createToolbarButton(actionGroup, 'remove-button', pnc.ui.Resources.REMOVE, 'Remover', false, goog.bind(this.remove, this));

    backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button', pnc.ui.Resources.ARROW_LEFT, 'Atras', false);
    forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var self = this;
    var navCallback = function(e) {
        if(!goog.isNull(updateButton))
            updateButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;
    };
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallback);
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);

    this.paginator = new pnc.ui.Paginator(['Nombre'], [], null, 20, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderGroup, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Renders the pane that contains the ability to add new groups
 * @private
 */
pnc.ui.GroupView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.GROUP_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));
    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Grupo');
    goog.dom.appendChild(addButton, span);
};

pnc.ui.GroupView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '157px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 1, 'Descripcion');
    body.id  = 'detail-table';
};

/**
 * Paginator callback. Used to create a row that represents a group.
 * @param  {Element}         parent  Parent element where the group representation will be placed
 * @param  {pnc.model.Group} group   Group model to represent
 * @private
 */
pnc.ui.GroupView.prototype.renderGroup = function(parent, group) {
    var row = goog.dom.createDom('tr', {'data-group' : group.getId()});
    goog.dom.appendChild(parent, row);
    var buttons = [
        goog.dom.getElement('user-button'),
        goog.dom.getElement('priv-button'),
        goog.dom.getElement('update-button'),
        goog.dom.getElement('remove-button')
    ];

    var self = this;
    var callback = function(group, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;

        self.populateDetailPane(group);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, group));

    var cell = goog.dom.createDom('td', null, group.getName());
    goog.dom.appendChild(row, cell);
};

/**
 * Populates the group pane with the provided array of groups
 * @param  {Array.<pnc.model.Group>} groups  Groups that will be used to populate
 * @private
 */
pnc.ui.GroupView.prototype.populateGroupPane = function(groups) {
    var content = goog.dom.getElement('group-pane');
    var buttons = [
        goog.dom.getElement('user-button'),
        goog.dom.getElement('priv-button'),
        goog.dom.getElement('update-button'),
        goog.dom.getElement('remove-button')
    ];

    for(var cont = 0, size = buttons.length; cont < size; cont++)
        if(goog.isDefAndNotNull(buttons[cont]))
            buttons[cont].disabled = true;

    this.paginator.setData(groups);
    this.paginator.refresh();
};

/**
 * Shows the group description in the detail pane when a group is selected in the main view.
 * @param  {pnc.model.Group} group  Selected group
 * @private
 */
pnc.ui.GroupView.prototype.populateDetailPane = function(group) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);

    if(goog.isDefAndNotNull(group))
        pnc.ui.createSummaryContent(body, group.getDescription());
};

/**
 * Creates a form that will allow the user to add a new group
 * @private
 */
pnc.ui.GroupView.prototype.add = function() {
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createGroupForm(content, null);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('butotn', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.CREATED:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('El nombre del grupo no se puede repetir').open();
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var group = response.content();
        self.paginator.getData().push(group);
        goog.array.sort(self.paginator.getData(), pnc.model.Group.compare);
        self.paginator.refresh();
        cancelCallback();
        pnc.ui.Alert.success('El group fue agregado');
    }

    var saveCallback = function() {
        if(!self.validateGroupForm(inputs))
            return;

        var group = self.parseGroupForm(inputs);
        self.client.add(group, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Creates a form that will be used to update an existing group
 * @private
 */
pnc.ui.GroupView.prototype.update = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var group     = pnc.model.findModel(this.paginator.getData(), row.dataset.group);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createGroupForm(content, group);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var self = this;
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var newGroup = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newGroup);
        self.paginator.refresh();

        var buttons = [
            goog.dom.getElement('user-button'),
            goog.dom.getElement('priv-button'),
            goog.dom.getElement('update-button'),
            goog.dom.getElement('remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El grupo de actualizado');
    };

    var saveCallback = function() {
        if(!self.validateGroupForm(inputs))
            return;

        var newGroup = self.parseGroupForm(inputs, group);
        newGroup.setId(group.getId());
        self.client.update(newGroup, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog. If accepted, executes the transaction to remove the selected group.
 * @private
 */
pnc.ui.GroupView.prototype.remove = function() {
    var row    = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var group  = pnc.model.findModel(this.paginator.getData(), row.dataset.group);
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover a este grupo');

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), group);
        self.paginator.refresh();
        pnc.ui.Alert.success('El grupo fue removido');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.remove(group, responseCallback);
    };
    dialog.setCallback(selectCallback);
    dialog.open();
};

/**
 * Creates a slider that displays all the users that are subscribed to the selected group. Exposes functionality to
 * administer said users.
 * @private
 */
pnc.ui.GroupView.prototype.users = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var group     = pnc.model.findModel(this.paginator.getData(), row.dataset.group);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var searchGroup = goog.dom.createDom('div', 'input-append');
    goog.dom.appendChild(content, searchGroup);
    var userSearch = goog.dom.createDom('input', {'class' : 'span4', 'type' : 'text', 'style' : 'width : 210px'});
    goog.dom.appendChild(searchGroup, userSearch);
    var userAc = goog.ui.ac.createSimpleAutoComplete([], userSearch, false, true);
    this.disposables.push(userAc);
    var userPill         = pnc.ui.createPill(userSearch, userAc);
    userPill.id          = 'user-search-pill';
    userPill.style.width = '202px';
    var userAdd = goog.dom.createDom('button', {'id' : 'user-add', 'class' : 'btn btn-primary', 'disabled' : true}, 'Agregar');
    goog.dom.appendChild(searchGroup, userAdd);
    var self = this;
    var callback = function() {
        var user = self.userMap[userSearch.value];
        self.addUser(group, user);
        pnc.ui.closePill(userPill);
    };
    goog.events.listen(userAdd, goog.events.EventType.CLICK, callback);

    var link = pnc.client.Profile.getLink(pnc.client.User.Rel.SEARCH);
    var resolver = function() { return userSearch.value; };
    var decoder  = goog.partial(pnc.client.decodeCollection, pnc.client.Customer.decode);
    pnc.ui.updateAcData(userSearch, userAc, link, {'{name}' : resolver}, decoder);

    var xhrCallback = function(e) {
        goog.object.clear(self.userMap);

        switch(e.target.getStatus()) {
            case pnc.client.Status.OK:
                var users = pnc.client.decodeCollection(pnc.client.User.decode, e.target.getResponseText());
                var data  = [];

                for(var cont = 0, size = users.length; cont < size; cont++) {
                    data[cont] = users[cont].toString();
                    self.userMap[data[cont]] = users[cont];
                }

                userAc.matcher_.setRows(data);
                break;

            case pnc.client.Status.NOT_FOUND:
                userAc.matcher_.setRows([]);
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(e.target)).open();
                break;
        }
    };

    var inputCallback = function(e) {
        var userValue = goog.string.trim(userSearch.value);
        userAdd.disabled = userValue.length < 1;

        if(userValue.length < 1 || !goog.events.KeyCodes.isCharacterKey(e.keyCode))
            return;

        var uri = link.uri().replace('{name}', goog.string.urlEncode(userValue));
        goog.net.XhrIo.send(uri, xhrCallback, link.method());
    };
    goog.events.listen(userSearch, goog.events.EventType.KEYUP, inputCallback);

    var slider = new pnc.ui.SlidePanel('Busqueda', pnc.ui.Resources.SEARCH, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush(group.getName());
    pnc.ui.Main.breadcrumbPush('Usuarios');
    this.renderUserPane(slider.getElement(), group);

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var callback = function() {
        pnc.ui.Main.breadcrumbPop();
        pnc.ui.Main.breadcrumbPop();
        self.userPaginator.dispose();
        slider.dispose();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    pnc.ui.createToolbarButton(navGroup, 'back-button', pnc.ui.Resources.BACK, 'Regresar', true, callback);
};

/**
 * Renders the pane where the collection of users that are subscribed to a group will be displayed
 * @param  {Element} parent [description]
 * @param  {pnc.model.Group} group  [description]
 * @private
 */
pnc.ui.GroupView.prototype.renderUserPane = function(parent, group) {
    var content = pnc.ui.createBox(parent, 'span7', pnc.ui.Resources.LIST, 'Usuarios');
    content.id  = 'group-pane';
    content.style.minHeight = '494px';
    content.parentElement.style.marginTop = '20px';

    var head    = goog.dom.getElementByClass('box-head', content.parentElement);
    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);
    var backButton     = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton  = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    this.userPaginator = new pnc.ui.Paginator(['Nombre', 'Usuario', ''], [], null, 10, backButton, forwardButton);
    this.userPaginator.setRowRenderer(goog.bind(this.renderUser, this, group));
    this.userPaginator.render(content);
    this.userPaginator.getElement().style.width = '515px';
    var self = this;

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.NOT_FOUND:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var users = response.content();
        self.userPaginator.setData(users);
        self.userPaginator.refresh();
    };
    this.userClient.getInGroup(group, callback);
};

/**
 * Paginator callback. Used to create a representation of a user to place within the paginator.
 * @param  {pnc.model.Group} group   Group for which users are being displayed
 * @param  {Element}         parent  Parent element where the representation will be placed
 * @param  {pnc.model.User}  user    User model to represent
 * @private
 */
pnc.ui.GroupView.prototype.renderUser = function(group, parent, user) {
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);

    var cells = [
        goog.dom.createDom('td', null, user.toString()),
        goog.dom.createDom('td', null, user.getUsername()),
        goog.dom.createDom('td', {'style' : 'width : 30px'})
    ];

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);

    var button = goog.dom.createDom('button', {'class' : 'btn', 'style' : 'padding: 0 6px'});
    goog.dom.appendChild(cells[2], button);
    var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.REMOVE, 'style' : 'width 14px; height: 14px'});
    goog.dom.appendChild(button, img);
    goog.events.listen(button, goog.events.EventType.CLICK, goog.bind(this.removeUser, this, group, user, row));
};

/**
 * Processes the transaction necessary to add a user to a group
 * @param {pnc.model.Group} group  Group the user will be added to
 * @param {pnc.model.User}  user   User to add
 * @private
 */
pnc.ui.GroupView.prototype.addUser = function(group, user) {
    if(!goog.isDefAndNotNull(user))
        return;

    if(goog.array.contains(this.userPaginator.getData(), user)) {
        pnc.ui.Alert.info('El usuario ya es miembro de este grupo');
        return;
    }

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.Alert.error('No se puede agregar el usuario mas de una vez al grupo');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.userPaginator.getData().push(user);
        self.userPaginator.refresh();
        pnc.ui.Alert.success('El usuario fue agregado');
    };
    this.client.addUser(group, user, responseCallback);
};

/**
 * Processes the transaction necessary to remove a user from a group
 * @param  {pnc.model.Group} group  Group the user will be removed from
 * @param  {pnc.model.user}  user   User that will be removed
 * @param  {Element}         row    Row with user representation that was selected
 * @private
 */
pnc.ui.GroupView.prototype.removeUser = function(group, user, row) {
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover a este usuario del group');
    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.userPaginator.getData(), user);
        self.userPaginator.refresh();
        pnc.ui.Alert.success('El usuario fue removido del grupo');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.removeUser(group, user, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a slider that displays all available system privileges, and indicates which have been granted to the currently
 * selected group. This will be used to administer which privileges have been granted (or revoked) from groups.
 * @private
 */
pnc.ui.GroupView.prototype.privileges = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var group     = pnc.model.findModel(this.paginator.getData(), row.dataset.group);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createPrivilegeForm(content, group);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var xhrCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                pnc.ui.Alert.success('Los privilegios del grupo fueron actualizados');
                cancelCallback();
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response));
                return;
        }
    };

    var self = this;
    var saveCallback = function() {
        var grants = self.parsePrivilegeForm();
        self.privClient.sync(group, grants, xhrCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Privilegios', pnc.ui.Resources.PAWN, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush(group.getName());
    pnc.ui.Main.breadcrumbPush('Privilegios');
};

/**
 * Creates a form that will be used to add/edit group data
 * @param  {Element}         parent  Parent element where the form will be placed
 * @param  {pnc.model.Group} group   Optional group. If provided the form will be pre-populated with it's data.
 * @return {Object.<string, Element>}  Map of created form input elements.
 * @private
 */
pnc.ui.GroupView.prototype.createGroupForm = function(parent, group) {
    var inputs = {};

    var makeField = function(key, dom, type, name, message) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'class' : 'span2'}, name);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom(dom, {'id' : key, 'class' : 'span3', 'type' : type});
        goog.dom.appendChild(group, inputs[key]);

        if(goog.isDefAndNotNull(message)) {
            var help = goog.dom.createDom('span', 'help-inline', message);
            goog.dom.appendChild(group, help);
        }
    }

    makeField('group-name', 'input', 'text', 'Nombre', 'No puede estar vacio');
    makeField('group-description', 'textarea', '', 'Descripcion', null);
    inputs['group-description'].style.height = '100px';

    if(goog.isDefAndNotNull(group)) {
        inputs['group-name'].value = group.getName();
        inputs['group-description'].value = group.getDescription();
    }

    return inputs;
};

/**
 * Validates the data in the group form. Any errors found will be displayed inline within the form.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @return {boolean}                          Was the form contents OK?
 * @private
 */
pnc.ui.GroupView.prototype.validateGroupForm = function(inputs) {
    var errors = goog.dom.getElementsByClass('error', inputs['group-name'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(inputs['group-name'].value.length < 1)
        goog.dom.classes.add(inputs['group-name'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['group-name'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the data within a group form and produces a group model from it.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @param  {pnc.model.Group}          group   Optional group. If povided it's details will be updated with form contents.
 * @return {pnc.model.Group}                  Group derived from form.
 * @private
 */
pnc.ui.GroupView.prototype.parseGroupForm = function(inputs, group) {
    group = goog.isDefAndNotNull(group) ? group : new pnc.model.Group();

    group.setName(inputs['group-name'].value);
    group.setDescription(inputs['group-description'].value);

    return group;
};

/**
 * Creates the form that will be used to select which privileges will be granted to a group.
 * @param  {Element}         parent  Parent element where the form will be placed
 * @param  {pnc.model.Group} group   Group for which privileges will be granted/revoked.
 * @private
 */
pnc.ui.GroupView.prototype.createPrivilegeForm = function(parent, group) {
    var content = goog.dom.getElement('priv-form');

    if(goog.isDefAndNotNull(content))
        goog.dom.removeNode(content);

    content = goog.dom.createDom('div', {'id' : 'priv-form', 'class' : 'priv-form'});
    goog.dom.appendChild(parent, content);
    var self = this;

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var privs = response.content();
        var privMap = {};

        for(var cont = 0, size = privs.length; cont < size; cont++) {
            var priv   = privs[cont];
            var subMap = privMap[priv.getModule()];

            if(!goog.isDefAndNotNull(subMap)) {
                subMap = {};
                privMap[priv.getModule()] = subMap;
            }

            var list = subMap[priv.getSubmodule()];

            if(!goog.isDefAndNotNull(list)) {
                list = [];
                subMap[priv.getSubmodule()] = list;
            }

            list.push(priv);
        }

        for(var module in privMap) {
            var moduleLabel = goog.dom.createDom('h3', null, module);
            goog.dom.appendChild(content, moduleLabel);

            var submodulePrivs = privMap[module];

            for(var submodule in submodulePrivs) {
                var submoduleLabel = goog.dom.createDom('h4', null, submodule);
                goog.dom.appendChild(content, submoduleLabel);

                var privContainer = goog.dom.createDom('div');
                goog.dom.appendChild(content, privContainer);
                privs = submodulePrivs[submodule];

                for(var cont = 0, size = privs.length; cont < size; cont++) {
                    var priv = privs[cont];
                    var id = 'priv-' + priv.getId();
                    var checkbox = goog.dom.createDom('input', {'id' : id, 'type' : 'checkbox', 'data-priv' : priv.getId().toString()});
                    goog.dom.appendChild(privContainer, checkbox);
                    var label = goog.dom.createDom('label', {'id' : 'label-' + priv.getId(), 'for' : id}, priv.getName());
                    goog.dom.appendChild(privContainer, label);

                    if(priv.getDependencies().length < 1)
                        continue;

                    var overCallback = function(priv) {
                        for(var cont = 0, size = priv.getDependencies().length; cont < size; cont++) {
                            var id = 'label-' + priv.getDependencies()[cont];
                            var parent = goog.dom.getElement(id);
                            goog.dom.classes.add(parent, 'required');
                        }
                    };
                    goog.events.listen(checkbox, goog.events.EventType.MOUSEOVER, goog.partial(overCallback, priv));
                    goog.events.listen(label,    goog.events.EventType.MOUSEOVER, goog.partial(overCallback, priv));

                    var outCallback = function(priv) {
                        for(var cont = 0, size = priv.getDependencies().length; cont < size; cont++) {
                            var id = 'label-' + priv.getDependencies()[cont];
                            var parent = goog.dom.getElement(id);
                            goog.dom.classes.remove(parent, 'required');
                        }
                    };
                    goog.events.listen(checkbox, goog.events.EventType.MOUSEOUT, goog.partial(outCallback, priv));
                    goog.events.listen(label,    goog.events.EventType.MOUSEOUT, goog.partial(outCallback, priv));

                    var checkCallback = function(checkbox, priv) {
                        if(!checkbox.checked)
                            return;

                        for(var cont = 0, size = priv.getDependencies().length; cont < size; cont++) {
                            var id = 'priv-' + priv.getDependencies()[cont];
                            var parent = goog.dom.getElement(id);
                            parent.checked = true;
                        }
                    };
                    goog.events.listen(checkbox, goog.events.EventType.CLICK, goog.partial(checkCallback, checkbox, priv));
                }
            }
        }

        self.populatePrivilegeForm(group);
    };
    this.privClient.getAll(callback);
};

/**
 * Populates the privilege form. Checks all privileges which have already been granted to the group
 * @param  {pnc.model.Group} group  Group for which privileges will be populated
 * @private
 */
pnc.ui.GroupView.prototype.populatePrivilegeForm = function(group) {
    var content = goog.dom.getElement('priv-form');

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response));
                return;
        }

        var granted = response.content();

        for(var cont = 0, size = granted.length; cont < size; cont++) {
            var priv = goog.dom.getElement('priv-' + granted[cont].getId());

            if(goog.isDefAndNotNull(priv))
                priv.checked = true;
        }
    };
    this.privClient.getGroupGranted(group, callback);
};

/**
 * Parses all privileges which have been selected to be granted.
 * @return {Array.<number>}  IDs of all selected privileges
 * @private
 */
pnc.ui.GroupView.prototype.parsePrivilegeForm = function() {
    var content = goog.dom.getElement('priv-form');
    var privs   = goog.dom.getElementsByTagNameAndClass('input', null, content);
    var granted = [];

    for(var cont = 0, size = privs.length; cont < size; cont++) {
        var priv = privs[cont];

        if(priv.type !== 'checkbox' || !priv.checked)
            continue

        granted.push(goog.string.toNumber(priv.dataset.priv));
    }

    return granted;
};
