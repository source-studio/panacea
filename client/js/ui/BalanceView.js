goog.provide('pnc.ui.BalanceView');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.string');
goog.require('goog.ui.MenuItem');
goog.require('goog.ui.Select');

goog.require('pnc.client.Balance');
goog.require('pnc.client.Profile');
goog.require('pnc.client.User');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');

/**
 * View that allows a user to search balances which have already been processed.
 * @constructor
 */
pnc.ui.BalanceView = function() {
    /**
     * Collection of existing users
     * @type {Array.<pnc.model.Users>}
     * @private
     */
    this.users = [];

    /**
     * The last balance that was created, if it exsits
     * @type {pnc.model.Balance}
     * @private
     */
    this.lastBalance = null;

    /**
     * Client used to make balance service requests
     * @type {pnc.client.Balance}
     * @private
     */
    this.client = new pnc.client.Balance();

    //------------- elements -------------//
    /** @private */ this.container   = null;
    /** @private */ this.disposables = [];
    /** @private */ this.paginator   = null;
};
pnc.ui.Main.registerModule('balances', pnc.ui.BalanceView);


/**
 * Renders the balance view
 * @param  {Element} parent  Parent element where the view will be placed
 */
pnc.ui.BalanceView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.getLastBalance();
    this.renderSearchPane();
    this.renderBalancePane();
    this.renderDetailPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Balances');
};

/**
 * Cleans up resources and removes the view from the DOM
 */
pnc.ui.BalanceView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};

/**
 * Executes the transaction to retrieve the last balance from the server, if one exists
 * @private
 */
pnc.ui.BalanceView.prototype.getLastBalance = function() {
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                self.lastBalance = null;
                break;

            case pnc.client.Status.OK:
                self.lastBalance = response.content();
                break;

            default:
                pnc.ui.Alert.error('No se pudo recuperar informacion sobre el ultimo balance');
                break;
        }
    };
    var client = new pnc.client.Balance();
    client.getLast(callback);
};

/**
 * Renders the search pane. This is where the user inserts search criteria to look up balances
 * @private
 */
pnc.ui.BalanceView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var fromLabel = goog.dom.createDom('label', 'span1', 'Desde');
    goog.dom.appendChild(content, fromLabel);
    var fromInput = goog.dom.createDom('input', {'id' : 'from-input', 'type' : 'date', 'class' : 'span2'});
    goog.dom.appendChild(content, fromInput);

    var toLabel = goog.dom.createDom('label', 'span1', 'Hasta');
    goog.dom.appendChild(content, toLabel);
    var toInput = goog.dom.createDom('input', {'id' : 'to-input', 'class' : 'span2', 'type' : 'date'});
    goog.dom.appendChild(content, toInput);

    var userLabel = goog.dom.createDom('label', 'span1', 'Usuario');
    goog.dom.appendChild(content, userLabel);
    var userSelect = new goog.ui.Select();
    userSelect.render(content);
    userSelect.setEnabled(false);
    this.disposables.push(userSelect);

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Busqueda');
    goog.dom.appendChild(controlPane, searchButton);

    var self = this;
    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateBalancePane(response.content());
    };

    var clickCallback = function() { 
        if(fromInput.value.length < 1)
            goog.dom.classes.add(fromInput, 'validation-error');

        if(toInput.value.length < 1)
            goog.dom.classes.add(toInput, 'validation-error');

        if(fromInput.value.length < 1 || toInput.value.length < 1)
            return;

        goog.dom.classes.remove(fromInput, 'validation-error');
        goog.dom.classes.remove(toInput, 'validation-error');
        var from = new Date();
        var to   = new Date(); 
        pnc.client.DATE_PARSER.parse(fromInput.value, from);
        pnc.client.DATE_PARSER.parse(toInput.value, to);

        self.client.search(from, to, userSelect.getValue(), searchCallback); 
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);

    var userCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.users = response.content();

        for(var cont = 0, size = self.users.length; cont < size; cont++) {
            var user = self.users[cont];

            if(!user.isEnabled())
                continue;

            var item = new goog.ui.MenuItem(user.toString());
            userSelect.addItem(item);
            item.setValue(user.getId());
        }

        userSelect.setEnabled(true);
    };

    var userClient = new pnc.client.User();
    userClient.getAll(userCallback);
};

/**
 * Renders the balance pane. This is where the user sees the balance objects that were retrieved by the search function.
 * Can inspect detail, and actions if the context permits.
 * @private
 */
pnc.ui.BalanceView.prototype.renderBalancePane = function() {
    var content             = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Balances');
    content.id              = 'balance-pane';
    content.style.minHeight = '390px';
    var head                = goog.dom.getElementByClass('box-head', content.parentElement);

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);

    var createButton = function(id, src, title, callback) {
        var button = goog.dom.createDom('button', {'id' : id, 'class' : 'btn'});
        goog.dom.appendChild(toolbar, button);
        button.disabled = true;
        var img         = goog.dom.createDom('img', {'src' : src, 'title' : title});
        goog.dom.appendChild(button, img);

        if(goog.isDefAndNotNull(callback))
            goog.events.listen(button, goog.events.EventType.CLICK, callback);

        return button;
    };

    var backButton    = createButton('back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras');
    var forwardButton = createButton('forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante');

    var self = this;
    var forwardCallback = function(e) {
        var result             = self.paginator.next();
        backButton.disabled    = false;
        forwardButton.disabled = !result;
        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, forwardCallback);

    var backCallback = function(e) {
        var result             = self.paginator.previous();
        backButton.disabled    = !result;
        forwardButton.disabled = false;
        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
    };
    goog.events.listen(backButton, goog.events.EventType.CLICK, backCallback);
};

/**
 * Renders the detail pane. The detail pane will show further detail on a balance when clicked on. It will also expose a
 * undo option, but only if the user has the required privilege, and if it's the last created balance.
 * @private
 */
pnc.ui.BalanceView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '285px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 2, 'Balance');
    body.id  = 'detail-table';
};

/**
 * Populates the balance pane. Used to populate with content when the search request receives a response.
 * @param  {Array.<pnc.model.Balance>} balances  Balances to populate in the view
 * @private
 */
pnc.ui.BalanceView.prototype.populateBalancePane = function(balances) {
    var content       = goog.dom.getElement('balance-pane');
    var backButton    = goog.dom.getElement('back-button');
    var forwardButton = goog.dom.getElement('forward-button');
    var detailTable   = goog.dom.getElement('detail-table');
    var undoButton    = goog.dom.getElementsByTagNameAndClass('button', null, detailTable.parentElement);

    goog.dom.removeChildren(detailTable);

    if(goog.isDefAndNotNull(undoButton))
        goog.dom.removeNode(undoButton);

    backButton.disabled    = true;
    forwardButton.disabled = goog.isDefAndNotNull(balances) && balances.length <= pnc.ui.PAGE_SIZE;        
    
    if(goog.isDefAndNotNull(this.paginator))
        this.paginator.dispose();

    var titles = ['Fecha/Hora', 'Usuario', 'Balance', 'Diferencia'];
    this.paginator = new pnc.ui.Paginator(titles, balances, null, pnc.ui.PAGE_SIZE);
    this.paginator.setRowRenderer(goog.bind(this.renderBalance, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Callback function used by the paginator to determine how an individual balance will be rendered within the paginator.
 * @param  {Element}           parent   Parent element where the balance will be rendered
 * @param  {pnc.model.Balance} balance  Balance object to represent
 * @private
 */
pnc.ui.BalanceView.prototype.renderBalance = function(parent, balance) {
    var row = goog.dom.createDom('tr', {'data-balance' : balance.getId()});
    goog.dom.appendChild(parent, row);

    var self = this;
    var callback = function(balance, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');
        self.populateDetailPane(balance, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, balance));

    var cells = [];
    cells[0]  = goog.dom.createDom('td', {'style' : 'width: 140px'}, pnc.ui.DATE_TIME_FORMATTER.format(balance.getTimestamp()));
    cells[1]  = goog.dom.createDom('td', {}, pnc.model.findModel(this.users, balance.getIdUser()).toString());
    cells[2]  = goog.dom.createDom('td', {'style' : 'text-align: right'}, pnc.ui.DECIMAL_FORMATTER.format(balance.getBalance()));
    cells[3]  = goog.dom.createDom('td', {'style' : 'text-align: right'}, pnc.ui.DECIMAL_FORMATTER.format(balance.getDifference()));

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Populates the detail pane with content from a balance.
 * @param  {pnc.model.Balance} balance  Balance object to represent
 * @param  {Element}           row      Row that is selected in the paginator.
 * @private
 */
pnc.ui.BalanceView.prototype.populateDetailPane = function(balance, row) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);
    var button = goog.dom.getElementsByTagNameAndClass('button', null, body.parentElement.parentElement)[0];

    if(goog.isDefAndNotNull(button))
        goog.dom.removeNode(button);

    pnc.ui.createSummaryContent(body, 'Inicio',         balance.getStart());
    pnc.ui.createSummaryContent(body, 'Ventas Contado', balance.getCashSales());
    pnc.ui.createSummaryContent(body, 'Ventas Credito', balance.getCreditSales());
    pnc.ui.createSummaryContent(body, 'Devolucion',     balance.getRefunds());
    pnc.ui.createSummaryContent(body, 'Pagos',          balance.getPayments());
    pnc.ui.createSummaryContent(body, 'Gastos',         balance.getExpenses());
    pnc.ui.createSummaryContent(body, 'Depositos',      balance.getDeposits());
    pnc.ui.createSummaryContent(body, 'Proximo Inicio', balance.getNextStart());
    pnc.ui.createSummaryContent(body, 'Notas',          balance.getNotes());
    
    var self  = this;
    var removeCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                pnc.ui.Alert.success('El balance fue deshecho');
                goog.array.remove(self.paginator.getData(), balance);
                goog.dom.removeNode(row);
                goog.dom.removeChildren(body);
                goog.dom.removeNode(button);
                break;

            case pnc.client.Status.CONFLICT:
                pnc.ui.SlideDialog.error('Solo se puede deshacer el ultimo balance').open();
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                break;
        }
    };

    var privs = pnc.client.Profile.getPrivileges();
    if(goog.array.contains(privs, pnc.client.Privileges.BALANCE_REMOVE)) {
        if(goog.isDefAndNotNull(this.lastBalance) && balance.getId() === this.lastBalance.getId()) {
            button = goog.dom.createDom('button', {'class' : 'btn btn-danger', 'style' : 'margin: 10px; width: 195px'}, 'Deshacer Balance');
            goog.dom.appendChild(body.parentElement.parentElement, button);
            var callback = goog.bind(this.client.remove, this.client, balance, removeCallback);
            goog.events.listen(button, goog.events.EventType.CLICK, callback);
        }
    }
};

