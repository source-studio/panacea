
goog.provide('pnc.ui.Login');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.math');
goog.require('goog.net.XhrIo');
goog.require('goog.object');

goog.require('pnc.client.Profile');
goog.require('pnc.model.Link');
goog.require('pnc.model.User');

/**
 * Login functionality
 * @class
 * @static
 */
pnc.ui.Login = {};

/**
 * Collection of messages to display in the hero subtitle
 * @type {Array.<string>}
 * @private
 */
pnc.ui.Login.Messages = [
    'Una vaina bien',
    'No tiene acentos, pero si tiene eñe',
    'Porque anotar en una mascota esta medio pendejo',
    'Apoyando la haraganeria',
    'Haciendo que tu vida sea un chin menos jodona',
    'Cuidado si todavia estan usando la mascota...',
    'Nadie se recuerda del pendejo que escribio esto',
    'Feliz Navidad (aunque no estemos en diciembre)',
    'No es un conflicto de intereses que una doctora tambien tenga una farmacia?',
    'Y hasta la cuanta es?',
    'Disculpandose por los macos',
    'Porque el software es como el arte: nunca se termina solo se abandona',
    'Donde se consigue una buena fritura a esta hora?',
    'Yo no cojo esa que los medicamentos genericos son iguales a los de marca',
    'Y porque en la farmacia se venden prendas mala clase?',
    'Recuerda no mirar raro a los clientes que pidan condones o viagra',
    'Te va mejor ahora? Apuesto que manejar este negocio como quiera es un lio',
    'Deberian hacer una novela del chisme entre empleadas de una farmacia',
    'Imaginate si aprovecharas todas las horas muertas aqui en algo productivo',
    'La gallina fue primero que el huevo. Fuera de coro',
    'Por que puse este subtitulo asi? Hay que tener sentido del humor',
    'Na\' e\' na\', lo que vale es la persona',
    'Si la farmacia vende alcohol isopropilico, porque no puede vender romo?',
    'Mal, pero tu no tienes la culpa',
    'Y es que ustedes no se van a hartar de la bachata nunca?',
    'Vender "medicina" para la gripe es una charlataneria',
    'Apuesto a que si esto esa usara en un iPhone tu estuvieras mas interesada',
    'Todavia no imprime o escanea. Fue que la doctora se desespero',
    'Porque la juventud esta acabando con el mundo desde el año 200 AD',
    'De una vez por todas acepten que la pelota no es un deporte de verdad',
    'Sabias que las piscinas son mas peligrosas que las armas de fuego?',
    'Por que las farmacias no tienen empleados masculinos?',
    'Porque Facebook es una perdida de tiempo',
    'Por favor: Menos telenovela y mas lectura',
    'Saludo amiga, saludo amigo. Saludo vecina, saludo vecino',
    'La vida se vive mejor cuando te acostumbras a bajar el rabo'
];

/**
 * Registers the login functionality on the login page
 */
pnc.ui.Login.register = function() {
    pnc.client.Profile.init();
    pnc.ui.Login.insertMessage();
    pnc.ui.Login.registerCleanup();
    pnc.ui.Login.handleLogin();

    var box = goog.dom.getElement('login-user');
    
    if(goog.isDefAndNotNull(box))
        box.focus();
};
goog.exportSymbol('pnc.ui.Login.register', pnc.ui.Login.register);

/**
 * Processes the login transaction and handles the response
 * @private
 */
pnc.ui.Login.handleLogin = function() {
    var userText = goog.dom.getElement('login-user');
    var passText = goog.dom.getElement('login-password');
    var form     = goog.dom.getElement('login-form');

    var xhrCallback = function(e) {
        switch(e.target.getStatus()) {
            case pnc.client.Status.OK:
                pnc.ui.Login.registerProfile(e.target.getResponseText());
                document.location.href = 'main.html#';
                break;

            case pnc.client.Status.INTERNAL_SERVER_ERROR:
                pnc.ui.SlideDialog.error('Error al tratar de entrar a la aplicacion').open();
                break;

            case pnc.client.Status.FORBIDDEN:
                pnc.ui.SlideDialog.error('Esta cuenta de usuario ha sido deshabilitada').open();
                break;

            case pnc.client.Status.NOT_FOUND:
                pnc.ui.SlideDialog.error('No se encuentra ninguna cuenta de usuario con el nombre proporcionado').open();
                break;

            case pnc.client.Status.UNAUTHORIZED:
                pnc.ui.SlideDialog.error('La contraseña proporcionada no es correcta').open();
                break;

            default:
                var error = pnc.client.decodeStatus(e.target.getStatus());
                pnc.ui.SlideDialog.error(error).open();
                break;
        }
    };

    var submitCallback = function(e) {
        e.preventDefault();

        var username = userText.value.toLowerCase();
        var password = passText.value;
        var text     = null;

        if(username.length < 1 || password.length < 1) {
            pnc.ui.SlideDialog.error('Debe entrar usuario y contraseña para poder entrar').open();
            return;
        }

        var postdata = ['username=', username, '&password=', password].join('');
        goog.net.XhrIo.send('/auth/session', xhrCallback, pnc.client.Method.POST, postdata);
    };
    goog.events.listen(form, goog.events.EventType.SUBMIT, submitCallback);
};


/**
 * Inserts a random message from the Messages array into the hero subtitle
 * @private
 */
pnc.ui.Login.insertMessage = function() {
    var hero    = goog.dom.getElement('hero');
    var message = goog.dom.getElementsByTagNameAndClass('h2', null, hero)[0];
    var index   = goog.math.randomInt(pnc.ui.Login.Messages.length);
    goog.dom.setTextContent(message, pnc.ui.Login.Messages[index]);
};

pnc.ui.Login.registerProfile = function(resp) {
    var job        = goog.json.unsafeParse(resp);
    var rel        = goog.object.getKeys(job.user)[0];
    var user       = pnc.client.User.decode(rel, job.user[rel].links, job.user[rel].model);
    var privs      = [];

    for(var rel in job.privileges)
        privs.push(pnc.client.Privilege.decode(rel, [], job.privileges[rel].model));

    pnc.client.Profile.setUser(user);
    pnc.client.Profile.setPrivileges(privs);
    var linkModels = {};
    
    for(var cont = 0, size = job.links.length; cont < size; cont++) {
        var linkJob = job.links[cont];
        linkModels[linkJob.Rel] = new pnc.model.Link(linkJob.ContentType, linkJob.Rel, linkJob.Method, linkJob.Uri);
    }
    
    pnc.client.Profile.setLinks(linkModels);
};

pnc.ui.Login.registerCleanup = function() {
    var callback = function() {
        goog.net.XhrIo.cleanup();
        goog.events.removeAll();
    };

    goog.events.listenOnce(window, goog.events.EventType.UNLOAD, callback);
};
