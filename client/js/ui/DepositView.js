goog.provide('pnc.ui.DepositView');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.string');
goog.require('goog.ui.MenuItem');
goog.require('goog.ui.Select');

goog.require('pnc.client.Balance');
goog.require('pnc.client.Deposit');
goog.require('pnc.client.User');
goog.require('pnc.model.Balance');
goog.require('pnc.model.Deposit');
goog.require('pnc.ui');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.Paginator');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');

/**
 * View that lets the user view and modify deposit data
 * @constructor
 */
pnc.ui.DepositView = function() {
    /**
     * The last balance that was created, if such exists
     * @type {pnc.model.Balance}
     * @private
     */
    this.lastBalance = null;

    /**
     * Collection of all existing enabled users
     * @type {Array.<pnc.model.User>}
     * @private
     */
    this.users        = null;

    /**
     * Client used to make deposit related server transactions
     * @type {pnc.client.Deposit}
     * @private
     */
    this.client      = new pnc.client.Deposit();

    //----------- elements -----------//
    this.container   = null;
    this.disposables = [];
    this.paginator   = null;
};
pnc.ui.Main.registerModule('deposits', pnc.ui.DepositView);


/**
 * Renders the deposit view
 * @param  {Element} parent  Parent elements where the deposit view will be placed in
 */
pnc.ui.DepositView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left : 0px'});
    goog.dom.appendChild(parent, this.container);

    this.getLastBalance();
    this.renderSearchPane();
    this.renderActionPane();
    this.renderDepositPane();
    this.renderDetailPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Depositos');
};

/**
 * Disposes the view and releases any used resources
 */
pnc.ui.DepositView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};

/**
 * Performs a server transaction to get the last balance that was made
 * @private
 */
pnc.ui.DepositView.prototype.getLastBalance = function() {
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                self.balance = null;
                break;

            case pnc.client.Status.OK:
                self.lastBalance = response.content();
                break;

            default:
                pnc.ui.Alert.error('No se pudo recuperar informacion sobre el ultimo balance');
                break;
        }
    };
    var client = new pnc.client.Balance();
    client.getLast(callback);
};

/**
 * Renders the pane that allows the user to search deposits by date and optionally user.
 * @return {private}
 */
pnc.ui.DepositView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var fromLabel = goog.dom.createDom('label', 'span1 bold', 'Desde');
    goog.dom.appendChild(content, fromLabel);
    var fromInput = goog.dom.createDom('input', {'id' : 'from-input', 'type' : 'date', 'class' : 'span2'});
    goog.dom.appendChild(content, fromInput);

    var toLabel = goog.dom.createDom('label', 'span1 bold', 'Hasta');
    goog.dom.appendChild(content, toLabel);
    var toInput = goog.dom.createDom('input', {'id' : 'to-input', 'type' : 'date', 'class' : 'span2'});
    goog.dom.appendChild(content, toInput);

    var userLabel = goog.dom.createDom('label', 'span1', 'Usuario');
    goog.dom.appendChild(content, userLabel);
    var userSelect = new goog.ui.Select();
    userSelect.render(content);
    userSelect.setEnabled(false);
    this.disposables.push(userSelect);

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Busqueda');
    goog.dom.appendChild(controlPane, searchButton);

    var self = this;
    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateDepositPane(response.content());
    };

    var clickCallback = function() {
        if(fromInput.value.length < 1)
            goog.dom.classes.add(fromInput, 'validation-error');

        if(toInput.value.length < 1)
            goog.dom.classes.add(toInput, 'validation-error');

        if(fromInput.value.length < 1 || toInput.value.length < 1)
            return;

        goog.dom.classes.remove(fromInput, 'validation-error');
        goog.dom.classes.remove(toInput,   'validation-error');
        var from = new Date();
        var to   = new Date();
        pnc.client.DATE_PARSER.parse(fromInput.value, from);
        pnc.client.DATE_PARSER.parse(toInput.value,   to);

        self.client.search(from, to, userSelect.getValue(), searchCallback);
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);

    var userCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.users = response.content();

        for(var cont = 0, size = self.users.length; cont < size; cont++) {
            var user = self.users[cont];

            if(!user.isEnabled())
                continue;

            var item = new goog.ui.MenuItem(user.toString());
            userSelect.addItem(item);
            item.setValue(user.getId());
        }

        userSelect.setEnabled(true);
    };
    var userClient = new pnc.client.User();
    userClient.getAll(userCallback);
};

/**
 * Renders the pane that allows the user to create new deposits
 * @private
 */
pnc.ui.DepositView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.DEPOSIT_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));

    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Deposito');
    goog.dom.appendChild(addButton, span);
};

/**
 * Renders the pane that shows all deposits that match the search criteria
 * @private
 */
pnc.ui.DepositView.prototype.renderDepositPane = function() {
    var content             = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Depositos');
    content.id              = 'deposit-pane';
    content.style.minHeight = '390px';

    var head    = goog.dom.getElementsByClass('box-head', content.parentElement)[0];
    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var backButton, forwardButton, updateButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.DEPOSIT_UPDATE))
        updateButton = pnc.ui.createToolbarButton(actionGroup, 'update-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.update, this));

    backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT, 'Atras', false);
    forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var self = this;
    var navCallbck = function(e) {
        if(!goog.isNull(updateButton))
            updateButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
    };
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallbck);
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallbck);

    var hasPriv = goog.array.contains(privs, pnc.client.Privileges.DEPOSIT_REMOVE);
    var titles  = ['Fecha/Hora', 'Usuario', 'Cantidad', ''];
    this.paginator = new pnc.ui.Paginator(titles, [], null, pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderDeposit, this, hasPriv));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Renders the pane that providers additional detail on a deposit
 * @private
 */
pnc.ui.DepositView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '142px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-condensed', 2, 'Deposito');
    body.id = 'detail-table';
};

/**
 * Populates the deposit pane with search results
 * @param  {Array.<pnc.model.Desposit>} deposits  Deposits that were retrieved by the search operation
 * @private
 */
pnc.ui.DepositView.prototype.populateDepositPane = function(deposits) {
    var content      = goog.dom.getElement('deposit-pane');
    var updateButton = goog.dom.getElement('update-button');
    var detailTable  = goog.dom.getElement('detail-table');

    goog.dom.removeChildren(detailTable);

    if(goog.isDefAndNotNull(updateButton))
        updateButton.disabled = true;

    this.paginator.setData(deposits);
    this.paginator.refresh();
};

/**
 * Paginator callback. used to create a row representation of a deposit model.
 * @param  {Boolean}           hasPriv  Does the current user have the delete deposit privilege?
 * @param  {Element}           parent   Parent element where the deposit representation will be placed
 * @param  {model.pnc.Deposit} deposit  Deposito model to represent
 * @private
 */
pnc.ui.DepositView.prototype.renderDeposit = function(hasPriv, parent, deposit) {
    var row = goog.dom.createDom('tr', {'data-deposit' : deposit.getId()});
    goog.dom.appendChild(parent, row);
    var balanced = goog.isDefAndNotNull(this.lastBalance) && deposit.getTimestamp() < this.lastBalance.getTimestamp();
    var included = deposit.isIncludedInBalance();

    var self = this;
    var callback = function(deposit, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', 'info', parent);
        var updateButton = goog.dom.getElement('update-button');

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        if(goog.isDefAndNotNull(updateButton))
            updateButton.disabled = included && balanced;

        self.populateDetailPane(deposit, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, deposit));

    var cells = [];
    cells[0] = goog.dom.createDom('td', null, pnc.ui.DATE_FORMATTER.format(deposit.getTimestamp()));
    cells[1] = goog.dom.createDom('td', null, pnc.model.findModel(this.users, deposit.getIdUser()).toString());
    cells[2] = goog.dom.createDom('td', {'style' : 'text-align : right'}, pnc.ui.DECIMAL_FORMATTER.format(deposit.getAmount()));
    cells[3] = goog.dom.createDom('td', {'style' : 'width : 27px'});

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);

    if(hasPriv && (!included || (included && !balanced))) {
        var removeButton = goog.dom.createDom('button', {'class' : 'btn', 'style' : 'padding: 0 6px'});
        goog.dom.appendChild(cells[3], removeButton);
        var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.REMOVE, 'title' : 'Remover Deposito', 
                    'style' : 'width: 14px; height: 14px'});
        goog.dom.appendChild(removeButton, img);
        goog.events.listen(removeButton, goog.events.EventType.CLICK, goog.bind(this.remove, this, deposit, row));
    }
};

/**
 * Populates the detail pane with additional info about the deposit which is selected by the user
 * @param  {pnc.model.Deposit} deposit  Deposit selected
 * @param  {Element}           row      Row element the user clicked on
 * @private
 */
pnc.ui.DepositView.prototype.populateDetailPane = function(deposit, row) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);
    var existing = goog.dom.getElementsByTagNameAndClass('button', null, body);
    var self     = this;

    if(goog.isDefAndNotNull(existing))
        goog.dom.removeNode(existing);

    if(!goog.isDefAndNotNull(deposit))
        return;

    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(body, row);
    var cell = goog.dom.createDom('td', 'bold', 'En Balance');
    goog.dom.appendChild(row, cell);
    cell = goog.dom.createDom('td', null, deposit.isIncludedInBalance() ? 'Si' : 'No');
    goog.dom.appendChild(row, cell);
    row = goog.dom.createDom('tr');
    goog.dom.appendChild(body, row);
    cell = goog.dom.createDom('td', {'class' : 'bold', 'style' : 'text-align : center', 'colspan' : '2'}, 'Razon');
    goog.dom.appendChild(row, cell);
    row = goog.dom.createDom('tr');
    goog.dom.appendChild(body, row);
    var reason = goog.isDefAndNotNull(deposit.getReason()) ? deposit.getReason() : '';
    cell = goog.dom.createDom('td', {'style' : 'text-align : left', 'colspan' : '2'}, goog.string.truncate(reason, 350));
    goog.dom.appendChild(row, cell);
};

/**
 * Creates a form that will let the user add a new deposit
 * @private
 */
pnc.ui.DepositView.prototype.add = function() {
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createDepositForm(content, null);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.CREATED:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.paginator.getData().push(response.content());
        goog.array.sort(self.paginator.getData(), pnc.model.Deposit.compare);
        self.paginator.refresh();
        self.populateDetailPane(null);
        cancelCallback();
        pnc.ui.Alert.success('El deposito fue agregado');
    };

    var saveCallback = function() {
        if(!self.validateDepositForm(inputs))
            return;

        var deposit = self.parseDepositForm(inputs);
        self.client.add(deposit, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Creates a form that will let the user update an existing deposit. Deposits can only be updated if they have not been
 * included in a balance. Only the amaount and reason properties can be updated
 * @private
 */
pnc.ui.DepositView.prototype.update = function() {
    var row       = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var deposit   = pnc.model.findModel(this.paginator.getData(), row.dataset.deposit);
    var addButton = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(addButton))
        addButton.disabled = true;

    var content = goog.dom.createDom('div');
    var inputs  = this.createDepositForm(content, deposit);
    var actions = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, actions);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(actions, cancelButton);

    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(addButton))
            addButton.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(actions, saveButton);

    var self = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        var newDeposit = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newDeposit);
        self.paginator.refresh();
        self.populateDetailPane(newDeposit, row);
        cancelCallback();
        pnc.ui.Alert.success('El deposito fue actualizado');

        var updateButton = goog.dom.getElement('update-button');

        if(goog.isDefAndNotNull(updateButton))
            updateButton.disabled = true;
    };

    var saveCallback = function() {
        if(!self.validateDepositForm(inputs))
            return;

        var newDeposit = self.parseDepositForm(inputs, deposit);
        newDeposit.setId(deposit.getId());
        self.client.update(newDeposit, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Creates a dialog that asks the user to confirm to th delete operation. If the user accepts initializes a delete 
 * transaction.
 * @param  {pnc.model.Deposit} deposit  Deposit to delete
 * @param  {Element}           row      Row the user clicked on
 * @private
 */
pnc.ui.DepositView.prototype.remove = function(deposit, row) {
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea eliminar este deposito?');
    var self   = this;
    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), deposit);
        self.paginator.refresh();
        self.populateDetailPane(null);
        pnc.ui.Alert.success('El deposito fue removido');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.remove(deposit, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a form that will let the user create or update a deposit.
 * @param  {Element}           parent   Parent element where the form will be placed
 * @param  {pnc.model.Deposit} deposit  Optional deposit object. If provided the form will be pre-populated with its data.
 * @return {Object.<string, Element>}   Generated form input elmeents
 */
pnc.ui.DepositView.prototype.createDepositForm = function(parent, deposit) {
    var inputs = {};
    var group, label, value = null;

    if(goog.isDefAndNotNull(deposit)) {
        group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);
        label = goog.dom.createDom('label', 'span2', 'Fecha / Hora');
        goog.dom.appendChild(group, label);
        value = goog.dom.createDom('label', {'class' : 'span2', 'style' : 'margin-left: 0'}, 
                pnc.ui.DATE_TIME_FORMATTER.format(deposit.getTimestamp()));
        goog.dom.appendChild(group, value);

        group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);
        label = goog.dom.createDom('label', 'span2', 'Usuario');
        goog.dom.appendChild(group, label);
        var user = pnc.model.findModel(this.users, deposit.getIdUser());
        var userStr = goog.isDefAndNotNull(user) ? user.toString() :'';
        value = goog.dom.createDom('label', {'class' : 'span2', 'style' : 'margin-left : 0'}, userStr);
        goog.dom.appendChild(group, value);
    }

    group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    label = goog.dom.createDom('label', {'for' : 'deposit-include', 'class' : 'span2'}, 'Incluido En Balance');
    goog.dom.appendChild(group, label);

    if(!goog.isDefAndNotNull(deposit) || deposit.getTimestamp() > this.lastBalance.getTimestamp()) {
        inputs['deposit-include'] = goog.dom.createDom('input', {'id' : 'deposit-include', 'type' : 'checkbox',
                                    'style' : 'width: 16px; height: 16px; vertical-align: baseline'});
        goog.dom.appendChild(group, inputs['deposit-include']);
    } else if(goog.isDefAndNotNull(deposit)) {
        label = goog.dom.createDom('label', {'id' : 'deposit-include', 'class' : 'span2', 'style' : 'margin-left : 0'}, 
                    deposit.isIncludedInBalance() ? 'Si' : 'No');
        goog.dom.appendChild(group, label);
    }

    group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    label = goog.dom.createDom('label', {'for' : 'deposit-amount', 'class' : 'span2'}, 'Cantidad');
    goog.dom.appendChild(group, label);
    inputs['deposit-amount'] = goog.dom.createDom('input', {'id' : 'deposit-amount', 'class' : 'span2', 'type' : 'text'});
    goog.dom.appendChild(group, inputs['deposit-amount']);
    pnc.ui.assureNumbers(inputs['deposit-amount']);
    var help = goog.dom.createDom('span', 'help-inline', 'Debe ser un numbero');
    goog.dom.appendChild(group, help);

    group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    label = goog.dom.createDom('label', {'for' : 'deposit-reason', 'class' : 'span2'}, 'Razon');
    goog.dom.appendChild(group, label);
    inputs['deposit-reason'] = goog.dom.createDom('textarea', {'id' : 'deposit-reason', 'class' : 'span4', 'style' : 'height : 100px'});
    goog.dom.appendChild(group, inputs['deposit-reason']);

    if(goog.isDefAndNotNull(deposit)) {
        if(goog.isDefAndNotNull(inputs['deposit-include']))
            inputs['deposit-include'].checked = deposit.isIncludedInBalance();

        inputs['deposit-amount'].value = pnc.ui.DECIMAL_FORMATTER.format(deposit.getAmount());
        inputs['deposit-reason'].value = goog.isDefAndNotNull(deposit.getReason()) ? deposit.getReason() : '';
    }

    return inputs;
};

/**
 * Checks the input values in the deposit form. If errors are found, they are highlighted inline.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @return {boolean}                          Is the form content valid?
 * @private
 */
pnc.ui.DepositView.prototype.validateDepositForm = function(inputs) {
    var errors   = goog.dom.getElementsByClass('error', inputs['deposit-amount'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(!pnc.ui.FLOAT_RGX.test(inputs['deposit-amount'].value))
        goog.dom.classes.add(inputs['deposit-amount'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['deposit-amount'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Reads the contnet of the deposit form and creates an deposit object from it.
 * @param  {Object.<string, Element>} inputs   Form input elements
 * @param  {pnc.model.Deposit}        deposit  Optional deposit model. If provided its data will be used to pre-populate
 *                                              the new deposit
 * @return {pnc.model.Desposit}                Deposit model derived from form.
 * @private
 */
pnc.ui.DepositView.prototype.parseDepositForm = function(inputs, deposit) {
    var newDeposit = goog.isDefAndNotNull(deposit) ? deposit : new pnc.model.Deposit();
    newDeposit.setAmount(pnc.ui.round(inputs['deposit-amount'].value));
    newDeposit.setReason(inputs['deposit-reason'].value);

    if(goog.isDefAndNotNull(inputs['deposit-include']))
        newDeposit.setIncludedInBalance(inputs['deposit-include'].checked);

    if(!goog.isDefAndNotNull(deposit)) {
        newDeposit.setIdUser(pnc.client.Profile.getUser().getId());
    } else {
        newDeposit.setId(deposit.getId());
        newDeposit.setIdUser(deposit.getIdUser());
        newDeposit.setTimestamp(deposit.getTimestamp());
    }

    return newDeposit;
};
