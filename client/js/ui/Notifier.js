goog.provide('pnc.ui.Notifier');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');
goog.require('goog.net.WebSocket');

goog.require('pnc.client.Notification');
goog.require('pnc.client.Product');
goog.require('pnc.model.Notification');
goog.require('pnc.ui.Alert');

/**
 * User interface element that displays notifications to the user. The user will get a queue of all pending notifications
 * on login, and then new notifications will be delivered in quasi-real time as they are generated via websockets. The
 * user can dismiss notifications at any time.
 * @constructor
 */
pnc.ui.Notifier = function() {
    this.button        = null;
    this.notifications = [];
    this.client        = new pnc.client.Notification();
    this.productClient = new pnc.client.Product();
    this.productCache  = [];
};


/**
 * Renders the notifier. The initial state is simply a button. However, when pressed, the notification pane will be
 * displayed, which will show notification content.
 * @param  {Element} parent  Parent element where the notification button will be placed
 */
pnc.ui.Notifier.prototype.render = function(parent) {
    this.button = goog.dom.createDom('button', {'class' : 'btn btn-inverse', 'disabled' : 'true', 'style' : 'min-width: 66px'});
    goog.dom.appendChild(parent, this.button);
    goog.events.listen(this.button, goog.events.EventType.CLICK, goog.bind(this.toggle, this));
    var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LIGHT_BULB, 'title' : 'Notificaciones'});
    goog.dom.appendChild(this.button, img);
    this.initSocket();
};

/**
 * Disposes of the notifier and closes the websocket.
 */
pnc.ui.Notifier.prototype.dispose = function() {
    goog.dom.removeNode(this.button);
    this.client.disconnect();
};


/**
 * Initializes the web socket that gets new notifications
 * @private
 */
pnc.ui.Notifier.prototype.initSocket = function() {
    var self = this;
    var decoder = goog.partial(pnc.client.decode, pnc.client.Notification.decode);
    var callback = function(e) {
        var notification = decoder(e.message);
        self.notifications.push(notification);
        self.updateBadge();
    };
    goog.events.listen(this.client.socket, goog.net.WebSocket.EventType.MESSAGE, callback);

    var callback = function(e) {
        var existing = goog.dom.getElementsByClass('notification-error');

        if(existing.length > 0)
            return;

        var alert = pnc.ui.Alert.error('Error al tratar de iniciar comunicacion con servicio de notificaciones');
        goog.dom.classes.add(alert, 'notification-error');
    };
    goog.events.listen(this.client.socket, goog.net.WebSocket.EventType.ERROR, callback);

    this.client.connect();
}

/**
 * Retrieves the current notification queue for the user
 * @private
 */
pnc.ui.Notifier.prototype.update = function() {
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.NOT_FOUND:
                break;

            default:
                pnc.ui.Alert.error(pnc.client.decodeStatus(response));
                return;
        }

        self.button.disabled = false;
        self.notifications = goog.isDefAndNotNull(response.content()) ? response.content() : [];
        self.updateBadge();
    };
    this.client.getByUser(callback);
};

/**
 * Updates the badge icon. The badge icon represents the number of pending notifications the user has not dismissed.
 * @private
 */
pnc.ui.Notifier.prototype.updateBadge = function() {
    var badge = goog.dom.getElementByClass('badge', this.button);

    if(goog.isDefAndNotNull(badge))
        goog.dom.removeNode(badge);

    var label = this.notifications.length;

    if(label < 1)
        return;

    label = label > 9 ? '9+' : label.toString();
    badge = goog.dom.createDom('div', 'badge badge-important', label);
    goog.dom.appendChild(this.button, badge);
};

/**
 * Toggles the visibility of the notification pane.
 * @private
 */
pnc.ui.Notifier.prototype.toggle = function() {
    var pane = goog.dom.getElement('notifier-pane');

    if(goog.isDefAndNotNull(pane)) {
        goog.dom.removeNode(pane);
        return;
    }

    var position = (this.button.offsetLeft + this.button.offsetWidth - 300) + 'px';
    pane = goog.dom.createDom('div', {'id' : 'notifier-pane', 'style' : 'left:' + position});
    goog.dom.appendChild(this.button.parentElement, pane);

    if(this.notifications.length < 1) {
        var message = goog.dom.createDom('h3', null, 'No hay notificaciones nuevas');
        goog.dom.appendChild(pane, message);
        return;
    }

    for(var cont = 0, size = this.notifications.length; cont < size; cont++) {
        var notification = this.notifications[cont];
        this.renderNotification(pane, notification);
    }
};

/**
 * Renders a notification element within the notification pane
 * @param  {Element}               parent        Parent element where the notification representation will be placed
 * @param  {pnc.model.Notiication} notification  Notification model to represent
 * @private
 */
pnc.ui.Notifier.prototype.renderNotification = function(parent, notification) {
    var container = goog.dom.createDom('div', 'notification');
    goog.dom.appendChild(parent, container);

    var close = goog.dom.createDom('a', {'class' : 'pull-right', 'style' : 'padding-top: 0'}, 'x');
    goog.dom.appendChild(container, close);
    goog.events.listen(close, goog.events.EventType.CLICK, goog.bind(this.remove, this, container, notification));

    var render = function(product) {
        var messages = null;
        switch(notification.getNotificationType()) {
            case pnc.model.Notification.Type.INVENTORY_THRESHOLD:
                messages = ['El inventario de ', ' se esta agotando'];
                break;

            case pnc.model.Notification.Type.EXPIRED_INVENTORY:
                messages = ['', ' tiene inventario expirado'];
                break;

            default:
                pnc.ui.Alert.error('Tipo de notificacion no esperada');
                return;
        }

        var spans = [
            goog.dom.createDom('span', null, messages[0]),
            goog.dom.createDom('span', 'bold', product.toString()),
            goog.dom.createDom('span', null, messages[1])
        ];

        var messageBox = goog.dom.createDom('div');
        goog.dom.appendChild(container, messageBox);

        for(var cont = 0, size = spans.length; cont < size; cont++)
            goog.dom.appendChild(messageBox, spans[cont]);

        var timestampBox = goog.dom.createDom('h6', null, pnc.ui.DATE_TIME_FORMATTER.format(notification.getTimestamp()));
        goog.dom.appendChild(container, timestampBox);
    };

    // Assumes all notifications are product related, which is true when this code was initially written.
    // This may change in the future, in which case this will get awkward very fast.
    var product = pnc.model.findModel(this.productCache, notification.getIdSource());

    if(goog.isDefAndNotNull(product)) {
        render(product);
        return;
    }

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                return;

            default:
                pnc.ui.Alert.error(pnc.client.decodeStatus(response));
                return;
        }

        if(self.productCache.length > 24)
            goog.array.clear(self.productCache);

        self.productCache.push(response.content());
        render(response.content());
    };
    this.productClient.get(notification.getIdSource(), callback);
};

/**
 * Creates a transaction to remove the notification from the user's queue. If successful, creates an animation to 
 * fade away the element. Once done, the element is removed from the DOM.
 * @param  {Element}                element       Notification element
 * @param  {pnc.model.Notification} notification  Notification model
 * @private
 */
pnc.ui.Notifier.prototype.remove = function(element, notification) {
    var self = this;
    var fadeCallback = function() { 
        goog.dom.removeNode(element);
        goog.array.remove(self.notifications, notification);
        self.updateBadge();

        if(self.notifications.length < 1)
            self.toggle();
    };

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
            case pnc.client.Status.NOT_FOUND:
                break;

            default:
                pnc.ui.Alert.error(pnc.client.decodeStatus(response));
                return;
        }

        var fade = new goog.fx.dom.FadeOutAndHide(element, 300);
        goog.events.listen(fade, goog.fx.Animation.EventType.END, fadeCallback);
        fade.play();
    };
    this.client.remove(notification, responseCallback);
};
