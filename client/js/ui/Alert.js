goog.provide('pnc.ui.Alert');

goog.require('goog.async.Delay');
goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.fx.Animation');
goog.require('goog.fx.dom.FadeInAndShow');
goog.require('goog.fx.dom.FadeOutAndHide');

/**
 * An alert box that will fade into the screen, and optionally fade out after a set of time. The invoker has four choices
 * of styling of the alert box. These choices are available in the pnc.ui.Alert.Types enumeration.
 * @param  {string}  type     Type of alert to create. Should be a value from the pnc.ui.Alert.Types enumeration. If none
 *                                is provided, INFO will be used.
 * @param  {string}  title    Value to place as the title of the alert
 * @param  {string}  message  Value to place as the contents of the alert
 * @param  {boolean} fadeOut  If true the alert will fade out automatically after a couple of seconds
 * @constructor
 */
pnc.ui.Alert = function(type, message, fadeOut) {
    /**
     * Type of alert to create. Should be a value from the pnc.ui.Alert.Types enumeration. If none is provided, INFO 
     * will be used.
     * @type {string}
     * @private
     */
    this.type      = goog.isDefAndNotNull(type) ? type : pnc.ui.Alert.Types.INFO;

    /**
     * Value to place as the contents of the alert
     * @type {string}
     * @private
     */
    this.message   = goog.isDefAndNotNull(message) ? message : '';

    /**
     * If true the alert will fade out automatically after a couple of seconds
     * @type {boolean}
     * @private
     */
    this.fadeOut   = goog.isDefAndNotNull(fadeOut) ? fadeOut : false;

    /**
     * Container for the alert widget
     * @type {Element}
     * @private
     */
    this.container = null;
};

/**
 * Enumerations that detail the different type of styles that can be applied to an alert
 * @enum {string}
 */
pnc.ui.Alert.Type = {
    INFO    : 'alert-info',
    SUCCESS : 'alert-success',
    WARNING : 'alert-warning',
    ERROR   : 'alert-error'
};

/**
 * Utility static function that creates an info style alert that auto fades out.
 * @param  {string} message  Message to display in the alert
 * @return {Element}         Generated alert element
 */
pnc.ui.Alert.info = function(message) {
    var alert = new pnc.ui.Alert(pnc.ui.Alert.Type.INFO, message, true);
    return alert.render();
};

/**
 * Utility static function that creates a success style alert that auto fades out
 * @param  {string} message  Message to display in the alert
 * @return {Element}         Generated alert element
 */
pnc.ui.Alert.success = function(message) {
    var alert = new pnc.ui.Alert(pnc.ui.Alert.Type.SUCCESS, message, true);
    return alert.render();
};

/**
 * Utility static function that creates a warning style alert that does NOT fade out automatically
 * @param  {string} message  Message to display in alert
 * @return {Element}         Generated alert element
 */
pnc.ui.Alert.warning = function(message) {
    var alert = new pnc.ui.Alert(pnc.ui.Alert.Type.WARNING, message, true);
    return alert.render();
};

/**
 * Utility static function that creates an error style alert that does NOT fade out automatically
 * @param  {string} message  Message to display in the alert
 * @return {Element}         Generated alert element
 */
pnc.ui.Alert.error = function(message) {
    var alert = new pnc.ui.Alert(pnc.ui.Alert.Type.ERROR, message, true);
    return alert.render();
};

/**
 * Renders the alert
 * @param  {Element} parent  Parent element where the alert will be placed, if none is porivded, the div of ID 'container'
 *                               will be queried, and that will be used.
 * @return {Element}         Generated alert element
 */
pnc.ui.Alert.prototype.render = function(parent) {
    parent       = goog.isDefAndNotNull(parent) ? parent : goog.dom.getElement('header');
    var self     = this;
    var wrapper  = goog.dom.createDom('div', 'alert-wrapper');
    var existing = goog.dom.getElementsByClass('alert-wrapper', parent);
    var index    = goog.isDefAndNotNull(existing) ? existing.length : 0;
    goog.dom.insertChildAt(parent, wrapper, index);

    if(index > 0)
        wrapper.style.marginTop = (index * 44) + 'px';

    this.container = goog.dom.createDom('div', 'alert ' + self.type);
    goog.dom.appendChild(wrapper, this.container);
    var button = goog.dom.createDom('button', 'close pull-right', 'x');
    goog.dom.appendChild(this.container, button);
    var src, title  = '';

    switch(this.type) {
        case pnc.ui.Alert.Type.INFO:
            src = pnc.ui.Resources.ALERT_INFO;
            title = 'Informacion';
            break;

        case pnc.ui.Alert.Type.SUCCESS:
            src = pnc.ui.Resources.ALERT_OK;
            title = 'OK';
            break;

        case pnc.ui.Alert.Type.WARNING:
            src = pnc.ui.Resources.ALERT_WARNING;
            title = 'Advertencia';
            break;

        case pnc.ui.Alert.Type.ERROR:
            src = pnc.ui.Resources.ALERT_ERROR;
            title = 'Error';
            break;
    }   

    var img = goog.dom.createDom('img', {'src' : src, 'title' : title});
    goog.dom.appendChild(this.container, img);

    if(this.message.length > 0) {
        var content = goog.dom.createDom('span', null, this.message);
        goog.dom.appendChild(this.container, content);
    }

    var fadeIn = new goog.fx.dom.FadeInAndShow(this.container, 500);
    fadeIn.play();

    var fadeOut = function() {
        var fadeOut = new goog.fx.dom.FadeOutAndHide(self.container, 500);
        fadeOut.play();

        var callback = function() { self.dispose(); };
        goog.events.listen(fadeOut, goog.fx.Animation.EventType.END, callback);
    };
    goog.events.listen(button, goog.events.EventType.CLICK, fadeOut);
    goog.events.listen(wrapper, goog.events.EventType.CLICK, fadeOut);

    if(this.fadeOut) {
        var delay = new goog.async.Delay(fadeOut, 3000);
        delay.start();
    }

    return this.container;
};

/**
 * The alert will be disposed of, either automatically if the fadeOut argument is passed as true to the constructor, or
 * when the user clicks on the close button. However, if programmatic access is required to disposal, it can be done
 * through this method.
 */
pnc.ui.Alert.prototype.dispose = function() {
    goog.dom.removeNode(this.container.parentElement);
};
