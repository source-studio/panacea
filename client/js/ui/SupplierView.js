goog.provide('pnc.ui.SupplierView');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');
goog.require('goog.json');
goog.require('goog.ui.ac');
goog.require('goog.ui.ac.AutoComplete');

goog.require('pnc.client');
goog.require('pnc.client.Product')
goog.require('pnc.client.Supplier');
goog.require('pnc.model.Contact');
goog.require('pnc.model.Supplier');
goog.require('pnc.ui');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.Paginator');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');


/**
 * View that provides the user the functionality to search suppliers, edit their detials and manage their orders and 
 * credit notes.
 * @constructor
 */
pnc.ui.SupplierView = function() {
    /**
     * Client used to make supplier related transactions to the server.
     * @type {pnc.client.Supplier}
     * @private
     */
    this.client           = new pnc.client.Supplier();
    
    /**
     * Client used to make product related transactions to the server.
     * @type {pnc.client.Product}
     * @private
     */
    this.productClient    = new pnc.client.Product();

    /**
     * Cache used to populate the product auto complete control.
     * @type {Object.<string, pnc.model.Product>}
     * @private
     */
    this.productMap       = {};

    /**
     * List of products which are bought from the currently selected supplier
     * @type {Array.<pnc.model.Product>}
     * @private
     */
    this.products         = [];

    //------------- elements -------------//
    /** @private */ this.container        = null;
    /** @private */ this.paginator        = null;
    /** @private */ this.productPaginator = null;
    /** @private */ this.orderPaginator   = null;
    /** @private */ this.notePaginator    = null;
    /** @private */ this.disposables      = [];
};
pnc.ui.Main.registerModule('suppliers', pnc.ui.SupplierView);


/**
 * Renders the supplier view 
 * @param  {Element} parent  Parent element where the view will be rendered in
 */
pnc.ui.SupplierView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderSearchPane();
    this.renderActionPane();
    this.renderSupplierPane();
    this.renderDetailPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Suplidores');
};

/**
 * Disposes of the supplier view. Releases any resources that may have been used.
 */
pnc.ui.SupplierView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        if(goog.isDefAndNotNull(this.disposables[cont]) && goog.isDef(this.disposables[cont].dispose))
            this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};

/**
 * Renders the pane that provides the user the functionality to search for existing suppliers
 * @private
 */
pnc.ui.SupplierView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var nameLabel = goog.dom.createDom('label', 'span1', 'Nombre');
    goog.dom.appendChild(content, nameLabel);
    var nameSearch = goog.dom.createDom('input', {'id' : 'name-search', 'class' : 'span3', 'type' : 'text'});
    goog.dom.appendChild(content, nameSearch);

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Buscar');
    goog.dom.appendChild(controlPane, searchButton);
    var self = this;

    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateSupplierPane(response.content());
    };

    var clickCallback = function() {
        if(nameSearch.value.length < 1) {
            goog.dom.classes.add(nameSearch, 'validation-error');
            return;
        }

        goog.dom.classes.remove(nameSearch, 'validation-error');
        self.client.search(nameSearch.value, searchCallback);
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);
};

/**
 * Renders the pane that provides the user the functionlaity of adding new suppliers.
 * @private
 */
pnc.ui.SupplierView.prototype.renderActionPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3', pnc.ui.Resources.BOLT, 'Acciones');
    var privs   = pnc.client.Profile.getPrivileges();

    if(!goog.array.contains(privs, pnc.client.Privileges.SUPPLIER_ADD))
        return;

    var addButton = goog.dom.createDom('button', {'id' : 'add-button', 'class' : 'btn btn-success btn-block'});
    goog.dom.appendChild(content, addButton);
    goog.events.listen(addButton, goog.events.EventType.CLICK, goog.bind(this.add, this));
    var plus = goog.dom.createDom('img', {'src' : pnc.ui.Resources.PLUS});
    goog.dom.appendChild(addButton, plus);
    var span = goog.dom.createDom('span', null, 'Suplidor');
    goog.dom.appendChild(addButton, span);
};

/**
 * Renders the pane that displays the list of suppliers that match the search criteria. It also exposes a toolbar that
 * lets the user manage suppliers and their orders and credit notes.
 * @private
 */
pnc.ui.SupplierView.prototype.renderSupplierPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Suplidores');
    content.id  = 'supplier-pane';
    content.style.minHeight = '390px';
    var head = goog.dom.getElementsByClass('box-head', content.parentElement)[0];

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 5px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var editButton, removeButton, productButton, orderButton, creditNoteButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.SUPPLIER_PRODUCT))
        productButton = pnc.ui.createToolbarButton(actionGroup, 'product-button', pnc.ui.Resources.BAR_CODE, 'Productos', false, goog.bind(this.productDetail, this));

    if(goog.array.contains(privs, pnc.client.Privileges.SUPPLIER_ORDER))
        orderButton = pnc.ui.createToolbarButton(actionGroup, 'order-button', pnc.ui.Resources.ORDER, 'Ordenes', false, goog.bind(this.orderDetail, this));

    if(goog.array.contains(privs, pnc.client.Privileges.SUPPLIER_CREDIT_NOTE))
        creditNoteButton = pnc.ui.createToolbarButton(actionGroup, 'credit-note-button', pnc.ui.Resources.CREDIT_NOTE, 'Notas de Credito', false, goog.bind(this.noteDetail, this));

    if(goog.array.contains(privs, pnc.client.Privileges.SUPPLIER_UPDATE))    
        editButton = pnc.ui.createToolbarButton(actionGroup, 'edit-button', pnc.ui.Resources.EDIT, 'Editar', false, goog.bind(this.edit, this));

    if(goog.array.contains(privs, pnc.client.Privileges.SUPPLIER_REMOVE))
        editButton = pnc.ui.createToolbarButton(actionGroup, 'remove-button', pnc.ui.Resources.REMOVE, 'Remover', false, goog.bind(this.remove, this));

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var navCallback = function(e) {
        if(!goog.isNull(editButton))
            editButton.disabled = true;

        if(!goog.isNull(removeButton))
            removeButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);
    goog.events.listen(backButton,    goog.events.EventType.CLICK, navCallback);

    var titles = ['Organizacion', 'Nombre'];
    this.paginator = new pnc.ui.Paginator(titles, [], null, pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderSupplier, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Creates the pane that shows further detail on the supplier, like address and contacts.
 * @private
 */
pnc.ui.SupplierView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '157px';
    var body = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 1, 'Suplidor');
    body.id = 'detail-table';
};

/**
 * Paginator callback used to create a row that represnets a supplier.
 * @param  {Element}            parent    Parent element where the row will be placed
 * @param  {pnc.model.Supplier} supplier  Supplier that will be represented
 * @private
 */
pnc.ui.SupplierView.prototype.renderSupplier = function(parent, supplier) {
    var row = goog.dom.createDom('tr', {'data-supplier' : supplier.getId()});
    goog.dom.appendChild(parent, row);
    var buttons = [ 
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button'),
        goog.dom.getElement('credit-note-button'),
        goog.dom.getElement('product-button'),
        goog.dom.getElement('order-button')
    ];
    var self = this;

    var callback = function(supplier, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = false;

        self.populateDetailPane(supplier, row);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, supplier));

    var cells = [
        goog.dom.createDom('td', null, supplier.getOrganization()),
        goog.dom.createDom('td', null, supplier.getFirstName() + ' ' + supplier.getLastName())
    ];

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Populates the supplier pane with the provides array of suppliers
 * @param  {Array.<pnc.model.Supplier>} suppliers  Suppliers to populate
 * @private
 */
pnc.ui.SupplierView.prototype.populateSupplierPane = function(suppliers) {
    var content       = goog.dom.getElement('supplier-pane');
    var editButton    = goog.dom.getElement('edit-button');
    var removeButton  = goog.dom.getElement('remove-button');
    var backButton    = goog.dom.getElement('back-button');
    var forwardButton = goog.dom.getElement('forward-button');
    var detailTable   = goog.dom.getElement('detail-table');

    goog.dom.removeChildren(detailTable);
    backButton.disabled    = true;
    forwardButton.disabled = goog.isDefAndNotNull(suppliers) && suppliers.length <= pnc.ui.PAGE_SIZE;

    if(goog.isDefAndNotNull(editButton))
        editButton.disabled = true;

    if(goog.isDefAndNotNull(removeButton))
        removeButton.disabled = true;

    this.paginator.setData(suppliers);
    this.paginator.refresh();
};

/**
 * Populates the detail pane to show auxiliary info for a supplier once selected in the paginator.
 * @param  {pnc.model.Supplier} supplier  Supplier for which auxiliary info will be shown.
 * @private
 */
pnc.ui.SupplierView.prototype.populateDetailPane = function(supplier) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);

    if(!goog.isDefAndNotNull(supplier))
        return;

    pnc.ui.createSummaryContent(body, supplier.toString());
    var contacts = supplier.getContacts();

    if(goog.isDefAndNotNull(contacts) && contacts.length > 0) {
        var row  = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td', {'colspan' : '2'});
        goog.dom.appendChild(row, cell);

        for(var cont = 0, size = contacts.length; cont < size; cont++) {
            var entry = goog.dom.createDom('div', null, contacts[cont].toString());
            goog.dom.appendChild(cell, entry);
        }
    } else {
        var row = pnc.ui.createSummaryContent(body, 'Ningun contacto registrado');
        row.children[0].style.textAlign = 'center';
    }

    var row = pnc.ui.createSummaryContent(body, 'Ordenes Pendientes');
    row.children[0].style.fontWeight = 'bold';
    row.children[0].style.textAlign  = 'center';
    row = pnc.ui.createSummaryContent(body, '');
    var orderCell = row.children[0];
    orderCell.id  = 'unpaid-order-cell';

    row = pnc.ui.createSummaryContent(body, 'Credito Pendiente');
    row.children[0].style.fontWeight = 'bold';
    row.children[0].style.textAlign  = 'center';
    row = pnc.ui.createSummaryContent(body, '');
    var creditCell = row.children[0];
    creditCell.id  = 'unredeemed-credit-note-cell';

    var callback = function(cell, name, response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(0));
                break;

            case pnc.client.Status.OK:
                var total  = 0;
                var orders = response.content();

                for(var cont = 0, size = orders.length; cont < size; cont++)
                    total += orders[cont].getAmount();

                goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(total));
                break;

            default:
                pnc.ui.Alert.error('Error al tratar de calcular el total de ' + name + ' pendientes del suplidor');
                break;
        }
    };
    this.client.getOrdersByPaid(supplier, false, goog.partial(callback, orderCell, 'ordenes'));
    this.client.getNotesByRedeemed(supplier, false, goog.partial(callback, creditCell, 'notas de credito'));
};

/**
 * Provides a form to add a new supplier ot the collection
 * @private
 */
pnc.ui.SupplierView.prototype.add = function() {
    var button = goog.dom.getElement('add-button');
    var buttons = [
        goog.dom.getElement('product-button'),
        goog.dom.getElement('order-button'),
        goog.dom.getElement('credit-note-button'),
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button')
    ];

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var content     = goog.dom.createDom('div');
    var inputs      = this.createSupplierForm(content, null);
    var contactForm = new pnc.ui.ContactForm();
    contactForm.render(content);
    contactForm.getElement().style.marginTop = '-10px';
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        contactForm.dispose();
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(button))
            button.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        var supplier = response.content();
        self.paginator.getData().push(supplier);
        goog.array.sort(self.paginator.getData(), pnc.model.Supplier.compare);
        self.paginator.refresh();
        self.populateDetailPane(null);

        if(goog.isDefAndNotNull(button))
            button.disabled = true;

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;
        
        cancelCallback();
        pnc.ui.Alert.success('El suplidor fue agregado');
    };

    var saveCallback = function() {
        if(!self.validateSupplierForm(inputs))
            return;

        var supplier = self.parseSupplierForm(inputs);
        supplier.setContacts(contactForm.getContacts());
        self.client.add(supplier, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar Suplidor', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Provides a form to edit the details of an existing supplier
 * @private
 */
pnc.ui.SupplierView.prototype.edit = function() {
    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var supplier = pnc.model.findModel(this.paginator.getData(), row.dataset.supplier);
    var button   = goog.dom.getElement('add-button');
    var buttons  = [
        goog.dom.getElement('product-button'),
        goog.dom.getElement('order-button'),
        goog.dom.getElement('credit-note-button'),
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button')
    ];

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var content     = goog.dom.createDom('div');
    var inputs      = this.createSupplierForm(content, supplier);
    var contactForm = new pnc.ui.ContactForm(supplier.getContacts());
    contactForm.render(content);
    contactForm.getElement().style.marginTop = '-10px';
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() {
        contactForm.dispose();
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();

        if(goog.isDefAndNotNull(button))
            button.disabled = false;
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newSupplier = response.content();
        pnc.model.replaceModel(self.paginator.getData(), newSupplier);
        self.paginator.refresh();
        self.populateDetailPane(newSupplier);

        if(goog.isDefAndNotNull(button))
            button.disabled = false;

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;

        cancelCallback();
        pnc.ui.Alert.success('El suplidor fue actualizado');
    };

    var saveCallback = function() {
        if(!self.validateSupplierForm(inputs))
            return;

        var newSupplier = self.parseSupplierForm(inputs, supplier);
        newSupplier.setId(supplier.getId());
        newSupplier.setContacts(contactForm.getContacts());
        self.client.update(newSupplier, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog. If accepted, performs the transaction to remove a supplier.
 * @private
 */
pnc.ui.SupplierView.prototype.remove = function() {
    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var supplier = pnc.model.findModel(this.paginator.getData(), row.dataset.supplier);
    var dialog   = pnc.ui.SlideDialog.warning('Esta seguro que desea remover este suplidor?');
    var self     = this;
    var buttons  = [
        goog.dom.getElement('product-button'),
        goog.dom.getElement('order-button'),
        goog.dom.getElement('credit-note-button'),
        goog.dom.getElement('edit-button'),
        goog.dom.getElement('remove-button')
    ];

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Este suplidor tiene referencias a otra data. No puede ser removido.').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.paginator.getData(), supplier);
        self.paginator.refresh();
        self.populateDetailPane(null);

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;
        
        pnc.ui.Alert.success('El suplidor fue removido');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.remove(supplier, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a form that is used to both create and update supplier data.
 * @param  {Element}            parent    Parent element where the form will be placed
 * @param  {pnc.model.Supplier} supplier  Optional supplier. If passed, its data will be used to pre-populate the form.
 * @private
 */
pnc.ui.SupplierView.prototype.createSupplierForm = function(parent, supplier) {
    var inputs = {};
    var header = goog.dom.createDom('h4', 'form-header', 'Suplidor');
    goog.dom.appendChild(parent, header);

    var create = function(key, name, help) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var label = goog.dom.createDom('label', {'for' : key, 'style' : 'width: 100px'}, name);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span3', 'type' : 'text'});
        goog.dom.appendChild(group, inputs[key]);

        if(!goog.isNull(help)) {
            var help = goog.dom.createDom('span', 'help-inline', help);
            goog.dom.appendChild(group, help);
        }
    };

    var fields = {
        'supplier-organization' : ['Organizacion', 'No puede estar vacio'],
        'supplier-first'        : ['Nombre',       'No puede estar vacio'],
        'supplier-last'         : ['Apellido',     'No puede estar vacio']
    };

    for(var key in fields)
        create(key, fields[key][0], fields[key][1]);

    if(goog.isDefAndNotNull(supplier)) {
        inputs['supplier-organization'].value = supplier.getOrganization();
        inputs['supplier-first'].value        = supplier.getFirstName();
        inputs['supplier-last'].value         = supplier.getLastName();
    }

    header = goog.dom.createDom('h4', 'form-header', 'Contacto');
    goog.dom.appendChild(parent, header);

    return inputs;
};

/**
 * Validates the contents of the supplier form. If issues are found with the data, they will be highlighted inline.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @return {boolean}                          Was the form content satisfactory?
 * @private
 */
pnc.ui.SupplierView.prototype.validateSupplierForm = function(inputs) {
    var errors = goog.dom.getElementsByClass('error', inputs['supplier-organization'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    for(var key in inputs)
        if(inputs[key].value.length < 1)
            goog.dom.classes.add(inputs[key].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['supplier-organization'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Takes the content of a supplier form and creates a model out of it.
 * @param  {Object.<string, Element>} inputs    Form input elements
 * @param  {pnc.model.Supplier}       supplier  Optional supplier. If passed, this method will update this model instead
 *                                              of creating a new one.
 * @private
 */
pnc.ui.SupplierView.prototype.parseSupplierForm = function(inputs, supplier) {
    supplier = goog.isDefAndNotNull(supplier) ? supplier : new pnc.model.Supplier();
    supplier.setOrganization( inputs['supplier-organization'].value);
    supplier.setFirstName(    inputs['supplier-first'].value);
    supplier.setLastName(     inputs['supplier-last'].value);

    return supplier;
};

/**
 * Shows the a list of all products which are bought from a supplier, and lets the user manipulate the data set.
 * @private
 */
pnc.ui.SupplierView.prototype.productDetail = function() {
    var button = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var supplier = pnc.model.findModel(this.paginator.getData(), row.dataset.supplier);
    var content  = goog.dom.createDom('div');
    var slider   = new pnc.ui.SlidePanel('Productos Suplidos por ' + supplier.toString(), pnc.ui.Resources.BAR_CODE, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Productos');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 5px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var self = this;

    var callback = function() { 
        if(goog.isDefAndNotNull(button))
            button.disabled = false;

        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'supplier-back-button', pnc.ui.Resources.BACK, 'Regresar al Suplidor', true, callback);
    this.createProductAddPane(content, supplier);
    this.createProductListPane(slider.getElement(), supplier);
};

/**
 * Creates a pane that lets the user add products to the list that is bought from a supplier. This does not create new
 * products. It searches in the catalog of existing products and associates them with the supplier. 
 * @param  {Element}            parent    Parent element where the pane will be placed
 * @param  {pnc.model.Supplier} supplier  Supplier for which products will be added
 * @private
 */
pnc.ui.SupplierView.prototype.createProductAddPane = function(parent, supplier) {
    var row = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, row);

    var label = goog.dom.createDom('label', {'for' : 'brand-search', 'class' : 'span1'}, 'Marca');
    goog.dom.appendChild(row, label);
    var brandSearch = goog.dom.createDom('input', {'id' : 'brand-search', 'class' : 'span4', 'type' : 'text'});
    goog.dom.appendChild(row, brandSearch);
    var brandAc = goog.ui.ac.createSimpleAutoComplete([], brandSearch, false, true);
    this.disposables.push(brandAc);
    var brandPill = pnc.ui.createPill(brandSearch, brandAc);
    brandPill.id  = 'brand-search-pill';

    row = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, row);

    label = goog.dom.createDom('label', {'for' : 'product-search', 'class' : 'span1'}, 'Producto');
    goog.dom.appendChild(row, label);
    var searchGroup = goog.dom.createDom('div', 'input-append');
    goog.dom.appendChild(row, searchGroup);
    var productSearch = goog.dom.createDom('input', {'id' : 'product-search', 'class' : 'span4', 'style' : 'width: 210px', 'type' : 'text'});
    goog.dom.appendChild(searchGroup, productSearch);
    var productAc = goog.ui.ac.createSimpleAutoComplete([], productSearch, false, true);
    this.disposables.push(productAc);
    var productPill = pnc.ui.createPill(productSearch, productAc);
    productPill.id  = 'product-search-pill';
    productPill.style.width = '202px';
    var productAdd = goog.dom.createDom('button', {'id' : 'product-add', 'class' : 'btn btn-primary', 'disabled' : true}, 'Agregar');
    goog.dom.appendChild(searchGroup, productAdd);
    var callback = function() {
        self.addProduct(supplier);
        pnc.ui.closePill(productPill);
    };
    goog.events.listen(productAdd, goog.events.EventType.CLICK, callback);

    var brandLink = pnc.client.Profile.getLink(pnc.client.Product.Rel.SEARCH_BRANDS);
    var nameLink  = pnc.client.Profile.getLink(pnc.client.Product.Rel.SEARCH);
    var resolver  = function() { return brandSearch.value; };
    pnc.ui.updateAcData(brandSearch, brandAc, brandLink, {'{brand}' : resolver}, goog.json.unsafeParse);

    var brandCallback = function(e) {
        if(goog.events.KeyCodes.isCharacterKey(e.keyCode) && goog.events.KeyCodes.isTextModifyingKeyEvent(e)) {
            productSearch.value = '';
            pnc.ui.closePill(productPill);
        }
    };
    goog.events.listen(brandSearch, goog.events.EventType.KEYUP, brandCallback);
    var self = this;

    var xhrCallback = function(e) {
        goog.object.clear(self.productMap);

        switch(e.target.getStatus()) {
            case pnc.client.Status.OK:
                var products = pnc.client.decodeCollection(pnc.client.Product.decode, e.target.getResponseText());
                var data     = [];

                for(var cont = 0, size = products.length; cont < size; cont++) {
                    data[cont] = products[cont].toString();
                    self.productMap[data[cont]] = products[cont];
                }

                productAc.matcher_.setRows(data);
                break;

            case pnc.client.Status.NOT_FOUND:
                productAc.matcher_.setRows([]);
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(e.target)).open();
                break;
        }
    };

    var inputCallback = function(e) {
        var productValue    = goog.string.trim(productSearch.value);
        var brandValue      = goog.string.trim(brandSearch.value);
        productAdd.disabled = productValue.length < 1;

        if(productValue.length < 1 || !goog.events.KeyCodes.isCharacterKey(e.keyCode))
            return;

        var method, uri = null;

        if(brandValue.length > 0) {
            method = nameLink.method();
            uri    = nameLink.uri().replace('{brand}', goog.string.urlEncode(brandValue));
            uri    = uri.replace('{name}', goog.string.urlEncode(productValue));
        } else {
            method = nameLink.method();
            uri    = nameLink.uri().replace('{name}', goog.string.urlEncode(productValue));
            uri    = uri.replace('{brand}', '');
        }

        goog.net.XhrIo.send(uri, xhrCallback, method);
    };
    goog.events.listen(productSearch, goog.events.EventType.KEYUP, inputCallback);
};

/**
 * Creates a pane that shows a list of all the products that are bought from a supplier.
 * @param  {Element}            parent    Parent element where the pane will be placed
 * @param  {pnc.model.Supplier} supplier  Supplier for which products will be displayed
 * @private
 */
pnc.ui.SupplierView.prototype.createProductListPane = function(parent, supplier) {
    var content = pnc.ui.createBox(parent, 'span7', pnc.ui.Resources.LIST, 'Productos');
    content.id  = 'product-pane';
    content.style.minHeight = '390px';
    content.parentElement.style.paddingTop = '10px';
    var head = goog.dom.getElementsByClass('box-head', content.parentElement)[0];
    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 5px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    
    if(goog.isDefAndNotNull(this.productPaginator))
        this.productPaginator.dispose();
    
    this.productPaginator = new pnc.ui.Paginator(['Marca', 'Nombre', ''], [], 'summary-table table-striped table-condensed', 
                    pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.productPaginator.setRowRenderer(goog.bind(this.renderProduct, this, supplier));
    this.productPaginator.render(content);
    var self      = this;

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                self.products = response.content();
                break;

            case pnc.client.Status.NOT_FOUND:
                self.products = [];
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.productPaginator.setData(self.products);
        self.productPaginator.refresh();
    };
    this.productClient.getBySupplier(supplier, callback);
};

/**
 * Paginator callback used to render a product as a row.
 * @param  {pnc.model.Supplier} supplier  Supplier for which products are rendered
 * @param  {Element}            parent    Parent element where the product representation will be placed
 * @param  {pnc.mode.Product}   product   Product to represent
 * @private
 */
pnc.ui.SupplierView.prototype.renderProduct = function(supplier, parent, product) {
    var row = goog.dom.createDom('tr');
    row.dataset.product = product.getId().toString();
    goog.dom.appendChild(parent, row);

    var data = [product.getBrand(), product.getName(), ''];

    for(var cont = 0, size = data.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', null, data[cont]);
        goog.dom.appendChild(row, cell);
        cell.style.width = 'initial';
    }
    
    var controlPane = row.children[row.children.length - 1];
    controlPane.style.width = '28px';
    controlPane.style.paddingLeft = '5px';
    var removeButton = goog.dom.createDom('button', {'class' : 'btn', 'style' : 'padding: 0 6px'});
    goog.dom.appendChild(controlPane, removeButton);
    var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.REMOVE, 'title' : 'Remover Producto', 
                'style' : 'width : 14px; height: 14px'});
    goog.dom.appendChild(removeButton, img);
    goog.events.listen(removeButton, goog.events.EventType.CLICK, goog.bind(this.removeProduct, this, supplier));
};

/**
 * Provides a pane where the user can add a product to the list bought from a supplier
 * @param {pnc.model.Supplier} supplier  Supplier for which the product will be added
 * @private
 */
pnc.ui.SupplierView.prototype.addProduct = function(supplier) {
    var productSearch = goog.dom.getElement('product-search');
    var product       = this.productMap[productSearch.value];

    if(!goog.isDefAndNotNull(product)) {
        pnc.ui.Alert.error('Este producto no existe.');
        return;
    }

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                pnc.ui.Alert.error('No se encontraron los datos del producto o suplidor para agregar');
                return;

            case pnc.client.Status.CONFLICT:
                pnc.ui.Alert.info('Este producto ya ha sido agregado al suplidor');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        self.productPaginator.getData().push(product);
        self.productPaginator.refresh();
        pnc.ui.Alert.success('Product agregado a suplidor');
    };
    this.client.addProduct(supplier, product, callback);
};

/**
 * Provides the user with a confirmation dialog. If the user confirms, executes the transaction to remove the product
 * from the list provided by the supplier. This does not delete the product. It merely disassociates the product from
 * the supplier.
 * @param  {pnc.model.Supplier} supplier  Supplier for which the product will be removed
 * @param  {Event}              e         Event raised
 * @private
 */
pnc.ui.SupplierView.prototype.removeProduct = function(supplier, e) {
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover este producto de la lista proveida por este suplidor?');
    var self   = this;

    var responseCallback = function(r) {
        switch(r.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                pnc.ui.Alert.error('No se encontraron los datos del suplidor y/o producto');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(r)).open();
                break;
        }

        var parent = e.target.parentElement.parentElement;
        parent = parent.nodeName == 'TD' ? parent.parentElement : parent;
        pnc.model.removeModel(self.products, parent.dataset.product);
        goog.dom.removeNode(parent);
        pnc.ui.Alert.success('El producto fue removido de la lista proveida por este suplidor');
    };
    var selectCallback = function(de) {
        if(de.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        var parent = e.target.parentElement.parentElement;
        parent = parent.nodeName == 'TD' ? parent.parentElement : parent;
        var product = pnc.model.findModel(self.products, parent.dataset.product);
        self.client.removeProduct(supplier, product, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Provides a pane where the user can see past orders made to a supplier, as well as enter new ones.
 * @private
 */
pnc.ui.SupplierView.prototype.orderDetail = function() {
    var button = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var supplier = pnc.model.findModel(this.paginator.getData(), row.dataset.supplier);
    var content  = goog.dom.createDom('div');
    var slider   = new pnc.ui.SlidePanel('Ordenes puestas a ' + supplier.toString(), pnc.ui.Resources.ORDER, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Ordenes');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 5px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);

    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var callback = function() { 
        if(goog.isDefAndNotNull(button))
            button.disabled = false;

        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'supplier-back-button', pnc.ui.Resources.BACK, 'Regresar al Suplidor', true, callback);
    var privs = pnc.client.Profile.getPrivileges();

    if(goog.array.contains(privs, pnc.client.Privileges.ORDER_ADD))
        pnc.ui.createToolbarButton(actionGroup, 'order-add-button', pnc.ui.Resources.PLUS_GRAY, 'Agregar Orden', true, goog.bind(this.addOrder, this, supplier));

    if(goog.array.contains(privs, pnc.client.Privileges.ORDER_UPDATE))
        pnc.ui.createToolbarButton(actionGroup, 'order-update-button', pnc.ui.Resources.EDIT, 'Editar Orden', false, goog.bind(this.updateOrder, this, supplier));

    if(goog.array.contains(privs, pnc.client.Privileges.ORDER_REMOVE))
        pnc.ui.createToolbarButton(actionGroup, 'order-remove-button', pnc.ui.Resources.REMOVE, 'Remover Orden', false, goog.bind(this.removeOrder, this, supplier));

    var backButton      = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton   = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    this.orderPaginator = new pnc.ui.Paginator(['Fecha', 'Referencia', 'Monto', 'Pagado'], [], 
                        'table-condensed table-bordered summary-table table-striped', 20, backButton, forwardButton);
    this.orderPaginator.setRowRenderer(goog.bind(this.renderOrder, this, supplier));
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                self.orderPaginator.setData(response.content());
                self.orderPaginator.render(content);
                break;

            case pnc.client.Status.NOT_FOUND:
                self.orderPaginator.setData([]);
                self.orderPaginator.render(content);
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                break;
        }
    };
    this.client.getOrders(supplier, callback);
};

/**
 * Paginator callback used to create order representations as rows within the paginator.
 * @param  {pnc.model.Supplier} supplier  Supplier for which orders are being shown
 * @param  {Element}            parent    Parent element where order representation will be placed
 * @param  {pnc.model.order}    order     Order to represent
 * @pirvate
 */
pnc.ui.SupplierView.prototype.renderOrder = function(supplier, parent, order) {
    var row = goog.dom.createDom('tr');
    row.dataset.order = order.getId().toString();
    goog.dom.appendChild(parent, row);

    var data = [
        pnc.ui.DATE_FORMATTER.format(order.getTimestamp()),
        order.getReference(),
        pnc.ui.DECIMAL_FORMATTER.format(order.getAmount()),
        ''
    ];

    for(var cont = 0, size = data.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', null, data[cont]);
        goog.dom.appendChild(row, cell);
        cell.style.width = 'initial';
    }

    var amountCell = row.children[row.children.length - 2];
    amountCell.style.textAlign = 'right';

    var paidCell = row.children[row.children.length - 1];
    paidCell.style.width       = '28px';
    paidCell.style.paddingLeft = '5px';
    paidCell.style.textAlign   = 'center';

    if(order.isPaid()) {
        var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.RESULT, 'title' : 'Pago'});
        goog.dom.appendChild(paidCell, img);
    }

    var self = this;
    var editButton   = goog.dom.getElement('order-update-button');
    var removeButton = goog.dom.getElement('order-remove-button');
    var callback = function(order, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = false;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = false;
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, order));
};

/**
 * Provides a pane where the user can add a new order to a supplier's list.
 * @param {pnc.model.Supplier} supplier  Supplier for which the order will be added
 * @private
 */
pnc.ui.SupplierView.prototype.addOrder = function(supplier) {
    var content     = goog.dom.createDom('div');
    var inputs      = this.createOrderForm(content, null);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        self.orderPaginator.getData().push(response.content());
        goog.array.sort(self.orderPaginator.getData(), pnc.model.Order.compare);
        self.orderPaginator.refresh();
        cancelCallback();
        pnc.ui.Alert.success('La orden fue agregada');
    };

    var saveCallback = function() {
        if(!self.validateOrderForm(inputs))
            return;

        var newOrder = self.parseOrderForm(inputs);
        newOrder.setIdSupplier(supplier.getId());
        self.client.addOrder(supplier, newOrder, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar Orden', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Provides a pane where the user can update the details on an existing order.
 * @param  {pnc.model.Supplier} supplier  Supplier for which the order will be added
 * @private
 */
pnc.ui.SupplierView.prototype.updateOrder = function(supplier) {
    var row         = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.orderPaginator.getElement())[0];
    var order       = pnc.model.findModel(this.orderPaginator.getData(), row.dataset.order);
    var paid        = order.isPaid();
    var content     = goog.dom.createDom('div');
    var inputs      = this.createOrderForm(content, order);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newOrder = response.content();
        pnc.model.replaceModel(self.orderPaginator.getData(), newOrder);
        self.orderPaginator.refresh();

        if(newOrder.isPaid() && !paid) {
            var cell  = goog.dom.getElement('unpaid-order-cell');
            var value = pnc.ui.DECIMAL_FORMATTER.parse(goog.dom.getTextContent(cell));
            value    -= newOrder.getAmount();
            goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(value));
        } else if(!newOrder.isPaid() && paid) {
            var cell  = goog.dom.getElement('unpaid-order-cell');
            var value = pnc.ui.DECIMAL_FORMATTER.parse(goog.dom.getTextContent(cell));
            value    += newOrder.getAmount();
            goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(value));
        }

        cancelCallback();
        pnc.ui.Alert.success('La orden fue actualizada');

        var buttons = [ 
            goog.dom.getElement('order-update-button'),
            goog.dom.getElement('order-remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;
    };

    var saveCallback = function() {
        if(!self.validateOrderForm(inputs))
            return; 

        var newOrder = self.parseOrderForm(inputs, order);
        newOrder.setIdSupplier(supplier.getId());
        self.client.updateOrder(newOrder, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog. If the user confirms, executes the transaction to remove the order.
 * @param  {pnc.model.Supplier} supplier  Supplier for which the order will be removed
 * @private
 */
pnc.ui.SupplierView.prototype.removeOrder = function(supplier) {
    var row    = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.orderPaginator.getElement())[0];
    var order  = pnc.model.findModel(this.orderPaginator.getData(), row.dataset.order);
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover esta orden?');
    var self   = this;

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Esta orden tiene referencias a otra data. No puede ser removido.').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.orderPaginator.getData(), order);
        self.orderPaginator.refresh();
        pnc.ui.Alert.success('La orden fue removida');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.removeOrder(order, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a form that is used for creating orders as well as updating the details in existing orders.
 * @param  {Element}         parent  Parent element where the form will be placed
 * @param  {pnc.model.Order} order   Optional order. If provided, the form will be pre-populated with its details.
 * @private
 */
pnc.ui.SupplierView.prototype.createOrderForm = function(parent, order) {
    var inputs = {};
    var fields = {
        'order-date'      : ['Fecha',      'date',     'No puede estar vacio'],
        'order-reference' : ['Referencia', 'text',     null],
        'order-amount'    : ['Cantidad',   'text',     'Debe ser un numero mayor a cero']
    };

    for(var key in fields) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var name  = fields[key][0];
        var label = goog.dom.createDom('label', {'for' : key, 'style' : 'width: 100px'}, name);
        goog.dom.appendChild(group, label);

        inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span2', 'type' : fields[key][1]});
        goog.dom.appendChild(group, inputs[key]);
        var help = fields[key][2];

        if(!goog.isNull(help)) {
            var help = goog.dom.createDom('span', 'help-inline', help);
            goog.dom.appendChild(group, help);
        }
    }

    pnc.ui.assureNumbers(inputs['order-amount']);

    var group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    var label = goog.dom.createDom('label', {'for' :'order-paid', 'style' : 'width: 80px'}, 'Pagado');
    goog.dom.appendChild(group, label);
    inputs['order-paid'] = goog.dom.createDom('input', {'id' : 'order-paid', 'type' : 'checkbox', 
                    'style' : 'width: 15px; height : 15px; margin: -10px 5px 0 0'});
    goog.dom.appendChild(group, inputs['order-paid']);
    inputs['order-paid-date'] = goog.dom.createDom('input', {'id' : 'order-paid-date', 'type' : 'date', 'class' : 'span2'});
    goog.dom.appendChild(group, inputs['order-paid-date']);
    var help = goog.dom.createDom('span', 'help-inline', 'Debe entrar la fecha de pago');
    goog.dom.appendChild(group, help);

    var callback = function(e) {
        inputs['order-paid-date'].value    = e.target.checked ? pnc.model.DATE_FORMATTER.format(new Date()) : '';
        inputs['order-paid-date'].disabled = !e.target.checked;
    };
    goog.events.listen(inputs['order-paid'], goog.events.EventType.CLICK, callback);

    if(goog.isDefAndNotNull(order)) {
        inputs['order-date'].value      = goog.isDefAndNotNull(order.getTimestamp()) 
                                        ? pnc.model.DATE_FORMATTER.format(order.getTimestamp()) 
                                        : '';
        inputs['order-reference'].value = order.getReference();
        inputs['order-amount'].value    = pnc.ui.DECIMAL_FORMATTER.format(order.getAmount());
        inputs['order-paid'].checked    = order.isPaid();

        if(order.isPaid())
            inputs['order-paid-date'].value = pnc.model.DATE_FORMATTER.format(order.getPaidDate());
        else if(!goog.isDefAndNotNull(order.getPaidDate()))
            inputs['order-paid-date'].disabled = true;
    } else {
        inputs['order-date'].value         = pnc.model.DATE_FORMATTER.format(new Date());
        inputs['order-paid-date'].disabled = true;
    }

    return inputs;
};

/**
 * Validates the content of the order form. If any issues are found, they are highlighted inline within the form.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @return {boolean}                          Was the form contents satisfactory?
 * @private
 */
pnc.ui.SupplierView.prototype.validateOrderForm = function(inputs) {
    var errors   = goog.dom.getElementsByClass('error', inputs['order-date'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(inputs['order-date'].value.length < 1)
        goog.dom.classes.add(inputs['order-date'].parentElement, 'error');

    if(inputs['order-amount'].value.length < 1 || !pnc.ui.FLOAT_RGX.test(inputs['order-amount'].value) || inputs['order-amount'].value <= 0)
        goog.dom.classes.add(inputs['order-amount'].parentElement, 'error');

    if(inputs['order-paid'].checked && inputs['order-paid-date'].value.length < 1)
        goog.dom.classes.add(inputs['order-paid'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['order-date'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the content of the order form and creates an order model object out of it.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @param  {pnc.model.Order}          order   Optional order. If provided the form contents will be used to update this model.
 * @return {pnc.model.Order}                  Order derived from the form.
 * @private
 */
pnc.ui.SupplierView.prototype.parseOrderForm = function(inputs, order) {
    order = goog.isDefAndNotNull(order) ? order : new pnc.model.Order();
    var timestamp = new Date();

    pnc.model.DATE_PARSER.parse(inputs['order-date'].value, timestamp);
    order.setTimestamp(timestamp);
    order.setReference(inputs['order-reference'].value);
    order.setAmount(pnc.ui.DECIMAL_FORMATTER.parse(inputs['order-amount'].value));
    order.setPaid(inputs['order-paid'].checked);

    if(order.isPaid() && inputs['order-paid-date'].value.length > 0) {
        var paidDate = new Date();
        pnc.model.DATE_PARSER.parse(inputs['order-paid-date'].value, paidDate);
        paidDate.setHours(0); paidDate.setMinutes(0); paidDate.setSeconds(0);
        order.setPaidDate(paidDate);
    } else {
        order.setPaidDate(null);
    }

    return order;
};

/**
 * Provies a pane where the user will be shown a list of all the existing credit notes that have been provied by a supplier.
 * @private
 */
pnc.ui.SupplierView.prototype.noteDetail = function() {
    var button = goog.dom.getElement('add-button');

    if(goog.isDefAndNotNull(button))
        button.disabled = true;

    var row      = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.paginator.getElement())[0];
    var supplier = pnc.model.findModel(this.paginator.getData(), row.dataset.supplier);
    var content  = goog.dom.createDom('div');
    var slider   = new pnc.ui.SlidePanel('Notas de Credito por ' + supplier.toString(), pnc.ui.Resources.CREDIT_NOTE, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Notas de Credito');

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 5px 0 0'});
    goog.dom.appendChild(slider.getHeaderElement(), toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);

    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);
    var self = this;

    var callback = function() { 
        if(goog.isDefAndNotNull(button))
            button.disabled = false;

        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    pnc.ui.createToolbarButton(actionGroup, 'supplier-back-button', pnc.ui.Resources.BACK, 'Regresar al Suplidor', true, callback);
    var privs = pnc.client.Profile.getPrivileges();

    if(goog.array.contains(privs, pnc.client.Privileges.CREDIT_NOTE_ADD))
        pnc.ui.createToolbarButton(actionGroup, 'note-add-button', pnc.ui.Resources.PLUS_GRAY, 'Agregar Nota de Credito', true, goog.bind(this.addNote, this, supplier));

    if(goog.array.contains(privs, pnc.client.Privileges.CREDIT_NOTE_UPDATE))
        pnc.ui.createToolbarButton(actionGroup, 'note-update-button', pnc.ui.Resources.EDIT, 'Editar Nota de Credito', false, goog.bind(this.updateNote, this, supplier));

    if(goog.array.contains(privs, pnc.client.Privileges.CREDIT_NOTE_REMOVE))
        pnc.ui.createToolbarButton(actionGroup, 'note-remove-button', pnc.ui.Resources.REMOVE, 'Remover Nota de Credito', false, goog.bind(this.removeNote, this, supplier));

    var backButton     = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras',  false);
    var forwardButton  = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);
    this.notePaginator = new pnc.ui.Paginator(['Fecha', 'Referencia', 'Monto', 'Redimido'], [], 
                        'table-condensed table-bordered summary-table table-striped', 20, backButton, forwardButton);
    this.notePaginator.setRowRenderer(goog.bind(this.renderNote, this, supplier));

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                self.notePaginator.setData(response.content());
                self.notePaginator.render(content);
                break;

            case pnc.client.Status.NOT_FOUND:
                self.notePaginator.setData([]);
                self.notePaginator.render(content);
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                break;
        }
    };
    this.client.getNotes(supplier, callback);
};

/**
 * Paginator callback used to render a credit note as a row.
 * @param  {pnc.model.Supplier}   supplier  Supplier for which the credit note will be presented
 * @param  {Element}              parent    Parent element where the row will be placed in
 * @param  {pnc.model.CreditNote} note      Credit not to represent
 * @private
 */
pnc.ui.SupplierView.prototype.renderNote = function(supplier, parent, note) {
    var row = goog.dom.createDom('tr');
    row.dataset.note = note.getId().toString();
    goog.dom.appendChild(parent, row);

    var data = [
        pnc.ui.DATE_FORMATTER.format(note.getTimestamp()),
        note.getReference(),
        pnc.ui.DECIMAL_FORMATTER.format(note.getAmount()),
        ''
    ];

    for(var cont = 0, size = data.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', null, data[cont]);
        goog.dom.appendChild(row, cell);
        cell.style.width = 'initial';
    }

    var amountCell = row.children[row.children.length - 2];
    amountCell.style.textAlign = 'right';

    var redeemedCell = row.children[row.children.length - 1];
    redeemedCell.style.width       = '28px';
    redeemedCell.style.paddingLeft = '5px';
    redeemedCell.style.textAlign   = 'center';

    if(note.isRedeemed()) {
        var img = goog.dom.createDom('img', {'src' : pnc.ui.Resources.RESULT, 'title' : 'Redimido'});
        goog.dom.appendChild(redeemedCell, img);
    }

    var self         = this;
    var editButton   = goog.dom.getElement('note-update-button');
    var removeButton = goog.dom.getElement('note-remove-button');
    var callback = function(note, e) {
        var rows = goog.dom.getElementsByTagNameAndClass('tr', null, parent);

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        if(goog.isDefAndNotNull(editButton))
            editButton.disabled = false;

        if(goog.isDefAndNotNull(removeButton))
            removeButton.disabled = false;
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, note));
};

/**
 * Provides the user with a panel that is used to add a new credit note
 * @param {pnc.model.Supplier} supplier  Supplier for which the note will be added
 * @private
 */
pnc.ui.SupplierView.prototype.addNote = function(supplier) {
    var content     = goog.dom.createDom('div');
    var inputs      = this.createNoteForm(content, null);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        };

        self.notePaginator.getData().push(response.content());
        goog.array.sort(self.notePaginator.getData(), pnc.model.CreditNote.compare);
        self.notePaginator.refresh();
        cancelCallback();
        pnc.ui.Alert.success('La Nota de Credito fue agregada');
    };

    var saveCallback = function() {
        if(!self.validateNoteForm(inputs))
            return;

        var newNote = self.parseNoteForm(inputs);
        newNote.setIdSupplier(supplier.getId());
        self.client.addNote(supplier, newNote, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Agregar Nota de Credito', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Agregar');
};

/**
 * Provides the user with a panel that is used to update an existing credit note
 * @param  {pnc.model.Supplier} supplier  Supplier that holds the note that will be updated
 * @private
 */
pnc.ui.SupplierView.prototype.updateNote = function(supplier) {
    var row         = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.notePaginator.getElement())[0];
    var note        = pnc.model.findModel(this.notePaginator.getData(), row.dataset.note);
    var redeemed    = note.isRedeemed();
    var content     = goog.dom.createDom('div');
    var inputs      = this.createNoteForm(content, note);
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var cancelButton = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancelButton);
    var cancelCallback = function() {
        slider.dispose();
        pnc.ui.Main.breadcrumbPop();
    };
    goog.events.listen(cancelButton, goog.events.EventType.CLICK, cancelCallback);

    var saveButton = goog.dom.createDom('button', 'btn btn-primary', 'Guardar');
    goog.dom.appendChild(controlPane, saveButton);
    var self = this;

    var responseCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        var newNote = response.content();
        pnc.model.replaceModel(self.notePaginator.getData(), newNote);
        self.notePaginator.refresh();

        if(newNote.isRedeemed() && !redeemed) {
            var cell  = goog.dom.getElement('unredeemed-credit-note-cell');
            var value = pnc.ui.DECIMAL_FORMATTER.parse(goog.dom.getTextContent(cell));
            value    -= newNote.getAmount();
            goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(value));
        } else if(!newNote.isRedeemed() && redeemed) {
            var cell  = goog.dom.getElement('unredeemed-credit-note-cell');
            var value = pnc.ui.DECIMAL_FORMATTER.parse(goog.dom.getTextContent(cell));
            value    += newNote.getAmount();
            goog.dom.setTextContent(cell, pnc.ui.DECIMAL_FORMATTER.format(value));
        }

        cancelCallback();
        pnc.ui.Alert.success('La orden fue actualizada');

        var buttons = [ 
            goog.dom.getElement('note-update-button'),
            goog.dom.getElement('note-remove-button')
        ];

        for(var cont = 0, size = buttons.length; cont < size; cont++)
            if(goog.isDefAndNotNull(buttons[cont]))
                buttons[cont].disabled = true;
    };

    var saveCallback = function() {
        if(!self.validateNoteForm(inputs))
            return;

        var newNote = self.parseNoteForm(inputs, note);
        newNote.setIdSupplier(supplier.getId());
        self.client.updateNote(newNote, responseCallback);
    };
    goog.events.listen(saveButton, goog.events.EventType.CLICK, saveCallback);

    var slider = new pnc.ui.SlidePanel('Editar', pnc.ui.Resources.EDIT, content);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Editar');
};

/**
 * Provides the user with a confirmation dialog. If confirmed, executes the transaction to remove the credit note.
 * @param  {pnc.model.Supplier} supplier  Supplier that holds the credit note that will be removed
 * @private
 */
pnc.ui.SupplierView.prototype.removeNote = function(supplier) {
    var row    = goog.dom.getElementsByTagNameAndClass('tr', 'info', this.notePaginator.getElement())[0];
    var note   = pnc.model.findModel(this.notePaginator.getData(), row.dataset.note);
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que desea remover esta nota de credito?');
    var self   = this;

    var responseCallback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('Esta nota de creidot tiene referencias a otra data. No puede ser removida.').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        pnc.model.removeModel(self.notePaginator.getData(), note);
        self.notePaginator.refresh();
        pnc.ui.Alert.success('La nota de credito fue removida');
    };

    var selectCallback = function(e) {
        if(e.key !== pnc.ui.SlideDialog.Keys.OK)
            return;

        self.client.removeNote(note, responseCallback);
    };
    goog.events.listen(dialog.getElement(), goog.ui.Dialog.EventType.SELECT, selectCallback);
    dialog.open();
};

/**
 * Creates a form that can be used to add new credit notes, as well as update existing ones.
 * @param  {Element}              parent  Parent element where the form will be placed in
 * @param  {pnc.model.CreditNote} note    Optiona credit note. If provided its details will be pre-populated into the form.
 * @private
 */
pnc.ui.SupplierView.prototype.createNoteForm = function(parent, note) {
    var inputs = {};
    var fields = {
        'note-date'      : ['Fecha',      'date',     'No puede estar vacio'],
        'note-reference' : ['Referencia', 'text',     null],
        'note-amount'    : ['Cantidad',   'text',     'Debe ser un numero mayor a cero'],
        'note-reason'    : ['Razon',      'textarea', null]
    };

    for(var key in fields) {
        var group = goog.dom.createDom('div', 'control-group');
        goog.dom.appendChild(parent, group);

        var name  = fields[key][0];
        var label = goog.dom.createDom('label', {'for' : key, 'style' : 'width: 100px'}, name);
        goog.dom.appendChild(group, label);

        if(fields[key][1] === 'textarea')
            inputs[key] = goog.dom.createDom('textarea', {'id' : key, 'class' : 'span4', 'style' : 'height: 60px'});
        else
            inputs[key] = goog.dom.createDom('input', {'id' : key, 'class' : 'span2', 'type' : fields[key][1]});

        goog.dom.appendChild(group, inputs[key]);
        var help = fields[key][2];

        if(!goog.isNull(help)) {
            var help = goog.dom.createDom('span', 'help-inline', help);
            goog.dom.appendChild(group, help);
        }
    }

    pnc.ui.assureNumbers(inputs['note-amount']);

    var group = goog.dom.createDom('div', 'control-group');
    goog.dom.appendChild(parent, group);
    var label = goog.dom.createDom('label', {'for' : 'note-redeemed', 'style' : 'width: 80px'}, 'Redimido');
    goog.dom.appendChild(group, label);
    inputs['note-redeemed'] = goog.dom.createDom('input', {'id' : 'note-redeemed', 'type' : 'checkbox', 
                                'style' : 'width: 15px; height: 15px; margin: -10px 5px 0 0'});
    goog.dom.appendChild(group, inputs['note-redeemed']);
    
    inputs['note-redeemed-date'] = goog.dom.createDom('input', {'id' : 'note-redeemed-date', 'class' : 'span2', 'type' : 'date'});
    goog.dom.appendChild(group, inputs['note-redeemed-date']);

    var callback = function(e) {
        inputs['note-redeemed-date'].value    = e.target.checked ? pnc.model.DATE_FORMATTER.format(new Date()) : '';
        inputs['note-redeemed-date'].disabled = !e.target.checked;
    };
    goog.events.listen(inputs['note-redeemed'], goog.events.EventType.CLICK, callback);

    if(goog.isDefAndNotNull(note)) {
        inputs['note-date'].value       = goog.isDefAndNotNull(note.getTimestamp()) 
                                        ? pnc.model.DATE_FORMATTER.format(note.getTimestamp()) 
                                        : '';
        inputs['note-reference'].value  = note.getReference();
        inputs['note-amount'].value     = pnc.ui.DECIMAL_FORMATTER.format(note.getAmount());
        inputs['note-reason'].value     = note.getReason();
        inputs['note-redeemed'].checked = note.isRedeemed();

        if(note.isRedeemed())
            inputs['note-redeemed-date'].value = pnc.model.DATE_FORMATTER.format(note.getRedeemedDate());
        else
            inputs['note-redeemed-date'].disabled = true;
    } else {
        inputs['note-date'].value = pnc.model.DATE_FORMATTER.format(new Date());
        inputs['note-redeemed-date'].disabled = true;
    }

    return inputs;
};

/**
 * Validates the contents of the credit note form. If errors are found, they will be highlighted within the form inline.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @private
 */
pnc.ui.SupplierView.prototype.validateNoteForm = function(inputs) {
    var errors   = goog.dom.getElementsByClass('error', inputs['note-date'].parentElement.parentElement);

    for(var cont = 0, size = errors.length; cont < size; cont++)
        goog.dom.classes.remove(errors[cont], 'error');

    if(inputs['note-date'].value.length < 1)
        goog.dom.classes.add(inputs['order-date'].parentElement, 'error');

    if(inputs['note-amount'].value.length < 1 || !pnc.ui.FLOAT_RGX.test(inputs['note-amount'].value) || inputs['note-amount'].value <= 0)
        goog.dom.classes.add(inputs['note-amount'].parentElement, 'error');

    if(inputs['note-redeemed'].checked && inputs['note-redeemed-date'].value.length < 1)
        goog.dom.classes.add(inputs['note-redeemed'].parentElement, 'error');

    errors = goog.dom.getElementsByClass('error', inputs['note-date'].parentElement.parentElement);
    return errors.length < 1;
};

/**
 * Parses the credit note form to obtain a credit note model from its contents.
 * @param  {Object.<string, Element>} inputs  Form input elements
 * @param  {pnc.model.CreditNote}     note    Optional credit note. If provided, it will be updated, instead of creating
 *                                            a new credit note model.
 * @return {pnc.model.CreditNote}             Credit note derived from form.
 * @private
 */
pnc.ui.SupplierView.prototype.parseNoteForm = function(inputs, note) {
    note = goog.isDefAndNotNull(note) ? note : new pnc.model.CreditNote();
    var timestamp = new Date();
    pnc.model.DATE_PARSER.parse(inputs['note-date'].value, timestamp);
    note.setTimestamp(timestamp);
    note.setReference(inputs['note-reference'].value);
    note.setAmount(pnc.ui.DECIMAL_FORMATTER.parse(inputs['note-amount'].value));
    note.setReason(inputs['note-reason'].value);
    note.setRedeemed(inputs['note-redeemed'].checked);

    if(note.isRedeemed() && inputs['note-redeemed-date'].value.length > 0) {
        var redeemedDate = new Date();
        pnc.model.DATE_PARSER.strictParse(inputs['note-redeemed-date'].value, redeemedDate);
        redeemedDate.setHours(0); redeemedDate.setMinutes(0); redeemedDate.setSeconds(0);
        note.setRedeemedDate(redeemedDate);
    }

    return note;
};
