goog.provide('pnc.ui.Paginator');

goog.require('goog.dom');
goog.require('goog.dom.classes');

/**
 * Generic UI control used to paginate through a potential large number of elements. The control will receive a number
 * of elements and let the user page through them. All elements must be fetched before hand. There are no navigation
 * buttons included. This is by design. The invoker will navigate via the next and previous methods.
 * @param  {Array.<string>} titles    Titles to place in the table header. It is expected that all the data array will
 *                                        match the width of the titles.
 * @param  {Array}          data      The data array. The invoker can pass any type of object, as long as a row render
 *                                        is set (setRowRenderer method). Otherwise an array of arrays is expected with
 *                                        a matrix of strings.
 * @param  {string}         styles    A string with one or more class names to apply to the table
 * @param  {number}         pageSize  The number of elements that will be displayed in one page.
 * @constructor
 */
pnc.ui.Paginator = function(titles, data, styles, pageSize, backButton, forwardButton) {
    /**
     * Titles to place in the table header. It is expected that all the data array will match the width of the titles.
     * @type {Array.<string>}
     * @private
     */
    this.titles        = goog.isDefAndNotNull(titles)   ? titles        : [];

    /**
     * The data array. The invoker can pass any type of object, as long as a row render is set (setRowRenderer method). 
     * Otherwise an array of arrays is expected with a matrix of strings.
     * @type {Array}
     * @private
     */
    this.data          = goog.isDefAndNotNull(data)     ? data          : [];

    /**
     * A string with one or more class names to apply to the table
     * @type {Array}
     * @private
     */
    this.styles        = goog.isDefAndNotNull(styles)   ? styles        : null;

    /**
     * The number of elements that will be displayed in one page.
     * @type {number}
     * @private
     */
    this.pageSize      = goog.isDefAndNotNull(pageSize) ? pageSize      : pnc.ui.PAGE_SIZE;

    /**
     * If provided by the invoker, this button will be used to navigate the paginator backwards
     * @type {Element}
     * @private
     */
    this.backButton    = goog.isDef(backButton)         ? backButton    : null;

    /**
     * If provided by the invoker, this button will be used to navigate the paginator forwards
     * @type {Element}
     * @private
     */
    this.forwardButton = goog.isDef(forwardButton)      ? forwardButton : null;

    /**
     * Body of paginator table
     * @type {Element}
     * @private
     */
    this.body          = null;

    /**
     * Keeps track of the current page being shown in the paginator
     * @type {Number}
     * @private
     */
    this.currentPage   = -1;

    /**
     * Function that will render an item within the paginator. The function is passed the parent element where the
     * representation will be placed, and the object to represent.
     * @type {Function(Element, *)}
     * @private
     */
    this.rowRenderer   = null;
};

/**
 * Generic renderer which renders an array
 * @param  {Element} parent [description]
 * @param  {Array} item   [description]
 */
pnc.ui.Paginator.arrayRenderer = function(parent, item) {
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);

    for(var cont = 0, size = item.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', null, item[cont]);
        goog.dom.appendChild(row, cell);
    }
};

/**
 * Rendersthe paginator.
 * @param  {Element} parent  Parent element where the paginator will be placed
 */
pnc.ui.Paginator.prototype.render = function(parent) {
    this.body = pnc.ui.createTable(parent, this.styles, this.titles);
    this.body.parentElement.style.marginBottom = '0';
    var self = this;
    
    if(this.data.length < 1) {
        var row  = goog.dom.createDom('tr', 'warning');
        goog.dom.appendChild(this.body, row);
        var message = 'No hay elementos disponibles para mostrar.';
        var cell = goog.dom.createDom('td', {'colspan' : this.titles.length.toString(), 'style' : 'font-text : bold; text-align: center'}, message);
        goog.dom.appendChild(row, cell);
    } else {
        this.next();
    }

    if(goog.isDefAndNotNull(this.backButton)) {
        this.backButton.disabled = true;

        var callback = function(e) {
            var result               = self.previous();
            self.backButton.disabled = !result;
            
            if(goog.isDefAndNotNull(self.forwardButton))
                self.forwardButton.disabled = false;
        };
        goog.events.listen(this.backButton, goog.events.EventType.CLICK, callback);
    }

    if(goog.isDefAndNotNull(this.forwardButton)) {
        this.forwardButton.disabled = this.data.length <= this.pageSize;

        var callback = function(e) {
            var result               = self.next();
            self.backButton.disabled = false;

            if(goog.isDefAndNotNull(self.forwardButton))
                self.forwardButton.disabled = !result;
        };
        goog.events.listen(this.forwardButton, goog.events.EventType.CLICK, callback);
    }
};

/**
 * Cleans up resources and removes the paginator from the DOM
 */
pnc.ui.Paginator.prototype.dispose = function() {
    if(goog.isDefAndNotNull(this.body) && goog.isDefAndNotNull(this.body.parentElement))
        goog.dom.removeNode(this.body.parentElement);
};

/**
 * Returns the element that represents the paginator on the DOM
 * @return {Element} Paginator element
 */
pnc.ui.Paginator.prototype.getElement = function() {
    return this.body.parentElement;
};

/**
 * Returns the data being used by the paginator
 * @return {Array}
 */
pnc.ui.Paginator.prototype.getData = function() {
    return this.data;
};

/**
 * Establishes the paginator data
 * @param  {Array} data  data
 */
pnc.ui.Paginator.prototype.setData = function(data) {
    this.data = goog.isDefAndNotNull(data) ? data : [];
};

/**
 * Lets the invoker establish a row renderer. This is used if the invoker wants to have the responsability of determining
 * how rows will be rendered. The input must be a function that will receive a parent element and the item to be rendered.
 * The type will depend on the data that was provided to the constructor at object creation time.
 * @param  {function(Element,*)} renderer  Render function
 */
pnc.ui.Paginator.prototype.setRowRenderer = function(renderer) {
    this.rowRenderer = renderer;
};

/**
 * Navigates to the next page if possible. Returns a boolean indicating if further navigation in this direction is possible.
 * @return {boolean}  Determines if further navigation in this direction is possible.
 */
pnc.ui.Paginator.prototype.next = function() {
    if(this.currentPage > -1 && (this.currentPage * this.pageSize) > this.data.length)
        return false;
    
    this.currentPage++;
    var min = this.currentPage * this.pageSize;
    var max = Math.min(this.data.length, min + this.pageSize);
    goog.dom.removeChildren(this.body);

    for(var cont = min; cont < max; cont++)
        this.renderRow(this.data[cont]);

    if(goog.isDefAndNotNull(this.backButton))
        this.backButton.disabled = false;

    if(goog.isDefAndNotNull(this.forwardButton))
        this.forwardButton.disabled = this.currentPage >= (Math.ceil(this.data.length / this.pageSize) - 1);

    return this.currentPage < (Math.ceil(this.data.length / this.pageSize) - 1);
};

/**
 * Navigates to the previous page if possible. Returns a boolean indicating if further navigation in this direction is possible.
 * @return {boolean}  Determines if further navigation in this direction is possible.
 */
pnc.ui.Paginator.prototype.previous = function() {
    if(this.currentPage === 0)
        return false;

    this.currentPage--;
    var min = this.currentPage * this.pageSize;
    var max = min + this.pageSize;
    goog.dom.removeChildren(this.body);

    for(var cont = min; cont < max; cont++)
        this.renderRow(this.data[cont]);

    if(goog.isDefAndNotNull(this.backButton))
        this.backButton.disabled = this.currentPage <= 0;

    if(goog.isDefAndNotNull(this.forwardButton))
        this.forwardButton.disabled = false;

    return this.currentPage > 0;
};

/**
 * Refreshes the content displayed in the paginator. Uses the current page.
 */
pnc.ui.Paginator.prototype.refresh = function() {
    this.currentPage = this.currentPage > -1 ? this.currentPage : 0;
    var min = Math.max(this.currentPage * this.pageSize, 0);
    var max = Math.min(this.data.length, min + this.pageSize);
    goog.dom.removeChildren(this.body);

    for(var cont = min; cont < max; cont++)
        this.renderRow(this.data[cont]);

    if(this.data.length < 1) {
        var row  = goog.dom.createDom('tr', 'warning');
        goog.dom.appendChild(this.body, row);
        var message = 'No hay elementos disponibles para mostrar.';
        var cell = goog.dom.createDom('td', {'colspan' : this.titles.length.toString(), 'style' : 'font-text : bold; text-align: center'}, message);
        goog.dom.appendChild(row, cell);
    }

    if(goog.isDefAndNotNull(this.backButton))
        this.backButton.disabled = this.currentPage <= 0;

    if(goog.isDefAndNotNull(this.forwardButton))
        this.forwardButton.disabled = this.currentPage >= (Math.ceil(this.data.length / this.pageSize) - 1);
};

/**
 * Renders an item as a table row
 * @param  {*} item  Item to render
 * @private
 */
pnc.ui.Paginator.prototype.renderRow = function(item) {
    if(goog.isDefAndNotNull(this.rowRenderer)) {
        this.rowRenderer(this.body, item);
        return;
    }        

    if(!goog.isArray(item))
        return;

    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(this.body, row);

    for(var cont = 0, size = item.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', {}, item[cont]);
        goog.dom.appnendChild(row, cell);
    }        
};
