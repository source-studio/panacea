goog.provide('pnc.ui.ReportView');

goog.require('goog.dom');
goog.require('goog.dom.classes');
goog.require('goog.events');
goog.require('goog.object');

goog.require('pnc.client.Profile');
goog.require('pnc.client.Report');
goog.require('pnc.client.Supplier');
goog.require('pnc.model.Report');
goog.require('pnc.model.Supplier');
goog.require('pnc.ui');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');

/**
 * UI View used to generate and consume report data
 * @constructor
 */
pnc.ui.ReportView = function() {
    this.client = new pnc.client.Report();

    //----------- elements -----------//
    this.container   = null;
    this.disposables = [];
    this.supplierMap = {};
};
pnc.ui.Main.registerModule('reports', pnc.ui.ReportView);

/**
 * Pallette of colors used to create report charts
 * @type {Array.<string>}
 * @private
 */
pnc.ui.ReportView.pallette = ['#5EDB9F', '#62A7D4', '#FFBF6D'];


/**
 * Renders the report view on screen. This will provide the user with a control that will be used to select which report
 * will be rendered, and provide any required criteria the report may need to run
 * @param  {Element} parent  Parent element where the report view will be placed
 */
pnc.ui.ReportView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left : 0px'});
    goog.dom.appendChild(parent, this.container);

    this.renderReportBar();
    this.renderContentPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
};

/**
 * Disposes of all the resources used by the report view, and removes it from the DOM
 */
pnc.ui.ReportView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};


/**
 * Renders the selection bar. This is the top area where the users will select which report is going to be run, and if
 * necessary provides execution arguments.
 * @private
 */
pnc.ui.ReportView.prototype.renderReportBar = function() {
    var bar = goog.dom.createDom('div', {'id' : 'report-bar', 'class' : 'report-bar span10'});
    goog.dom.appendChild(this.container, bar);

    var row = goog.dom.createDom('div', {'id' : 'main-menu'});
    goog.dom.appendChild(bar, row);

    var stateButton = goog.dom.createDom('button', 'btn', 'Estado');
    goog.dom.appendChild(row, stateButton);
    goog.events.listen(stateButton, goog.events.EventType.CLICK, goog.bind(this.generateState, this));

    var activityButton = goog.dom.createDom('button', 'btn', 'Actividad');
    goog.dom.appendChild(row, activityButton);
    goog.events.listen(activityButton, goog.events.EventType.CLICK, goog.bind(this.renderActivityMenu, this));

    var inventoryButton = goog.dom.createDom('button', 'btn', 'Inventario');
    goog.dom.appendChild(row, inventoryButton);
    goog.events.listen(inventoryButton, goog.events.EventType.CLICK, goog.bind(this.renderInventoryMenu, this));
};

/**
 * Renders the content pane. This is where the generated report data will be displayed.
 * @private
 */
pnc.ui.ReportView.prototype.renderContentPane = function() {
    var content = goog.dom.createDom('div', {'id' : 'report-content', 'class' : 'report-content'});
    goog.dom.appendChild(this.container, content);

    var message = goog.dom.createDom('h2', null, 'Elije un reporte para iniciar');
    goog.dom.appendChild(content, message);
};


/**
 * Generates the state report.
 * @private
 */
pnc.ui.ReportView.prototype.generateState = function() {
    this.reset(1);
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Estado');

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var total  = report['inventory'] + report['credit'] + report['notes'];
        var data   = [
            ['Inventario',                  pnc.ui.DECIMAL_FORMATTER.format(report['inventory'])],
            ['Credito por Cobrar',          pnc.ui.DECIMAL_FORMATTER.format(report['credit'])],
            ['Notas de Credito por Cobrar', pnc.ui.DECIMAL_FORMATTER.format(report['notes'])],
            ['Total',                       pnc.ui.DECIMAL_FORMATTER.format(total)]
        ];
        var body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Activos', data);
        
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            ['Ordenes por Pagar', pnc.ui.DECIMAL_FORMATTER.format(report['orders'])],
            ['Total',             pnc.ui.DECIMAL_FORMATTER.format(report['orders'])]
        ];
        body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Pasivos', data);

        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            {'value' : report['inventory'], 'label' : 'Inventario', 'color' : pnc.ui.ReportView.pallette[0]},
            {'value' : report['credit'],    'label' : 'Credito',    'color' : pnc.ui.ReportView.pallette[1]},
            {'value' : report['notes'],     'label' : 'Notas',      'color' : pnc.ui.ReportView.pallette[2]}
        ];
        var graph = goog.dom.createDom('div', {'class' : 'graph', 'style' : 'display: inline-block; width : 50%'});
        goog.dom.appendChild(content, graph);
        var canvas = goog.dom.createDom('canvas', {'width' : '400', 'height' : '200'});
        goog.dom.appendChild(graph, canvas);
        new Chart(canvas.getContext('2d')).Pie(data);
        self.renderLegend(graph, data);

        data = [
            {'value' : report['orders'],   'label' : 'Ordenes por Pagar', 'color' : pnc.ui.ReportView.pallette[0]}
        ];

        graph = goog.dom.createDom('div', {'class' : 'graph', 'style' : 'display: inline-block; width : 50%'});
        goog.dom.appendChild(content, graph);
        canvas = goog.dom.createDom('canvas', {'width' : '400', 'height' : '200'});
        goog.dom.appendChild(graph, canvas);
        new Chart(canvas.getContext('2d')).Pie(data);
        self.renderLegend(graph, data);
    };
    this.client.generateState(callback);
};

/**
 * Generates the date range driven activity report
 * @private
 */
pnc.ui.ReportView.prototype.generateActivity = function() {
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Actividad');
    pnc.ui.Main.breadcrumbPush('Rango');
    var fromInput = goog.dom.getElement('from-date-input');
    var toInput   = goog.dom.getElement('to-date-input');

    if(fromInput.value.length < 1)
        goog.dom.classes.add(fromInput, 'validation-error');

    if(toInput.value.length < 1)
        goog.dom.classes.add(toInput, 'validation-error');

    if(fromInput.value.length < 1 || toInput.value.length < 1)
        return;

    goog.dom.classes.remove(fromInput, 'validation-error');
    goog.dom.classes.remove(toInput, 'validation-error');

    var fromDate = new Date();
    var toDate   = new Date();
    pnc.model.DATE_PARSER.parse(fromInput.value, fromDate);
    pnc.model.DATE_PARSER.parse(toInput.value, toDate);

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var total  = report['cash'] + report['payments'] + report['deposits'];
        var data = [
            ['Ventas Contado',  pnc.ui.DECIMAL_FORMATTER.format(report['cash'])],
            ['Pagos a Credito', pnc.ui.DECIMAL_FORMATTER.format(report['payments'])],
            ['Depositos',       pnc.ui.DECIMAL_FORMATTER.format(report['deposits'])],
            ['Total',           pnc.ui.DECIMAL_FORMATTER.format(total)]
        ];

        var body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Entradas', data);
        
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        total = report['credit'] + report['expenses'];
        data = [
            ['Ventas a Credito', pnc.ui.DECIMAL_FORMATTER.format(report['credit'])],
            ['Gastos',           pnc.ui.DECIMAL_FORMATTER.format(report['expenses'])],
            ['Total',            pnc.ui.DECIMAL_FORMATTER.format(total)]
        ];

        body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Salidas', data);
                
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            {'value' : report['cash'],     'label' : 'Ventas Contado',  'color' : pnc.ui.ReportView.pallette[0]},
            {'value' : report['payments'], 'label' : 'Pagos a Credito', 'color' : pnc.ui.ReportView.pallette[1]},
            {'value' : report['deposits'], 'label' : 'Depositos',       'color' : pnc.ui.ReportView.pallette[2]}
        ];
        var assetGraph = goog.dom.createDom('div', {'class' : 'graph', 'style' : 'display: inline-block; width : 50%'});
        goog.dom.appendChild(content, assetGraph);
        var canvas = goog.dom.createDom('canvas', {'width' : '400', 'height' : '200'});
        goog.dom.appendChild(assetGraph, canvas);
        new Chart(canvas.getContext('2d')).Pie(data);
        self.renderLegend(assetGraph, data);

        data = [
            {'value' : report['credit'],   'label' : 'Ventas Credito', 'color' : pnc.ui.ReportView.pallette[0]},
            {'value' : report['expenses'], 'label' : 'Gastos',         'color' : pnc.ui.ReportView.pallette[1]}
        ];

        var liabilityGraph = goog.dom.createDom('div', {'class' : 'graph', 'style' : 'display: inline-block; width : 50%'});
        goog.dom.appendChild(content, liabilityGraph);
        canvas = goog.dom.createDom('canvas', {'width' : '400', 'height' : '200'});
        goog.dom.appendChild(liabilityGraph, canvas);
        new Chart(canvas.getContext('2d')).Pie(data);
        self.renderLegend(liabilityGraph, data);
    };
    this.client.generateActivity(fromDate, toDate, callback);
};

/**
 * Generates the monthly activity report. 
 * @private
 */
pnc.ui.ReportView.prototype.generateActivityMonth = function() {
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Actividad');
    pnc.ui.Main.breadcrumbPush('Mes');

    var monthInput = goog.dom.getElement('month-input');
    var yearInput  = goog.dom.getElement('year-input');

    if(monthInput.value.length < 1)
        goog.dom.classes.add(monthInput, 'validation-error');

    if(yearInput.value.length < 1)
        goog.dom.classes.add(yearInput, 'validatoin-error');

    if(monthInput.value.length < 1 || yearInput.value.length < 1)
        return;

    var fromDate = new Date(yearInput.value, monthInput.value - 1, 1);
    var toDate   = new Date(yearInput.value, monthInput.value, 1);

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var cashTotal = 0, paymentTotal = 0, depositTotal = 0, creditTotal = 0, expenseTotal = 0;

        for(var date in report['cash'])
            cashTotal += report['cash'][date];

        for(var date in report['payments'])
            paymentTotal += report['payments'][date];

        for(var date in report['deposits'])
            depositTotal += report['deposits'][date];

        for(var date in report['credit'])
            creditTotal += report['credit'][date];

        for(var date in report['expenses'])
            expenseTotal += report['expenses'][date];

        var data = [
            ['Ventas a Contado', pnc.ui.DECIMAL_FORMATTER.format(cashTotal)],
            ['Pagos a Credito',  pnc.ui.DECIMAL_FORMATTER.format(paymentTotal)],
            ['Depositos',        pnc.ui.DECIMAL_FORMATTER.format(depositTotal)],
            ['Total',            pnc.ui.DECIMAL_FORMATTER.format(cashTotal + paymentTotal + depositTotal)]
        ];

        var body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Entradas', data);
        
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            {'label' : 'Ventas Contado',  'color' : pnc.ui.ReportView.pallette[0]},
            {'label' : 'Pagos a Credito', 'color' : pnc.ui.ReportView.pallette[1]},
            {'label' : 'Depositos',       'color' : pnc.ui.ReportView.pallette[2]}
        ];

        self.renderLegend(content, data, true);

        var days = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', 
                '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
        data = {
            'labels' : days,
            'datasets' : [
                {
                    'fillColor'        : 'rgba(94, 219, 159,0.3)',
                    'strokeColor'      : 'rgba(94, 219, 159,1)',
                    'pointColor'       : 'rgba(94, 219, 159,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillDays(report['cash'])
                },
                {
                    'fillColor'        : 'rgba(98, 167, 212,0.3)',
                    'strokeColor'      : 'rgba(98, 167, 212,1)',
                    'pointColor'       : 'rgba(98, 167, 212,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillDays(report['payments'])
                },
                {
                    'fillColor'        : 'rgba(255, 191, 109,0.3)',
                    'strokeColor'      : 'rgba(255, 191, 109,1)',
                    'pointColor'       : 'rgba(255, 191, 109,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillDays(report['deposits'])
                }
            ]
        };
        var graph = goog.dom.createDom('div', 'graph');
        goog.dom.appendChild(content, graph);
        var canvas = goog.dom.createDom('canvas', {'width' : '700', 'height' : '350'});
        goog.dom.appendChild(graph, canvas);
        new Chart(canvas.getContext('2d')).Line(data);

        var ruler = goog.dom.createDom('hr');
        goog.dom.appendChild(content, ruler);

        data = [
            ['Ventas a Credito', pnc.ui.DECIMAL_FORMATTER.format(creditTotal)],
            ['Gastos',           pnc.ui.DECIMAL_FORMATTER.format(expenseTotal)],
            ['Total',            pnc.ui.DECIMAL_FORMATTER.format(creditTotal + expenseTotal)]
        ];

        body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Salidas', data);
        
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            {'label' : 'Ventas a Credito', 'color' : pnc.ui.ReportView.pallette[0]},
            {'label' : 'Gastos',           'color' : pnc.ui.ReportView.pallette[1]}
        ];

        self.renderLegend(content, data, true);

        data = {
            'labels' : days,
            'datasets' : [
                {
                    'fillColor'        : 'rgba(94, 219, 159,0.3)',
                    'strokeColor'      : 'rgba(94, 219, 159,1)',
                    'pointColor'       : 'rgba(94, 219, 159,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillDays(report['credit'])
                },
                {
                    'fillColor'        : 'rgba(98, 167, 212,0.3)',
                    'strokeColor'      : 'rgba(98, 167, 212,1)',
                    'pointColor'       : 'rgba(98, 167, 212,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillDays(report['expenses'])
                }
            ]
        };
        graph = goog.dom.createDom('div', 'graph');
        goog.dom.appendChild(content, graph);
        canvas = goog.dom.createDom('canvas', {'width' : '700', 'height' : '350'});
        goog.dom.appendChild(graph, canvas);
        new Chart(canvas.getContext('2d')).Line(data);
    };
    this.client.generateActivityMonth(fromDate, toDate, callback);
};

/**
 * Generates the yearly activity report.
 * @private
 */
pnc.ui.ReportView.prototype.generateActivityYear = function() {
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Actividad');
    pnc.ui.Main.breadcrumbPush('Año');

    var yearInput  = goog.dom.getElement('year-input');

    if(yearInput.value.length < 1) {
        goog.dom.classes.add(yearInput, 'validation-error');
        return;
    }

    var fromDate = new Date(yearInput.value, 0, 1);
    var toDate   = new Date(goog.string.toNumber(yearInput.value) + 1, 0, 1);

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var cashTotal = 0, paymentTotal = 0, depositTotal = 0, creditTotal = 0, expenseTotal = 0;

        for(var date in report['cash'])
            cashTotal += report['cash'][date];

        for(var date in report['payments'])
            paymentTotal += report['payments'][date];

        for(var date in report['deposits'])
            depositTotal += report['deposits'][date];

        for(var date in report['credit'])
            creditTotal += report['credit'][date];

        for(var date in report['expenses'])
            expenseTotal += report['expenses'][date];

        var data = [
            ['Ventas a Contado', pnc.ui.DECIMAL_FORMATTER.format(cashTotal)],
            ['Pagos a Credito',  pnc.ui.DECIMAL_FORMATTER.format(paymentTotal)],
            ['Depositos',        pnc.ui.DECIMAL_FORMATTER.format(depositTotal)],
            ['Total',            pnc.ui.DECIMAL_FORMATTER.format(cashTotal + paymentTotal + depositTotal)]
        ];

        var body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Entradas', data);
        
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            {'label' : 'Ventas Contado',  'color' : pnc.ui.ReportView.pallette[0]},
            {'label' : 'Pagos a Credito', 'color' : pnc.ui.ReportView.pallette[1]},
            {'label' : 'Depositos',       'color' : pnc.ui.ReportView.pallette[2]}
        ];

        self.renderLegend(content, data, true);

        var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiempbre', 'Octubre',
                      'Noviembre', 'Diciembre'];
        data = {
            'labels' : months,
            'datasets' : [
                {
                    'fillColor'        : 'rgba(94, 219, 159,0.3)',
                    'strokeColor'      : 'rgba(94, 219, 159,1)',
                    'pointColor'       : 'rgba(94, 219, 159,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillMonths(report['cash'])
                },
                {
                    'fillColor'        : 'rgba(98, 167, 212,0.3)',
                    'strokeColor'      : 'rgba(98, 167, 212,1)',
                    'pointColor'       : 'rgba(98, 167, 212,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillMonths(report['payments'])
                },
                {
                    'fillColor'        : 'rgba(255, 191, 109,0.3)',
                    'strokeColor'      : 'rgba(255, 191, 109,1)',
                    'pointColor'       : 'rgba(255, 191, 109,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillMonths(report['deposits'])
                }
            ]
        };
        var graph = goog.dom.createDom('div', 'graph');
        goog.dom.appendChild(content, graph);
        var canvas = goog.dom.createDom('canvas', {'width' : '700', 'height' : '350'});
        goog.dom.appendChild(graph, canvas);
        new Chart(canvas.getContext('2d')).Line(data);

        var ruler = goog.dom.createDom('hr');
        goog.dom.appendChild(content, ruler);

        data = [
            ['Ventas a Credito', pnc.ui.DECIMAL_FORMATTER.format(creditTotal)],
            ['Gastos',           pnc.ui.DECIMAL_FORMATTER.format(expenseTotal)],
            ['Total',            pnc.ui.DECIMAL_FORMATTER.format(creditTotal + expenseTotal)]
        ];

        body = pnc.ui.createSummaryTable(content, 'table table-bordered balance-table summary-table', 2, 'Salidas', data);
        
        for(var cont = 0, size = body.lastChild.children.length; cont < size; cont++)
            goog.dom.classes.add(body.lastChild.children[cont], 'bold');

        data = [
            {'label' : 'Ventas a Credito', 'color' : pnc.ui.ReportView.pallette[0]},
            {'label' : 'Gastos',           'color' : pnc.ui.ReportView.pallette[1]}
        ];

        self.renderLegend(content, data, true);

        data = {
            'labels' : months,
            'datasets' : [
                {
                    'fillColor'        : 'rgba(94, 219, 159,0.3)',
                    'strokeColor'      : 'rgba(94, 219, 159,1)',
                    'pointColor'       : 'rgba(94, 219, 159,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillMonths(report['credit'])
                },
                {
                    'fillColor'        : 'rgba(98, 167, 212,0.3)',
                    'strokeColor'      : 'rgba(98, 167, 212,1)',
                    'pointColor'       : 'rgba(98, 167, 212,1)',
                    'pointStrokeColor' : '#fff',
                    'data'             : self.fillMonths(report['expenses'])
                }
            ]
        };
        graph = goog.dom.createDom('div', 'graph');
        goog.dom.appendChild(content, graph);
        canvas = goog.dom.createDom('canvas', {'width' : '700', 'height' : '350'});
        goog.dom.appendChild(graph, canvas);
        new Chart(canvas.getContext('2d')).Line(data);

    };
    this.client.generateActivityYear(fromDate, toDate, callback);
};

/**
 * Generates the general inventory report.
 * @private
 */
pnc.ui.ReportView.prototype.generateInventory = function() {
    this.reset(2);
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Inventario');
    pnc.ui.Main.breadcrumbPush('General');

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var data   = self.aggregateInventoryReport(report);

        var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 0 0 10px 0'});
        goog.dom.appendChild(content, toolbar);
        var navGroup = goog.dom.createDom('div', 'btn-group');
        goog.dom.appendChild(toolbar, navGroup);
        var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
        var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

        var headers   = ['Producto', 'Minimo Deseado', 'Restante', 'Costo', 'Precio'];
        var paginator = new pnc.ui.Paginator(headers, data, 'table table-condensed table-striped table-bordered', 20, backButton, forwardButton);
        self.disposables.push(paginator);
        paginator.setRowRenderer(goog.bind(self.inventoryRenderer, self));
        paginator.render(content);
        paginator.getElement().style.margin = '0';
    };
    this.client.generateInventory(callback);
};

/**
 * Generates the supplier filtered inventory report.
 * @private
 */
pnc.ui.ReportView.prototype.generateInventorySupplier = function() {
    this.reset(3);
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Inventario');
    pnc.ui.Main.breadcrumbPush('Suplidor');

    var input    = goog.dom.getElement('supplier-input');
    var supplier = this.supplierMap[input.value];

    if(!goog.isDefAndNotNull(supplier)) {
        pnc.ui.Alert.error('El suplidor insertado no fue encontrado');
        return;
    }

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                goog.dom.removeNode(loading);
                pnc.ui.Alert.info('No hay records de inventario para este suplidor');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var data   = self.aggregateInventoryReport(report);

        var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 0 0 10px 0'});
        goog.dom.appendChild(content, toolbar);
        var navGroup = goog.dom.createDom('div', 'btn-group');
        goog.dom.appendChild(toolbar, navGroup);
        var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
        var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

        var headers   = ['Producto', 'Minimo Deseado', 'Restante', 'Costo', 'Precio'];
        var paginator = new pnc.ui.Paginator(headers, data, 'table table-condensed table-striped table-bordered', 20, backButton, forwardButton);
        self.disposables.push(paginator);
        paginator.setRowRenderer(goog.bind(self.inventoryRenderer, self));
        paginator.render(content);
        paginator.getElement().style.margin = '0';
    };
    this.client.generateInventorySupplier(supplier, callback);
};

/**
 * Generates the exitence report
 * @private
 */
pnc.ui.ReportView.prototype.generateInventoryExistence = function() {
    this.reset(2);
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Inventario');
    pnc.ui.Main.breadcrumbPush('Por Existencia Minima');

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                goog.dom.removeNode(loading);
                pnc.ui.Alert.info('No hay productos que esten por debajo del minimo de existencia deseado');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var data   = self.aggregateInventoryReport(report);

        var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 0 0 10px 0'});
        goog.dom.appendChild(content, toolbar);
        var navGroup = goog.dom.createDom('div', 'btn-group');
        goog.dom.appendChild(toolbar, navGroup);
        var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
        var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

        var headers   = ['Producto', 'Minimo Deseado', 'Restante', 'Costo', 'Precio'];
        var paginator = new pnc.ui.Paginator(headers, data, 'table table-condensed table-striped table-bordered', 20, backButton, forwardButton);
        self.disposables.push(paginator);
        paginator.setRowRenderer(goog.bind(self.inventoryRenderer, self));
        paginator.render(content);
        paginator.getElement().style.margin = '0';
    };
    this.client.generateInventoryExistence(callback);
};

/**
 * Generates the inventory expiration report
 * @private
 */
pnc.ui.ReportView.prototype.generateInventoryExpiration = function() {
    this.reset(3);
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Reportes');
    pnc.ui.Main.breadcrumbPush('Inventario');
    pnc.ui.Main.breadcrumbPush('Por Expiracion');

    var expiredButton    = goog.dom.getElement('expired-button');
    var notExpiredButton = goog.dom.getElement('not-expired-button');
    var daysInput        = goog.dom.getElement('days-input');

    if(!expiredButton.checked && !notExpiredButton.checked) {
        pnc.ui.SlideDialog.error('Debe elegir un criterio de expiracion para generar el reporte').open();
        return;
    }

    if(notExpiredButton.checked && daysInput.value.length < 1) {
        pnc.ui.SlideDialog.error('Para generar el reporte de producto no expirado, debe insertar la cantidad de dias a expirar').open();
        return;
    }

    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);
    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(content, loading);

    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            case pnc.client.Status.NOT_FOUND:
                goog.dom.removeNode(loading);
                pnc.ui.Alert.info('No hay records de expiracion para el criterio proveido');
                return;

            default:
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
        }

        goog.dom.removeChildren(content);
        var report = response.content().getData();
        var data = [];

        for(var key in report) {
            var row   = [];
            var entry = report[key];
            data.push(row);
            row.push(key);
            row.push(entry['remaining'].toString())
            var entered    = new Date();
            var expiration = new Date();
            pnc.model.DATE_PARSER.parse(entry['entered'], entered);
            pnc.model.DATE_PARSER.parse(entry['expiration'], expiration);
            row.push(pnc.ui.DATE_FORMATTER.format(entered));
            row.push(pnc.ui.DATE_FORMATTER.format(expiration));
        }

        var renderer = function(parent, item) {
            var row = goog.dom.createDom('tr');
            goog.dom.appendChild(parent, row);

            for(var cont = 0, size = item.length; cont < size; cont++) {
                var cell = goog.dom.createDom('td', null, item[cont]);
                goog.dom.appendChild(row, cell);

                if(cont > 0)
                    cell.style.textAlign = 'right';
            }
        };

        var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 0 0 10px 0'});
        goog.dom.appendChild(content, toolbar);
        var navGroup = goog.dom.createDom('div', 'btn-group');
        goog.dom.appendChild(toolbar, navGroup);
        var backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
        var forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

        var headers   = ['Producto', 'Restante', 'Fecha de Entrada', 'Fecha de Expiracion'];
        var paginator = new pnc.ui.Paginator(headers, data, 'table table-condensed table-striped table-bordered', 20, backButton, forwardButton);
        self.disposables.push(paginator);
        paginator.setRowRenderer(renderer);
        paginator.render(content);
        paginator.getElement().style.margin = '0';
    };
    this.client.generateInventoryExpiration(expiredButton.checked, daysInput.value, callback);
};


/**
 * Renders the activity submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderActivityMenu = function() {
    this.reset(1);
    this.setMessage('Elige el tipo de reporte para continuar');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', {'id' : 'activity-submenu', 'class' : 'level2'});
    goog.dom.appendChild(bar, row);

    var title = goog.dom.createDom('h3', null, 'Actividad');
    goog.dom.appendChild(row, title);

    var rangeButton = goog.dom.createDom('button', 'btn', 'Rango');
    goog.dom.appendChild(row, rangeButton);
    goog.events.listen(rangeButton, goog.events.EventType.CLICK, goog.bind(this.renderActivityRangeMenu, this));

    var monthButton = goog.dom.createDom('button', 'btn', 'Mes');
    goog.dom.appendChild(row, monthButton);
    goog.events.listen(monthButton, goog.events.EventType.CLICK, goog.bind(this.renderActivityMonthMenu, this));

    var yearButton = goog.dom.createDom('button', 'btn', 'Año');
    goog.dom.appendChild(row, yearButton);
    goog.events.listen(yearButton, goog.events.EventType.CLICK, goog.bind(this.renderActivityYearMenu, this));
};

/**
 * Renders the activty - range submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderActivityRangeMenu = function() {
    this.reset(2);
    this.setMessage('Elige el rango de fechas');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', {'class' : 'level3'});
    goog.dom.appendChild(bar, row);
    
    var title = goog.dom.createDom('h3', null, 'Rango');
    goog.dom.appendChild(row, title);

    var fromLabel = goog.dom.createDom('label', null, 'Desde');
    goog.dom.appendChild(row, fromLabel);

    var fromInput = goog.dom.createDom('input', {'id' : 'from-date-input', 'type' : 'date'});
    goog.dom.appendChild(row, fromInput);

    var toLabel = goog.dom.createDom('label', null, 'Hasta');
    goog.dom.appendChild(row, toLabel);

    var toInput = goog.dom.createDom('input', {'id' : 'to-date-input', 'type' : 'date'});
    goog.dom.appendChild(row, toInput);

    var execButton = goog.dom.createDom('button', 'btn btn-primary bold', 'Ejecutar');
    goog.dom.appendChild(row, execButton);
    goog.events.listen(execButton, goog.events.EventType.CLICK, goog.bind(this.generateActivity, this));
};

/**
 * Renders the activity - month submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderActivityMonthMenu = function() {
    this.reset(2);
    this.setMessage('Elige el mes y año');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', {'class' : 'level3'});
    goog.dom.appendChild(bar, row);

    var title = goog.dom.createDom('h3', null, 'Mes');
    goog.dom.appendChild(row, title);

    var monthLabel = goog.dom.createDom('label', null, 'Mes');
    goog.dom.appendChild(row, monthLabel);

    var monthSelect = goog.dom.createDom('select', {'id' : 'month-input'});
    goog.dom.appendChild(row, monthSelect);
    var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiempbre', 'Octubre',
                  'Noviembre', 'Diciembre'];
    var current = new Date().getMonth();

    for(var cont = 0, size = months.length; cont < size; cont++) {
        var option = goog.dom.createDom('option', {'value' : (cont + 1).toString()}, months[cont]);
        goog.dom.appendChild(monthSelect, option);

        if(cont === current)
            option.selected = true;
    }

    var yearLabel = goog.dom.createDom('label', null, 'Año');
    goog.dom.appendChild(row, yearLabel);

    var yearSelect = goog.dom.createDom('select', {'id' : 'year-input', 'disabled' : 'true'});
    goog.dom.appendChild(row, yearSelect);

    var execButton = goog.dom.createDom('button', {'class' : 'btn btn-primary bold', 'disabled' : 'true'}, 'Ejecutar');
    goog.dom.appendChild(row, execButton);
    goog.events.listen(execButton, goog.events.EventType.CLICK, goog.bind(this.generateActivityMonth, this));

    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(row, loading);

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.Alert.error('No se puedo buscar el intervalo de actividad');
                return;
        }

        var report = response.content().getData();
        var first  = new Date();
        var last   = new Date();
        pnc.model.DATE_TIME_PARSER.parse(report['first'], first);
        pnc.model.DATE_TIME_PARSER.parse(report['last'],  last);

        first   = first.getFullYear();
        last    = last.getFullYear();
        current = new Date().getFullYear();

        for(var cont = first; cont <= last; cont++) {
            var option = goog.dom.createDom('option', {'value' : cont.toString()}, cont.toString())
            goog.dom.appendChild(yearSelect, option);

            if(cont === current)
                option.selected = true;
        }

        goog.dom.removeNode(loading);
        yearSelect.disabled = false;
        execButton.disabled = false;
    };
    this.client.getInterval(callback);
};

/**
 * Renders the activity - year submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderActivityYearMenu = function() {
    this.reset(2);
    this.setMessage('Elige el año');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', {'class' : 'level3'});
    goog.dom.appendChild(bar, row);

    var title = goog.dom.createDom('h3', null, 'Año');
    goog.dom.appendChild(row, title);

    var yearLabel = goog.dom.createDom('label', null, 'Año');
    goog.dom.appendChild(row, yearLabel);

    var yearSelect = goog.dom.createDom('select', {'id' : 'year-input', 'disabled' : 'true'});
    goog.dom.appendChild(row, yearSelect);

    var execButton = goog.dom.createDom('button', {'class' : 'btn btn-primary bold', 'disabled' : 'true'}, 'Ejecutar');
    goog.dom.appendChild(row, execButton);
    goog.events.listen(execButton, goog.events.EventType.CLICK, goog.bind(this.generateActivityYear, this));

    var loading = goog.dom.createDom('img', {'src' : pnc.ui.Resources.LOADING});
    goog.dom.appendChild(row, loading);

    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.OK:
                break;

            default:
                pnc.ui.Alert.error('No se puedo buscar el intervalo de actividad');
                return;
        }

        var report = response.content().getData();
        var first  = new Date();
        var last   = new Date();
        pnc.model.DATE_TIME_PARSER.parse(report['first'], first);
        pnc.model.DATE_TIME_PARSER.parse(report['last'],  last);

        first   = first.getFullYear();
        last    = last.getFullYear();
        current = new Date().getFullYear();

        for(var cont = first; cont <= last; cont++) {
            var option = goog.dom.createDom('option', {'value' : cont.toString()}, cont.toString())
            goog.dom.appendChild(yearSelect, option);

            if(cont === current)
                option.selected = true;
        }

        goog.dom.removeNode(loading);
        yearSelect.disabled = false;
        execButton.disabled = false;
    };
    this.client.getInterval(callback);
};

/**
 * Renders the inventory submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderInventoryMenu = function() {
    this.reset(1);
    this.setMessage('Elige el tipo de reporte para continuar');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', {'id' : 'inventory-submenu', 'class' : 'level2'});
    goog.dom.appendChild(bar, row);

    var title = goog.dom.createDom('h3', null, 'Inventory');
    goog.dom.appendChild(row, title);

    var generalButton = goog.dom.createDom('button', 'btn', 'General');
    goog.dom.appendChild(row, generalButton);
    goog.events.listen(generalButton, goog.events.EventType.CLICK, goog.bind(this.generateInventory, this));

    var supplierButton = goog.dom.createDom('button', 'btn', 'Suplidor');
    goog.dom.appendChild(row, supplierButton);
    goog.events.listen(supplierButton, goog.events.EventType.CLICK, goog.bind(this.renderInventorySupplierMenu, this));

    var existenceButton = goog.dom.createDom('button', 'btn', 'Existencia');
    goog.dom.appendChild(row, existenceButton);
    goog.events.listen(existenceButton, goog.events.EventType.CLICK, goog.bind(this.generateInventoryExistence, this));

    var expirationButton = goog.dom.createDom('button', 'btn', 'Expiracion');
    goog.dom.appendChild(row, expirationButton);
    goog.events.listen(expirationButton, goog.events.EventType.CLICK, goog.bind(this.renderInventoryExpirationMenu, this));
};

/**
 * Renders the inventory - supplier submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderInventorySupplierMenu = function() {
    this.reset(2);
    this.setMessage('Elige el suplidor');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', 'level3');
    goog.dom.appendChild(bar, row);

    var title = goog.dom.createDom('h3', null, 'Suplidor');
    goog.dom.appendChild(row, title);

    var label = goog.dom.createDom('label', null, 'Suplidor');
    goog.dom.appendChild(row, label);

    var input = goog.dom.createDom('input', {'id' : 'supplier-input', 'type' : 'text', 'style' : 'width: 178px'});
    goog.dom.appendChild(row, input);
    var ac = goog.ui.ac.createSimpleAutoComplete([], input, false, true);
    this.disposables.push(ac);
    var pill     = pnc.ui.createPill(input, ac);
    var link     = pnc.client.Profile.getLink(pnc.client.Supplier.Rel.SEARCH);
    var decoder  = goog.partial(pnc.client.decodeCollection, pnc.client.Supplier.decode);
    var resolver = function() { return input.value; };
    pnc.ui.updateAcData(input, ac, link, {'{org}' : resolver}, decoder, this.supplierMap);

    var execButton = goog.dom.createDom('button', {'class' : 'btn btn-primary', 'disabled'  : 'true'}, 'Ejecutar');
    goog.dom.appendChild(row, execButton);
    goog.events.listen(execButton, goog.events.EventType.CLICK, goog.bind(this.generateInventorySupplier, this));

    var callback = function() { execButton.disabled = true; };
    pnc.ui.onPillClose(pill, callback);

    callback = function() { execButton.disabled = false; };
    goog.events.listen(ac, goog.ui.ac.AutoComplete.EventType.UPDATE, callback);
};

/**
 * Renders the inventory - expiration submenu
 * @private
 */
pnc.ui.ReportView.prototype.renderInventoryExpirationMenu = function() {
    this.reset(2);
    this.setMessage('Elige el criterio de expiracion');
    var bar = goog.dom.getElement('report-bar');
    var row = goog.dom.createDom('div', 'level3');
    goog.dom.appendChild(bar, row);

    var title = goog.dom.createDom('h3', null, 'Expiracion');
    goog.dom.appendChild(row, title);

    var choiceBox = goog.dom.createDom('div', {'style' : 'display : inline-block'});
    goog.dom.appendChild(row, choiceBox);

    var choice = goog.dom.createDom('div');
    goog.dom.appendChild(choiceBox, choice);
    var expiredButton = goog.dom.createDom('input', {'id' : 'expired-button', 'type' : 'radio', 'name' : 'expiration', 'value' : 'true'});
    goog.dom.appendChild(choice, expiredButton);
    var label = goog.dom.createDom('label', null, 'Expirado');
    goog.dom.appendChild(choice, label);
    var callback = function() {
        input.value = '';
        execButton.disabled = false;
        daysBox.style.visibility = 'hidden';
    };
    goog.events.listen(expiredButton, goog.events.EventType.CLICK, callback);

    choice = goog.dom.createDom('div');
    goog.dom.appendChild(choiceBox, choice);
    var notExpiredButton = goog.dom.createDom('input', {'id' : 'not-expired-button', 'type' : 'radio', 'name' : 'expiration', 'value' : 'false'});
    goog.dom.appendChild(choice, notExpiredButton);
    label = goog.dom.createDom('label', null, 'No Expirado');
    goog.dom.appendChild(choice, label);
    callback = function() {
        execButton.disabled = input.value.length < 1;
        daysBox.style.visibility = 'initial';
    };
    goog.events.listen(notExpiredButton, goog.events.EventType.CLICK, callback);

    var daysBox = goog.dom.createDom('div', {'style' : 'display : inline-block; visibility: hidden'});
    goog.dom.appendChild(row, daysBox);

    label = goog.dom.createDom('label', null, 'Dias Restantes');
    goog.dom.appendChild(daysBox, label);

    var input = goog.dom.createDom('input', {'id' : 'days-input', 'type' : 'text'});
    goog.dom.appendChild(daysBox, input);
    pnc.ui.assureNumbers(input);
    callback = function() {
        if(notExpiredButton.checked)
            execButton.disabled = input.value.length < 1;
    };
    goog.events.listen(input, goog.events.EventType.KEYUP, callback);

    var execButton = goog.dom.createDom('button', {'class' : 'btn btn-primary', 'disabled' : 'true', 'style' : 'vertical-align : super'}, 'Ejecutar');
    goog.dom.appendChild(row, execButton);
    goog.events.listen(execButton, goog.events.EventType.CLICK, goog.bind(this.generateInventoryExpiration, this));
};


/**
 * Sets a message to be displayed in the report content pane
 * @param {string} message  Message to set
 * @private
 */
pnc.ui.ReportView.prototype.setMessage = function(message) {
    var content = goog.dom.getElement('report-content');
    goog.dom.removeChildren(content);

    var message = goog.dom.createDom('h2', null, message);
    goog.dom.appendChild(content, message);
};

/**
 * Renders a legend that accompanies graphs to provide the user guidance as to what each color represents.
 * @param  {Element} parent    Parent element where the legend will be placed
 * @param  {Array}   data      Data for the legend. An array of objets is expected. The 'color' and 'label' properties
 *                              of the object will be checked. It's values will be used as a basis to generate the legend.
 * @param  {boolean} vertical  Does this legend need to have a vertical orientation?
 * @return {Element}           Generated element
 * @private
 */
pnc.ui.ReportView.prototype.renderLegend = function(parent, data, vertical) {
    vertical = goog.isDefAndNotNull(vertical) ? vertical : false;
    var container = goog.dom.createDom('div', 'legend');
    goog.dom.appendChild(parent, container);

    if(vertical)
        goog.dom.classes.add(container, 'vertical');

    for(var cont = 0, size = data.length; cont < size; cont++) {
        if(vertical) {
            var row = goog.dom.createDom('div');
            goog.dom.appendChild(container, row);
            container = row;
        }

        var legend = goog.dom.createDom('div', {'class' : 'key', 'style' : 'background-color: ' + data[cont].color});
        goog.dom.appendChild(container, legend);

        var label = goog.dom.createDom('label', null, data[cont].label);
        goog.dom.appendChild(container, label);
    }

    return container;
};

/**
 * Resets the report bar to a given level. Removes all elements that are in inferior levels
 * @param  {number} level  Level to reset to
 * @private
 */
pnc.ui.ReportView.prototype.reset = function(level) {
    var bar   = goog.dom.getElement('report-bar');
    var queue = [];
    level     = goog.isDefAndNotNull(level) ? level : 1;

    switch(level) {
        case 1:
            queue.push(goog.dom.getElementsByClass('level2', bar)[0]);

        case 2:
            queue.push(goog.dom.getElementsByClass('level3', bar)[0]);

        case 3:
            queue.push(goog.dom.getElementsByClass('level4', bar)[0]);
    }

    for(var cont = 0, size = queue.length; cont < size; cont++)
        goog.dom.removeNode(queue[cont]);
};

/**
 * Takes in a sparse collection of data, and fills in with zeroes for all the days that are missing. Used for generating
 * graphs.
 * @param  {Object.<string, number>} dataset  Dataset to check
 * @return {Array.<number>}                   Generated array with data.
 * @private
 */
pnc.ui.ReportView.prototype.fillDays = function(dataset) {
    if(!goog.isDefAndNotNull(dataset) || goog.object.getCount(dataset) < 1)
        return [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

    var first  = goog.object.getKeys(dataset)[0];
    var prefix = first.substring(0, 8);
    var result = [];

    for(var cont = 1; cont < 32; cont++) {
        var day = cont < 10 ? '0' + cont : cont.toString();
        var value = dataset[prefix + day];

        if(goog.isDef(value))
            result.push(value)
        else
            result.push(0);
    }

    return result;
};

/**
 * Takes in a sparse collection of data, and fills in with zeroes for all the months that are missing. Used for
 * generating graphs.
 * @param  {Object.<string, number>} dataset  Dataset to check
 * @return {Array.<number>}                   Generated array with data.
 * @private
 */
pnc.ui.ReportView.prototype.fillMonths = function(dataset) {
    if(!goog.isDefAndNotNull(dataset) || goog.object.getCount(dataset) < 1)
        return [0,0,0,0,0,0,0,0,0,0,0,0];

    var result = [];

    for(var cont = 0; cont < 12; cont++) {
        var month = cont < 10 ? '0' + cont : cont.toString();
        var value = dataset[month];

        if(goog.isDef(value))
            result.push(value);
        else
            result.push(0);
    }

    return result;
};

/**
 * Paginator callback used to generate inventory rows
 * @param  {Element} parent  Parent element where the representation will be placed
 * @param  {Array}   item    Inventory entry item to represent
 * @private
 */
pnc.ui.ReportView.prototype.inventoryRenderer = function(parent, item) {
    var row = goog.dom.createDom('tr');
    goog.dom.appendChild(parent, row);

    for(var cont = 0, size = item.length; cont < size; cont++) {
        var cell = goog.dom.createDom('td', null, item[cont]);
        goog.dom.appendChild(row, cell);

        if(cont > 0)
            cell.style.textAlign = 'right';
    }
};

/**
 * Aggregates the data in a report map and converts it to an array that can be easily passed to a paginator or table.
 * @param  {Object} report  Report data to aggregate
 * @return {Array}          Aggregated report data.
 * @private
 */
pnc.ui.ReportView.prototype.aggregateInventoryReport = function(report) {
    var data = [];

    for(var key in report) {
        var row   = [];
        var entry = report[key];
        data.push(row);
        row.push(key);
        row.push(entry['threshold'].toString())
        row.push(entry['remaining'].toString());
        row.push(pnc.ui.DECIMAL_FORMATTER.format(entry['cost']));
        row.push(pnc.ui.DECIMAL_FORMATTER.format(entry['price']));
    }

    return data;
};
