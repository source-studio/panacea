goog.provide('pnc.ui.InvoiceView');

goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.string');
goog.require('goog.ui.MenuItem');
goog.require('goog.ui.Select');

goog.require('pnc.client.Balance');
goog.require('pnc.client.Invoice');
goog.require('pnc.client.User');
goog.require('pnc.model.Customer');
goog.require('pnc.model.Invoice');
goog.require('pnc.model.InvoiceItem');
goog.require('pnc.model.User');
goog.require('pnc.ui.Alert');
goog.require('pnc.ui.Main');
goog.require('pnc.ui.SlideDialog');
goog.require('pnc.ui.SlidePanel');

/**
 * View that allows user to search invoices which have already been processed, see their detail and perform certain
 * actions on them if the appropriate privileges have been granted.
 * @constructor
 */
pnc.ui.InvoiceView = function() {
    /**
     * A map of customers. Keys are string representations of the customer, values are customre objects. Used for the 
     * customer search function
     * @type {Object.<stirng, pnc.model.Customer>}
     * @private
     */
    this.customerMap   = {};

    /**
     * Collection of existing users
     * @type {Array.<pnc.model.Users>}
     * @private
     */
    this.users = [];

    /**
     * The last balance that was created, if it exsits
     * @type {pnc.model.Balance}
     * @private
     */
    this.lastBalance = null;

    /**
     * A cache of products which have already been requested from the server. Used to avoid subsequent requests for the
     * same product.
     * @type {Array.<pnc.model.Products>}
     * @private
     */
    this.productCache = [];

    /**
     * Client used to make invoice service requests
     * @type {pnc.client.Invoice}
     * @private
     */
    this.invoiceClient = new pnc.client.Invoice();

    /**
     * Client used to make product service requests
     * @type {pnc.client.Product}
     * @private
     */
    this.productClient = new pnc.client.Product();

    //------------- elements -------------//
    /** @private */ this.container   = null;
    /** @private */ this.disposables = [];
    /** @private */ this.paginator   = null;
};
pnc.ui.Main.registerModule('orders', pnc.ui.InvoiceView);

/**
 * Human readable versions of the pnc.model.InvoiceView.Payment enumeration.
 * @enum {string}
 */
pnc.ui.InvoiceView.Payment = {
    ALL       : 'Todos',
    CASH      : 'Efectivo',
    CREDIT    : 'Credito',
    QUOTE     : 'Cotizacion',
    SPONSORED : 'Patrocinio'
};

/**
 * Human readable representations of the pnc.model.Invoice.State enumeration
 * @enum {string}
 */
pnc.ui.InvoiceView.State = {
    OK          : 'OK',
    REFUNDED    : 'Devolucion',
    INVALIDATED : 'Invalidado'
};


/**
 * Renders the invoice view
 * @param  {Element} parent  Parent element where the view will be rendered in
 */
pnc.ui.InvoiceView.prototype.render = function(parent) {
    this.container = goog.dom.createDom('div', {'class' : 'span10', 'style' : 'margin-left: 0px'});
    goog.dom.appendChild(parent, this.container);

    this.getLastBalance();
    this.renderSearchPane();
    this.renderInvoicePane();
    this.renderDetailPane();
    this.renderItemPane();
    pnc.ui.Main.breadcrumbClear();
    pnc.ui.Main.breadcrumbPush('Facturas');
};

/**
 * Cleans up resources and removes the invoice view from the main container.
 */
pnc.ui.InvoiceView.prototype.dispose = function() {
    for(var cont = 0, size = this.disposables.length; cont < size; cont++)
        this.disposables[cont].dispose();

    goog.dom.removeNode(this.container);
};

/**
 * Executes the transaction to retrieve the last balance from the server, if one exists
 * @private
 */
pnc.ui.InvoiceView.prototype.getLastBalance = function() {
    var self = this;
    var callback = function(response) {
        switch(response.status()) {
            case pnc.client.Status.NOT_FOUND:
                self.lastBalance = null;
                break;

            case pnc.client.Status.OK:
                self.lastBalance = response.content();
                break;

            default:
                pnc.ui.Alert.error('No se pudo recuperar informacion sobre el ultimo balance');
                break;
        }
    };
    var client = new pnc.client.Balance();
    client.getLast(callback);
};

/**
 * Renders the search pane. Produces the search controls that the user can use to pull invoices.
 * @private
 */
pnc.ui.InvoiceView.prototype.renderSearchPane = function() {
    var content = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.SEARCH, 'Busqueda');
    goog.dom.classes.add(content, 'search-pane');

    var fromLabel = goog.dom.createDom('label', 'span1', 'Desde');
    goog.dom.appendChild(content, fromLabel);
    var fromInput = goog.dom.createDom('input', {'id' : 'from-input', 'type' : 'date', 'class' : 'span2'});
    goog.dom.appendChild(content, fromInput);

    var toLabel = goog.dom.createDom('label', 'span1', 'Hasta');
    goog.dom.appendChild(content, toLabel);
    var toInput = goog.dom.createDom('input', {'id' : 'to-input', 'class' : 'span2', 'type' : 'date'});
    goog.dom.appendChild(content, toInput);

    var userLabel = goog.dom.createDom('label', 'span1', 'Usuario');
    goog.dom.appendChild(content, userLabel);
    var userSelect = new goog.ui.Select();
    userSelect.render(content);
    userSelect.setEnabled(false);
    this.disposables.push(userSelect);

    var typeLabel = goog.dom.createDom('label', 'span1', 'Pago');
    goog.dom.appendChild(content, typeLabel);
    var typeSelect = new goog.ui.Select();
    typeSelect.render(content);

    for(var key in pnc.ui.InvoiceView.Payment) {
        var item = new goog.ui.MenuItem(pnc.ui.InvoiceView.Payment[key]);
        typeSelect.addItem(item);
        item.setValue(key);
    }

    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(content, controlPane);

    var searchButton = goog.dom.createDom('button', 'btn btn-primary', 'Busqueda');
    goog.dom.appendChild(controlPane, searchButton);

    var self = this;
    var searchCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK && response.status() !== pnc.client.Status.NOT_FOUND) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.populateInvoicePane(response.content());
    };

    var clickCallback = function() { 
        if(fromInput.value.length < 1)
            goog.dom.classes.add(fromInput, 'validation-error');

        if(toInput.value.length < 1)
            goog.dom.classes.add(toInput, 'validation-error');

        if(fromInput.value.length < 1 || toInput.value.length < 1)
            return;

        goog.dom.classes.remove(fromInput, 'validation-error');
        goog.dom.classes.remove(toInput, 'validation-error');
        var from = new Date();
        var to   = new Date(); 
        pnc.client.DATE_PARSER.parse(fromInput.value, from);
        pnc.client.DATE_PARSER.parse(toInput.value, to);

        self.invoiceClient.search(from, to, userSelect.getValue(), typeSelect.getValue(),
            pnc.model.Invoice.Transaction.CUSTOMER, searchCallback); 
    };
    goog.events.listen(searchButton, goog.events.EventType.CLICK, clickCallback);

    var userCallback = function(response) {
        if(response.status() !== pnc.client.Status.OK) {
            pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
            return;
        }

        self.users = response.content();

        for(var cont = 0, size = self.users.length; cont < size; cont++) {
            var user = self.users[cont];

            if(!user.isEnabled())
                continue;

            var item = new goog.ui.MenuItem(user.toString());
            userSelect.addItem(item);
            item.setValue(user.getId());
        }

        userSelect.setEnabled(true);
    };

    var userClient = new pnc.client.User();
    userClient.getAll(userCallback);
};

/**
 * Renders the invoice pane. This displays invoice search results, and exposes actions that can be performed on invoices.
 * @private
 */
pnc.ui.InvoiceView.prototype.renderInvoicePane = function() {
    var content             = pnc.ui.createBox(this.container, 'span7', pnc.ui.Resources.LIST, 'Facturas');
    content.id              = 'invoice-pane';
    content.style.minHeight = '390px';
    var head                = goog.dom.getElementByClass('box-head', content.parentElement);

    var toolbar = goog.dom.createDom('div', {'class' : 'pull-right btn-toolbar', 'style' : 'margin: 4px 10px 0 0'});
    goog.dom.appendChild(head, toolbar);
    var actionGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, actionGroup);
    var navGroup = goog.dom.createDom('div', 'btn-group');
    goog.dom.appendChild(toolbar, navGroup);

    var privs = pnc.client.Profile.getPrivileges();
    var backButton, forwardButton, invalidateButton, refundButon, copyButton = null;

    if(goog.array.contains(privs, pnc.client.Privileges.INVOICE_SELL))
        copyButton = pnc.ui.createToolbarButton(actionGroup, 'copy-button', pnc.ui.Resources.COPY, 'Copiar', false, goog.bind(this.copy, this));
    
    if(goog.array.contains(privs, pnc.client.Privileges.INVOICE_REFUND))
        refundButton = pnc.ui.createToolbarButton(actionGroup, 'refund-button', pnc.ui.Resources.RETURN, 'Devolucion', false, goog.bind(this.refund, this));


    if(goog.array.contains(privs, pnc.client.Privileges.INVOICE_INVALIDATE))
        invalidateButton = pnc.ui.createToolbarButton(actionGroup, 'invalidate-button', pnc.ui.Resources.REMOVE, 'Invalidar', false, goog.bind(this.invalidate, this));
    
    backButton    = pnc.ui.createToolbarButton(navGroup, 'back-button',    pnc.ui.Resources.ARROW_LEFT,  'Atras', false);
    forwardButton = pnc.ui.createToolbarButton(navGroup, 'forward-button', pnc.ui.Resources.ARROW_RIGHT, 'Alante', false);

    var self = this;
    var navCallback = function(e) {
        if(!goog.isNull(invalidateButton))
            invalidateButton.disabled = true;

        if(!goog.isNull(refundButton))
            refundButton.disabled = true;

        if(!goog.isNull(copyButton))
            copyButton.disabled = true;

        goog.dom.removeChildren(goog.dom.getElement('detail-table'));
        goog.dom.removeChildren(goog.dom.getElement('item-table'));
    };
    goog.events.listen(forwardButton, goog.events.EventType.CLICK, navCallback);
    goog.events.listen(backButton, goog.events.EventType.CLICK, navCallback);

    var titles = ['#', 'Fecha/Hora', 'Pago', 'Total'];
    this.paginator = new pnc.ui.Paginator(titles, [], null, pnc.ui.PAGE_SIZE, backButton, forwardButton);
    this.paginator.setRowRenderer(goog.bind(this.renderInvoice, this));
    this.paginator.render(content);
    this.paginator.getElement().style.width = '515px';
};

/**
 * Renders the detail pane. A detail pane on the right that will provide further details on the invoice.
 * @private
 */
pnc.ui.InvoiceView.prototype.renderDetailPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.INFO, 'Detalles');
    content.style.minHeight = '142px';
    var body    = pnc.ui.createSummaryTable(content, 'summary-table table-striped table-condensed', 2, 'Factura');
    body.id     = 'detail-table';
};

/**
 * Renders the Item pane. A detail pane on the right that will provide a list of all the items that were bought in the
 * selected invoice.
 * @private
 */
pnc.ui.InvoiceView.prototype.renderItemPane = function() {
    var content = pnc.ui.createBox(this.container, 'span3 pull-right', pnc.ui.Resources.LIST, 'Itemes');
    var headers = ['#', 'Producto', 'Sub-Total'];
    var body    = pnc.ui.createTable(content, 'table-condensed', headers);
    body.id = 'item-table';
};

/**
 * Populates the invoice pane after the invoice search results come back 
 * @param  {Array.<pnc.model.Invoice>} invoices  Invoice list to display.
 * @private
 */
pnc.ui.InvoiceView.prototype.populateInvoicePane = function(invoices) {
    var content          = goog.dom.getElement('invoice-pane');
    var invalidateButton = goog.dom.getElement('invalidate-button');
    var refundButton     = goog.dom.getElement('refund-button');
    var copyButton       = goog.dom.getElement('copy-button');
    var backButton       = goog.dom.getElement('back-button');
    var forwardButton    = goog.dom.getElement('forward-button');
    var detailTable      = goog.dom.getElement('detail-table');
    var itemTable        = goog.dom.getElement('item-table');
    var undoButton       = goog.dom.getElementsByTagNameAndClass('button', null, detailTable.parentElement.parentElement);

    goog.dom.removeChildren(detailTable);
    goog.dom.removeChildren(itemTable);

    if(goog.isDefAndNotNull(undoButton[0]))
        goog.dom.removeNode(undoButton[0]);

    if(!goog.isNull(invalidateButton))
        invalidateButton.disabled = true;

    if(!goog.isNull(refundButton))
        refundButton.disabled = true;

    if(!goog.isNull(copyButton))
        copyButton.disabled = true;
   
    this.paginator.setData(invoices);
    this.paginator.refresh();
};

/**
 * Renders an invoice as a row in the paginated table
 * @param  {Element}           parent   Parent element where the row will be placed
 * @param  {pnc.model.Invoice} invoice  Invoice that will be rendered
 * @private
 */
pnc.ui.InvoiceView.prototype.renderInvoice = function(parent, invoice) {
    var row = goog.dom.createDom('tr', {'data-invoice' : invoice.getId()});
    goog.dom.appendChild(parent, row);

    switch(invoice.getState()) {
        case pnc.model.Invoice.State.REFUNDED:
            goog.dom.classes.add(row, 'warning');
            break;

        case pnc.model.Invoice.State.INVALIDATED:
            goog.dom.classes.add(row, 'error');
            break;
    }

    var self = this;
    var callback = function(invoice, e) {
        var rows             = goog.dom.getElementsByTagNameAndClass('tr', 'info', parent);
        var invalidateButton = goog.dom.getElement('invalidate-button');
        var refundButton     = goog.dom.getElement('refund-button');
        var copyButton       = goog.dom.getElement('copy-button');

        for(var cont = 0, size = rows.length; cont < size; cont++)
            goog.dom.classes.remove(rows[cont], 'info');

        goog.dom.classes.add(e.target.parentElement, 'info');

        invalidateButton.disabled = invoice.getState() !== pnc.model.Invoice.State.OK;
        refundButton.disabled     = invoice.getState() !== pnc.model.Invoice.State.OK;
        copyButton.disabled       = false;

        self.populateDetailPane(invoice, row);
        self.populateItemPane(invoice);
    };
    goog.events.listen(row, goog.events.EventType.CLICK, goog.partial(callback, invoice));

    var cells = [];
    cells[0]  = goog.dom.createDom('td', null, invoice.getId().toString());
    cells[1]  = goog.dom.createDom('td', {'style' : 'width: 140px'}, pnc.ui.DATE_TIME_FORMATTER.format(invoice.getTimestamp()));
    cells[2]  = goog.dom.createDom('td', {}, pnc.ui.InvoiceView.Payment[invoice.getPayment()]);
    cells[3]  = goog.dom.createDom('td', {'style' : 'text-align: right'}, pnc.ui.DECIMAL_FORMATTER.format(invoice.getTotal() - invoice.getDiscount()));

    for(var cont = 0, size = cells.length; cont < size; cont++)
        goog.dom.appendChild(row, cells[cont]);
};

/**
 * Populates the content of the detail pane depending on which invoice has been selected
 * @param  {pnc.model.Invoice} invoice  Invoice who's data will be populated
 * @param  {Element}           row      Highlighted row
 * @private
 */
pnc.ui.InvoiceView.prototype.populateDetailPane = function(invoice, row) {
    var body = goog.dom.getElement('detail-table');
    goog.dom.removeChildren(body);
    var existing = goog.dom.getElementsByTagNameAndClass('button', null, body.parentElement.parentElement)[0];
    var self = this;

    if(goog.isDefAndNotNull(existing))
        goog.dom.removeNode(existing);

    var createContent = function(title, value) {
        var row = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        var cell = goog.dom.createDom('td', {}, title);
        goog.dom.appendChild(row, cell);
        cell = goog.dom.createDom('td', {'style' : 'text-align : right'}, value);
        goog.dom.appendChild(row, cell);
    };

    createContent('Estado',      pnc.ui.InvoiceView.State[invoice.getState()]);
    createContent('Vendida Por', pnc.model.findModel(this.users, invoice.getIdUser()).toString());
    
    if(invoice.getDiscount() > 0)
        createContent('Descuento',   pnc.ui.DECIMAL_FORMATTER.format(invoice.getDiscount()));
    
    createContent('Total',       pnc.ui.DECIMAL_FORMATTER.format(invoice.getTotal() - invoice.getDiscount()));
    var invalidation, refund = null;

    if(invoice.getState() === pnc.model.Invoice.State.INVALIDATED) {
        var invalidationCallback = function(response) {
            if(response.status() !== pnc.client.Status.OK) {
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
            }

            invalidation = response.content();
            createContent('Invalidado Por', pnc.model.findModel(self.users, invalidation.getIdUser()).toString());
            createContent('Invalidado En',  pnc.ui.DATE_TIME_FORMATTER.format(invalidation.getTimestamp()));
        };
        this.invoiceClient.getInvalidation(invoice, invalidationCallback);
    } else if(invoice.getState() === pnc.model.Invoice.State.REFUNDED) {
        var refundCallback = function(response) {
            if(response.status() !== pnc.client.Status.OK) {
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                return;
            }

            refund = response.content();
            createContent('Devolucion Por', pnc.model.findModel(self.users, refund.getIdUser()).toString());
            createContent('Devolueto En',   pnc.ui.DATE_TIME_FORMATTER.format(refund.getTimestamp()));
        };
        this.invoiceClient.getRefund(invoice, refundCallback);
    }

    var privs = pnc.client.Profile.getPrivileges();

    var undoCallback = function(action, style, response) {
        if(response.status() === pnc.client.Status.OK) {
            pnc.ui.Alert.success('La ' + action + ' fue deshecha');
            goog.dom.classes.remove(row, style, 'info');
            invoice.setState(pnc.model.Invoice.State.OK);
            self.populateDetailPane(invoice, row);
        } else {
            pnc.ui.SlideDialog.error('Error al tratar de desahacer la ' + action).open();
        }
    };

    if(invoice.getState() === pnc.model.Invoice.State.INVALIDATED && goog.array.contains(privs, pnc.client.Privileges.INVOICE_INVALIDATE_UNDO)) {
        if(!goog.isDefAndNotNull(this.lastBalance) || this.lastBalance.getTimestamp() < invoice.getTimestamp()) {
            var button = goog.dom.createDom('button', 'btn btn-danger btn-block', 'Deshacer Invalidacion');
            goog.dom.appendChild(body.parentElement.parentElement, button);
            var undo     = goog.partial(undoCallback, 'Invalidacion', 'error');
            var callback = function() { self.invoiceClient.undoInvalidate(invalidation, undo); };
            goog.events.listen(button, goog.events.EventType.CLICK, callback);
        }
    } else if(invoice.getState() === pnc.model.Invoice.State.REFUNDED && goog.array.contains(privs, pnc.client.Privileges.INVOICE_REFUND_UNDO)) {
        if(!goog.isDefAndNotNull(this.lastBalance) || this.lastBalance.getTimestamp() < invoice.getTimestamp()) {
            var button = goog.dom.createDom('button', 'btn btn-danger btn-block', 'Deshacer Devolcuion');
            goog.dom.appendChild(body.parentElement.parentElement, button);
            var undo     = goog.partial(undoCallback, 'Devolucion', 'warning');
            var callback = function() { self.invoiceClient.undoRefund(refund, undo); };
            goog.events.listen(button, goog.events.EventType.CLICK, callback);
        }
    }
};

/**
 * Populates all invoice items from the selected invoice
 * @param  {pnc.model.Invoice} invoice  Invoice which holds the items to display.
 * @private
 */
pnc.ui.InvoiceView.prototype.populateItemPane = function(invoice) {
    var body = goog.dom.getElement('item-table');
    goog.dom.removeChildren(body);
        
    for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
        var item = invoice.getItems()[cont];
        var row  = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);

        var cell = goog.dom.createDom('td', null, item.getQuantity().toString());
        goog.dom.appendChild(row, cell);

        var productCell = goog.dom.createDom('td');
        goog.dom.appendChild(row, productCell);
        var totalCell = goog.dom.createDom('td', {'style' : 'text-align: right'});
        goog.dom.appendChild(row, totalCell);

        var self = this;
        var callback = function(item, productCell, totalCell, response) {
            switch(response.status()) {
                case pnc.client.Status.OK:
                    var product = response.content();
                    goog.dom.setTextContent(productCell, product.getName());
                    goog.dom.setTextContent(totalCell, pnc.ui.DECIMAL_FORMATTER.format(item.getQuantity() * product.getPrice()));

                    if(!goog.array.contains(self.productCache, product))
                        self.productCache.push(product);
                    break;

                case pnc.client.Status.NOT_FOUND:
                    goog.dom.setTextContent(productCell, 'Product No Existente');
                    goog.dom.setTextContent(totalCell, '?');
                    break;

                default:
                    pnc.ui.SlideDialog.error(pnc.client.decodeStatus(response)).open();
                    break;
            }
        };

        var product = pnc.model.findModel(this.productCache, item.getIdProduct());

        if(goog.isDefAndNotNull(product)) {
            goog.dom.setTextContent(productCell, product.getName());
            goog.dom.setTextContent(totalCell, pnc.ui.DECIMAL_FORMATTER.format(item.getQuantity() * product.getPrice()));
        } else {
            this.productClient.get(item.getIdProduct(), goog.partial(callback, item, productCell, totalCell));
        }
    }
};

/**
 * Performs request and handling of response of the invalidate invoice transaction
 * @private
 */
pnc.ui.InvoiceView.prototype.invalidate = function() {
    var dialog = pnc.ui.SlideDialog.warning('Esta seguro que quiere invalidar esa factura? No sera contabilizada.');
    var row, invoice = null;
    var self = this;
    var responseCallback = function(e) {
        switch(e.status()) {
            case pnc.client.Status.CREATED:
                break;

            case pnc.client.Status.CONFLICT:
                dialog.dispose();
                pnc.ui.SlideDialog.error('No se puede invalidar una factura que no esta en estado OK').open();
                return;

            default:
                dialog.dispose();
                pnc.ui.SlideDialog.error(pnc.client.decodeStatus(e)).open();
                return;
        }

        goog.dom.classes.add(row, 'error');
        self.populateDetailPane(invoice, row);
        invoice.setState(pnc.model.Invoice.State.INVALIDATED);
        pnc.model.replaceModel(self.paginator.getData(), invoice);
        goog.dom.classes.remove(row, 'info');
        goog.dom.classes.add(row, 'error');
        goog.dom.getElement('invalidate-button').disabled = true;
        goog.dom.getElement('refund-button').disabled     = true;
        goog.dom.getElement('copy-button').disabled       = true;

        pnc.ui.Alert.success('La factura fue invalidada');
    };

    var selectCallback = function(e) {
        row     = goog.dom.getElementsByTagNameAndClass('tr', 'info')[0];
        invoice = pnc.model.findModel(self.paginator.getData(), row.dataset.invoice);
        var invalidation = new pnc.model.Invalidation(0, pnc.client.Profile.getUser().getId(), invoice.getId());
        self.invoiceClient.invalidate(invoice, invalidation, responseCallback);
    };
    dialog.setCallback(selectCallback);
    dialog.open();
};

/**
 * Generates a popup with all the items sold in the invoice, and provides the user with selection to pick which elements
 * and quantity should be refunded.
 * @private
 */
pnc.ui.InvoiceView.prototype.refund = function() {
    var invoiceRow = goog.dom.getElementsByTagNameAndClass('tr', 'info')[0];
    var invoice    = pnc.model.findModel(this.paginator.getData(), invoiceRow.dataset.invoice);
    var discountMult = 1 - (invoice.getDiscount() / invoice.getTotal());
    var panel   = goog.dom.createDom('div', 'return-panel');
    var message = goog.dom.createDom('span', {}, 'Seleccione los productos que seran devueltos');
    goog.dom.appendChild(panel, message);

    var titles  = ['', '#', 'Reponer', 'Producto', 'Sub-Total'];
    var body    = pnc.ui.createTable(panel, 'table-condensed table-striped', titles);
    var refund  = new pnc.model.Refund(null, pnc.client.Profile.getUser().getId(), invoice.getId());
    var restock = {};
    var self    = this;
    
    for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
        var item    = invoice.getItems()[cont];
        var product = pnc.model.findModel(self.productCache, item.getIdProduct());
        restock[item.getId()] = new pnc.model.Restock(null, null, item.getId(), 1, product.getPrice());
        row      = goog.dom.createDom('tr');
        goog.dom.appendChild(body, row);
        
        var cell = goog.dom.createDom('td', {'style' : 'width: 16px'});
        goog.dom.appendChild(row, cell);
        var cb = goog.dom.createDom('input', {'class' : 'item-cb', 'data-id' : item.getId(), 'type' : 'checkbox'});
        goog.dom.appendChild(cell, cb);

        cell = goog.dom.createDom('td', {'style' : 'width: 40px'});
        goog.dom.appendChild(row, cell);
        var input = goog.dom.createDom('input', {'class' : 'qty-input', 'data-id' : item.getId(), 'type' : 'text', 'value' : item.getQuantity().toString(), 'disabled' : 'true'});
        goog.dom.appendChild(cell, input);
        pnc.ui.assureNumbers(input);

        var callback = goog.bind(this.handleRefundQtyInput, this, input, invoice, item, refund, restock);
        goog.events.listen(input, goog.events.EventType.KEYUP, callback);

        cell = goog.dom.createDom('td', {'style' : 'width: 60px'});
        goog.dom.appendChild(row, cell);
        var rscb = goog.dom.createDom('input', {'class' : 'item-restock', 'data-id' : item.getId(), 'style' : 'margin-left: 32px', 'type' : 'checkbox', 'disabled' : 'true'});
        goog.dom.appendChild(cell, rscb);

        var rsCallback = function(item, rscb) {
            var r = restock[item.getId()];
            r.setRestocked(rscb.checked);
        };
        goog.events.listen(rscb, goog.events.EventType.CLICK, goog.partial(rsCallback, item, rscb));

        var product = pnc.model.findModel(this.productCache, item.getIdProduct());
        cell = goog.dom.createDom('td', {'style' : 'width: 160px'}, product.getName());
        goog.dom.appendChild(row, cell);

        cell = goog.dom.createDom('td', {'style' : 'text-align : right; width: 80px'}, '0.00');
        goog.dom.appendChild(row, cell);

        var cbCallback = function(item, product, cb, input, rscb, subtotalCell) {
            input.value    = item.getQuantity().toString();
            input.disabled = !cb.checked;
            rscb.disabled  = !cb.checked;
            rscb.checked   = false;

            var r = restock[item.getId()];
            var value = cb.checked ? Number((product.getPrice() * discountMult).toFixed(2)) : 0;
            r.setQuantity(1);
            r.setValue(value);
            r.setRestocked(false);

            goog.dom.setTextContent(subtotalCell, pnc.ui.DECIMAL_FORMATTER.format(value));
            self.calculateRefundTotal(invoice, refund, restock);
        };
        goog.events.listen(cb, goog.events.EventType.CLICK, goog.partial(cbCallback, item, product, cb, input, rscb, cell));
    }

    var outer = goog.dom.createDom('div', {'style' : 'min-height : 55px'});
    goog.dom.appendChild(panel, outer);
    var calculatedDiv = goog.dom.createDom('div', {'class' : 'pull-left', 'style' : 'margin-left: 15px'});
    goog.dom.appendChild(outer, calculatedDiv);
    var calculatedLabel = goog.dom.createDom('span', {'style' : 'display: block'}, 'Total calculado');
    goog.dom.appendChild(calculatedDiv, calculatedLabel);
    var calculatedValue = goog.dom.createDom('span', {'id' : 'refund-calculated', 'class' : 'bold pull-right', 'style' : 'font-size: 20px; margin-top: 5px'}, '0.00');
    goog.dom.appendChild(calculatedDiv, calculatedValue);

    var actualDiv = goog.dom.createDom('div', {'class' : 'pull-right input-prepend', 'style' : 'margin-right : 15px'});
    goog.dom.appendChild(outer, actualDiv);
    var actualLabel = goog.dom.createDom('span', {'style' : 'display: block'}, 'Valor a devolver');
    goog.dom.appendChild(actualDiv, actualLabel);
    var moneyLabel = goog.dom.createDom('span', 'add-on', '$');
    goog.dom.appendChild(actualDiv, moneyLabel);
    var actualInput = goog.dom.createDom('input', {'id' : 'refund-actual', 'type' : 'text', 'style' : 'width: 100px'});
    goog.dom.appendChild(actualDiv, actualInput);
    pnc.ui.assureNumbers(actualInput);
   
    var controlPane = goog.dom.createDom('div', 'box-actions');
    goog.dom.appendChild(panel, controlPane);

    var closeCallback = function() { 
        slider.dispose(); 
        pnc.ui.Main.breadcrumbPop();
    };
    var cancel = goog.dom.createDom('button', 'btn', 'Cancelar');
    goog.dom.appendChild(controlPane, cancel);
    goog.events.listen(cancel, goog.events.EventType.CLICK, closeCallback);

    var self     = this;
    var okButton = goog.dom.createDom('button', 'btn btn-primary', 'OK');
    goog.dom.appendChild(controlPane, okButton);
    var processCallback = function() { self.processRefund(invoice, refund, restock, invoiceRow, slider); };
    goog.events.listen(okButton, goog.events.EventType.CLICK, processCallback);

    var slider = new pnc.ui.SlidePanel('Devolucion Factura # ' + invoice.getId(), pnc.ui.Resources.BACK, panel);
    slider.render();
    pnc.ui.Main.breadcrumbPush('Devolucion');
};

/**
 * Handles user input into the quantity fields used to specify how much of a product is being returned. The quantity cannot
 * be grater than the original sold quantity, if so, the field will be highlighted in red. When changes are made, the sub
 * totals for the product row will be updated automatically.
 * @param  {Element}                            input    Input element where the user will update the quantity to be returned
 * @param  {pnc.model.Invoice}                  invoice  Invoice that was originally sold
 * @param  {pnc.model.InvoiceItem}              item     Invoice item with the product being returned
 * @param  {pnc.model.Refund}                   refund   Refund model. This will be updated based on user input.
 * @param  {Object.<number, pnc.model.Restock>} restock  Restock map. These will be updated based on user input.
 * @private
 */
pnc.ui.InvoiceView.prototype.handleRefundQtyInput = function(input, invoice, item, refund, restock) {
    var discountMult = 1 - (invoice.getDiscount() / invoice.getTotal());
    var qty = input.value.length > 0 ? pnc.ui.round(input.value) : 1;

    if(qty > item.getQuantity())
        goog.dom.classes.add(input, 'validation-error');
    else
        goog.dom.classes.remove(input, 'validation-error');

    var product = pnc.model.findModel(this.productCache, item.getIdProduct());
    var value   = Number((qty * product.getPrice() * discountMult).toFixed(2));
    goog.dom.setTextContent(input.parentElement.parentElement.lastElementChild, pnc.ui.DECIMAL_FORMATTER.format(value));

    var r = restock[item.getId()];
    r.setQuantity(qty);
    r.setValue(value);
    this.calculateRefundTotal(invoice, refund, restock);
};

/**
 * Calculates the refund total value based on the existing data, and updates the calculated total label with result.
 * Also updates the refund model.
 * @param  {pnc.model.Invoice}                  invoice  The invoice model that holds the products being returned
 * @param  {pnc.model.Refund}                   refund   The refund model that will be updated with the calculated values
 * @param  {Object.<number, pnc.model.Restock>} restock  Map with restock items.
 * @private
 */
pnc.ui.InvoiceView.prototype.calculateRefundTotal = function(invoice, refund, restock) {
    var discountMult = 1 - (invoice.getDiscount() / invoice.getTotal());
    var itemcbs = goog.dom.getElementsByClass('item-cb');
    var total   = 0;

    for(var cont = 0, size = itemcbs.length; cont < size; cont++) {
        var cb = itemcbs[cont];

        if(!cb.checked)
            continue;

        var qtyInput = goog.dom.getElementByClass('qty-input', cb.parentElement.parentElement);
        var qty      = qtyInput.value.length > 0 ? pnc.ui.round(qtyInput.value) : 1;
        var item     = pnc.model.findModel(invoice.getItems(), cb.dataset.id);

        if(qty > item.getQuantity())
            continue;

        var product = pnc.model.findModel(this.productCache, item.getIdProduct());
        total      += Number((qty * product.getPrice() * discountMult).toFixed(2));
    }

    refund.setCalculated(total);
    total = pnc.ui.DECIMAL_FORMATTER.format(total);
    goog.dom.setTextContent(goog.dom.getElement('refund-calculated'), total);
};

/**
 * Performs the refund transaction to the server and handles results.
 * @param  {pnc.model.Invoice}                  invoice  Invoice object that is being refunded
 * @param  {pnc.model.Refund}                   refund   Refund model that holds data about refund
 * @param  {Object.<number, pnc.model.Restock>} restock  Map with restock items
 * @param  {Element}                            row      Row representing the invoice to be refunded.
 * @param  {event}                              e        Event that was fired
 * @private
 */
pnc.ui.InvoiceView.prototype.processRefund = function(invoice, refund, restock, row, slider) {
    var itemcbs = goog.dom.getElementsByClass('item-cb');

    for(var cont = 0, size = itemcbs.length; cont < size; cont++) {
        var cb = itemcbs[cont];

        if(cb.checked) 
            refund.getRestock().push(restock[cb.dataset.id]);
    }

    if(refund.getRestock().length < 1) {
        pnc.ui.Alert.error('No ha seleccionado productos a devolver');
        return;
    }

    if(refund.getCalculated() <= 0) {
        pnc.ui.Alert.error('No hay un valor agregado en los productos seleccionados a devolver');
        return;
    }

    var actual = goog.dom.getElement('refund-actual').value;
    actual = actual.length > 0 ? pnc.ui.round(actual) : 0;

    if(actual <= 0) {
        pnc.ui.Alert.error('No ha insertado dado un valor a devolver');
        return;
    }

    refund.setActual(actual);
    var self = this;
    var refundCallback = function(response) {
        if(response.status() !== pnc.client.Status.CREATED) {
            var message = pnc.client.decodeStatus(response);
            pnc.ui.SlideDialog.error(message).open();
        } else {
            pnc.ui.Alert.success('Devolucion procesada');
            goog.dom.classes.add(row, 'warning');
            invoice.setState(pnc.model.Invoice.State.REFUNDED);
            self.populateDetailPane(invoice, row);
            slider.dispose();
        }
    };

    this.invoiceClient.refund(invoice, refund, refundCallback);
};

/**
 * Copies invoice intems from an existing invoice into the point of sale, so that the same sale may happen again.
 * @private
 */
pnc.ui.InvoiceView.prototype.copy = function() {
    var invoiceRow = goog.dom.getElementsByTagNameAndClass('tr', 'info')[0];
    var invoice    = pnc.model.findModel(this.paginator.getData(), invoiceRow.dataset.invoice);
    var products   = [];

    for(var cont = 0, size = invoice.getItems().length; cont < size; cont++) {
        var product = pnc.model.findModel(this.productCache, invoice.getItems()[cont].getIdProduct());
        products.push(product);
    }

    pnc.ui.Main.dropbox = ['copy', invoice, products];
    window.location.hash = 'pos';
};
