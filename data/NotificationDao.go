package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 * Data Access Object implementation for the Notification model
 */
type NotificationDao struct {
	*data.BaseDbDao
}

/*
 Chanel used to pass notifications to the server front end layer, which will send them to any registered users on
 the client side
*/
var NotifyChan = make(chan *model.Notification, 25)

/*
 * Creates a new Notification data access object
 */
func NewNotificationDao(template *data.SqlTemplate, dictionary map[string]string) (*NotificationDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &NotificationDao{base}, nil
}

/*
 Creates a notification with the given source and type
*/
func Notify(dao *data.BaseDbDao, txt *data.TxTemplate, source uint, ntype model.NotificationType) (uint, error) {
	var query = dao.GetQuery("pnc.get-subscriptions-by-type")
	var result, err = txt.Select(query, mapSubscription, ntype)

	if err != nil && err != data.NOT_FOUND {
		return 0, err
	}

	var subs = castSubscriptions(result)
	var total uint
	query = dao.GetQuery("pnc.insert-notification")

	for _, sub := range subs {
		var stamp = time.Now()
		id, err := txt.Insert(query,
			sub.Id(),
			sub.IdUser(),
			source,
			ntype,
			stamp.Format(data.DATE_TIME_FORMAT),
		)

		if err != nil && data.CheckConstraintError(err) == data.CONSTRAINT {
			continue
		} else if err != nil {
			return 0, err
		}

		total++
		notification, err := model.NewNotification(uint(id), sub.Id(), sub.IdUser(), source, ntype, stamp)

		if err != nil {
			return 0, err
		}

		NotifyChan <- notification
	}

	return total, nil
}

/*
 * Maps a notification object from a result set row
 */
func mapNotification(rows *sql.Rows) (interface{}, error) {
	var id, idSubscription, idUser, idSource, typeUint uint
	var timestampStr string

	var err = rows.Scan(&id, &idSubscription, &idUser, &idSource, &typeUint, &timestampStr)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewNotification(id, idSubscription, idUser, idSource, model.NotificationType(typeUint), timestamp)
}

/**
 * Retrieves a notification as identified by a given ID
 */
func (dao *NotificationDao) Get(id uint) (*model.Notification, error) {
	var query = dao.GetQuery("pnc.get-notification")
	var result, err = dao.Template().SelectFirst(query, mapNotification, id)

	if result == nil || err != nil {
		return nil, err
	}

	return result.(*model.Notification), nil
}

/**
 * Retrieves all notifications which are in queue for a given user
 */
func (dao *NotificationDao) GetByUser(idUser uint) ([]*model.Notification, error) {
	var query = dao.GetQuery("pnc.get-notifications-for-user")
	var result, err = dao.Template().Select(query, mapNotification, idUser)

	return castNotifications(result), err
}

/**
 * Retrieves all notifications that are tied to a particular object source
 */
func (dao *NotificationDao) GetBySource(idSource uint, notificationType model.NotificationType) ([]*model.Notification, error) {
	var query = dao.GetQuery("pnc.get-notifications-for-source-and-type")
	var result, err = dao.Template().Select(query, mapNotification, idSource, notificationType)

	return castNotifications(result), err
}

/**
 * Add a notification.
 */
func (dao *NotificationDao) Add(notification *model.Notification) (uint, error) {
	var query = dao.GetQuery("pnc.insert-notification")
	var result, err = dao.Template().Insert(query,
		notification.IdSubscription(),
		notification.IdUser(),
		notification.IdSource(),
		notification.NotificationType(),
		notification.Timestamp().Format(data.DATE_TIME_FORMAT),
	)

	if err != nil {
		return 0, err
	}

	notification.SetId(uint(result))
	return notification.Id(), nil
}

/**
 * Remove a noitification
 */
func (dao *NotificationDao) Remove(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.remove-notification")
	var result, err = dao.Template().Update(query, id)

	return uint(result), err
}

/**
 * Remove all notifications for a given user. Effecively empties out a user's queue.
 */
func (dao *NotificationDao) RemoveByUser(idUser uint) (uint, error) {
	var query = dao.GetQuery("pnc.clear-user-notitifications")
	var result, err = dao.Template().Update(query, idUser)

	return uint(result), err
}

/**
 * Removes all notifications for a given object source
 */
func (dao *NotificationDao) RemoveBySource(idSource uint, notificationType model.NotificationType) (uint, error) {
	var query = dao.GetQuery("pnc.clear-source-notifications")
	var result, err = dao.Template().Update(query, idSource, notificationType)

	return uint(result), err
}

/*
 Casts a group of notification objects into notification type
*/
func castNotifications(result []interface{}) []*model.Notification {
	if result == nil {
		return nil
	}

	var notifications = make([]*model.Notification, len(result))

	for cont, n := range result {
		notifications[cont] = n.(*model.Notification)
	}

	return notifications
}
