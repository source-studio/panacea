package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
)

/*
 Data access object implementation for the Subscription model
*/
type SubscriptionDao struct {
	*data.BaseDbDao
}

/*
 Creates a new subscription data access object
*/
func NewSubscriptionDao(template *data.SqlTemplate, dictionary map[string]string) (*SubscriptionDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &SubscriptionDao{base}, nil
}

/*
 Maps a subscription from a result set row
*/
func mapSubscription(rows *sql.Rows) (interface{}, error) {
	var id, idUser, typeUint uint

	var err = rows.Scan(&id, &idUser, &typeUint)

	if err != nil {
		return nil, err
	}

	return model.NewSubscription(id, idUser, model.NotificationType(typeUint))
}

/*
 Retrieves the subscription identified by the given ID
*/
func (dao *SubscriptionDao) Get(id uint) (*model.Subscription, error) {
	var query = dao.GetQuery("pnc.get-subscription")
	var result, err = dao.Template().SelectFirst(query, mapSubscription, id)

	if result == nil || err != nil {
		return nil, err
	}

	return result.(*model.Subscription), nil
}

/*
 Retrieve all the subscriptions for a given user
*/
func (dao *SubscriptionDao) GetByUser(idUser uint) ([]*model.Subscription, error) {
	var query = dao.GetQuery("pnc.get-subscriptions-for-user")
	var result, err = dao.Template().Select(query, mapSubscription, idUser)

	return castSubscriptions(result), err
}

/*
 Sychronizes subscriptions for a user. Removes all existing subscriptions and creates new ones, based on the
 notification types speficied as arguments
*/
func (dao *SubscriptionDao) Sync(idUser uint, ntypes []model.NotificationType) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.clear-subscriptions-for-user")
		_, err := txt.Update(query, idUser)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.subscribe-user")

		for _, ntype := range ntypes {
			count, err := txt.Update(query, idUser, ntype)

			if err != nil {
				return err
			}

			records += count
		}

		return nil
	}
	err = txt.Execute(tx)

	return uint(records), err
}

/*
 Removes a subscription
*/
func (dao *SubscriptionDao) Remove(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.unsubscribe-user")
	var result, err = dao.Template().Update(query, id)

	return uint(result), err
}

/*
 Casts a slice of subscription objects into subscription type
*/
func castSubscriptions(result []interface{}) []*model.Subscription {
	if result == nil {
		return nil
	}

	var subscriptions = make([]*model.Subscription, len(result))

	for cont, s := range result {
		subscriptions[cont] = s.(*model.Subscription)
	}

	return subscriptions
}
