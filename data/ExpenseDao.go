package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Data access object implementation for the expense model
*/
type ExpenseDao struct {
	*data.BaseDbDao
}

/*
 Creates a new Expense data access object
*/
func NewExpenseDao(template *data.SqlTemplate, dictionary map[string]string) (*ExpenseDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var dao = &ExpenseDao{base}
	return dao, nil
}

/*
 Maps a expense model object from row data
*/
func mapExpense(rows *sql.Rows) (interface{}, error) {
	var id, idUser, include uint
	var timestampStr string
	var amount float32
	var reason []byte

	var err = rows.Scan(&id, &idUser, &timestampStr, &reason, &amount, &include)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewExpense(id, idUser, timestamp, string(reason), amount, include > 0)
}

/*
 Retreives an expense object identified by a given ID
*/
func (dao *ExpenseDao) Get(id uint) (*model.Expense, error) {
	var query = dao.GetQuery("pnc.get-expense")
	var result, err = dao.Template().SelectFirst(query, mapExpense, id)

	if result == nil || err != nil {
		return nil, err
	}

	return result.(*model.Expense), nil
}

/*
 Retrieves all available expense objects in the collection
*/
func (dao *ExpenseDao) GetAll() ([]*model.Expense, error) {
	var query = dao.GetQuery("pnc.get-expenses")
	var result, err = dao.Template().Select(query, mapExpense)

	return castExpenses(result), err
}

/*
 Retreives all expense objects which were created within the provided date range
*/
func (dao *ExpenseDao) Search(from, to time.Time, user uint) ([]*model.Expense, error) {
	var query string
	var result []interface{}
	var err error

	if user < 1 {
		query = dao.GetQuery("pnc.get-expenses-by-time-range")
		result, err = dao.Template().Select(query, mapExpense,
			from.Format(data.DATE_TIME_FORMAT),
			to.Format(data.DATE_TIME_FORMAT),
		)
	} else {
		query = dao.GetQuery("pnc.get-expenses-by-time-range-and-user")
		result, err = dao.Template().Select(query, mapExpense,
			from.Format(data.DATE_TIME_FORMAT),
			to.Format(data.DATE_TIME_FORMAT),
			user,
		)
	}

	return castExpenses(result), err
}

/*
 Creates a new expnse object
*/
func (dao *ExpenseDao) Add(expense *model.Expense) (uint, error) {
	var query = dao.GetQuery("pnc.add-expense")
	var result, err = dao.Template().Insert(query,
		expense.IdUser(),
		expense.Timestamp().Format(data.DATE_TIME_FORMAT),
		expense.Reason(),
		expense.Amount(),
		data.ToNumber(expense.IncludeInBalance()),
	)

	if err != nil {
		return 0, err
	}

	expense.SetId(uint(result))
	return expense.Id(), nil
}

/*
 Updates the details of an expense object. Only the amount and reason will be updated.
*/
func (dao *ExpenseDao) Update(expense *model.Expense) (uint, error) {
	var query = dao.GetQuery("pnc.update-expense")
	var result, err = dao.Template().Update(query,
		expense.Reason(),
		expense.Amount(),
		data.ToNumber(expense.IncludeInBalance()),
		expense.Id(),
	)

	return uint(result), err
}

/*
 Removes a given expense object identified by the provided ID.
*/
func (dao *ExpenseDao) Remove(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.remove-expense")
	var result, err = dao.Template().Update(query, id)

	return uint(result), err
}

/*
 Casts a collection of expense objects into Expende type
*/
func castExpenses(result []interface{}) []*model.Expense {
	if result == nil {
		return nil
	}

	var expenses = make([]*model.Expense, len(result))

	for cont, e := range result {
		expenses[cont] = e.(*model.Expense)
	}

	return expenses
}
