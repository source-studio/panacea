package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
)

/*
 Data driven data access object implemention for the Customer model
*/
type CustomerDao struct {
	*data.BaseDbDao
}

/*
 Struct used to hold data about the relationship between an address and the customer (id) it belongs to
*/
type customerAddress struct {
	idCustomer uint           // ID of the customer that holds the address
	address    *model.Address // Address data
}

/*
 Struct used to hold data about the relationship between a constact and the customer (id) it belongs to
*/
type customerContact struct {
	idCustomer uint           // ID of the customer that holds the contact
	contact    *model.Contact // Contact data
}

/*
 Creates a new Customer data access object
*/
func NewCustomerDao(template *data.SqlTemplate, dictionary map[string]string) (*CustomerDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &CustomerDao{base}, nil
}

/*
 Maps a row holding customer data into a customer model object. Only expects base customer data to be
 present. Address and contact data is mapped separately.
*/
func mapCustomer(rows *sql.Rows) (interface{}, error) {
	var id uint
	var firstName, lastName string
	var nickName []byte

	var err = rows.Scan(&id, &firstName, &lastName, &nickName)

	if err != nil {
		return nil, err
	}

	return model.NewCustomer(id, firstName, lastName, string(nickName))
}

/*
 Maps a row holding a customer's address into a native struct. The ID of the customer that holds the
 address, as well as the address object are produced to facilite mapping.
*/
func mapCustomerAddress(rows *sql.Rows) (interface{}, error) {
	var idAddress, idCustomer uint
	var street1, city, province string
	var street2, country []byte

	var err = rows.Scan(&idCustomer, &idAddress, &street1, &street2, &city, &province, &country)

	if err != nil {
		return nil, err
	}

	address, err := model.NewAddress(idAddress, street1, string(street2), city, province, string(country))

	if err != nil {
		return nil, err
	}

	var ca = customerAddress{idCustomer, address}
	return &ca, nil
}

/*
 Maps a row holding a customer's contact into a native struct. The ID of the customer that holds the
 contact, as well as the contact object are produced to facilitate mapping.
*/
func mapCustomerContact(rows *sql.Rows) (interface{}, error) {
	var idCustomer, idContact, typeUi uint
	var uuid, value string

	var err = rows.Scan(&idCustomer, &idContact, &uuid, &typeUi, &value)

	if err != nil {
		return nil, err
	}

	contactType, err := model.ToContactType(typeUi)

	if err != nil {
		return nil, err
	}

	contact, err := model.NewContact(idContact, uuid, contactType, value)

	if err != nil {
		return nil, err
	}

	var cc = customerContact{idCustomer, contact}
	return &cc, nil
}

/*
 Retrieves a single customer from the collection. Needs to match the provied ID.
*/
func (dao *CustomerDao) Get(id uint) (*model.Customer, error) {
	var customer *model.Customer
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-customer")
		c, err := txt.SelectFirst(query, mapCustomer, id)

		if err != nil {
			return err
		}

		customer = c.(*model.Customer)
		query = dao.GetQuery("pnc.get-customer-address")
		car, err := txt.SelectFirst(query, mapCustomerAddress, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if car != nil {
			var ca = car.(*customerAddress)
			customer.SetAddress(ca.address)
		}

		query = dao.GetQuery("pnc.get-customer-contacts")
		ccr, err := txt.Select(query, mapCustomerContact, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, c := range ccr {
			var cc = c.(*customerContact)
			customer.Add(cc.contact)
		}

		return nil
	}

	err = txt.Execute(tx)
	return customer, err
}

/*
 Retreives all customers in the collection
*/
func (dao *CustomerDao) GetAll() ([]*model.Customer, error) {
	var customers []*model.Customer
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-customers")
		result, err := txt.Select(query, mapCustomer)

		if err != nil {
			return err
		}

		customers = castCustomers(result)
		query = dao.GetQuery("pnc.get-all-customer-addresses")
		result, err = txt.Select(query, mapCustomerAddress)

		if err != nil {
			return err
		}

		var addresses = castCustomerAddresses(result)
		matchCustomerAddresses(customers, addresses)
		query = dao.GetQuery("pnc.get-all-customer-contacts")
		result, err = txt.Select(query, mapCustomerContact)

		if err != nil {
			return err
		}

		var contacts = castCustomerContacts(result)
		matchCustomerContacts(customers, contacts)
		return nil
	}

	err = txt.Execute(tx)
	return customers, err
}

/*
 Searches for customers. Uses the given criteria to search. Attempts to match the customer name against the criteria.
*/
func (dao *CustomerDao) Search(criteria string) ([]*model.Customer, error) {
	var customers []*model.Customer = nil
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var input = "%" + criteria + "%"
		var query = dao.GetQuery("pnc.search-customers")
		var result, err = txt.Select(query, mapCustomer, input, input)

		if err != nil {
			return err
		}

		customers = castCustomers(result)
		query = dao.GetQuery("pnc.search-customer-addresses")
		result, err = txt.Select(query, mapCustomerAddress, input, input)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var addresses = castCustomerAddresses(result)
		matchCustomerAddresses(customers, addresses)
		query = dao.GetQuery("pnc.search-customer-contacts")
		result, err = txt.Select(query, mapCustomerContact, input, input)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castCustomerContacts(result)
		matchCustomerContacts(customers, contacts)
		return nil
	}

	err = txt.Execute(tx)
	return customers, err
}

/*
 Adds a new customer to the collection. The newly generated ID for the customer is returned.
*/
func (dao *CustomerDao) Add(customer *model.Customer) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-customer")
		id, err := txt.Insert(query,
			customer.FirstName(),
			customer.LastName(),
			customer.NickName(),
		)

		if err != nil {
			return err
		}

		customer.SetId(uint(id))

		if customer.Address() != nil {
			query = dao.GetQuery("pnc.add-address")
			idAddress, err := txt.Insert(query,
				customer.Address().Street1(),
				customer.Address().Street2(),
				customer.Address().City(),
				customer.Address().Province(),
				customer.Address().Country(),
			)

			if err != nil {
				return err
			}

			customer.Address().SetId(uint(idAddress))
			query = dao.GetQuery("pnc.link-address-to-customer")
			_, err = txt.Update(query,
				customer.Id(),
				customer.Address().Id(),
			)

			if err != nil {
				return err
			}
		}

		query = dao.GetQuery("pnc.add-contact")
		var linkQuery = dao.GetQuery("pnc.link-contact-to-customer")

		for _, contact := range customer.Contacts() {
			id, err = txt.Insert(query,
				contact.Uuid(),
				uint(contact.ContactType()),
				contact.Value(),
			)

			if err != nil {
				return err
			}

			contact.SetId(uint(id))
			_, err = txt.Update(linkQuery,
				customer.Id(),
				contact.Id(),
			)

			if err != nil {
				return err
			}
		}

		return nil
	}

	err = txt.Execute(tx)
	return customer.Id(), err
}

/*
 Updates an existing customer. Uses the customer ID as search criteria to find the elements to update.
 All address and contact data is synced.
*/
func (dao *CustomerDao) Update(customer *model.Customer) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.update-customer")
		result, err := txt.Update(query,
			customer.FirstName(),
			customer.LastName(),
			customer.NickName(),
			customer.Id(),
		)

		if err != nil {
			return err
		}

		records = uint(result)
		err = dao.syncAddress(txt, customer)

		if err != nil {
			return err
		}

		return dao.syncContacts(txt, customer)
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Removes a customer from the collection as identified by the given ID. Removes any corresponding address
 and contact information.
*/
func (dao *CustomerDao) Remove(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-customer-address")
		var addresses, err = txt.Select(query, mapCustomerAddress, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if addresses != nil {
			query = dao.GetQuery("pnc.unlink-address-from-customer")
			_, err := txt.Update(query, id)

			if err != nil {
				return err
			}

			var address *customerAddress

			for _, ca := range addresses {
				address = ca.(*customerAddress)
				break
			}

			if address != nil {
				query = dao.GetQuery("pnc.remove-address")
				_, err = txt.Update(query, address.address.Id())

				if err != nil {
					return err
				}
			}
		}

		query = dao.GetQuery("pnc.get-customer-contacts")
		contacts, err := txt.Select(query, mapCustomerContact, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var cc = castCustomerContacts(contacts)

		if cc != nil && len(cc) > 0 {
			query = dao.GetQuery("pnc.unlink-all-contacts-from-customer")
			_, err = txt.Update(query, id)

			if err != nil {
				return err
			}

			query = dao.GetQuery("pnc.remove-contact")

			for _, c := range cc {
				_, err := txt.Update(query, c.contact.Id())

				if err != nil {
					return err
				}
			}
		}

		query = dao.GetQuery("pnc.remove-customer")
		result, err := txt.Update(query, id)
		records = uint(result)

		return data.CheckConstraintError(err)
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Synchronizes address data for a customer. Makes the address representation in the model the
 data that is held in the data store.
*/
func (dao *CustomerDao) syncAddress(txt *data.TxTemplate, customer *model.Customer) error {
	var address = customer.Address()
	var query string
	var err error

	if address == nil {
		query = dao.GetQuery("pnc.get-customer-address")
		var car, err = txt.SelectFirst(query, mapCustomerAddress, customer.Id())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if car != nil {
			query = dao.GetQuery("pnc.unlink-address-from-customer")
			_, err = txt.Update(query, customer.Id())

			if err != nil {
				return err
			}

			var existing = car.(*model.Address)
			query = dao.GetQuery("pnc.remove-address")
			_, err = txt.Update(query, existing.Id())

			if err != nil {
				return err
			}
		}

		return nil
	}

	if address.Id() < 1 {
		query = dao.GetQuery("pnc.add-address")
		id, err := txt.Insert(query,
			address.Street1(),
			address.Street2(),
			address.City(),
			address.Province(),
			address.Country(),
		)

		address.SetId(uint(id))

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.link-address-to-customer")
		_, err = txt.Update(query,
			customer.Id(),
			address.Id(),
		)
	} else {
		query = dao.GetQuery("pnc.update-address")
		_, err = txt.Update(query,
			address.Street1(),
			address.Street2(),
			address.City(),
			address.Province(),
			address.Country(),
			address.Id(),
		)
	}

	return err
}

/*
 Synchronizes contact data for a customer. Makes the contact representations held in the model
 the data that is saved in the data store.
*/
func (dao *CustomerDao) syncContacts(txt *data.TxTemplate, customer *model.Customer) error {
	var query = dao.GetQuery("pnc.get-customer-contacts")
	var result, err = txt.Select(query, mapCustomerContact, customer.Id())

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	var cc = castCustomerContacts(result)
	var existing = make([]*model.Contact, len(cc))

	for cont, c := range cc {
		existing[cont] = c.contact
	}

	var unlinkQuery = dao.GetQuery("pnc.unlink-contact-from-customer")
	var removeQuery = dao.GetQuery("pnc.remove-contact")

	for _, contact := range existing {
		if !customer.HasContact(contact) {
			_, err = txt.Update(unlinkQuery, customer.Id(), contact.Id())

			if err != nil {
				return err
			}

			_, err = txt.Update(removeQuery, contact.Id())

			if err != nil {
				return err
			}
		}
	}

	var addQuery = dao.GetQuery("pnc.add-contact")
	var updateQuery = dao.GetQuery("pnc.update-contact")
	var linkQuery = dao.GetQuery("pnc.link-contact-to-customer")

	for _, contact := range customer.Contacts() {
		if containsContact(existing, contact) {
			_, err = txt.Update(updateQuery,
				uint(contact.ContactType()),
				contact.Value(),
				contact.Id(),
			)
		} else {
			id, err := txt.Insert(addQuery,
				contact.Uuid(),
				uint(contact.ContactType()),
				contact.Value(),
			)

			if err != nil {
				return err
			}

			contact.SetId(uint(id))

			_, err = txt.Update(linkQuery,
				customer.Id(),
				contact.Id(),
			)
		}

		if err != nil {
			return err
		}
	}

	return nil
}

/*
 Casts a collection of customers which are provided in interface{} type to native model types.
*/
func castCustomers(result []interface{}) []*model.Customer {
	if result == nil {
		return nil
	}

	var customers = make([]*model.Customer, len(result))

	for cont, c := range result {
		customers[cont] = c.(*model.Customer)
	}

	return customers
}

/*
 Casts a collection of customer addresses which are provided in interface{} type to native model types.
*/
func castCustomerAddresses(result []interface{}) []*customerAddress {
	if result == nil {
		return nil
	}

	var addresses = make([]*customerAddress, len(result))

	for cont, a := range result {
		addresses[cont] = a.(*customerAddress)
	}

	return addresses
}

/*
 Casts a collection of customer contacts which are pvodied in interface{} type to native model types.
*/
func castCustomerContacts(result []interface{}) []*customerContact {
	if result == nil {
		return nil
	}

	var contacts = make([]*customerContact, len(result))

	for cont, c := range result {
		contacts[cont] = c.(*customerContact)
	}

	return contacts
}

/*
 Attaches a collection of customer address objects to their corresponding customer objects.
*/
func matchCustomerAddresses(customers []*model.Customer, addresses []*customerAddress) {
	var customerMap = make(map[uint]*model.Customer, len(customers))

	for _, customer := range customers {
		customerMap[customer.Id()] = customer
	}

	for _, address := range addresses {
		var customer = customerMap[address.idCustomer]
		customer.SetAddress(address.address)
	}
}

/*
 Attaches a collection of customer contact objects to their corresponding customer objects.
*/
func matchCustomerContacts(customers []*model.Customer, contacts []*customerContact) {
	var customerMap = make(map[uint]*model.Customer, len(customers))

	for _, customer := range customers {
		customerMap[customer.Id()] = customer
	}

	for _, contact := range contacts {
		var customer = customerMap[contact.idCustomer]
		customer.Add(contact.contact)
	}
}

/*
 Determines if a contact is held within a collection of contacts. Uses the UUID property as
 search criteria.
*/
func containsContact(contacts []*model.Contact, contact *model.Contact) bool {
	if contacts == nil || len(contacts) < 1 {
		return false
	}

	for _, c := range contacts {
		if c.Uuid() == contact.Uuid() {
			return true
		}
	}

	return false
}
