package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"strconv"
	"strings"
	"time"
)

/*
 Database driven data access object implementation for the Balance model
*/
type BalanceDao struct {
	*data.BaseDbDao
}

/*
 Creates a new data access object for the balance model
*/
func NewBalanceDao(template *data.SqlTemplate, dictionary map[string]string) (*BalanceDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &BalanceDao{base}, nil
}

/*
 Maps a balance from a result set row
*/
func mapBalance(rows *sql.Rows) (interface{}, error) {
	var id, idUser uint
	var timestampStr string
	var start, cashSales, creditSales, refunds, payments, expenses, deposits, nextStart, balance, difference float32
	var notes []byte

	var err = rows.Scan(&id, &idUser, &timestampStr, &start, &cashSales, &creditSales, &refunds, &payments, &expenses,
		&deposits, &nextStart, &balance, &difference, &notes)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewBalance(id, idUser, timestamp, start, cashSales, creditSales, refunds, payments, expenses, deposits,
		nextStart, balance, difference, string(notes))
}

/*
 Retrieves the balance identified by the given ID
*/
func (dao *BalanceDao) Get(id uint) (*model.Balance, error) {
	var query = dao.GetQuery("pnc.get-balance")
	var result, err = dao.Template().SelectFirst(query, mapBalance, id)

	if result == nil || err != nil {
		return nil, err
	}

	var balance = result.(*model.Balance)
	return balance, nil
}

/*
 Returns the last balance that was stored
*/
func (dao *BalanceDao) GetLast() (*model.Balance, error) {
	var query = dao.GetQuery("pnc.get-last-balance")
	var result, err = dao.Template().SelectFirst(query, mapBalance)

	if result == nil || err != nil {
		return nil, err
	}

	var balance = result.(*model.Balance)
	return balance, nil
}

/*
 Retrieves all available balances
*/
func (dao *BalanceDao) GetAll() ([]*model.Balance, error) {
	var query = dao.GetQuery("pnc.get-balances")
	var result, err = dao.Template().Select(query, mapBalance)

	return castBalances(result), err
}

/*
 Retrieves all balances created within the provided date range
*/
func (dao *BalanceDao) GetByRange(from, to time.Time) ([]*model.Balance, error) {
	var query = dao.GetQuery("pnc.get-balances-by-time-range")
	var result, err = dao.Template().Select(query, mapBalance,
		from.Format(data.DATE_TIME_FORMAT),
		to.Format(data.DATE_TIME_FORMAT),
	)

	return castBalances(result), err
}

/*
 Searches balances. The time range values are required. The user value is optional, can be passed as zero.
*/
func (dao *BalanceDao) Search(from, to time.Time, idUser uint) ([]*model.Balance, error) {
	var query = []string{dao.GetQuery("pnc.get-balances-by-time-range")}

	if idUser > 0 {
		query = append(query, " AND id_user = ")
		query = append(query, strconv.FormatUint(uint64(idUser), 10))
	}

	query = append(query, " ORDER BY timestamp DESC")

	var balances []*model.Balance
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var fromStr, toStr = from.Format(data.DATE_TIME_FORMAT), to.Format(data.DATE_TIME_FORMAT)
		result, err := txt.Select(strings.Join(query, ""), mapBalance, fromStr, toStr)

		if err != nil {
			return err
		}

		balances = castBalances(result)
		return nil
	}

	err = txt.Execute(tx)
	return balances, err
}

/*
 Generates part of the information required to create a new balance. Queries activity logged since the last balance was
 generated, as well as info from the last balance. However this is an incomplete picture, the user will still need to
 confirm data and reconcile cash at hand before this can be saved as a new balance.
*/
func (dao *BalanceDao) Generate(idUser uint) (*model.Balance, error) {
	var balance *model.Balance
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-last-balance")
		var lbr, err = txt.SelectFirst(query, mapBalance)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var lastDate string
		var lastStart float32

		if lbr == nil || err == data.NOT_FOUND {
			lastDate = time.Date(1970, 1, 1, 0, 0, 0, 0, time.Now().Location()).Format(data.DATE_TIME_FORMAT)
			lastStart = 0
		} else {
			var lastBalance = lbr.(*model.Balance)
			lastDate = lastBalance.Timestamp().Format(data.DATE_TIME_FORMAT)
			lastStart = lastBalance.NextStart()
		}

		var now = time.Now().Format(data.DATE_TIME_FORMAT)
		query = dao.GetQuery("pnc.get-invoices-by-date-range-and-state")
		irs, err := txt.Select(query, mapInvoice, lastDate, now, uint(model.OK))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var cashTotal, creditTotal, refundTotal, paymentTotal, expenseTotal, depositTotal float32
		for _, ir := range irs {
			var invoice = ir.(*model.Invoice)

			switch invoice.Payment() {
			case model.CASH:
				cashTotal += invoice.Total() - invoice.Discount()
			case model.CREDIT:
				creditTotal += invoice.Total() - invoice.Discount()
			}
		}

		query = dao.GetQuery("pnc.get-refunds-by-date-range")
		rrs, err := txt.Select(query, mapRefund, lastDate, now)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, pr := range rrs {
			var refund = pr.(*model.Refund)
			refundTotal += refund.Actual()
		}

		query = dao.GetQuery("pnc.get-payments-by-time-range")
		prs, err := txt.Select(query, mapPayment, lastDate, now)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, pr := range prs {
			var payment = pr.(*model.Payment)
			paymentTotal += payment.Amount()
		}

		query = dao.GetQuery("pnc.get-expenses-by-time-range-and-inclusion")
		ers, err := txt.Select(query, mapExpense, lastDate, now, data.ToNumber(true))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, er := range ers {
			var expense = er.(*model.Expense)
			expenseTotal += expense.Amount()
		}

		query = dao.GetQuery("pnc.get-deposits-by-time-range-and-inclusion")
		drs, err := txt.Select(query, mapDeposit, lastDate, now, data.ToNumber(true))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, dr := range drs {
			var deposit = dr.(*model.Deposit)
			depositTotal += deposit.Amount()
		}

		var balanceTotal = lastStart + cashTotal - refundTotal + paymentTotal + depositTotal - expenseTotal
		balance, err = model.NewBalance(0, idUser, time.Now(), lastStart, cashTotal, creditTotal, refundTotal,
			paymentTotal, expenseTotal, depositTotal, 0, balanceTotal, 0, "")

		return err
	}
	err = txt.Execute(tx)
	return balance, err
}

/*
 Creates a new balance
*/
func (dao *BalanceDao) Add(balance *model.Balance) (uint, error) {
	var query = dao.GetQuery("pnc.add-balance")
	var result, err = dao.Template().Insert(query,
		balance.IdUser(),
		balance.Timestamp().Format(data.DATE_TIME_FORMAT),
		balance.Start(),
		balance.CashSales(),
		balance.CreditSales(),
		balance.Refunds(),
		balance.Payments(),
		balance.Expenses(),
		balance.Deposits(),
		balance.NextStart(),
		balance.Balance(),
		balance.Difference(),
		balance.Notes(),
	)

	if err != nil {
		return 0, err
	}

	balance.SetId(uint(result))
	return balance.Id(), nil
}

/*
 Updates the details on an existing balance object
*/
func (dao *BalanceDao) Update(balance *model.Balance) (uint, error) {
	var query = dao.GetQuery("pnc.update-balance")
	var result, err = dao.Template().Update(query,
		balance.NextStart(),
		balance.Balance(),
		balance.Difference(),
		balance.Notes(),
		balance.Id(),
	)

	return uint(result), err
}

/*
 Removes a balance object
*/
func (dao *BalanceDao) Remove(id uint) (uint, error) {
	var result int64
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-last-balance")
		var lbr, err = txt.SelectFirst(query, mapBalance)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if err != data.NOT_FOUND {
			var lastBalance = lbr.(*model.Balance)

			if lastBalance.Id() != id {
				return data.CONFLICT
			}
		}

		query = dao.GetQuery("pnc.remove-balance")
		result, err = txt.Update(query, id)

		return err
	}
	err = txt.Execute(tx)

	return uint(result), err
}

/*
 Casts slice of balances to balance type
*/
func castBalances(result []interface{}) []*model.Balance {
	if result == nil {
		return nil
	}

	var balances = make([]*model.Balance, len(result))

	for cont, r := range result {
		balances[cont] = r.(*model.Balance)
	}

	return balances
}
