package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Databse driven data access object implementation for the Supplier model
*/
type SupplierDao struct {
	*data.BaseDbDao
}

/*
 Utility struct used to map contacts to their parent supplier object
*/
type supplierContact struct {
	idSupplier uint
	contact    *model.Contact
}

/*
 Creates a new Supplier dao
*/
func NewSupplierDao(template *data.SqlTemplate, dictionary map[string]string) (*SupplierDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &SupplierDao{base}, nil
}

/*
 Maps a row containing supplier data into a native model object
*/
func mapSupplier(rows *sql.Rows) (interface{}, error) {
	var id uint
	var organization, firstName, lastName string

	var err = rows.Scan(&id, &organization, &firstName, &lastName)

	if err != nil {
		return nil, err
	}

	return model.NewSupplier(id, organization, firstName, lastName)
}

/*
 Maps a row containing order data into a native model object
*/
func mapOrder(rows *sql.Rows) (interface{}, error) {
	var id, idSupplier, paid uint
	var timestampStr string
	var paidStr []byte
	var reference []byte
	var amount float32
	var paidDate *time.Time

	var err = rows.Scan(&id, &idSupplier, &timestampStr, &reference, &amount, &paid, &paidStr)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	if len(paidStr) > 0 {
		tmp, err := time.Parse(data.DATE_TIME_FORMAT, string(paidStr))

		if err != nil {
			return nil, err
		}

		paidDate = &tmp
	}

	return model.NewOrder(id, idSupplier, timestamp, string(reference), amount, paid > 0, paidDate)
}

/*
 Maps a row containing credit note data into a native model object
*/
func mapCreditNote(rows *sql.Rows) (interface{}, error) {
	var id, idSupplier, redeemed uint
	var timestampStr string
	var reference, reason, redeemedStr []byte
	var amount float32
	var redeemedDate *time.Time

	var err = rows.Scan(&id, &idSupplier, &timestampStr, &reference, &amount, &reason, &redeemed, &redeemedStr)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	if len(redeemedStr) > 0 {
		tmp, err := time.Parse(data.DATE_TIME_FORMAT, string(redeemedStr))

		if err != nil {
			return nil, err
		}

		redeemedDate = &tmp
	}

	return model.NewCreditNote(id, idSupplier, timestamp, string(reference), amount, string(reason), redeemed > 0, redeemedDate)
}

/*
 maps a row containing supplier contact data into a native model object
*/
func mapSupplierContact(rows *sql.Rows) (interface{}, error) {
	var idSupplier, idContact, typeUi uint
	var uuid, value string

	var err = rows.Scan(&idSupplier, &idContact, &uuid, &typeUi, &value)

	if err != nil {
		return nil, err
	}

	contactType, err := model.ToContactType(typeUi)

	if err != nil {
		return nil, err
	}

	contact, err := model.NewContact(idContact, uuid, contactType, value)

	if err != nil {
		return nil, err
	}

	var sc = supplierContact{idSupplier, contact}
	return &sc, nil
}

/*
 Retrieve a supplier as identified by a provided ID
*/
func (dao *SupplierDao) Get(id uint) (*model.Supplier, error) {
	var supplier *model.Supplier
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-supplier")
		s, err := txt.SelectFirst(query, mapSupplier, id)

		if s == nil || err != nil {
			return err
		}

		supplier = s.(*model.Supplier)
		query = dao.GetQuery("pnc.get-supplier-contacts")
		scr, err := txt.Select(query, mapSupplierContact, id)

		if err != nil {
			return err
		}

		for _, c := range scr {
			var sc = c.(*supplierContact)
			supplier.Add(sc.contact)
		}

		return nil
	}

	err = txt.Execute(tx)
	return supplier, err
}

/*
 Retrieve all providers in the collection
*/
func (dao *SupplierDao) GetAll() ([]*model.Supplier, error) {
	var suppliers []*model.Supplier
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-suppliers")
		result, err := txt.Select(query, mapSupplier)

		if err != nil {
			return err
		}

		suppliers = castSuppliers(result)
		query = dao.GetQuery("pnc-get-all-supplier-contacts")
		result, err = txt.Select(query, mapSupplierContact)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castSupplierContacts(result)
		matchSupplierContacts(suppliers, contacts)
		return nil
	}

	err = txt.Execute(tx)
	return suppliers, err
}

/*
 Search in the catalog of current suppliers based on organization criteria. Returns all suppliers that match
*/
func (dao *SupplierDao) Search(criteria string) ([]*model.Supplier, error) {
	var suppliers []*model.Supplier
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.search-suppliers")
		var criteriaVal = "%" + criteria + "%"
		result, err := txt.Select(query, mapSupplier, criteriaVal, criteriaVal)

		if err != nil {
			return err
		}

		suppliers = castSuppliers(result)
		query = dao.GetQuery("pnc.search-supplier-contacts")
		result, err = txt.Select(query, mapSupplierContact, criteriaVal, criteriaVal)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castSupplierContacts(result)
		matchSupplierContacts(suppliers, contacts)
		return nil
	}

	err = txt.Execute(tx)
	return suppliers, err
}

/*
 Adds a new supplier to the collection
*/
func (dao *SupplierDao) Add(supplier *model.Supplier) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-supplier")
		var id, err = txt.Insert(query,
			supplier.Organization(),
			supplier.FirstName(),
			supplier.LastName(),
		)

		if err != nil {
			return err
		}

		supplier.SetId(uint(id))
		query = dao.GetQuery("pnc.add-contact")
		var linkQuery = dao.GetQuery("pnc.link-contact-to-supplier")

		for _, contact := range supplier.Contacts() {
			id, err = txt.Insert(query,
				contact.Uuid(),
				uint(contact.ContactType()),
				contact.Value(),
			)

			if err != nil {
				return err
			}

			contact.SetId(uint(id))

			_, err = txt.Update(linkQuery,
				supplier.Id(),
				contact.Id(),
			)

			if err != nil {
				return err
			}
		}

		return nil
	}

	err = txt.Execute(tx)
	return supplier.Id(), err
}

/*
 Updates a given supplier
*/
func (dao *SupplierDao) Update(supplier *model.Supplier) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.update-supplier")
		result, err := txt.Update(query,
			supplier.Organization(),
			supplier.FirstName(),
			supplier.LastName(),
			supplier.Id(),
		)

		if err != nil {
			return err
		}

		records = uint(result)
		return dao.syncContacts(txt, supplier)
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Removes the provider identified by the given ID
*/
func (dao *SupplierDao) Remove(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-supplier-contacts")
		scr, err := txt.Select(query, mapSupplierContact, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castSupplierContacts(scr)

		if contacts != nil && len(contacts) > 0 {
			query = dao.GetQuery("pnc.unlink-all-contacts-from-supplier")
			_, err = txt.Update(query, id)

			if err != nil {
				return err
			}

			query = dao.GetQuery("pnc.remove-contact")
			for _, contact := range contacts {
				_, err = txt.Update(query, contact.contact.Id())

				if err != nil {
					return err
				}
			}
		}

		query = dao.GetQuery("pnc.remove-supplier-orders")
		_, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-supplier-credit-notes")
		_, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.supplier-remove-all-products")
		result, err := txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-supplier")
		result, err = txt.Update(query, id)

		if err != nil {
			return data.CheckConstraintError(err)
		}

		records = uint(result)
		return nil
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Retrieve existing orders for a supplier
*/
func (dao *SupplierDao) GetOrders(idSupplier uint) ([]*model.Order, error) {
	var query = dao.GetQuery("pnc.get-supplier-orders")
	var os, err = dao.Template().Select(query, mapOrder, idSupplier)
	return castOrders(os), err
}

/*
 Retrieves orders made to a supplier filtered by their paid status
*/
func (dao *SupplierDao) GetOrdersByPaid(idSupplier uint, paid bool) ([]*model.Order, error) {
	var query = dao.GetQuery("pnc.get-supplier-orders-by-paid")
	var os, err = dao.Template().Select(query, mapOrder, idSupplier, data.ToNumber(paid))
	return castOrders(os), err
}

/*
 Add an order for the collection of a supplier
*/
func (dao *SupplierDao) AddOrder(order *model.Order) (uint, error) {
	var query = dao.GetQuery("pnc.add-order")
	var paidStr *string

	if order.PaidDate() != nil {
		var tmp = order.PaidDate().Format(data.DATE_TIME_FORMAT)
		paidStr = &tmp
	}

	var id, err = dao.Template().Insert(query,
		order.IdSupplier(),
		order.Timestamp().Format(data.DATE_TIME_FORMAT),
		order.Reference(),
		order.Amount(),
		data.ToNumber(order.Paid()),
		paidStr,
	)

	if err != nil {
		return 0, err
	}

	order.SetId(uint(id))
	return order.Id(), nil
}

/*
 Update the details of an existing order
*/
func (dao *SupplierDao) UpdateOrder(order *model.Order) (uint, error) {
	var query = dao.GetQuery("pnc.update-order")
	var paidStr *string

	if order.PaidDate() != nil {
		var tmp = order.PaidDate().Format(data.DATE_TIME_FORMAT)
		paidStr = &tmp
	}

	var records, err = dao.Template().Update(query,
		order.Reference(),
		order.Amount(),
		data.ToNumber(order.Paid()),
		paidStr,
		order.Id(),
	)

	return uint(records), err
}

/*
 Remove an order from the collection
*/
func (dao *SupplierDao) RemoveOrder(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.remove-order")
	var records, err = dao.Template().Update(query, id)
	return uint(records), err
}

/*
 Retrieve existing credit notes for a given supplier
*/
func (dao *SupplierDao) GetCreditNotes(idSupplier uint) ([]*model.CreditNote, error) {
	var query = dao.GetQuery("pnc.get-supplier-credit-notes")
	var cns, err = dao.Template().Select(query, mapCreditNote, idSupplier)
	return castCreditNotes(cns), err
}

/*
 Retrieve existing credit notes for a given supplier filtered by redeemed state
*/
func (dao *SupplierDao) GetCreditNotesByRedeemed(idSupplier uint, redeemed bool) ([]*model.CreditNote, error) {
	var query = dao.GetQuery("pnc.get-supplier-credit-notes-by-redeemed")
	var cns, err = dao.Template().Select(query, mapCreditNote, idSupplier, data.ToNumber(redeemed))
	return castCreditNotes(cns), err
}

/*
 Add a credit note to the collection of a supplier
*/
func (dao *SupplierDao) AddCreditNote(note *model.CreditNote) (uint, error) {
	var query = dao.GetQuery("pnc.add-credit-note")
	var redeemedStr *string

	if note.RedeemedDate() != nil {
		var tmp = note.RedeemedDate().Format(data.DATE_TIME_FORMAT)
		redeemedStr = &tmp
	}

	var id, err = dao.Template().Insert(query,
		note.IdSupplier(),
		note.Timestamp().Format(data.DATE_TIME_FORMAT),
		note.Reference(),
		note.Amount(),
		note.Reason(),
		data.ToNumber(note.Redeemed()),
		redeemedStr,
	)

	return uint(id), err
}

/*
 Updates the details of a credit note
*/
func (dao *SupplierDao) UpdateCreditNote(note *model.CreditNote) (uint, error) {
	var query = dao.GetQuery("pnc.update-credit-note")
	var redeemedStr *string

	if note.RedeemedDate() != nil {
		var tmp = note.RedeemedDate().Format(data.DATE_TIME_FORMAT)
		redeemedStr = &tmp
	}

	var records, err = dao.Template().Update(query,
		note.Reference(),
		note.Amount(),
		note.Reason(),
		data.ToNumber(note.Redeemed()),
		redeemedStr,
		note.Id(),
	)

	return uint(records), err
}

/*
 Removes an existing credit line
*/
func (dao *SupplierDao) RemoveCreditNote(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.remove-credit-note")
	var records, err = dao.Template().Update(query, id)
	return uint(records), err
}

/*
 Adds an association betwween a supplier and a product. Indicates that the product can be purchased from this supplier.
*/
func (dao *SupplierDao) AddProduct(idSupplier, idProduct uint) (uint, error) {
	var query = dao.GetQuery("pnc.supplier-add-product")
	var records, err = dao.Template().Update(query, idSupplier, idProduct)
	return uint(records), data.CheckConstraintError(err)
}

/*
 Removes an association between a product and supplier. Indicates the product can no longer be purchased from the supplier.
*/
func (dao *SupplierDao) RemoveProduct(idSupplier, idProduct uint) (uint, error) {
	var query = dao.GetQuery("pnc.supplier-remove-product")
	var records, err = dao.Template().Update(query, idSupplier, idProduct)
	return uint(records), err
}

/*
 Will sync the contact collection in the given supplier model, and persist it in the database.
*/
func (dao *SupplierDao) syncContacts(txt *data.TxTemplate, supplier *model.Supplier) error {
	var query = dao.GetQuery("pnc.get-supplier-contacts")
	var result, err = txt.Select(query, mapSupplierContact, supplier.Id())
	var existing []*model.Contact

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	if err != data.NOT_FOUND {
		var sc = castSupplierContacts(result)
		existing = make([]*model.Contact, len(sc))

		for cont, c := range sc {
			existing[cont] = c.contact
		}

		var unlinkQuery = dao.GetQuery("pnc.unlink-contact-from-supplier")
		var removeQuery = dao.GetQuery("pnc.remove-contact")

		for _, contact := range existing {
			if !supplier.HasContact(contact) {
				_, err = txt.Update(unlinkQuery, supplier.Id(), contact.Id())

				if err != nil {
					return err
				}

				_, err = txt.Update(removeQuery, contact.Id())

				if err != nil {
					return err
				}
			}
		}
	} else {
		existing = make([]*model.Contact, 0)
	}

	var addQuery = dao.GetQuery("pnc.add-contact")
	var updateQuery = dao.GetQuery("pnc.update-contact")
	var linkQuery = dao.GetQuery("pnc.link-contact-to-supplier")

	for _, contact := range supplier.Contacts() {
		if containsContact(existing, contact) {
			_, err = txt.Update(updateQuery,
				uint(contact.ContactType()),
				contact.Value(),
				contact.Id(),
			)
		} else {
			id, err := txt.Insert(addQuery,
				contact.Uuid(),
				uint(contact.ContactType()),
				contact.Value(),
			)

			if err != nil {
				return err
			}

			contact.SetId(uint(id))

			_, err = txt.Update(linkQuery,
				supplier.Id(),
				contact.Id(),
			)
		}

		if err != nil {
			return err
		}
	}

	return nil
}

/*
 Casta a collection of supplier objects into Supplier type
*/
func castSuppliers(result []interface{}) []*model.Supplier {
	if result == nil {
		return nil
	}

	var suppliers = make([]*model.Supplier, len(result))

	for cont, c := range result {
		suppliers[cont] = c.(*model.Supplier)
	}

	return suppliers
}

/*
 Casts a collection of order objects into Order type
*/
func castOrders(result []interface{}) []*model.Order {
	if result == nil {
		return nil
	}

	var orders = make([]*model.Order, len(result))

	for cont, order := range result {
		orders[cont] = order.(*model.Order)
	}

	return orders
}

/*
 Casts a collection of credit note objects into CreditNote type
*/
func castCreditNotes(result []interface{}) []*model.CreditNote {
	if result == nil {
		return nil
	}

	var notes = make([]*model.CreditNote, len(result))

	for cont, note := range result {
		notes[cont] = note.(*model.CreditNote)
	}

	return notes
}

/*
 Casta a collection of supplier contact objects into supplierContact type
*/
func castSupplierContacts(result []interface{}) []*supplierContact {
	if result == nil {
		return nil
	}

	var contacts = make([]*supplierContact, len(result))

	for cont, c := range result {
		contacts[cont] = c.(*supplierContact)
	}

	return contacts
}

/*
 Maps a contacts to their respective parent objects
*/
func matchSupplierContacts(suppliers []*model.Supplier, contacts []*supplierContact) {
	if len(suppliers) < 1 || len(contacts) < 1 {
		return
	}

	var supplierMap = make(map[uint]*model.Supplier, len(suppliers))

	for _, supplier := range suppliers {
		supplierMap[supplier.Id()] = supplier
	}

	for _, contact := range contacts {
		var supplier = supplierMap[contact.idSupplier]
		supplier.Add(contact.contact)
	}
}
