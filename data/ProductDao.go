package data

import (
	"database/sql"
	"errors"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"strings"
)

/*
 Database driven data access object implementation for the Product model
*/
type ProductDao struct {
	*data.BaseDbDao
}

/*
 Creates a new Product DAO
*/
func NewProductDao(template *data.SqlTemplate, dictionary map[string]string) (*ProductDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var pd = &ProductDao{base}
	return pd, nil
}

/*
 Maps a database row into a product model object
*/
func mapProduct(rows *sql.Rows) (interface{}, error) {
	var id, threshold, minimum uint
	var brand, name string
	var barCodePtr *uint64
	var barCode uint64
	var taxRate, price float32

	var err = rows.Scan(&id, &brand, &name, &barCodePtr, &taxRate, &price, &threshold, &minimum)

	if err != nil {
		return nil, err
	}

	if barCodePtr != nil {
		barCode = *barCodePtr
	}

	return model.NewProduct(id, brand, name, barCode, taxRate, price, threshold, minimum)
}

/*
 Maps a database row into a brand string
*/
func mapBrand(rows *sql.Rows) (interface{}, error) {
	var brand string
	var err = rows.Scan(&brand)

	if err != nil {
		return nil, err
	}

	return brand, nil
}

/*
 Retrieves a product identified by an ID
*/
func (dao *ProductDao) Get(id uint) (*model.Product, error) {
	var query = dao.GetQuery("pnc.get-product")
	var result, err = dao.Template().SelectFirst(query, mapProduct, id)

	if result == nil || err != nil {
		return nil, err
	}

	var product = result.(*model.Product)
	return product, nil
}

/*
 Retrieves all existing products
*/
func (dao *ProductDao) GetAll() ([]*model.Product, error) {
	var query = dao.GetQuery("pnc.get-products")
	var result, err = dao.Template().Select(query, mapProduct)

	return dao.cast(result), err
}

/*
 Retrieves all products which are sold by a given supplier
*/
func (dao *ProductDao) GetBySupplier(idSupplier uint) ([]*model.Product, error) {
	var query = dao.GetQuery("pnc.get-products-by-supplier")
	var result, err = dao.Template().Select(query, mapProduct, idSupplier)

	return dao.cast(result), err
}

/*
 Retrieves al products which were sold in a given invoice
*/
func (dao *ProductDao) GetByInvoice(idInvoice uint) ([]*model.Product, error) {
	var query = dao.GetQuery("pnc.get-products-by-invoice")
	var result, err = dao.Template().Select(query, mapProduct, idInvoice)

	return dao.cast(result), err
}

/*
 Checks to see which products have expired inventory. Generates notifications for all the ones it finds
*/
func (dao *ProductDao) NotifyExpired() error {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-products-expired")
		result, err := txt.Select(query, mapProduct)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, r := range result {
			product := r.(*model.Product)
			Notify(dao.BaseDbDao, txt, product.Id(), model.EXPIRED_INVENTORY)
		}

		return nil
	}

	return txt.Execute(tx)
}

/*
 Retrieves all products who's name match a given name criteria
*/
func (dao *ProductDao) SearchBySupplier(nameCriteria, brandCriteria string, idSupplier uint) ([]*model.Product, error) {
	var query = []string{dao.GetQuery("pnc.get-products-by-supplier")}

	if len(nameCriteria) > 0 {
		query = append(query, "AND name LIKE '%"+nameCriteria+"%' ")
	}

	if len(brandCriteria) > 0 {
		query = append(query, "AND brand LIKE '%"+brandCriteria+"%' ")
	}

	var result, err = dao.Template().Select(strings.Join(query, ""), mapProduct, idSupplier)

	return dao.cast(result), err
}

/*
 Retrieves all products who's brand nad name match the given criteria
*/
func (dao *ProductDao) Search(nameCriteria, brandCriteria string) ([]*model.Product, error) {
	var query = []string{dao.GetQuery("pnc.get-products"), " WHERE 1 = 1 "}

	if len(nameCriteria) > 0 {
		query = append(query, "AND name LIKE '%"+nameCriteria+"%' ")
	}

	if len(brandCriteria) > 0 {
		query = append(query, "AND brand LIKE '%"+brandCriteria+"%' ")
	}

	var result, err = dao.Template().Select(strings.Join(query, ""), mapProduct)
	return dao.cast(result), err
}

/*
 Retrieves all brand names that match the given criteria
*/
func (dao *ProductDao) SearchBrands(criteria string) ([]string, error) {
	var query = dao.GetQuery("pnc.search-brands")
	var result, err = dao.Template().Select(query, mapBrand, "%"+criteria+"%")

	if err != nil {
		return nil, err
	}

	var strings = make([]string, len(result))
	var ok bool

	for cont, val := range result {
		strings[cont], ok = val.(string)

		if !ok {
			return nil, errors.New("Unexpected type")
		}
	}

	return strings, nil
}

/*
 Adds a product to the collection
*/
func (dao *ProductDao) Add(product *model.Product) (uint, error) {
	var query = dao.GetQuery("pnc.insert-product")
	var id, err = dao.Template().Insert(query,
		product.Brand(),
		product.Name(),
		product.BarCode(),
		product.TaxRate(),
		product.Price(),
		product.Threshold(),
		product.Minimum())

	return uint(id), data.CheckConstraintError(err)
}

/*
 Updates the properties of a product
*/
func (dao *ProductDao) Update(product *model.Product) (uint, error) {
	var query = dao.GetQuery("pnc.update-product")
	var records, err = dao.Template().Update(query,
		product.Brand(),
		product.Name(),
		product.BarCode(),
		product.TaxRate(),
		product.Price(),
		product.Threshold(),
		product.Minimum(),
		product.Id())

	return uint(records), data.CheckConstraintError(err)
}

/*
 Removes a product from the collection
*/
func (dao *ProductDao) Remove(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.remove-product")
	records, err := dao.Template().Update(query, id)
	return uint(records), data.CheckConstraintError(err)
}

/*
 Casts a collection of product objects with interface{} type into product model type
*/
func (dao *ProductDao) cast(result []interface{}) []*model.Product {
	if result == nil {
		return nil
	}

	var products = make([]*model.Product, len(result))

	for cont, r := range result {
		products[cont] = r.(*model.Product)
	}

	return products
}
