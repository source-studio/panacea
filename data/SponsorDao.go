package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Databse drive data access object implementation for the Sponsor model
*/
type SponsorDao struct {
	*data.BaseDbDao
}

/*
 Utility struct used to map addresses to sponsors
*/
type sponsorAddress struct {
	idSponsor uint           // ID of the sponsor that holds the address
	address   *model.Address // Address data
}

/*
 Utility struct used to map contacts to sponsors
*/
type sponsorContact struct {
	idSponsor uint           // ID of the sponsor that holds the contact
	contact   *model.Contact // Contact data
}

/*
 Creates a new sponsor dao
*/
func NewSponsorDao(template *data.SqlTemplate, dictionary map[string]string) (*SponsorDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &SponsorDao{base}, nil
}

/*
 Maps a sponsor model object from database rows
*/
func mapSponsor(rows *sql.Rows) (interface{}, error) {
	var id uint
	var firstName, lastName, organization string

	var err = rows.Scan(&id, &organization, &firstName, &lastName)

	if err != nil {
		return nil, err
	}

	return model.NewSponsor(id, organization, firstName, lastName)
}

/*
 Maps a sponsor address object from a database row
*/
func mapSponsorAddress(rows *sql.Rows) (interface{}, error) {
	var idAddress, idSponsor uint
	var street1, city, province, country string
	var street2 []byte

	var err = rows.Scan(&idSponsor, &idAddress, &street1, &street2, &city, &province, &country)

	if err != nil {
		return nil, err
	}

	address, err := model.NewAddress(idAddress, street1, string(street2), city, province, country)

	if err != nil {
		return nil, err
	}

	var sa = sponsorAddress{idSponsor, address}
	return &sa, nil
}

/*
 Maps a sponsor contact object from a database row
*/
func mapSponsorContact(rows *sql.Rows) (interface{}, error) {
	var idSponsor, idContact, typeUi uint
	var uuid, value string

	var err = rows.Scan(&idSponsor, &idContact, &uuid, &typeUi, &value)

	if err != nil {
		return nil, err
	}

	contactType, err := model.ToContactType(typeUi)

	if err != nil {
		return nil, err
	}

	contact, err := model.NewContact(idContact, uuid, contactType, value)

	if err != nil {
		return nil, err
	}

	var sc = sponsorContact{idSponsor, contact}
	return &sc, nil
}

/*
 Maps a sponsorship from a database row
*/
func mapSponsorship(rows *sql.Rows) (interface{}, error) {
	var id, idSponsor, idInvoice, paid uint
	var timestampStr string
	var total, sponsored float32
	var paidStr []byte
	var paidDate *time.Time

	var err = rows.Scan(&id, &idSponsor, &idInvoice, &timestampStr, &total, &sponsored, &paid, &paidStr)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	if len(paidStr) > 0 {
		tmp, err := time.Parse(data.DATE_TIME_FORMAT, string(paidStr))

		if err != nil {
			return nil, err
		}

		paidDate = &tmp
	}

	return model.NewSponsorship(id, idSponsor, idInvoice, timestamp, total, sponsored, paid > 0, paidDate)
}

/*
 Retrieves a sponsor identified by an ID
*/
func (dao *SponsorDao) Get(id uint) (*model.Sponsor, error) {
	var sponsor *model.Sponsor
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-sponsor")
		s, err := txt.SelectFirst(query, mapSponsor, id)

		if s == nil || err != nil {
			return err
		}

		sponsor = s.(*model.Sponsor)
		query = dao.GetQuery("pnc.get-sponsor-address")
		sar, err := txt.SelectFirst(query, mapSponsorAddress, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if sar != nil {
			var sa = sar.(*sponsorAddress)
			sponsor.SetAddress(sa.address)
		}

		query = dao.GetQuery("pnc.get-sponsor-contacts")
		scr, err := txt.Select(query, mapSponsorContact, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, c := range scr {
			var sc = c.(*sponsorContact)
			sponsor.Add(sc.contact)
		}

		return nil
	}

	err = txt.Execute(tx)
	return sponsor, err
}

/*
 Retreives all sponsors available in the collection
*/
func (dao *SponsorDao) GetAll() ([]*model.Sponsor, error) {
	var sponsors []*model.Sponsor
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-sponsors")
		result, err := txt.Select(query, mapSponsor)

		if err != nil {
			return err
		}

		sponsors = castSponsors(result)
		query = dao.GetQuery("pnc.get-all-sponsor-addresses")
		result, err = txt.Select(query, mapSponsorAddress)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var addresses = castSponsorAddresses(result)
		matchSponsorAddresses(sponsors, addresses)
		query = dao.GetQuery("pnc.get-all-sponsor-contacts")
		result, err = txt.Select(query, mapSponsorContact)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castSponsorContacts(result)
		matchSponsorContacts(sponsors, contacts)
		return nil
	}

	err = txt.Execute(tx)
	return sponsors, err
}

/*
 Searches for sponsors based on a given criteria
*/
func (dao *SponsorDao) Search(criteria string) ([]*model.Sponsor, error) {
	var sponsors []*model.Sponsor
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.search-sponsors")
		var criteriaVal = "%" + criteria + "%"
		result, err := txt.Select(query, mapSponsor, criteriaVal, criteriaVal)

		if err != nil {
			return err
		}

		sponsors = castSponsors(result)
		query = dao.GetQuery("pnc.search-sponsor-addresses")
		result, err = txt.Select(query, mapSponsorAddress, criteriaVal, criteriaVal)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var addresses = castSponsorAddresses(result)
		matchSponsorAddresses(sponsors, addresses)
		query = dao.GetQuery("pnc.search-sponsor-contacts")
		result, err = txt.Select(query, mapSponsorContact, criteriaVal, criteriaVal)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castSponsorContacts(result)
		matchSponsorContacts(sponsors, contacts)
		return nil
	}

	err = txt.Execute(tx)
	return sponsors, err
}

/*
 Adds a new sponsor to the collection
*/
func (dao *SponsorDao) Add(sponsor *model.Sponsor) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-sponsor")
		id, err := txt.Insert(query,
			sponsor.Organization(),
			sponsor.FirstName(),
			sponsor.LastName(),
		)

		if err != nil {
			return err
		}

		sponsor.SetId(uint(id))

		if sponsor.Address() != nil {
			query = dao.GetQuery("pnc.add-address")
			idAddress, err := txt.Insert(query,
				sponsor.Address().Street1(),
				sponsor.Address().Street2(),
				sponsor.Address().City(),
				sponsor.Address().Province(),
				sponsor.Address().Country(),
			)

			if err != nil {
				return err
			}

			sponsor.Address().SetId(uint(idAddress))
			query = dao.GetQuery("pnc.link-address-to-sponsor")
			_, err = txt.Update(query, sponsor.Id(), sponsor.Address().Id())

			if err != nil {
				return err
			}
		}

		return dao.syncContacts(txt, sponsor)
	}

	err = txt.Execute(tx)
	return sponsor.Id(), err
}

/*
 Updates the details for a sponsor
*/
func (dao *SponsorDao) Update(sponsor *model.Sponsor) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.update-sponsor")
		result, err := txt.Update(query,
			sponsor.Organization(),
			sponsor.FirstName(),
			sponsor.LastName(),
			sponsor.Id(),
		)

		if err != nil {
			return err
		}

		records = uint(result)
		err = dao.syncAddress(txt, sponsor)

		if err != nil {
			return err
		}

		return dao.syncContacts(txt, sponsor)
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Removes a sponsor from the collection
*/
func (dao *SponsorDao) Remove(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-sponsor-address")
		sar, err := txt.SelectFirst(query, mapSponsorAddress, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		query = dao.GetQuery("pnc.get-sponsor-contacts")
		scr, err := txt.Select(query, mapSponsorContact, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var contacts = castSponsorContacts(scr)

		if contacts != nil && len(contacts) > 0 {
			query = dao.GetQuery("pnc.unlink-all-contacts-from-sponsor")
			_, err = txt.Update(query, id)

			if err != nil {
				return err
			}

			query = dao.GetQuery("pnc.remove-contact")

			for _, contact := range contacts {
				_, err = txt.Update(query, contact.contact.Id())

				if err != nil {
					return err
				}
			}
		}

		if sar != nil {
			query = dao.GetQuery("pnc.unlink-address-from-sponsor")
			_, err := txt.Update(query, id)

			if err != nil {
				return err
			}

			var address = sar.(*sponsorAddress)
			query = dao.GetQuery("pnc.remove-address")
			_, err = txt.Update(query, address.address.Id())

			if err != nil {
				return err
			}
		}

		query = dao.GetQuery("pnc.remove-sponsorships-by-sponsor")
		_, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-sponsor")
		result, err := txt.Update(query, id)

		if err != nil {
			return data.CheckConstraintError(err)
		}

		records = uint(result)

		return nil
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Retrieve a sponsorhip identified by the given ID
*/
func (dao *SponsorDao) GetSponsorship(id uint) (*model.Sponsorship, error) {
	var query = dao.GetQuery("pnc.get-sponsorship")
	var result, err = dao.Template().SelectFirst(query, mapSponsorship, id)

	if result == nil || err != nil {
		return nil, err
	}

	return result.(*model.Sponsorship), nil
}

/*
 Retrieve a collection of sponsorships that were made by the sponsor provided
*/
func (dao *SponsorDao) GetSponsorships(idSponsor uint) ([]*model.Sponsorship, error) {
	var query = dao.GetQuery("pnc.get-sponsorships-by-sponsor")
	var result, err = dao.Template().Select(query, mapSponsorship, idSponsor)

	return castSponsorships(result), err
}

/*
 Returns all sponsorships made bya sponsor filtered by their paid state
*/
func (dao *SponsorDao) GetSponsorshipsByPaid(idSponsor uint, paid bool) ([]*model.Sponsorship, error) {
	var query = dao.GetQuery("pnc.get-sponsorships-by-sponsor-paid")
	var result, err = dao.Template().Select(query, mapSponsorship, idSponsor, data.ToNumber(paid))

	return castSponsorships(result), err
}

/*
 Creates a new sponsorship
*/
func (dao *SponsorDao) AddSponsorShip(sponsorship *model.Sponsorship) (uint, error) {
	var paidStr *string

	if sponsorship.PaidDate() != nil {
		var tmp = sponsorship.PaidDate().Format(data.DATE_TIME_FORMAT)
		paidStr = &tmp
	}

	var query = dao.GetQuery("pnc.insert-sponsorship")
	var result, err = dao.Template().Insert(query,
		sponsorship.IdSponsor(),
		sponsorship.IdInvoice(),
		sponsorship.Timestamp().Format(data.DATE_TIME_FORMAT),
		sponsorship.Total(),
		sponsorship.Sponsored(),
		data.ToNumber(sponsorship.Paid()),
		paidStr,
	)

	if err != nil {
		return 0, err
	}

	sponsorship.SetId(uint(result))
	return sponsorship.Id(), nil
}

/*
 Updates the detials on an existing sponsorship. To avoid creating discrepancies in balances, the only properties which
 can be changed are the paid and paid date.
*/
func (dao *SponsorDao) UpdateSponsorship(sponsorship *model.Sponsorship) (uint, error) {
	var paidStr *string

	if sponsorship.PaidDate() != nil {
		var tmp = sponsorship.PaidDate().Format(data.DATE_TIME_FORMAT)
		paidStr = &tmp
	}

	var query = dao.GetQuery("pnc.update-sponsorship")
	var result, err = dao.Template().Update(query,
		data.ToNumber(sponsorship.Paid()),
		paidStr,
		sponsorship.Id(),
	)

	if err != nil {
		return 0, err
	}

	return uint(result), err
}

/*
 Syncs the address details for a sponsor. Will take the state in the model and persist that state in the database.
*/
func (dao *SponsorDao) syncAddress(txt *data.TxTemplate, sponsor *model.Sponsor) error {
	var address = sponsor.Address()
	var query string
	var err error
	var id int64

	if address == nil {
		query = dao.GetQuery("pnc.get-sponsor-address")
		var sar, err = txt.SelectFirst(query, mapSponsorAddress, sponsor.Id())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if sar != nil {
			query = dao.GetQuery("pnc.unlink-address-from-sponsor")
			_, err = txt.Update(query, sponsor.Id())

			if err != nil {
				return err
			}

			var existing = sar.(*model.Address)
			query = dao.GetQuery("pnc.remove-address")
			_, err = txt.Update(query, existing.Id())

			if err != nil {
				return err
			}
		}

		return nil
	}

	if address.Id() < 1 {
		query = dao.GetQuery("pnc.add-address")
		id, err = txt.Insert(query,
			address.Street1(),
			address.Street2(),
			address.City(),
			address.Province(),
			address.Country(),
		)

		if err != nil {
			return nil
		}

		address.SetId(uint(id))
		query = dao.GetQuery("pnc.link-address-to-sponsor")
		_, err = txt.Update(query, sponsor.Id(), address.Id())
	} else {
		query = dao.GetQuery("pnc.update-address")
		_, err = txt.Update(query,
			address.Street1(),
			address.Street2(),
			address.City(),
			address.Province(),
			address.Country(),
			address.Id(),
		)
	}

	return err
}

/*
 Syncs the contact details for a sponsor. Will takethe state in the model and persist that state in the database.
*/
func (dao *SponsorDao) syncContacts(txt *data.TxTemplate, sponsor *model.Sponsor) error {
	var query = dao.GetQuery("pnc.get-sponsor-contacts")
	var result, err = txt.Select(query, mapSponsorContact, sponsor.Id())

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	var sc = castSponsorContacts(result)
	var existing = make([]*model.Contact, len(sc))

	for cont, c := range sc {
		existing[cont] = c.contact
	}

	var unlinkQuery = dao.GetQuery("pnc.unlink-contact-from-sponsor")
	var removeQuery = dao.GetQuery("pnc.remove-contact")

	for _, contact := range existing {
		if !sponsor.HasContact(contact) {
			_, err = txt.Update(unlinkQuery, sponsor.Id(), contact.Id())

			if err != nil {
				return err
			}

			_, err = txt.Update(removeQuery, contact.Id())

			if err != nil {
				return err
			}
		}
	}

	var addQuery = dao.GetQuery("pnc.add-contact")
	var updateQuery = dao.GetQuery("pnc.update-contact")
	var linkQuery = dao.GetQuery("pnc.link-contact-to-sponsor")

	for _, contact := range sponsor.Contacts() {
		if containsContact(existing, contact) {
			_, err = txt.Update(updateQuery,
				uint(contact.ContactType()),
				contact.Value(),
				contact.Id(),
			)

			if err != nil {
				return err
			}
		} else {
			id, err := txt.Insert(addQuery,
				contact.Uuid(),
				uint(contact.ContactType()),
				contact.Value(),
			)

			if err != nil {
				return err
			}

			contact.SetId(uint(id))
			_, err = txt.Insert(linkQuery,
				sponsor.Id(),
				contact.Id(),
			)

			if err != nil {
				return err
			}
		}
	}

	return nil
}

/*
 Casts a collection of sponsors into Sponsor types
*/
func castSponsors(result []interface{}) []*model.Sponsor {
	if result == nil {
		return nil
	}

	var sponsors = make([]*model.Sponsor, len(result))

	for cont, c := range result {
		sponsors[cont] = c.(*model.Sponsor)
	}

	return sponsors
}

/*
 Casts a collection of sponsor address objects into sponsorAddress types
*/
func castSponsorAddresses(result []interface{}) []*sponsorAddress {
	if result == nil {
		return nil
	}

	var addresses = make([]*sponsorAddress, len(result))

	for cont, a := range result {
		addresses[cont] = a.(*sponsorAddress)
	}

	return addresses
}

/*
 Casts a collectoin of sponsor contact objects into sponsorContact types
*/
func castSponsorContacts(result []interface{}) []*sponsorContact {
	if result == nil {
		return nil
	}

	var contacts = make([]*sponsorContact, len(result))

	for cont, c := range result {
		contacts[cont] = c.(*sponsorContact)
	}

	return contacts
}

/*
 Casts a collection of sponsorships into sponsorhip types
*/
func castSponsorships(result []interface{}) []*model.Sponsorship {
	if result == nil {
		return nil
	}

	var sponsorships = make([]*model.Sponsorship, len(result))

	for cont, c := range result {
		sponsorships[cont] = c.(*model.Sponsorship)
	}

	return sponsorships
}

/*
 Maps a collection of addresses to their respective parent sponsor objects
*/
func matchSponsorAddresses(sponsors []*model.Sponsor, addresses []*sponsorAddress) {
	var sponsorMap = make(map[uint]*model.Sponsor, len(sponsors))

	for _, sponsor := range sponsors {
		sponsorMap[sponsor.Id()] = sponsor
	}

	for _, address := range addresses {
		var sponsor = sponsorMap[address.idSponsor]
		sponsor.SetAddress(address.address)
	}
}

/*
 Maps a collection of contacts to their respective parent sponsor objects
*/
func matchSponsorContacts(sponsors []*model.Sponsor, contacts []*sponsorContact) {
	var sponsorMap = make(map[uint]*model.Sponsor, len(sponsors))

	for _, sponsor := range sponsors {
		sponsorMap[sponsor.Id()] = sponsor
	}

	for _, contact := range contacts {
		var sponsor = sponsorMap[contact.idSponsor]
		sponsor.Add(contact.contact)
	}
}
