package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Data access object implementation for the Deposit model
*/
type DepositDao struct {
	*data.BaseDbDao
}

/*
 Creates a new deposit data access object
*/
func NewDepositDao(template *data.SqlTemplate, dictionary map[string]string) (*DepositDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &DepositDao{base}, nil
}

/*
 Maps a deposit model object from row data
*/
func mapDeposit(rows *sql.Rows) (interface{}, error) {
	var id, idUser, include uint
	var timestampStr string
	var amount float32
	var reason []byte

	var err = rows.Scan(&id, &idUser, &timestampStr, &reason, &amount, &include)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewDeposit(id, idUser, timestamp, string(reason), amount, include > 0)
}

/*
 Retrieves a deposit identified by the given ID
*/
func (dao *DepositDao) Get(id uint) (*model.Deposit, error) {
	var query = dao.GetQuery("pnc.get-deposit")
	var result, err = dao.Template().SelectFirst(query, mapDeposit, id)

	if result == nil || err != nil {
		return nil, err
	}

	return result.(*model.Deposit), nil
}

/*
 Retrieves all deposits available in the collection
*/
func (dao *DepositDao) GetAll() ([]*model.Deposit, error) {
	var query = dao.GetQuery("pnc.get-deposits")
	var result, err = dao.Template().Select(query, mapDeposit)

	return castDeposits(result), err
}

/*
 Retrieves deposits which were created between the provided times
*/
func (dao *DepositDao) Search(from, to time.Time, user uint) ([]*model.Deposit, error) {
	var query string
	var result []interface{}
	var err error

	if user < 1 {
		query = dao.GetQuery("pnc.get-deposits-by-time-range")
		result, err = dao.Template().Select(query, mapDeposit,
			from.Format(data.DATE_TIME_FORMAT),
			to.Format(data.DATE_TIME_FORMAT),
		)
	} else {
		query = dao.GetQuery("pnc.get-deposits-by-time-range-and-user")
		result, err = dao.Template().Select(query, mapDeposit,
			from.Format(data.DATE_TIME_FORMAT),
			to.Format(data.DATE_TIME_FORMAT),
			user,
		)
	}

	return castDeposits(result), err
}

/*
 Create a new deposit
*/
func (dao *DepositDao) Add(deposit *model.Deposit) (uint, error) {
	var query = dao.GetQuery("pnc.add-deposit")
	var result, err = dao.Template().Insert(query,
		deposit.IdUser(),
		deposit.Timestamp().Format(data.DATE_TIME_FORMAT),
		deposit.Reason(),
		deposit.Amount(),
		data.ToNumber(deposit.IncludeInBalance()),
	)

	if err != nil {
		return 0, err
	}

	deposit.SetId(uint(result))
	return deposit.Id(), nil
}

/*
 Update the details of an existing deposit. Only the reason and maount will be updated.
*/
func (dao *DepositDao) Update(deposit *model.Deposit) (uint, error) {
	var query = dao.GetQuery("pnc.update-deposit")
	var result, err = dao.Template().Update(query,
		deposit.Reason(),
		deposit.Amount(),
		data.ToNumber(deposit.IncludeInBalance()),
		deposit.Id(),
	)

	return uint(result), err
}

/*
 Removes a deposit identified by the given ID
*/
func (dao *DepositDao) Remove(id uint) (uint, error) {
	var query = dao.GetQuery("pnc.remove-deposit")
	var result, err = dao.Template().Update(query, id)

	return uint(result), err
}

/*
 Casts a group of deposit objects into deposit type.
*/
func castDeposits(result []interface{}) []*model.Deposit {
	if result == nil {
		return nil
	}

	var deposits = make([]*model.Deposit, len(result))

	for cont, d := range result {
		deposits[cont] = d.(*model.Deposit)
	}

	return deposits
}
