package data

import (
	"database/sql"
	"errors"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Data access object implementation for the Report model. Back end interface that pulls the data required to generate
 reports on the front end. Like other DAOs, the report data retrieved is fairly bare-bones. It will be the client's
 responsability to perform formatting and may be some totalling or agregation.
*/
type ReportDao struct {
	*data.BaseDbDao
}

/*
 Creates a new report Data Access Object. Requires the template and sql dictionary
*/
func NewReportDao(template *data.SqlTemplate, dictionary map[string]string) (*ReportDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var dao = &ReportDao{base}
	return dao, nil
}

/*
 Maps a row that contains a float amount
*/
func mapAmount(rows *sql.Rows) (interface{}, error) {
	var amount *float32
	var err = rows.Scan(&amount)
	var value float32 = 0

	if amount != nil {
		value = *amount
	}

	return value, err
}

/*
 Maps a row that contains an interval of date values
*/
func mapInterval(rows *sql.Rows) (interface{}, error) {
	var firstStr, lastStr string
	var interval = make([]time.Time, 2)
	var err = rows.Scan(&firstStr, &lastStr)

	if err != nil {
		return nil, err
	}

	interval[0], err = time.Parse(data.DATE_TIME_FORMAT, firstStr)

	if err != nil {
		return nil, err
	}

	interval[1], err = time.Parse(data.DATE_TIME_FORMAT, lastStr)

	return interval, err
}

/*
 Maps a row that contains a single date value
*/
func mapDateValue(rows *sql.Rows) (interface{}, error) {
	var dateStr string
	var total float32
	var err = rows.Scan(&dateStr, &total)

	if err != nil {
		return nil, err
	}

	return []interface{}{dateStr, total}, nil
}

/*
 Maps an inventory entry. An inventory entry consists of a product full name, threshold, inventory and price data
*/
func mapInventoryEntry(rows *sql.Rows) (interface{}, error) {
	var name string
	var threshold uint
	var remaining, cost, price float32
	var err = rows.Scan(&name, &threshold, &remaining, &cost, &price)

	if err != nil {
		return nil, err
	}

	return []interface{}{name, threshold, remaining, cost, price}, nil
}

/*
 Maps an expiration entry. An expiration entry consists of a product full name, remaining inventory and dates when the
 inventory was entered and when it expires.
*/
func mapExpirationEntry(rows *sql.Rows) (interface{}, error) {
	var name string
	var remaining uint
	var entered, expiration []byte
	var err = rows.Scan(&name, &remaining, &entered, &expiration)

	if err != nil {
		return nil, err
	}

	return []interface{}{name, remaining, string(entered), string(expiration)}, nil
}

/*
 Generates the data set for the state report. Provides information on the current state value recorded by the application.
*/
func (dao *ReportDao) GenerateState() (*model.Report, error) {
	var report = model.NewEmptyReport()
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.state-inventory-value")
		report.Data()["inventory"], err = txt.SelectFirst(query, mapAmount)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.state-total-credit-owed")
		owed, err := txt.SelectFirst(query, mapAmount)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.state-total-credit-payments")
		paid, err := txt.SelectFirst(query, mapAmount)

		if err != nil {
			return err
		}

		owedf, ok := owed.(float32)

		if !ok {
			return errors.New("Owed value is not a float")
		}

		paidf, ok := paid.(float32)

		if !ok {
			return errors.New("Paid value is not a float")
		}

		report.Data()["credit"] = owedf - paidf
		query = dao.GetQuery("pnc.state-unpaid-orders")
		report.Data()["orders"], err = txt.SelectFirst(query, mapAmount)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.state-unredeemed-notes")
		report.Data()["notes"], err = txt.SelectFirst(query, mapAmount)

		return err
	}
	err = txt.Execute(tx)
	return report, err
}

/*
 Generates the data set for the activity report. Provides a summary of the activity that happened within a date interval.
*/
func (dao *ReportDao) GenerateActivity(from, to time.Time) (*model.Report, error) {
	var report = model.NewEmptyReport()
	var fromStr = from.Format(data.DATE_TIME_FORMAT)
	var toStr = to.Format(data.DATE_TIME_FORMAT)
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-invoices-by-date-range-and-state")
		irs, err := txt.Select(query, mapInvoice, fromStr, toStr, uint(model.OK))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var cashTotal, creditTotal, refundTotal, paymentTotal, expenseTotal, depositTotal float32
		for _, ir := range irs {
			var invoice = ir.(*model.Invoice)

			switch invoice.Payment() {
			case model.CASH:
				cashTotal += invoice.Total() - invoice.Discount()
			case model.CREDIT:
				creditTotal += invoice.Total() - invoice.Discount()
			}
		}

		query = dao.GetQuery("pnc.get-refunds-by-date-range")
		rrs, err := txt.Select(query, mapRefund, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, pr := range rrs {
			var refund = pr.(*model.Refund)
			refundTotal += refund.Actual()
		}

		query = dao.GetQuery("pnc.get-payments-by-time-range")
		prs, err := txt.Select(query, mapPayment, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, pr := range prs {
			var payment = pr.(*model.Payment)
			paymentTotal += payment.Amount()
		}

		query = dao.GetQuery("pnc.get-expenses-by-time-range-and-inclusion")
		ers, err := txt.Select(query, mapExpense, fromStr, toStr, data.ToNumber(true))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, er := range ers {
			var expense = er.(*model.Expense)
			expenseTotal += expense.Amount()
		}

		query = dao.GetQuery("pnc.get-deposits-by-time-range-and-inclusion")
		drs, err := txt.Select(query, mapDeposit, fromStr, toStr, data.ToNumber(true))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, dr := range drs {
			var deposit = dr.(*model.Deposit)
			depositTotal += deposit.Amount()
		}

		report.Data()["cash"] = cashTotal
		report.Data()["credit"] = creditTotal
		report.Data()["refunds"] = refundTotal
		report.Data()["payments"] = paymentTotal
		report.Data()["expenses"] = expenseTotal
		report.Data()["deposits"] = depositTotal

		return nil
	}
	err = txt.Execute(tx)
	return report, err
}

/*
 Generates the montly activity report. Provides a summary of the acitivy that happened within a month, grouped by day of
 month.
*/
func (dao *ReportDao) GenerateActivityMonth(from, to time.Time) (*model.Report, error) {
	var report = model.NewEmptyReport()
	var fromStr = from.Format(data.DATE_TIME_FORMAT)
	var toStr = to.Format(data.DATE_TIME_FORMAT)
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var trans = func(result []interface{}, dict map[string]float32) {
		for _, row := range result {
			val := row.([]interface{})
			key := val[0].(string)
			value := val[1].(float32)

			dict[key] = value
		}
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.activity-invoices-by-date")
		iid, err := txt.Select(query, mapDateValue, fromStr, toStr, uint(model.OK), uint(model.CASH))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var cashmap = make(map[string]float32)
		report.Data()["cash"] = cashmap
		trans(iid, cashmap)

		query = dao.GetQuery("pnc.activity-payments-by-date")
		ipd, err := txt.Select(query, mapDateValue, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var paymentMap = make(map[string]float32)
		report.Data()["payments"] = paymentMap
		trans(ipd, paymentMap)

		query = dao.GetQuery("pnc.activity-deposits-by-date")
		idd, err := txt.Select(query, mapDateValue, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var depositMap = make(map[string]float32)
		report.Data()["deposit"] = depositMap
		trans(idd, depositMap)

		query = dao.GetQuery("pnc.activity-invoices-by-date")
		iid, err = txt.Select(query, mapDateValue, fromStr, toStr, uint(model.OK), uint(model.CREDIT))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var creditMap = make(map[string]float32)
		report.Data()["credit"] = creditMap
		trans(iid, creditMap)

		query = dao.GetQuery("pnc.activity-expenses-by-date")
		ied, err := txt.Select(query, mapDateValue, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var expenseMap = make(map[string]float32)
		report.Data()["expenses"] = expenseMap
		trans(ied, expenseMap)

		return nil
	}
	err = txt.Execute(tx)
	return report, err
}

/*
 Generates the data from the yearly activity report. Provides a summary of the activity that happend within a year,
 grouped by month.
*/
func (dao *ReportDao) GenerateActivityYear(from, to time.Time) (*model.Report, error) {
	var report = model.NewEmptyReport()
	var fromStr = from.Format(data.DATE_TIME_FORMAT)
	var toStr = to.Format(data.DATE_TIME_FORMAT)
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var trans = func(result []interface{}, dict map[string]float32) {
		for _, row := range result {
			val := row.([]interface{})
			key := val[0].(string)
			value := val[1].(float32)

			dict[key] = value
		}
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.activity-invoices-by-month")
		iid, err := txt.Select(query, mapDateValue, fromStr, toStr, uint(model.OK), uint(model.CASH))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var cashmap = make(map[string]float32)
		report.Data()["cash"] = cashmap
		trans(iid, cashmap)

		query = dao.GetQuery("pnc.activity-payments-by-month")
		ipd, err := txt.Select(query, mapDateValue, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var paymentMap = make(map[string]float32)
		report.Data()["payments"] = paymentMap
		trans(ipd, paymentMap)

		query = dao.GetQuery("pnc.activity-deposits-by-month")
		idd, err := txt.Select(query, mapDateValue, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var depositMap = make(map[string]float32)
		report.Data()["deposit"] = depositMap
		trans(idd, depositMap)

		query = dao.GetQuery("pnc.activity-invoices-by-month")
		iid, err = txt.Select(query, mapDateValue, fromStr, toStr, uint(model.OK), uint(model.CREDIT))

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var creditMap = make(map[string]float32)
		report.Data()["credit"] = creditMap
		trans(iid, creditMap)

		query = dao.GetQuery("pnc.activity-expenses-by-month")
		ied, err := txt.Select(query, mapDateValue, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var expenseMap = make(map[string]float32)
		report.Data()["expenses"] = expenseMap
		trans(ied, expenseMap)

		return nil
	}
	err = txt.Execute(tx)
	return report, err
}

/*
 Retrieves the date interval in which activity has been recorded by the application
*/
func (dao *ReportDao) GetInterval() (*model.Report, error) {
	var query = dao.GetQuery("pnc.activity-invoice-interval")
	var result, err = dao.Template().SelectFirst(query, mapInterval)

	if result == nil || err != nil {
		return nil, err
	}

	var interval = result.([]time.Time)
	var report = model.NewEmptyReport()
	report.Data()["first"] = interval[0].Format(data.DATE_TIME_FORMAT)
	report.Data()["last"] = interval[1].Format(data.DATE_TIME_FORMAT)

	return report, nil
}

/*
 Generates the data for the general inventory report. Provides a full listing of all products with existing inventory.
*/
func (dao *ReportDao) GenerateInventory() (*model.Report, error) {
	var report = model.NewEmptyReport()
	var query = dao.GetQuery("pnc.inventory-inventory")
	var ii, err = dao.Template().Select(query, mapInventoryEntry)

	if ii == nil || (err != nil && err != data.NOT_FOUND) {
		return nil, err
	}

	for _, i := range ii {
		var inventoryMap = make(map[string]interface{})
		var inventory = i.([]interface{})
		var key = inventory[0].(string)
		report.Data()[key] = inventoryMap
		inventoryMap["threshold"] = inventory[1]
		inventoryMap["remaining"] = inventory[2]
		inventoryMap["cost"] = inventory[3]
		inventoryMap["price"] = inventory[4]
	}

	return report, nil
}

/*
 Generates the data for the supplier inventory report. Provides a listing of all the products which are provided by a
 supplier and their inventory details
*/
func (dao *ReportDao) GenerateInventorySupplier(idSupplier uint) (*model.Report, error) {
	var report = model.NewEmptyReport()
	var query = dao.GetQuery("pnc.inventory-supplier")
	var ii, err = dao.Template().Select(query, mapInventoryEntry, idSupplier)

	if ii == nil || (err != nil && err != data.NOT_FOUND) {
		return nil, err
	}

	for _, i := range ii {
		var inventoryMap = make(map[string]interface{})
		var inventory = i.([]interface{})
		var key = inventory[0].(string)
		report.Data()[key] = inventoryMap
		inventoryMap["threshold"] = inventory[1]
		inventoryMap["remaining"] = inventory[2]
		inventoryMap["cost"] = inventory[3]
		inventoryMap["price"] = inventory[4]
	}

	return report, nil
}

/*
 Generates the data for the inventory existence report. Provides a listing of all products, and their inventory details.
*/
func (dao *ReportDao) GenerateInventoryExistence() (*model.Report, error) {
	var report = model.NewEmptyReport()
	var query = dao.GetQuery("pnc.inventory-under-threshold")
	var ii, err = dao.Template().Select(query, mapInventoryEntry)

	if ii == nil || (err != nil && err != data.NOT_FOUND) {
		return nil, err
	}

	for _, i := range ii {
		var inventoryMap = make(map[string]interface{})
		var inventory = i.([]interface{})
		var key = inventory[0].(string)
		report.Data()[key] = inventoryMap
		inventoryMap["threshold"] = inventory[1]
		inventoryMap["remaining"] = inventory[2]
		inventoryMap["cost"] = inventory[3]
		inventoryMap["price"] = inventory[4]
	}

	return report, nil
}

/*
 Generates the data for the expiration report. Provides a listing of all products expired, or that will expire in a given
 number of days. Depends on the provided arguments.
*/
func (dao *ReportDao) GenerateInventoryExpiration(expired bool, days uint) (*model.Report, error) {
	var report = model.NewEmptyReport()
	var ii []interface{}
	var err error

	if expired {
		var query = dao.GetQuery("pnc.inventory-expired")
		ii, err = dao.Template().Select(query, mapExpirationEntry)
	} else {
		var query = dao.GetQuery("pnc.inventory-future-expiration")
		ii, err = dao.Template().Select(query, mapExpirationEntry, days)
	}

	if ii == nil || (err != nil && err != data.NOT_FOUND) {
		return nil, err
	}

	for _, i := range ii {
		var expirationMap = make(map[string]interface{})
		var expiration = i.([]interface{})
		var key = expiration[0].(string)
		report.Data()[key] = expirationMap
		expirationMap["remaining"] = expiration[1]
		expirationMap["entered"] = expiration[2]
		expirationMap["expiration"] = expiration[3]
	}

	return report, nil
}
