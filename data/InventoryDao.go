package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Database driven data access object implemenation for the Inventory model
*/
type InventoryDao struct {
	*data.BaseDbDao
}

/*
 Create a new Inventory DAO
*/
func NewInventoryDao(template *data.SqlTemplate, dictionary map[string]string) (*InventoryDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var dao = &InventoryDao{base}
	return dao, nil
}

/*
 Maps a row containing inventory data to an inventory model object
*/
func mapInventory(rows *sql.Rows) (interface{}, error) {
	var id, idProduct uint
	var enteredStr string
	var expirationByte []byte
	var quantity, remaining, cost float32
	var expiration *time.Time

	var err = rows.Scan(&id, &idProduct, &quantity, &remaining, &enteredStr, &expirationByte, &cost)

	if err != nil {
		return nil, err
	}

	entered, err := time.Parse(data.DATE_FORMAT, enteredStr)

	if err != nil {
		return nil, err
	}

	expirationStr := string(expirationByte)

	if len(expirationStr) > 0 {
		tmpExpr, err := time.Parse(data.DATE_FORMAT, expirationStr)

		if err != nil {
			return nil, err
		}

		expiration = &tmpExpr
	}

	inventory, err := model.NewInventory(id, idProduct, quantity, remaining, entered, expiration, cost)

	return inventory, err
}

/*
 Maps an array of numbers which represent product remaining existence and inventory threshold
*/
func mapInventoryState(rows *sql.Rows) (interface{}, error) {
	var remaining, threshold uint
	var err = rows.Scan(&remaining, &threshold)

	return []uint{remaining, threshold}, err
}

/*
 Retrieves an Inventory object identified by the given ID
*/
func (dao *InventoryDao) Get(id uint) (*model.Inventory, error) {
	var query = dao.GetQuery("pnc.get-inventory")
	var result, err = dao.Template().SelectFirst(query, mapInventory, id)

	if result == nil || err != nil {
		return nil, err
	}

	var inventory = result.(*model.Inventory)
	return inventory, nil
}

/*
 Retrieves all available inventory objects
*/
func (dao *InventoryDao) GetAll() ([]*model.Inventory, error) {
	var query = dao.GetQuery("pnc.get-all-inventory")
	var result, err = dao.Template().Select(query, mapInventory)

	return castInventory(result), err
}

/*
 Retrieves all inventory objects that keep track of a specific product
*/
func (dao *InventoryDao) GetByProduct(idProduct uint) ([]*model.Inventory, error) {
	var query = dao.GetQuery("pnc.get-inventory-by-product")
	var result, err = dao.Template().Select(query, mapInventory, idProduct)

	return castInventory(result), err
}

/*
 Add an inventory item to the collection
*/
func (dao *InventoryDao) Add(inventory *model.Inventory) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var id int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-inventory-state")
		var result, err = txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var state = castInventoryState(result)
		var expirationStr *string

		if inventory.Expiration() != nil {
			tmp := inventory.Expiration().Format(data.DATE_FORMAT)
			expirationStr = &tmp
		}

		query = dao.GetQuery("pnc.add-inventory")
		id, err = txt.Insert(query,
			inventory.IdProduct(),
			inventory.Quantity(),
			inventory.Remaining(),
			inventory.Entered().Format(data.DATE_FORMAT),
			expirationStr,
			inventory.Cost(),
		)

		if err != nil {
			return err
		}

		if len(state) > 1 && state[0] < state[1] && state[0]+uint(inventory.Remaining()) > state[1] {
			query = dao.GetQuery("pnc.remove-notification-by-source-type")
			_, err := txt.Update(query, inventory.IdProduct(), model.INVENTORY_THRESHOLD)

			if err != nil {
				return err
			}
		}

		return nil
	}
	err = txt.Execute(tx)

	return uint(id), err
}

/*
 Update the details of an inventory item
*/
func (dao *InventoryDao) Update(inventory *model.Inventory) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-inventory-state")
		var result, err = txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var before = castInventoryState(result)
		var expirationStr *string

		if inventory.Expiration() != nil {
			tmp := inventory.Expiration().Format(data.DATE_FORMAT)
			expirationStr = &tmp
		}

		query = dao.GetQuery("pnc.update-inventory")
		records, err = txt.Update(query,
			inventory.Quantity(),
			inventory.Remaining(),
			inventory.Entered().Format(data.DATE_FORMAT),
			expirationStr,
			inventory.Cost(),
			inventory.Id(),
		)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.get-inventory-state")
		result, err = txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var after = castInventoryState(result)

		if len(before) > 1 && len(after) > 1 {
			if before[0] >= before[1] && after[0] < after[1] {
				_, err = Notify(dao.BaseDbDao, txt, inventory.IdProduct(), model.INVENTORY_THRESHOLD)
			} else if before[0] < before[1] && after[0] >= after[1] {
				query = dao.GetQuery("pnc.remove-notification-by-source-type")
				_, err = txt.Update(query, inventory.IdProduct(), model.INVENTORY_THRESHOLD)
			}
		}

		return err
	}
	err = txt.Execute(tx)

	return uint(records), err
}

/*
 The Update method will update all details of an inventory item. However a transaction that will be done most of the
 time will be to update the remaining quantity of a product (every time a sale is performed), so it has it's own method.
*/
func (dao *InventoryDao) UpdateRemaining(id, remaining uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-inventory")
		var result, err = txt.SelectFirst(query, mapInventory, id)

		if err != nil {
			return err
		}

		var inventory = result.(*model.Inventory)
		query = dao.GetQuery("pnc.get-inventory-state")
		stateResult, err := txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var before = castInventoryState(stateResult)
		query = dao.GetQuery("pnc.update-inventory-remaining")
		records, err = dao.Template().Update(query, remaining, id)

		query = dao.GetQuery("pnc.get-inventory-state")
		stateResult, err = txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var after = castInventoryState(stateResult)

		if len(before) > 1 || len(after) > 1 {
			if before[0] > before[1] && after[0] < after[1] {
				_, err = Notify(dao.BaseDbDao, txt, inventory.IdProduct(), model.INVENTORY_THRESHOLD)
			} else if before[0] < before[1] && after[0] > after[1] {
				query = dao.GetQuery("pnc.remove-notification-by-source-type")
				_, err = txt.Update(query, inventory.IdProduct(), model.INVENTORY_THRESHOLD)
			}
		}

		return err
	}
	err = txt.Execute(tx)

	return uint(records), err
}

/*
 Removes an inventory item from the collection
*/
func (dao *InventoryDao) Remove(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-inventory")
		var result, err = txt.SelectFirst(query, mapInventory, id)

		if err != nil {
			return err
		} else if result == nil {
			return nil
		}

		var inventory = result.(*model.Inventory)

		query = dao.GetQuery("pnc.get-inventory-state")
		stateResult, err := txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var before = castInventoryState(stateResult)
		query = dao.GetQuery("pnc.remove-inventory")
		records, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.get-inventory-state")
		stateResult, err = txt.SelectFirst(query, mapInventoryState, inventory.IdProduct())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var after = castInventoryState(stateResult)

		if len(before) > 1 && len(after) > 1 {
			if before[0] > before[1] && after[0] < after[1] {
				_, err = Notify(dao.BaseDbDao, txt, inventory.IdProduct(), model.INVENTORY_THRESHOLD)
			}
		}

		return nil
	}
	err = txt.Execute(tx)

	return uint(records), err
}

/*
 Casts a group of inventory objects of interface{} type into Inventory model types
*/
func castInventory(result []interface{}) []*model.Inventory {
	if result == nil {
		return nil
	}

	var inventory = make([]*model.Inventory, len(result))

	for cont, i := range result {
		inventory[cont] = i.(*model.Inventory)
	}

	return inventory
}

/*
 Casts a slice containing inventory state data into a slice of unsigned integers
 The slice is expected to have two values: the total quantity remaining and the expected threshold
*/
func castInventoryState(result interface{}) []uint {
	if result == nil {
		return []uint{}
	}

	return result.([]uint)
}
