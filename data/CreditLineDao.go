package data

import (
	"database/sql"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"time"
)

/*
 Data driven data accessobject implementation for the credit line and related models
*/
type CreditLineDao struct {
	*data.BaseDbDao
}

/*
 Creates a new Credit Line data access object
*/
func NewCreditLineDao(template *data.SqlTemplate, dictionary map[string]string) (*CreditLineDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &CreditLineDao{base}, nil
}

/*
 Maps a row holding credit line data into a credit line model object. Only expects base credit line data to be
 present. Payment and invoice data is mapped separately.
*/
func mapCreditLine(rows *sql.Rows) (interface{}, error) {
	var id, active uint
	var limit, balance float32

	var err = rows.Scan(&id, &limit, &balance, &active)

	if err != nil {
		return nil, err
	}

	return model.NewCreditLine(id, limit, balance, active != 0)
}

/*
 Maps a row holding payment data into a credit line model object.
*/
func mapPayment(rows *sql.Rows) (interface{}, error) {
	var id, idCreditLine, idUser uint
	var timestampStr string
	var note []byte
	var amount float32

	var err = rows.Scan(&id, &idCreditLine, &idUser, &timestampStr, &amount, &note)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewPayment(id, idCreditLine, idUser, timestamp, amount, string(note))
}

/*
 Retrieves a credit line identified by the given ID
*/
func (dao *CreditLineDao) Get(id uint) (*model.CreditLine, error) {
	var creditLine *model.CreditLine
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-credit-line")
		cl, err := txt.SelectFirst(query, mapCreditLine, id)

		if cl == nil || err != nil {
			return err
		}

		creditLine = cl.(*model.CreditLine)
		return dao.addChildren(txt, creditLine)
	}

	err = txt.Execute(tx)
	return creditLine, err
}

/*
 Retrieves a credit line for a customer if one exists
*/
func (dao *CreditLineDao) GetByCustomer(idCustomer uint) (*model.CreditLine, error) {
	var creditLine *model.CreditLine
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-credit-line-by-customer")
		cl, err := txt.SelectFirst(query, mapCreditLine, idCustomer)

		if cl == nil || err != nil {
			return err
		}

		creditLine = cl.(*model.CreditLine)
		return dao.addChildren(txt, creditLine)
	}

	err = txt.Execute(tx)
	return creditLine, err
}

/*
 Retrieves a credit line for a supplier if one exists
*/
func (dao *CreditLineDao) GetBySupplier(idSupplier uint) (*model.CreditLine, error) {
	var creditLine *model.CreditLine
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-credit-line-by-supplier")
		cl, err := txt.SelectFirst(query, mapCreditLine, idSupplier)

		if cl == nil || err != nil {
			return err
		}

		creditLine = cl.(*model.CreditLine)
		return dao.addChildren(txt, creditLine)
	}

	err = txt.Execute(tx)
	return creditLine, err
}

/*
 Retrieves a credit line for a sponsor if one exists
*/
func (dao *CreditLineDao) GetBySponsor(idSponsor uint) (*model.CreditLine, error) {
	var creditLine *model.CreditLine
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-credit-line-by-sponsor")
		cl, err := txt.SelectFirst(query, mapCreditLine, idSponsor)

		if cl == nil || err != nil {
			return err
		}

		creditLine = cl.(*model.CreditLine)
		return dao.addChildren(txt, creditLine)
	}

	err = txt.Execute(tx)
	return creditLine, err
}

/*
 Adds a new credit line and links it to a customer
*/
func (dao *CreditLineDao) AddToCustomer(idCustomer uint, creditLine *model.CreditLine) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-credit-line")

		id, err := txt.Insert(query,
			creditLine.Limit(),
			creditLine.Balance(),
			creditLine.Active(),
		)

		if err != nil {
			return err
		}

		creditLine.SetId(uint(id))
		query = dao.GetQuery("pnc.link-credit-line-to-customer")
		_, err = txt.Insert(query,
			idCustomer,
			creditLine.Id(),
		)

		return data.CheckConstraintError(err)
	}

	err = txt.Execute(tx)
	return creditLine.Id(), err
}

/*
 Adds a new credit line and links it to a supplier
*/
func (dao *CreditLineDao) AddToSupplier(idSupplier uint, creditLine *model.CreditLine) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-credit-line")
		id, err := txt.Insert(query,
			creditLine.Limit(),
			creditLine.Balance(),
			creditLine.Active(),
		)

		if err != nil {
			return err
		}

		creditLine.SetId(uint(id))
		query = dao.GetQuery("pnc.link-credit-line-to-supplier")
		_, err = txt.Insert(query,
			idSupplier,
			creditLine.Id(),
		)

		return data.CheckConstraintError(err)
	}

	err = txt.Execute(tx)
	return creditLine.Id(), err
}

/*
 Updates the details for a credit line
*/
func (dao *CreditLineDao) Update(creditLine *model.CreditLine) (uint, error) {
	var query = dao.GetQuery("pnc.update-credit-line")
	records, err := dao.Template().Update(query,
		creditLine.Limit(),
		creditLine.Balance(),
		creditLine.Active(),
		creditLine.Id(),
	)

	return uint(records), data.CheckConstraintError(err)
}

/*
 Removes a credit line
*/
func (dao *CreditLineDao) Remove(idCreditLine uint) (uint, error) {
	var records uint
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var keys = []string{
			"pnc.remove-payments-by-credit-line",
			"pnc.unlink-all-invoices-from-credit-line",
			"pnc.unlink-credit-line",
		}

		for _, key := range keys {
			var query = dao.GetQuery(key)
			_, err := txt.Update(query, idCreditLine)

			if err != nil {
				return err
			}
		}

		var query = dao.GetQuery("pnc.remove-credit-line")
		result, err := txt.Update(query, idCreditLine)
		records = uint(result)
		return data.CheckConstraintError(err)
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Links an invoice to a credit line
*/
func (dao *CreditLineDao) LinkInvoice(idCreditLine, idInvoice uint) (uint, error) {
	var query = dao.GetQuery("pnc.link-invoice-to-credit-line")
	var result, err = dao.Template().Update(query, idCreditLine, idInvoice)

	return uint(result), data.CheckConstraintError(err)
}

/*
 Unlinks an invoice from a credit line
*/
func (dao *CreditLineDao) UnlinkInvoice(idCreditLine, idInvoice uint) (uint, error) {
	var query = dao.GetQuery("pnc.unlink-invoice-from-credit-line")
	var result, err = dao.Template().Update(query, idCreditLine, idInvoice)

	return uint(result), err
}

/*
 Retrieve all payments made within the given time range
*/
func (dao *CreditLineDao) GetPayments(from, to time.Time) ([]*model.Payment, error) {
	var query = dao.GetQuery("pnc.get-payments-by-time-range")
	var result, err = dao.Template().Select(query, mapPayment,
		from.Format(data.DATE_TIME_FORMAT),
		to.Format(data.DATE_TIME_FORMAT),
	)

	return castPayments(result), err
}

/*
 Adds a payment to a credit line
*/
func (dao *CreditLineDao) AddPayment(payment *model.Payment) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-payment")
		var id, err = txt.Insert(query,
			payment.IdCreditLine(),
			payment.IdUser(),
			payment.Timestamp().Format(data.DATE_TIME_FORMAT),
			payment.Amount(),
			payment.Note(),
		)

		if err != nil {
			return err
		}

		payment.SetId(uint(id))

		query = dao.GetQuery("pnc.get-credit-line")
		cri, err := txt.SelectFirst(query, mapCreditLine, payment.IdCreditLine())

		if cri == nil || err != nil {
			return err
		}

		var creditLine = cri.(*model.CreditLine)
		var newBalance = creditLine.Balance() - payment.Amount()

		if newBalance < 0 {
			return CREDIT_LINE_LIMIT
		}

		query = dao.GetQuery("pnc.update-credit-line-balance")
		_, err = txt.Update(query, newBalance, payment.IdCreditLine())

		return err
	}
	err = txt.Execute(tx)
	return payment.Id(), err
}

/*
 Removes a payment
*/
func (dao *CreditLineDao) RemovePayment(idPayment uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-payment")
		pi, err := txt.SelectFirst(query, mapPayment, idPayment)

		if err != nil {
			return err
		}

		var payment = pi.(*model.Payment)

		query = dao.GetQuery("pnc.get-last-balance")
		bi, err := txt.SelectFirst(query, mapBalance)

		if err != nil {
			return err
		}

		var balance = bi.(*model.Balance)

		if balance.Timestamp().After(payment.Timestamp()) {
			return data.CONFLICT
		}

		query = dao.GetQuery("pnc.get-credit-line")
		cli, err := txt.SelectFirst(query, mapCreditLine, payment.IdCreditLine())

		if err != nil {
			return err
		}

		var creditLine = cli.(*model.CreditLine)
		var newBalance = creditLine.Balance() + payment.Amount()

		query = dao.GetQuery("pnc.update-credit-line-balance")
		_, err = txt.Update(query, newBalance, creditLine.Id())

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-payment")
		records, err = txt.Update(query, idPayment)

		return err
	}
	err = txt.Execute(tx)
	return uint(records), err
}

/*
 Utility used to facilite persisting of credit line children to the database
*/
func (dao *CreditLineDao) addChildren(txt *data.TxTemplate, creditLine *model.CreditLine) error {
	var query = dao.GetQuery("pnc.get-invoices-by-credit-line")
	clir, err := txt.Select(query, mapInvoice, creditLine.Id())

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	for _, cli := range clir {
		var i = cli.(*model.Invoice)
		creditLine.AddInvoice(i)
	}

	query = dao.GetQuery("pnc.get-payments-by-credit-line")
	clpr, err := txt.Select(query, mapPayment, creditLine.Id())

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	for _, clp := range clpr {
		var p = clp.(*model.Payment)
		creditLine.AddPayment(p)
	}

	return nil
}

/*
 Casts a slice of payment objects into payment type
*/
func castPayments(result []interface{}) []*model.Payment {
	if result == nil {
		return nil
	}

	var payments = make([]*model.Payment, len(result))

	for cont, e := range result {
		payments[cont] = e.(*model.Payment)
	}

	return payments
}
