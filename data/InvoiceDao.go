package data

import (
	"database/sql"
	"errors"
	"gxs3/godfather/data"
	"gxs3/panacea/model"
	"strconv"
	"strings"
	"time"
)

var (
	INSUFFICIENT_INVENTORY = errors.New("Insufficient inventory to sell item")
	INVALID_STATE          = errors.New("Cannot change inventory's state, since it's not currently OK")
	CREDIT_LINE_LIMIT      = errors.New("Credit Line doesn't have enough credit to process sale or payment")
)

/*
 Data driven data access object implementation for the Invoice model
*/
type InvoiceDao struct {
	*data.BaseDbDao
}

type invoiceItemInventory struct {
	id            uint
	idInvoiceItem uint
	idInventory   uint
	quantity      float32
}

/*
 Creates a new Invoice Dao
*/
func NewInvoiceDao(template *data.SqlTemplate, dictionary map[string]string) (*InvoiceDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	return &InvoiceDao{base}, nil
}

/*
 Maps a row to an invoice model object
*/
func mapInvoice(rows *sql.Rows) (interface{}, error) {
	var id, idUser, txu, payu, stu uint
	var timestampStr string
	var total, discount float32

	var err = rows.Scan(&id, &idUser, &txu, &payu, &stu, &timestampStr, &total, &discount)

	if err != nil {
		return nil, err
	}

	transaction, err := model.ToTransactionType(txu)

	if err != nil {
		return nil, err
	}

	payment, err := model.ToPaymentType(payu)

	if err != nil {
		return nil, err
	}

	state, err := model.ToStateType(stu)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewInvoice(id, idUser, transaction, payment, state, timestamp, total, discount)
}

/*
 Maps a row to an invoice item model object
*/
func mapInvoiceItem(rows *sql.Rows) (interface{}, error) {
	var id, idInvoice, idProduct uint
	var uuid string
	var quantity float32

	var err = rows.Scan(&id, &uuid, &idInvoice, &idProduct, &quantity)

	if err != nil {
		return nil, err
	}

	return model.NewInvoiceItem(id, uuid, idInvoice, idProduct, quantity)
}

/*
 Maps a row to a invoice item invne
*/
func mapInvoiceItemInventory(rows *sql.Rows) (interface{}, error) {
	var id, idInvoiceItem, idInventory uint
	var quantity float32

	var err = rows.Scan(&id, &idInvoiceItem, &idInventory, &quantity)

	if err != nil {
		return nil, err
	}

	var iii = invoiceItemInventory{id, idInvoiceItem, idInventory, quantity}
	return &iii, nil
}

/*
 Maps a refund object from a result set row
*/
func mapRefund(rows *sql.Rows) (interface{}, error) {
	var id, idUser, idInvoice uint
	var timestampStr string
	var calculated, actual float32

	var err = rows.Scan(&id, &idUser, &idInvoice, &timestampStr, &calculated, &actual)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewRefund(id, idUser, idInvoice, timestamp, calculated, actual)
}

/*
 Maps a resotck object from a result set row
*/
func mapRestock(rows *sql.Rows) (interface{}, error) {
	var id, idRefund, idInvoiceItem uint
	var quantity, value float32
	var restocked bool

	var err = rows.Scan(&id, &idRefund, &idInvoiceItem, &quantity, &value, &restocked)

	if err != nil {
		return nil, err
	}

	return model.NewRestock(id, idRefund, idInvoiceItem, quantity, value, restocked)
}

/**
 * Maps an invalidation from a query result set row
 */
func mapInvalidation(rows *sql.Rows) (interface{}, error) {
	var id, idUser, idInvoice uint
	var timestampStr string
	var reason []byte

	var err = rows.Scan(&id, &idUser, &idInvoice, &timestampStr, &reason)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewInvalidation(id, idUser, idInvoice, timestamp, string(reason))
}

/*
 Retrieves an invoice by the provided ID
*/
func (dao *InvoiceDao) Get(id uint) (*model.Invoice, error) {
	var invoice *model.Invoice = nil
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-invoice")
		i, err := txt.SelectFirst(query, mapInvoice, id)

		if i == nil || err != nil {
			return err
		}

		invoice = i.(*model.Invoice)

		query = dao.GetQuery("pnc.get-invoice-items-by-invoice")
		items, err := txt.Select(query, mapInvoiceItem, id)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, i := range items {
			var item = i.(*model.InvoiceItem)
			invoice.Add(item)
		}

		return nil
	}

	err = txt.Execute(tx)
	return invoice, err
}

/*
 Retrieves all available invoices
*/
func (dao *InvoiceDao) GetAll() ([]*model.Invoice, error) {
	var invoices []*model.Invoice
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-invoices")
		result, err := txt.Select(query, mapInvoice)

		if err != nil {
			return err
		}

		invoices = castInvoices(result)
		query = dao.GetQuery("pnc.get-invoice-items")
		result, err = txt.Select(query, mapInvoiceItem)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var items = castItems(result)
		mapItems(invoices, items)
		return nil
	}

	err = txt.Execute(tx)
	return invoices, err
}

/*
 Retrieves all invoices within the date range provided
*/
func (dao *InvoiceDao) GetByRange(from, to time.Time) ([]*model.Invoice, error) {
	var invoices []*model.Invoice
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var fromStr, toStr = from.Format(data.DATE_TIME_FORMAT), to.Format(data.DATE_TIME_FORMAT)
		var query = dao.GetQuery("pnc.get-invoices-by-date-range")
		result, err := txt.Select(query, mapInvoice, fromStr, toStr)

		if err != nil {
			return err
		}

		invoices = castInvoices(result)
		query = dao.GetQuery("pnc.get-invoice-items-by-date-range")
		result, err = txt.Select(query, mapInvoiceItem, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var items = castItems(result)
		mapItems(invoices, items)
		return nil
	}

	err = txt.Execute(tx)
	return invoices, err
}

/*
 Retrieves all invoices within the date range provided also disciriminating by user account
*/
func (dao *InvoiceDao) GetByRangeAndUser(idUser uint, from, to time.Time) ([]*model.Invoice, error) {
	var invoices []*model.Invoice
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var fromStr, toStr = from.Format(data.DATE_TIME_FORMAT), to.Format(data.DATE_TIME_FORMAT)
		var query = dao.GetQuery("pnc.get-invoices-by-date-range-and-user")
		result, err := txt.Select(query, mapInvoice, idUser, fromStr, toStr)

		if err != nil {
			return err
		}

		invoices = castInvoices(result)
		query = dao.GetQuery("pnc.get-invoice-items-by-date-range-and-user")
		result, err = txt.Select(query, mapInvoiceItem, idUser, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var items = castItems(result)
		mapItems(invoices, items)
		return nil
	}

	err = txt.Execute(tx)
	return invoices, err
}

/*
 Returns all the invoices that are associated with the given customer
*/
func (dao *InvoiceDao) GetByCustomer(idCustomer uint) ([]*model.Invoice, error) {
	var invoices []*model.Invoice
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-invoices-by-customer")
		result, err := txt.Select(query, mapInvoice, idCustomer)

		if err != nil {
			return err
		}

		invoices = castInvoices(result)
		query = dao.GetQuery("pnc.get-invoice-items-by-customer")
		result, err = txt.Select(query, mapInvoiceItem, idCustomer)

		if err != nil {
			return err
		}

		var items = castItems(result)
		mapItems(invoices, items)
		return nil
	}

	err = txt.Execute(tx)
	return invoices, err
}

/*
 Searches for invoices. The time range values are required and should always have meaningful values. The user and payment
 arguments may optionally have zero values, if it is not required to use those values as search parameters
*/
func (dao *InvoiceDao) Search(from, to time.Time, idUser uint, payment model.PaymentType, transaction model.TransactionType) ([]*model.Invoice, error) {
	var query = []string{dao.GetQuery("pnc.get-invoices-by-date-range")}
	var clause = []string{}

	if idUser > 0 {
		clause = append(clause, " AND id_user = ")
		clause = append(clause, strconv.FormatUint(uint64(idUser), 10))
	}

	if payment > 0 {
		clause = append(clause, " AND payment_type = ")
		clause = append(clause, strconv.FormatUint(uint64(payment), 10))
	}

	if transaction > 0 {
		clause = append(clause, " AND transaction_type = ")
		clause = append(clause, strconv.FormatUint(uint64(transaction), 10))
	}

	query = append(query, strings.Join(clause, ""))
	query = append(query, " ORDER BY timestamp DESC")

	var invoices []*model.Invoice
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var fromStr, toStr = from.Format(data.DATE_TIME_FORMAT), to.Format(data.DATE_TIME_FORMAT)
		result, err := txt.Select(strings.Join(query, ""), mapInvoice, fromStr, toStr)

		if err != nil {
			return err
		}

		invoices = castInvoices(result)
		query = []string{dao.GetQuery("pnc.get-invoice-items-by-date-range")}
		query = append(query, strings.Join(clause, ""))
		result, err = txt.Select(strings.Join(query, ""), mapInvoiceItem, fromStr, toStr)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		var items = castItems(result)
		mapItems(invoices, items)
		return nil
	}

	err = txt.Execute(tx)
	return invoices, err
}

func (dao *InvoiceDao) sellInternal(invoice *model.Invoice, txt *data.TxTemplate) (uint, error) {
	var query = dao.GetQuery("pnc.add-invoice")
	var id, err = txt.Insert(query,
		invoice.IdUser(),
		uint(invoice.Transaction()),
		uint(invoice.Payment()),
		uint(invoice.State()),
		invoice.Timestamp().Format(data.DATE_TIME_FORMAT),
		invoice.Total(),
		invoice.Discount(),
	)

	if err != nil {
		return 0, err
	}

	invoice.SetId(uint(id))
	query = dao.GetQuery("pnc.add-invoice-item")

	for _, item := range invoice.Items() {
		id, err := txt.Insert(query,
			item.Uuid(),
			invoice.Id(),
			item.IdProduct(),
			item.Quantity(),
		)

		if err != nil {
			return 0, err
		}

		item.SetId(uint(id))
		err = dao.updateInventory(txt, item)

		if err != nil {
			return 0, err
		}

		err = dao.checkInventoryState(txt, item)
	}

	return uint(id), nil
}

/*
 Adds a new invoice
*/
func (dao *InvoiceDao) Sell(invoice *model.Invoice) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		_, err := dao.sellInternal(invoice, txt)
		return err
	}

	err = txt.Execute(tx)
	return invoice.Id(), err
}

func (dao *InvoiceDao) SellCredit(invoice *model.Invoice, idCreditLine uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		_, err := dao.sellInternal(invoice, txt)

		if err != nil {
			return err
		}

		var query = dao.GetQuery("pnc.link-invoice-to-credit-line")
		_, err = txt.Update(query, idCreditLine, invoice.Id())

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.get-credit-line")
		cri, err := txt.SelectFirst(query, mapCreditLine, idCreditLine)

		if cri == nil || err != nil {
			return err
		}

		var creditLine = cri.(*model.CreditLine)
		var newBalance = creditLine.Balance() + invoice.Total()

		if creditLine.Limit() > -1 && newBalance > creditLine.Limit() {
			return CREDIT_LINE_LIMIT
		}

		query = dao.GetQuery("pnc.update-credit-line-balance")
		_, err = txt.Update(query, newBalance, idCreditLine)

		return err
	}

	err = txt.Execute(tx)
	return invoice.Id(), err
}

func (dao *InvoiceDao) SellSponsored(invoice *model.Invoice, idSponsor uint, amount float32) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		_, err := dao.sellInternal(invoice, txt)

		if err != nil {
			return err
		}

		var query = dao.GetQuery("pnc.insert-sponsorship")
		_, err = txt.Insert(query,
			idSponsor,
			invoice.Id(),
			invoice.Timestamp().Format(data.DATE_TIME_FORMAT),
			invoice.Total(),
			amount,
			0,
			nil,
		)

		return err
	}

	err = txt.Execute(tx)
	return invoice.Id(), err
}

/*
 Retrieves an invalidation using the corresponding invoice as a reference. The invoker will pass the ID of the invoice.
 If an invalidation exists for that invoice, it will be returned.
*/
func (dao *InvoiceDao) GetInvalidation(id uint) (*model.Invalidation, error) {
	var query = dao.GetQuery("pnc.get-invalidation-by-invoice")
	var result, err = dao.Template().SelectFirst(query, mapInvalidation, id)

	if result == nil || err != nil {
		return nil, err
	}

	var invalidation = result.(*model.Invalidation)
	return invalidation, nil
}

/*
 Marks an invoice as invalid. Returns all inventory back to available for sale. used in cases, where an error is made in
 an invoice, or a problem arises, and that invoice needs to be marked as invalid.
*/
func (dao *InvoiceDao) Invalidate(invalidation *model.Invalidation) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.add-invalidation")
		var id, err = txt.Insert(query, invalidation.IdUser(),
			invalidation.IdInvoice(),
			invalidation.Timestamp().Format(data.DATE_TIME_FORMAT),
			invalidation.Reason(),
		)

		if err != nil {
			return err
		}

		invalidation.SetId(uint(id))
		query = dao.GetQuery("pnc.get-invoice")
		ri, err := txt.SelectFirst(query, mapInvoice, invalidation.IdInvoice())

		if err != nil {
			return err
		}

		var invoice = ri.(*model.Invoice)

		if invoice.State() != model.OK {
			return INVALID_STATE
		}

		query = dao.GetQuery("pnc.update-invoice-state")
		result, err := txt.Update(query, uint(model.INVALIDATED), invalidation.IdInvoice())

		if err != nil {
			return err
		}

		records = uint(result)
		query = dao.GetQuery("pnc.get-invoice-items-by-invoice")
		raw, err := txt.Select(query, mapInvoiceItem, invalidation.IdInvoice())

		if err != nil {
			return err
		}

		var items = castItems(raw)
		var iiQuery = dao.GetQuery("pnc.get-invoice-item-inventory-by-invoice-item")
		var inventoryQuery = dao.GetQuery("pnc.get-inventory")
		var updateInventoryQuery = dao.GetQuery("pnc.update-inventory-remaining")
		for _, item := range items {
			raw, err := txt.Select(iiQuery, mapInvoiceItemInventory, item.Id())

			if err != nil {
				return err
			}

			for _, iir := range raw {
				var ii = iir.(*invoiceItemInventory)
				ir, err := txt.SelectFirst(inventoryQuery, mapInventory, ii.idInventory)

				if err != nil {
					return err
				}

				var inventory = ir.(*model.Inventory)
				inventory.SetRemaining(inventory.Remaining() + ii.quantity)
				_, err = txt.Update(updateInventoryQuery, inventory.Remaining(), inventory.Id())

				if err != nil {
					return err
				}
			}
		}

		if invoice.Payment() != model.CREDIT {
			return nil
		}

		query = dao.GetQuery("pnc.get-credit-line-by-invoice")
		cli, err := txt.SelectFirst(query, mapCreditLine, invoice.Id())

		if err != nil {
			return err
		}

		var creditLine = cli.(*model.CreditLine)
		creditLine.SetBalance(creditLine.Balance() - invoice.Total())
		query = dao.GetQuery("pnc.update-credit-line-balance")
		_, err = txt.Update(query, creditLine.Balance(), creditLine.Id())

		return err
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Reverses the actions taken by a previous invalidate transaction. Places the state of the invoice as OK. Will only work
 if the refund has not been added to a balance yet, otherwise a CONFLICT error will be raised.
*/
func (dao *InvoiceDao) UndoInvalidate(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-invoice")
		var ri, err = txt.SelectFirst(query, mapInvoice, id)

		if err != nil {
			return err
		}

		var invoice = ri.(*model.Invoice)

		if invoice.State() != model.INVALIDATED {
			return INVALID_STATE
		}

		query = dao.GetQuery("pnc.get-last-balance")
		bi, err := txt.SelectFirst(query, mapBalance)

		if err != nil {
			return err
		}

		var balance = bi.(*model.Balance)

		if balance.Timestamp().After(invoice.Timestamp()) {
			return data.CONFLICT
		}

		query = dao.GetQuery("pnc.remove-invalidation-by-invoice")
		records, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.update-invoice-state")
		records, err = txt.Update(query, uint(model.OK), id)

		if err != nil {
			return err
		} else if invoice.Payment() != model.CREDIT {
			return nil
		}

		query = dao.GetQuery("pnc.get-credit-line-by-invoice")
		cli, err := txt.SelectFirst(query, mapCreditLine, invoice.Id())

		if err != nil {
			return err
		}

		var creditLine = cli.(*model.CreditLine)
		creditLine.SetBalance(creditLine.Balance() + invoice.Total())
		query = dao.GetQuery("pnc.update-credit-line-balance")
		_, err = txt.Update(query, creditLine.Balance(), creditLine.Id())

		return err
	}

	err = txt.Execute(tx)
	return uint(records), err
}

/*
 Retrieves a refund for an invoice if it exists
*/
func (dao *InvoiceDao) GetRefund(idInvoice uint) (*model.Refund, error) {
	var refund *model.Refund = nil
	var txt, err = data.NewTxTemplate(dao.Template())

	if err != nil {
		return nil, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-refund-by-invoice")
		r, err := txt.SelectFirst(query, mapRefund, idInvoice)

		if err != nil {
			return err
		}

		refund = r.(*model.Refund)

		query = dao.GetQuery("pnc.get-restock-by-refund")
		items, err := txt.Select(query, mapRestock, refund.Id())

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		for _, rs := range items {
			var model = rs.(*model.Restock)
			refund.Add(model)
		}

		return nil
	}

	err = txt.Execute(tx)
	return refund, err
}

/*
 Provides a refund for an invoice. Used when a customer is unsatisfied with the purchase, and wishes to return the bought goods.
 Invoker has t passthe ID of the invoice, and a map. The map will contain IDs of invoice items and booleans indicating if
 the products for each item will be restocked or not.
*/
func (dao *InvoiceDao) Refund(refund *model.Refund) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-invoice")
		var ir, err = txt.SelectFirst(query, mapInvoice, refund.IdInvoice())

		if err != nil {
			return err
		}

		var invoice = ir.(*model.Invoice)

		if invoice.State() != model.OK {
			return INVALID_STATE
		}

		query = dao.GetQuery("pnc.update-invoice-state")
		result, err := txt.Update(query, uint(model.REFUNDED), refund.IdInvoice())

		if err != nil {
			return err
		}

		records = uint(result)
		query = dao.GetQuery("pnc.add-refund")
		idRefund, err := txt.Insert(query,
			refund.IdUser(),
			refund.IdInvoice(),
			refund.Timestamp().Format(data.DATE_TIME_FORMAT),
			refund.Calculated(),
			refund.Actual(),
		)

		if err != nil {
			return err
		}

		refund.SetId(uint(idRefund))
		query = dao.GetQuery("pnc.add-restock")
		var iiQuery = dao.GetQuery("pnc.get-invoice-item-inventory-by-invoice-item")
		var inventoryQuery = dao.GetQuery("pnc.get-inventory")
		var updateInventoryQuery = dao.GetQuery("pnc.update-inventory-remaining")

		for _, restock := range refund.Restock() {
			restock.SetIdRefund(refund.Id())
			id, err := txt.Update(query,
				restock.IdRefund(),
				restock.IdInvoiceItem(),
				restock.Quantity(),
				restock.Value(),
				data.ToNumber(restock.Restocked()),
			)

			if err != nil {
				return err
			}

			restock.SetId(uint(id))

			if !restock.Restocked() {
				continue
			}

			iis, err := txt.Select(iiQuery, mapInvoiceItemInventory, restock.IdInvoiceItem())

			if err != nil {
				return err
			}

			for _, iir := range iis {
				var ii = iir.(*invoiceItemInventory)
				raw, err := txt.SelectFirst(inventoryQuery, mapInventory, ii.idInventory)

				if err != nil {
					return err
				}

				var inventory = raw.(*model.Inventory)
				inventory.SetRemaining(inventory.Remaining() + ii.quantity)
				_, err = txt.Update(updateInventoryQuery, inventory.Remaining(), inventory.Id())

				if err != nil {
					return err
				}
			}
		}

		return nil
	}

	err = txt.Execute(tx)
	return records, err
}

/*
 Reverses the effects of a previous refund transaction. Places the state of the invoice as OK, and deletes existing
 refund and restock data. Will only work if the refund has not been added to a balance yet, otherwise a CONFLICT error
 will be raised.
*/
func (dao *InvoiceDao) UndoRefund(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records int64

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.get-refund-by-invoice")
		ri, err := txt.SelectFirst(query, mapRefund, id)

		if err != nil {
			return err
		}

		var refund = ri.(*model.Refund)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.get-invoice")
		ii, err := txt.SelectFirst(query, mapInvoice, refund.IdInvoice())

		if err != nil {
			return err
		}

		var invoice = ii.(*model.Invoice)

		if invoice.State() != model.REFUNDED {
			return INVALID_STATE
		}

		query = dao.GetQuery("pnc.get-last-balance")
		bi, err := txt.SelectFirst(query, mapBalance)

		if err != nil && err != data.NOT_FOUND {
			return err
		}

		if bi != nil {
			var balance = bi.(*model.Balance)

			if balance.Timestamp().After(refund.Timestamp()) {
				return data.CONFLICT
			}
		}

		query = dao.GetQuery("pnc.update-invoice-state")
		records, err = txt.Update(query, uint(model.OK), refund.IdInvoice())

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-restock-by-refund")
		_, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-refund")
		_, err = txt.Update(query, id)

		if err != nil {
			return err
		}

		return nil
	}

	err = txt.Execute(tx)
	return uint(records), err
}

/*
 Removes an invoice from the collection
*/
func (dao *InvoiceDao) Remove(id uint) (uint, error) {
	var txt, err = data.NewTxTemplate(dao.Template())
	var records uint

	if err != nil {
		return 0, err
	}

	var tx = func(txt *data.TxTemplate) error {
		var query = dao.GetQuery("pnc.remove-invoice-items-by-invoice")
		_, err := txt.Update(query, id)

		if err != nil {
			return err
		}

		query = dao.GetQuery("pnc.remove-invoice")
		result, err := txt.Update(query, id)

		if err != nil {
			return err
		}

		records = uint(result)
		return nil
	}

	err = txt.Execute(tx)
	return records, nil
}

/*
 Used when making a sale. Updates the item inventory to reflect the items leaving the buisness
 as a result of the sale.
*/
func (dao *InvoiceDao) updateInventory(txt *data.TxTemplate, item *model.InvoiceItem) error {
	var query = dao.GetQuery("pnc.get-existing-inventory-by-product")
	var irs, err = txt.Select(query, mapInventory, item.IdProduct())
	var total float32

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	var inventory = make([]*model.Inventory, len(irs))
	for cont, i := range irs {
		inventory[cont] = i.(*model.Inventory)
		total += inventory[cont].Remaining()
	}

	if item.Quantity() > total {
		return INSUFFICIENT_INVENTORY
	}

	total = 0
	var left = item.Quantity()
	var remainingQuery = dao.GetQuery("pnc.update-inventory-remaining")
	var itemQuery = dao.GetQuery("pnc.add-invoice-item-inventory")
	for _, i := range inventory {
		total += i.Remaining()

		if left > total {
			left -= i.Remaining()
			i.SetRemaining(0)
			_, err = txt.Update(remainingQuery, 0, i.Id())

			if err != nil {
				return err
			}

			_, err = txt.Update(itemQuery, item.Id(), i.Id(), i.Remaining())

			if err != nil {
				return err
			}
		} else {
			_, err = txt.Update(remainingQuery, i.Remaining()-left, i.Id())

			if err != nil {
				return err
			}

			_, err = txt.Update(itemQuery, item.Id(), i.Id(), left)

			if err != nil {
				return err
			}

			break
		}
	}

	return nil
}

/*
 Checks the state of the inventory afte the sale has happened. If the remaining quantity has dipped under the threshold
 it wil generated a notification to alert users.
*/
func (dao *InvoiceDao) checkInventoryState(txt *data.TxTemplate, item *model.InvoiceItem) error {
	var query = dao.GetQuery("pnc.get-inventory-state")
	var result, err = txt.SelectFirst(query, mapInventoryState, item.IdProduct())

	if err != nil && err != data.NOT_FOUND {
		return err
	}

	var state = castInventoryState(result)

	if len(state) > 1 && state[0] < state[1] {
		_, err = Notify(dao.BaseDbDao, txt, item.IdProduct(), model.INVENTORY_THRESHOLD)
	}

	return err
}

/*
 Attaches a series of invoice items to its corresponding invoices. Used when pulling collections to the database
 and items need to be mapped to their respective invoices.
*/
func mapItems(invoices []*model.Invoice, items []*model.InvoiceItem) {
	var invoiceMap = make(map[uint]*model.Invoice, len(invoices))

	for _, invoice := range invoices {
		invoiceMap[invoice.Id()] = invoice
	}

	for _, item := range items {
		var invoice = invoiceMap[item.IdInvoice()]
		invoice.Add(item)
	}
}

/*
 Tests to see if an invoice item is contained within the provided slice
*/
func containsItem(items []*model.InvoiceItem, item *model.InvoiceItem) bool {
	if items == nil || len(items) < 1 || item == nil {
		return false
	}

	for _, i := range items {
		if i.Uuid() == item.Uuid() {
			return true
		}
	}

	return false
}

/*
 Casts a group of invoices as type interface{} into invoice model types
*/
func castInvoices(result []interface{}) []*model.Invoice {
	if result == nil {
		return nil
	}

	var invoices = make([]*model.Invoice, len(result))

	for cont, i := range result {
		invoices[cont] = i.(*model.Invoice)
	}

	return invoices
}

/*
 Casts a group of invoice items as type interface{} into invoice item types
*/
func castItems(result []interface{}) []*model.InvoiceItem {
	if result == nil {
		return nil
	}

	var items = make([]*model.InvoiceItem, len(result))

	for cont, i := range result {
		items[cont] = i.(*model.InvoiceItem)
	}

	return items
}
