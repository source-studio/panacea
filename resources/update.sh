#!/bin/bash

# Script that helps with the process of updating a panacea deployment. This script will attempt to...
# -- Update binary files
# -- Update client files
# -- Update resources files that are not database or config
#   -- Only create database or config if not previously existing.
# -- Update service init and cron backup scripts
# -- Bounce the service 
# 
# Assumes it's being run from the directory where the script is
# Depends on upstart

if [[ "$(whoami)" != "root" ]]; then
    echo "This script must be run with sudo or root"
    exit 1
fi

if [ "${1}" == "" ]; then
    echo "You must pass the user where the config files will be deployed"
    exit 1
fi

if [ ! -d "/home/${1}" ]; then
    echo "A home directory for the given user ${1} doesn't exist"
    exit 1
fi

path="/usr/local/bin/panacea"
resource="/home/${1}/.config/panacea"
files=( "godfather-queries.json" "panacea-backup" "panacea-config.json-sample" "panacea-install.sql" "panacea-queries.json" "panacea.conf" )

if pgrep panacea > /dev/null 2>&1; then
    stop panacea
fi

mkdir -p $path

if [ ! -d "${path}/bin" ]; then
    rm -rf "${path}/bin"
fi

if [ ! -d "${path}/client" ]; then
    rm -rf "${path}/client"
fi

cp -R ../bin $path
cp -R ../client $path

mkdir -p $resource

for file in "${files[@]}"; do
    if [ ! -f "${resource}/${file}" ]; then
        cp -rf "../resources/${file}" $resource
    fi
done

files=( "database.sqlite" "panacea-config.json" )

for file in "${files[@]}"; do
    if [ ! -f "${resource}/${file}" ] && [ -f "../resources/${file}" ]; then
        cp -rf "../resources/${file}" $resource
    fi
done 

unset files

chown "${1}" -R $resource

cp -rf ../resources/panacea.conf /etc/init
chown root:root /etc/init/panacea.conf
chmod 644 /etc/init/panacea.conf

cp -rf ../resources/panacea-backup /etc/cron.daily
chown root:root /etc/cron.daily/panacea-backup
chmod 755 /etc/cron.daily/panacea-backup

start panacea
