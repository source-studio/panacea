#!/bin/bash

# Script that bundles all build artifacts are required resources into a single zip file for simpler deployment.

# target directory where all files will be copied to
target="${HOME}/Desktop/panacea-build"

# zip file that will be generated
zip="${HOME}/Desktop/panacea-build.zip"

if [ -d $target ]; then
    rm -rf $target
    mkdir -p $target
fi

if [ -f $zip ]; then
    rm $zip
fi

mkdir -p "${target}/bin"
cp "${GOPATH}/bin/panacea" "${target}/bin"

mkdir -p "${target}/resources"
cp -R "${GOPATH}/src/gxs3/panacea/resources/." "${target}/resources"
cp -R "${GOPATH}/src/gxs3/godfather/resources/godfather-queries.json" "${target}/resources" 
rm "${target}/resources/deploy.sh"

mkdir -p "${target}/client/static"
cp -R "${GOPATH}/src/gxs3/panacea/client/static/." "${target}/client/static"

cd "${HOME}/Desktop"
zip -q -r $zip "panacea-build"
cd - > /dev/null
rm -rf $target
