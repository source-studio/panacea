PRAGMA foreign_keys = OFF;

/********************************** creating tables *****************************************/

drop table if exists users;
create table users (
    id_user integer not null primary key autoincrement,
    username text not null,
    password text not null,
    first_name text not null,
    last_name text not null,
    enabled integer not null default 1,
    unique(username)
);

drop table if exists groups;
create table groups (
    id_group integer not null primary key autoincrement,
    name text not null,
    description text,
    unique(name)
);

drop table if exists users_groups;
create table users_groups (
    id_user_group integer not null primary key autoincrement,
    id_user integer not null,
    id_group integer not null,
    foreign key(id_user) references users(id_user),
    foreign key(id_group) references groups(id_group),
    unique(id_user, id_group)
);

drop table if exists privileges;
create table privileges (
    id_privilege integer not null primary key,
    module text not null,
    submodule text not null,
    key text not null,
    name text not null,
    unique(key)
);

drop table if exists privilege_dependencies;
create table privilege_dependencies (
    id_dependency integer not null primary key autoincrement,
    id_parent integer not null,
    id_child integer not null,
    unique(id_parent, id_child),
    foreign key(id_parent) references privileges(id_privilege),
    foreign key(id_child) references privileges(id_privilege)
);

drop table if exists grants;
create table grants (
    id_grant integer not null primary key autoincrement,
    id_privilege integer not null,
    id_group integer not null,
    foreign key(id_privilege) references privileges(id_privilege),
    foreign key(id_group) references groups(id_group),
    unique(id_privilege, id_group)
);


drop table if exists subscriptions;
create table subscriptions (
    id_subscription integer not null primary key autoincrement,
    id_user integer not null,
    notification_type integer not null,
    foreign key(id_user) references users(id_user),
    unique(id_user, notification_type)
);

drop table if exists notifications;
create table notifications (
    id_notification integer not null primary key autoincrement,
    id_subscription integer not null,
    id_user integer not null,
    id_source integer not null,
    notification_type integer not null,
    timestamp text no tnull default now,
    foreign key(id_subscription) references subscriptions(id_subscription),
    foreign key(id_user) references users(id_user),
    unique(id_user, id_source, notification_type)
);

drop table if exists products;
create table products (
    id_product integer not null primary key autoincrement,
    brand text not null,
    name text not null,
    bar_code integer,
    tax_rate real not null default 0,
    price real not null,
    threshold integer,
    minimum integer,
    unique(brand, name)
);

drop table if exists inventory;
create table inventory (
    id_inventory integer not null primary key autoincrement,
    id_product integer not null,
    quantity real not null,
    remaining real not null,
    entered text not null,
    expiration text,
    cost real not null,
    foreign key(id_product) references products(id_product)
);

drop table if exists invoices;
create table invoices (
    id_invoice integer not null primary key autoincrement,
    id_user integer not null,
    transaction_type integer not null,
    payment_type integer not null,
    state_type integer not null,
    timestamp text not null default now,
    total real not null,
    discount real not null default 0,
    foreign key(id_user) references users(id_user)
);

drop table if exists invoice_items;
create table invoice_items (
    id_invoice_item integer not null primary key autoincrement,
    uuid text not null,
    id_invoice integer not null,
    id_product integer not null,
    quantity real not null default 1,
    foreign key(id_invoice) references invoices(id_invoice),
    foreign key(id_product) references products(id_product),
    unique(id_invoice, id_product)
);

drop table if exists invoice_items_inventory;
create table invoice_items_inventory (
    id_invoice_item_inventory integer not null primary key autoincrement,
    id_invoice_item integer not null,
    id_inventory integer not null,
    quantity real not null,
    foreign key(id_invoice_item) references invoice_items(id_invoice_item),
    foreign key(id_inventory) references inventory(id_inventory)
);

drop table if exists refunds;
create table refunds (
    id_refund integer not null primary key autoincrement,
    id_user integer not null,
    id_invoice integer not null,
    timestamp text not null default now,
    calculated real not null,
    actual real not null,
    unique(id_invoice),
    foreign key(id_user) references users(id_user),
    foreign key(id_invoice) references invoices(id_invoice)
);

drop table if exists restock;
create table restock (
    id_restock integer not null primary key autoincrement,
    id_refund integer not null,
    id_invoice_item integer not null,
    quantity real not null,
    value real not null,
    restock integer not null default 0,
    unique(id_invoice_item),
    foreign key(id_refund) references refunds(id_refund),
    foreign key(id_invoice_item) references invoice_items(id_invoice_item)
);

drop table if exists invalidations;
create table invalidations (
    id_invalidation integer not null primary key autoincrement,
    id_user integer not null,
    id_invoice integer not null,
    timestamp text not null default now,
    reason text,
    foreign key(id_user) references users(id_user),
    foreign key(id_invoice) references invoices(id_invoice),
    unique(id_invoice)
);

drop table if exists addresses;
create table addresses (
    id_address integer not null primary key autoincrement,
    street1 text not null,
    street2 text,
    city text not null,
    province text not null,
    country text  not null
);

drop table if exists contacts;
create table contacts (
    id_contact integer not null primary key autoincrement,
    uuid text not null,
    type integer not null,
    value text not null
);

drop table if exists credit_lines;
create table credit_lines (
    id_credit_line integer not null primary key autoincrement,
    _limit real not null default 0,
    balance real not null default 0,
    active integer default 1
);

drop table if exists credit_lines_invoices;
create table credit_lines_invoices (
    id_credit_line_invoice integer not null primary key autoincrement,
    id_credit_line integer not null,
    id_invoice integer not null,
    foreign key(id_credit_line) references credit_lines(id_credit_line),
    foreign key(id_invoice) references invoices(id_invoice),
    unique(id_credit_line, id_invoice)
);

drop table if exists customers;
create table customers (
    id_customer integer not null primary key autoincrement,
    first_name text not null,
    last_name text not null,
    nick_name text
);

drop table if exists customers_addresses;
create table customers_addresses (
    id_customer_address integer not null primary key autoincrement,
    id_customer integer not null,
    id_address integer not null,
    foreign key(id_customer) references customers(id_customer),
    foreign key(id_address) references addresses(id_address),
    unique(id_customer)
);

drop table if exists customers_contacts;
create table customers_contacts (
    id_customer_contact integer not null primary key autoincrement,
    id_customer integer not null,
    id_contact integer not null,
    foreign key(id_customer) references customers(id_customer),
    foreign key(id_contact) references contacts(id_contact)
);

drop table if exists customers_invoices;
create table customers_invoices (
    id_customer_invoice integer not null primary key autoincrement,
    id_customer integer not null,
    id_invoice integer not null,
    foreign key(id_customer) references customers(id_customer),
    foreign key(id_invoice) references invoices(id_invoice),
    unique(id_customer, id_invoice)
);

drop table if exists customers_credit_lines;
create table customers_credit_lines (
    id_customer_credit_line integer not null primary key autoincrement,
    id_customer integer not null,
    id_credit_line integer not null,
    foreign key(id_customer) references customers(id_customer),
    foreign key(id_credit_line) references credit_lines(id_credit_line),
    unique(id_customer)
);

drop table if exists payments;
create table payments (
    id_payment integer not null primary key autoincrement,
    id_credit_line integer not null,
    id_user integer not null,
    timestamp text not null default now,
    amount real not null,
    note text,
    foreign key(id_user) references users(id_user),
    foreign key(id_credit_line) references credit_lines(id_credit_line)
);

drop table if exists sponsors;
create table sponsors (
    id_sponsor integer not null primary key autoincrement,
    organization text not null,
    first_name text not null,
    last_name text not null
);

drop table if exists sponsors_addresses;
create table sponsors_addresses (
    id_sponsor_address integer not null primary key autoincrement,
    id_sponsor integer not null,
    id_address integer not null,
    foreign key(id_sponsor) references sponsors(id_sponsor),
    foreign key(id_address) references addresses(id_address)
);

drop table if exists sponsors_contacts;
create table sponsors_contacts (
    id_sponsor_contact integer not null primary key autoincrement,
    id_sponsor integer not null,
    id_contact integer not null,
    foreign key(id_sponsor) references sponsors(id_sponsor),
    foreign key(id_contact) references contacts(id_contact)
);

drop table if exists sponsorships;
create table sponsorships (
    id_sponsorship integer not null primary key autoincrement,
    id_sponsor integer not null,
    id_invoice integer not null,
    timestamp text not null,
    total real not null,
    sponsored real not null,
    paid integer not null,
    paid_date text,
    foreign key(id_sponsor) references sponsors(id_sponsor),
    foreign key(id_invoice) references invoices(id_invoice),
    unique(id_invoice)
);

drop table if exists expenses;
create table expenses (
    id_expense integer not null primary key autoincrement,
    id_user integer not null,
    timestamp text not null default now,
    reason text,
    amount real not null,
    include_in_balance integer default 0,
    foreign key(id_user) references users(id_user)
);

drop table if exists deposits;
create table deposits (
    id_deposit integer not null primary key autoincrement,
    id_user integer not null,
    timestamp text not null default now,
    reason text,
    amount real not null,
    include_in_balance integer default 0,
    foreign key(id_user) references users(id_user)
);

drop table if exists suppliers;
create table suppliers (
    id_supplier integer not null primary key autoincrement,
    organization text,
    first_name text,
    last_name text
);

drop table if exists suppliers_contacts;
create table suppliers_contacts (
    id_supplier_contact integer not null primary key autoincrement,
    id_supplier integer not null,
    id_contact integer not null,
    foreign key(id_supplier) references suppliers(id_supplier),
    foreign key(id_contact) references contacts(id_contact),
    unique(id_supplier, id_contact)
);

drop table if exists suppliers_products;
create table suppliers_products (
    id_supplier_product integer not null primary key autoincrement,
    id_supplier integer not null,
    id_product integer not null,
    foreign key(id_supplier) references suppliers(id_supplier),
    foreign key(id_product) references products(id_product),
    unique(id_supplier, id_product)
);

drop table if exists credit_notes;
create table credit_notes (
    id_credit_note integer not null primary key autoincrement,
    id_supplier integer not null,
    timestamp text not null default now,
    reference text,
    amount real not null,
    reason text,
    redeemed integer default 0,
    redeemed_date text,
    foreign key(id_supplier) references suppliers(id_supplier)
);

drop table if exists orders;
create table orders (
    id_order integer not null primary key autoincrement,
    id_supplier int not null,
    timestamp text not null default now,
    reference text,
    amount real not null,
    paid int not null default 0,
    paid_date,
    foreign key(id_supplier) references suppliers(id_supplier)
);

drop table if exists balances;
create table balances (
    id_balance integer not null primary key autoincrement,
    id_user integer not null,
    timestamp text not null default now,
    start real not null,
    cash_sales real not null,
    credit_sales real not null,
    refunds real not null,
    payments real not null,
    expenses real not null,
    deposits real not null,
    next_start real not null,
    balance real not null,
    difference real not null,
    notes text,
    foreign key(id_user) references users(id_user)
);

/********************************************************************************************/

/******************************** Populating Initial Data ***********************************/

insert into privileges(id_privilege, module, submodule, key, name) values
(1,  'Admin', 'Usuario',          'gf.user-view',                'Ver Usuarios'),
(2,  'Admin', 'Usuario',          'gf.user-add',                 'Agregar Usuarios'),
(3,  'Admin', 'Usuario',          'gf.user-update',              'Actualizar Usuarios'),
(4,  'Admin', 'Usuario',          'gf.user-remove',              'Remover Usuarios'),
(5,  'Admin', 'Usuario',          'gf.user-password-reset',      'Reestablecer contraseña'),
(76, 'Admin', 'Usuario',          'pnc.user-notification',       'Notificaciones de Usuario'),
(6,  'Admin', 'Grupo',            'gf.group-view',               'Ver Grupos'),
(7,  'Admin', 'Grupo',            'gf.group-add',                'Agregar Grupos'),
(8,  'Admin', 'Grupo',            'gf.group-update',             'Actualizar Grupos'),
(9,  'Admin', 'Grupo',            'gf.group-remove',             'Remover Grupos'),
(10, 'Admin', 'Grupo',            'gf.group-subscription',       'Manejar Subscripciones'),
(11, 'Admin', 'Privilegio',       'gf.priv-view',                'Ver Privilegios'),
(12, 'Admin', 'Privilegio',       'gf.priv-grant',               'Conceder/Revocar Privilegios'),
(13, 'Data',  'Notificacion',     'pnc.notification-view',       'Ver Notificaciones'),
(14, 'Data',  'Notificacion',     'pnc.notification-remove',     'Remover Notificaciones'),
(15, 'Data',  'Subscripcion',     'pnc.subscription-view',       'Ver Subscripciones'),
(16, 'Data',  'Subscripcion',     'pnc.subscription-add',        'Agregar Subscripcion'),
(17, 'Data',  'Subscripcion',     'pnc.subscription-remove',     'Remover Subscription'),
(18, 'Data',  'Producto',         'pnc.product-view',            'Ver Productos'),
(19, 'Data',  'Producto',         'pnc.product-add',             'Agregar Products'),
(20, 'Data',  'Producto',         'pnc.product-update',          'Acutalizar Productos'),
(21, 'Data',  'Producto',         'pnc.product-remove',          'Remover Productos'),
(22, 'Data',  'Factura',          'pnc.invoice-view',            'Ver Factura'),
(23, 'Data',  'Factura',          'pnc.invoice-sell',            'Vender Factura'),
(24, 'Data',  'Factura',          'pnc.invoice-invalidate',      'Invalidar Factura'),
(25, 'Data',  'Factura',          'pnc.invoice-invalidate-undo', 'Deshacer Invalidacion'),
(26, 'Data',  'Factura',          'pnc.invoice-refund',          'Hacer Devolucion'),
(27, 'Data',  'Factura',          'pnc.invoice-refund-undo',     'Deshacer Invalidacion'),
(28, 'Data',  'Inventario',       'pnc.inventory-view',          'Ver Inventario'),
(29, 'Data',  'Inventario',       'pnc.inventory-add',           'Agregar Inventario'),
(30, 'Data',  'Inventario',       'pnc.inventory-update',        'Actualizar Inventario'),
(31, 'Data',  'Inventario',       'pnc.inventory-remove',        'Remover Inventario'),
(32, 'Data',  'Balance',          'pnc.balance-view',            'Ver Balances'),
(33, 'Data',  'Balance',          'pnc.balance-generate',        'Generar Balances'),
(34, 'Data',  'Balance',          'pnc.balance-add',             'Agregar Balances'),
(35, 'Data',  'Balance',          'pnc.balance-update',          'Actualizar Balances'),
(36, 'Data',  'Balance',          'pnc.balance-remove',          'Remover Balances'),
(37, 'Data',  'Linea de Credito', 'pnc.credit-line-view',        'Ver Lineas de Credito'),
(38, 'Data',  'Linea de Credito', 'pnc.credit-line-add',         'Agregar Lineas de Credito'),
(39, 'Data',  'Linea de Credito', 'pnc.credit-line-update',      'Actualizar Lineas de Credito'),
(40, 'Data',  'Linea de Credito', 'pnc.credit-line-remove',      'Remover Lineas de Credito'),
(41, 'Data',  'Linea de Credito', 'pnc.credit-line-invoice',     'Manejar Facturas'),
(42, 'Data',  'Linea de Credito', 'pnc.credit-line-payment',     'Manejar Pagos'),
(43, 'Data',  'Cliente',          'pnc.customer-view',           'Ver Clientes'),
(44, 'Data',  'Cliente',          'pnc.customer-add',            'Agregar Clientes'),
(45, 'Data',  'Cliente',          'pnc.customer-update',         'Actualizar Clientes'),
(46, 'Data',  'Cliente',          'pnc.customer-remove',         'Remover Clientes'),
(47, 'Data',  'Suplidor',         'pnc.supplier-view',           'Ver Suplidores'),
(48, 'Data',  'Suplidor',         'pnc.supplier-add',            'Agregar Suplidores'),
(49, 'Data',  'Suplidor',         'pnc.supplier-update',         'Actualizar Suplidores'),
(50, 'Data',  'Suplidor',         'pnc.supplier-remove',         'Remover Suplidores'),
(51, 'Data',  'Suplidor',         'pnc.supplier-order',          'Manejar Ordenes'),
(52, 'Data',  'Suplidor',         'pnc.supplier-credit-note',    'Manejar Notas de Credito'),
(53, 'Data',  'Suplidor',         'pnc.supplier-product',        'Manejar Productos'),
(54, 'Data',  'Ordenes',          'pnc.order-view',              'Ver Ordenes'),
(55, 'Data',  'Ordenes',          'pnc.order-add',               'Agregar Ordenes'),
(56, 'Data',  'Ordenes',          'pnc.order-update',            'Actualizar Ordenes'),
(57, 'Data',  'Ordenes',          'pnc.order-remove',            'Remover Ordenes'),
(58, 'Data',  'Notas de Credito', 'pnc.credit-note-view',        'Ver Notas de Credito'),
(59, 'Data',  'Notas de Credito', 'pnc.credit-note-add',         'Agregar Notas de Credito'),
(60, 'Data',  'Notas de Credito', 'pnc.credit-note-update',      'Actualizar Notas de Credito'),
(61, 'Data',  'Notas de Credito', 'pnc.credit-note-remove',      'Remover Notas de Credito'),
(62, 'Data',  'Patrocinadores',   'pnc.sponsor-view',            'Ver Patrocinadores'),
(63, 'Data',  'Patrocinadores',   'pnc.sponsor-add',             'Agregar Patrocinadores'),
(64, 'Data',  'Patrocinadores',   'pnc.sponsor-update',          'Actualizar Patrocinadores'),
(65, 'Data',  'Patrocinadores',   'pnc.sponsor-remove',          'Remover Patrocinadores'),
(66, 'Data',  'Patrocinadores',   'pnc.sponsor-sponsorship',     'Manejar Patrocinios'),
(67, 'Data',  'Gastos',           'pnc.expense-view',            'Ver Gastos'),
(68, 'Data',  'Gastos',           'pnc.expense-add',             'Agregar Gastos'),
(69, 'Data',  'Gastos',           'pnc.expense-update',          'Actualizar Gastos'),
(70, 'Data',  'Gastos',           'pnc.expense-remove',          'Remover Gastos'),
(71, 'Data',  'Depositos',        'pnc.deposit-view',            'Ver Depositos'),
(72, 'Data',  'Depositos',        'pnc.deposit-add',             'Agregar Depositos'),
(73, 'Data',  'Depositos',        'pnc.deposit-update',          'Actualizar Depositos'),
(74, 'Data',  'Depositos',        'pnc.deposit-remove',          'Remover Depositos'),
(75, 'Data',  'Reportes',         'pnc.report-generate',         'Generar Reportes');


insert into users(username, password, first_name, last_name, enabled) 
values('admin', '$2a$10$buHZKhISQEhIQHQpiQSqvuDDzmipZYgM4btW9dP20L16wcCZwFXcu', 'Administrador', '', 1);
/* pasword: "admin" (no quotes). Hashed by bcrypt. */

insert into groups(name, description)
values('admin', 'Administrative Users');

insert into users_groups(id_user, id_group)
values(
    (select id_user from users where username = 'admin'),
    (select id_group from groups where name = 'admin')
);

insert into grants(id_group, id_privilege)
select (select id_group from groups where name = 'admin'), id_privilege from privileges order by id_privilege;

insert into privilege_dependencies(id_parent, id_child) values 
(1, 2), (1, 3), (1, 4), (1, 5), (1, 76),                                        -- users
(6, 7), (6, 8), (6, 9), (6, 10), (1, 10), (11, 10),                             -- groups
(11, 12),                                                                       -- privileges
(18, 13), (13, 14),                                                             -- notifications
(15, 16), (15, 17),                                                             -- subscriptions
(18, 19), (18, 20), (18, 21),                                                   -- products
(22, 23), (22, 24), (22, 25), (22, 26), (22, 27), (18, 23), (43, 23),           -- invoices
(28, 29), (28, 30), (28, 31),                                                   -- inventory
(32, 33), (32, 34), (32, 35), (32, 36),                                         -- balances
(37, 38), (37, 39), (37, 40), (37, 41), (37, 42), (22, 41),                     -- credit lines
(43, 44), (43, 45), (43, 46),                                                   -- customers
(47, 48), (47, 49), (47, 50), (47, 51), (47, 52), (47, 53), (54, 51), (18, 53), -- suppliers
(54, 55), (54, 56), (54, 57),                                                   -- orders
(58, 59), (58, 60), (58, 61),                                                   -- credit notes
(62, 63), (62, 64), (62, 65), (62, 66),                                         -- sponsors
(67, 68), (67, 69), (67, 70),                                                   -- expenses
(71, 72), (71, 73), (71, 74);                                                   -- deposits

/********************************************************************************************/

PRAGMA foreign_keys = ON;
