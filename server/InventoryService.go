package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	"gxs3/panacea/model"
	"net/http"
	"strconv"
	"time"
)

const (
	INVENTORY_COLLECTION = "/inventory" //Inventory service global route

	PRIV_INVENTORY_VIEW   = "pnc.inventory-view"   // Inventory view privilege
	PRIV_INVENTORY_ADD    = "pnc.inventory-add"    // Inventory add privilege
	PRIV_INVENTORY_UPDATE = "pnc.inventory-update" // Inventory view privilege
	PRIV_INVENTORY_REMOVE = "pnc.inventory-remove" // Inventory remove privilege

	REL_INVENTORY_COLLECTION = "/rels/pnc.inventory-collection" // Rel for link that retrieves inventory collection
	REL_INVENTORY_ADD        = "/rels/pnc.inventory-add"        // Rel for link that adds an inventory item
	REL_INVENTORY_UPDATE     = "/rels/pnc.inventory-update"     // Rel for link that updates an inventory item
	REL_INVENTORY_REMOVE     = "/rels/pnc.inventory-remove"     // Rel for link that removes an inventory item

	PRODUCT = "product" // Identifier to get product values from request queries
)

/*
 Service implementation for the Inventory model
*/
type InventoryService struct {
	*svr.AuthModelService
	dao *pd.InventoryDao
}

/*
 Creates a new inventory serivce and sets up internal routing
*/
func NewInventoryService(router *mux.Router, store *sessions.CookieStore) *InventoryService {
	var (
		is        = InventoryService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(INVENTORY_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(is.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(INVENTORY_COLLECTION)).HandlerFunc(is.getAll)
	subrouter.Methods(svr.GET).Queries(PRODUCT, "").HandlerFunc(is.getByProduct)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(INVENTORY_COLLECTION)).HandlerFunc(is.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(is.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(is.remove)

	return &is
}

/*
 Initializes the service
*/
func (is *InventoryService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	is.dao, err = pd.NewInventoryDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVENTORY_COLLECTION, svr.GET, INVENTORY_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_INVENTORY_ADD, svr.POST, INVENTORY_COLLECTION})
}

/*
 Retrieves an inventory item identified by a provided ID
 Method: GET - URI: /inventory/{id}
*/
func (is *InventoryService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return is.dao.Get(id) }
	is.Get(writer, req, PRIV_INVENTORY_VIEW, get, encodeInventory)
}

/*
 Retrieves all available inventory items
 Method: GET - URI: /inventory
*/
func (is *InventoryService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var inventory, err = is.dao.GetAll()
		return is.toModels(inventory), err
	}

	is.GetCollection(writer, req, PRIV_INVENTORY_VIEW, getAll, is.Base().EncodeModels(encodeInventory))
}

/*
 Retrieves all inventory items corresponding to a product
 Method: GET - URI: /inventory?product={id-product}
*/
func (is *InventoryService) getByProduct(writer http.ResponseWriter, req *http.Request) {
	var product, err = strconv.ParseUint(req.FormValue(PRODUCT), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the product id argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByProduct = func() ([]gm.Model, error) {
		var inventory, err = is.dao.GetByProduct(uint(product))
		return is.toModels(inventory), err
	}

	is.GetCollection(writer, req, PRIV_INVENTORY_VIEW, getByProduct, is.Base().EncodeModels(encodeInventory))
}

/*
 Adds a new inventory item to the collection. Inventory item to add is expected in the request payload.
 Method: POST - URI: /inventory
*/
func (is *InventoryService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var inventory = m.(*model.Inventory)
		return is.dao.Add(inventory)
	}

	is.Add(writer, req, PRIV_INVENTORY_ADD, add, decodeInventory, encodeInventory)
}

/*
 Updates an existing inventory item. Inventory item to update is expected in the request payload.
 Method: PUT - URI: /inventory/{id}
*/
func (is *InventoryService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var inventory = m.(*model.Inventory)
		return is.dao.Update(inventory)
	}

	is.Update(writer, req, PRIV_INVENTORY_UPDATE, update, decodeInventory, encodeInventory)
}

/*
 Removes an existing inventory item from the collection.
 Method: DELETE - URI: /inventory/{id}
*/
func (is *InventoryService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return is.dao.Remove(id) }
	is.Remove(writer, req, PRIV_INVENTORY_REMOVE, remove)
}

/*
 Converts a slice of inventory objects to type model
*/
func (is *InventoryService) toModels(inventory []*model.Inventory) []gm.Model {
	var models = make([]gm.Model, len(inventory))

	for cont, i := range inventory {
		models[cont] = i
	}

	return models
}

/*
 Encodes an inventory item model to a serialized representation for transport. Adds rest links.
*/
func encodeInventory(m gm.Model) ([]byte, error) {
	var inventory, tok = m.(*model.Inventory)
	var expiration *string

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type Inventory")
	}

	if inventory == nil {
		return nil, nil
	}

	if inventory.Expiration() != nil {
		tmp := inventory.Expiration().Format(gd.DATE_FORMAT)
		expiration = &tmp
	}

	var model = struct {
		Id         uint
		IdProduct  uint
		Quantity   float32
		Remaining  float32
		Entered    string
		Expiration *string
		Cost       float32
	}{
		inventory.Id(),
		inventory.IdProduct(),
		inventory.Quantity(),
		inventory.Remaining(),
		inventory.Entered().Format(gd.DATE_FORMAT),
		expiration,
		inventory.Cost(),
	}

	var resource = fmt.Sprintf("%s/%d", INVENTORY_COLLECTION, inventory.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_INVENTORY_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_INVENTORY_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a serialized representation of an inventory item into a model object.
*/
func decodeInventory(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 7)
	var id, idProduct, quantity, remaining, cost float64
	var enteredStr, expirationStr string
	var expiration *time.Time

	id, ok[0] = job["Id"].(float64)
	idProduct, ok[1] = job["IdProduct"].(float64)
	quantity, ok[2] = job["Quantity"].(float64)
	remaining, ok[3] = job["Remaining"].(float64)
	enteredStr, ok[4] = job["Entered"].(string)
	cost, ok[5] = job["Cost"].(float64)

	if job["Expiration"] == nil {
		ok[6] = true
	} else {
		expirationStr, ok[6] = job["Expiration"].(string)
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	entered, err := time.Parse(gd.DATE_TIME_FORMAT, enteredStr)

	if err != nil {
		return nil, err
	}

	if len(expirationStr) > 0 {
		tmpExp, err := time.Parse(gd.DATE_FORMAT, expirationStr)

		if err != nil {
			return nil, err
		}

		expiration = &tmpExp
	}

	return model.NewInventory(
		uint(id),
		uint(idProduct),
		float32(quantity),
		float32(remaining),
		entered,
		expiration,
		float32(cost),
	)
}
