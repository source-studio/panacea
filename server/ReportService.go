package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	REPORT_COLLECTION    = "/report"             // Defines the report global route
	PRIV_REPORT_GENERATE = "pnc.report-generate" // Report generation privilege key

	REL_REPORT_STATE                = "/rels/pnc.report-state"                // Rel for link to generate the state report
	REL_REPORT_ACTIVITY             = "/rels/pnc.report-activity"             // Rel for link to generate the activity report
	REL_REPORT_ACTIVITY_MONTH       = "/rels/pnc.report-activity-month"       // Rel for link to generate the monthly activity report
	REL_REPORT_ACTIVITY_YEAR        = "/rels/pnc.report-activity-year"        // Rel for link to generate the yearly activity report
	REL_REPORT_ACTIVITY_INTERVAL    = "/rels/pnc.report-activity-interval"    // Rel for link to retrieve the interval of available activity
	REL_REPORT_INVENTORY            = "/rels/pnc.report-inventory"            // Rel for link to generate the general inventory report
	REL_REPORT_INVENTORY_SUPPLIER   = "/rels/pnc.report-inventory-supplier"   // Rel for link to generate the supplier inventory report
	REL_RPEORT_INVENTORY_EXISTENCE  = "/rels/pnc.report-inventory-existence"  // Rel for link to generate inventory by minimum threshold
	REL_REPORT_INVENTORY_EXPIRATION = "/rels/pnc.report-inventory-expiration" // Rel for link to generate inventory by expiration

	STATE                = "state"
	ACTIVITY             = "activity"
	ACTIVITY_MONTH       = "activity-month"
	ACTIVITY_YEAR        = "activity-year"
	ACTIVITY_INTERVAL    = "activity-interval"
	INVENTORY            = "inventory"
	INVENTORY_SUPPLIER   = "inventory-supplier"
	INVENTORY_EXISTENCE  = "inventory-existence"
	INVENTORY_EXPIRATION = "inventory-expiration"
)

/*
 Service implementation for reporting data. This service will provide the user the required functionality to generate
 reports on the client side. Like other services, the data sent back will be bare bones. The client will have the
 responsability of formatting and assembling reports, and doing some last minute totalling and/or aggregation.
*/
type ReportService struct {
	*svr.AuthModelService
	dao *pd.ReportDao // Data access object to perform database transactions
}

/*
 Creates a new report service
*/
func NewReportService(router *mux.Router, store *sessions.CookieStore) *ReportService {
	var (
		rs        = ReportService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(REPORT_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path("/state").HandlerFunc(rs.generateState)
	subrouter.Methods(svr.GET).Path("/activity").Queries(FROM, "").Queries(TO, "").HandlerFunc(rs.generateActivity)
	subrouter.Methods(svr.GET).Path("/activity/month").Queries(FROM, "").Queries(TO, "").HandlerFunc(rs.generateActivityMonth)
	subrouter.Methods(svr.GET).Path("/activity/year").Queries(FROM, "").Queries(TO, "").HandlerFunc(rs.generateActivityYear)
	subrouter.Methods(svr.GET).Path("/activity/interval").HandlerFunc(rs.getInterval)
	subrouter.Methods(svr.GET).Path("/inventory").Queries("supplier", "").HandlerFunc(rs.generateInventorySupplier)
	subrouter.Methods(svr.GET).Path("/inventory/existence").HandlerFunc(rs.generateInventoryExistence)
	subrouter.Methods(svr.GET).Path("/inventory/expiration").Queries("expired", "").Queries("remaining", "").HandlerFunc(rs.generateInventoryExpiration)
	subrouter.Methods(svr.GET).Path("/inventory").HandlerFunc(rs.generateInventory)

	return &rs
}

/*
 Initializes the report services after creation
*/
func (rs *ReportService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	rs.dao, err = pd.NewReportDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_STATE, svr.GET, REPORT_COLLECTION + "/state"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_ACTIVITY, svr.GET, REPORT_COLLECTION + "/activity?from={from}&to={to}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_ACTIVITY_MONTH, svr.GET, REPORT_COLLECTION + "/activity/month?from={from}&to={to}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_ACTIVITY_YEAR, svr.GET, REPORT_COLLECTION + "/activity/year?from={from}&to={to}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_ACTIVITY_INTERVAL, svr.GET, REPORT_COLLECTION + "/activity/interval"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_INVENTORY, svr.GET, REPORT_COLLECTION + "/inventory"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_INVENTORY_SUPPLIER, svr.GET, REPORT_COLLECTION + "/inventory?supplier={supplier}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_RPEORT_INVENTORY_EXISTENCE, svr.GET, REPORT_COLLECTION + "/inventory/existence"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_REPORT_INVENTORY_EXPIRATION, svr.GET, REPORT_COLLECTION + "/inventory/expiration?expired={expired}&remaining={remaining}"})
}

/*
 Generates the state report. Provides information on the current state value recorded by the application.
*/
func (rs *ReportService) generateState(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)
	if !ok {
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateState()

		if err != nil {
			return nil, err
		}

		report.SetReportType(STATE)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the activity report. Provides a summary of the activity that happened within a date interval.
*/
func (rs *ReportService) generateActivity(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateActivity(from, to)

		if err != nil {
			return nil, err
		}

		report.SetReportType(ACTIVITY)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the monthly activity report. Provides a summary of the acitivy that happened within a month, grouped by day
 of month.
*/
func (rs *ReportService) generateActivityMonth(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateActivityMonth(from, to)

		if err != nil {
			return nil, err
		}

		report.SetReportType(ACTIVITY_MONTH)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the yearly activity report. Provides a summary of the activity that happend within a year,
 grouped by month.
*/
func (rs *ReportService) generateActivityYear(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateActivityYear(from, to)

		if err != nil {
			return nil, err
		}

		report.SetReportType(ACTIVITY_YEAR)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Retrieves the date interval in which activity has been recorded by the application
*/
func (rs *ReportService) getInterval(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GetInterval()

		if err != nil {
			return nil, err
		}

		report.SetReportType(ACTIVITY_INTERVAL)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the general inventory report. Provides a full listing of all products with existing inventory.
*/
func (rs *ReportService) generateInventory(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateInventory()

		if err != nil {
			return nil, err
		}

		report.SetReportType(INVENTORY)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the supplier inventory report. Provides a listing of all the products which are provided by a
 supplier and their inventory details
*/
func (rs *ReportService) generateInventorySupplier(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var supplier, err = strconv.ParseUint(req.FormValue(SUPPLIER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse supplier argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateInventorySupplier(uint(supplier))

		if err != nil {
			return nil, err
		}

		report.SetReportType(INVENTORY_SUPPLIER)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the inventory existence report. Provides a listing of all products, and their inventory details.
*/
func (rs *ReportService) generateInventoryExistence(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)

	if !ok {
		return
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateInventoryExistence()

		if err != nil {
			return nil, err
		}

		report.SetReportType(INVENTORY_EXISTENCE)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Generates the inventory expiration report. Provides a listing of all products expired, or that will expire in a given
 number of days. Depends on the provided arguments.
*/
func (rs *ReportService) generateInventoryExpiration(writer http.ResponseWriter, req *http.Request) {
	var idUser, ok = rs.getUser(writer, req)
	var days uint64

	if !ok {
		return
	}

	var expired, err = strconv.ParseBool(req.FormValue("expired"))

	if err != nil {
		http.Error(writer, "Error attempting to parse expired argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	if len(req.FormValue("remaining")) > 0 {
		days, err = strconv.ParseUint(req.FormValue("remaining"), 10, 32)

		if err != nil {
			http.Error(writer, "Error attempting to parse days argument: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	var generate = func() (gm.Model, error) {
		var report, err = rs.dao.GenerateInventoryExpiration(expired, uint(days))

		if err != nil {
			return nil, err
		}

		report.SetReportType(INVENTORY_EXPIRATION)
		report.SetIdUser(idUser)

		return report, nil
	}
	rs.GetSingle(writer, req, PRIV_REPORT_GENERATE, generate, encodeReport)
}

/*
 Retrieves the user ID from the session info
*/
func (rs *ReportService) getUser(writer http.ResponseWriter, req *http.Request) (uint, bool) {
	session, err := rs.Base().Store().Get(req, svr.GODFATHER)

	if err != nil {
		http.Error(writer, "Error attempting to get session information: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		log.Println("Error attempting to get session information: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		return 0, false
	}

	idUser, ok := session.Values[svr.USER_ID].(uint)

	if !ok {
		http.Error(writer, "Error attempting to get the user ID from the session ", svr.INTERNAL_SERVER_ERROR)
		log.Println("Error attempting to get session information: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		return 0, false
	}

	return idUser, true
}

/*
Encodes a report into a serialized representation for transport
*/
func encodeReport(m gm.Model) ([]byte, error) {
	var report, tok = m.(*pm.Report)

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type Report")
	}

	if report == nil {
		return nil, nil
	}

	var model = struct {
		Id         uint
		IdUser     uint
		Timestamp  string
		ReportType string
		Argments   map[string]string
		Data       map[string]interface{}
	}{
		report.Id(),
		report.IdUser(),
		report.Timestamp().Format(gd.DATE_TIME_FORMAT),
		report.ReportType(),
		report.Arguments(),
		report.Data(),
	}

	var resource = fmt.Sprintf("%s/%s", REPORT_COLLECTION, report.ReportType())
	var links = make([]svr.Link, 0)
	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}
