package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"net/http"
)

const (
	CUSTOMER_COLLECTION = "/customer" //Customer service global route

	PRIV_CUSTOMER_VIEW   = "pnc.customer-view"   // Privilege to view customers
	PRIV_CUSTOMER_ADD    = "pnc.customer-add"    // Privilege to add customers
	PRIV_CUSTOMER_UPDATE = "pnc.customer-update" // Privilege to update customers
	PRIV_CUSTOMER_REMOVE = "pnc.customer-remove" // Privilege to remove customers

	REL_CUSTOMER                 = "/rels/pnc.customer"                 // Rel for link that retrieves a single customer
	REL_CUSTOMER_COLLECTION      = "/rels/pnc.customer-collection"      // Rel for link that retrieves the customer collection
	REL_CUSTOMER_SEARCH          = "/rels/pnc.customer-search"          // Rel for link that searches customers
	REL_CUSTOMER_ADD             = "/rels/pnc.customer-add"             // Rel for link that adds a customer
	REL_CUSTOMER_UPDATE          = "/rels/pnc.customer-update"          // Rel for link that updates a customer
	REL_CUSTOMER_REMOVE          = "/rels/pnc.customer-remove"          // Rel for link that removes a customer
	REL_CUSTOMER_CREDIT_LINE     = "/rels/pnc.customer-credit-line"     // Rel for link that retrieves the credit line for a customer
	REL_CUSTOMER_CREDIT_LINE_ADD = "/rels/pnc.customer-credit-line-add" // Rel for link that adds a credit-line for a customer
	REL_CUSTOMER_INVOICES        = "/rels/pnc.customer-invoices"        // Rel for link that retrieves all invoices associated with a customer
)

/*
 Service implementation for the Customer model
*/
type CustomerService struct {
	*svr.AuthModelService
	dao *pd.CustomerDao
}

/*
 Crates a new customer service
*/
func NewCustomerService(router *mux.Router, store *sessions.CookieStore) *CustomerService {
	var (
		cs        = CustomerService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(CUSTOMER_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(cs.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(CUSTOMER_COLLECTION)).HandlerFunc(cs.getAll)
	subrouter.Methods(svr.GET).Queries(NAME, "").HandlerFunc(cs.search)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(CUSTOMER_COLLECTION)).HandlerFunc(cs.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(cs.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(cs.remove)

	return &cs
}

/*
 Service Initialization
*/
func (cs *CustomerService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	cs.dao, err = pd.NewCustomerDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_CUSTOMER, svr.GET, CUSTOMER_COLLECTION + "/{id}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_CUSTOMER_COLLECTION, svr.GET, CUSTOMER_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_CUSTOMER_SEARCH, svr.GET, CUSTOMER_COLLECTION + "?name={name}"})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_CUSTOMER_ADD, svr.POST, CUSTOMER_COLLECTION})
}

/*
 Retrieves a customer identified by the provided ID
 Method: GET - URI: /customer/{id}
*/
func (cs *CustomerService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return cs.dao.Get(id) }
	cs.Get(writer, req, PRIV_CUSTOMER_VIEW, get, encodeCustomer)
}

/*
 Retrieves all avaialable customers
 Method: GET - URI: /customer
*/
func (cs *CustomerService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var customers, err = cs.dao.GetAll()
		return cs.toModels(customers), err
	}

	cs.GetCollection(writer, req, PRIV_CUSTOMER_VIEW, getAll, cs.Base().EncodeModels(encodeCustomer))
}

/*
 Searches for a customer who's name mathces the search criteria
*/
func (cs *CustomerService) search(writer http.ResponseWriter, req *http.Request) {
	var name = req.FormValue(NAME)

	if len(name) < 1 {
		http.Error(writer, "Empty value for the name criteria", svr.BAD_REQUEST)
		return
	}

	var search = func() ([]gm.Model, error) {
		var customers, err = cs.dao.Search(name)
		return cs.toModels(customers), err
	}

	cs.GetCollection(writer, req, PRIV_CUSTOMER_VIEW, search, cs.Base().EncodeModels(encodeCustomer))
}

/*
 Adds a new customer to the collection. The customer is expected in the request payload
 Method: POST - URI: /customer
*/
func (cs *CustomerService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var customer = m.(*pm.Customer)
		return cs.dao.Add(customer)
	}

	cs.Add(writer, req, PRIV_CUSTOMER_ADD, add, decodeCustomer, encodeCustomer)
}

/*
 Updates an existing customer. the customer is expected in the request payload
 Method: PUT - URI: /customer/{id}
*/
func (cs *CustomerService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var customer = m.(*pm.Customer)
		return cs.dao.Update(customer)
	}

	cs.Update(writer, req, PRIV_CUSTOMER_UPDATE, update, decodeCustomer, encodeCustomer)
}

/*
 Removes a customer from the collection
 Method: DELETE - URI: /customer/{id}
*/
func (cs *CustomerService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return cs.dao.Remove(id) }
	cs.Remove(writer, req, PRIV_CUSTOMER_REMOVE, remove)
}

/*
 Converts a collection of customer objects to Model types
*/
func (cs *CustomerService) toModels(customers []*pm.Customer) []gm.Model {
	var models = make([]gm.Model, len(customers))

	for cont, c := range customers {
		models[cont] = c
	}

	return models
}

/*
 Encodes a customer model to a serialized representation for transport. Adds rest links.
*/
func encodeCustomer(m gm.Model) ([]byte, error) {
	var customer, tok = m.(*pm.Customer)

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Customer")
	}

	if customer == nil {
		return nil, nil
	}

	var address map[string]interface{}
	var ca = customer.Address()

	if ca != nil {
		address = map[string]interface{}{
			"Id":       ca.Id(),
			"Street1":  ca.Street1(),
			"Street2":  ca.Street2(),
			"City":     ca.City(),
			"Province": ca.Province(),
			"Country":  ca.Country(),
		}
	}

	var contacts = make([]map[string]interface{}, len(customer.Contacts()))
	for cont, contact := range customer.Contacts() {
		typeStr, err := pm.FormatContactType(contact.ContactType())

		if err != nil {
			return nil, err
		}

		contacts[cont] = map[string]interface{}{
			"Id":          contact.Id(),
			"Uuid":        contact.Uuid(),
			"ContactType": typeStr,
			"Value":       contact.Value(),
		}
	}

	var model = struct {
		Id        uint
		FirstName string
		LastName  string
		NickName  string
		Address   map[string]interface{}
		Contacts  []map[string]interface{}
	}{
		customer.Id(),
		customer.FirstName(),
		customer.LastName(),
		customer.NickName(),
		address,
		contacts,
	}

	var resource = fmt.Sprintf("%s/%d", CUSTOMER_COLLECTION, customer.Id())
	var creditLineResource = fmt.Sprintf("%s?customer=%d", CREDIT_LINE_COLLECTION, customer.Id())
	var invoiceResource = fmt.Sprintf("%s?customer=%d", INVOICE_COLLECTION, customer.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_CUSTOMER_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_CUSTOMER_REMOVE, svr.DELETE, resource},
		svr.Link{svr.JSON, REL_CUSTOMER_CREDIT_LINE, svr.GET, creditLineResource},
		svr.Link{svr.PLAIN, REL_CUSTOMER_CREDIT_LINE_ADD, svr.POST, creditLineResource},
		svr.Link{svr.JSON, REL_CUSTOMER_INVOICES, svr.GET, invoiceResource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a customer representation into a native model object
*/
func decodeCustomer(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 6)
	var id float64
	var firstName, lastName, nickName, street1, street2, city, province, country, uuid, value, contactTypeRaw string
	var addressRaw, contactJob map[string]interface{}
	var contactsRaw []interface{}

	id, ok[0] = job["Id"].(float64)
	firstName, ok[1] = job["FirstName"].(string)
	lastName, ok[2] = job["LastName"].(string)
	nickName, ok[3] = job["NickName"].(string)
	contactsRaw, ok[5] = job["Contacts"].([]interface{})

	if job["Address"] == nil {
		ok[4] = true
	} else {
		addressRaw, ok[4] = job["Address"].(map[string]interface{})
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	customer, err := pm.NewCustomer(uint(id), firstName, lastName, nickName)

	if err != nil {
		return nil, err
	}

	if addressRaw != nil {
		id, ok[0] = addressRaw["Id"].(float64)
		street1, ok[1] = addressRaw["Street1"].(string)
		city, ok[2] = addressRaw["City"].(string)
		province, ok[3] = addressRaw["Province"].(string)
		country, ok[4] = addressRaw["Country"].(string)

		if addressRaw["Street2"] == nil {
			ok[5] = true
		} else {
			street2, ok[5] = addressRaw["Street2"].(string)
		}

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		address, err := pm.NewAddress(uint(id), street1, street2, city, province, country)

		if err != nil {
			return nil, err
		}

		customer.SetAddress(address)
	}

	ok = make([]bool, 5)

	for _, cr := range contactsRaw {
		contactJob, ok[0] = cr.(map[string]interface{})
		id, ok[1] = contactJob["Id"].(float64)
		uuid, ok[2] = contactJob["Uuid"].(string)
		contactTypeRaw, ok[3] = contactJob["ContactType"].(string)
		value, ok[4] = contactJob["Value"].(string)

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		contactType, err := pm.ParseContactType(contactTypeRaw)

		if err != nil {
			return nil, err
		}

		contact, err := pm.NewContact(uint(id), uuid, contactType, value)
		customer.Add(contact)
	}

	return customer, nil
}
