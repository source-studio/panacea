/*
 Provides service implementations for panacea
*/
package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	"gxs3/panacea/model"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const (
	PRODUCT_COLLECTION = "/product" //Product service global route

	PRIV_PRODUCT_VIEW   = "pnc.product-view"   // Product update privilege
	PRIV_PRODUCT_ADD    = "pnc.product-add"    // Product add privilege
	PRIV_PRODUCT_UPDATE = "pnc.product-update" // Product update privilege
	PRIV_PRODUCT_REMOVE = "pnc.product-remove" // Product remove privilege

	REL_PRODUCT                    = "/rels/pnc.product"                    // Rel for link to retrieve a single product
	REL_PRODUCT_COLLECTION         = "/rels/pnc.product-collection"         // Rel for link to retrieve product collection
	REL_PRODUCT_SEARCH             = "/rels/pnc.product-search"             // Rel for link to search products by brand and name
	REL_PRODUCT_SEARCH_BY_SUPPLIER = "/rels/pnc.product-search-by-supplier" // Rel for link to search products by supplier
	REL_PRODUCT_SEARCH_BRANDS      = "/rels/pnc.product-search-brands"      // Rel for link to search brands
	REL_PRODUCT_ADD                = "/rels/pnc-product-add"                // Rel for link to add a product
	REL_PRODUCT_UPDATE             = "/rels/pnc.product-update"             // Rel for link to update a product
	REL_PRODUCT_REMOVE             = "/rels/pnc.product-remove"             // Rel for link to remove a product
	REL_PRODUCT_INVENTORY          = "/rels/pnc.product-inventory"          // Rel fro link that get product inventory

	BRAND    = "brand"    // Identifier to get brand values from request URIs
	NAME     = "name"     // Identifier to get name values from request URIs
	SUPPLIER = "supplier" // Identifier to get supplier values from request URIs
)

/*
 Service implementation that handles access to the product collection.
*/
type ProductService struct {
	*svr.AuthModelService
	dao *pd.ProductDao
}

/*
 Creates a new product service. Expects the server's request router
*/
func NewProductService(router *mux.Router, store *sessions.CookieStore) *ProductService {
	var (
		ps        = ProductService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(PRODUCT_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(ps.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(PRODUCT_COLLECTION)).HandlerFunc(ps.getAll)
	subrouter.Methods(svr.GET).Queries(SUPPLIER, "").HandlerFunc(ps.getBySupplier)
	subrouter.Methods(svr.GET).Queries(INVOICE, "").HandlerFunc(ps.getByInvoice)
	subrouter.Methods(svr.GET).Queries(BRAND, "").Queries(NAME, "").HandlerFunc(ps.search)
	subrouter.Methods(svr.GET).Queries(BRAND, "").HandlerFunc(ps.searchBrands)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(PRODUCT_COLLECTION)).HandlerFunc(ps.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(ps.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(ps.remove)

	return &ps
}

/*
 Initializes the service
*/
func (ps *ProductService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	ps.dao, err = pd.NewProductDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_PRODUCT, svr.GET, PRODUCT_COLLECTION + "/{id}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_PRODUCT_COLLECTION, svr.GET, PRODUCT_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_PRODUCT_SEARCH, svr.GET, PRODUCT_COLLECTION + "?brand={brand}&name={name}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_PRODUCT_SEARCH_BRANDS, svr.GET, PRODUCT_COLLECTION + "?brand={brand}"})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_PRODUCT_ADD, svr.POST, PRODUCT_COLLECTION})
}

/*
 Retrieves the product identified by the provided ID
 Method: GET - URI: /product/{id}
*/
func (ps *ProductService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return ps.dao.Get(id) }
	ps.Get(writer, req, PRIV_PRODUCT_VIEW, get, encodeProduct)
}

/*
 Retrieves all available products
 Method: GET - URI: /product
*/
func (ps *ProductService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var products, err = ps.dao.GetAll()
		return ps.toModels(products), err
	}

	ps.GetCollection(writer, req, PRIV_PRODUCT_VIEW, getAll, ps.Base().EncodeModels(encodeProduct))
}

/*
 Retrieves all products that are provided by a given supplier
 Method: GET - URI: /product?suplier={id-supplier}
*/
func (ps *ProductService) getBySupplier(writer http.ResponseWriter, req *http.Request) {
	var supplier, err = strconv.ParseUint(req.FormValue(SUPPLIER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse supplier argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getBySupplier = func() ([]gm.Model, error) {
		var products, err = ps.dao.GetBySupplier(uint(supplier))
		return ps.toModels(products), err
	}

	ps.GetCollection(writer, req, PRIV_PRODUCT_VIEW, getBySupplier, ps.Base().EncodeModels(encodeProduct))
}

/*
 Retrieves all products that are referenced in an invoice
 Method: GET - URI: /product?invoice={id-invoice}
*/
func (ps *ProductService) getByInvoice(writer http.ResponseWriter, req *http.Request) {
	var invoice, err = strconv.ParseUint(req.FormValue(INVOICE), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse invoice argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByInvoice = func() ([]gm.Model, error) {
		var products, err = ps.dao.GetByInvoice(uint(invoice))
		return ps.toModels(products), err
	}

	ps.GetCollection(writer, req, PRIV_PRODUCT_VIEW, getByInvoice, ps.Base().EncodeModels(encodeProduct))
}

/*
 Retrieves all products who's brand and name match the provided search criteria
 Method: GET - URI: /product?brand={brand}&name={name}
*/
func (ps *ProductService) search(writer http.ResponseWriter, req *http.Request) {
	var brand = req.FormValue(BRAND)
	var name = req.FormValue(NAME)

	if len(brand) < 1 && len(name) < 1 {
		http.Error(writer, "Error: No value passed in the name or brand argument", svr.BAD_REQUEST)
		return
	}

	var search = func() ([]gm.Model, error) {
		var products, err = ps.dao.Search(name, brand)
		return ps.toModels(products), err
	}

	ps.GetCollection(writer, req, PRIV_PRODUCT_VIEW, search, ps.Base().EncodeModels(encodeProduct))
}

/*
 Retreives all brands that match the provided search criteria. The result is a json array with brand names.
 Method: GET - URI: /product?brand={brand}
*/
func (ps *ProductService) searchBrands(writer http.ResponseWriter, req *http.Request) {
	var hasPriv, err = ps.VerifyPriv(writer, req, PRIV_PRODUCT_VIEW)

	if err != nil {
		http.Error(writer, "Error attempting to verify user privileges: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		log.Println("Error attempting to verify user privileges: " + err.Error())
		return
	}

	if !hasPriv {
		http.Error(writer, "Error: this user does not have the required privilege to perform this transaction", svr.UNAUTHORIZED)
		return
	}

	var brand = req.FormValue(BRAND)

	if len(brand) < 1 {
		http.Error(writer, "Error: no value passed in the brand argument", svr.BAD_REQUEST)
		return
	}

	brands, err := ps.dao.SearchBrands(brand)

	if err != nil && err != gd.NOT_FOUND {
		http.Error(writer, "Error while attempting to retrieve brands: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		log.Println("Error while attempting to retrieve brands: " + err.Error())
		return
	}

	if len(brands) < 1 {
		http.Error(writer, "No brands found", svr.NOT_FOUND)
		return
	}

	var response = []string{"["}

	for _, b := range brands {
		response = append(response, `"`)
		response = append(response, b)
		response = append(response, `", `)
	}

	response[len(response)-1] = `"]`
	writer.Header().Set(svr.CONTENT_TYPE, svr.JSON)
	writer.Write([]byte(strings.Join(response, "")))
}

/*
 Adds a new product to the collection. Product to add is expected in message payload.
 Method: POST - URI: /product
*/
func (ps *ProductService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var product, _ = m.(*model.Product)
		return ps.dao.Add(product)
	}

	ps.Add(writer, req, PRIV_PRODUCT_ADD, add, decodeProduct, encodeProduct)
}

/*
 Updates an existing product. Product to update is expected in message payload.
 Method: PUT - URI: /product/{id}
*/
func (ps *ProductService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var product, _ = m.(*model.Product)
		return ps.dao.Update(product)
	}

	ps.Update(writer, req, PRIV_PRODUCT_UPDATE, update, decodeProduct, encodeProduct)
}

/*
 Removes a product from the collection
 Method: DELETE - URI: /product/{id}
*/
func (ps *ProductService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return ps.dao.Remove(id) }
	ps.Remove(writer, req, PRIV_PRODUCT_REMOVE, remove)
}

/*
 Converts a slice of products into a slice of Models. Used for compatibility with godfather.
*/
func (ps *ProductService) toModels(products []*model.Product) []gm.Model {
	var models = make([]gm.Model, len(products))

	for cont, product := range products {
		models[cont] = product
	}

	return models
}

/*
 Encodes a single project object into its serialized representation for transport. Adds rest links.
*/
func encodeProduct(m gm.Model) ([]byte, error) {
	var product, tok = m.(*model.Product)

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type Product")
	}

	if product == nil {
		return nil, nil
	}

	var model = struct {
		Id        uint
		Brand     string
		Name      string
		BarCode   uint64
		TaxRate   float32
		Price     float32
		Threshold uint
		Minimum   uint
	}{
		product.Id(),
		product.Brand(),
		product.Name(),
		product.BarCode(),
		product.TaxRate(),
		product.Price(),
		product.Threshold(),
		product.Minimum(),
	}

	var resource = fmt.Sprintf("%s/%d", PRODUCT_COLLECTION, product.Id())
	var inventoryResource = fmt.Sprintf("%s?product=%d", INVENTORY_COLLECTION, product.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_PRODUCT_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_PRODUCT_REMOVE, svr.DELETE, resource},
		svr.Link{svr.JSON, REL_PRODUCT_INVENTORY, svr.GET, inventoryResource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a project model object from a serialized represnetation.
*/
func decodeProduct(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 8)
	var id, barCode, taxRate, price, threshold, minimum float64
	var brand, name string

	id, ok[0] = job["Id"].(float64)
	brand, ok[1] = job["Brand"].(string)
	name, ok[2] = job["Name"].(string)
	taxRate, ok[3] = job["TaxRate"].(float64)
	price, ok[4] = job["Price"].(float64)
	minimum, ok[5] = job["Minimum"].(float64)

	if job["BarCode"] == nil {
		ok[6] = true
	} else {
		barCode, ok[6] = job["BarCode"].(float64)
	}

	if job["Threshold"] == nil {
		ok[7] = true
	} else {
		threshold, ok[7] = job["Threshold"].(float64)
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	product, err := model.NewProduct(
		uint(id),
		brand,
		name,
		uint64(barCode),
		float32(taxRate),
		float32(price),
		uint(threshold),
		uint(minimum),
	)

	return product, err
}
