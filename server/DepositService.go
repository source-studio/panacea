package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"net/http"
	"strconv"
	"time"
)

const (
	DEPOSIT_COLLECTION = "/deposit" // Defines the deposit global route

	PRIV_DEPOSIT_VIEW   = "pnc.deposit-view"   // Deposit view privilege
	PRIV_DEPOSIT_ADD    = "pnc.deposit-add"    // Deposit add privilege
	PRIV_DEPOSIT_UPDATE = "pnc.deposit-update" // Deposit update privilege
	PRIV_DEPOSIT_REMOVE = "pnc.deposit-remove" // Deposit remove privilege

	REL_DEPOSIT_COLLECTION = "/rels/pnc.deposit-collection" // Rel for link to retrieve deposit collection
	REL_DEPOSIT_SEARCH     = "/rels/pnc.deposit-search"     // Rel for link to search deposits by date range and optinally user
	REL_DEPOSIT_ADD        = "/rels/pnc.deposit-add"        // Rel for link to add a deposit
	REL_DEPOSIT_UPDATE     = "/rels/pnc.deposit-update"     // Rel for link to update a deposit
	REL_DEPOSIT_REMOVE     = "/rels/pnc.deposit-remove"     // Rel for link to remove a deposit
)

/*
 Service implementation for the deposit model
*/
type DepositService struct {
	*svr.AuthModelService
	dao *pd.DepositDao // Data access object to perform database transactions
}

/*
 Creates a new deposit service
*/
func NewDepositService(router *mux.Router, store *sessions.CookieStore) *DepositService {
	var (
		ds        = DepositService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(DEPOSIT_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(ds.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(DEPOSIT_COLLECTION)).HandlerFunc(ds.getAll)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").Queries(USER, "").HandlerFunc(ds.search)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(DEPOSIT_COLLECTION)).HandlerFunc(ds.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(ds.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(ds.remove)

	return &ds
}

/*
 Initializes the service
*/
func (ds *DepositService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	ds.dao, err = pd.NewDepositDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	var searchUri = fmt.Sprintf("%s?from={from}&to={to}&user={user}", DEPOSIT_COLLECTION)
	svr.RegisterLink(&svr.Link{svr.JSON, REL_DEPOSIT_COLLECTION, svr.GET, DEPOSIT_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_DEPOSIT_SEARCH, svr.GET, searchUri})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_DEPOSIT_ADD, svr.POST, DEPOSIT_COLLECTION})
}

/*
 Retrieves a single deposit object as identified by the given ID
 Method: GET - URI: /deposit/{id}
*/
func (ds *DepositService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return ds.dao.Get(id) }
	ds.Get(writer, req, PRIV_DEPOSIT_VIEW, get, encodeDeposit)
}

/*
 Retrieves all existing deposit objects
 Method: GET - URI: /deposit
*/
func (ds *DepositService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var deposits, err = ds.dao.GetAll()
		return ds.toModels(deposits), err
	}

	ds.GetCollection(writer, req, PRIV_DEPOSIT_VIEW, getAll, ds.Base().EncodeModels(encodeDeposit))
}

/*
 Retrieves all deposits within the given date range
 Method: GET - URI: /deposit?from={from-date-time}&to={to-date-time}
*/
func (ds *DepositService) search(writer http.ResponseWriter, req *http.Request) {
	var user uint64 = 0
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	if len(req.FormValue(USER)) > 0 {
		user, err = strconv.ParseUint(req.FormValue(USER), 10, 32)

		if err != nil {
			http.Error(writer, "Error attempting to parse user argument: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	var search = func() ([]gm.Model, error) {
		var deposits, err = ds.dao.Search(from, to, uint(user))
		return ds.toModels(deposits), err
	}

	ds.GetCollection(writer, req, PRIV_DEPOSIT_VIEW, search, ds.Base().EncodeModels(encodeDeposit))
}

/*
 Adds a new deposit to the collection. Expects the deposit to be serialized in the request body.
 Method: POST - URI: /deposit
*/
func (ds *DepositService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var deposit = m.(*pm.Deposit)
		return ds.dao.Add(deposit)
	}

	ds.Add(writer, req, PRIV_DEPOSIT_ADD, add, decodeDeposit, encodeDeposit)
}

/*
 Updates the detail of an exisitng deposit.
 Method: PUT - URI: /deposit/{id}
*/
func (ds *DepositService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var deposit = m.(*pm.Deposit)
		return ds.dao.Update(deposit)
	}

	ds.Update(writer, req, PRIV_DEPOSIT_UPDATE, update, decodeDeposit, encodeDeposit)
}

/*
 Removes an existing deposit
 Method: DELETE - URI: /deposit/{id}
*/
func (ds *DepositService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return ds.dao.Remove(id) }
	ds.Remove(writer, req, PRIV_DEPOSIT_REMOVE, remove)
}

/*
 Cast a collection of deposit objects to model type
*/
func (ds *DepositService) toModels(deposits []*pm.Deposit) []gm.Model {
	var models = make([]gm.Model, len(deposits))

	for cont, deposit := range deposits {
		models[cont] = deposit
	}

	return models
}

/*
 Encodes an deposit object to a serialized json representation for transport
*/
func encodeDeposit(m gm.Model) ([]byte, error) {
	var deposit, tok = m.(*pm.Deposit)

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Deposit.")
	}

	if deposit == nil {
		return nil, nil
	}

	var model = struct {
		Id               uint
		IdUser           uint
		Timestamp        string
		Reason           string
		Amount           float32
		IncludeInBalance bool
	}{
		deposit.Id(),
		deposit.IdUser(),
		deposit.Timestamp().Format(gd.DATE_TIME_FORMAT),
		deposit.Reason(),
		deposit.Amount(),
		deposit.IncludeInBalance(),
	}

	var resource = fmt.Sprintf("%s/%d", DEPOSIT_COLLECTION, deposit.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_DEPOSIT_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_DEPOSIT_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Deocdes an deposit representation into a native model object
*/
func decodeDeposit(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 6)
	var id, idUser, amount float64
	var timestampStr, reason string
	var includeInBalance bool

	id, ok[0] = job["Id"].(float64)
	idUser, ok[1] = job["IdUser"].(float64)
	timestampStr, ok[2] = job["Timestamp"].(string)
	reason, ok[3] = job["Reason"].(string)
	amount, ok[4] = job["Amount"].(float64)
	includeInBalance, ok[5] = job["IncludeInBalance"].(bool)

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	deposit, err := pm.NewDeposit(
		uint(id),
		uint(idUser),
		timestamp,
		reason,
		float32(amount),
		includeInBalance,
	)

	return deposit, err
}
