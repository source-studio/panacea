package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	BALANCE_COLLECTION = "/balance" // Defines the balance global route

	PRIV_BALANCE_VIEW     = "pnc.balance-view"     // Balance view privilege
	PRIV_BALANCE_GENERATE = "pnc.balance-generate" // Balance generate privilege
	PRIV_BALANCE_ADD      = "pnc.balance-add"      // Balance create privilege
	PRIV_BALANCE_UPDATE   = "pnc.balance-update"   // Balance update privilege
	PRIV_BALANCE_REMOVE   = "pnc.balance-remove"   // Balance remove privilege

	REL_BALANCE_LAST       = "/rels/pnc.balance-last"       // Rel for link that retrieves the last balance
	REL_BALANCE_COLLECTION = "/rels/pnc.balance-collection" // Rel for link to retrieve balance collection
	REL_BALANCE_RANGE      = "/rels/pnc.balance-range"      // Rel for link to retreive balances within a date range
	REL_BALANCE_SEARCH     = "/rels/pnc.balance-search"     // Rel for link to search balances by date range and user
	REL_BALANCE_GENERATE   = "/rels/pnc.balance-generate"   // Rel for link to generate the initial data for a balance
	REL_BALANCE_ADD        = "/rels/pnc.balance-add"        // Rel for link to add a balance
	REL_BALANCE_UPDATE     = "/rels/pnc.balance-update"     // Rel for link to update a balance
	REL_BALANCE_REMOVE     = "/rels/pnc.balance-remove"     // Rel for link to remove a balance
)

/*
 Service implementation for the balance model
*/
type BalanceService struct {
	*svr.AuthModelService
	dao *pd.BalanceDao // Data access object for database transactions
}

/*
 Creates a new balance service
*/
func NewBalanceService(router *mux.Router, store *sessions.CookieStore) *BalanceService {
	var (
		bs        = BalanceService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(BALANCE_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(bs.get)
	subrouter.Methods(svr.GET).Path("/last").HandlerFunc(bs.getLast)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(BALANCE_COLLECTION)).HandlerFunc(bs.getAll)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").Queries(USER, "").HandlerFunc(bs.search)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").HandlerFunc(bs.getByRange)
	subrouter.Methods(svr.GET).Path("/generator").HandlerFunc(bs.generate)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(BALANCE_COLLECTION)).HandlerFunc(bs.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(bs.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(bs.remove)

	return &bs
}

/*
 Initializes the balance service
*/
func (bs *BalanceService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	bs.dao, err = pd.NewBalanceDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	var rangeUri = BALANCE_COLLECTION + "?from={from}&to={to}"
	svr.RegisterLink(&svr.Link{svr.JSON, REL_BALANCE_LAST, svr.GET, BALANCE_COLLECTION + "/last"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_BALANCE_COLLECTION, svr.GET, BALANCE_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_BALANCE_RANGE, svr.GET, rangeUri})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_BALANCE_SEARCH, svr.GET, rangeUri + "&user={user}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_BALANCE_GENERATE, svr.GET, BALANCE_COLLECTION + "/generator"})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_BALANCE_ADD, svr.POST, BALANCE_COLLECTION})
}

/*
 Retrieves a balance identified by the given ID
 Method: GET - URI: /balance/{id}
*/
func (bs *BalanceService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return bs.dao.Get(id) }
	bs.Get(writer, req, PRIV_BALANCE_VIEW, get, encodeBalance)
}

/*
 Retrieves the last balance created, if one exists
 Method: GET - URI: /balance/last
*/
func (bs *BalanceService) getLast(writer http.ResponseWriter, req *http.Request) {
	var getLast = func() (gm.Model, error) { return bs.dao.GetLast() }
	bs.GetSingle(writer, req, PRIV_BALANCE_VIEW, getLast, encodeBalance)
}

/*
 Retrieves all available balances
 Method: GET - URI: /balance
*/
func (bs *BalanceService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var balances, err = bs.dao.GetAll()
		return bs.toModels(balances), err
	}

	bs.GetCollection(writer, req, PRIV_BALANCE_VIEW, getAll, bs.Base().EncodeModels(encodeBalance))
}

/*
 Retrieves all balances created within the provided date range
 Method: GET - URI: /balance?from={from-date-time}&to={to-date-time}
*/
func (bs *BalanceService) getByRange(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, vars[FROM])

	if err != nil {
		http.Error(writer, "Error attempting to parse the from date/time argument: "+err.Error(), svr.BAD_REQUEST)
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, vars[TO])

	if err != nil {
		http.Error(writer, "Error attempting to parse the to date/time argument: "+err.Error(), svr.BAD_REQUEST)
	}

	var getByRange = func() ([]gm.Model, error) {
		var balances, err = bs.dao.GetByRange(from, to)
		return bs.toModels(balances), err
	}

	bs.GetCollection(writer, req, PRIV_BALANCE_VIEW, getByRange, bs.Base().EncodeModels(encodeBalance))
}

/*
 Searches for balances. The from and to arguments are required. The user is optional and can be passed as an empty string.
 Method: GET - URI: /balance?from={from}&to={to}&user={user}
*/
func (bs *BalanceService) search(writer http.ResponseWriter, req *http.Request) {
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse the from argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse the to argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var user uint64 = 0

	if len(req.FormValue(USER)) > 0 {
		user, err = strconv.ParseUint(req.FormValue(USER), 10, 32)

		if err != nil {
			http.Error(writer, "Error attempting to parse the user argument: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	var search = func() ([]gm.Model, error) {
		var invoices, err = bs.dao.Search(from, to, uint(user))
		return bs.toModels(invoices), err
	}

	bs.GetCollection(writer, req, PRIV_INVOICE_VIEW, search, bs.Base().EncodeModels(encodeBalance))
}

/*
 Generates a balance. Uses activity since the last balance to generate all totals. The user
 will need to complete balance data with values that can't be calculated like, amount to be left
 at next start, total cash count, etc.
 Method: GET - URI: /balance/generator
*/
func (bs *BalanceService) generate(writer http.ResponseWriter, req *http.Request) {
	var session, err = bs.Base().Store().Get(req, svr.GODFATHER)

	if err != nil {
		http.Error(writer, "Error attemtping to retrieve session information: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		log.Println("Error attemtping to retrieve session information: " + err.Error())
		return
	}

	var id = session.Values[svr.USER_ID].(uint)
	var generate = func() (gm.Model, error) { return bs.dao.Generate(id) }
	bs.GetSingle(writer, req, PRIV_BALANCE_GENERATE, generate, encodeBalance)
}

/*
 Adds a new balance. The most common scenario will fist invoke generate to create the base balance data off calculted values
 and have the user insert the rest.
 Method: POST - URI: /balance
*/
func (bs *BalanceService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var balance = m.(*pm.Balance)
		return bs.dao.Add(balance)
	}

	bs.Add(writer, req, PRIV_BALANCE_GENERATE, add, decodeBalance, encodeBalance)
}

/*
 Updates an existing balance.
 Method: PUT - URI: /balance/{id}
*/
func (bs *BalanceService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var balance = m.(*pm.Balance)
		return bs.dao.Update(balance)
	}

	bs.Update(writer, req, PRIV_BALANCE_UPDATE, update, decodeBalance, encodeBalance)
}

/*
 Removes an existing balance.
 Method: DELETE - URI: /balance/{id}
*/
func (bs *BalanceService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return bs.dao.Remove(id) }
	bs.Remove(writer, req, PRIV_BALANCE_REMOVE, remove)
}

/*
 Casts a collection of balances to Balance type
*/
func (bs *BalanceService) toModels(balances []*pm.Balance) []gm.Model {
	var models = make([]gm.Model, len(balances))

	for cont, balance := range balances {
		models[cont] = balance
	}

	return models
}

/*
 Encodes a balance object to a serialized json representation for transport
*/
func encodeBalance(m gm.Model) ([]byte, error) {
	var balance, tok = m.(*pm.Balance)

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Balance")
	}

	if balance == nil {
		return nil, nil
	}

	var model = struct {
		Id          uint
		IdUser      uint
		Timestamp   string
		Start       float32
		CashSales   float32
		CreditSales float32
		Refunds     float32
		Payments    float32
		Expenses    float32
		Deposits    float32
		NextStart   float32
		Balance     float32
		Difference  float32
		Notes       string
	}{
		balance.Id(),
		balance.IdUser(),
		balance.Timestamp().Format(gd.DATE_TIME_FORMAT),
		balance.Start(),
		balance.CashSales(),
		balance.CreditSales(),
		balance.Refunds(),
		balance.Payments(),
		balance.Expenses(),
		balance.Deposits(),
		balance.NextStart(),
		balance.Balance(),
		balance.Difference(),
		balance.Notes(),
	}

	var resource = fmt.Sprintf("%s/%d", BALANCE_COLLECTION, balance.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_BALANCE_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_BALANCE_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Attempts to decode a balance presentation into a native model object
*/
func decodeBalance(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 14)
	var id, idUser, start, cash, credit, refunds, payments, expenses, deposits, nextStart, balanceFlt, difference float64
	var timestampStr, notes string

	id, ok[0] = job["Id"].(float64)
	idUser, ok[1] = job["IdUser"].(float64)
	timestampStr, ok[2] = job["Timestamp"].(string)
	start, ok[3] = job["Start"].(float64)
	cash, ok[4] = job["CashSales"].(float64)
	credit, ok[5] = job["CreditSales"].(float64)
	refunds, ok[6] = job["Refunds"].(float64)
	payments, ok[7] = job["Payments"].(float64)
	expenses, ok[8] = job["Expenses"].(float64)
	deposits, ok[9] = job["Deposits"].(float64)
	nextStart, ok[10] = job["NextStart"].(float64)
	balanceFlt, ok[11] = job["Balance"].(float64)
	difference, ok[12] = job["Difference"].(float64)
	notes, ok[13] = job["Notes"].(string)

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	balance, err := pm.NewBalance(
		uint(id),
		uint(idUser),
		timestamp,
		float32(start),
		float32(cash),
		float32(credit),
		float32(refunds),
		float32(payments),
		float32(expenses),
		float32(deposits),
		float32(nextStart),
		float32(balanceFlt),
		float32(difference),
		notes,
	)

	return balance, err
}
