package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/gorilla/websocket"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	NOTIFICATION_COLLECTION = "/notification" // Defines the notification global route

	PRIV_NOTIFICATION_VIEW   = "pnc.notification-view"   // Notification view privilege
	PRIV_NOTIFICATION_REMOVE = "pnc.notification-remove" // Notification remove privilege

	REL_NOTIFICATION_GET        = "/rels/pnc.notification-get"        // Rel for link to view notifications
	REL_NOTIFICATION_BY_USER    = "/rels/pnc.notification-by-user"    // Rel for link to retrieve notifications for the user
	REL_NOTIFICATION_REMOVE     = "/rels/pnc.notification-remove"     // Rel for link to remove a notification
	REL_NOTIFICATION_CONNECT    = "/rels/pnc.notification-connect"    // Rel for link to subscribe to receive notifications
	REL_NOTIFICATION_DISCONNECT = "/rels/pnc.notification-disconnect" // Rel for link to unsubscribe from receiving notifications

	SOURCE = "source" // Source constant for uri parameters
	TYPE   = "type"   // Type constant for uri parameters
)

/*
 Service implementation that handles access ot the Notification collection
*/
type NotificationService struct {
	*svr.AuthModelService
	dao         *pd.NotificationDao
	connections map[uint]*websocket.Conn
}

/*
 Creates a new Notification servers. Expects the server's request router
*/
func NewNotificationService(router *mux.Router, store *sessions.CookieStore) *NotificationService {
	var (
		ns        = NotificationService{svr.NewAuthModelService(router, store), nil, make(map[uint]*websocket.Conn)}
		subrouter = router.PathPrefix(NOTIFICATION_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(ns.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(NOTIFICATION_COLLECTION)).HandlerFunc(ns.getByUser)
	subrouter.Path("/socket").HandlerFunc(ns.connect)
	subrouter.Methods(svr.DELETE).Path("/socket").HandlerFunc(ns.disconnect)
	subrouter.Methods(svr.GET).Queries(SOURCE, "").Queries(TYPE, "").HandlerFunc(ns.getBySource)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(ns.remove)
	subrouter.Methods(svr.DELETE).Queries(USER, "").HandlerFunc(ns.removeByUser)
	subrouter.Methods(svr.DELETE).Queries(SOURCE, "").Queries(TYPE, "").HandlerFunc(ns.removeBySource)

	return &ns
}

/*
 Initializes the service after it's been loaded to the server.
*/
func (ns *NotificationService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	ns.dao, err = pd.NewNotificationDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_NOTIFICATION_GET, svr.GET, NOTIFICATION_COLLECTION + "/{id}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_NOTIFICATION_BY_USER, svr.GET, NOTIFICATION_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_NOTIFICATION_CONNECT, svr.POST, NOTIFICATION_COLLECTION + "/socket"})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_NOTIFICATION_DISCONNECT, svr.DELETE, NOTIFICATION_COLLECTION + "/socket"})

	go ns.monitor()
}

/*
 Retrieves the notification identified by the provided ID
 Method: GET - URI: /notification/{id}
*/
func (ns *NotificationService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return ns.dao.Get(id) }
	ns.Get(writer, req, PRIV_NOTIFICATION_VIEW, get, encodeNotification)
}

/*
 Retrieves all notifications in queue for a given user
 Method: GET - URI: /notification?user={user}
*/
func (ns *NotificationService) getByUser(writer http.ResponseWriter, req *http.Request) {
	var user, ok = ns.getUser(writer, req)

	if !ok {
		return
	}

	var getByUser = func() ([]gm.Model, error) {
		var notifications, err = ns.dao.GetByUser(user)
		return ns.toModels(notifications), err
	}

	ns.GetCollection(writer, req, PRIV_NOTIFICATION_VIEW, getByUser, ns.Base().EncodeModels(encodeNotification))
}

/*
 Retrieves all notifications in queue for a given source and type. The source is an optional field
 and can be tied to different elements. There is no referential integrity around it. The combination
 of source and type will infer the type of element it refers to.
 Method: GET - URI: /notification?source={source}&type={type}
*/
func (ns *NotificationService) getBySource(writer http.ResponseWriter, req *http.Request) {
	var source, err = strconv.ParseUint(req.FormValue(SOURCE), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the source argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	notificationType, err := strconv.ParseUint(req.FormValue(TYPE), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the type argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getBySource = func() ([]gm.Model, error) {
		var notifications, err = ns.dao.GetBySource(uint(source), pm.NotificationType(notificationType))
		return ns.toModels(notifications), err
	}

	ns.GetCollection(writer, req, PRIV_NOTIFICATION_VIEW, getBySource, ns.Base().EncodeModels(encodeNotification))
}

/*
 Allows clients to subscribe to receive notifications via websockets
*/
func (ns *NotificationService) connect(writer http.ResponseWriter, req *http.Request) {
	var user, ok = ns.getUser(writer, req)

	if !ok {
		return
	}

	conn, ok := ns.connections[user]

	if ok {
		conn.Close()
	}

	conn, err := websocket.Upgrade(writer, req, nil, 1024, 1024)

	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(writer, "Not a websocket handshake", svr.BAD_REQUEST)
		return
	} else if err != nil {
		http.Error(writer, "Error attempting to open a websocket connection", svr.INTERNAL_SERVER_ERROR)
		return
	}

	ns.connections[user] = conn
}

/*
 Allows client to disconnect from receiving websocket notifications
*/
func (ns *NotificationService) disconnect(writer http.ResponseWriter, req *http.Request) {
	var user, ok = ns.getUser(writer, req)

	if !ok {
		return
	}

	conn, ok := ns.connections[user]

	if ok {
		conn.Close()
		writer.WriteHeader(svr.OK)
	} else {
		writer.WriteHeader(svr.NOT_FOUND)
	}
}

/*
 Removes a notification identified by a given ID.
 Method: DELETE - URI: /notification/{id}
*/
func (ns *NotificationService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return ns.dao.Remove(id) }
	ns.Remove(writer, req, PRIV_NOTIFICATION_REMOVE, remove)
}

/*
 Removes all notifications that are in queue for a given user. This effectively clear's the user's queue of notifications.
 Method: DELETE - URI: /notification?user={user}
*/
func (ns *NotificationService) removeByUser(writer http.ResponseWriter, req *http.Request) {
	var user, err = strconv.ParseUint(req.FormValue(USER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the user argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var removeByUser = func() (uint, error) { return ns.dao.RemoveByUser(uint(user)) }
	ns.Execute(writer, req, PRIV_NOTIFICATION_REMOVE, removeByUser)
}

/*
  Removes all notifications that are in queue for a source.
  Method: DELETE - URI: /notification?source={source}&type={type}
*/
func (ns *NotificationService) removeBySource(writer http.ResponseWriter, req *http.Request) {
	var source, err = strconv.ParseUint(req.FormValue(SOURCE), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the source argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	notificationType, err := strconv.ParseUint(req.FormValue(TYPE), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the type argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var removeBySource = func() (uint, error) {
		return ns.dao.RemoveBySource(uint(source), pm.NotificationType(notificationType))
	}
	ns.Execute(writer, req, PRIV_NOTIFICATION_REMOVE, removeBySource)
}

/*
 Converts a slice of notification objects into a slice of model types
*/
func (ns *NotificationService) toModels(notifications []*pm.Notification) []gm.Model {
	var models = make([]gm.Model, len(notifications))

	for cont, notification := range notifications {
		models[cont] = notification
	}

	return models
}

/*
 Retrieves the user ID from the session info
*/
func (ns *NotificationService) getUser(writer http.ResponseWriter, req *http.Request) (uint, bool) {
	session, err := ns.Base().Store().Get(req, svr.GODFATHER)

	if err != nil {
		http.Error(writer, "Error attempting to get session information: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		log.Println("Error attempting to get session information: "+err.Error(), svr.INTERNAL_SERVER_ERROR)
		return 0, false
	}

	value, ok := session.Values[svr.USER_ID]

	if !ok {
		return 0, false
	}

	idUser, ok := value.(uint)

	return idUser, ok
}

/*
 Monitors the notification channel. When a new notification is received, if the subscribed user is present, the message
 will be sent via websocket. Otherwise it will be discarded.
*/
func (ns *NotificationService) monitor() {
	for {
		var notification = <-pd.NotifyChan
		var conn, ok = ns.connections[notification.IdUser()]

		if !ok {
			continue
		}

		var message, err = encodeNotification(notification)

		if err == nil {
			conn.WriteMessage(websocket.TextMessage, message)
		}
	}
}

/*
 Encodes a notification to a json representation for transport to the client
*/
func encodeNotification(m gm.Model) ([]byte, error) {
	var notification, tok = m.(*pm.Notification)

	if !tok {
		return nil, errors.New("Cannot dcode object. Not of type Notification")
	}

	if notification == nil {
		return nil, nil
	}

	var typeStr, err = pm.FormatNotificationType(notification.NotificationType())

	if err != nil {
		return nil, err
	}

	var model = struct {
		Id               uint
		IdSubscription   uint
		IdUser           uint
		IdSource         uint
		NotificationType string
		Timestamp        string
	}{
		notification.Id(),
		notification.IdSubscription(),
		notification.IdUser(),
		notification.IdSource(),
		typeStr,
		notification.Timestamp().Format(gd.DATE_TIME_FORMAT),
	}

	var resource = fmt.Sprintf("%s/%d", NOTIFICATION_COLLECTION, notification.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_NOTIFICATION_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Deocdes a notification from a json representation to a native model object
*/
func decodeNotification(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 6)
	var id, idSubscription, idUser, idSource float64
	var typeStr, timestampStr string

	id, ok[0] = job["Id"].(float64)
	idSubscription, ok[1] = job["IdSubscription"].(float64)
	idUser, ok[2] = job["IdUser"].(float64)
	idSource, ok[3] = job["IdSource"].(float64)
	typeStr, ok[4] = job["NotificationType"].(string)
	timestampStr, ok[5] = job["Timestamp"].(string)

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	ntype, err := pm.ParseNotificationType(typeStr)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	notification, err := pm.NewNotification(
		uint(id),
		uint(idSubscription),
		uint(idUser),
		uint(idSource),
		ntype,
		timestamp,
	)

	return notification, err
}
