package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	"gxs3/panacea/model"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	INVOICE_COLLECTION = "/invoice" //Invoice service global route

	PRIV_INVOICE_VIEW            = "pnc.invoice-view"            // Invoice view privilege
	PRIV_INVOICE_SELL            = "pnc.invoice-sell"            // Invoice sell privilege
	PRIV_INVOICE_INVALIDATE      = "pnc.invoice-invalidate"      // Invoice invalidate privilege
	PRIV_INVOICE_INVALIDATE_UNDO = "pnc.invoice-invalidate-undo" // Invoice invalidate undo privilege
	PRIV_INVOICE_REFUND          = "pnc.invoice-refund"          // Invoice refund privilege
	PRIV_INVOICE_REFUND_UNDO     = "pnc.invoice-refund-undo"     // Refund undo privilege

	REL_INVOICE                  = "/rels/pnc.invoice-get"              // Rel for link that retrieves a single invoice
	REL_INVOICE_COLLECTION       = "/rels/pnc.invoice-collection"       // Rel for link that retrieves the invoice collection
	REL_INVOICE_RANGE            = "/rels/pnc.invoice-range"            // Rel for link that retrieves a range of invoices
	REL_INVOICE_SEARCH           = "/rels/pnc.invoice-search"           // Rel for link that searches invoices
	REL_INVOICE_GET_INVALIDATION = "/rels/pnc.invoice-get-invalidation" // Rel for link that retrieves an invalidation for an invoice
	REL_INVOICE_GET_REFUND       = "/rels/pnc.invoice-get-refund"       // Rel for link that retrieves refunds
	REL_INVOICE_SELL             = "/rels/pnc.invoice-sell"             // Rel for link to sell an invoice
	REL_INVOICE_SELL_CREDIT      = "/rels/pnc.invoice-sell-credit"      // Rel for link to sell an invoice and tie it to a credit line
	REL_INVOICE_SELL_SPONSORED   = "/rels/pnc.invoice-sell-sponsored"   // Rel for link to sell an sponsored invoice
	REL_INVOICE_INVALIDATE       = "/rels/pnc.invoice-invalidate"       // Rel for link that updates an invoice
	REL_INVOICE_INVALIDATE_UNDO  = "/rels/pnc.invoice-invalidate-undo"  // Rel for link that reverses changes introduced by an invalidate transaciton
	REL_INVOICE_REFUND           = "/rels/pnc.invoice-refund"           // Rel for link that removes an invoice
	REL_INVOICE_REFUND_UNDO      = "/rels/pnc.invoice-refund-undo"      // Rel for link that reverses changes introduced by a refund transaction
	REL_INVOICE_PRODUCTS         = "/rels/pnc.invoice-product"          // Rel for link that retrieves all products sold in an invoice

	FROM        = "from"        // Used to extract from date/time values from request queries
	TO          = "to"          // Used to extact to date/time values from request queries
	USER        = "user"        // Used to extract user values from request queries
	CUSTOMER    = "customer"    // Used to extract customer values from request queries
	PAYMENT     = "payment"     // Used to extract payment values from request queries
	TRANSACTION = "transaction" // Used to extract transaction values from request queries
	CREDIT_LINE = "credit-line" // Used to extract credit line values from request queries
	AMOUNT      = "amount"
)

/*
 Service implementation that handles access to the invoice collection.
*/
type InvoiceService struct {
	*svr.AuthModelService
	dao *pd.InvoiceDao
}

/*
 Serializable representation of the invoice item model. Used to make it easier to serialize invoice items into json
*/
type sItem struct {
	Id        uint
	Uuid      string
	IdInvoice uint
	IdProduct uint
	Quantity  float32
}

/*
 Serializable representation of the restock model. Used to make it eaiser to serialized restock objects into json
*/
type sRestock struct {
	Id            uint
	IdRefund      uint
	IdInvoiceItem uint
	Quantity      float32
	Value         float32
	Restocked     bool
}

/*
 Creates a new invoice service
*/
func NewInvoiceService(router *mux.Router, store *sessions.CookieStore) *InvoiceService {
	var (
		is        = InvoiceService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(INVOICE_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(is.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(INVOICE_COLLECTION)).HandlerFunc(is.getAll)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").Queries(USER, "").Queries(PAYMENT, "").Queries(TRANSACTION, "").HandlerFunc(is.search)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").Queries(USER, "").HandlerFunc(is.getByRangeAndUser)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").HandlerFunc(is.getByRange)
	subrouter.Methods(svr.GET).Queries(CUSTOMER, "").HandlerFunc(is.getByCustomer)
	subrouter.Methods(svr.POST).Queries(CREDIT_LINE, "").HandlerFunc(is.sellCredit)
	subrouter.Methods(svr.POST).Queries(SPONSOR, "").Queries(AMOUNT, "").HandlerFunc(is.sellSponsored)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(INVOICE_COLLECTION)).HandlerFunc(is.sell)
	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN + "/invalidation").HandlerFunc(is.getInvdaliation)
	subrouter.Methods(svr.POST).Path(svr.ID_PATTERN + "/invalidation").HandlerFunc(is.invalidate)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN + "/invalidation").HandlerFunc(is.undoInvalidate)
	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN + "/refund").HandlerFunc(is.getRefund)
	subrouter.Methods(svr.POST).Path(svr.ID_PATTERN + "/refund").HandlerFunc(is.refund)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN + "/refund").HandlerFunc(is.undoRefund)

	return &is
}

/*
 Initializes the invoice service
*/
func (is *InvoiceService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	is.dao, err = pd.NewInvoiceDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	var rangeUri = fmt.Sprintf("%s?from={from}&to={to}", INVOICE_COLLECTION)
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE, svr.GET, INVOICE_COLLECTION + "/{invoice}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE_COLLECTION, svr.GET, INVOICE_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE_RANGE, svr.GET, rangeUri})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE_SEARCH, svr.GET, rangeUri + "&user={user}&payment={payment}&transaction={transaction}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE_SELL, svr.POST, INVOICE_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE_SELL_CREDIT, svr.POST, INVOICE_COLLECTION + "?credit-line={credit-line}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_INVOICE_SELL_SPONSORED, svr.POST, INVOICE_COLLECTION + "?sponsor={sponsor}&amount={amount}"})
}

/*
 Retrive an invoice identified by a given ID.
 Method: GET - URI: /invoice/{id}
*/
func (is *InvoiceService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return is.dao.Get(id) }
	is.Get(writer, req, PRIV_INVOICE_VIEW, get, encodeInvoice)
}

/*
 Retrieve all available invoices
 Method: GET - URI: /invoice
*/
func (is *InvoiceService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var invoices, err = is.dao.GetAll()
		return is.toModels(invoices), err
	}

	is.GetCollection(writer, req, PRIV_INVOICE_VIEW, getAll, is.Base().EncodeModels(encodeInvoice))
}

/*
 Retrieve all invoices found between the provided date/time range
 Method: GET - URI: /invoice?from={from-date-time}&to={to-date-time}
*/
func (is *InvoiceService) getByRange(writer http.ResponseWriter, req *http.Request) {
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByRange = func() ([]gm.Model, error) {
		var invoices, err = is.dao.GetByRange(from, to)
		return is.toModels(invoices), err
	}

	is.GetCollection(writer, req, PRIV_INVOICE_VIEW, getByRange, is.Base().EncodeModels(encodeInvoice))
}

/*
 Retrieve all invoices found between the provided date/time range, and that were created by the specified user.
 Method: GET - URI: /invoice?from={from-date-time}&to={to-date-time}&user={id-user}
*/
func (is *InvoiceService) getByRangeAndUser(writer http.ResponseWriter, req *http.Request) {
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' dae/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	user, err := strconv.ParseUint(req.FormValue(USER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse user argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByRangeAndUser = func() ([]gm.Model, error) {
		var invoices, err = is.dao.GetByRangeAndUser(uint(user), from, to)
		return is.toModels(invoices), err
	}

	is.GetCollection(writer, req, PRIV_INVOICE_VIEW, getByRangeAndUser, is.Base().EncodeModels(encodeInvoice))
}

/*
 Retrieves all invoices which are associated with a given customer
 Method: GET - URI: /invoice?customer={id-customer}
*/
func (is *InvoiceService) getByCustomer(writer http.ResponseWriter, req *http.Request) {
	var customer, err = strconv.ParseUint(req.FormValue(CUSTOMER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse customer argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByCustomer = func() ([]gm.Model, error) {
		var invoices, err = is.dao.GetByCustomer(uint(customer))
		return is.toModels(invoices), err
	}

	is.GetCollection(writer, req, PRIV_INVOICE_VIEW, getByCustomer, is.Base().EncodeModels(encodeInvoice))
}

/*
 Searches for invoices. Expects a date range which is required. Optionally values can also be provided for user and payment.
 If user and payment is not required for the search, the parameters should still be put into the URL, with empty values.
 Method: GET - URI: /invoice?from={from}&to={to}&user={user}&payment={payment}
*/
func (is *InvoiceService) search(writer http.ResponseWriter, req *http.Request) {
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse the from argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse the to argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var user uint64 = 0
	var payType model.PaymentType = model.PaymentType(0)
	var transaction model.TransactionType = model.TransactionType(0)

	if len(req.FormValue(USER)) > 0 {
		user, err = strconv.ParseUint(req.FormValue(USER), 10, 32)

		if err != nil {
			http.Error(writer, "Error attempting to parse the user argument: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	if len(req.FormValue(PAYMENT)) > 0 {
		payType, err = model.ParsePaymentType(req.FormValue(PAYMENT))

		if err != nil {
			http.Error(writer, "Error attempting to parse payment type: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	if len(req.FormValue(TRANSACTION)) > 0 {
		transaction, err = model.ParseTransactionType(req.FormValue(TRANSACTION))

		if err != nil {
			http.Error(writer, "Error attempting to parse transaction type: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	var search = func() ([]gm.Model, error) {
		var invoices, err = is.dao.Search(from, to, uint(user), payType, transaction)
		return is.toModels(invoices), err
	}

	is.GetCollection(writer, req, PRIV_INVOICE_VIEW, search, is.Base().EncodeModels(encodeInvoice))
}

/*
 Sells an invoice on credit. Ties the invoice to a credit line.
 Method: POST - URI: /invoice?credit-line={credit-line}
*/
func (is *InvoiceService) sellCredit(writer http.ResponseWriter, req *http.Request) {
	var form, err = url.ParseQuery(req.URL.RawQuery)

	if err != nil {
		http.Error(writer, "Error attempting to parse the invoice form: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var element, ok = form[CREDIT_LINE]

	if !ok || len(element[0]) < 1 {
		http.Error(writer, "Missing credit line argument", svr.BAD_REQUEST)
		return
	}

	creditLine, err := strconv.ParseUint(form[CREDIT_LINE][0], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse credit line argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var sell = func(m gm.Model) (uint, error) {
		var invoice = m.(*model.Invoice)
		var id, err = is.dao.SellCredit(invoice, uint(creditLine))

		if err != nil && err == pd.INSUFFICIENT_INVENTORY {
			return 0, gd.CONSTRAINT
		}

		return id, err
	}
	is.Add(writer, req, PRIV_INVOICE_SELL, sell, decodeInvoice, encodeInvoice)
}

/*
 Sells a sponsored invoice.
 Method: POST - URI: /invoice?sponsor={sponsor}&amount={amount}
*/
func (is *InvoiceService) sellSponsored(writer http.ResponseWriter, req *http.Request) {
	var form, err = url.ParseQuery(req.URL.RawQuery)

	if err != nil {
		http.Error(writer, "Errro attempting to parse the invoice form: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var element, ok = form[SPONSOR]

	if !ok || len(element[0]) < 1 {
		http.Error(writer, "Missing sponsor argument", svr.BAD_REQUEST)
		return
	}

	sponsor, err := strconv.ParseUint(form[SPONSOR][0], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse sponsor argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	element, ok = form[AMOUNT]

	if !ok || len(element[0]) < 1 {
		http.Error(writer, "Missing amount argument", svr.BAD_REQUEST)
		return
	}

	amount, err := strconv.ParseFloat(form[AMOUNT][0], 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse amount argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var sell = func(m gm.Model) (uint, error) {
		var invoice = m.(*model.Invoice)
		var id, err = is.dao.SellSponsored(invoice, uint(sponsor), float32(amount))

		if err != nil && err == pd.INSUFFICIENT_INVENTORY {
			return 0, gd.CONSTRAINT
		}

		return id, err
	}
	is.Add(writer, req, PRIV_INVOICE_SELL, sell, decodeInvoice, encodeInvoice)
}

/*
 Performs a sell transaction on an invoice. Will make the appropriate modifications to sold inventory.
 Method: POST - URI: /invoice
*/
func (is *InvoiceService) sell(writer http.ResponseWriter, req *http.Request) {
	var sell = func(m gm.Model) (uint, error) {
		var invoice = m.(*model.Invoice)
		var id, err = is.dao.Sell(invoice)

		if err != nil {
			switch err {
			case pd.INSUFFICIENT_INVENTORY:
				return 0, gd.CONSTRAINT

			case pd.CREDIT_LINE_LIMIT:
				return 0, gd.PRECONDITION_FAILED
			}
		}

		return id, err
	}

	is.Add(writer, req, PRIV_INVOICE_SELL, sell, decodeInvoice, encodeInvoice)
}

/*
 Retrieves an invalidation object for an invoice if such an object exists
*/
func (is *InvoiceService) getInvdaliation(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return is.dao.GetInvalidation(id) }
	is.Get(writer, req, PRIV_INVOICE_VIEW, get, encodeInvalidation)
}

/*
 Invalidates a pre-existant invoice. Helpful when the effects of an invoice need to be nullified.
 Method: POST - URI: /invoice/{id}/invalidate
*/
func (is *InvoiceService) invalidate(writer http.ResponseWriter, req *http.Request) {
	var tx = func(m gm.Model) (uint, error) {
		var invalidation = m.(*model.Invalidation)
		var id, err = is.dao.Invalidate(invalidation)

		if err != nil && err == pd.INVALID_STATE {
			return 0, gd.CONSTRAINT
		}

		return id, err
	}
	is.Add(writer, req, PRIV_INVOICE_INVALIDATE, tx, decodeInvalidation, encodeInvalidation)
}

/*
 Reverses the actions made by a previous invalidate transaction. Establishes the state of the invoice as OK, leaving its
 totals to be used again. Will not work it the invoice is not already in an INVALIDATED state. Will not work if the invoice
 has already been used to create a balance.
 Method: DELETE - URI: /invoice/{id}/invalidate
*/
func (is *InvoiceService) undoInvalidate(writer http.ResponseWriter, req *http.Request) {
	var id, err = strconv.ParseUint(mux.Vars(req)[svr.ID], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var undo = func() (uint, error) {
		var records, err = is.dao.UndoInvalidate(uint(id))

		if err != nil && err == pd.INVALID_STATE {
			return 0, gd.CONSTRAINT
		}

		return records, err
	}
	is.Execute(writer, req, PRIV_INVOICE_INVALIDATE, undo)
}

/*
 Retrieves a refund for a given invoice if it exists
 Method: GET - URI: /invoice/{id}/refund
*/
func (is *InvoiceService) getRefund(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return is.dao.GetRefund(id) }
	is.Get(writer, req, PRIV_INVOICE_VIEW, get, encodeRefund)
}

/*
 Performs a refund on an invoice.
 Method: POST - URI: /invoice/{id}/refund
*/
func (is *InvoiceService) refund(writer http.ResponseWriter, req *http.Request) {
	var tx = func(m gm.Model) (uint, error) {
		var refund = m.(*model.Refund)
		var id, err = is.dao.Refund(refund)

		if err != nil && err == pd.INVALID_STATE {
			return 0, gd.CONSTRAINT
		}

		return id, err
	}
	is.Add(writer, req, PRIV_INVOICE_REFUND, tx, decodeRefund, encodeRefund)
}

/*
 Reverses the actions made by a previous refund transaction. Deletes any existing refund/restock data, and establishes
 the state of the invoice as OK, leaving all of it's values to be used again. Will not work if the invoice is not already
 in a REFUNDED state. Will not work if the refund has already been used to create a balance.
 Method: DELETE - URI: /invoice/{id}/refund/{refund}
*/
func (is *InvoiceService) undoRefund(writer http.ResponseWriter, req *http.Request) {
	var id, err = strconv.ParseUint(mux.Vars(req)["id"], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the Refund argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var undo = func() (uint, error) {
		var records, err = is.dao.UndoRefund(uint(id))

		if err != nil && err == pd.INVALID_STATE {
			return 0, gd.CONSTRAINT
		}

		return records, err
	}
	is.Execute(writer, req, PRIV_INVOICE_INVALIDATE, undo)
}

/*
 Convert a list of invoices from their native type to interface model types
*/
func (is *InvoiceService) toModels(invoices []*model.Invoice) []gm.Model {
	var models = make([]gm.Model, len(invoices))

	for cont, invoice := range invoices {
		models[cont] = invoice
	}

	return models
}

/*
 Encodes an invoice to a serialized representation for transport. Adds REST links
*/
func encodeInvoice(m gm.Model) ([]byte, error) {
	var invoice, tok = m.(*model.Invoice)

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type Invoice.")
	}

	if invoice == nil {
		return nil, nil
	}

	var items = make([]sItem, len(invoice.Items()))
	for cont, item := range invoice.Items() {
		items[cont] = sItem{
			item.Id(),
			item.Uuid(),
			item.IdInvoice(),
			item.IdProduct(),
			item.Quantity(),
		}
	}

	var err = make([]error, 3)
	var transactionStr, paymentStr, stateStr string
	transactionStr, err[0] = model.FormatTransactionType(invoice.Transaction())
	paymentStr, err[1] = model.FormatPaymentType(invoice.Payment())
	stateStr, err[2] = model.FormatStateType(invoice.State())

	for _, e := range err {
		if e != nil {
			return nil, e
		}
	}

	var im = struct {
		Id          uint
		IdUser      uint
		Transaction string
		Payment     string
		State       string
		Timestamp   string
		Total       float32
		Discount    float32
		Items       []sItem
	}{
		invoice.Id(),
		invoice.IdUser(),
		transactionStr,
		paymentStr,
		stateStr,
		invoice.Timestamp().Format(gd.DATE_TIME_FORMAT),
		invoice.Total(),
		invoice.Discount(),
		items,
	}

	var resource = fmt.Sprintf("%s/%d", INVOICE_COLLECTION, invoice.Id())
	var invalidateResource = fmt.Sprintf("%s/%d/invalidation", INVOICE_COLLECTION, invoice.Id())
	var refundResource = fmt.Sprintf("%s/%d/refund", INVOICE_COLLECTION, invoice.Id())
	var productResource = fmt.Sprintf("%s?invoice=%d", PRODUCT_COLLECTION, invoice.Id())

	var links = []svr.Link{
		svr.Link{svr.PLAIN, REL_INVOICE_INVALIDATE, svr.POST, invalidateResource},
		svr.Link{svr.PLAIN, REL_INVOICE_REFUND, svr.POST, refundResource},
		svr.Link{svr.JSON, REL_INVOICE_GET_INVALIDATION, svr.GET, invalidateResource},
		svr.Link{svr.JSON, REL_INVOICE_GET_REFUND, svr.GET, refundResource},
		svr.Link{svr.JSON, REL_INVOICE_PRODUCTS, svr.GET, productResource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: im,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes an invoice representation into a native model object.
*/
func decodeInvoice(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 9)
	var id, idUser, idInvoice, idProduct, quantity, total, discount float64
	var timestampStr, uuid, txStr, payStr, stateStr string
	var items []interface{}

	id, ok[0] = job["Id"].(float64)
	idUser, ok[1] = job["IdUser"].(float64)
	txStr, ok[2] = job["Transaction"].(string)
	payStr, ok[3] = job["Payment"].(string)
	stateStr, ok[4] = job["State"].(string)
	timestampStr, ok[5] = job["Timestamp"].(string)
	total, ok[6] = job["Total"].(float64)
	discount, ok[7] = job["Discount"].(float64)
	items, ok[8] = job["Items"].([]interface{})

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	transaction, err := model.ParseTransactionType(txStr)

	if err != nil {
		return nil, err
	}

	payment, err := model.ParsePaymentType(payStr)

	if err != nil {
		return nil, err
	}

	state, err := model.ParseStateType(stateStr)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	invoice, err := model.NewInvoice(uint(id), uint(idUser), transaction, payment, state, timestamp, float32(total), float32(discount))

	if err != nil {
		return nil, err
	}

	ok = ok[:5]

	for _, item := range items {
		var itemJob = item.(map[string]interface{})
		id, ok[0] = itemJob["Id"].(float64)
		uuid, ok[1] = itemJob["Uuid"].(string)
		idInvoice, ok[2] = itemJob["IdInvoice"].(float64)
		idProduct, ok[3] = itemJob["IdProduct"].(float64)
		quantity, ok[4] = itemJob["Quantity"].(float64)

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		item, err := model.NewInvoiceItem(
			uint(id),
			uuid,
			uint(idInvoice),
			uint(idProduct),
			float32(quantity),
		)

		if err != nil {
			return nil, err
		}

		invoice.Add(item)
	}

	return invoice, nil
}

/*
 Encodes a refund to a serialized representation for transport to the server. Adds REST links.
*/
func encodeRefund(m gm.Model) ([]byte, error) {
	var refund, tok = m.(*model.Refund)

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type Refund.")
	}

	if refund == nil {
		return nil, nil
	}

	var restock = make([]sRestock, len(refund.Restock()))
	for cont, rs := range refund.Restock() {
		restock[cont] = sRestock{
			rs.Id(),
			rs.IdRefund(),
			rs.IdInvoiceItem(),
			rs.Quantity(),
			rs.Value(),
			rs.Restocked(),
		}
	}

	var model = struct {
		Id         uint
		IdUser     uint
		IdInvoice  uint
		Timestamp  string
		Calculated float32
		Actual     float32
		Restock    []sRestock
	}{
		refund.Id(),
		refund.IdUser(),
		refund.IdInvoice(),
		refund.Timestamp().Format(gd.DATE_TIME_FORMAT),
		refund.Calculated(),
		refund.Actual(),
		restock,
	}

	var invoice = fmt.Sprintf("%s/%d/refund", INVOICE_COLLECTION, refund.IdInvoice())
	var resource = fmt.Sprintf("%s/%d/refund/%d", INVOICE_COLLECTION, refund.IdInvoice(), refund.Id())
	var links = []svr.Link{svr.Link{svr.PLAIN, REL_INVOICE_REFUND_UNDO, svr.DELETE, invoice}}
	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a refund representation into a native model object.
*/
func decodeRefund(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 7)
	var id, idUser, idInvoice, calculated, actual, idRefund, idInvoiceItem, quantity, value float64
	var timestampStr string
	var restockList []interface{}
	var restock bool

	id, ok[0] = job["Id"].(float64)
	idUser, ok[1] = job["IdUser"].(float64)
	idInvoice, ok[2] = job["IdInvoice"].(float64)
	timestampStr, ok[3] = job["Timestamp"].(string)
	calculated, ok[4] = job["Calculated"].(float64)
	actual, ok[5] = job["Actual"].(float64)
	restockList, ok[6] = job["Restock"].([]interface{})

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	refund, err := model.NewRefund(uint(id), uint(idUser), uint(idInvoice), timestamp, float32(calculated), float32(actual))
	ok = make([]bool, 6)

	for _, r := range restockList {
		var rjob = r.(map[string]interface{})
		id, ok[0] = rjob["Id"].(float64)
		idRefund, ok[1] = rjob["IdRefund"].(float64)
		idInvoiceItem, ok[2] = rjob["IdInvoiceItem"].(float64)
		quantity, ok[3] = rjob["Quantity"].(float64)
		value, ok[4] = rjob["Value"].(float64)
		restock, ok[5] = rjob["Restocked"].(bool)

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		rs, err := model.NewRestock(uint(id), uint(idRefund), uint(idInvoiceItem), float32(quantity), float32(value), restock)

		if err != nil {
			return nil, err
		}

		refund.Add(rs)
	}

	return refund, nil
}

/*
 Encodes an invalidation object to a serialized representation for transport to the server. Adds REST links.
*/
func encodeInvalidation(m gm.Model) ([]byte, error) {
	var invalidation, tok = m.(*model.Invalidation)

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type invalidation")
	}

	if invalidation == nil {
		return nil, nil
	}

	var model = struct {
		Id        uint
		IdUser    uint
		IdInvoice uint
		Timestamp string
		Reason    string
	}{
		invalidation.Id(),
		invalidation.IdUser(),
		invalidation.IdInvoice(),
		invalidation.Timestamp().Format(gd.DATE_TIME_FORMAT),
		invalidation.Reason(),
	}

	var invoice = fmt.Sprintf("%s/%d/invalidation", INVOICE_COLLECTION, invalidation.IdInvoice())
	var resource = fmt.Sprintf("%s/%d/invalidation/%d", INVOICE_COLLECTION, invalidation.IdInvoice(), invalidation.Id())
	var links = []svr.Link{svr.Link{svr.PLAIN, REL_INVOICE_INVALIDATE_UNDO, svr.DELETE, invoice}}
	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes an invalidation representation into a native model object
*/
func decodeInvalidation(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 5)
	var id, idUser, idInvoice float64
	var timestampStr, reason string

	id, ok[0] = job["Id"].(float64)
	idUser, ok[1] = job["IdUser"].(float64)
	idInvoice, ok[2] = job["IdInvoice"].(float64)
	timestampStr, ok[3] = job["Timestamp"].(string)

	if job["Reason"] == nil {
		ok[4] = true
	} else {
		reason, ok[4] = job["Reason"].(string)
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewInvalidation(uint(id), uint(idUser), uint(idInvoice), timestamp, reason)
}
