package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"net/http"
	"strconv"
	"time"
)

const (
	EXPENSE_COLLECTION = "/expense" // Defines the expense gobal collection

	PRIV_EXPENSE_VIEW   = "pnc.expense-view"   // Expense view privilege
	PRIV_EXPENSE_ADD    = "pnc.expense-add"    // Expense add privilege
	PRIV_EXPENSE_UPDATE = "pnc.expense-update" // Expense update privilege
	PRIV_EXPENSE_REMOVE = "pnc.expense-remove" // Expense remove privilege

	REL_EXPENSE_COLLECTION = "/rels/pnc.expense-collection" // Rel for link to retrieve expense collection
	REL_EXPENSE_SEARCH     = "/rels/pnc-expense-search"     // Rel for link to search expenses by date range and optionally user
	REL_EXPENSE_ADD        = "/rels/pnc.expense-add"        // Rel for link to add an expense
	REL_EXPENSE_UPDATE     = "/rels/pnc.expense-update"     // Rel for link to update an expense
	REL_EXPENSE_REMOVE     = "/rels/pnc.expense-remove"     // Rel for link to remove an expense
)

/*
 Service implementation for the expense model
*/
type ExpenseService struct {
	*svr.AuthModelService
	dao *pd.ExpenseDao // Data access object for database transactions
}

/*
 Creates a new expense service
*/
func NewExpenseService(router *mux.Router, store *sessions.CookieStore) *ExpenseService {
	var (
		es        = ExpenseService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(EXPENSE_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(es.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(EXPENSE_COLLECTION)).HandlerFunc(es.getAll)
	subrouter.Methods(svr.GET).Queries(FROM, "").Queries(TO, "").Queries(USER, "").HandlerFunc(es.search)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(EXPENSE_COLLECTION)).HandlerFunc(es.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(es.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(es.remove)

	return &es
}

/*
 Initialices the expense service
*/
func (es *ExpenseService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	es.dao, err = pd.NewExpenseDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	var searchUri = fmt.Sprintf("%s?from={from}&to={to}&user={user}", EXPENSE_COLLECTION)
	svr.RegisterLink(&svr.Link{svr.JSON, REL_EXPENSE_COLLECTION, svr.GET, EXPENSE_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_EXPENSE_SEARCH, svr.GET, searchUri})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_EXPENSE_ADD, svr.POST, EXPENSE_COLLECTION})
}

/*
 Retrieves a single expense object as identified by the given ID
 Method: GET - URI: /expense/{id}
*/
func (es *ExpenseService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return es.dao.Get(id) }
	es.Get(writer, req, PRIV_EXPENSE_VIEW, get, encodeExpense)
}

/*
 Retrieves all existing expense objects
 Method: GET - URI: /expense
*/
func (es *ExpenseService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var expenses, err = es.dao.GetAll()
		return es.toModels(expenses), err
	}

	es.GetCollection(writer, req, PRIV_EXPENSE_VIEW, getAll, es.Base().EncodeModels(encodeExpense))
}

/*
 Searches through the existing collection of expenses by date range, and optionally user that created the expense entry.
 Method: GET - URI: /expense?from={from}&to={to}&user={user}
*/
func (es *ExpenseService) search(writer http.ResponseWriter, req *http.Request) {
	var user uint64 = 0
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	if len(req.FormValue(USER)) > 0 {
		user, err = strconv.ParseUint(req.FormValue(USER), 10, 32)

		if err != nil {
			http.Error(writer, "Error attempting to parse user argument: "+err.Error(), svr.BAD_REQUEST)
			return
		}
	}

	var search = func() ([]gm.Model, error) {
		var expenses, err = es.dao.Search(from, to, uint(user))
		return es.toModels(expenses), err
	}

	es.GetCollection(writer, req, PRIV_EXPENSE_VIEW, search, es.Base().EncodeModels(encodeExpense))
}

/*
 Adds a new expense to the collection. Expects the expense to be serialized in the request body.
 Method: POST - URI: /expense
*/
func (es *ExpenseService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var expense = m.(*pm.Expense)
		return es.dao.Add(expense)
	}

	es.Add(writer, req, PRIV_EXPENSE_ADD, add, decodeExpense, encodeExpense)
}

/*
 Updates the detail of an exisitng expense.
 Method: PUT - URI: /expense/{id}
*/
func (es *ExpenseService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var expense = m.(*pm.Expense)
		return es.dao.Update(expense)
	}

	es.Update(writer, req, PRIV_EXPENSE_UPDATE, update, decodeExpense, encodeExpense)
}

/*
 Removes an existing expense
 Method: DELETE - URI: /expense/{id}
*/
func (es *ExpenseService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return es.dao.Remove(id) }
	es.Remove(writer, req, PRIV_EXPENSE_REMOVE, remove)
}

/*
 Cast a collection of expense objects to model type
*/
func (es *ExpenseService) toModels(expenses []*pm.Expense) []gm.Model {
	var models = make([]gm.Model, len(expenses))

	for cont, expense := range expenses {
		models[cont] = expense
	}

	return models
}

/*
 Encodes an expense object to a serialized json representation for transport
*/
func encodeExpense(m gm.Model) ([]byte, error) {
	var expense, tok = m.(*pm.Expense)

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Expense.")
	}

	if expense == nil {
		return nil, nil
	}

	var model = struct {
		Id               uint
		IdUser           uint
		Timestamp        string
		Reason           string
		Amount           float32
		IncludeInBalance bool
	}{
		expense.Id(),
		expense.IdUser(),
		expense.Timestamp().Format(gd.DATE_TIME_FORMAT),
		expense.Reason(),
		expense.Amount(),
		expense.IncludeInBalance(),
	}

	var resource = fmt.Sprintf("%s/%d", EXPENSE_COLLECTION, expense.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_EXPENSE_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_EXPENSE_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Deocdes an expense representation into a native model object
*/
func decodeExpense(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 6)
	var id, idUser, amount float64
	var timestampStr, reason string
	var includeInBalance bool

	id, ok[0] = job["Id"].(float64)
	idUser, ok[1] = job["IdUser"].(float64)
	timestampStr, ok[2] = job["Timestamp"].(string)
	reason, ok[3] = job["Reason"].(string)
	amount, ok[4] = job["Amount"].(float64)
	includeInBalance, ok[5] = job["IncludeInBalance"].(bool)

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	expense, err := pm.NewExpense(
		uint(id),
		uint(idUser),
		timestamp,
		reason,
		float32(amount),
		includeInBalance,
	)

	return expense, err
}
