package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"net/http"
	"strconv"
	"time"
)

const (
	SPONSOR_COLLECTION = "/sponsor" // Sponsor service global route

	PRIV_SPONSOR_VIEW        = "pnc.sponsor-view"        // Sponsor view privilege
	PRIV_SPONSOR_ADD         = "pnc.sponsor-add"         // Sponsor add privilege
	PRIV_SPONSOR_UPDATE      = "pnc.sponsor-update"      // Sponsor update privilege
	PRIV_SPONSOR_REMOVE      = "pnc.sponsor-remove"      // Sponsor remove privilege
	PRIV_SPONSOR_SPONSORSHIP = "pnc.sponsor-sponsorship" // Sponsorship managmenet privilege

	REL_SPONSOR_COLLECTION           = "/rels/pnc.sponsor-collection"           // Rel for link that retrieves the sponsor collection
	REL_SPONSOR_SEARCH               = "/rels/pnc.sponsor-search"               // Rel for link that searches sponsors
	REL_SPONSOR_ADD                  = "/rels/pnc.sponsor-add"                  // Rel for link that adds a sponsor
	REL_SPONSOR_UPDATE               = "/rels/pnc.sponsor-update"               // Rel for link that updates a sponsor
	REL_SPONSOR_REMOVE               = "/rels/pnc.sponsor-remove"               // Rel for link that removes a sponsor
	REL_SPONSOR_CREDIT_LINE          = "/rels/pnc.sponsor-credit-line"          // Rel for link that retrieves the credit line for a sponsor
	REL_SPONSOR_SPONSORSHIPS         = "/rels/pnc.sponsor-sponsorships"         // Rel for link that retrieves sponsorships made by a sponsor
	REL_SPONSOR_SPONSORSHIPS_BY_PAID = "/rels/pnc.sponsor-sponsorships-by-paid" // Rel for link that retrieves sponsorships by paid state
	REL_SPONSORSHIP_UPDATE           = "/rels/pnc.sponsorship-update"           // Rel for link that updates the details on a sponsorship

	SPONSOR = "sponsor"
)

/*
 Model service implementation for the sponsor model
*/
type SponsorService struct {
	*svr.AuthModelService
	dao *pd.SponsorDao
}

/*
 Creaes a new model service
*/
func NewSponsorService(router *mux.Router, store *sessions.CookieStore) *SponsorService {
	var (
		ss        = SponsorService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(SPONSOR_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path("/{id-sponsor:[0-9]+}/sponsorship/{id:[0-9]+}").HandlerFunc(ss.getSponsorship)
	subrouter.Methods(svr.GET).Path("/{id-sponsor:[0-9]+}/sponsorship").Queries(PAID, "").HandlerFunc(ss.getSponsorshipsByPaid)
	subrouter.Methods(svr.GET).Path("/{id-sponsor:[0-9]+}/sponsorship").HandlerFunc(ss.getSponsorships)
	subrouter.Methods(svr.PUT).Path("/{id-sponsor:[0-9]+}/sponsorship/{id:[0-9]+}").HandlerFunc(ss.updateSponsorship)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(ss.get)
	subrouter.Methods(svr.GET).Queries(ORG, "").HandlerFunc(ss.search)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(SPONSOR_COLLECTION)).HandlerFunc(ss.getAll)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(SPONSOR_COLLECTION)).HandlerFunc(ss.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(ss.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(ss.remove)

	return &ss
}

/*
 Initializes the services
*/
func (ss *SponsorService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	ss.dao, err = pd.NewSponsorDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_SPONSOR_COLLECTION, svr.GET, SPONSOR_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_SPONSOR_SEARCH, svr.GET, SPONSOR_COLLECTION + "?org={org}"})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_SPONSOR_ADD, svr.POST, SPONSOR_COLLECTION})
}

/*
 Retrieves a sponsor as identified by a given ID. The ID is expected in the URL path.
 Method: GET - URI: /sponsor/{id}
*/
func (ss *SponsorService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return ss.dao.Get(id) }
	ss.Get(writer, req, PRIV_SPONSOR_VIEW, get, encodeSponsor)
}

/*
 Retrieves all availble sponsors
 Method: GET - URI: /sponsor
*/
func (ss *SponsorService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var sponsors, err = ss.dao.GetAll()
		return ss.toModels(sponsors), err
	}

	ss.GetCollection(writer, req, PRIV_SPONSOR_VIEW, getAll, ss.Base().EncodeModels(encodeSponsor))
}

/*
 Searches in the catalog of available sponsors using the provided search criteria
 Method: GET - URI: /sponsor?org={org}
*/
func (ss *SponsorService) search(writer http.ResponseWriter, req *http.Request) {
	var org = req.FormValue(ORG)

	if len(org) < 1 {
		http.Error(writer, "Error: No value passed in the organization argument", svr.BAD_REQUEST)
		return
	}

	var search = func() ([]gm.Model, error) {
		var sponsors, err = ss.dao.Search(org)
		return ss.toModels(sponsors), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_VIEW, search, ss.Base().EncodeModels(encodeSponsor))
}

/*
 Adds a new sponsor to the collection. Expects the sponsor serialized in the request body
 Method: POST - URI: /sponsor
*/
func (ss *SponsorService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var sponsor = m.(*pm.Sponsor)
		return ss.dao.Add(sponsor)
	}

	ss.Add(writer, req, PRIV_SPONSOR_ADD, add, decodeSponsor, encodeSponsor)
}

/*
 Updates the details of an existing sponsor. Expects the sponsor serialized in the request body.
 Method: PUT - URI: /sponsor/{id}
*/
func (ss *SponsorService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var sponsor = m.(*pm.Sponsor)
		return ss.dao.Update(sponsor)
	}

	ss.Update(writer, req, PRIV_SPONSOR_UPDATE, update, decodeSponsor, encodeSponsor)
}

/*
 Removes a sponsor for the collection. Address and contact details are also removed.
 Methods: DELETE - URI: /sponsor/{id}
*/
func (ss *SponsorService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return ss.dao.Remove(id) }
	ss.Remove(writer, req, PRIV_SPONSOR_REMOVE, remove)
}

/*
 Retrieves a sponsorship identified by a given ID
 Method: GET - URI: /sponsor/{id-sponsor}/sponsorship/{id-sponsorship}
*/
func (ss *SponsorService) getSponsorship(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return ss.dao.GetSponsorship(id) }
	ss.Get(writer, req, PRIV_SPONSOR_SPONSORSHIP, get, encodeSponsorship)
}

/*
 Retrieves all sponsorships for a given sponsor
 Method: GET - URI: /sponsor/{id-sponsor}/sponsorship
*/
func (ss *SponsorService) getSponsorships(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSponsor, err = strconv.ParseUint(vars["id-sponsor"], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Sponsor ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var get = func() ([]gm.Model, error) {
		var sponsorships, err = ss.dao.GetSponsorships(uint(idSponsor))
		return ss.sponsorshipsToModels(sponsorships), err
	}

	ss.GetCollection(writer, req, PRIV_SPONSOR_SPONSORSHIP, get, ss.Base().EncodeModels(encodeSponsorship))
}

/*
 Retrieves all sponsorships made by a sponsor filtered by paid state
 Method: GET - URI: /sponsor/{id}/sponsorship?paid={paid}
*/
func (ss *SponsorService) getSponsorshipsByPaid(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSponsor, err = strconv.ParseUint(vars["id-sponsor"], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Sponsor ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	paid, err := strconv.ParseBool(req.FormValue(PAID))

	if err != nil {
		http.Error(writer, "Error attempting to parse Paid value: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var get = func() ([]gm.Model, error) {
		var sponsorships, err = ss.dao.GetSponsorshipsByPaid(uint(idSponsor), paid)
		return ss.sponsorshipsToModels(sponsorships), err
	}

	ss.GetCollection(writer, req, PRIV_SPONSOR_SPONSORSHIP, get, ss.Base().EncodeModels(encodeSponsorship))
}

/*
 Updates the details on a given sponsorship. The only properties that will be updates are the paid and paid date elements.
 All other changes will be ignored.
 Methods: PUT - URI: /sponsor/{id-sponsor}/sponsorship/{id-sponsorship}
*/
func (ss *SponsorService) updateSponsorship(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSponsor, err = strconv.ParseUint(vars["id-sponsor"], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Sponsor ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var update = func(m gm.Model) (uint, error) {
		var sponsorship = m.(*pm.Sponsorship)

		if sponsorship.IdSponsor() != uint(idSponsor) {
			http.Error(writer, "Sponsor ID in URI does not match Sponsor ID in sponsorship model", svr.BAD_REQUEST)
			return 0, nil
		}

		return ss.dao.UpdateSponsorship(sponsorship)
	}

	ss.Update(writer, req, PRIV_SPONSOR_SPONSORSHIP, update, decodeSponsorship, encodeSponsorship)
}

/*
 Casts a collection of sponsor objects into Model types.
*/
func (ss *SponsorService) toModels(sponsors []*pm.Sponsor) []gm.Model {
	var models = make([]gm.Model, len(sponsors))

	for cont, c := range sponsors {
		models[cont] = c
	}

	return models
}

/*
 Casts a collection of sponsorship objets into Model types
*/
func (ss *SponsorService) sponsorshipsToModels(sponsorships []*pm.Sponsorship) []gm.Model {
	var models = make([]gm.Model, len(sponsorships))

	for cont, c := range sponsorships {
		models[cont] = c
	}

	return models
}

/*
 Encodes a sponsor object into a serialized format for transport.
*/
func encodeSponsor(m gm.Model) ([]byte, error) {
	var sponsor, tok = m.(*pm.Sponsor)

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Sponsor.")
	}

	if sponsor == nil {
		return nil, nil
	}

	var address map[string]interface{}
	var sa = sponsor.Address()

	if sa != nil {
		address = map[string]interface{}{
			"Id":       sa.Id(),
			"Street1":  sa.Street1(),
			"Street2":  sa.Street2(),
			"City":     sa.City(),
			"Province": sa.Province(),
			"Country":  sa.Country(),
		}
	}

	var contacts = make([]map[string]interface{}, len(sponsor.Contacts()))
	for cont, contact := range sponsor.Contacts() {
		typeStr, err := pm.FormatContactType(contact.ContactType())

		if err != nil {
			return nil, err
		}

		contacts[cont] = map[string]interface{}{
			"Id":          contact.Id(),
			"Uuid":        contact.Uuid(),
			"ContactType": typeStr,
			"Value":       contact.Value(),
		}
	}

	var model = struct {
		Id           uint
		Organization string
		FirstName    string
		LastName     string
		Address      map[string]interface{}
		Contacts     []map[string]interface{}
	}{
		sponsor.Id(),
		sponsor.Organization(),
		sponsor.FirstName(),
		sponsor.LastName(),
		address,
		contacts,
	}

	var resource = fmt.Sprintf("%s/%d", SPONSOR_COLLECTION, sponsor.Id())
	var lineResource = fmt.Sprintf("%s?sponsor=%d", CREDIT_LINE_COLLECTION, sponsor.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_SPONSOR_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_SPONSOR_REMOVE, svr.DELETE, resource},
		svr.Link{svr.JSON, REL_SPONSOR_CREDIT_LINE, svr.GET, lineResource},
		svr.Link{svr.JSON, REL_SPONSOR_SPONSORSHIPS, svr.GET, resource + "/sponsorship"},
		svr.Link{svr.JSON, REL_SPONSOR_SPONSORSHIPS_BY_PAID, svr.GET, resource + "/sponsorship?paid={paid}"},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a sponsor object from a serialized representation to a native model object.
*/
func decodeSponsor(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 7)
	var id float64
	var organization, firstName, lastName, street1, street2, city, province, country, uuid, value, contactTypeRaw string
	var addressRaw, contactJob map[string]interface{}
	var contactsRaw []interface{}

	id, ok[0] = job["Id"].(float64)
	organization, ok[1] = job["Organization"].(string)
	firstName, ok[2] = job["FirstName"].(string)
	lastName, ok[3] = job["LastName"].(string)
	contactsRaw, ok[4] = job["Contacts"].([]interface{})
	ok[6] = true

	if job["Address"] == nil {
		ok[5] = true
	} else {
		addressRaw, ok[5] = job["Address"].(map[string]interface{})
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	sponsor, err := pm.NewSponsor(uint(id), organization, firstName, lastName)

	if err != nil {
		return nil, err
	}

	if addressRaw != nil {
		id, ok[0] = addressRaw["Id"].(float64)
		street1, ok[1] = addressRaw["Street1"].(string)
		city, ok[2] = addressRaw["City"].(string)
		province, ok[3] = addressRaw["Province"].(string)
		country, ok[4] = addressRaw["Country"].(string)
		ok[6] = true

		if addressRaw["Street2"] == nil {
			ok[5] = true
		} else {
			street2, ok[5] = addressRaw["Street2"].(string)
		}

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		address, err := pm.NewAddress(uint(id), street1, street2, city, province, country)

		if err != nil {
			return nil, err
		}

		sponsor.SetAddress(address)
	}

	ok[5], ok[6] = true, true

	for _, cr := range contactsRaw {
		contactJob, ok[0] = cr.(map[string]interface{})
		id, ok[1] = contactJob["Id"].(float64)
		uuid, ok[2] = contactJob["Uuid"].(string)
		contactTypeRaw, ok[3] = contactJob["ContactType"].(string)
		value, ok[4] = contactJob["Value"].(string)

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		contactType, err := pm.ParseContactType(contactTypeRaw)

		if err != nil {
			return nil, err
		}

		contact, err := pm.NewContact(uint(id), uuid, contactType, value)
		sponsor.Add(contact)
	}

	return sponsor, nil
}

/*
 Encodes a sponsorship object to a serialized format for transport
*/
func encodeSponsorship(m gm.Model) ([]byte, error) {
	var sponsorship, tok = m.(*pm.Sponsorship)
	var paidDate *string

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Sponsorship.")
	}

	if sponsorship == nil {
		return nil, nil
	}

	if sponsorship.PaidDate() != nil {
		var tmp = sponsorship.PaidDate().Format(gd.DATE_TIME_FORMAT)
		paidDate = &tmp
	}

	var model = struct {
		Id        uint
		IdSponsor uint
		IdInvoice uint
		Timestamp string
		Total     float32
		Sponsored float32
		Paid      bool
		PaidDate  *string
	}{
		sponsorship.Id(),
		sponsorship.IdSponsor(),
		sponsorship.IdInvoice(),
		sponsorship.Timestamp().Format(gd.DATE_TIME_FORMAT),
		sponsorship.Total(),
		sponsorship.Sponsored(),
		sponsorship.Paid(),
		paidDate,
	}

	var resource = fmt.Sprintf("%s/%d/sponsorship/%d", SPONSOR_COLLECTION, sponsorship.IdSponsor(), sponsorship.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_SPONSORSHIP_UPDATE, svr.PUT, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a sponsorship object from a serialized representation to a native model
*/
func decodeSponsorship(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 8)
	var id, idSponsor, idInvoice, total, sponsored float64
	var timestampStr, paidStr string
	var paid bool
	var paidDate *time.Time

	id, ok[0] = job["Id"].(float64)
	idSponsor, ok[1] = job["IdSponsor"].(float64)
	idInvoice, ok[2] = job["IdInvoice"].(float64)
	timestampStr, ok[3] = job["Timestamp"].(string)
	total, ok[4] = job["Total"].(float64)
	sponsored, ok[5] = job["Sponsored"].(float64)
	paid, ok[6] = job["Paid"].(bool)

	if job["PaidDate"] != nil {
		paidStr, ok[7] = job["PaidDate"].(string)
	} else {
		ok[7] = true
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	if len(paidStr) > 0 {
		tmp, err := time.Parse(gd.DATE_TIME_FORMAT, paidStr)

		if err != nil {
			return nil, err
		}

		paidDate = &tmp
	}

	sponsorship, err := pm.NewSponsorship(
		uint(id),
		uint(idSponsor),
		uint(idInvoice),
		timestamp,
		float32(total),
		float32(sponsored),
		paid,
		paidDate,
	)

	return sponsorship, err
}
