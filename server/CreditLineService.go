package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	CREDIT_LINE_COLLECTION = "/credit-line" //Credit Line service global route

	PRIV_CREDIT_LINE_VIEW    = "pnc.credit-line-view"    // Credit line view privilege
	PRIV_CREDIT_LINE_ADD     = "pnc.credit-line-add"     // Credit line add privilege
	PRIV_CREDIT_LINE_UPDATE  = "pnc.credit-line-update"  // Credit line update privilege
	PRIV_CREDIT_LINE_REMOVE  = "pnc.credit-line-remove"  // Credit line remove privilege
	PRIV_CREDIT_LINE_INVOICE = "pnc.credit-line-invoice" // Credit line invoice management privilege
	PRIV_CREDIT_LINE_PAYMENT = "pnc.credit-line-payment" // Credit line payment privilege

	REL_CREDIT_LINE                = "/rels/pnc.credit-line"                // Rel for link that retrieves a single credit line
	REL_CREDIT_LINE_UPDATE         = "/rels/pnc.credit-line-update"         // Rel for link to update a credit line
	REL_CREDIT_LINE_REMOVE         = "/rels/pnc.credit-line-remove"         // Rel for link to remove a credit line
	REL_CREDIT_LINE_LINK_INVOICE   = "/rels/pnc.credit-line-link-invoice"   // Rel for link to link an invoice to a credit line
	REL_CREDIT_LINE_UNLINK_INVOICE = "/rels/pnc.credit-line-unlink-invoice" // Rel for link to unlink an invoice from a credit line
	REL_CREDIT_LINE_ADD_PAYMENT    = "/rels/pnc.credit-line-add-payment"    // Rel for link to add a payment to a credit line
	REL_PAYMENT_BY_RANGE           = "/rels/pnc.payments-by-range"          // Rel for link to retrieve payments by date range
	REL_PAYMENT_UPDATE             = "/rels/pnc.payment-update"             // Rel for link to update a payment
	REL_PAYMENT_REMOVE             = "/rels/pnc.payment-remove"             // Rel for link to remove a payment

	INVOICE        = "invoice"        // Used to extract invoice id values from URL queries
	ID_CREDIT_LINE = "id-credit-line" // Used to extract credit line id value sfrom URL queries
)

/*
 Service implementation for the Credit Line model
*/
type CreditLineService struct {
	*svr.AuthModelService
	dao *pd.CreditLineDao
}

/*
 Creates a new inventory service and sets up internal routing
*/
func NewCreditLineService(router *mux.Router, store *sessions.CookieStore) *CreditLineService {
	var (
		cls       = CreditLineService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(CREDIT_LINE_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(cls.get)
	subrouter.Methods(svr.GET).Queries(CUSTOMER, "").HandlerFunc(cls.getByCustomer)
	subrouter.Methods(svr.GET).Queries(SUPPLIER, "").HandlerFunc(cls.getBySupplier)
	subrouter.Methods(svr.GET).Queries(SPONSOR, "").HandlerFunc(cls.getBySponsor)
	subrouter.Methods(svr.POST).Queries(CUSTOMER, "").HandlerFunc(cls.addToCustomer)
	subrouter.Methods(svr.POST).Queries(SUPPLIER, "").HandlerFunc(cls.addToSupplier)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(cls.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(cls.remove)
	subrouter.Methods(svr.POST).Path(svr.ID_PATTERN).Queries(INVOICE, "").HandlerFunc(cls.linkInvoice)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).Queries(INVOICE, "").HandlerFunc(cls.unlinkInvoice)
	subrouter.Methods(svr.GET).Path("/payment").Queries(FROM, "").Queries(TO, "").HandlerFunc(cls.getPayments)
	subrouter.Methods(svr.POST).Path(svr.ID_PATTERN + "/payment").HandlerFunc(cls.addPayment)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN + "/payment/{id-payment:[0-9]+}").HandlerFunc(cls.removePayment)

	return &cls
}

/*
 Initializes the service
*/
func (cls *CreditLineService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error

	cls.dao, err = pd.NewCreditLineDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_CREDIT_LINE, svr.GET, CREDIT_LINE_COLLECTION + "/{id}"})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_PAYMENT_BY_RANGE, svr.GET, CREDIT_LINE_COLLECTION + "/payment?from={from}&to={to}"})
}

/*
 Retrieves a credit line as identified by a given ID.
 Method: GET - URI: /credit-line/{id}
*/
func (cls *CreditLineService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return cls.dao.Get(id) }
	cls.Get(writer, req, PRIV_CREDIT_LINE_VIEW, get, encodeCreditLine)
}

/*
 Retreives a credit line that is tied to a provided customer
 Method: GET - URI: /credit-line?customer={id-customer}
*/
func (cls *CreditLineService) getByCustomer(writer http.ResponseWriter, req *http.Request) {
	var customer, err = strconv.ParseUint(req.FormValue(CUSTOMER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse customer argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByCustomer = func() (gm.Model, error) { return cls.dao.GetByCustomer(uint(customer)) }
	cls.GetSingle(writer, req, PRIV_CREDIT_LINE_VIEW, getByCustomer, encodeCreditLine)
}

/*
 Retreives a credit line that is tied to a provider supplier
 Method: GET - URI: /credit-line?supplier={id-supplier}
*/
func (cls *CreditLineService) getBySupplier(writer http.ResponseWriter, req *http.Request) {
	var supplier, err = strconv.ParseUint(req.FormValue(SUPPLIER), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse supplier argument:"+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getBySupplier = func() (gm.Model, error) { return cls.dao.GetBySupplier(uint(supplier)) }
	cls.GetSingle(writer, req, PRIV_CREDIT_LINE_VIEW, getBySupplier, encodeCreditLine)
}

/*
 Retreives a credit line that is tied to a provider sponsor
 Method: GET - URI: /credit-line?sponsor={id-sponsor}
*/
func (cls *CreditLineService) getBySponsor(writer http.ResponseWriter, req *http.Request) {
	var sponsor, err = strconv.ParseUint(req.FormValue(SPONSOR), 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse sponsor argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getBySponsor = func() (gm.Model, error) { return cls.dao.GetBySponsor(uint(sponsor)) }
	cls.GetSingle(writer, req, PRIV_CREDIT_LINE_VIEW, getBySponsor, encodeCreditLine)
}

/*
 Creates a new credit line and ties it to the provided customer
 Method: POST - URI: /credit-line?customer={id-customer}
*/
func (cls *CreditLineService) addToCustomer(writer http.ResponseWriter, req *http.Request) {
	var form, err = url.ParseQuery(req.URL.RawQuery)
	customer, err := strconv.ParseUint(form[CUSTOMER][0], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse customer form value: "+err.Error(), svr.BAD_REQUEST)
	}

	var add = func(m gm.Model) (uint, error) {
		var creditLine = m.(*pm.CreditLine)
		return cls.dao.AddToCustomer(uint(customer), creditLine)
	}

	cls.Add(writer, req, PRIV_CREDIT_LINE_ADD, add, decodeCreditLine, encodeCreditLine)
}

/*
 Creates a new credit line and ties it to the provided supplier
 Method: POST - URI: /credit-line?supplier={id-supplier}
*/
func (cls *CreditLineService) addToSupplier(writer http.ResponseWriter, req *http.Request) {
	var form, err = url.ParseQuery(req.URL.RawQuery)
	supplier, err := strconv.ParseUint(form[SUPPLIER][0], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse supplier form value: "+err.Error(), svr.BAD_REQUEST)
	}

	var add = func(m gm.Model) (uint, error) {
		var creditLine = m.(*pm.CreditLine)
		return cls.dao.AddToSupplier(uint(supplier), creditLine)
	}

	cls.Add(writer, req, PRIV_CREDIT_LINE_ADD, add, decodeCreditLine, encodeCreditLine)
}

/*
 Updates the details for a given credit line. Does not update related invoices or payments.
 Method: PUT - URI: /credit-line/{id}
*/
func (cls *CreditLineService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var creditLine = m.(*pm.CreditLine)
		return cls.dao.Update(creditLine)
	}

	cls.Update(writer, req, PRIV_CREDIT_LINE_UPDATE, update, decodeCreditLine, encodeCreditLine)
}

/*
 Removes a credit line from the collection. Diassociates any link invoices, and removes any associated payments.
 Method: DELETE - URI: /credit-line/{id}
*/
func (cls *CreditLineService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) {
		return cls.dao.Remove(id)
	}

	cls.Remove(writer, req, PRIV_CREDIT_LINE_REMOVE, remove)
}

/*
 Links an invoice to a credit line.
 Method: POST - URI: /credit-line/{id-credit-line}/invoice/{id-invoice}
*/
func (cls *CreditLineService) linkInvoice(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var creditLine, err1 = strconv.ParseUint(vars[ID_CREDIT_LINE], 10, 32)
	var invoice, err2 = strconv.ParseUint(req.FormValue(INVOICE), 10, 32)

	if err1 != nil || err2 != nil {
		http.Error(writer, "Invalid credit line and/or invoice argument(s)", svr.BAD_REQUEST)
		return
	}

	var linkInvoice = func() (uint, error) {
		return cls.dao.LinkInvoice(uint(creditLine), uint(invoice))
	}

	cls.Execute(writer, req, PRIV_CREDIT_LINE_INVOICE, linkInvoice)
}

/*
 Unlinks an invoice from a credit line.
 Method: DELETE - URI: /credit-line/{id-credit-line}/invoice/{id-invoice}
*/
func (cls *CreditLineService) unlinkInvoice(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var creditLine, err1 = strconv.ParseUint(vars[ID_CREDIT_LINE], 10, 32)
	var invoice, err2 = strconv.ParseUint(req.FormValue(INVOICE), 10, 32)

	if err1 != nil || err2 != nil {
		http.Error(writer, "Invalid credit line and/or invoice argument(s)", svr.BAD_REQUEST)
		return
	}

	var unlinkInvoice = func() (uint, error) {
		return cls.dao.UnlinkInvoice(uint(creditLine), uint(invoice))
	}

	cls.Execute(writer, req, PRIV_CREDIT_LINE_INVOICE, unlinkInvoice)
}

/*
 Retrieves all payments made within a given date/time range
 Method: GET - URI: /credit-line/payment?from={from}&to={to}
*/
func (cls *CreditLineService) getPayments(writer http.ResponseWriter, req *http.Request) {
	var from, err = time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(FROM))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'from' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	to, err := time.Parse(gd.DATE_TIME_FORMAT, req.FormValue(TO))

	if err != nil {
		http.Error(writer, "Error attempting to parse 'to' date/time argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var getByRange = func() ([]gm.Model, error) {
		var payments, err = cls.dao.GetPayments(from, to)
		return cls.toModels(payments), err
	}

	cls.GetCollection(writer, req, PRIV_CREDIT_LINE_VIEW, getByRange, cls.Base().EncodeModels(encodePayment))
}

/*
 Add a payment to an invoice
 Method: POST - URI: /credit-line/{id}/payment
*/
func (cls *CreditLineService) addPayment(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var payment = m.(*pm.Payment)
		var err = checkPayment(writer, req, payment)

		if err != nil {
			return 0, err
		}

		id, err := cls.dao.AddPayment(payment)

		if err != nil && err == pd.CREDIT_LINE_LIMIT {
			return 0, gd.PRECONDITION_FAILED
		}

		return id, err
	}

	cls.Add(writer, req, PRIV_CREDIT_LINE_PAYMENT, add, decodePayment, encodePayment)
}

/*
 Removes a payment
 Method: DELETE - URI: /credit-line/{id-credit-line}/payment/{id-payment}
*/
func (cls *CreditLineService) removePayment(writer http.ResponseWriter, req *http.Request) {
	var id, err = strconv.ParseUint(mux.Vars(req)["id-payment"], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the payment argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var remove = func() (uint, error) { return cls.dao.RemovePayment(uint(id)) }
	cls.Execute(writer, req, PRIV_CREDIT_LINE_PAYMENT, remove)
}

/*
 Casts a slice of payment objects into model type
*/
func (cls *CreditLineService) toModels(payments []*pm.Payment) []gm.Model {
	var models = make([]gm.Model, len(payments))

	for cont, payment := range payments {
		models[cont] = payment
	}

	return models
}

/*
 Encodes a credit line into a serialized representation for transport. Adds rest links.
 All child payments are encoded. Child invoices are encoded, but not invoice items.
*/
func encodeCreditLine(m gm.Model) ([]byte, error) {
	var creditLine, tok = m.(*pm.CreditLine)
	var resource string

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type of Credit Line")
	}

	if creditLine == nil {
		return nil, nil
	}

	var invoices = make([]map[string]interface{}, len(creditLine.Invoices()))
	var payments = make([]map[string]interface{}, len(creditLine.Payments()))

	var model = struct {
		Id       uint
		Limit    float32
		Balance  float32
		Active   bool
		Invoices []map[string]interface{}
		Payments []map[string]interface{}
	}{
		creditLine.Id(),
		creditLine.Limit(),
		creditLine.Balance(),
		creditLine.Active(),
		invoices,
		payments,
	}

	var err = make([]error, 3)
	var transactionStr, paymentStr, stateStr string

	for cont, invoice := range creditLine.Invoices() {
		transactionStr, err[0] = pm.FormatTransactionType(invoice.Transaction())
		paymentStr, err[1] = pm.FormatPaymentType(invoice.Payment())
		stateStr, err[2] = pm.FormatStateType(invoice.State())

		for _, e := range err {
			if e != nil {
				return nil, e
			}
		}

		invoices[cont] = map[string]interface{}{
			"Id":          invoice.Id(),
			"IdUser":      invoice.IdUser(),
			"Transaction": transactionStr,
			"Payment":     paymentStr,
			"State":       stateStr,
			"Timestamp":   invoice.Timestamp().Format(gd.DATE_TIME_FORMAT),
			"Total":       invoice.Total(),
			"Items":       make([]map[string]interface{}, 0),
		}
	}

	for cont, payment := range creditLine.Payments() {
		resource = fmt.Sprintf("%s/%d/payment/%d", CREDIT_LINE_COLLECTION, payment.IdCreditLine(), payment.Id())
		var links = []svr.Link{
			svr.Link{svr.JSON, REL_PAYMENT_UPDATE, svr.PUT, resource},
			svr.Link{svr.PLAIN, REL_PAYMENT_REMOVE, svr.DELETE, resource},
		}

		payments[cont] = map[string]interface{}{
			resource: map[string]interface{}{
				svr.MODEL: map[string]interface{}{
					"Id":           payment.Id(),
					"IdCreditLine": payment.IdCreditLine(),
					"IdUser":       payment.IdUser(),
					"Timestamp":    payment.Timestamp().Format(gd.DATE_TIME_FORMAT),
					"Amount":       payment.Amount(),
					"Note":         payment.Note(),
				},
				svr.LINKS: links,
			},
		}
	}

	resource = fmt.Sprintf("%s/%d", CREDIT_LINE_COLLECTION, creditLine.Id())
	var invoiceResource = fmt.Sprintf("%s/%d?invoice={invoice}", CREDIT_LINE_COLLECTION, creditLine.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_CREDIT_LINE_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_CREDIT_LINE_REMOVE, svr.DELETE, resource},
		svr.Link{svr.PLAIN, REL_CREDIT_LINE_LINK_INVOICE, svr.POST, invoiceResource},
		svr.Link{svr.PLAIN, REL_CREDIT_LINE_UNLINK_INVOICE, svr.DELETE, invoiceResource},
		svr.Link{svr.PLAIN, REL_CREDIT_LINE_ADD_PAYMENT, svr.POST, resource + "/payment"},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a credit line from a serialized representation to a native model object
*/
func decodeCreditLine(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 4)
	var id, limit, balance float64
	var active bool

	id, ok[0] = job["Id"].(float64)
	limit, ok[1] = job["Limit"].(float64)
	balance, ok[2] = job["Balance"].(float64)
	active, ok[3] = job["Active"].(bool)

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	return pm.NewCreditLine(
		uint(id),
		float32(limit),
		float32(balance),
		active,
	)
}

/*
 Encodes a payment model into a serialized representation for transport
*/
func encodePayment(m gm.Model) ([]byte, error) {
	var payment, tok = m.(*pm.Payment)

	if !tok {
		return nil, errors.New("Cannot decode object. Not of type payment.")
	}

	if payment == nil {
		return nil, nil
	}

	var model = struct {
		Id           uint
		IdCreditLine uint
		IdUser       uint
		Timestamp    string
		Amount       float32
		Note         string
	}{
		payment.Id(),
		payment.IdCreditLine(),
		payment.IdUser(),
		payment.Timestamp().Format(gd.DATE_TIME_FORMAT),
		payment.Amount(),
		payment.Note(),
	}

	var resource = fmt.Sprintf("%s/%d/payment/%d", CREDIT_LINE_COLLECTION, payment.IdCreditLine(), payment.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_PAYMENT_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_PAYMENT_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a serialized representation of a payment into a native model object
*/
func decodePayment(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 6)
	var id, idCreditLine, idUser, amount float64
	var timestampStr, note string

	id, ok[0] = job["Id"].(float64)
	idCreditLine, ok[1] = job["IdCreditLine"].(float64)
	idUser, ok[2] = job["IdUser"].(float64)
	timestampStr, ok[3] = job["Timestamp"].(string)
	amount, ok[4] = job["Amount"].(float64)
	note, ok[5] = job["Note"].(string)

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return pm.NewPayment(
		uint(id),
		uint(idCreditLine),
		uint(idUser),
		timestamp,
		float32(amount),
		note,
	)
}

/*
 Verifies that the payment belongs to the current credit line. If not an error is raised.
*/
func checkPayment(writer http.ResponseWriter, req *http.Request, payment *pm.Payment) error {
	var creditLine, err = strconv.ParseUint(mux.Vars(req)[svr.ID], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse credit line ID argument: "+err.Error(), svr.BAD_REQUEST)
		return err
	}

	if uint(creditLine) != payment.IdCreditLine() {
		http.Error(writer, "Credit Line ID on the payment does not match the credit line", svr.BAD_REQUEST)
		return err
	}

	return nil
}
