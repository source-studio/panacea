package server

import (
	gd "gxs3/godfather/data"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	"time"
)

/*
 Monitor that checks on products with expired invenvtory. If expired inventory is found, notifications are created to
 alert users which products are expired. The monitor will check the data once a day.
*/
type ExpirationMonitor struct {
	dao    *pd.ProductDao // DAO used to access product data
	ticker *time.Ticker   // Ticker used to run the scheduler.
}

/*
 Creates a new expiration monitor
*/
func NewExpirationMonitor() *ExpirationMonitor {
	var em = new(ExpirationMonitor)
	em.ticker = time.NewTicker(time.Hour * 24)

	return em
}

/**
 * Initializes the expiration monitor, and kicks off the scheduler which will run the data checking task once a day.
 */
func (em *ExpirationMonitor) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error

	em.dao, err = pd.NewProductDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	go em.monitor()
}

/*
 Monitoring process. Will check if there is expired product, and then wait for the next scheduler event.
*/
func (em *ExpirationMonitor) monitor() {
	for {
		em.dao.NotifyExpired()
		<-em.ticker.C
	}
}
