package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	gd "gxs3/godfather/data"
	gm "gxs3/godfather/model"
	svr "gxs3/godfather/server"
	pd "gxs3/panacea/data"
	pm "gxs3/panacea/model"
	"net/http"
	"strconv"
	"time"
)

const (
	SUPPLIER_COLLECTION = "/supplier" // Defines the supplier global route

	PRIV_SUPPLIER_VIEW        = "pnc.supplier-view"        // Supplier view privilege
	PRIV_SUPPLIER_ADD         = "pnc.supplier-add"         // Supplier add privilege
	PRIV_SUPPLIER_UPDATE      = "pnc.supplier-update"      // Supplier update privilege
	PRIV_SUPPLIER_REMOVE      = "pnc.supplier-remove"      // Supplier remove privilege
	PRIV_SUPPLIER_ORDER       = "pnc.supplier-order"       // Supplier order privilege
	PRIV_SUPPLIER_CREDIT_NOTE = "pnc.supplier-credit-note" // Supplier credit note privilege
	PRIV_SUPPLIER_PRODUCT     = "pnc.supplier-product"     // Supplier product privilege

	REL_SUPPLIER_COLLECTION               = "/rels/pnc.supplier-collection"               // Rel for link to retreive supplier collection
	REL_SUPPLIER_SEARCH                   = "/rels/pnc.supplier-search"                   // Rel for link that searches suppliers
	REL_SUPPLIER_ADD                      = "/rels/pnc.supplier-add"                      // Rel for link to add a supplier
	REL_SUPPLIER_UPDATE                   = "/rels/pnc.supplier-update"                   // Rel for link to update a supplier
	REL_SUPPLIER_REMOVE                   = "/rels/pnc.supplier-remove"                   // Rel for link to remove a supplier
	REL_SUPPLIER_ORDERS                   = "/rels/pnc.supplier-orders"                   // Rel for link to retreive orders made to a supplier
	REL_SUPPLIER_ORDERS_BY_PAID           = "/rels/pnc.supplier-orders-by-paid"           // Rel for link that retrieves orders made by a supplier by paid state
	REL_SUPPLIER_CREDIT_NOTES             = "/rels/pnc.supplier-credit-notes"             // Rel for link to retrieve credit notes owed by a supplier
	REL_SUPPLIER_CREDIT_NOTES_BY_REDEEMED = "/rels/pnc.supplier-credit-notes-by-redeemed" // Rel for link to retrieve credit notes owed by redeemed state
	REL_SUPPLIER_ORDER_ADD                = "/rels/pnc.supplier-order-add"                // Rel for link to add an order to a supplier
	REL_ORDER_UPDATE                      = "/rels/pnc.order-update"                      // Rel for link to update an order
	REL_ORDER_REMOVE                      = "/rels/pnc.order-remove"                      // Rel for link to remove an order
	REL_CREDIT_NOTE_ADD                   = "/rels/pnc.credit-note-add"                   // Rel for link to add a credit note
	REL_CREDIT_NOTE_UPDATE                = "/rels/pnc.credit-note-update"                // Rel for link to update a credit note
	REL_CREDIT_NOTE_REMOVE                = "/rels/pnc.credit-note-remove"                // Rel for link to remove a credit note
	REL_SUPPLIER_PRODUCTS                 = "/rels/pnc.supplier-product"                  // Rel that retrieves products bought from a supplier
	REL_SUPPLIER_PRODUCT_ADD              = "/rels/pnc.supplier-product-add"              // Rel that associates a supplier with a product
	REL_SUPPLIER_PRODUCT_REMOVE           = "/rels/pnc.supplier-product-remove"           // Rel that removes the association between a supplier and product
	REL_SUPPLIER_PRODUCT_SEARCH           = "/rels/pnc.supplier-product-search"           // Rel that searches produces associated witha supplier

	ORG         = "org"         // Used to extract organization values from URI queries
	ID_SUPPLIER = "id-supplier" // Used to extract supplier ID values from URI queries
	PAID        = "paid"        // Used to extract paid values from URI queries
	REDEEMED    = "redeemed"    // Used to extract redeemed values from URI queries
)

/*
 Service implementation for the supplier model
*/
type SupplierService struct {
	*svr.AuthModelService
	dao *pd.SupplierDao // Data access objecct for database transactions
}

/*
 Creates a new supplier service
*/
func NewSupplierService(router *mux.Router, store *sessions.CookieStore) *SupplierService {
	var (
		ss        = SupplierService{svr.NewAuthModelService(router, store), nil}
		subrouter = router.PathPrefix(SUPPLIER_COLLECTION).Subrouter()
	)

	subrouter.Methods(svr.GET).Path("/{id-supplier:[0-9]+}/order").Queries(PAID, "").HandlerFunc(ss.getOrdersByPaid)
	subrouter.Methods(svr.GET).Path("/{id-supplier:[0-9]+}/order").HandlerFunc(ss.getOrders)
	subrouter.Methods(svr.POST).Path("/{id-supplier:[0-9]+}/order").HandlerFunc(ss.addOrder)
	subrouter.Methods(svr.PUT).Path("/{id-supplier:[0-9]+}/order/{id:[0-9]+}").HandlerFunc(ss.updateOrder)
	subrouter.Methods(svr.DELETE).Path("/{id-supplier:[0-9]+}/order/{id:[0-9]+}").HandlerFunc(ss.removeOrder)

	subrouter.Methods(svr.GET).Path("/{id-supplier:[0-9]+}/credit-note").Queries(REDEEMED, "").HandlerFunc(ss.getCreditNotesByRedeemed)
	subrouter.Methods(svr.GET).Path("/{id-supplier:[0-9]+}/credit-note").HandlerFunc(ss.getCreditNotes)
	subrouter.Methods(svr.POST).Path("/{id-supplier:[0-9]+}/credit-note").HandlerFunc(ss.addCreditNote)
	subrouter.Methods(svr.PUT).Path("/{id-supplier:[0-9]+}/credit-note/{id:[0-9]+}").HandlerFunc(ss.updateCreditNote)
	subrouter.Methods(svr.DELETE).Path("/{id-supplier:[0-9]+}/credit-note/{id:[0-9]+}").HandlerFunc(ss.removeCreditNote)

	subrouter.Methods(svr.POST).Path("/{id-supplier:[0-9]+}/product/{id:[0-9]+}").HandlerFunc(ss.addProduct)
	subrouter.Methods(svr.DELETE).Path("/{id-supplier:[0-9]+}/product/{id:[0-9]+}").HandlerFunc(ss.removeProduct)

	subrouter.Methods(svr.GET).Path(svr.ID_PATTERN).HandlerFunc(ss.get)
	subrouter.Methods(svr.GET).MatcherFunc(svr.ExactMatch(SUPPLIER_COLLECTION)).HandlerFunc(ss.getAll)
	subrouter.Methods(svr.GET).Queries(ORG, "").HandlerFunc(ss.search)
	subrouter.Methods(svr.POST).MatcherFunc(svr.ExactMatch(SUPPLIER_COLLECTION)).HandlerFunc(ss.add)
	subrouter.Methods(svr.PUT).Path(svr.ID_PATTERN).HandlerFunc(ss.update)
	subrouter.Methods(svr.DELETE).Path(svr.ID_PATTERN).HandlerFunc(ss.remove)

	return &ss
}

/*
 Initializes the service
*/
func (ss *SupplierService) Init(context *svr.Context) {
	var template = context.Get(svr.DB_TEMPLATE).(*gd.SqlTemplate)
	var dictionary = context.Get(svr.DB_DICTIONARY).(map[string]string)
	var err error
	ss.dao, err = pd.NewSupplierDao(template, dictionary)

	if err != nil {
		panic(err.Error())
	}

	svr.RegisterLink(&svr.Link{svr.JSON, REL_SUPPLIER_COLLECTION, svr.GET, SUPPLIER_COLLECTION})
	svr.RegisterLink(&svr.Link{svr.JSON, REL_SUPPLIER_SEARCH, svr.GET, SUPPLIER_COLLECTION + "?org={org}"})
	svr.RegisterLink(&svr.Link{svr.PLAIN, REL_SUPPLIER_ADD, svr.POST, SUPPLIER_COLLECTION})
}

/*
 Retrieves a supplier identified by a given ID
 Method: GET - URI: /supplier/{id}
*/
func (ss *SupplierService) get(writer http.ResponseWriter, req *http.Request) {
	var get = func(id uint) (gm.Model, error) { return ss.dao.Get(id) }
	ss.Get(writer, req, PRIV_SUPPLIER_VIEW, get, encodeSupplier)
}

/*
 Retrieves all suppliers in the collection
 Method: GET - URI: /supplier
*/
func (ss *SupplierService) getAll(writer http.ResponseWriter, req *http.Request) {
	var getAll = func() ([]gm.Model, error) {
		var suppliers, err = ss.dao.GetAll()
		return ss.toModels(suppliers), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_VIEW, getAll, ss.Base().EncodeModels(encodeSupplier))
}

/*
 Searches suppliers by their organization name
 Method: GET - URI: /supplier?org={org}
*/
func (ss *SupplierService) search(writer http.ResponseWriter, req *http.Request) {
	var org = req.FormValue(ORG)

	if len(org) < 1 {
		http.Error(writer, "Error: No value passed in the organization argument", svr.BAD_REQUEST)
		return
	}

	var search = func() ([]gm.Model, error) {
		var suppliers, err = ss.dao.Search(org)
		return ss.toModels(suppliers), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_VIEW, search, ss.Base().EncodeModels(encodeSupplier))
}

/*
 Adds a new supplier to the collection. Expects the supplier to add serialized in the request body.
 Method: POST - URI: /supplier
*/
func (ss *SupplierService) add(writer http.ResponseWriter, req *http.Request) {
	var add = func(m gm.Model) (uint, error) {
		var supplier = m.(*pm.Supplier)
		return ss.dao.Add(supplier)
	}

	ss.Add(writer, req, PRIV_SUPPLIER_ADD, add, decodeSupplier, encodeSupplier)
}

/*
 Updats the details for an existing supplier. Expects the updated supplier representation in the request body.
 Method: PUT - URI: /supplier/{id}
*/
func (ss *SupplierService) update(writer http.ResponseWriter, req *http.Request) {
	var update = func(m gm.Model) (uint, error) {
		var supplier = m.(*pm.Supplier)
		return ss.dao.Update(supplier)
	}

	ss.Update(writer, req, PRIV_SUPPLIER_UPDATE, update, decodeSupplier, encodeSupplier)
}

/*
 Removes a supplier from the collection.
 Method: DELETE - URI: /supplier/{id}
*/
func (ss *SupplierService) remove(writer http.ResponseWriter, req *http.Request) {
	var remove = func(id uint) (uint, error) { return ss.dao.Remove(id) }
	ss.Remove(writer, req, PRIV_SUPPLIER_REMOVE, remove)
}

/*
 Retrieves all existing orders for a given supplier
 Method: GET - URI: /supplier/{id}/order
*/
func (ss *SupplierService) getOrders(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var get = func() ([]gm.Model, error) {
		var orders, err = ss.dao.GetOrders(uint(idSupplier))
		return ss.ordersToModels(orders), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_ORDER, get, ss.Base().EncodeModels(encodeOrder))
}

/*
 Retrieves orders for a supplier and a paid state
 Method: GET - URI: /supplier/{id}/order?paid={paid}
*/
func (ss *SupplierService) getOrdersByPaid(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	paid, err := strconv.ParseBool(req.FormValue(PAID))

	if err != nil {
		http.Error(writer, "Error attempting to parse paid argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var get = func() ([]gm.Model, error) {
		var orders, err = ss.dao.GetOrdersByPaid(uint(idSupplier), paid)
		return ss.ordersToModels(orders), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_ORDER, get, ss.Base().EncodeModels(encodeOrder))
}

/*
 Adds an order to th ecollection
 Method: GET - URI: /supplier/{id}/order
*/
func (ss *SupplierService) addOrder(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var add = func(m gm.Model) (uint, error) {
		var order = m.(*pm.Order)

		if order.IdSupplier() != uint(idSupplier) {
			return 0, errors.New("Supplier ID in URI does not match Supplier ID in order model.")
		}

		return ss.dao.AddOrder(order)
	}

	ss.Add(writer, req, PRIV_SUPPLIER_ORDER, add, decodeOrder, encodeOrder)
}

/*
 Update the details of an existing order
 Method: UPDATE - URI: /supplier/{id-supplier}/order/{id-order}
*/
func (ss *SupplierService) updateOrder(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var update = func(m gm.Model) (uint, error) {
		var order = m.(*pm.Order)

		if order.IdSupplier() != uint(idSupplier) {
			http.Error(writer, "Supplier ID in URI does not match Supplier ID in order model", svr.BAD_REQUEST)
			return 0, nil
		}

		return ss.dao.UpdateOrder(order)
	}

	ss.Update(writer, req, PRIV_SUPPLIER_ORDER, update, decodeOrder, encodeOrder)
}

/*
 Remove an existing order from the collection
 Method: DELETE - URI: /supplier/{id-supplier}/order/{id-order}
*/
func (ss *SupplierService) removeOrder(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idOrder, err = strconv.ParseUint(vars[svr.ID], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var remove = func() (uint, error) { return ss.dao.RemoveOrder(uint(idOrder)) }
	ss.Execute(writer, req, PRIV_SUPPLIER_ORDER, remove)
}

/*
 Retrieves all existing credit notes for a given supplier
 Method: GET - URI: /supplier/{id}/credit-note
*/
func (ss *SupplierService) getCreditNotes(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var arg, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var idSupplier = uint(arg)

	var get = func() ([]gm.Model, error) {
		var notes, err = ss.dao.GetCreditNotes(idSupplier)
		return ss.notesToModels(notes), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_CREDIT_NOTE, get, ss.Base().EncodeModels(encodeNote))
}

/*
 Retrieves credit notes owed by a given supplier filtered by redeemed state
 Method: GET - URI: /supplier/{id}/credit-note?redeemed={redeemed}
*/
func (ss *SupplierService) getCreditNotesByRedeemed(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	redeemed, err := strconv.ParseBool(req.FormValue(REDEEMED))

	if err != nil {
		http.Error(writer, "Error attempting to parse redeemed argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var get = func() ([]gm.Model, error) {
		var notes, err = ss.dao.GetCreditNotesByRedeemed(uint(idSupplier), redeemed)
		return ss.notesToModels(notes), err
	}

	ss.GetCollection(writer, req, PRIV_SUPPLIER_CREDIT_NOTE, get, ss.Base().EncodeModels(encodeNote))
}

/*
 Adds a credit note to the collection of notes for the given supplier
 Method: POST - URI: /supplier/{id}/credit-note
*/
func (ss *SupplierService) addCreditNote(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var add = func(m gm.Model) (uint, error) {
		var note = m.(*pm.CreditNote)

		if note.IdSupplier() != uint(idSupplier) {
			return 0, errors.New("Supplier ID in URI does not match Supplier ID in credit note model.")
		}

		return ss.dao.AddCreditNote(note)
	}

	ss.Add(writer, req, PRIV_SUPPLIER_CREDIT_NOTE, add, decodeNote, encodeNote)
}

/*
 Update the details of an existing credit note
 Method: UPDATE - URI: /supplier/{id-supplier}/credit-note/{id-credit-note}
*/
func (ss *SupplierService) updateCreditNote(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var update = func(m gm.Model) (uint, error) {
		var note = m.(*pm.CreditNote)

		if note.IdSupplier() != uint(idSupplier) {
			http.Error(writer, "Supplier ID in URI does not match Supplier ID in credit note model", svr.BAD_REQUEST)
			return 0, nil
		}

		return ss.dao.UpdateCreditNote(note)
	}

	ss.Update(writer, req, PRIV_SUPPLIER_CREDIT_NOTE, update, decodeNote, encodeNote)
}

/*
 Removes a credit note from the supplier's collection
 Method: DELETE - URI: /supplier/{id-supplier}/credit-note/{id-credit-note}
*/
func (ss *SupplierService) removeCreditNote(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idNote, err = strconv.ParseUint(vars[svr.ID], 10, 32)

	if err != nil {
		http.Error(writer, "Error attempting to parse the ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var remove = func() (uint, error) { return ss.dao.RemoveCreditNote(uint(idNote)) }
	ss.Execute(writer, req, PRIV_SUPPLIER_CREDIT_NOTE, remove)
}

/*
 Associates a product with a supplier indicating the product can be purchased from said supplier.
 Method: POST - URI: /supplier/{id-supplier}/product/{id-product}
*/
func (ss *SupplierService) addProduct(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	idProduct, err := strconv.ParseUint(vars[svr.ID], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the Product ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var add = func() (uint, error) { return ss.dao.AddProduct(uint(idSupplier), uint(idProduct)) }
	ss.Execute(writer, req, PRIV_SUPPLIER_PRODUCT, add)
}

/*
 Removes the association between a supplier and product indicating the product can/will no longer be bought from said supplier.
 Method: DELETE - URI: /supplier/{id-supplier}/product/{id-product}
*/
func (ss *SupplierService) removeProduct(writer http.ResponseWriter, req *http.Request) {
	var vars = mux.Vars(req)
	var idSupplier, err = strconv.ParseUint(vars[ID_SUPPLIER], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the Supplier ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	idProduct, err := strconv.ParseUint(vars[svr.ID], 10, 32)

	if err != nil {
		http.Error(writer, "Error parsing the Product ID argument: "+err.Error(), svr.BAD_REQUEST)
		return
	}

	var add = func() (uint, error) { return ss.dao.RemoveProduct(uint(idSupplier), uint(idProduct)) }
	ss.Execute(writer, req, PRIV_SUPPLIER_PRODUCT, add)
}

/*
 Casts a collection of supplier objects to Model type
*/
func (ss *SupplierService) toModels(suppliers []*pm.Supplier) []gm.Model {
	var models = make([]gm.Model, len(suppliers))

	for cont, supplier := range suppliers {
		models[cont] = supplier
	}

	return models
}

/*
 Casts a collection of order objects into Model type
*/
func (ss *SupplierService) ordersToModels(orders []*pm.Order) []gm.Model {
	var models = make([]gm.Model, len(orders))

	for cont, order := range orders {
		models[cont] = order
	}

	return models
}

/*
 Casts a collection of credit note objects into Model type
*/
func (ss *SupplierService) notesToModels(notes []*pm.CreditNote) []gm.Model {
	var models = make([]gm.Model, len(notes))

	for cont, note := range notes {
		models[cont] = note
	}

	return models
}

/*
 Encodes a supplier object to a serialized representation for transport
*/
func encodeSupplier(m gm.Model) ([]byte, error) {
	var supplier, tok = m.(*pm.Supplier)

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Supplier.")
	}

	if supplier == nil {
		return nil, nil
	}

	var contacts = make([]map[string]interface{}, len(supplier.Contacts()))
	for cont, contact := range supplier.Contacts() {
		typeStr, err := pm.FormatContactType(contact.ContactType())

		if err != nil {
			return nil, err
		}

		contacts[cont] = map[string]interface{}{
			"Id":          contact.Id(),
			"Uuid":        contact.Uuid(),
			"ContactType": typeStr,
			"Value":       contact.Value(),
		}
	}

	var model = struct {
		Id           uint
		Organization string
		FirstName    string
		LastName     string
		Contacts     []map[string]interface{}
	}{
		supplier.Id(),
		supplier.Organization(),
		supplier.FirstName(),
		supplier.LastName(),
		contacts,
	}

	var resource = fmt.Sprintf("%s/%d", SUPPLIER_COLLECTION, supplier.Id())
	var orderResource = fmt.Sprintf("%s/%d/order", SUPPLIER_COLLECTION, supplier.Id())
	var noteResource = fmt.Sprintf("%s/%d/credit-note", SUPPLIER_COLLECTION, supplier.Id())
	var productResource = fmt.Sprintf("%s?supplier=%d", PRODUCT_COLLECTION, supplier.Id())
	var reportResource = fmt.Sprintf("%s/inventory?supplier=%d", REPORT_COLLECTION, supplier.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_SUPPLIER_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_SUPPLIER_REMOVE, svr.DELETE, resource},
		svr.Link{svr.JSON, REL_SUPPLIER_ORDERS, svr.GET, orderResource},
		svr.Link{svr.JSON, REL_SUPPLIER_ORDERS_BY_PAID, svr.GET, orderResource + "?paid={paid}"},
		svr.Link{svr.JSON, REL_SUPPLIER_ORDER_ADD, svr.POST, orderResource},
		svr.Link{svr.JSON, REL_SUPPLIER_CREDIT_NOTES, svr.GET, noteResource},
		svr.Link{svr.JSON, REL_SUPPLIER_CREDIT_NOTES_BY_REDEEMED, svr.GET, noteResource + "?redeemed={redeemed}"},
		svr.Link{svr.JSON, REL_CREDIT_NOTE_ADD, svr.POST, noteResource},
		svr.Link{svr.JSON, REL_SUPPLIER_PRODUCTS, svr.GET, productResource},
		svr.Link{svr.JSON, REL_SUPPLIER_PRODUCT_SEARCH, svr.GET, productResource + "&name={name}&brand={brand}"},
		svr.Link{svr.JSON, REL_SUPPLIER_PRODUCT_ADD, svr.POST, resource + "/product/{product}"},
		svr.Link{svr.JSON, REL_SUPPLIER_PRODUCT_REMOVE, svr.DELETE, resource + "/product/{product}"},
		svr.Link{svr.JSON, REL_REPORT_INVENTORY_SUPPLIER, svr.GET, reportResource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a supplier representation into a native model object
*/
func decodeSupplier(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 5)
	var id float64
	var organization, firstName, lastName, uuid, value, contactTypeRaw string
	var contactsRaw []interface{}
	var contactJob map[string]interface{}

	id, ok[0] = job["Id"].(float64)
	organization, ok[1] = job["Organization"].(string)
	firstName, ok[2] = job["FirstName"].(string)
	lastName, ok[3] = job["LastName"].(string)
	contactsRaw, ok[4] = job["Contacts"].([]interface{})

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	supplier, err := pm.NewSupplier(uint(id), organization, firstName, lastName)

	if err != nil {
		return nil, err
	}

	for _, cr := range contactsRaw {
		contactJob, ok[0] = cr.(map[string]interface{})
		id, ok[1] = contactJob["Id"].(float64)
		uuid, ok[2] = contactJob["Uuid"].(string)
		contactTypeRaw, ok[3] = contactJob["ContactType"].(string)
		value, ok[4] = contactJob["Value"].(string)

		for _, val := range ok {
			if !val {
				return nil, svr.INVALID_PAYLOAD
			}
		}

		contactType, err := pm.ParseContactType(contactTypeRaw)

		if err != nil {
			return nil, err
		}

		contact, err := pm.NewContact(uint(id), uuid, contactType, value)
		supplier.Add(contact)
	}

	return supplier, nil
}

/*
 Creates a serialized representation of an order for transport
*/
func encodeOrder(m gm.Model) ([]byte, error) {
	var order, tok = m.(*pm.Order)
	var paidStr *string

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type Order.")
	}

	if order == nil {
		return nil, nil
	}

	if order.PaidDate() != nil {
		var tmp = order.PaidDate().Format(gd.DATE_TIME_FORMAT)
		paidStr = &tmp
	}

	var model = struct {
		Id         uint
		IdSupplier uint
		Timestamp  string
		Reference  string
		Amount     float32
		Paid       bool
		PaidDate   *string
	}{
		order.Id(),
		order.IdSupplier(),
		order.Timestamp().Format(gd.DATE_TIME_FORMAT),
		order.Reference(),
		order.Amount(),
		order.Paid(),
		paidStr,
	}

	var resource = fmt.Sprintf("%s/%d/order/%d", SUPPLIER_COLLECTION, order.IdSupplier(), order.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_ORDER_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_ORDER_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Translates a serialized represenation of an order into a native model object
*/
func decodeOrder(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 7)
	var id, idSupplier, amount float64
	var timestampStr, reference, paidStr string
	var paid bool
	var paidDate *time.Time

	id, ok[0] = job["Id"].(float64)
	idSupplier, ok[1] = job["IdSupplier"].(float64)
	timestampStr, ok[2] = job["Timestamp"].(string)
	reference, ok[3] = job["Reference"].(string)
	amount, ok[4] = job["Amount"].(float64)
	paid, ok[5] = job["Paid"].(bool)

	if job["PaidDate"] != nil {
		paidStr, ok[6] = job["PaidDate"].(string)
	} else {
		ok[6] = true
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	if len(paidStr) > 0 {
		tmp, err := time.Parse(gd.DATE_TIME_FORMAT, paidStr)

		if err != nil {
			return nil, err
		}

		paidDate = &tmp
	}

	order, err := pm.NewOrder(
		uint(id),
		uint(idSupplier),
		timestamp,
		reference,
		float32(amount),
		paid,
		paidDate,
	)

	return order, err
}

/*
 Encodes a credit note object into a serialized representation for transport
*/
func encodeNote(m gm.Model) ([]byte, error) {
	var note, tok = m.(*pm.CreditNote)
	var redeemedStr *string

	if !tok {
		return nil, errors.New("Cannot encode object. Not of type CreditNote.")
	}

	if note == nil {
		return nil, nil
	}

	if note.RedeemedDate() != nil {
		var tmp = note.RedeemedDate().Format(gd.DATE_TIME_FORMAT)
		redeemedStr = &tmp
	}

	var model = struct {
		Id           uint
		IdSupplier   uint
		Timestamp    string
		Reference    string
		Amount       float32
		Reason       string
		Redeemed     bool
		RedeemedDate *string
	}{
		note.Id(),
		note.IdSupplier(),
		note.Timestamp().Format(gd.DATE_TIME_FORMAT),
		note.Reference(),
		note.Amount(),
		note.Reason(),
		note.Redeemed(),
		redeemedStr,
	}

	var resource = fmt.Sprintf("%s/%d/credit-note/%d", SUPPLIER_COLLECTION, note.IdSupplier(), note.Id())
	var links = []svr.Link{
		svr.Link{svr.JSON, REL_CREDIT_NOTE_UPDATE, svr.PUT, resource},
		svr.Link{svr.PLAIN, REL_CREDIT_NOTE_REMOVE, svr.DELETE, resource},
	}

	var job = map[string]interface{}{
		resource: map[string]interface{}{
			svr.MODEL: model,
			svr.LINKS: links,
		},
	}

	return json.Marshal(job)
}

/*
 Decodes a representation of a credit note into a native model object
*/
func decodeNote(message []byte) (gm.Model, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 8)
	var id, idSupplier, amount float64
	var timestampStr, reference, reason, redeemedStr string
	var redeemed bool
	var redeemedDate *time.Time

	id, ok[0] = job["Id"].(float64)
	idSupplier, ok[1] = job["IdSupplier"].(float64)
	timestampStr, ok[2] = job["Timestamp"].(string)
	reference, ok[3] = job["Reference"].(string)
	amount, ok[4] = job["Amount"].(float64)
	reason, ok[5] = job["Reason"].(string)
	redeemed, ok[6] = job["Redeemed"].(bool)

	if job["RedeemedDate"] != nil {
		redeemedStr, ok[7] = job["RedeemedDate"].(string)
	} else {
		ok[7] = true
	}

	for _, val := range ok {
		if !val {
			return nil, svr.INVALID_PAYLOAD
		}
	}

	timestamp, err := time.Parse(gd.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	if len(redeemedStr) > 0 {
		tmp, err := time.Parse(gd.DATE_TIME_FORMAT, redeemedStr)

		if err != nil {
			return nil, err
		}

		redeemedDate = &tmp
	}

	note, err := pm.NewCreditNote(
		uint(id),
		uint(idSupplier),
		timestamp,
		reference,
		float32(amount),
		reason,
		redeemed,
		redeemedDate,
	)

	return note, err
}
