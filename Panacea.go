package main

import (
	"fmt"
	_ "github.com/gwenn/gosqlite"
	"gxs3/godfather/server"
	ps "gxs3/panacea/server"
)

func main() {
	var (
		s      = server.NewServer()
		router = s.Router()
		store  = s.Store()
	)

	s.AddService(server.NewAuthService(router, store))
	s.AddService(server.NewUserService(router, store))
	s.AddService(server.NewGroupService(router, store))
	s.AddService(server.NewPrivilegeService(router, store))
	s.AddService(ps.NewSubscriptionService(router, store))
	s.AddService(ps.NewNotificationService(router, store))
	s.AddService(ps.NewProductService(router, store))
	s.AddService(ps.NewInventoryService(router, store))
	s.AddService(ps.NewInvoiceService(router, store))
	s.AddService(ps.NewCustomerService(router, store))
	s.AddService(ps.NewCreditLineService(router, store))
	s.AddService(ps.NewSponsorService(router, store))
	s.AddService(ps.NewSupplierService(router, store))
	s.AddService(ps.NewExpenseService(router, store))
	s.AddService(ps.NewDepositService(router, store))
	s.AddService(ps.NewBalanceService(router, store))
	s.AddService(ps.NewReportService(router, store))
	s.AddService(ps.NewExpirationMonitor())

	var err = s.Start()

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
